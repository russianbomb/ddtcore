package
{
	import com.demonsters.debugger.MonsterDebugger;
	import flash.display.Sprite;
	public class CodeModule extends Sprite
	{
		private var _instance:CodeModule = null;
		
		public function CodeModule()
		{
			init();
		}
		
		private function init() : void
		{
			_instance = this;
			MonsterDebugger.initialize(this);
			trace("CodeModule initialized~!");
		}
		
		public function get Instance() : CodeModule
		{
			return _instance;
		}
	}
}