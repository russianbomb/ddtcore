package flowerGiving
{
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import ddt.data.UIModuleTypes;
   import ddt.manager.SocketManager;
   import ddt.manager.StateManager;
   import ddt.manager.TimeManager;
   import ddt.states.StateType;
   import ddt.view.UIModuleSmallLoading;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.events.MouseEvent;
   import flash.events.TimerEvent;
   import flash.utils.Dictionary;
   import flash.utils.Timer;
   import flowerGiving.components.FlowerFallMc;
   import flowerGiving.events.FlowerGivingEvent;
   import flowerGiving.views.FlowerGivingFrame;
   import hallIcon.HallIconManager;
   import hallIcon.HallIconType;
   import road7th.comm.PackageIn;
   import wonderfulActivity.WonderfulActivityManager;
   import wonderfulActivity.data.GiftBagInfo;
   import wonderfulActivity.data.GmActivityInfo;
   import wonderfulActivity.data.WonderfulActivityTypeData;
   
   public class FlowerGivingManager extends EventDispatcher
   {
      
      private static var _instance:FlowerGivingManager;
       
      
      private var _frame:FlowerGivingFrame;
      
      private var flowerMc:FlowerFallMc;
      
      public var isShowIcon:Boolean;
      
      public var actId:String;
      
      public var xmlData:GmActivityInfo;
      
      private var delayTimer:Timer;
      
      public var flowerTempleteId:int;
      
      public function FlowerGivingManager()
      {
         super(null);
         this.addEvents();
      }
      
      public static function get instance() : FlowerGivingManager
      {
         if(!_instance)
         {
            _instance = new FlowerGivingManager();
         }
         return _instance;
      }
      
      public function addEvents() : void
      {
         SocketManager.Instance.addEventListener(FlowerGivingEvent.FLOWER_FALL,this.__flowerFallHandler);
         SocketManager.Instance.addEventListener(FlowerGivingEvent.FLOWER_GIVING_OPEN,this.__flowerGivingOpenHandler);
      }
      
      public function removeEvents() : void
      {
         SocketManager.Instance.removeEventListener(FlowerGivingEvent.FLOWER_FALL,this.__flowerFallHandler);
         SocketManager.Instance.removeEventListener(FlowerGivingEvent.FLOWER_GIVING_OPEN,this.__flowerGivingOpenHandler);
      }
      
      protected function __flowerGivingOpenHandler(param1:FlowerGivingEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readInt();
         this.checkOpen();
      }
      
      protected function __flowerFallHandler(param1:FlowerGivingEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readInt();
         var _loc4_:int = _loc2_.readInt();
         var _loc5_:String = StateManager.currentStateType;
         if(_loc5_ == StateType.MAIN || _loc5_ == StateType.ROOM_LIST || _loc5_ == StateType.CONSORTIA || _loc5_ == StateType.AUCTION || _loc5_ == StateType.FARM || _loc5_ == StateType.SHOP || _loc5_ == StateType.TOFFLIST || _loc5_ == StateType.DDTCHURCH_ROOM_LIST || _loc5_ == StateType.CONSORTIA_BATTLE_SCENE || _loc5_ == StateType.MATCH_ROOM || _loc5_ == StateType.DUNGEON_LIST || _loc5_ == StateType.DUNGEON_ROOM || _loc5_ == StateType.CHALLENGE_ROOM)
         {
            if(!this.flowerMc)
            {
               this.flowerMc = new FlowerFallMc();
               this.flowerMc.addEventListener(Event.COMPLETE,this.__flowerMcComplete);
               LayerManager.Instance.addToLayer(this.flowerMc,LayerManager.GAME_TOP_LAYER,false,LayerManager.NONE_BLOCKGOUND,false);
            }
            if(this.delayTimer)
            {
               this.delayTimer.stop();
               this.delayTimer.removeEventListener(TimerEvent.TIMER,this.onDelayTimer);
               this.delayTimer = null;
            }
            this.delayTimer = new Timer(8000,1);
            this.delayTimer.addEventListener(TimerEvent.TIMER,this.onDelayTimer);
            this.delayTimer.start();
         }
      }
      
      protected function __flowerMcComplete(param1:Event) : void
      {
         this.flowerMc.removeEventListener(Event.COMPLETE,this.__flowerMcComplete);
         this.flowerMc.dispose();
         this.flowerMc = null;
      }
      
      protected function onDelayTimer(param1:TimerEvent) : void
      {
         this.delayTimer.stop();
         this.delayTimer.removeEventListener(TimerEvent.TIMER,this.onDelayTimer);
         this.delayTimer = null;
         if(this.flowerMc)
         {
            this.flowerMc.isOver = true;
         }
      }
      
      public function checkOpen() : void
      {
         var _loc3_:GmActivityInfo = null;
         this.isShowIcon = false;
         var _loc1_:Date = TimeManager.Instance.Now();
         var _loc2_:Dictionary = WonderfulActivityManager.Instance.activityData;
         for each(_loc3_ in _loc2_)
         {
            if(_loc3_.activityType == WonderfulActivityTypeData.FLOWER_GIVING_ACTIVITY && _loc1_.time >= Date.parse(_loc3_.beginTime) && _loc1_.time <= Date.parse(_loc3_.endShowTime))
            {
               switch(_loc3_.activityChildType)
               {
                  case 0:
                     this.flowerTempleteId = 334128;
                     break;
                  case 1:
                     this.flowerTempleteId = 334129;
                     break;
                  case 2:
                     this.flowerTempleteId = 334130;
               }
               this.actId = _loc3_.activityId;
               this.xmlData = _loc2_[this.actId];
               this.isShowIcon = true;
            }
         }
         if(this.isShowIcon)
         {
            this.createFlowerIcon();
         }
         else
         {
            this.deleteFlowerIcon();
         }
      }
      
      public function getDataByRewardMark(param1:int) : Array
      {
         var _loc3_:GiftBagInfo = null;
         var _loc2_:Array = new Array();
         for each(_loc3_ in this.xmlData.giftbagArray)
         {
            if(_loc3_.rewardMark == param1)
            {
               _loc2_.push(_loc3_);
            }
         }
         _loc2_.sortOn("giftbagOrder",Array.NUMERIC);
         return _loc2_;
      }
      
      public function createFlowerIcon() : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.FLOWERGIVING,true);
      }
      
      public function deleteFlowerIcon() : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.FLOWERGIVING,false);
      }
      
      public function onIconClick(param1:MouseEvent) : void
      {
         this.onShow();
      }
      
      public function onShow() : void
      {
         if(!this._frame)
         {
            UIModuleSmallLoading.Instance.progress = 0;
            UIModuleSmallLoading.Instance.show();
            UIModuleSmallLoading.Instance.addEventListener(Event.CLOSE,this.onSmallLoadingClose);
            UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.createFlowerGivingFrame);
            UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUIProgress);
            UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.FLOWER_GIVING);
         }
         else
         {
            this._frame = ComponentFactory.Instance.creatComponentByStylename("flowerGiving.flowerGivingFrame");
            LayerManager.Instance.addToLayer(this._frame,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
         }
      }
      
      protected function onSmallLoadingClose(param1:Event) : void
      {
         UIModuleSmallLoading.Instance.hide();
         UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this.onSmallLoadingClose);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.createFlowerGivingFrame);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUIProgress);
      }
      
      protected function onUIProgress(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.FLOWER_GIVING)
         {
            UIModuleSmallLoading.Instance.progress = param1.loader.progress * 100;
         }
      }
      
      protected function createFlowerGivingFrame(param1:UIModuleEvent) : void
      {
         if(param1.module != UIModuleTypes.FLOWER_GIVING)
         {
            return;
         }
         UIModuleSmallLoading.Instance.hide();
         UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this.onSmallLoadingClose);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.createFlowerGivingFrame);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUIProgress);
         this._frame = ComponentFactory.Instance.creatComponentByStylename("flowerGiving.flowerGivingFrame");
         LayerManager.Instance.addToLayer(this._frame,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
      }
   }
}
