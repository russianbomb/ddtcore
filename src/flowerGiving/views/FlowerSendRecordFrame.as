package flowerGiving.views
{
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.container.VBox;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.SoundManager;
   import flash.display.Bitmap;
   import flowerGiving.components.FlowerSendRecordItem;
   
   public class FlowerSendRecordFrame extends Frame
   {
       
      
      private var _bg:Bitmap;
      
      private var _vbox:VBox;
      
      private var _infoArr:Array;
      
      public function FlowerSendRecordFrame(param1:Array)
      {
         this._infoArr = param1;
         super();
         this.initView();
         this.addEvent();
      }
      
      private function initView() : void
      {
         var _loc2_:FlowerSendRecordItem = null;
         this._bg = ComponentFactory.Instance.creat("flowerGiving.flowerSendRecordFrame.bg");
         addToContent(this._bg);
         this._vbox = ComponentFactory.Instance.creatComponentByStylename("flowerGiving.flowerSendRecordFrame.vBox");
         addToContent(this._vbox);
         var _loc1_:int = 0;
         while(_loc1_ < this._infoArr.length)
         {
            _loc2_ = new FlowerSendRecordItem(_loc1_);
            if(this._infoArr[_loc1_])
            {
               _loc2_.setData(this._infoArr[_loc1_]);
            }
            this._vbox.addChild(_loc2_);
            _loc1_++;
         }
      }
      
      private function addEvent() : void
      {
         addEventListener(FrameEvent.RESPONSE,this._responseHandle);
      }
      
      protected function _responseHandle(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         switch(param1.responseCode)
         {
            case FrameEvent.CANCEL_CLICK:
               break;
            case FrameEvent.ESC_CLICK:
            case FrameEvent.CLOSE_CLICK:
               this.dispose();
         }
      }
      
      private function removeEvent() : void
      {
         removeEventListener(FrameEvent.RESPONSE,this._responseHandle);
      }
      
      override public function dispose() : void
      {
         this.removeEvent();
         if(this._bg)
         {
            ObjectUtils.disposeObject(this._bg);
         }
         this._bg = null;
         if(this._vbox)
         {
            ObjectUtils.disposeObject(this._vbox);
         }
         this._vbox = null;
         super.dispose();
      }
   }
}
