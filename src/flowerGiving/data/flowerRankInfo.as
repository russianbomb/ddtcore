package flowerGiving.data
{
   import wonderfulActivity.data.GiftRewardInfo;
   
   public class flowerRankInfo
   {
       
      
      public var place:int;
      
      public var name:String;
      
      public var num:int;
      
      public var vipLvl:int;
      
      public var rewardVec:Vector.<GiftRewardInfo>;
      
      public function flowerRankInfo()
      {
         super();
      }
      
      public function get isVIP() : Boolean
      {
         return this.vipLvl >= 1;
      }
   }
}
