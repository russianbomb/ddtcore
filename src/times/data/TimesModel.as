package times.data
{
   import com.pickgliss.loader.LoaderQueue;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   
   public class TimesModel implements Disposeable
   {
      
      public static const SMALL_PIC_WIDTH:uint = 250;
      
      public static const SMALL_PIC_HEIGHT:uint = 140;
       
      
      public var smallPicInfos:Vector.<TimesPicInfo>;
      
      public var bigPicInfos:Vector.<TimesPicInfo>;
      
      public var contentInfos:Array;
      
      public var webPath:String;
      
      public var edition:int;
      
      public var editor:String;
      
      public var nextDate:String;
      
      public var isDailyGotten:Boolean;
      
      public var isShowEgg:Boolean;
      
      public var smallPicWidth:int = 250;
      
      public var smallPicHeight:int = 140;
      
      private var _thumbnailQueue:LoaderQueue;
      
      private var _thumbnailLoaders:Array;
      
      public function TimesModel()
      {
         super();
      }
      
      private function init() : void
      {
         this.smallPicInfos = new Vector.<TimesPicInfo>();
         this.bigPicInfos = new Vector.<TimesPicInfo>();
      }
      
      public function dispose() : void
      {
         var l:int = 0;
         for(var i:int = 0; i < this.smallPicInfos.length; i++)
         {
            this.smallPicInfos[i] = null;
         }
         this.smallPicInfos = null;
         for(var j:int = 0; j < this.bigPicInfos.length; j++)
         {
            this.bigPicInfos[j] = null;
         }
         this.bigPicInfos = null;
         for(var k:int = 0; k < this.contentInfos.length; k++)
         {
            for(l = 0; l < this.contentInfos[k].length; l++)
            {
               this.contentInfos[k][l] = null;
            }
         }
         this.contentInfos = null;
         ObjectUtils.disposeObject(this._thumbnailQueue);
         this._thumbnailQueue = null;
      }
   }
}
