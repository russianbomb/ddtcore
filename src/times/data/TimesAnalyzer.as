package times.data
{
   import com.pickgliss.loader.DataAnalyzer;
   import com.pickgliss.utils.ObjectUtils;
   
   public class TimesAnalyzer extends DataAnalyzer
   {
       
      
      public var webPath:String;
      
      public var edition:int;
      
      public var editor:String;
      
      public var nextDate:String;
      
      public var smallPicInfos:Vector.<TimesPicInfo>;
      
      public var bigPicInfos:Vector.<TimesPicInfo>;
      
      public var contentInfos:Array;
      
      public function TimesAnalyzer(onCompleteCall:Function)
      {
         super(onCompleteCall);
      }
      
      override public function analyze(data:*) : void
      {
         var xmllist:XMLList = null;
         var i:int = 0;
         var k:int = 0;
         var info:TimesPicInfo = null;
         var info1:TimesPicInfo = null;
         var info2:TimesPicInfo = null;
         this.smallPicInfos = new Vector.<TimesPicInfo>();
         this.bigPicInfos = new Vector.<TimesPicInfo>();
         var xml:XML = new XML(data);
         if(xml.@value == "true")
         {
            this.webPath = xml.@webPath;
            this.edition = xml.@edition;
            this.editor = xml.@editor;
            this.nextDate = xml.@nextDate;
            xmllist = xml..item.(@type == "small");
            for(i = 0; i < xmllist.length(); i++)
            {
               info = new TimesPicInfo();
               ObjectUtils.copyPorpertiesByXML(info,xmllist[i]);
               this.smallPicInfos.push(info);
            }
            xmllist = xml..item.(@type == "big");
            for(i = 0; i < xmllist.length(); i++)
            {
               info1 = new TimesPicInfo();
               ObjectUtils.copyPorpertiesByXML(info1,xmllist[i]);
               this.bigPicInfos.push(info1);
            }
            this.contentInfos = new Array();
            for(k = 0; k < 4; k++)
            {
               xmllist = xml..item.(@type == "category" + String(k));
               this.contentInfos.push(new Vector.<TimesPicInfo>());
               for(i = 0; i < xmllist.length(); i++)
               {
                  info2 = new TimesPicInfo();
                  ObjectUtils.copyPorpertiesByXML(info2,xmllist[i]);
                  info2.category = k;
                  info2.page = i;
                  this.contentInfos[k].push(info2);
               }
            }
            onAnalyzeComplete();
         }
         else
         {
            message = xml.@message;
            onAnalyzeError();
            onAnalyzeComplete();
         }
      }
   }
}
