package times
{
   import com.pickgliss.loader.BaseLoader;
   import com.pickgliss.loader.LoaderManager;
   import com.pickgliss.loader.LoaderQueue;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.ComponentSetting;
   import com.pickgliss.utils.DisplayUtils;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.DisplayObject;
   import flash.display.InteractiveObject;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.events.IEventDispatcher;
   import flash.events.MouseEvent;
   import flash.geom.Point;
   import flash.utils.Dictionary;
   import flash.utils.getDefinitionByName;
   import times.data.TimesAnalyzer;
   import times.data.TimesEvent;
   import times.data.TimesModel;
   import times.data.TimesPicInfo;
   import times.utils.TimesUtils;
   import times.view.TimesContentView;
   import times.view.TimesThumbnailView;
   import times.view.TimesView;
   
   public class TimesController extends EventDispatcher
   {
      
      private static var _instance:TimesController;
       
      
      private var _currentPointer:int;
      
      private var _model:TimesModel;
      
      private var _thumbnailLoaders:Array;
      
      private var _extraDisplays:Dictionary;
      
      private var _tipCurrentKey:String;
      
      private var _tipItemMcs:Dictionary;
      
      private var _timesView:TimesView;
      
      private var _thumbnailView:TimesThumbnailView;
      
      private var _contentViews:Vector.<TimesContentView>;
      
      private var _statisticsInstance;
      
      private var _egg:MovieClip;
      
      private var _eggLoc:Point;
      
      private var _updateContenList:Array;
      
      private var _isShowUpdateView:Boolean = false;
      
      public function TimesController(target:IEventDispatcher = null)
      {
         this._eggLoc = new Point(-1,-1);
         super(target);
         this.init();
      }
      
      public static function get Instance() : TimesController
      {
         if(_instance == null)
         {
            _instance = new TimesController();
         }
         return _instance;
      }
      
      public function get currentPointer() : int
      {
         return this._currentPointer;
      }
      
      public function set currentPointer(value:int) : void
      {
         this._currentPointer = value;
      }
      
      public function setup(analyzer:TimesAnalyzer) : void
      {
         this._model.smallPicInfos = analyzer.smallPicInfos;
         this._model.bigPicInfos = analyzer.bigPicInfos;
         this._model.contentInfos = analyzer.contentInfos;
         this._model.edition = analyzer.edition;
         this._model.editor = analyzer.editor;
         this._model.nextDate = analyzer.nextDate;
         this.loadThumbnail();
      }
      
      public function get model() : TimesModel
      {
         return this._model;
      }
      
      private function init() : void
      {
         this._model = new TimesModel();
         this._tipItemMcs = new Dictionary();
      }
      
      public function initView(timesView:TimesView, thumbnailViews:TimesThumbnailView, contentViews:Vector.<TimesContentView>) : void
      {
         this._timesView = timesView;
         this._thumbnailView = thumbnailViews;
         this._contentViews = contentViews;
         var info:TimesPicInfo = new TimesPicInfo();
         info.type = "category0";
         info.targetCategory = 0;
         info.targetPage = 0;
         this.__gotoContent(new TimesEvent(TimesEvent.GOTO_CONTENT,info));
      }
      
      public function initEvent() : void
      {
         TimesController.Instance.addEventListener(TimesEvent.PUSH_TIP_ITEMS,this.__pushTipItems);
         TimesController.Instance.addEventListener(TimesEvent.GOTO_CONTENT,this.__gotoContent);
         TimesController.Instance.addEventListener(TimesEvent.GOTO_PRE_CONTENT,this.__gotoPreContent);
         TimesController.Instance.addEventListener(TimesEvent.GOTO_NEXT_CONTENT,this.__gotoNextContent);
         TimesController.Instance.addEventListener(TimesEvent.PUSH_TIP_CELLS,this.__pushTipCells);
      }
      
      public function removeEvent() : void
      {
         TimesController.Instance.removeEventListener(TimesEvent.PUSH_TIP_ITEMS,this.__pushTipItems);
         TimesController.Instance.removeEventListener(TimesEvent.GOTO_CONTENT,this.__gotoContent);
         TimesController.Instance.removeEventListener(TimesEvent.GOTO_PRE_CONTENT,this.__gotoPreContent);
         TimesController.Instance.removeEventListener(TimesEvent.GOTO_NEXT_CONTENT,this.__gotoNextContent);
         TimesController.Instance.removeEventListener(TimesEvent.PUSH_TIP_CELLS,this.__pushTipCells);
      }
      
      private function __pushTipCells(event:TimesEvent) : void
      {
         this.addTip(event.info.category,event.info.page,event.params);
      }
      
      private function __gotoContent(event:TimesEvent) : void
      {
         var managerReference:* = undefined;
         var info:TimesPicInfo = event.info;
         this.currentPointer = info.targetCategory;
         this._timesView.menuSelected(this.currentPointer);
         this._contentViews[this.currentPointer].frame = info.targetPage;
         this.tryRemoveAllViews();
         this._timesView.addChild(this._contentViews[this.currentPointer]);
         this.tryShowTipDisplay(this.currentPointer,info.targetPage);
         this.tryShowEgg();
         this.updateGuildViewPoint(this.currentPointer,info.targetPage);
         if(this._statisticsInstance)
         {
            this._statisticsInstance.startTick();
         }
         else
         {
            managerReference = getDefinitionByName("times.TimesManager") as Class;
            if(managerReference)
            {
               this._statisticsInstance = managerReference.Instance.statistics;
               this._statisticsInstance.startTick();
            }
         }
      }
      
      private function __gotoNextContent(event:TimesEvent) : void
      {
         var managerReference:* = undefined;
         this.currentPointer = this.currentPointer < 3?int(this.currentPointer + 1):int(0);
         this._timesView.menuSelected(this.currentPointer);
         this.tryRemoveAllViews();
         this._contentViews[this.currentPointer].frame = 0;
         this._timesView.addChild(this._contentViews[this.currentPointer]);
         this.tryShowTipDisplay(this.currentPointer,0);
         this.tryShowEgg();
         this.updateGuildViewPoint(this.currentPointer,0);
         if(this._statisticsInstance)
         {
            this._statisticsInstance.startTick();
         }
         else
         {
            managerReference = getDefinitionByName("times.TimesManager") as Class;
            if(managerReference)
            {
               this._statisticsInstance = managerReference.Instance.statistics;
               this._statisticsInstance.startTick();
            }
         }
      }
      
      private function __gotoPreContent(event:TimesEvent) : void
      {
         var managerReference:* = undefined;
         this.currentPointer = this.currentPointer > 0?int(this.currentPointer - 1):int(this._contentViews.length - 1);
         this._timesView.menuSelected(this.currentPointer);
         this.tryRemoveAllViews();
         this._contentViews[this.currentPointer].frame = this._contentViews[this.currentPointer].maxIdx - 1;
         this._timesView.addChild(this._contentViews[this.currentPointer]);
         this.tryShowTipDisplay(this.currentPointer,this._contentViews[this.currentPointer].maxIdx - 1);
         this.tryShowEgg();
         this.updateGuildViewPoint(this.currentPointer,this._contentViews[this.currentPointer].maxIdx - 1);
         if(this._statisticsInstance)
         {
            this._statisticsInstance.startTick();
         }
         else
         {
            managerReference = getDefinitionByName("times.TimesManager") as Class;
            if(managerReference)
            {
               this._statisticsInstance = managerReference.Instance.statistics;
               this._statisticsInstance.startTick();
            }
         }
      }
      
      private function __pushTipItems(event:TimesEvent) : void
      {
         var arr:Array = null;
         var info:TimesPicInfo = event.info as TimesPicInfo;
         var item:MovieClip = event.params[0] as MovieClip;
         var key:String = String(info.category) + String(info.page);
         if(this._tipItemMcs[key])
         {
            arr = this._tipItemMcs[key];
         }
         else
         {
            arr = [];
         }
         arr.push(item);
         this._tipItemMcs[key] = arr;
      }
      
      public function updateGuildViewPoint(posX:int = -1, posY:int = -1) : void
      {
         var j:int = 0;
         var info:TimesPicInfo = null;
         var idx:int = 0;
         if(posX == -1 && posY == -1)
         {
            this._thumbnailView.pointIdx = idx;
            return;
         }
         for(var i:int = 0; i < this._model.contentInfos.length; i++)
         {
            for(j = 0; j < this._model.contentInfos[i].length; j++)
            {
               info = this._model.contentInfos[i][j];
               if(posX == info.category && posY == info.page)
               {
                  this._thumbnailView.pointIdx = idx;
                  return;
               }
               idx++;
            }
         }
      }
      
      private function tryRemoveAllViews() : void
      {
         for(var i:int = 0; i < this._contentViews.length; i++)
         {
            if(this._contentViews[i] && this._timesView.contains(this._contentViews[i]))
            {
               this._timesView.removeChild(this._contentViews[i]);
            }
         }
      }
      
      private function addTip(category:int, page:int, tipDisplayObjs:Array) : void
      {
         var key:String = String(category) + String(page);
         var container:Sprite = new Sprite();
         TimesUtils.setPos(container,"times.ContentBigPicPos");
         for(var i:int = 0; i < tipDisplayObjs.length; i++)
         {
            container.addChild(tipDisplayObjs[i]);
            tipDisplayObjs[i].addEventListener(MouseEvent.ROLL_OVER,this.__playEffect);
            tipDisplayObjs[i].addEventListener(MouseEvent.ROLL_OUT,this.__stopEffect);
         }
         if(this._extraDisplays == null)
         {
            this._extraDisplays = new Dictionary();
         }
         this._extraDisplays[key] = container;
      }
      
      private function __stopEffect(event:MouseEvent) : void
      {
         if(this._extraDisplays == null)
         {
            event.currentTarget.removeEventListener(MouseEvent.ROLL_OVER,this.__playEffect);
            event.currentTarget.removeEventListener(MouseEvent.ROLL_OUT,this.__stopEffect);
            return;
         }
         var idx:int = this._extraDisplays[this._tipCurrentKey].getChildIndex(event.currentTarget);
         this._tipItemMcs[this._tipCurrentKey][idx].gotoAndStop(1);
      }
      
      private function __playEffect(event:MouseEvent) : void
      {
         if(this._extraDisplays == null)
         {
            event.currentTarget.removeEventListener(MouseEvent.ROLL_OVER,this.__playEffect);
            event.currentTarget.removeEventListener(MouseEvent.ROLL_OUT,this.__stopEffect);
            return;
         }
         var idx:int = this._extraDisplays[this._tipCurrentKey].getChildIndex(event.currentTarget);
         this._tipItemMcs[this._tipCurrentKey][idx].gotoAndStop(2);
      }
      
      public function clearExtraObjects() : void
      {
         var arr:Array = null;
         var mc:MovieClip = null;
         for each(arr in this._tipItemMcs)
         {
            while(arr.length > 0)
            {
               mc = arr.shift();
               mc = null;
            }
            arr = null;
         }
         this._tipItemMcs = new Dictionary();
         if(this._egg)
         {
            this._egg.stop();
            ObjectUtils.disposeObject(this._egg);
            this._egg = null;
         }
         this.deleteTipDisplay();
      }
      
      public function tryShowTipDisplay(category:int, page:int) : void
      {
         this.removeTipDisplay();
         this._tipCurrentKey = String(category) + String(page);
         if(this._extraDisplays && this._extraDisplays[this._tipCurrentKey])
         {
            this._timesView.addChild(this._extraDisplays[this._tipCurrentKey]);
            if(this._egg && this._timesView.contains(this._egg))
            {
               this._timesView.addChild(this._egg);
            }
         }
      }
      
      private function removeTipDisplay() : void
      {
         var key:* = null;
         for(key in this._extraDisplays)
         {
            if(this._extraDisplays[key].parent)
            {
               this._extraDisplays[key].parent.removeChild(this._extraDisplays[key]);
            }
         }
      }
      
      private function deleteTipDisplay() : void
      {
         var key:* = null;
         var len:int = 0;
         var i:int = 0;
         var disp:DisplayObject = null;
         this.removeTipDisplay();
         for(key in this._extraDisplays)
         {
            len = this._extraDisplays[key];
            for(i = 0; i < len; i++)
            {
               disp = this._extraDisplays[key].getChildAt(i);
               if(disp is InteractiveObject)
               {
                  disp.addEventListener(MouseEvent.ROLL_OVER,this.__playEffect);
                  disp.addEventListener(MouseEvent.ROLL_OUT,this.__stopEffect);
               }
            }
            delete this._extraDisplays[key];
         }
         this._extraDisplays = null;
      }
      
      public function tryShowEgg() : void
      {
         var __eggClick:Function = null;
         var initialize:Function = function():void
         {
            _egg = ComponentFactory.Instance.creat("times.Egg");
            _egg.buttonMode = true;
            _egg.addEventListener(MouseEvent.CLICK,__eggClick);
            _eggLoc.x = _currentPointer;
            _eggLoc.y = _contentViews[_currentPointer].frame;
            _timesView.addChild(_egg);
         };
         __eggClick = function(event:MouseEvent):void
         {
            ComponentSetting.SEND_USELOG_ID(145);
            TimesController.Instance.dispatchEvent(new TimesEvent(TimesEvent.GOT_EGG));
            eggDispose();
         };
         var bingo:Function = function():Boolean
         {
            var num:Number = Math.random() * 5;
            return num > 4;
         };
         var eggDispose:Function = function():void
         {
            if(_egg && _egg.parent)
            {
               _egg.parent.removeChild(_egg);
               _egg.removeEventListener(MouseEvent.CLICK,__eggClick);
            }
            _egg = null;
            _eggLoc = null;
         };
         if(!this._egg && this._model.isShowEgg && bingo())
         {
            initialize();
         }
         if(this._egg)
         {
            if(this._currentPointer == this._eggLoc.x && this._contentViews[this._currentPointer].frame == this._eggLoc.y)
            {
               this._timesView.addChild(this._egg);
            }
            else
            {
               DisplayUtils.removeDisplay(this._egg);
            }
         }
      }
      
      public function get thumbnailLoaders() : Array
      {
         return this._thumbnailLoaders;
      }
      
      private function loadThumbnail() : void
      {
         var _queue:LoaderQueue = null;
         var __onQueueComplete:Function = null;
         var arr:Array = null;
         var j:int = 0;
         var info:TimesPicInfo = null;
         __onQueueComplete = function(e:Event):void
         {
            _queue.dispose();
            _queue = null;
            TimesController.Instance.dispatchEvent(new TimesEvent(TimesEvent.THUMBNAIL_LOAD_COMPLETE));
         };
         this._thumbnailLoaders = new Array();
         _queue = new LoaderQueue();
         _queue.addEventListener(Event.COMPLETE,__onQueueComplete);
         for(var i:int = 0; i < this._model.contentInfos.length; i++)
         {
            arr = new Array();
            for(j = 0; j < this._model.contentInfos[i].length; j++)
            {
               info = this._model.contentInfos[i][j];
               if(info.thumbnailPath && info.thumbnailPath != "null" && info.thumbnailPath != "")
               {
                  arr.push(LoaderManager.Instance.creatLoader(this._model.webPath + info.thumbnailPath,BaseLoader.BITMAP_LOADER));
                  _queue.addLoader(arr[j]);
               }
            }
            this._thumbnailLoaders.push(arr);
         }
         _queue.start();
      }
      
      public function dispose() : void
      {
         var display:DisplayObject = null;
         var i:int = 0;
         var p:int = 0;
         this._model.dispose();
         for(var t:int = 0; t < this._thumbnailLoaders.length; t++)
         {
            for(p = 0; p < this._thumbnailLoaders[t].length; p++)
            {
               ObjectUtils.disposeObject(this._thumbnailLoaders[t][p]);
               this._thumbnailLoaders[t][p] = null;
            }
         }
         this._thumbnailLoaders = null;
         for each(display in this._extraDisplays)
         {
            if(display)
            {
               ObjectUtils.disposeObject(display);
               display = null;
            }
         }
         this._extraDisplays = null;
         for(i = 0; i < this._tipItemMcs.length; i++)
         {
            if(this._tipItemMcs[i])
            {
               if(this._tipItemMcs[i].parent)
               {
                  this._tipItemMcs[i].parent.removeChild(this._tipItemMcs[i]);
               }
               MovieClip(this._tipItemMcs[i]).loaderInfo.loader.unloadAndStop();
               this._tipItemMcs[i] = null;
            }
         }
         if(this._statisticsInstance)
         {
            this._statisticsInstance.dispose();
         }
         this._tipItemMcs = null;
         this._timesView = null;
         this._contentViews = null;
      }
      
      public function get updateContenList() : Array
      {
         return this._updateContenList;
      }
      
      public function set updateContenList(value:Array) : void
      {
         this._updateContenList = value;
      }
      
      public function get isShowUpdateView() : Boolean
      {
         return this._isShowUpdateView;
      }
      
      public function set isShowUpdateView(value:Boolean) : void
      {
         var tmpArray:Array = null;
         var tmpInfoList:Vector.<TimesPicInfo> = null;
         var tmpInfo:TimesPicInfo = null;
         var tmpInfo2:TimesPicInfo = null;
         var tmpInfoList2:Vector.<TimesPicInfo> = null;
         if(this._isShowUpdateView == value)
         {
            return;
         }
         this._isShowUpdateView = value;
         if(this._isShowUpdateView)
         {
            tmpArray = [null];
            this._thumbnailLoaders[0] = tmpArray.concat(this._thumbnailLoaders[0] as Array);
            tmpInfoList = this._model.contentInfos[0];
            for each(tmpInfo in tmpInfoList)
            {
               tmpInfo.page++;
            }
            tmpInfo2 = new TimesPicInfo();
            tmpInfo2.category = 0;
            tmpInfo2.page = 0;
            if(tmpInfoList[0])
            {
               tmpInfo2.type = tmpInfoList[0].type;
            }
            tmpInfoList2 = new Vector.<TimesPicInfo>();
            tmpInfoList2.push(tmpInfo2);
            this._model.contentInfos[0] = tmpInfoList2.concat(this._model.contentInfos[0]);
         }
      }
   }
}
