package times.view
{
   import com.pickgliss.ui.controls.SelectedButtonGroup;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.Sprite;
   import times.TimesController;
   import times.data.TimesPicInfo;
   
   public class TimesThumbnailView extends Sprite implements Disposeable
   {
       
      
      private var _controller:TimesController;
      
      private var _pointArr:Vector.<TimesThumbnailPoint>;
      
      private var _spacing:int;
      
      private var _pointGroup:SelectedButtonGroup;
      
      private var _pointIdx:int;
      
      public function TimesThumbnailView($controller:TimesController)
      {
         super();
         this._controller = $controller;
         this.init();
      }
      
      private function init() : void
      {
         var pointSum:int = 0;
         var j:int = 0;
         var info:TimesPicInfo = null;
         var point:TimesThumbnailPoint = null;
         this._pointGroup = new SelectedButtonGroup();
         this._pointArr = new Vector.<TimesThumbnailPoint>();
         var lenArr:Vector.<int> = new Vector.<int>();
         var idx:int = 0;
         var infos:Array = this._controller.model.contentInfos;
         for(var i:int = 0; i < infos.length; i++)
         {
            lenArr.push(infos[i].length);
            pointSum = pointSum + lenArr[i];
         }
         if(pointSum != 0)
         {
            this._spacing = 360 / (pointSum - 1);
         }
         for(var k:int = 0; k < infos.length; k++)
         {
            for(j = 0; j < lenArr[k]; j++)
            {
               info = new TimesPicInfo();
               info.targetCategory = k;
               info.targetPage = j;
               point = new TimesThumbnailPoint(info);
               point.tipStyle = "times.view.TimesThumbnailPointTip";
               point.tipDirctions = "0";
               point.tipGapV = 10;
               point.tipData = {
                  "isRevertTip":Boolean(idx > pointSum / 2),
                  "category":k,
                  "page":j
               };
               point.x = idx++ * this._spacing;
               this._pointGroup.addSelectItem(point);
               addChild(point);
               this._pointArr.push(point);
            }
         }
         this._pointGroup.selectIndex = 0;
      }
      
      public function set pointIdx(value:int) : void
      {
         if(this._pointIdx == value)
         {
            return;
         }
         this._pointArr[this._pointIdx].pointPlay("rollOut");
         this._pointIdx = value;
         this._pointArr[this._pointIdx].pointStop("selected");
         this._pointGroup.selectIndex = this._pointIdx;
      }
      
      public function dispose() : void
      {
         this._controller = null;
         ObjectUtils.disposeObject(this._pointGroup);
         this._pointGroup = null;
         this._pointArr = null;
         this._controller = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
