package times.view
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import flash.geom.Point;
   import times.TimesController;
   import times.data.TimesEvent;
   import times.data.TimesPicInfo;
   
   public class TimesView extends Sprite implements Disposeable
   {
       
      
      private var _controller:TimesController;
      
      private var _bg:Bitmap;
      
      private var _closeBtn:SimpleBitmapButton;
      
      private var _contentViews:Vector.<TimesContentView>;
      
      private var _thumbnailView:TimesThumbnailView;
      
      private var _dateView:TimesDateView;
      
      private var _menuView:TimesMenuView;
      
      public function TimesView()
      {
         super();
         this.init();
      }
      
      public function init() : void
      {
         this._controller = TimesController.Instance;
         this._contentViews = new Vector.<TimesContentView>(4);
         this._bg = ComponentFactory.Instance.creatBitmap("asset.times.Bg");
         this._closeBtn = ComponentFactory.Instance.creatComponentByStylename("times.CloseBtn");
         this._thumbnailView = ComponentFactory.Instance.creatCustomObject("times.TimesThumbnailView",[this._controller]);
         this._dateView = ComponentFactory.Instance.creatCustomObject("times.DateView");
         this._menuView = ComponentFactory.Instance.creatCustomObject("times.MenuView");
         for(var i:int = 0; i < this._contentViews.length; i++)
         {
            this._contentViews[i] = new TimesContentView();
            this._contentViews[i].init(this._controller.model.contentInfos[i]);
         }
         this._closeBtn.addEventListener(MouseEvent.CLICK,this.__closeClick);
         addChild(this._bg);
         addChild(this._closeBtn);
         addChild(this._thumbnailView);
         addChild(this._dateView);
         addChild(this._menuView);
         this._controller.initView(this,this._thumbnailView,this._contentViews);
      }
      
      public function menuSelected(index:int) : void
      {
         this._menuView.selected = index;
      }
      
      private function __closeClick(event:MouseEvent) : void
      {
         this._closeBtn.removeEventListener(MouseEvent.CLICK,this.__closeClick);
         this._controller.dispatchEvent(new TimesEvent(TimesEvent.CLOSE_VIEW));
      }
      
      public function updateGuildViewPoint(pagePos:Point) : void
      {
         var j:int = 0;
         var info:TimesPicInfo = null;
         var idx:int = 0;
         if(pagePos.x == -1 && pagePos.y == -1)
         {
            this._thumbnailView.pointIdx = idx;
            return;
         }
         for(var i:int = 0; i < this._controller.model.contentInfos.length; i++)
         {
            for(j = 0; j < this._controller.model.contentInfos[i].length; j++)
            {
               info = this._controller.model.contentInfos[i][j];
               if(pagePos.x == info.category && pagePos.y == info.page)
               {
                  this._thumbnailView.pointIdx = idx + 1;
                  return;
               }
               idx++;
            }
         }
      }
      
      public function dispose() : void
      {
         this._controller = null;
         this._closeBtn.removeEventListener(MouseEvent.CLICK,this.__closeClick);
         ObjectUtils.disposeObject(this._bg);
         this._bg = null;
         ObjectUtils.disposeObject(this._closeBtn);
         this._closeBtn = null;
         ObjectUtils.disposeObject(this._dateView);
         this._dateView = null;
         ObjectUtils.disposeObject(this._menuView);
         this._menuView = null;
         for(var i:int = 0; i < this._contentViews.length; i++)
         {
            ObjectUtils.disposeObject(this._contentViews[i]);
            this._contentViews[i] = null;
         }
         this._contentViews = null;
         ObjectUtils.disposeObject(this._thumbnailView);
         this._thumbnailView = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
