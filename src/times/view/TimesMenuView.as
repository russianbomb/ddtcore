package times.view
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.SelectedButton;
   import com.pickgliss.ui.controls.SelectedButtonGroup;
   import com.pickgliss.ui.controls.container.VBox;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.Sprite;
   import flash.events.Event;
   import times.TimesController;
   import times.data.TimesEvent;
   import times.data.TimesPicInfo;
   
   public class TimesMenuView extends Sprite implements Disposeable
   {
       
      
      protected var _vBox:VBox;
      
      protected var _btnGroup:SelectedButtonGroup;
      
      protected var _menus:Vector.<SelectedButton>;
      
      protected var _infos:Vector.<TimesPicInfo>;
      
      public function TimesMenuView()
      {
         super();
         this.init();
      }
      
      protected function init() : void
      {
         this._vBox = ComponentFactory.Instance.creatComponentByStylename("times.MenuContainer");
         this._menus = new Vector.<SelectedButton>(4);
         this._infos = new Vector.<TimesPicInfo>(4);
         for(var i:int = 0; i < this._menus.length; i++)
         {
            this._menus[i] = ComponentFactory.Instance.creatComponentByStylename("times.MenuButton_" + String(i + 1));
            this._vBox.addChild(this._menus[i]);
            this._infos[i] = new TimesPicInfo();
            this._infos[i].type = "category" + String(i);
            this._infos[i].targetCategory = i;
            this._infos[i].targetPage = 0;
         }
         this._btnGroup = new SelectedButtonGroup();
         for(var j:int = 0; j < 4; j++)
         {
            this._btnGroup.addSelectItem(this._menus[j]);
         }
         this._btnGroup.selectIndex = 0;
         this._btnGroup.addEventListener(Event.CHANGE,this.__btnChangeHandler);
         addChild(this._vBox);
      }
      
      public function set selected(index:int) : void
      {
         this._btnGroup.selectIndex = index;
      }
      
      private function __btnChangeHandler(event:Event) : void
      {
         TimesController.Instance.dispatchEvent(new TimesEvent(TimesEvent.PLAY_SOUND));
         TimesController.Instance.dispatchEvent(new TimesEvent(TimesEvent.GOTO_CONTENT,this._infos[this._btnGroup.selectIndex]));
      }
      
      public function dispose() : void
      {
         this._btnGroup.removeEventListener(Event.CHANGE,this.__btnChangeHandler);
         for(var i:int = 0; i < this._menus.length; i++)
         {
            this._infos[i] = null;
         }
         ObjectUtils.disposeObject(this._vBox);
         this._vBox = null;
         this._menus = null;
         this._infos = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
