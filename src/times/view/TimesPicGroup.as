package times.view
{
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.events.TimerEvent;
   import flash.utils.Timer;
   import times.TimesController;
   import times.data.TimesEvent;
   import times.data.TimesPicInfo;
   
   public class TimesPicGroup extends Sprite implements Disposeable
   {
       
      
      private var _pics:Vector.<TimesPicBase>;
      
      private var _infos:Vector.<TimesPicInfo>;
      
      private var _currentIdx:int;
      
      private var _maxIdx:int;
      
      private var _picType:String;
      
      private var _timer:Timer;
      
      public function TimesPicGroup($info:Vector.<TimesPicInfo>)
      {
         super();
         this._infos = $info;
         this.init();
         this.initEvent();
      }
      
      public function init() : void
      {
         this._pics = new Vector.<TimesPicBase>();
         this._timer = new Timer(5000);
         this._maxIdx = this._infos.length;
         for(var i:int = 0; i < this._maxIdx; i++)
         {
            this._pics.push(new TimesPicBase(this._infos[i]));
            this._pics[i].load();
         }
         addChild(this._pics[this._currentIdx]);
      }
      
      protected function initEvent() : void
      {
         addEventListener(MouseEvent.CLICK,this.__onPicClick);
         if(this._infos && this._infos[0] && this._infos[0].type == TimesPicInfo.BIG)
         {
            addEventListener(MouseEvent.ROLL_OVER,this.__onMouseOver);
            addEventListener(MouseEvent.ROLL_OUT,this.__onMouseOut);
            this._timer.addEventListener(TimerEvent.TIMER,this.__onTick);
            this._timer.start();
         }
      }
      
      private function __onMouseOut(event:MouseEvent) : void
      {
         if(this._timer && !this._timer.running)
         {
            this._timer.start();
         }
      }
      
      private function __onMouseOver(event:MouseEvent) : void
      {
         if(this._timer && this._timer.running)
         {
            this._timer.stop();
         }
      }
      
      private function __onPicClick(e:MouseEvent) : void
      {
         if(this._infos[this._currentIdx].type == TimesPicInfo.BIG || this._infos[this._currentIdx].type == TimesPicInfo.SMALL)
         {
            TimesController.Instance.dispatchEvent(new TimesEvent(TimesEvent.GOTO_CONTENT,this._infos[this._currentIdx]));
         }
      }
      
      private function __onTick(event:TimerEvent) : void
      {
         if(this._currentIdx == this._maxIdx - 1)
         {
            this.currentIdx = 0;
         }
         else
         {
            this.currentIdx++;
         }
         dispatchEvent(new Event(Event.CHANGE));
      }
      
      public function get currentIdx() : int
      {
         return this._currentIdx;
      }
      
      public function set currentIdx(value:int) : void
      {
         if(contains(this._pics[this._currentIdx]))
         {
            removeChild(this._pics[this._currentIdx]);
         }
         this._currentIdx = value;
         this._timer.reset();
         this._timer.start();
         this.load(this._currentIdx);
         addChild(this._pics[this._currentIdx]);
      }
      
      public function get currentInfo() : TimesPicInfo
      {
         return this._infos[this._currentIdx];
      }
      
      public function set picType(str:String) : void
      {
         this._picType = str;
         for(var i:int = 0; i < this._maxIdx; i++)
         {
            this._infos[i].type = str;
         }
      }
      
      public function load(startIdx:int) : void
      {
         this.tryLoad(startIdx);
         this.tryLoad(startIdx + 1);
         this.tryLoad(startIdx + 2);
      }
      
      private function tryLoad(idx:int) : void
      {
         if(idx >= this._pics.length)
         {
            return;
         }
         if(idx < this._pics.length && this._pics[idx])
         {
            this._pics[idx].load();
         }
      }
      
      public function dispose() : void
      {
         removeEventListener(MouseEvent.CLICK,this.__onPicClick);
         if(this._infos && this._infos[0] && this._infos[0].type == TimesPicInfo.BIG)
         {
            removeEventListener(MouseEvent.ROLL_OVER,this.__onMouseOver);
            removeEventListener(MouseEvent.ROLL_OUT,this.__onMouseOut);
         }
         for(var i:int = 0; i < this._pics.length; i++)
         {
            ObjectUtils.disposeObject(this._pics[i]);
            this._pics[i] = null;
         }
         this._infos = null;
         this._pics = null;
         if(this._timer)
         {
            this._timer.stop();
            this._timer.removeEventListener(TimerEvent.TIMER,this.__onTick);
            this._timer = null;
         }
      }
   }
}
