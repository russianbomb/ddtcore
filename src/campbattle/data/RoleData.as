package campbattle.data
{
   public class RoleData
   {
       
      
      public var userID:int;
      
      public var type:int;
      
      public var sex:Boolean;
      
      public var name:String;
      
      public var team:int;
      
      public var baseURl:String;
      
      public var Style:String;
      
      public var Colors:String;
      
      public var isDefault:Boolean;
      
      public var sceneID:int;
      
      public var posX:int;
      
      public var posY:int;
      
      public var zoneID:int;
      
      public var stateType:int;
      
      public var isCapture:Boolean;
      
      public var timerCount:int;
      
      public var isVip:Boolean;
      
      public var vipLev:int;
      
      public var isDead:Boolean;
      
      public function RoleData()
      {
         super();
      }
   }
}
