package groupPurchase.view
{
   import bagAndInfo.cell.BagCell;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.manager.ItemManager;
   import ddt.utils.PositionUtils;
   import flash.display.Sprite;
   import groupPurchase.GroupPurchaseManager;
   import groupPurchase.data.GroupPurchaseAwardInfo;
   
   public class GroupPurchaseAwardView extends Sprite implements Disposeable
   {
       
      
      private var _awardList:Object;
      
      public function GroupPurchaseAwardView()
      {
         super();
         this._awardList = GroupPurchaseManager.instance.awardInfoList;
         var _loc1_:int = 1;
         while(_loc1_ <= 12)
         {
            this.createAwardCell(_loc1_);
            _loc1_++;
         }
      }
      
      private function createAwardCell(param1:int) : void
      {
         var _loc3_:BagCell = null;
         var _loc4_:InventoryItemInfo = null;
         var _loc2_:GroupPurchaseAwardInfo = this._awardList[param1];
         if(_loc2_)
         {
            _loc3_ = new BagCell(1,null,true,null,false);
            _loc3_.tipGapH = 0;
            _loc3_.tipGapV = 0;
            _loc4_ = new InventoryItemInfo();
            _loc4_.TemplateID = _loc2_.TemplateID;
            ItemManager.fill(_loc4_);
            _loc4_.StrengthenLevel = _loc2_.StrengthenLevel;
            _loc4_.AttackCompose = _loc2_.AttackCompose;
            _loc4_.DefendCompose = _loc2_.DefendCompose;
            _loc4_.LuckCompose = _loc2_.LuckCompose;
            _loc4_.AgilityCompose = _loc2_.AgilityCompose;
            _loc4_.IsBinds = _loc2_.IsBind;
            _loc4_.ValidDate = _loc2_.ValidDate;
            _loc4_.Count = _loc2_.Count;
            _loc3_.info = _loc4_;
            if(_loc4_.Count > 1)
            {
               _loc3_.setCount(_loc4_.Count);
            }
            PositionUtils.setPos(_loc3_,"groupPurchase.awardCellPos" + param1);
            addChild(_loc3_);
         }
      }
      
      public function dispose() : void
      {
         ObjectUtils.disposeAllChildren(this);
         this._awardList = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
