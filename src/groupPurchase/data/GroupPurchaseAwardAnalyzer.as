package groupPurchase.data
{
   import com.pickgliss.loader.DataAnalyzer;
   import com.pickgliss.utils.ObjectUtils;
   
   public class GroupPurchaseAwardAnalyzer extends DataAnalyzer
   {
       
      
      private var _awardList:Object;
      
      public function GroupPurchaseAwardAnalyzer(param1:Function)
      {
         super(param1);
      }
      
      override public function analyze(param1:*) : void
      {
         var _loc3_:XMLList = null;
         var _loc4_:int = 0;
         var _loc5_:GroupPurchaseAwardInfo = null;
         var _loc2_:XML = new XML(param1);
         this._awardList = {};
         if(_loc2_.@value == "true")
         {
            _loc3_ = _loc2_..item;
            _loc4_ = 0;
            while(_loc4_ < _loc3_.length())
            {
               _loc5_ = new GroupPurchaseAwardInfo();
               ObjectUtils.copyPorpertiesByXML(_loc5_,_loc3_[_loc4_]);
               this._awardList[_loc5_.SortID] = _loc5_;
               _loc4_++;
            }
            onAnalyzeComplete();
         }
         else
         {
            message = _loc2_.@message;
            onAnalyzeError();
            onAnalyzeError();
         }
      }
      
      public function get awardList() : Object
      {
         return this._awardList;
      }
   }
}
