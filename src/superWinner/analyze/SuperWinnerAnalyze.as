package superWinner.analyze
{
   import com.pickgliss.loader.DataAnalyzer;
   import superWinner.data.SuperWinnerAwardsMode;
   
   public class SuperWinnerAnalyze extends DataAnalyzer
   {
       
      
      private var _awardsArr:Vector.<Object>;
      
      public function SuperWinnerAnalyze(param1:Function)
      {
         this._awardsArr = new Vector.<Object>();
         super(param1);
      }
      
      override public function analyze(param1:*) : void
      {
         var _loc6_:Vector.<SuperWinnerAwardsMode> = null;
         var _loc7_:XML = null;
         var _loc8_:uint = 0;
         var _loc9_:SuperWinnerAwardsMode = null;
         var _loc2_:XML = new XML(param1);
         var _loc3_:XMLList = _loc2_..Item;
         var _loc4_:int = 0;
         while(_loc4_ < 6)
         {
            _loc6_ = new Vector.<SuperWinnerAwardsMode>();
            this._awardsArr.push(_loc6_);
            _loc4_++;
         }
         var _loc5_:int = 0;
         while(_loc5_ < _loc3_.length())
         {
            _loc7_ = _loc3_[_loc5_];
            _loc8_ = _loc7_.@rank - 1;
            _loc9_ = new SuperWinnerAwardsMode();
            _loc9_.type = _loc7_.@rank;
            _loc9_.goodId = _loc7_.@template;
            _loc9_.count = _loc7_.@count;
            (this._awardsArr[5 - _loc8_] as Vector.<SuperWinnerAwardsMode>).push(_loc9_);
            _loc5_++;
         }
         onAnalyzeComplete();
      }
      
      public function get awards() : Vector.<Object>
      {
         return this._awardsArr;
      }
   }
}
