package superWinner.manager
{
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.manager.StateManager;
   import ddt.states.StateType;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.events.MouseEvent;
   import hallIcon.HallIconManager;
   import hallIcon.HallIconType;
   import road7th.comm.PackageIn;
   import superWinner.analyze.SuperWinnerAnalyze;
   import superWinner.controller.SuperWinnerController;
   import superWinner.data.SuperWinnerPackageType;
   import superWinner.model.SuperWinnerModel;
   
   public class SuperWinnerManager extends EventDispatcher
   {
      
      private static var _instance:SuperWinnerManager;
      
      public static const ROOM_IS_OPEN:String = "roomIsOpen";
       
      
      private var _model:SuperWinnerModel;
      
      private var _isOpen:Boolean = false;
      
      private var isFirstOpen:Boolean;
      
      private var _awardsVector:Vector.<Object>;
      
      public function SuperWinnerManager(param1:PrivateClass)
      {
         super();
      }
      
      public static function get instance() : SuperWinnerManager
      {
         if(SuperWinnerManager._instance == null)
         {
            SuperWinnerManager._instance = new SuperWinnerManager(new PrivateClass());
         }
         return SuperWinnerManager._instance;
      }
      
      public function get isOpen() : Boolean
      {
         return this._isOpen;
      }
      
      public function setup() : void
      {
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.SUPER_WINNER,this.pkgHandler);
      }
      
      private function pkgHandler(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = param1._cmd;
         switch(_loc3_)
         {
            case SuperWinnerPackageType.SUPER_WINNER_OPEN:
               this._isOpen = _loc2_.readBoolean();
               if(this._isOpen)
               {
                  this.showSuperWinner();
                  if(!this.isFirstOpen)
                  {
                     dispatchEvent(new Event(ROOM_IS_OPEN));
                  }
               }
               else
               {
                  this.hideSuperWinnerIcon();
               }
               this.isFirstOpen = this._isOpen;
               break;
            case SuperWinnerPackageType.ENTER_ROOM:
               StateManager.setState(StateType.SUPER_WINNER);
               SuperWinnerController.instance.enterSuperWinnerRoom(_loc2_);
               break;
            case SuperWinnerPackageType.RETURN_DICES:
               SuperWinnerController.instance.returnDices(_loc2_);
               break;
            case SuperWinnerPackageType.START_ROLL_DICES:
               SuperWinnerController.instance.startRollDices();
               break;
            case SuperWinnerPackageType.TIMES_UP:
               SuperWinnerController.instance.timesUp(_loc2_);
               break;
            case SuperWinnerPackageType.JOIN_ROOM:
               SuperWinnerController.instance.joinRoom(_loc2_);
               break;
            case SuperWinnerPackageType.END_GAME:
               SuperWinnerController.instance.endGame();
         }
      }
      
      public function showSuperWinner() : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.SUPERWINNER,true);
      }
      
      public function openSuperWinner(param1:MouseEvent) : void
      {
         SoundManager.instance.playButtonSound();
         if(PlayerManager.Instance.Self.Grade >= 20)
         {
            SocketManager.Instance.out.enterSuperWinner();
         }
         else
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.superWinner.cannotEnter"));
         }
      }
      
      public function hideSuperWinnerIcon() : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.SUPERWINNER,false);
      }
      
      public function awardsLoadCompleted(param1:SuperWinnerAnalyze) : void
      {
         this._awardsVector = param1.awards;
      }
      
      public function get awardsVector() : Vector.<Object>
      {
         return this._awardsVector;
      }
   }
}

class PrivateClass
{
    
   
   function PrivateClass()
   {
      super();
   }
}
