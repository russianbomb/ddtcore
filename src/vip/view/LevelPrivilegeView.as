package vip.view
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.ScrollPanel;
   import com.pickgliss.ui.controls.container.VBox;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.image.Image;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.LanguageMgr;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerConfigManager;
   import ddt.utils.PositionUtils;
   import flash.display.DisplayObject;
   import flash.display.Sprite;
   import flash.geom.Point;
   import flash.utils.Dictionary;
   
   public class LevelPrivilegeView extends Sprite implements Disposeable
   {
      
      private static const MAX_LEVEL:int = 12;
      
      private static const VIP_SETTINGS_FIELD:Array = ["VIPRwardInfo","VIPRateForGP","VIPStrengthenEx","VIPQuestStar","VIPBruiedStar","VIPLotteryCountMaxPerDay","VIPExtraBindMoneyUpper","VIPTakeCardDisCount","VIPLotteryNoTime","VIPBossBattle","CanBuyFert","FarmAssistant","PetFifthSkill","LoginSysNotice","VIPMetalRelieve","VIPWeekly","VIPBenediction"];
       
      
      private var _bg:Image;
      
      private var _titleBg:Image;
      
      private var _titleSeperators:Image;
      
      private var _titleTxt:FilterFrameText;
      
      private var _titleIcons:Vector.<Image>;
      
      private var _itemScrollPanel:ScrollPanel;
      
      private var _itemContainer:VBox;
      
      private var _seperator:Image;
      
      private var _currentVip:Image;
      
      private var _units:Dictionary;
      
      private var _minPrivilegeLevel:Dictionary;
      
      public function LevelPrivilegeView()
      {
         this._minPrivilegeLevel = new Dictionary();
         super();
         this._units = new Dictionary();
         this._units[2] = "%";
         this._units[5] = this._units[6] = LanguageMgr.GetTranslation("ddt.vip.PrivilegeViewItem.TimesUnit");
         this._units[7] = LanguageMgr.GetTranslation("ddt.vip.PrivilegeViewItem.DiscountUnit");
         this.initView();
         this.initItem();
      }
      
      private function initItem() : void
      {
         var item:PrivilegeViewItem = null;
         var item2:PrivilegeViewItem = null;
         var j:int = 0;
         for(; j <= 7; j++)
         {
            item = null;
            if(j == 0)
            {
               this.parseVipIconItem();
            }
            else
            {
               if(j == 3 || j == 4)
               {
                  item = new PrivilegeViewItem(PrivilegeViewItem.GRAPHICS_TYPE,"asset.vip.star");
               }
               else
               {
                  if(j == 4)
                  {
                     continue;
                  }
                  if(this._units[j] != null)
                  {
                     item = new PrivilegeViewItem(PrivilegeViewItem.UNIT_TYPE,this._units[j]);
                  }
                  else
                  {
                     item = new PrivilegeViewItem();
                  }
               }
               item.itemTitleText = LanguageMgr.GetTranslation("ddt.vip.PrivilegeViewItem." + VIP_SETTINGS_FIELD[j]);
               if(j == 1)
               {
                  item.contentInterceptor = function(param1:String):String
                  {
                     return Number(param1).toFixed(1);
                  };
               }
               else if(j == 7)
               {
                  item.crossFilter = "100";
                  item.contentInterceptor = function(param1:String):String
                  {
                     return (Number(param1) / 10).toString();
                  };
               }
               if(j == 4)
               {
                  item.itemContent = Vector.<String>(ServerConfigManager.instance.analyzeData("VIPQuestStar"));
               }
               else if(j != 0)
               {
                  item.itemContent = Vector.<String>(ServerConfigManager.instance.analyzeData(VIP_SETTINGS_FIELD[j]));
               }
               this._itemContainer.addChild(item);
            }
         }
         this.parsePrivilegeItem(13,8);
         this.parsePrivilegeItem(11,9);
         this.parsePrivilegeItem(8,10);
         this.parsePrivilegeItem(10,11);
         this.parsePrivilegeItem(9,12);
         this.parsePrivilegeItem(12,13);
         var data:Vector.<String> = Vector.<String>(["1","1","1","1","1","1","1","1","1","1","1","1"]);
         j = 14;
         while(j <= 15)
         {
            item2 = new PrivilegeViewItem(PrivilegeViewItem.TRUE_FLASE_TYPE);
            item2.itemTitleText = LanguageMgr.GetTranslation("ddt.vip.PrivilegeViewItem." + VIP_SETTINGS_FIELD[j]);
            item2.itemContent = data;
            this._itemContainer.addChild(item2);
            j++;
         }
         this.parseBenediction();
         this.parseRingStation(20);
         this._itemScrollPanel.invalidateViewport();
      }
      
      private function parseVipIconItem() : void
      {
         var _loc1_:Array = GiveYourselfOpenView.getVipinfo();
         var _loc2_:PrivilegeViewItem = new PrivilegeViewItem(PrivilegeViewItem.ICON_TYPE);
         _loc2_.itemTitleText = LanguageMgr.GetTranslation("ddt.vip.PrivilegeViewItem.VIPRwardInfo");
         _loc2_.itemContentForIcontype = _loc1_;
         this._itemContainer.addChild(_loc2_);
      }
      
      private function parsePrivilegeItem(param1:int, param2:int) : void
      {
         var _loc3_:Array = new Array();
         var _loc4_:int = ServerConfigManager.instance.getPrivilegeMinLevel(param1.toString());
         var _loc5_:int = 1;
         while(_loc5_ <= MAX_LEVEL)
         {
            _loc3_.push(_loc5_ >= _loc4_?"1":"0");
            _loc5_++;
         }
         var _loc6_:PrivilegeViewItem = new PrivilegeViewItem(PrivilegeViewItem.TRUE_FLASE_TYPE);
         _loc6_.itemTitleText = LanguageMgr.GetTranslation("ddt.vip.PrivilegeViewItem." + VIP_SETTINGS_FIELD[param2]);
         _loc6_.itemContent = Vector.<String>(_loc3_);
         this._itemContainer.addChild(_loc6_);
      }
      
      private function parseBenediction() : void
      {
         var _loc1_:int = 0;
         var _loc2_:int = 16;
         var _loc3_:Vector.<String> = Vector.<String>(["0","0","0","0","0","0","0","0","0","0","0","0"]);
         var _loc4_:int = 1;
         while(_loc4_ <= 7)
         {
            _loc3_[ServerConfigManager.instance.getPrivilegeMinLevel(_loc4_.toString()) - 1] = _loc4_.toString();
            _loc4_++;
         }
         _loc1_ = ServerConfigManager.instance.getPrivilegeMinLevel("7");
         var _loc5_:int = _loc1_;
         while(_loc5_ < MAX_LEVEL)
         {
            _loc3_[_loc5_] = "All";
            _loc5_++;
         }
         var _loc6_:PrivilegeViewItem = new PrivilegeViewItem();
         _loc6_.itemTitleText = LanguageMgr.GetTranslation("ddt.vip.PrivilegeViewItem." + VIP_SETTINGS_FIELD[_loc2_]);
         _loc6_.analyzeFunction = this.benedictionAnalyzer;
         _loc6_.itemContent = _loc3_;
         this._itemContainer.addChild(_loc6_);
      }
      
      private function benedictionAnalyzer(param1:Vector.<String>) : Vector.<DisplayObject>
      {
         var _loc2_:Vector.<DisplayObject> = null;
         var _loc3_:Point = null;
         var _loc4_:String = null;
         var _loc5_:FilterFrameText = null;
         var _loc6_:DisplayObject = null;
         _loc2_ = new Vector.<DisplayObject>();
         _loc3_ = ComponentFactory.Instance.creatCustomObject("vip.levelPrivilegeBenedctionItemTxtStartPos");
         for each(_loc4_ in param1)
         {
            if(_loc4_ != "0")
            {
               _loc5_ = ComponentFactory.Instance.creatComponentByStylename("vip.PrivilegeViewBenedctionItemTxt");
               _loc5_.text = LanguageMgr.GetTranslation("ddt.vip.PrivilegeViewItem.MayneRand" + _loc4_);
               PositionUtils.setPos(_loc5_,_loc3_);
               _loc5_.x = _loc5_.x + 6;
               _loc5_.y = _loc5_.y - 5;
               _loc3_.x = _loc3_.x + (40 + 5);
               _loc2_.push(_loc5_);
               this.autoText(_loc5_);
            }
            else
            {
               _loc6_ = ComponentFactory.Instance.creatComponentByStylename("vip.PrivilegeViewItem.cross");
               PositionUtils.setPos(_loc6_,_loc3_);
               _loc6_.x = _loc3_.x + (40 - _loc6_.width);
               _loc3_.x = _loc3_.x + (40 + 5);
               _loc2_.push(_loc6_);
            }
         }
         return _loc2_;
      }
      
      private function autoText(param1:FilterFrameText) : void
      {
      }
      
      private function parseRingStation(param1:int) : void
      {
         var _loc2_:Array = new Array();
         var _loc3_:int = ServerConfigManager.instance.getPrivilegeMinLevel(param1.toString());
         var _loc4_:int = 1;
         while(_loc4_ <= MAX_LEVEL)
         {
            _loc2_.push(_loc4_ >= _loc3_?"1":"0");
            _loc4_++;
         }
         var _loc5_:PrivilegeViewItem = new PrivilegeViewItem(PrivilegeViewItem.TRUE_FLASE_TYPE);
         _loc5_.itemTitleText = LanguageMgr.GetTranslation("ddt.vip.PrivilegeViewItem.VIPRingStation");
         _loc5_.itemContent = Vector.<String>(_loc2_);
         this._itemContainer.addChild(_loc5_);
      }
      
      private function initView() : void
      {
         var _loc4_:Image = null;
         this._bg = ComponentFactory.Instance.creatComponentByStylename("vip.LevelPrivilegeViewBg");
         this._titleBg = ComponentFactory.Instance.creatComponentByStylename("vip.LevelPrivilegeTitleBg");
         this._titleSeperators = ComponentFactory.Instance.creatComponentByStylename("vip.PrivilegeViewTitleItemSeperators");
         this._titleTxt = ComponentFactory.Instance.creatComponentByStylename("vip.LevelPrivilegeView.TitleTxt");
         this._titleTxt.text = LanguageMgr.GetTranslation("ddt.vip.LevelPrivilegeView.TitleTxt");
         this._seperator = ComponentFactory.Instance.creatComponentByStylename("vip.LevelPrivilegeSeperator");
         this._currentVip = ComponentFactory.Instance.creatComponentByStylename("vip.LevelPrivilegeView.currentVip");
         this._currentVip.x = this._currentVip.x + (PlayerManager.Instance.Self.VIPLevel - 1) * 45;
         this._currentVip.visible = PlayerManager.Instance.Self.IsVIP;
         addChild(this._bg);
         addChild(this._titleBg);
         addChild(this._titleSeperators);
         addChild(this._titleTxt);
         addChild(this._seperator);
         this._titleIcons = new Vector.<Image>();
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:int = 1;
         while(_loc3_ <= MAX_LEVEL)
         {
            _loc4_ = ComponentFactory.Instance.creatComponentByStylename("vip.LevelPrivilegeView.VipIcon" + _loc3_);
            this._titleIcons.push(_loc4_);
            addChild(_loc4_);
            if(_loc3_ == 1)
            {
               _loc1_ = _loc4_.x;
               _loc2_ = _loc4_.y;
            }
            else
            {
               _loc1_ = _loc1_ + 45;
               _loc4_.x = _loc1_;
               _loc4_.y = _loc2_;
            }
            _loc3_++;
         }
         addChild(this._currentVip);
         this._itemScrollPanel = ComponentFactory.Instance.creatComponentByStylename("vip.LevelPrivilegeView.ItemScrollPanel");
         addChild(this._itemScrollPanel);
         this._itemContainer = ComponentFactory.Instance.creatComponentByStylename("vip.PrivilegeViewItemContainer");
         this._itemScrollPanel.setView(this._itemContainer);
      }
      
      public function dispose() : void
      {
         var _loc1_:Image = null;
         for each(_loc1_ in this._titleIcons)
         {
            ObjectUtils.disposeObject(_loc1_);
         }
         this._titleIcons = null;
         ObjectUtils.disposeObject(this._bg);
         ObjectUtils.disposeObject(this._titleBg);
         ObjectUtils.disposeObject(this._titleSeperators);
         ObjectUtils.disposeObject(this._titleTxt);
         ObjectUtils.disposeObject(this._itemContainer);
         ObjectUtils.disposeObject(this._itemScrollPanel);
         ObjectUtils.disposeObject(this._seperator);
         ObjectUtils.disposeObject(this._currentVip);
         this._bg = null;
         this._titleBg = null;
         this._titleSeperators = null;
         this._titleTxt = null;
         this._itemScrollPanel = null;
         this._itemContainer = null;
         this._seperator = null;
         this._currentVip = null;
      }
   }
}
