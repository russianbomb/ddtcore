package par
{
   import com.pickgliss.loader.BaseLoader;
   import com.pickgliss.loader.LoaderEvent;
   import com.pickgliss.loader.LoaderManager;
   import com.pickgliss.loader.ModuleLoader;
   import com.pickgliss.utils.ObjectUtils;
   import flash.system.ApplicationDomain;
   import flash.utils.describeType;
   import par.emitters.Emitter;
   import par.emitters.EmitterInfo;
   import par.lifeeasing.AbstractLifeEasing;
   import par.particals.ParticleInfo;
   import road7th.math.ColorLine;
   import road7th.math.XLine;
   
   public class ParticleManager
   {
      
      public static var list:Array = new Array();
      
      private static var _ready:Boolean;
      
      public static const PARTICAL_XML_PATH:String = "partical.xml";
      
      public static const SHAPE_PATH:String = "shape.swf";
      
      public static const PARTICAL_LITE:String = "particallite.xml";
      
      public static const SHAPE_LITE:String = "shapelite.swf";
      
      static var Domain:ApplicationDomain;
       
      
      public function ParticleManager()
      {
         super();
      }
      
      public static function get ready() : Boolean
      {
         return _ready;
      }
      
      public static function addEmitterInfo(info:EmitterInfo) : void
      {
         list.push(info);
      }
      
      public static function removeEmitterInfo(info:EmitterInfo) : void
      {
         for(var i:int = 0; i < list.length; i++)
         {
            if(list[i] == info)
            {
               list.splice(i,1);
               return;
            }
         }
      }
      
      public static function creatEmitter(id:Number) : Emitter
      {
         var info:EmitterInfo = null;
         var emitter:Emitter = null;
         for each(info in list)
         {
            if(info.id == id)
            {
               emitter = new Emitter();
               emitter.info = info;
               return emitter;
            }
         }
         return null;
      }
      
      public static function clear() : void
      {
         list = new Array();
         _ready = false;
      }
      
      private static function load(xml:XML) : void
      {
         var x:XML = null;
         var ei:EmitterInfo = null;
         var xml_pars:XMLList = null;
         var p:XML = null;
         var po:ParticleInfo = null;
         var easing:XMLList = null;
         var lifeEasing:AbstractLifeEasing = null;
         var e:XML = null;
         var xml_emitter:XMLList = xml..emitter;
         var pcInfo:XML = describeType(new ParticleInfo());
         var ecInfo:XML = describeType(new EmitterInfo());
         for each(x in xml_emitter)
         {
            ei = new EmitterInfo();
            ObjectUtils.copyPorpertiesByXML(ei,x);
            xml_pars = x.particle;
            for each(p in xml_pars)
            {
               po = new ParticleInfo();
               ObjectUtils.copyPorpertiesByXML(po,p);
               easing = p.easing;
               lifeEasing = new AbstractLifeEasing();
               for each(e in easing)
               {
                  if(e.@name != "colorLine")
                  {
                     lifeEasing[e.@name].line = XLine.parse(e.@value);
                  }
                  else
                  {
                     lifeEasing.colorLine = new ColorLine();
                     lifeEasing.colorLine.line = XLine.parse(e.@value);
                  }
               }
               po.lifeEasing = lifeEasing;
               ei.particales.push(po);
            }
            list.push(ei);
         }
         _ready = true;
      }
      
      public static function initPartical(FLASHSITE:String, mode:String = null) : void
      {
         var particalPath:String = null;
         var shapePath:String = null;
         var particalXMLLoader:BaseLoader = null;
         var shapeLoader:ModuleLoader = null;
         if(!_ready && FLASHSITE != null)
         {
            Domain = new ApplicationDomain();
            particalPath = FLASHSITE + (mode == "lite"?PARTICAL_LITE:PARTICAL_XML_PATH);
            shapePath = FLASHSITE + (mode == "lite"?SHAPE_LITE:SHAPE_PATH);
            particalXMLLoader = LoaderManager.Instance.creatLoader(particalPath,BaseLoader.TEXT_LOADER);
            particalXMLLoader.addEventListener(LoaderEvent.COMPLETE,__loadComplete);
            LoaderManager.Instance.startLoad(particalXMLLoader);
            shapeLoader = LoaderManager.Instance.creatLoader(shapePath,BaseLoader.MODULE_LOADER,null,"GET",Domain);
            shapeLoader.addEventListener(LoaderEvent.COMPLETE,__onShapeLoadComplete);
            LoaderManager.Instance.startLoad(shapeLoader);
         }
      }
      
      private static function __onShapeLoadComplete(event:LoaderEvent) : void
      {
         event.loader.removeEventListener(LoaderEvent.COMPLETE,__onShapeLoadComplete);
         ShapeManager.setup();
      }
      
      private static function __loadComplete(event:LoaderEvent) : void
      {
         event.loader.removeEventListener(LoaderEvent.COMPLETE,__loadComplete);
         try
         {
            load(new XML(event.loader.content));
         }
         catch(err:Error)
         {
         }
      }
      
      private static function save() : XML
      {
         var ei:EmitterInfo = null;
         var exml:XML = null;
         var pi:ParticleInfo = null;
         var xpi:XML = null;
         var doc:XML = <list></list>;
         for each(ei in list)
         {
            exml = ObjectUtils.encode("emitter",ei);
            for each(pi in ei.particales)
            {
               xpi = ObjectUtils.encode("particle",pi);
               xpi.appendChild(encodeXLine("vLine",pi.lifeEasing.vLine));
               xpi.appendChild(encodeXLine("rvLine",pi.lifeEasing.rvLine));
               xpi.appendChild(encodeXLine("spLine",pi.lifeEasing.spLine));
               xpi.appendChild(encodeXLine("sizeLine",pi.lifeEasing.sizeLine));
               xpi.appendChild(encodeXLine("weightLine",pi.lifeEasing.weightLine));
               xpi.appendChild(encodeXLine("alphaLine",pi.lifeEasing.alphaLine));
               if(pi.lifeEasing.colorLine)
               {
                  xpi.appendChild(encodeXLine("colorLine",pi.lifeEasing.colorLine));
               }
               exml.appendChild(xpi);
            }
            doc.appendChild(exml);
         }
         return doc;
      }
      
      private static function encodeXLine(name:String, value:XLine) : XML
      {
         return new XML("<easing name=\"" + name + "\" value=\"" + XLine.ToString(value.line) + "\" />");
      }
   }
}
