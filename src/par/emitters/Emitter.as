package par.emitters
{
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import par.enginees.ParticleEnginee;
   import par.particals.Particle;
   import par.particals.ParticleInfo;
   import road7th.math.randRange;
   
   [Event(name="complete",type="flash.events.Event")]
   public class Emitter extends EventDispatcher
   {
       
      
      public var x:Number = 0;
      
      public var y:Number = 0;
      
      private var _info:EmitterInfo;
      
      private var _enginee:ParticleEnginee;
      
      private var _interval:Number = 0;
      
      private var _age:Number = 0;
      
      public var angle:Number = 0;
      
      public var autoRestart:Boolean = false;
      
      public function Emitter()
      {
         super();
         this._interval = 0;
      }
      
      public function setEnginee(enginee:ParticleEnginee) : void
      {
         this._enginee = enginee;
      }
      
      public function restart() : void
      {
         this._age = 0;
      }
      
      public function get info() : EmitterInfo
      {
         return this._info;
      }
      
      public function set info(value:EmitterInfo) : void
      {
         this._info = value;
         this._interval = this._info.interval;
      }
      
      public function execute(time:Number) : void
      {
         if(this._enginee && this.info)
         {
            this._age = this._age + time;
            if(this.info.life <= 0 || this._age < this.info.life)
            {
               this._interval = this._interval + time;
               if(this._interval > this.info.interval)
               {
                  this._interval = 0;
                  this.emit();
               }
            }
            else if(this.autoRestart)
            {
               this.restart();
            }
            else
            {
               this.dispose();
            }
         }
      }
      
      public function dispose() : void
      {
         this._enginee.removeEmitter(this);
         dispatchEvent(new Event(Event.COMPLETE));
      }
      
      protected function emit() : void
      {
         var pi:ParticleInfo = null;
         var count:int = 0;
         var i:int = 0;
         var p:Particle = null;
         for each(pi in this.info.particales)
         {
            if(pi.beginTime < this._age && pi.endTime > this._age)
            {
               count = pi.countOrient + int(randRange(0,pi.countSize));
               for(i = 0; i < count; i++)
               {
                  p = this._enginee.createParticle(pi);
                  p.life = pi.lifeOrient + randRange(0,pi.lifeSize);
                  p.size = pi.sizeOrient + randRange(0,pi.sizeSize);
                  p.v = pi.vOrient + randRange(0,pi.vSize);
                  p.angle = this.angle + randRange(this.info.beginAngle,this.info.endAngle);
                  p.motionV = pi.motionVOrient + randRange(0,pi.motionVOrient);
                  p.weight = pi.weightOrient + randRange(0,pi.weightSize);
                  p.spin = pi.spinOrient + randRange(0,pi.spinSize);
                  p.rotation = pi.rotation + this.angle;
                  p.x = this.x;
                  p.y = this.y;
                  p.color = pi.colorOrient;
                  p.alpha = pi.alphaOrient;
                  this._enginee.addParticle(p);
               }
               continue;
            }
         }
      }
   }
}
