package par.lifeeasing
{
   import road7th.math.ColorLine;
   import road7th.math.XLine;
   
   public class AbstractLifeEasing
   {
       
      
      public var vLine:XLine;
      
      public var rvLine:XLine;
      
      public var spLine:XLine;
      
      public var sizeLine:XLine;
      
      public var weightLine:XLine;
      
      public var alphaLine:XLine;
      
      public var colorLine:ColorLine;
      
      public function AbstractLifeEasing()
      {
         this.vLine = new XLine();
         this.rvLine = new XLine();
         this.spLine = new XLine();
         this.sizeLine = new XLine();
         this.weightLine = new XLine();
         this.alphaLine = new XLine();
         super();
      }
      
      public function easingVelocity(orient:Number, energy:Number) : Number
      {
         return orient * this.vLine.interpolate(energy);
      }
      
      public function easingRandomVelocity(orient:Number, energy:Number) : Number
      {
         return orient * this.rvLine.interpolate(energy);
      }
      
      public function easingSize(orient:Number, energy:Number) : Number
      {
         return orient * this.sizeLine.interpolate(energy);
      }
      
      public function easingSpinVelocity(orient:Number, energy:Number) : Number
      {
         return orient * this.spLine.interpolate(energy);
      }
      
      public function easingWeight(orient:Number, energy:Number) : Number
      {
         return orient * this.weightLine.interpolate(energy);
      }
      
      public function easingColor(orient:uint, energy:Number) : uint
      {
         if(this.colorLine)
         {
            return this.colorLine.interpolate(energy);
         }
         return orient;
      }
      
      public function easingApha(orient:Number, energy:Number) : Number
      {
         return orient * this.alphaLine.interpolate(energy);
      }
   }
}
