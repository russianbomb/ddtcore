package par.particals
{
   import flash.display.DisplayObject;
   import flash.geom.ColorTransform;
   import flash.geom.Matrix;
   import flash.geom.Point;
   import par.ShapeManager;
   import par.lifeeasing.AbstractLifeEasing;
   import road7th.math.randRange;
   
   public class Particle
   {
       
      
      public var x:Number;
      
      public var y:Number;
      
      public var alpha:Number;
      
      public var color:Number;
      
      public var scale:Number;
      
      public var rotation:Number;
      
      public var life:Number;
      
      public var age:Number;
      
      public var size:Number;
      
      public var v:Number;
      
      public var angle:Number;
      
      public var gv:Number;
      
      public var motionV:Number;
      
      public var weight:Number;
      
      public var spin:Number;
      
      public var image:DisplayObject;
      
      public var info:ParticleInfo;
      
      public function Particle(info:ParticleInfo)
      {
         super();
         this.image = ShapeManager.create(info.displayCreator);
         this.info = info;
         this.initialize();
      }
      
      public function initialize() : void
      {
         this.x = 0;
         this.y = 0;
         this.color = 0;
         this.scale = 1;
         this.rotation = 0;
         this.age = 0;
         this.life = 1;
         this.alpha = 1;
         this.v = 0;
         this.angle = 0;
         this.gv = 0;
         if(this.image)
         {
            this.image.blendMode = this.info.blendMode;
         }
      }
      
      public function update(time:Number) : void
      {
         var ol:Number = this.age / this.life;
         var easing:AbstractLifeEasing = this.info.lifeEasing;
         this.v = easing.easingVelocity(this.v,ol);
         this.motionV = easing.easingRandomVelocity(this.motionV,ol);
         this.weight = easing.easingWeight(this.weight,ol);
         this.gv = this.gv + this.weight;
         var pv:Point = Point.polar(this.v,this.angle);
         var rv:Point = Point.polar(this.motionV,randRange(0,2 * Math.PI));
         this.x = this.x + (pv.x + rv.x) * time;
         this.y = this.y + (pv.y + rv.y + this.gv) * time;
         this.scale = easing.easingSize(this.size,ol);
         this.rotation = this.rotation + easing.easingSpinVelocity(this.spin,ol) * time;
         this.color = easing.easingColor(this.color,ol);
         this.alpha = easing.easingApha(1,ol);
      }
      
      public function get matrixTransform() : Matrix
      {
         var cos:Number = this.scale * Math.cos(this.rotation);
         var sin:Number = this.scale * Math.sin(this.rotation);
         return new Matrix(cos,sin,-sin,cos,this.x,this.y);
      }
      
      public function get colorTransform() : ColorTransform
      {
         if(this.info.keepColor)
         {
            return new ColorTransform(1,1,1,this.alpha,this.color >> 16 & 255,this.color >> 8 & 255,this.color & 255,0);
         }
         return new ColorTransform(0,0,0,this.alpha,this.color >> 16 & 255,this.color >> 8 & 255,this.color & 255,0);
      }
   }
}
