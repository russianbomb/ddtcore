package par.enginees
{
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.utils.Dictionary;
   import par.emitters.Emitter;
   import par.particals.Particle;
   import par.particals.ParticleInfo;
   import par.renderer.IParticleRenderer;
   
   public class ParticleEnginee
   {
       
      
      private var _maxCount:int;
      
      private var _root:Sprite;
      
      private var _last:int;
      
      private var _emitters:Dictionary;
      
      public var spareParticles:Dictionary;
      
      public var particles:Vector.<Particle>;
      
      private var _render:IParticleRenderer;
      
      public var cachable:Boolean = true;
      
      public function ParticleEnginee(render:IParticleRenderer)
      {
         super();
         this._render = render;
         this._maxCount = 200;
         this.spareParticles = new Dictionary();
         this.particles = new Vector.<Particle>();
         this._emitters = new Dictionary();
      }
      
      public function setMaxCount(value:Number) : void
      {
         this._maxCount = value;
      }
      
      public function addEmitter(emitter:Emitter) : void
      {
         this._emitters[emitter] = emitter;
         emitter.setEnginee(this);
      }
      
      public function removeEmitter(emitter:Emitter) : void
      {
         delete this._emitters[emitter];
         emitter.setEnginee(null);
      }
      
      public function addParticle(particle:Particle) : void
      {
         this.particles.push(particle);
         this._render.addParticle(particle);
      }
      
      private function __enterFrame(event:Event) : void
      {
         this.update();
      }
      
      public function update() : void
      {
         var p:Particle = null;
         var emitter:Emitter = null;
         var i:int = 0;
         while(this.particles.length > this._maxCount)
         {
            p = this.particles.shift();
            this._render.removeParticle(p);
            this.cacheParticle(p);
         }
         var time:Number = 0.04;
         for each(emitter in this._emitters)
         {
            emitter.execute(time);
         }
         for(i = 0; i < this.particles.length; i++)
         {
            p = this.particles[i];
            p.age = p.age + time;
            if(p.age >= p.life)
            {
               this.particles.splice(i,1);
               this._render.removeParticle(p);
               this.cacheParticle(p);
               i--;
            }
            else
            {
               p.update(time);
            }
         }
         this._render.renderParticles(this.particles);
      }
      
      protected function cacheParticle(particle:Particle) : void
      {
         particle.initialize();
         var cls:uint = particle.info.displayCreator;
         var cached:Array = this.spareParticles[cls];
         if(cached == null)
         {
            this.spareParticles[cls] = cached = new Array();
         }
         if(cached.length < 15)
         {
            cached.push(particle);
         }
      }
      
      public function reset() : void
      {
         this.particles = new Vector.<Particle>();
         this.spareParticles = new Dictionary();
         this._emitters = new Dictionary();
         this._render.reset();
      }
      
      public function createParticle(info:ParticleInfo) : Particle
      {
         if(this.spareParticles[info.displayCreator] && this.spareParticles[info.displayCreator].length > 0)
         {
            return this.spareParticles[info.displayCreator].shift();
         }
         return new Particle(info);
      }
      
      public function dispose() : void
      {
         this._render.dispose();
      }
   }
}
