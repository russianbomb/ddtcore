package equipretrieve
{
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import com.pickgliss.ui.vo.AlertInfo;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.UIModuleTypes;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.view.UIModuleSmallLoading;
   import equipretrieve.view.RetrieveBagFrame;
   import equipretrieve.view.RetrieveBagView;
   import equipretrieve.view.RetrieveBagcell;
   import equipretrieve.view.RetrieveBgView;
   import flash.utils.Dictionary;
   
   public class RetrieveFrame extends Frame
   {
       
      
      private var _retrieveBgView:RetrieveBgView;
      
      private var _retrieveBagView:RetrieveBagFrame;
      
      private var _alertInfo:AlertInfo;
      
      private var _BG:ScaleBitmapImage;
      
      public function RetrieveFrame()
      {
         super();
         this.mouseEnabled = false;
         UIModuleSmallLoading.Instance.progress = 0;
         UIModuleSmallLoading.Instance.show();
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadCompleteHandler);
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUimoduleLoadProgress);
         UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.BAGANDINFO);
      }
      
      private function onUimoduleLoadProgress(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.BAGANDINFO)
         {
            UIModuleSmallLoading.Instance.progress = param1.loader.progress * 100;
         }
      }
      
      private function loadCompleteHandler(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.BAGANDINFO)
         {
            UIModuleSmallLoading.Instance.hide();
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadCompleteHandler);
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUimoduleLoadProgress);
            this.start();
         }
      }
      
      private function start() : void
      {
         this._retrieveBgView = ComponentFactory.Instance.creatCustomObject("retrieve.retrieveBgView");
         this._retrieveBagView = ComponentFactory.Instance.creatCustomObject("retrieve.retrieveBagView");
         addToContent(this._retrieveBagView);
         addToContent(this._retrieveBgView);
      }
      
      public function show() : void
      {
         LayerManager.Instance.addToLayer(this,LayerManager.GAME_TOP_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
      }
      
      public function updateBag(param1:Dictionary) : void
      {
         if(this._retrieveBgView)
         {
            this._retrieveBgView.refreshData(param1);
         }
      }
      
      public function cellDoubleClick(param1:RetrieveBagcell) : void
      {
         if(this._retrieveBgView)
         {
            this._retrieveBgView.cellDoubleClick(param1);
         }
      }
      
      public function set bagType(param1:int) : void
      {
         RetrieveBagView(this._retrieveBagView.bagView).resultPoint(param1,this._retrieveBagView.x - this._retrieveBgView.x,this._retrieveBagView.y - this._retrieveBgView.y);
      }
      
      public function set shine(param1:Boolean) : void
      {
         if(param1 == true)
         {
            if(this._retrieveBgView)
            {
               this._retrieveBgView.startShine();
            }
         }
         else if(this._retrieveBgView)
         {
            this._retrieveBgView.stopShine();
         }
      }
      
      override public function set visible(param1:Boolean) : void
      {
         super.visible = param1;
         if(!param1)
         {
            SocketManager.Instance.out.sendClearStoreBag();
         }
      }
      
      public function clearItemCell() : void
      {
         if(this._retrieveBgView)
         {
            this._retrieveBgView.clearCellInfo();
         }
      }
      
      private function _response(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         if(!RetrieveController.Instance.viewMouseEvtBoolean)
         {
            return;
         }
         if(param1.responseCode == FrameEvent.CLOSE_CLICK || param1.responseCode == FrameEvent.ESC_CLICK || RetrieveController.Instance.viewMouseEvtBoolean)
         {
            RetrieveController.Instance.close();
         }
      }
      
      override public function dispose() : void
      {
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadCompleteHandler);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUimoduleLoadProgress);
         RetrieveController.Instance.close();
         if(this._retrieveBagView)
         {
            this._retrieveBagView.bagView.setBagType(0);
         }
         disposeChildren = true;
         if(this._retrieveBgView)
         {
            ObjectUtils.disposeObject(this._retrieveBgView);
         }
         this._retrieveBgView = null;
         if(this._retrieveBagView)
         {
            ObjectUtils.disposeObject(this._retrieveBagView);
         }
         this._retrieveBagView = null;
         if(parent)
         {
            parent.removeChild(this);
         }
         super.dispose();
      }
   }
}
