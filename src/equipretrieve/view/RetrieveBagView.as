package equipretrieve.view
{
   import bagAndInfo.bag.BagView;
   import bagAndInfo.cell.BagCell;
   import baglocked.BaglockedManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.ScrollPanel;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.events.CellEvent;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.ItemManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import equipretrieve.RetrieveController;
   import equipretrieve.RetrieveModel;
   import flash.display.Shape;
   import flash.events.Event;
   
   public class RetrieveBagView extends BagView
   {
       
      
      protected var _equipPanel:ScrollPanel;
      
      protected var _propPanel:ScrollPanel;
      
      public function RetrieveBagView()
      {
         super();
         isNeedCard(false);
      }
      
      override protected function set_breakBtn_enable() : void
      {
         if(_breakBtn)
         {
            _breakBtn.enable = false;
            _breakBtn.filters = ComponentFactory.Instance.creatFilters("grayFilter");
         }
         if(_sellBtn)
         {
            _sellBtn.enable = false;
            _sellBtn.filters = ComponentFactory.Instance.creatFilters("grayFilter");
         }
         if(_keySortBtn)
         {
            _keySortBtn.enable = false;
            _keySortBtn.filters = ComponentFactory.Instance.creatFilters("grayFilter");
         }
         if(_cardEnbleFlase)
         {
            _cardEnbleFlase.visible = false;
            _cardEnbleFlase.filters = ComponentFactory.Instance.creatFilters("grayFilter");
         }
         if(_continueBtn)
         {
            _continueBtn.enable = false;
            _continueBtn.filters = ComponentFactory.Instance.creatFilters("grayFilter");
         }
         if(_tabBtn3)
         {
            _tabBtn3.mouseEnabled = false;
         }
         _goodsNumInfoText.visible = false;
         _goodsNumTotalText.visible = false;
      }
      
      override protected function initBackGround() : void
      {
         _bg = ComponentFactory.Instance.creatComponentByStylename("bagBGAsset4");
         _itemtabBtn = ComponentFactory.Instance.creat("bagView.itemTabButton");
         _itemtabBtn.setFrame(1);
         _bg1 = ComponentFactory.Instance.creatComponentByStylename("bagAndInfo.view.bgIII");
         _moneyBg = ComponentFactory.Instance.creatComponentByStylename("bagAndInfo.moneyViewBG");
         _moneyBg1 = ComponentFactory.Instance.creatComponentByStylename("bagAndInfo.moneyViewBGI");
         _moneyBg2 = ComponentFactory.Instance.creatComponentByStylename("bagAndInfo.moneyViewBGII");
         _moneyBg3 = ComponentFactory.Instance.creatComponentByStylename("bagAndInfo.moneyViewBGIII");
         _bgShape = new Shape();
         _bgShape.graphics.beginFill(15262671,1);
         _bgShape.graphics.drawRoundRect(0,0,327,328,2,2);
         _bgShape.graphics.endFill();
         _bgShape.x = 11;
         _bgShape.y = 50;
      }
      
      override protected function initBagList() : void
      {
         _equiplist = new RetrieveBagEquipListView(0);
         _proplist = new RetrieveBagListView(1);
         _currentList = _equiplist;
         this._equipPanel = ComponentFactory.Instance.creat("ddtstore.StoreBagView.BagEquipScrollPanel");
         this._equipPanel.x = 16;
         this._equipPanel.y = 92;
         addChild(this._equipPanel);
         this._equipPanel.hScrollProxy = ScrollPanel.OFF;
         this._equipPanel.vScrollProxy = ScrollPanel.ON;
         this._equipPanel.setView(_equiplist);
         this._equipPanel.invalidateViewport();
         this._propPanel = ComponentFactory.Instance.creat("ddtstore.StoreBagView.BagEquipScrollPanel");
         this._propPanel.x = 16;
         this._propPanel.y = 266;
         addChild(this._propPanel);
         this._propPanel.hScrollProxy = ScrollPanel.OFF;
         this._propPanel.vScrollProxy = ScrollPanel.ON;
         this._propPanel.setView(_proplist);
         this._propPanel.invalidateViewport();
      }
      
      override protected function initTabButtons() : void
      {
         super.initTabButtons();
         _tabBtn1.visible = false;
         _tabBtn2.visible = false;
         _tabBtn3.visible = false;
         _buttonContainer.visible = false;
      }
      
      override protected function initEvent() : void
      {
         _equiplist.addEventListener(CellEvent.ITEM_CLICK,this.__cellClick);
         _equiplist.addEventListener(CellEvent.DOUBLE_CLICK,this.__cellDoubleClick);
         _equiplist.addEventListener(Event.CHANGE,__listChange);
         _equiplist.addEventListener(CellEvent.DRAGSTOP,this._stopDrag);
         _proplist.addEventListener(CellEvent.ITEM_CLICK,this.__cellClick);
         _proplist.addEventListener(CellEvent.DOUBLE_CLICK,this.__cellDoubleClick);
         _proplist.addEventListener(CellEvent.DRAGSTOP,this._stopDrag);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.USE_COLOR_SHELL,__useColorShell);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.ITEM_OPENUP,__openPreviewListFrame);
      }
      
      override public function setBagType(param1:int) : void
      {
         super.setBagType(param1);
         _buttonContainer.visible = false;
         _itemtabBtn.setFrame(param1 + 1);
         _sellBtn.enable = _breakBtn.enable = _continueBtn.enable = false;
         _sellBtn.filters = _breakBtn.filters = _continueBtn.filters = ComponentFactory.Instance.creatFilters("grayFilter");
         _equiplist.visible = true;
         _proplist.visible = true;
      }
      
      override protected function __cellDoubleClick(param1:CellEvent) : void
      {
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         param1.stopImmediatePropagation();
         var _loc2_:RetrieveBagcell = param1.data as RetrieveBagcell;
         var _loc3_:InventoryItemInfo = _loc2_.info as InventoryItemInfo;
         var _loc4_:ItemTemplateInfo = ItemManager.Instance.getTemplateById(_loc3_.TemplateID);
         var _loc5_:int = !!PlayerManager.Instance.Self.Sex?int(1):int(2);
         if(!_loc2_.locked)
         {
            RetrieveController.Instance.cellDoubleClick(_loc2_);
         }
      }
      
      override protected function __cellClick(param1:CellEvent) : void
      {
         var _loc2_:BagCell = null;
         var _loc3_:InventoryItemInfo = null;
         if(!_sellBtn.isActive)
         {
            param1.stopImmediatePropagation();
            _loc2_ = param1.data as BagCell;
            if(_loc2_)
            {
               _loc3_ = _loc2_.info as InventoryItemInfo;
            }
            if(_loc3_ == null)
            {
               return;
            }
            if(!_loc2_.locked)
            {
               SoundManager.instance.play("008");
               _loc2_.dragStart();
               RetrieveController.Instance.shine = true;
            }
         }
      }
      
      private function _stopDrag(param1:CellEvent) : void
      {
         RetrieveController.Instance.shine = false;
      }
      
      public function resultPoint(param1:int, param2:Number, param3:Number) : void
      {
         this.setBagType(param1);
         if(param1 == EQUIP)
         {
            RetrieveModel.Instance.setresultCell(RetrieveBagEquipListView(_equiplist).returnNullPoint(param2,param3));
         }
         else if(param1 == PROP)
         {
            RetrieveModel.Instance.setresultCell(RetrieveBagListView(_proplist).returnNullPoint(param2,param3));
         }
      }
      
      override public function dispose() : void
      {
         ObjectUtils.disposeObject(this._equipPanel);
         this._equipPanel = null;
         ObjectUtils.disposeObject(this._propPanel);
         this._propPanel = null;
         super.dispose();
      }
   }
}
