package kingDivision.analyze
{
   import com.pickgliss.loader.DataAnalyzer;
   import com.pickgliss.utils.ObjectUtils;
   import kingDivision.data.KingDivisionConsortionMessageInfo;
   
   public class KingDivisionConsortionMessageAnalyze extends DataAnalyzer
   {
       
      
      public var _list:Array;
      
      public function KingDivisionConsortionMessageAnalyze(param1:Function)
      {
         super(param1);
      }
      
      override public function analyze(param1:*) : void
      {
         var _loc3_:XMLList = null;
         var _loc4_:int = 0;
         var _loc5_:KingDivisionConsortionMessageInfo = null;
         var _loc2_:XML = new XML(param1);
         if(_loc2_.@value == "true")
         {
            _loc3_ = _loc2_..Info;
            this._list = new Array();
            _loc4_ = 0;
            while(_loc4_ < _loc3_.length())
            {
               _loc5_ = new KingDivisionConsortionMessageInfo();
               ObjectUtils.copyPorpertiesByXML(_loc5_,_loc3_[_loc4_]);
               this._list.push(_loc5_);
               _loc4_++;
            }
            onAnalyzeComplete();
         }
         else
         {
            message = _loc2_.@message;
            onAnalyzeError();
            onAnalyzeComplete();
         }
      }
   }
}
