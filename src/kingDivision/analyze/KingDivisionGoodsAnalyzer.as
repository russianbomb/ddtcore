package kingDivision.analyze
{
   import com.pickgliss.loader.DataAnalyzer;
   import com.pickgliss.utils.ObjectUtils;
   import kingDivision.data.KingDivisionGoodsInfo;
   
   public class KingDivisionGoodsAnalyzer extends DataAnalyzer
   {
       
      
      public var dataList:Array;
      
      public function KingDivisionGoodsAnalyzer(param1:Function)
      {
         super(param1);
      }
      
      override public function analyze(param1:*) : void
      {
         var _loc3_:XMLList = null;
         var _loc4_:int = 0;
         var _loc5_:KingDivisionGoodsInfo = null;
         var _loc6_:Array = null;
         var _loc2_:XML = new XML(param1);
         this.dataList = [];
         if(_loc2_.@value == "true")
         {
            _loc3_ = _loc2_..Item;
            _loc4_ = 0;
            while(_loc4_ < _loc3_.length())
            {
               if(_loc3_[_loc4_].@ActivityType == "30")
               {
                  _loc5_ = new KingDivisionGoodsInfo();
                  ObjectUtils.copyPorpertiesByXML(_loc5_,_loc3_[_loc4_]);
                  _loc6_ = this.dataList[_loc5_.Quality - 1];
                  if(!_loc6_)
                  {
                     _loc6_ = [];
                  }
                  _loc6_.push(_loc5_);
                  this.dataList[_loc5_.Quality - 1] = _loc6_;
               }
               _loc4_++;
            }
            onAnalyzeComplete();
         }
         else
         {
            message = _loc2_.@message;
            onAnalyzeError();
         }
      }
   }
}
