package kingDivision.analyze
{
   import com.pickgliss.loader.DataAnalyzer;
   import com.pickgliss.utils.ObjectUtils;
   import flash.utils.Dictionary;
   import kingDivision.data.KingDivisionAreaNameMessageInfo;
   
   public class AreaNameMessageAnalyze extends DataAnalyzer
   {
       
      
      public var _listDic:Dictionary;
      
      public function AreaNameMessageAnalyze(param1:Function)
      {
         super(param1);
      }
      
      override public function analyze(param1:*) : void
      {
         var _loc3_:XMLList = null;
         var _loc4_:Array = null;
         var _loc5_:int = 0;
         var _loc6_:KingDivisionAreaNameMessageInfo = null;
         var _loc7_:KingDivisionAreaNameMessageInfo = null;
         var _loc2_:XML = new XML(param1);
         if(_loc2_.@value == "true")
         {
            _loc3_ = _loc2_..Info;
            this._listDic = new Dictionary();
            _loc4_ = new Array();
            _loc5_ = 0;
            while(_loc5_ < _loc3_.length())
            {
               _loc7_ = new KingDivisionAreaNameMessageInfo();
               ObjectUtils.copyPorpertiesByXML(_loc7_,_loc3_[_loc5_]);
               _loc4_.push(_loc7_);
               _loc5_++;
            }
            for each(_loc6_ in _loc4_)
            {
               if(!this._listDic[_loc6_.AreaID])
               {
                  this._listDic[_loc6_.AreaID] = new Array();
               }
               this._listDic[_loc6_.AreaID].push(_loc6_.AreaName);
            }
            onAnalyzeComplete();
         }
         else
         {
            message = _loc2_.@message;
            onAnalyzeError();
            onAnalyzeComplete();
         }
      }
      
      public function get kingDivisionAreaNameDataDic() : Dictionary
      {
         return this._listDic;
      }
   }
}
