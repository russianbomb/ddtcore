package kingDivision
{
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.loader.BaseLoader;
   import com.pickgliss.loader.LoadResourceManager;
   import com.pickgliss.loader.LoaderEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.core.Component;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.GameInSocketOut;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PathManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerConfigManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.manager.StateManager;
   import ddt.manager.TimeManager;
   import ddt.states.StateType;
   import flash.display.Bitmap;
   import flash.events.EventDispatcher;
   import flash.utils.Dictionary;
   import hallIcon.HallIconManager;
   import hallIcon.HallIconType;
   import kingDivision.analyze.AreaNameMessageAnalyze;
   import kingDivision.analyze.KingDivisionConsortionMessageAnalyze;
   import kingDivision.data.KingDivisionConsortionItemInfo;
   import kingDivision.event.KingDivisionEvent;
   import kingDivision.loader.LoaderKingDivisionUIModule;
   import kingDivision.model.KingDivisionModel;
   import kingDivision.view.KingDivisionFrame;
   import road7th.comm.PackageIn;
   
   public class KingDivisionManager extends EventDispatcher
   {
      
      private static var _instance:KingDivisionManager;
       
      
      private var _model:KingDivisionModel;
      
      public var _kingDivFrame:KingDivisionFrame;
      
      public var openFrame:Boolean;
      
      private var analyzerArr:Array;
      
      public var isThisZoneWin:Boolean;
      
      public var dataDic:Dictionary;
      
      public function KingDivisionManager(param1:PrivateClass)
      {
         super();
         this.addEvent();
      }
      
      public static function get Instance() : KingDivisionManager
      {
         if(KingDivisionManager._instance == null)
         {
            KingDivisionManager._instance = new KingDivisionManager(new PrivateClass());
         }
         return KingDivisionManager._instance;
      }
      
      public function setup() : void
      {
         this._model = new KingDivisionModel();
         SocketManager.Instance.addEventListener(KingDivisionEvent.ACTIVITY_OPEN,this.__activityOpen);
      }
      
      private function addEvent() : void
      {
         SocketManager.Instance.addEventListener(KingDivisionEvent.CONSORTIA_MATCH_INFO,this.__consortiaMatchInfo);
         SocketManager.Instance.addEventListener(KingDivisionEvent.CONSORTIA_MATCH_SCORE,this.__consortiaMatchScore);
         SocketManager.Instance.addEventListener(KingDivisionEvent.CONSORTIA_MATCH_RANK,this.__consortiaMatchRank);
         SocketManager.Instance.addEventListener(KingDivisionEvent.CONSORTIA_MATCH_AREA_RANK,this.__consortiaMatchAreaRank);
         SocketManager.Instance.addEventListener(KingDivisionEvent.CONSORTIA_MATCH_AREA_RANK_INFO,this.__consortiaMatchAreaRankInfo);
      }
      
      private function __activityOpen(param1:KingDivisionEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         this._model.isOpen = _loc2_.readBoolean();
         this._model.states = _loc2_.readByte();
         this._model.level = _loc2_.readInt();
         this._model.dateArr = ServerConfigManager.instance.localConsortiaMatchDays;
         this._model.allDateArr = ServerConfigManager.instance.areaConsortiaMatchDays;
         this._model.consortiaMatchStartTime = ServerConfigManager.instance.consortiaMatchStartTime;
         this._model.consortiaMatchEndTime = ServerConfigManager.instance.consortiaMatchEndTime;
         this.updateConsotionMessage();
         this.kingDivisionIcon(this._model.isOpen);
      }
      
      public function updateConsotionMessage() : void
      {
         var _loc1_:Date = null;
         var _loc2_:Date = null;
         if(this._model.states == 1)
         {
            _loc1_ = TimeManager.Instance.Now();
            if(this._model.dateArr[0] == _loc1_.date)
            {
               GameInSocketOut.sendGetConsortionMessageThisZone();
            }
            else
            {
               GameInSocketOut.sendGetEliminateConsortionMessageThisZone();
            }
         }
         if(this._model.states == 2)
         {
            this.loaderConsortionMessage();
            this.loaderAreaNameMessage();
            _loc2_ = TimeManager.Instance.Now();
            if(this._model.allDateArr[0] == _loc2_.date)
            {
               GameInSocketOut.sendGetConsortionMessageAllZone();
            }
            else
            {
               GameInSocketOut.sendGetEliminateConsortionMessageAllZone();
            }
         }
      }
      
      public function loaderConsortionMessage() : void
      {
         var _loc1_:BaseLoader = LoadResourceManager.Instance.createLoader(PathManager.solveRequestPath("ConsortiaMatchHistory.ashx"),BaseLoader.REQUEST_LOADER);
         _loc1_.analyzer = new KingDivisionConsortionMessageAnalyze(this.__searchResult);
         LoadResourceManager.Instance.startLoad(_loc1_);
      }
      
      public function loaderAreaNameMessage() : void
      {
         var _loc1_:BaseLoader = LoadResourceManager.Instance.createLoader(PathManager.getAreaNameInfoPath(),BaseLoader.TEXT_LOADER);
         _loc1_.loadErrorMessage = LanguageMgr.GetTranslation("tank.auctionHouse.controller.KingDivisionAreaNameError");
         _loc1_.analyzer = new AreaNameMessageAnalyze(this.loadAreaNameDataComplete);
         LoadResourceManager.Instance.startLoad(_loc1_);
      }
      
      private function loadAreaNameDataComplete(param1:AreaNameMessageAnalyze) : void
      {
         this.dataDic = param1.kingDivisionAreaNameDataDic;
      }
      
      private function __onLoadError(param1:LoaderEvent) : void
      {
         var _loc2_:String = param1.loader.loadErrorMessage;
         if(param1.loader.analyzer)
         {
            _loc2_ = param1.loader.loadErrorMessage + "\n" + param1.loader.analyzer.message;
         }
         var _loc3_:BaseAlerFrame = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("alert"),param1.loader.loadErrorMessage,LanguageMgr.GetTranslation("tank.view.bagII.baglocked.sure"));
         _loc3_.addEventListener(FrameEvent.RESPONSE,this.__onAlertResponse);
      }
      
      private function __onAlertResponse(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         param1.currentTarget.removeEventListener(FrameEvent.RESPONSE,this.__onAlertResponse);
         ObjectUtils.disposeObject(param1.currentTarget);
      }
      
      private function __searchResult(param1:KingDivisionConsortionMessageAnalyze) : void
      {
         var _loc3_:KingDivisionConsortionItemInfo = null;
         this.isThisZoneWin = true;
         this.analyzerArr = param1._list;
         if(this._model.eliminateInfo != null)
         {
            ObjectUtils.disposeObject(this._model.eliminateInfo);
            this._model.eliminateInfo = null;
         }
         this._model.eliminateInfo = new Vector.<KingDivisionConsortionItemInfo>();
         if(this.analyzerArr == null || this.analyzerArr.length < 1)
         {
            return;
         }
         var _loc2_:int = 0;
         while(_loc2_ < this.analyzerArr.length)
         {
            _loc3_ = new KingDivisionConsortionItemInfo();
            _loc3_.conID = this.analyzerArr[_loc2_].ConsortiaID;
            _loc3_.conName = this.analyzerArr[_loc2_].ConsortiaName;
            _loc3_.score = this.analyzerArr[_loc2_].Score;
            _loc3_.conState = this.analyzerArr[_loc2_].State;
            _loc3_.conStyle = this.analyzerArr[_loc2_].Style;
            _loc3_.conSex = this.analyzerArr[_loc2_].Sex;
            this._model.eliminateInfo.push(_loc3_);
            _loc2_++;
         }
      }
      
      private function __consortiaMatchInfo(param1:KingDivisionEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         this._model.gameNum = _loc2_.readInt();
         this._model.points = _loc2_.readInt();
         if(this._kingDivFrame != null)
         {
            if(this._kingDivFrame.qualificationsFrame != null)
            {
               this._kingDivFrame.qualificationsFrame.cancelMatch();
               this._kingDivFrame.qualificationsFrame.updateMessage(this._model.points,this._model.gameNum);
            }
            else if(this._kingDivFrame.rankingRoundView != null)
            {
               this._kingDivFrame.rankingRoundView.cancelMatch();
               this._kingDivFrame.rankingRoundView.updateMessage(this._model.points,this._model.gameNum);
            }
         }
      }
      
      private function __consortiaMatchScore(param1:KingDivisionEvent) : void
      {
         var _loc6_:KingDivisionConsortionItemInfo = null;
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readByte();
         var _loc4_:int = _loc2_.readByte();
         this._model.conLen = _loc2_.readInt();
         if(this._model.conItemInfo != null)
         {
            ObjectUtils.disposeObject(this._model.conItemInfo);
            this._model.eliminateInfo = null;
         }
         this._model.conItemInfo = new Vector.<KingDivisionConsortionItemInfo>();
         if(this._model.conLen < 1)
         {
            return;
         }
         var _loc5_:int = 0;
         while(_loc5_ < this._model.conLen)
         {
            _loc6_ = new KingDivisionConsortionItemInfo();
            _loc6_.rang = _loc2_.readInt();
            _loc6_.consortionName = _loc2_.readUTF();
            _loc6_.consortionLevel = _loc2_.readInt();
            _loc6_.num = _loc2_.readInt();
            _loc6_.points = _loc2_.readInt();
            this._model.conItemInfo.push(_loc6_);
            _loc5_++;
         }
         if(this._kingDivFrame != null && this._kingDivFrame.qualificationsFrame != null)
         {
            this._kingDivFrame.qualificationsFrame.updateConsortiaMessage();
         }
      }
      
      private function __consortiaMatchRank(param1:KingDivisionEvent) : void
      {
         var _loc7_:KingDivisionConsortionItemInfo = null;
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readByte();
         var _loc4_:int = _loc2_.readByte();
         if(this._model.eliminateInfo != null)
         {
            ObjectUtils.disposeObject(this._model.eliminateInfo);
            this._model.eliminateInfo = null;
         }
         this._model.eliminateInfo = new Vector.<KingDivisionConsortionItemInfo>();
         var _loc5_:int = _loc2_.readInt();
         if(_loc5_ < 1)
         {
            return;
         }
         var _loc6_:int = 0;
         while(_loc6_ < _loc5_)
         {
            _loc7_ = new KingDivisionConsortionItemInfo();
            _loc7_.conID = _loc2_.readInt();
            _loc7_.conName = _loc2_.readUTF();
            _loc7_.name = _loc2_.readUTF();
            _loc7_.score = _loc2_.readInt();
            _loc7_.conState = _loc2_.readByte();
            _loc7_.isGame = _loc2_.readBoolean();
            this._model.eliminateInfo.push(_loc7_);
            _loc6_++;
         }
         if(this._kingDivFrame != null && this._kingDivFrame.rankingRoundView != null)
         {
            this._kingDivFrame.rankingRoundView.zone = 0;
         }
      }
      
      private function __consortiaMatchAreaRank(param1:KingDivisionEvent) : void
      {
         var _loc6_:KingDivisionConsortionItemInfo = null;
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readByte();
         var _loc4_:int = _loc2_.readByte();
         this._model.conLen = _loc2_.readInt();
         if(this._model.conItemInfo != null)
         {
            ObjectUtils.disposeObject(this._model.conItemInfo);
            this._model.conItemInfo = null;
         }
         this._model.conItemInfo = new Vector.<KingDivisionConsortionItemInfo>();
         if(this._model.conLen < 1)
         {
            return;
         }
         var _loc5_:int = 0;
         while(_loc5_ < this._model.conLen)
         {
            _loc6_ = new KingDivisionConsortionItemInfo();
            _loc6_.consortionIDArea = _loc2_.readInt();
            _loc6_.areaID = _loc2_.readInt();
            _loc6_.consortionLevel = _loc2_.readInt();
            _loc6_.num = _loc2_.readInt();
            _loc6_.consortionName = _loc2_.readUTF();
            _loc6_.points = _loc2_.readInt();
            this._model.conItemInfo.push(_loc6_);
            _loc5_++;
         }
         if(this._kingDivFrame != null && this._kingDivFrame.qualificationsFrame != null)
         {
            this._kingDivFrame.qualificationsFrame.updateConsortiaMessage();
         }
      }
      
      private function __consortiaMatchAreaRankInfo(param1:KingDivisionEvent) : void
      {
         var _loc7_:KingDivisionConsortionItemInfo = null;
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readByte();
         var _loc4_:int = _loc2_.readByte();
         if(this._model.eliminateAllZoneInfo != null)
         {
            ObjectUtils.disposeObject(this._model.eliminateAllZoneInfo);
            this._model.eliminateAllZoneInfo = null;
         }
         this._model.eliminateAllZoneInfo = new Vector.<KingDivisionConsortionItemInfo>();
         var _loc5_:int = _loc2_.readInt();
         if(_loc5_ < 1)
         {
            return;
         }
         var _loc6_:int = 0;
         while(_loc6_ < _loc5_)
         {
            _loc7_ = new KingDivisionConsortionItemInfo();
            _loc7_.consortionIDArea = _loc2_.readInt();
            _loc7_.areaID = _loc2_.readInt();
            _loc7_.consortionStyle = _loc2_.readUTF();
            _loc7_.consortionSex = _loc2_.readBoolean();
            _loc7_.consortionNameArea = _loc2_.readUTF();
            _loc7_.consortionState = _loc2_.readByte();
            _loc7_.consortionScoreArea = _loc2_.readInt();
            _loc7_.consortionIsGame = _loc2_.readBoolean();
            this._model.eliminateAllZoneInfo.push(_loc7_);
            _loc6_++;
         }
         if(this._kingDivFrame != null && this._kingDivFrame.rankingRoundView != null)
         {
            this._kingDivFrame.rankingRoundView.zone = 1;
         }
      }
      
      public function templateDataSetup(param1:Array) : void
      {
         this._model.goods = param1;
      }
      
      public function get model() : KingDivisionModel
      {
         return this._model;
      }
      
      public function kingDivisionIcon(param1:Boolean) : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.KINGDIVISION,param1);
      }
      
      public function onClickIcon() : void
      {
         if(StateManager.currentStateType == StateType.MAIN && this._model.isOpen)
         {
            LoaderKingDivisionUIModule.Instance.loadUIModule(this.doOpenKingDivisionFrame);
         }
      }
      
      private function doOpenKingDivisionFrame() : void
      {
         this._kingDivFrame = ComponentFactory.Instance.creatComponentByStylename("kingdivision.frame");
         LayerManager.Instance.addToLayer(this._kingDivFrame,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
      }
      
      public function returnComponent(param1:Bitmap, param2:String) : Component
      {
         var _loc3_:Component = new Component();
         _loc3_.tipData = param2;
         _loc3_.tipDirctions = "0,1,2";
         _loc3_.tipStyle = "ddt.view.tips.OneLineTip";
         _loc3_.tipGapH = 20;
         _loc3_.width = param1.width;
         _loc3_.x = param1.x;
         _loc3_.y = param1.y;
         param1.x = 0;
         param1.y = 0;
         _loc3_.addChild(param1);
         return _loc3_;
      }
      
      public function checkCanStartGame() : Boolean
      {
         var _loc1_:Boolean = true;
         if(PlayerManager.Instance.Self.Bag.getItemAt(6) == null)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("tank.room.RoomIIController.weapon"));
            _loc1_ = false;
         }
         return _loc1_;
      }
      
      public function checkGameTimeIsOpen() : Boolean
      {
         var _loc1_:Date = TimeManager.Instance.Now();
         var _loc2_:int = _loc1_.hours;
         var _loc3_:int = _loc1_.minutes;
         if(_loc2_ > int(this._model.consortiaMatchEndTime[0]) || _loc2_ < int(this._model.consortiaMatchStartTime[0]))
         {
            return false;
         }
         if(_loc2_ <= int(this._model.consortiaMatchEndTime[0]) && _loc2_ >= int(this._model.consortiaMatchStartTime[0]))
         {
            if(_loc2_ == int(this._model.consortiaMatchEndTime[0]))
            {
               if(_loc3_ >= int(this._model.consortiaMatchEndTime[1]))
               {
                  return false;
               }
               return true;
            }
            if(_loc2_ == int(this._model.consortiaMatchStartTime[0]))
            {
               if(_loc3_ <= int(this._model.consortiaMatchStartTime[1]))
               {
                  return false;
               }
               return true;
            }
            if(_loc2_ < int(this._model.consortiaMatchEndTime[0]) && _loc2_ > int(this._model.consortiaMatchStartTime[0]))
            {
               return true;
            }
         }
         return false;
      }
      
      public function get isOpen() : Boolean
      {
         return this._model == null?Boolean(false):Boolean(this._model.isOpen);
      }
      
      public function set zoneIndex(param1:int) : void
      {
         this._model.zoneIndex = param1;
      }
      
      public function get zoneIndex() : int
      {
         return this._model.zoneIndex;
      }
      
      public function get dateArr() : Array
      {
         return this._model.dateArr;
      }
      
      public function set dateArr(param1:Array) : void
      {
         this._model.dateArr = param1;
      }
      
      public function get allDateArr() : Array
      {
         return this._model.allDateArr;
      }
      
      public function set allDateArr(param1:Array) : void
      {
         this._model.allDateArr = param1;
      }
      
      public function get thisZoneNickName() : String
      {
         return this._model.thisZoneNickName;
      }
      
      public function set thisZoneNickName(param1:String) : void
      {
         this._model.thisZoneNickName = param1;
      }
      
      public function get allZoneNickName() : String
      {
         return this._model.allZoneNickName;
      }
      
      public function set allZoneNickName(param1:String) : void
      {
         this._model.allZoneNickName = param1;
      }
      
      public function get points() : int
      {
         return this._model.points;
      }
      
      public function set points(param1:int) : void
      {
         this._model.points = param1;
      }
      
      public function get gameNum() : int
      {
         return this._model.gameNum;
      }
      
      public function set gameNum(param1:int) : void
      {
         this._model.gameNum = param1;
      }
      
      public function get states() : int
      {
         return this._model.states;
      }
      
      public function set states(param1:int) : void
      {
         this._model.states = param1;
      }
      
      public function get level() : int
      {
         return this._model.level;
      }
      
      public function set level(param1:int) : void
      {
         this._model.level = param1;
      }
   }
}

class PrivateClass
{
    
   
   function PrivateClass()
   {
      super();
   }
}
