package kingDivision.view
{
   import com.pickgliss.ui.controls.container.HBox;
   import kingDivision.KingDivisionManager;
   
   public class RewardGoodsList extends HBox
   {
       
      
      private var _currentItem:RewardGoodsListItem;
      
      private var items:Vector.<RewardGoodsListItem>;
      
      private var _zone:int;
      
      private var _goodsArr:Array;
      
      public function RewardGoodsList()
      {
         super();
      }
      
      override protected function init() : void
      {
         super.init();
         spacing = 3;
      }
      
      public function setGoodsListItem(param1:int) : void
      {
         var _loc2_:int = 0;
         this._goodsArr = KingDivisionManager.Instance.model.getLevelGoodsItems(param1);
         this.items = new Vector.<RewardGoodsListItem>(this._goodsArr.length);
         _loc2_ = 0;
         while(_loc2_ < this._goodsArr.length)
         {
            this.items[_loc2_] = new RewardGoodsListItem();
            this.items[_loc2_].buttonMode = true;
            this.items[_loc2_].goodsInfo(this._goodsArr[_loc2_].TemplateID,this._goodsArr[_loc2_].AttackCompose,this._goodsArr[_loc2_].DefendCompose,this._goodsArr[_loc2_].AgilityCompose,this._goodsArr[_loc2_].LuckCompose,this._goodsArr[_loc2_].Count,this._goodsArr[_loc2_].IsBind,this._goodsArr[_loc2_].ValidDate);
            addChild(this.items[_loc2_]);
            _loc2_++;
         }
      }
   }
}
