package kingDivision.view
{
   import com.pickgliss.ui.controls.container.VBox;
   import kingDivision.KingDivisionManager;
   
   public class KingDivisionConsortionList extends VBox
   {
       
      
      private var _currentItem:KingDivisionConsortionListItem;
      
      private var items:Vector.<KingDivisionConsortionListItem>;
      
      private var length:int;
      
      public function KingDivisionConsortionList()
      {
         super();
         _spacing = 3;
      }
      
      override protected function init() : void
      {
         var _loc1_:int = 0;
         super.init();
         spacing = 2;
         this.length = KingDivisionManager.Instance.model.conLen;
         this.items = new Vector.<KingDivisionConsortionListItem>(this.length);
         _loc1_ = 1;
         while(_loc1_ <= this.length)
         {
            this.items[_loc1_ - 1] = new KingDivisionConsortionListItem(_loc1_);
            this.items[_loc1_ - 1].buttonMode = true;
            this.items[_loc1_ - 1].info = KingDivisionManager.Instance.model.conItemInfo[_loc1_ - 1];
            addChild(this.items[_loc1_ - 1]);
            _loc1_++;
         }
      }
   }
}
