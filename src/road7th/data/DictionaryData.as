package road7th.data
{
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.events.IEventDispatcher;
   import flash.utils.Dictionary;
   
   [Event(name="clear",type="road7th.data.DictionaryEvent")]
   [Event(name="remove",type="road7th.data.DictionaryEvent")]
   [Event(name="update",type="road7th.data.DictionaryEvent")]
   [Event(name="add",type="road7th.data.DictionaryEvent")]
   public dynamic class DictionaryData extends Dictionary implements IEventDispatcher
   {
       
      
      private var _dispatcher:EventDispatcher;
      
      private var _array:Array;
      
      private var _fName:String;
      
      private var _value:Object;
      
      public function DictionaryData(weakKeys:Boolean = false)
      {
         super(weakKeys);
         this._dispatcher = new EventDispatcher(this);
         this._array = [];
      }
      
      public function get length() : int
      {
         return this._array.length;
      }
      
      public function get list() : Array
      {
         return this._array;
      }
      
      public function filter(field:String, value:Object) : Array
      {
         this._fName = field;
         this._value = value;
         return this._array.filter(this.filterCallBack);
      }
      
      private function filterCallBack(item:*, index:int, array:Array) : Boolean
      {
         return item[this._fName] == this._value;
      }
      
      public function add(key:*, value:Object) : void
      {
         var old:Object = null;
         var t:int = 0;
         if(this[key] == null)
         {
            this[key] = value;
            this._array.push(value);
            this.dispatchEvent(new DictionaryEvent(DictionaryEvent.ADD,value));
         }
         else
         {
            old = this[key];
            this[key] = value;
            t = this._array.indexOf(old);
            if(t > -1)
            {
               this._array.splice(t,1);
            }
            this._array.push(value);
            this.dispatchEvent(new DictionaryEvent(DictionaryEvent.UPDATE,value));
         }
      }
      
      public function hasKey(key:*) : Boolean
      {
         return this[key] != null;
      }
      
      public function remove(key:*) : void
      {
         var t:int = 0;
         var obj:Object = this[key];
         if(obj != null)
         {
            this[key] = null;
            delete this[key];
            t = this._array.indexOf(obj);
            if(t > -1)
            {
               this._array.splice(t,1);
            }
            this.dispatchEvent(new DictionaryEvent(DictionaryEvent.REMOVE,obj));
         }
      }
      
      public function setData(dic:DictionaryData) : void
      {
         var i:* = null;
         this.cleardata();
         for(i in dic)
         {
            this[i] = dic[i];
            this._array.push(dic[i]);
         }
      }
      
      public function clear() : void
      {
         this.cleardata();
         this.dispatchEvent(new DictionaryEvent(DictionaryEvent.CLEAR));
      }
      
      public function slice(startIndex:int = 0, endIndex:int = 166777215) : Array
      {
         return this._array.slice(startIndex,endIndex);
      }
      
      public function concat(arr:Array) : Array
      {
         return this._array.concat(arr);
      }
      
      private function cleardata() : void
      {
         var i:* = null;
         var s:String = null;
         var temp:Array = [];
         for(i in this)
         {
            temp.push(i);
         }
         for each(s in temp)
         {
            this[s] = null;
            delete this[s];
         }
         this._array = [];
      }
      
      public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false) : void
      {
         this._dispatcher.addEventListener(type,listener,useCapture,priority,useWeakReference);
      }
      
      public function removeEventListener(type:String, listener:Function, useCapture:Boolean = false) : void
      {
         this._dispatcher.removeEventListener(type,listener,useCapture);
      }
      
      public function dispatchEvent(event:Event) : Boolean
      {
         return this._dispatcher.dispatchEvent(event);
      }
      
      public function hasEventListener(type:String) : Boolean
      {
         return this._dispatcher.hasEventListener(type);
      }
      
      public function willTrigger(type:String) : Boolean
      {
         return this._dispatcher.willTrigger(type);
      }
   }
}
