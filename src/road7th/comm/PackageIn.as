package road7th.comm
{
   import flash.utils.ByteArray;
   
   public class PackageIn extends ByteArray
   {
      
      public static const HEADER_SIZE:Number = 20;
       
      
      private var _len:int;
      
      private var _checksum:int;
      
      private var _clientId:int;
      
      private var _code:int;
      
      private var _extend1:int;
      
      private var _extend2:int;
      
      public function PackageIn()
      {
         super();
      }
      
      public function load(src:ByteArray, len:int) : void
      {
         writeByte(src.readByte());
         this.readHeader();
      }
      
      public function loadE(src:ByteArray, len:int, key:ByteArray) : void
      {
         var i:int = 0;
         var origion:ByteArray = new ByteArray();
         var result:ByteArray = new ByteArray();
         for(i = 0; i < len; i++)
         {
            origion.writeByte(src.readByte());
            result.writeByte(origion[i]);
         }
         for(i = 0; i < len; i++)
         {
            if(i > 0)
            {
               key[i % 8] = key[i % 8] + origion[i - 1] ^ i;
               result[i] = origion[i] - origion[i - 1] ^ key[i % 8];
            }
            else
            {
               result[i] = origion[i] ^ key[0];
            }
         }
         result.position = 0;
         for(i = 0; i < len; i++)
         {
            writeByte(result.readByte());
         }
         position = 0;
         this.readHeader();
      }
      
      public function readHeader() : void
      {
         readShort();
         this._len = readShort();
         this._checksum = readShort();
         this._code = readShort();
         this._clientId = readInt();
         this._extend1 = readInt();
         this._extend2 = readInt();
      }
      
      public function get checkSum() : int
      {
         return this._checksum;
      }
      
      public function get code() : int
      {
         return this._code;
      }
      
      public function get clientId() : int
      {
         return this._clientId;
      }
      
      public function get extend1() : int
      {
         return this._extend1;
      }
      
      public function get extend2() : int
      {
         return this._extend2;
      }
      
      public function get Len() : int
      {
         return this._len;
      }
      
      public function calculateCheckSum() : int
      {
         var val1:int = 119;
         var i:int = 6;
         while(i < length)
         {
            val1 = val1 + this[i++];
         }
         return val1 & 32639;
      }
      
      public function readXml() : XML
      {
         return new XML(readUTF());
      }
      
      public function readDateString() : String
      {
         return readShort() + "-" + readByte() + "-" + readByte() + " " + readByte() + ":" + readByte() + ":" + readByte();
      }
      
      public function readDate() : Date
      {
         var year:int = readShort();
         var month:int = readByte() - 1;
         var day:int = readByte();
         var hours:int = readByte();
         var minutes:int = readByte();
         var seconds:int = readByte();
         var date:Date = new Date(year,month,day,hours,minutes,seconds);
         return date;
      }
      
      public function readByteArray() : ByteArray
      {
         var temp:ByteArray = new ByteArray();
         readBytes(temp,0,this._len - position);
         temp.position = 0;
         return temp;
      }
      
      public function deCompress() : void
      {
         position = HEADER_SIZE;
         var temp:ByteArray = new ByteArray();
         readBytes(temp,0,this._len - HEADER_SIZE);
         temp.uncompress();
         position = HEADER_SIZE;
         writeBytes(temp,0,temp.length);
         this._len = HEADER_SIZE + temp.length;
         position = HEADER_SIZE;
      }
      
      public function readLong() : Number
      {
         var value:Number = new Number();
         var H32:Number = new Number(readInt());
         var D32:Number = new Number(readUnsignedInt());
         var temp:int = 1;
         if(H32 < 0)
         {
            temp = -1;
         }
         value = temp * (Math.abs(H32 * Math.pow(2,32)) + D32);
         return value;
      }
   }
}
