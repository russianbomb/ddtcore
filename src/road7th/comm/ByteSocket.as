package road7th.comm
{
   import flash.events.ErrorEvent;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.events.IOErrorEvent;
   import flash.events.ProgressEvent;
   import flash.events.SecurityErrorEvent;
   import flash.net.Socket;
   import flash.utils.ByteArray;
   
   [Event(name="data",type="SocketEvent")]
   [Event(name="error",type="flash.events.ErrorEvent")]
   [Event(name="close",type="flash.events.Event")]
   [Event(name="connect",type="flash.events.Event")]
   public class ByteSocket extends EventDispatcher
   {
      
      private static var KEY:Array = [174,191,86,120,171,205,239,241];
      
      public static var RECEIVE_KEY:ByteArray;
      
      public static var SEND_KEY:ByteArray;
       
      
      private var _debug:Boolean;
      
      private var _socket:Socket;
      
      private var _ip:String;
      
      private var _port:Number;
      
      private var _send_fsm:FSM;
      
      private var _receive_fsm:FSM;
      
      private var _encrypted:Boolean;
      
      private var _readBuffer:ByteArray;
      
      private var _readOffset:int;
      
      private var _writeOffset:int;
      
      private var _headerTemp:ByteArray;
      
      private var pkgNumber:int = 0;
      
      public function ByteSocket(encrypted:Boolean = true, debug:Boolean = false)
      {
         super();
         this._readBuffer = new ByteArray();
         this._send_fsm = new FSM(2059198199,1501);
         this._receive_fsm = new FSM(2059198199,1501);
         this._headerTemp = new ByteArray();
         this._encrypted = encrypted;
         this._debug = debug;
         this.setKey(KEY);
      }
      
      public function setKey(key:Array) : void
      {
         RECEIVE_KEY = new ByteArray();
         SEND_KEY = new ByteArray();
         for(var i:int = 0; i < 8; i++)
         {
            RECEIVE_KEY.writeByte(key[i]);
            SEND_KEY.writeByte(key[i]);
         }
      }
      
      public function resetKey() : void
      {
         this.setKey(KEY);
      }
      
      public function setFsm(adder:int, muliter:int) : void
      {
         this._send_fsm.setup(adder,muliter);
         this._receive_fsm.setup(adder,muliter);
      }
      
      public function connect(ip:String, port:Number) : void
      {
         try
         {
            if(this._socket)
            {
               this.close();
            }
            this._socket = new Socket();
            this.addEvent(this._socket);
            this._ip = ip;
            this._port = port;
            this._readBuffer.position = 0;
            this._readOffset = 0;
            this._writeOffset = 0;
            this._socket.connect(ip,port);
         }
         catch(err:Error)
         {
            dispatchEvent(new ErrorEvent(ErrorEvent.ERROR,false,false,err.message));
         }
      }
      
      private function addEvent(socket:Socket) : void
      {
         socket.addEventListener(Event.CONNECT,this.handleConnect);
         socket.addEventListener(Event.CLOSE,this.handleClose);
         socket.addEventListener(ProgressEvent.SOCKET_DATA,this.handleIncoming);
         socket.addEventListener(IOErrorEvent.IO_ERROR,this.handleIoError);
         socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR,this.handleIoError);
      }
      
      private function removeEvent(socket:Socket) : void
      {
         socket.removeEventListener(Event.CONNECT,this.handleConnect);
         socket.removeEventListener(Event.CLOSE,this.handleClose);
         socket.removeEventListener(ProgressEvent.SOCKET_DATA,this.handleIncoming);
         socket.removeEventListener(IOErrorEvent.IO_ERROR,this.handleIoError);
         socket.removeEventListener(SecurityErrorEvent.SECURITY_ERROR,this.handleIoError);
      }
      
      public function get connected() : Boolean
      {
         return this._socket && this._socket.connected;
      }
      
      public function isSame(ip:String, port:int) : Boolean
      {
         return this._ip == ip && port == this._port;
      }
      
      public function send(pkg:PackageOut) : void
      {
         var i:int = 0;
         if(this._socket && this._socket.connected)
         {
            pkg.pack();
            if(!this._debug)
            {
            }
            if(this._encrypted)
            {
               for(i = 0; i < pkg.length; i++)
               {
                  if(i > 0)
                  {
                     SEND_KEY[i % 8] = SEND_KEY[i % 8] + pkg[i - 1] ^ i;
                     pkg[i] = (pkg[i] ^ SEND_KEY[i % 8]) + pkg[i - 1];
                  }
                  else
                  {
                     pkg[0] = pkg[0] ^ SEND_KEY[0];
                  }
               }
            }
            this._socket.writeBytes(pkg,0,pkg.length);
            this._socket.flush();
         }
      }
      
      public function sendString(data:String) : void
      {
         if(this._socket.connected)
         {
            this._socket.writeUTF(data);
            this._socket.flush();
         }
      }
      
      public function close() : void
      {
         this.removeEvent(this._socket);
         if(this._socket.connected)
         {
            this._socket.close();
         }
      }
      
      private function handleConnect(event:Event) : void
      {
         try
         {
            this._send_fsm.reset();
            this._receive_fsm.reset();
            this._send_fsm.setup(2059198199,1501);
            this._receive_fsm.setup(2059198199,1501);
            dispatchEvent(new Event(Event.CONNECT));
         }
         catch(e:Error)
         {
         }
      }
      
      private function handleClose(event:Event) : void
      {
         try
         {
            this.removeEvent(this._socket);
            dispatchEvent(new Event(Event.CLOSE));
         }
         catch(e:Error)
         {
         }
      }
      
      private function handleIoError(event:ErrorEvent) : void
      {
         try
         {
            dispatchEvent(new ErrorEvent(ErrorEvent.ERROR,false,false,event.text));
         }
         catch(e:Error)
         {
         }
      }
      
      private function handleIncoming(event:ProgressEvent) : void
      {
         var len:int = 0;
         if(this._socket.bytesAvailable > 0)
         {
            len = this._socket.bytesAvailable;
            this._socket.readBytes(this._readBuffer,this._writeOffset,this._socket.bytesAvailable);
            this._writeOffset = this._writeOffset + len;
            if(this._writeOffset > 1)
            {
               this._readBuffer.position = 0;
               this._readOffset = 0;
               if(this._readBuffer.bytesAvailable >= PackageIn.HEADER_SIZE)
               {
                  this.readPackage();
               }
            }
         }
      }
      
      private function readPackage() : void
      {
         var len:int = 0;
         var buff:PackageIn = null;
         var dataLeft:int = this._writeOffset - this._readOffset;
         while(true)
         {
            len = 0;
            while(this._readOffset + 4 < this._writeOffset)
            {
               this._headerTemp.position = 0;
               this._headerTemp.writeByte(this._readBuffer[this._readOffset]);
               this._headerTemp.writeByte(this._readBuffer[this._readOffset + 1]);
               this._headerTemp.writeByte(this._readBuffer[this._readOffset + 2]);
               this._headerTemp.writeByte(this._readBuffer[this._readOffset + 3]);
               if(this._encrypted)
               {
                  this._headerTemp = this.decrptBytes(this._headerTemp,4,this.copyByteArray(RECEIVE_KEY));
               }
               this._headerTemp.position = 0;
               if(this._headerTemp.readShort() == PackageOut.HEADER)
               {
                  len = this._headerTemp.readUnsignedShort();
                  break;
               }
               this._readOffset++;
            }
            dataLeft = this._writeOffset - this._readOffset;
            if(dataLeft >= len && len != 0)
            {
               this._readBuffer.position = this._readOffset;
               buff = new PackageIn();
               if(this._encrypted)
               {
                  buff.loadE(this._readBuffer,len,RECEIVE_KEY);
               }
               else
               {
                  buff.load(this._readBuffer,len);
               }
               this._readOffset = this._readOffset + len;
               dataLeft = this._writeOffset - this._readOffset;
               this.handlePackage(buff);
               if(dataLeft < PackageIn.HEADER_SIZE)
               {
                  break;
               }
               continue;
            }
            break;
         }
         this._readBuffer.position = 0;
         if(dataLeft > 0)
         {
            this._readBuffer.writeBytes(this._readBuffer,this._readOffset,dataLeft);
         }
         this._readOffset = 0;
         this._writeOffset = dataLeft;
      }
      
      private function copyByteArray(src:ByteArray) : ByteArray
      {
         var result:ByteArray = new ByteArray();
         for(var i:int = 0; i < src.length; i++)
         {
            result.writeByte(src[i]);
         }
         return result;
      }
      
      public function decrptBytes(src:ByteArray, len:int, key:ByteArray) : ByteArray
      {
         var i:int = 0;
         var result:ByteArray = new ByteArray();
         for(i = 0; i < len; i++)
         {
            result.writeByte(src[i]);
         }
         for(i = 0; i < len; i++)
         {
            if(i > 0)
            {
               key[i % 8] = key[i % 8] + src[i - 1] ^ i;
               result[i] = src[i] - src[i - 1] ^ key[i % 8];
            }
            else
            {
               result[0] = src[0] ^ key[0];
            }
         }
         return result;
      }
      
      private function tracePkg(src:ByteArray, des:String, len:int = -1) : void
      {
         var str:String = des;
         var l:int = len < 0?int(src.length):int(len);
         for(var i:int = 0; i < l; i++)
         {
            str = str + (String(src[i]) + ", ");
         }
      }
      
      private function traceArr(arr:ByteArray) : void
      {
         var str:String = "[";
         for(var i:int = 0; i < arr.length; i++)
         {
            str = str + (arr[i] + " ");
         }
         str = str + "]";
      }
      
      private function handlePackage(pkg:PackageIn) : void
      {
         if(!this._debug)
         {
         }
         try
         {
            if(pkg.checkSum == pkg.calculateCheckSum())
            {
               pkg.position = PackageIn.HEADER_SIZE;
               dispatchEvent(new SocketEvent(SocketEvent.DATA,pkg));
            }
         }
         catch(err:Error)
         {
         }
      }
      
      public function dispose() : void
      {
         if(this._socket.connected)
         {
            this._socket.close();
         }
         this._socket = null;
      }
   }
}

class FSM
{
    
   
   private var _state:int;
   
   private var _adder:int;
   
   private var _multiper:int;
   
   function FSM(adder:int, multiper:int)
   {
      super();
      this.setup(adder,multiper);
   }
   
   public function getState() : int
   {
      return this._state;
   }
   
   public function reset() : void
   {
      this._state = 0;
   }
   
   public function setup(adder:int, multiper:int) : void
   {
      this._adder = adder;
      this._multiper = multiper;
      this.updateState();
   }
   
   public function updateState() : int
   {
      this._state = (~this._state + this._adder) * this._multiper;
      this._state = this._state ^ this._state >> 16;
      return this._state;
   }
}
