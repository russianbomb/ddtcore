package road7th.math
{
   import flash.geom.Point;
   
   public class XLine
   {
       
      
      protected var list:Array;
      
      protected var fix:Boolean = true;
      
      protected var fixValue:Number = 1;
      
      public function XLine(... arg)
      {
         super();
         this.line = arg;
      }
      
      public static function ToString(value:Array) : String
      {
         var p:Point = null;
         var rlt:String = "";
         try
         {
            for each(p in value)
            {
               rlt = rlt + (p.x + ":" + p.y + ",");
            }
         }
         catch(e:Error)
         {
         }
         return rlt;
      }
      
      public static function parse(str:String) : Array
      {
         var temp:Array = null;
         var nums:Array = null;
         var i:int = 0;
         var list:Array = new Array();
         try
         {
            temp = str.match(/([-]?\d+[\.]?\d*)/g);
            nums = new Array();
            for(i = 0; i < temp.length; i = i + 2)
            {
               list.push(new Point(Number(temp[i]),Number(temp[i + 1])));
            }
         }
         catch(e:Error)
         {
         }
         return list;
      }
      
      public function set line(value:Array) : void
      {
         this.list = value;
         if(this.list == null || this.list.length == 0)
         {
            this.fix = true;
            this.fixValue = 1;
         }
         else if(this.list.length == 1)
         {
            this.fix = true;
            this.fixValue = this.list[0].y;
         }
         else if(this.list.length == 2 && this.list[0].y == this.list[1].y)
         {
            this.fix = true;
            this.fixValue = this.list[0].y;
         }
         else
         {
            this.fix = false;
         }
      }
      
      public function get line() : Array
      {
         return this.list;
      }
      
      public function interpolate(x:Number) : Number
      {
         var p1:Point = null;
         var p2:Point = null;
         var i:int = 0;
         if(!this.fix)
         {
            for(i = 1; i < this.list.length; i++)
            {
               p2 = this.list[i];
               p1 = this.list[i - 1];
               if(p2.x > x)
               {
                  break;
               }
            }
            return interpolatePointByX(p1,p2,x);
         }
         return this.fixValue;
      }
   }
}
