package road7th.utils
{
   import flash.geom.Point;
   import flash.text.TextField;
   import flash.utils.ByteArray;
   
   public class StringHelper
   {
      
      private static var blankSpaceType:Array = [9,61656,59349,59350,59351,59352,59353,59354,59355,59355,59356,59357,59358,59359,59360,59361,59362,59363,59364,59365];
      
      private static const OFFSET:Number = 2000;
      
      private static const reg:RegExp = /[^\x00-\xff]{1,}/g;
      
      public static const _leftReg:RegExp = /</g;
      
      public static const _rightReg:RegExp = />/g;
      
      public static const _reverseLeftReg:RegExp = /&lt;/g;
      
      public static const _reverseRightReg:RegExp = /&gt;/g;
      
      private static var idR1:RegExp = /^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$/;
      
      private static var idR2:RegExp = /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}(\d|x|X)$/;
       
      
      public function StringHelper()
      {
         super();
      }
      
      public static function trimLeft(targetString:String) : String
      {
         if(!targetString)
         {
            return targetString;
         }
         return targetString.replace(/^\s*/g,"");
      }
      
      public static function trim(str:String) : String
      {
         if(!str)
         {
            return str;
         }
         return str.replace(/(^\s*)|(\s*$)/g,"");
      }
      
      public static function trimRight(targetString:String) : String
      {
         return targetString.replace(/\s*$/g,"");
      }
      
      public static function trimAll(str:String) : String
      {
         var s:String = trim(str);
         var newStr:String = "";
         for(var i:uint = 0; i < s.length; i++)
         {
            if(blankSpaceType.indexOf(s.charCodeAt(i)) <= -1)
            {
               newStr = newStr + s.charAt(i);
            }
         }
         return newStr;
      }
      
      public static function replaceStr(str:String, sourceWord:String, targetWord:String) : String
      {
         return str.split(sourceWord).join(targetWord);
      }
      
      public static function isNullOrEmpty(str:String) : Boolean
      {
         return str == null || str == "";
      }
      
      public static function stringToPath(pathString:String) : Array
      {
         var x:int = 0;
         var y:int = 0;
         var path:Array = new Array();
         for(var i:int = 0; i < pathString.length; i = i + 2)
         {
            x = pathString.charCodeAt(i);
            y = pathString.charCodeAt(i + 1);
            path.push(new Point(x - OFFSET,y - OFFSET));
         }
         return path;
      }
      
      public static function stringToPoint(str:String) : Point
      {
         return new Point(str.charCodeAt(0) - OFFSET,str.charCodeAt(1) - OFFSET);
      }
      
      public static function pathToString(path:Array) : String
      {
         if(path == null || path.length <= 0)
         {
            return "";
         }
         var pathString:String = "";
         for(var i:int = 0; i < path.length; i++)
         {
            pathString = pathString + String.fromCharCode(Math.round(path[i].x + OFFSET));
            pathString = pathString + String.fromCharCode(Math.round(path[i].y + OFFSET));
         }
         return pathString;
      }
      
      public static function pointToString(point:Point) : String
      {
         var result:String = "";
         result = result + String.fromCharCode(Math.round(point.x + OFFSET));
         result = result + String.fromCharCode(Math.round(point.y + OFFSET));
         return result;
      }
      
      public static function numberToString(number:Number) : String
      {
         return String.fromCharCode(Math.round(number + OFFSET));
      }
      
      public static function stringToNumber(str:String) : Number
      {
         return str.charCodeAt(0) - OFFSET;
      }
      
      public static function getIsBiggerMaxCHchar(str:String, max:uint) : Boolean
      {
         var b:ByteArray = new ByteArray();
         b.writeUTF(trim(str));
         if(b.length > max * 3 + 2)
         {
            return true;
         }
         return false;
      }
      
      public static function getRandomNumber() : String
      {
         var n:uint = Math.round(Math.random() * 1000000);
         return n.toString();
      }
      
      public static function checkTextFieldLength(textfield:TextField, max:uint, input:String = null) : void
      {
         var ulen1:uint = Boolean(textfield.text)?uint(textfield.text.match(reg).join("").length):uint(0);
         var ulen2:uint = Boolean(input)?uint(input.match(reg).join("").length):uint(0);
         textfield.maxChars = max > ulen1 + ulen2?int(max - ulen1 - ulen2):max > ulen2?int(max - ulen2):int(max / 2);
      }
      
      public static function rePlaceHtmlTextField(s:String) : String
      {
         s = s.replace(_leftReg,"&lt;");
         s = s.replace(_rightReg,"&gt;");
         return s;
      }
      
      public static function parseTime(res:String, len:uint) : String
      {
         var str:String = res;
         var mlk:Date = new Date(Number(str.substr(0,4)),Number(str.substr(5,2)) - 1,Number(str.substr(8,2)));
         var targetDate:Date = new Date();
         targetDate.setTime(mlk.getTime() + (len + 1) * 24 * 60 * 60 * 1000);
         str = String(targetDate.getUTCFullYear()) + "-" + (targetDate.getUTCMonth() + 1) + "-" + targetDate.getUTCDate();
         return str;
      }
      
      public static function reverseHtmlTextField(s:String) : String
      {
         s = s.replace(_reverseLeftReg,"<");
         s = s.replace(_reverseRightReg,">");
         return s;
      }
      
      public static function cidCheck(id:String) : Boolean
      {
         if(idR1.test(id) || idR2.test(id))
         {
            return true;
         }
         return false;
      }
      
      public static function replaceHtmlTag(str:String) : String
      {
         str = str.replace(/<(\S*?)[^>]*>|<.*? \/>/g,"");
         return str;
      }
      
      public static function replaceToHtmlText(s:String) : String
      {
         var txt:TextField = new TextField();
         txt.text = s;
         s = replaceHtmlTag(txt.htmlText);
         return s;
      }
      
      public static function getConvertedHtmlArray(str:String) : Array
      {
         return str.match(/&[a-z]*?;/g);
      }
      
      public static function format(str:String, ... args) : String
      {
         if(args == null || args.length <= 0)
         {
            return str;
         }
         for(var i:uint = 0; i < args.length; i++)
         {
            str = replaceStr(str,"{" + i.toString() + "}",args[i]);
         }
         return str;
      }
      
      public static function getConvertedLst(fromCharCode:String) : Array
      {
         return fromCharCode.match(/&[a-z]*?;/g);
      }
      
      public static function trimHtmlText(value:String) : String
      {
         var result:String = null;
         value = trim(value);
         var startIdx:int = 0;
         var endIdx:int = 0;
         var reg:RegExp = /<[\S+]>/g;
         var obj:Object = reg.exec(value);
         var fragments:Vector.<String> = new Vector.<String>();
         while(obj)
         {
            endIdx = obj.index;
            fragments.push(value.substring(startIdx,endIdx));
            fragments.push(obj[0]);
            startIdx = reg.lastIndex;
            obj = reg.exec(value);
         }
         fragments.push(value.substring(startIdx));
         fragments[2] = trimLeft(fragments[2]);
         fragments[fragments.length - 2] = trimRight(fragments[fragments.length - 2]);
         return fragments.join("");
      }
   }
}
