package escort.data
{
   public class EscortPlayerInfo
   {
       
      
      public var zoneId:int;
      
      public var id:int;
      
      public var index:int;
      
      public var carType:int;
      
      public var name:String;
      
      public var isSelf:Boolean;
      
      public var acceleEndTime:Date;
      
      public var deceleEndTime:Date;
      
      public var invisiEndTime:Date;
      
      public var posX:int = 0;
      
      public var fightState:int;
      
      public function EscortPlayerInfo()
      {
         this.acceleEndTime = new Date(2000);
         this.deceleEndTime = new Date(2000);
         this.invisiEndTime = new Date(2000);
         super();
      }
   }
}
