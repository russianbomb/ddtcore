package escort
{
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.BaseLoader;
   import com.pickgliss.loader.LoadResourceManager;
   import com.pickgliss.loader.LoaderEvent;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import ddt.data.ServerConfigInfo;
   import ddt.data.UIModuleTypes;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.ChatManager;
   import ddt.manager.LanguageMgr;
   import ddt.manager.PathManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerConfigManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.manager.TimeManager;
   import ddt.view.UIModuleSmallLoading;
   import escort.data.EscortCarInfo;
   import escort.data.EscortInfoVo;
   import escort.data.EscortPlayerInfo;
   import escort.event.EscortEvent;
   import escort.view.EscortFrame;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.events.IEventDispatcher;
   import flash.events.TimerEvent;
   import flash.utils.Timer;
   import road7th.comm.PackageIn;
   import road7th.data.DictionaryData;
   
   public class EscortManager extends EventDispatcher
   {
      
      public static const ICON_RES_LOAD_COMPLETE:String = "escortIconResLoadComplete";
      
      public static const CAR_STATUS_CHANGE:String = "escortCarStatusChange";
      
      public static const START_GAME:String = "escortStartGame";
      
      public static const ENTER_GAME:String = "escortEnterGame";
      
      public static const ALL_READY:String = "escortAllReady";
      
      public static const MOVE:String = "escortMove";
      
      public static const REFRESH_ITEM:String = "escortAppearItem";
      
      public static const REFRESH_BUFF:String = "escortRefreshBuff";
      
      public static const USE_SKILL:String = "escortUseSkill";
      
      public static const RANK_LIST:String = "escortRankList";
      
      public static const RANK_ARRIVE_LIST:String = "";
      
      public static const ARRIVE:String = "escortArrive";
      
      public static const DESTROY:String = "escortDestroy";
      
      public static const RE_ENTER_ALL_INFO:String = "escortReEnterAllInfo";
      
      public static const CAN_ENTER:String = "escortCanEnter";
      
      public static const FIGHT_STATE_CHANGE:String = "escortFightStateChange";
      
      public static const LEAP_PROMPT_SHOW_HIDE:String = "escortLeapPromptShowHide";
      
      public static const END:String = "escortEnd";
      
      public static const REFRESH_ENTER_COUNT:String = "escortRefreshEnterCount";
      
      public static const REFRESH_ITEM_FREE_COUNT:String = "escortRefreshItemCount";
      
      public static const CANCEL_GAME:String = "escortCancelGame";
      
      private static var _instance:EscortManager;
       
      
      public var dataInfo:EscortInfoVo;
      
      public var isShowDungeonTip:Boolean = true;
      
      private var _isStart:Boolean;
      
      private var _isLoadIconComplete:Boolean;
      
      private var _isInGame:Boolean;
      
      private var _carStatus:int;
      
      private var _freeCount:int;
      
      private var _usableCount:int;
      
      private var _rankAddInfo:Array;
      
      private var _playerList:Vector.<EscortPlayerInfo>;
      
      private var _accelerateRate:int;
      
      private var _decelerateRate:int;
      
      private var _buyRecordStatus:Array;
      
      private var _startGameNeedMoney:int;
      
      private var _doubleTimeArray:Array;
      
      private var _timer:Timer;
      
      private var _itemFreeCountList:Array;
      
      private var _sprintAwardInfo:Array;
      
      private var _endTime:int = -1;
      
      private var _hasPrompted:DictionaryData;
      
      private var _isPromptDoubleTime:Boolean = false;
      
      private var _loadResCount:int;
      
      public function EscortManager(param1:IEventDispatcher = null)
      {
         this._doubleTimeArray = [];
         this._itemFreeCountList = [0,0,0];
         super(param1);
      }
      
      public static function get instance() : EscortManager
      {
         if(_instance == null)
         {
            _instance = new EscortManager();
         }
         return _instance;
      }
      
      public function get sprintAwardInfo() : Array
      {
         return this._sprintAwardInfo;
      }
      
      public function get itemFreeCountList() : Array
      {
         return this._itemFreeCountList;
      }
      
      public function get isInDoubleTime() : Boolean
      {
         var _loc1_:Date = TimeManager.Instance.Now();
         var _loc2_:Number = _loc1_.hours;
         var _loc3_:Number = _loc1_.minutes;
         var _loc4_:int = int(this._doubleTimeArray[0]);
         var _loc5_:int = int(this._doubleTimeArray[1]);
         var _loc6_:int = int(this._doubleTimeArray[2]);
         var _loc7_:int = int(this._doubleTimeArray[3]);
         var _loc8_:int = int(this._doubleTimeArray[4]);
         var _loc9_:int = int(this._doubleTimeArray[5]);
         var _loc10_:int = int(this._doubleTimeArray[6]);
         var _loc11_:int = int(this._doubleTimeArray[7]);
         if((_loc2_ > _loc4_ || _loc2_ == _loc4_ && _loc3_ >= _loc5_) && (_loc2_ < _loc6_ || _loc2_ == _loc6_ && _loc3_ < _loc7_) || (_loc2_ > _loc8_ || _loc2_ == _loc8_ && _loc3_ >= _loc9_) && (_loc2_ < _loc10_ || _loc2_ == _loc10_ && _loc3_ < _loc11_))
         {
            return true;
         }
         return false;
      }
      
      public function get startGameNeedMoney() : int
      {
         return this._startGameNeedMoney;
      }
      
      public function getBuyRecordStatus(param1:int) : Object
      {
         var _loc2_:int = 0;
         var _loc3_:Object = null;
         if(!this._buyRecordStatus)
         {
            this._buyRecordStatus = [];
            _loc2_ = 0;
            while(_loc2_ < 5)
            {
               _loc3_ = {};
               _loc3_.isNoPrompt = false;
               _loc3_.isBand = false;
               this._buyRecordStatus.push(_loc3_);
               _loc2_++;
            }
         }
         return this._buyRecordStatus[param1];
      }
      
      public function get rankAddInfo() : Array
      {
         return this._rankAddInfo;
      }
      
      public function get decelerateRate() : int
      {
         return this._decelerateRate;
      }
      
      public function get accelerateRate() : int
      {
         return this._accelerateRate;
      }
      
      public function get playerList() : Vector.<EscortPlayerInfo>
      {
         return this._playerList;
      }
      
      public function get usableCount() : int
      {
         return this._usableCount;
      }
      
      public function get freeCount() : int
      {
         return this._freeCount;
      }
      
      public function get carStatus() : int
      {
         return this._carStatus;
      }
      
      public function get isInGame() : Boolean
      {
         return this._isInGame;
      }
      
      public function get isStart() : Boolean
      {
         return this._isStart;
      }
      
      public function get isLoadIconComplete() : Boolean
      {
         return this._isLoadIconComplete;
      }
      
      public function setup() : void
      {
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.ESCORT,this.pkgHandler);
      }
      
      private function pkgHandler(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readByte();
         switch(_loc3_)
         {
            case EscortPackageType.START_OR_END:
               this.openOrCloseHandler(_loc2_);
               break;
            case EscortPackageType.CALL:
               this.changeCarStatus(_loc2_);
               break;
            case EscortPackageType.START_GAME:
               this.startGameHandler(_loc2_);
               break;
            case EscortPackageType.ENTER_GAME:
               this.enterGameHandler(_loc2_);
               break;
            case EscortPackageType.ALL_READY:
               this.allReadyHandler(_loc2_);
               break;
            case EscortPackageType.MOVE:
               this.moveHandler(_loc2_);
               break;
            case EscortPackageType.REFRESH_ITEM:
               this.refreshItemHandler(_loc2_);
               break;
            case EscortPackageType.REFRESH_BUFF:
               this.refreshBuffHandler(_loc2_);
               break;
            case EscortPackageType.USE_SKILL:
               this.useSkillHandler(_loc2_);
               break;
            case EscortPackageType.RANK_LIST:
               this.rankListHandler(_loc2_);
               break;
            case EscortPackageType.ARRIVE:
               this.arriveHandler(_loc2_);
               break;
            case EscortPackageType.DESTROY:
               this.destroyHandler(_loc2_);
               break;
            case EscortPackageType.RE_ENTER_ALL_INFO:
               this.reEnterAllInfoHandler(_loc2_);
               break;
            case EscortPackageType.IS_CAN_ENTER:
               this.canEnterHandler(_loc2_);
               break;
            case EscortPackageType.REFRESH_FIGHT_STATE:
               this.refreshFightStateHandler(_loc2_);
               break;
            case EscortPackageType.REFRESH_ENTER_COUNT:
               this.refreshEnterCountHandler(_loc2_);
               break;
            case EscortPackageType.REFRESH_ITEM_FREE_COUNT:
               this.refreshItemFreeCountHandler(_loc2_);
               break;
            case EscortPackageType.CANCEL_GAME:
               dispatchEvent(new Event(CANCEL_GAME));
         }
      }
      
      private function refreshItemFreeCountHandler(param1:PackageIn) : void
      {
         this._itemFreeCountList[0] = param1.readInt();
         this._itemFreeCountList[1] = param1.readInt();
         this._itemFreeCountList[2] = param1.readInt();
         dispatchEvent(new Event(REFRESH_ITEM_FREE_COUNT));
      }
      
      private function refreshEnterCountHandler(param1:PackageIn) : void
      {
         this._freeCount = param1.readInt();
         param1.readInt();
         this._usableCount = 0;
         dispatchEvent(new Event(REFRESH_ENTER_COUNT));
      }
      
      private function refreshFightStateHandler(param1:PackageIn) : void
      {
         var _loc2_:int = param1.readInt();
         var _loc3_:int = param1.readInt();
         var _loc4_:int = param1.readInt();
         var _loc5_:int = param1.readInt();
         var _loc6_:EscortEvent = new EscortEvent(FIGHT_STATE_CHANGE);
         _loc6_.data = {
            "id":_loc2_,
            "zoneId":_loc3_,
            "fightState":_loc4_,
            "posX":_loc5_
         };
         dispatchEvent(_loc6_);
      }
      
      private function canEnterHandler(param1:PackageIn) : void
      {
         this._isInGame = param1.readBoolean();
         if(this._isInGame)
         {
            dispatchEvent(new EscortEvent(CAN_ENTER));
         }
         else
         {
            this.loadEscortModule();
         }
      }
      
      private function reEnterAllInfoHandler(param1:PackageIn) : void
      {
         var _loc7_:EscortPlayerInfo = null;
         var _loc2_:Date = param1.readDate();
         this._playerList = new Vector.<EscortPlayerInfo>();
         var _loc3_:int = param1.readInt();
         var _loc4_:int = 0;
         while(_loc4_ < _loc3_)
         {
            _loc7_ = new EscortPlayerInfo();
            _loc7_.index = _loc4_;
            _loc7_.id = param1.readInt();
            _loc7_.zoneId = param1.readInt();
            _loc7_.name = param1.readUTF();
            _loc7_.carType = param1.readInt();
            _loc7_.posX = param1.readInt();
            _loc7_.fightState = param1.readInt();
            _loc7_.acceleEndTime = param1.readDate();
            _loc7_.deceleEndTime = param1.readDate();
            _loc7_.invisiEndTime = param1.readDate();
            if(_loc7_.zoneId == PlayerManager.Instance.Self.ZoneID && _loc7_.id == PlayerManager.Instance.Self.ID)
            {
               _loc7_.isSelf = true;
            }
            else
            {
               _loc7_.isSelf = false;
            }
            this._playerList.push(_loc7_);
            _loc4_++;
         }
         dispatchEvent(new Event(RE_ENTER_ALL_INFO));
         this.refreshItemHandler(param1);
         this.rankListHandler(param1);
         var _loc5_:Date = param1.readDate();
         var _loc6_:EscortEvent = new EscortEvent(ALL_READY);
         _loc6_.data = {
            "endTime":_loc2_,
            "isShowStartCountDown":false,
            "sprintEndTime":_loc5_
         };
         dispatchEvent(_loc6_);
      }
      
      private function destroyHandler(param1:PackageIn) : void
      {
         this._isInGame = false;
         this._carStatus = 0;
         dispatchEvent(new EscortEvent(DESTROY));
      }
      
      private function arriveHandler(param1:PackageIn) : void
      {
         var _loc2_:int = param1.readInt();
         var _loc3_:int = param1.readInt();
         if(_loc3_ == PlayerManager.Instance.Self.ZoneID && _loc2_ == PlayerManager.Instance.Self.ID)
         {
            this._isInGame = false;
            this._carStatus = 0;
         }
         var _loc4_:EscortEvent = new EscortEvent(ARRIVE);
         _loc4_.data = {
            "id":_loc2_,
            "zoneId":_loc3_
         };
         dispatchEvent(_loc4_);
      }
      
      private function rankListHandler(param1:PackageIn) : void
      {
         var _loc6_:int = 0;
         var _loc7_:String = null;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:Boolean = false;
         var _loc2_:int = param1.readInt();
         var _loc3_:Array = [];
         var _loc4_:int = 0;
         while(_loc4_ < _loc2_)
         {
            _loc6_ = param1.readInt();
            _loc7_ = param1.readUTF();
            _loc8_ = param1.readInt();
            _loc9_ = param1.readInt();
            _loc10_ = param1.readInt();
            _loc11_ = param1.readBoolean();
            _loc3_.push({
               "rank":_loc6_,
               "name":_loc7_,
               "carType":_loc8_,
               "id":_loc9_,
               "zoneId":_loc10_,
               "isSprint":_loc11_
            });
            _loc4_++;
         }
         _loc3_.sortOn("rank",Array.NUMERIC);
         var _loc5_:EscortEvent = new EscortEvent(RANK_ARRIVE_LIST);
         _loc5_.data = _loc3_;
         dispatchEvent(_loc5_);
      }
      
      private function useSkillHandler(param1:PackageIn) : void
      {
         var _loc2_:int = param1.readInt();
         var _loc3_:int = param1.readInt();
         var _loc4_:int = param1.readInt();
         var _loc5_:EscortEvent = new EscortEvent(USE_SKILL);
         _loc5_.data = {
            "id":_loc2_,
            "zoneId":_loc3_,
            "leapX":_loc4_,
            "sound":true
         };
         dispatchEvent(_loc5_);
      }
      
      private function refreshBuffHandler(param1:PackageIn) : void
      {
         var _loc2_:int = param1.readInt();
         var _loc3_:int = param1.readInt();
         var _loc4_:Date = param1.readDate();
         var _loc5_:Date = param1.readDate();
         var _loc6_:Date = param1.readDate();
         var _loc7_:EscortEvent = new EscortEvent(REFRESH_BUFF);
         _loc7_.data = {
            "id":_loc2_,
            "zoneId":_loc3_,
            "acceleEndTime":_loc4_,
            "deceleEndTime":_loc5_,
            "invisiEndTime":_loc6_
         };
         dispatchEvent(_loc7_);
      }
      
      private function refreshItemHandler(param1:PackageIn) : void
      {
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc2_:int = param1.readInt();
         var _loc3_:Array = [];
         var _loc4_:int = 0;
         while(_loc4_ < _loc2_)
         {
            _loc6_ = param1.readInt();
            _loc7_ = param1.readInt();
            _loc8_ = param1.readInt();
            _loc9_ = param1.readInt();
            _loc3_.push({
               "index":_loc6_,
               "type":_loc7_,
               "posX":_loc8_,
               "tag":_loc9_
            });
            _loc4_++;
         }
         var _loc5_:EscortEvent = new EscortEvent(REFRESH_ITEM);
         _loc5_.data = {"itemList":_loc3_};
         dispatchEvent(_loc5_);
      }
      
      private function moveHandler(param1:PackageIn) : void
      {
         var _loc2_:int = param1.readInt();
         var _loc3_:int = param1.readInt();
         var _loc4_:Number = param1.readInt();
         var _loc5_:EscortEvent = new EscortEvent(MOVE);
         _loc5_.data = {
            "zoneId":_loc3_,
            "id":_loc2_,
            "destX":_loc4_
         };
         dispatchEvent(_loc5_);
      }
      
      private function allReadyHandler(param1:PackageIn) : void
      {
         var _loc2_:Date = param1.readDate();
         var _loc3_:Date = param1.readDate();
         var _loc4_:EscortEvent = new EscortEvent(ALL_READY);
         _loc4_.data = {
            "endTime":_loc2_,
            "isShowStartCountDown":true,
            "sprintEndTime":_loc3_
         };
         dispatchEvent(_loc4_);
      }
      
      private function enterGameHandler(param1:PackageIn) : void
      {
         var _loc4_:EscortPlayerInfo = null;
         this._playerList = new Vector.<EscortPlayerInfo>();
         var _loc2_:int = param1.readInt();
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_)
         {
            _loc4_ = new EscortPlayerInfo();
            _loc4_.index = _loc3_;
            _loc4_.zoneId = param1.readInt();
            _loc4_.id = param1.readInt();
            _loc4_.carType = param1.readInt();
            _loc4_.name = param1.readUTF();
            if(_loc4_.zoneId == PlayerManager.Instance.Self.ZoneID && _loc4_.id == PlayerManager.Instance.Self.ID)
            {
               _loc4_.isSelf = true;
            }
            else
            {
               _loc4_.isSelf = false;
            }
            this._playerList.push(_loc4_);
            _loc3_++;
         }
         this._isInGame = true;
         dispatchEvent(new Event(ENTER_GAME));
      }
      
      private function startGameHandler(param1:PackageIn) : void
      {
         dispatchEvent(new Event(START_GAME));
      }
      
      private function changeCarStatus(param1:PackageIn) : void
      {
         this._carStatus = param1.readInt();
         dispatchEvent(new Event(CAR_STATUS_CHANGE));
      }
      
      private function openOrCloseHandler(param1:PackageIn) : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:String = null;
         var _loc6_:Array = null;
         var _loc7_:Array = null;
         var _loc8_:Array = null;
         var _loc9_:Array = null;
         var _loc10_:Array = null;
         var _loc11_:Array = null;
         var _loc12_:Array = null;
         var _loc13_:ServerConfigInfo = null;
         var _loc14_:Array = null;
         var _loc15_:Array = null;
         var _loc16_:Array = null;
         var _loc17_:Date = null;
         var _loc18_:EscortCarInfo = null;
         var _loc19_:int = 0;
         var _loc20_:int = 0;
         var _loc21_:int = 0;
         var _loc22_:int = 0;
         param1.readInt();
         this._isStart = param1.readBoolean();
         if(this._isStart)
         {
            this.dataInfo = new EscortInfoVo();
            this._isInGame = param1.readBoolean();
            this._freeCount = param1.readInt();
            param1.readInt();
            this._usableCount = 0;
            this._carStatus = param1.readInt();
            this.dataInfo.carInfo = {};
            _loc2_ = param1.readInt();
            _loc3_ = 0;
            while(_loc3_ < _loc2_)
            {
               _loc18_ = new EscortCarInfo();
               _loc18_.type = param1.readInt();
               _loc18_.needMoney = param1.readInt();
               _loc18_.speed = param1.readInt();
               _loc19_ = param1.readInt();
               _loc20_ = 0;
               while(_loc20_ < _loc19_)
               {
                  _loc21_ = param1.readInt();
                  _loc22_ = param1.readInt();
                  if(_loc21_ == 11772)
                  {
                     _loc18_.prestige = _loc22_;
                  }
                  else
                  {
                     _loc18_.itemId = _loc21_;
                     _loc18_.itemCount = _loc22_;
                  }
                  _loc20_++;
               }
               this.dataInfo.carInfo[_loc18_.type] = _loc18_;
               _loc3_++;
            }
            this.dataInfo.useSkillNeedMoney = [];
            this.dataInfo.useSkillNeedMoney.push(param1.readInt());
            this.dataInfo.useSkillNeedMoney.push(param1.readInt());
            this.dataInfo.useSkillNeedMoney.push(param1.readInt());
            this._rankAddInfo = [];
            _loc2_ = param1.readInt();
            _loc4_ = 0;
            while(_loc4_ < _loc2_)
            {
               this._rankAddInfo.push(param1.readInt());
               _loc4_++;
            }
            this._accelerateRate = param1.readInt();
            this._decelerateRate = param1.readInt();
            this._startGameNeedMoney = param1.readInt();
            _loc5_ = param1.readUTF();
            _loc6_ = _loc5_.split("|");
            _loc7_ = _loc6_[0].split(",");
            _loc8_ = _loc7_[0].split(":");
            _loc9_ = _loc7_[1].split(":");
            _loc10_ = _loc6_[1].split(",");
            _loc11_ = _loc10_[0].split(":");
            _loc12_ = _loc10_[1].split(":");
            this._doubleTimeArray = _loc8_.concat(_loc9_).concat(_loc11_).concat(_loc12_);
            this._sprintAwardInfo = param1.readUTF().split(",");
            this._timer = new Timer(1000);
            this._timer.addEventListener(TimerEvent.TIMER,this.timerHandler,false,0,true);
            this._timer.start();
            this.open();
            _loc13_ = ServerConfigManager.instance.findInfoByName("FiveYearCarEndDate");
            _loc14_ = _loc13_.Value.split(" ");
            if((_loc14_[0] as String).indexOf("-") > 0)
            {
               _loc15_ = _loc14_[0].split("-");
            }
            else
            {
               _loc15_ = _loc14_[0].split("/");
            }
            _loc16_ = _loc14_[1].split(":");
            _loc17_ = new Date(_loc15_[0],int(_loc15_[1]) - 1,_loc15_[2],_loc16_[0],_loc16_[1],_loc16_[2]);
            this._endTime = _loc17_.getTime() / 1000;
            this._hasPrompted = new DictionaryData();
         }
         else
         {
            this._isInGame = false;
            if(this._timer)
            {
               this._timer.removeEventListener(TimerEvent.TIMER,this.timerHandler);
               this._timer.stop();
               this._timer = null;
            }
            dispatchEvent(new Event(END));
         }
      }
      
      private function timerHandler(param1:TimerEvent) : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         if(this.isInDoubleTime)
         {
            if(!this._isPromptDoubleTime)
            {
               ChatManager.Instance.sysChatAmaranth(LanguageMgr.GetTranslation("escort.doubleTime.startTipTxt"));
               this._isPromptDoubleTime = true;
            }
         }
         else if(this._isPromptDoubleTime)
         {
            ChatManager.Instance.sysChatAmaranth(LanguageMgr.GetTranslation("escort.doubleTime.endTipTxt"));
            this._isPromptDoubleTime = false;
         }
         if(this._endTime > 0)
         {
            _loc2_ = TimeManager.Instance.Now().getTime() / 1000;
            _loc3_ = this._endTime - _loc2_;
            if(_loc3_ > 0)
            {
               _loc4_ = _loc3_ % 3600;
               if(_loc4_ < 5)
               {
                  _loc5_ = _loc3_ / 3600;
                  if(_loc5_ <= 48 && _loc5_ > 0 && !this._hasPrompted.hasKey(_loc5_))
                  {
                     ChatManager.Instance.sysChatAmaranth(LanguageMgr.GetTranslation("escort.willEnd.promptTxt",_loc5_));
                     this._hasPrompted.add(_loc5_,1);
                  }
               }
            }
         }
      }
      
      public function enterMainViewHandler() : void
      {
      }
      
      public function leaveMainViewHandler() : void
      {
         this._playerList = null;
      }
      
      public function open() : void
      {
         this._isLoadIconComplete = true;
         dispatchEvent(new Event(ICON_RES_LOAD_COMPLETE));
      }
      
      public function loadEscortModule() : void
      {
         this._loadResCount = 0;
         UIModuleSmallLoading.Instance.progress = 0;
         UIModuleSmallLoading.Instance.show();
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadFrameCompleteHandler);
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUimoduleLoadProgress);
         UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.ESCORT_FRAME);
         UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.ESCORT_GAME);
         UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.DDTROOM);
      }
      
      private function onUimoduleLoadProgress(param1:UIModuleEvent) : void
      {
         var _loc2_:Number = NaN;
         if(param1.module == UIModuleTypes.ESCORT_FRAME || param1.module == UIModuleTypes.ESCORT_GAME || param1.module == UIModuleTypes.DDTROOM)
         {
            _loc2_ = param1.loader.progress;
            _loc2_ = _loc2_ > 0.99?Number(0.99):Number(_loc2_);
            UIModuleSmallLoading.Instance.progress = _loc2_ * 100;
         }
      }
      
      private function loadFrameCompleteHandler(param1:UIModuleEvent) : void
      {
         var _loc2_:EscortFrame = null;
         if(param1.module == UIModuleTypes.ESCORT_FRAME)
         {
            this._loadResCount++;
         }
         if(param1.module == UIModuleTypes.ESCORT_GAME)
         {
            this._loadResCount++;
         }
         if(param1.module == UIModuleTypes.DDTROOM)
         {
            this._loadResCount++;
         }
         if(this._loadResCount >= 3)
         {
            UIModuleSmallLoading.Instance.hide();
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadFrameCompleteHandler);
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUimoduleLoadProgress);
            _loc2_ = ComponentFactory.Instance.creatComponentByStylename("EscortFrame");
            LayerManager.Instance.addToLayer(_loc2_,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
         }
      }
      
      public function getPlayerResUrl(param1:Boolean, param2:int) : String
      {
         var _loc3_:String = null;
         if(param1)
         {
            _loc3_ = "self";
         }
         else
         {
            _loc3_ = "other";
         }
         return PathManager.SITE_MAIN + "image/escort/" + "escort" + _loc3_ + param2 + ".swf";
      }
      
      public function loadSound() : void
      {
         var _loc1_:BaseLoader = LoadResourceManager.Instance.createLoader(PathManager.SITE_MAIN + "image/escort/escortAudio.swf",BaseLoader.MODULE_LOADER);
         _loc1_.addEventListener(LoaderEvent.COMPLETE,this.loadSoundCompleteHandler);
         LoadResourceManager.Instance.startLoad(_loc1_);
      }
      
      private function loadSoundCompleteHandler(param1:LoaderEvent) : void
      {
         param1.loader.removeEventListener(LoaderEvent.COMPLETE,this.loadSoundCompleteHandler);
         SoundManager.instance.initEscortSound();
      }
   }
}
