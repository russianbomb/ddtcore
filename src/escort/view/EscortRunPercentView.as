package escort.view
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.LanguageMgr;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   
   public class EscortRunPercentView extends Sprite implements Disposeable
   {
       
      
      private var _bg:Bitmap;
      
      private var _txt:FilterFrameText;
      
      private var _recordTxt:String;
      
      public function EscortRunPercentView()
      {
         super();
         this.x = 445;
         this.initView();
      }
      
      private function initView() : void
      {
         this._bg = ComponentFactory.Instance.creatBitmap("asset.escort.countDownBg");
         this._txt = ComponentFactory.Instance.creatComponentByStylename("escort.countDownView.txt");
         this._recordTxt = LanguageMgr.GetTranslation("escort.game.runPercentTxt");
         this._txt.text = this._recordTxt + "--";
         addChild(this._bg);
         addChild(this._txt);
      }
      
      public function refreshView(param1:int) : void
      {
         var _loc2_:int = 22780 - param1;
         _loc2_ = _loc2_ < 0?int(0):int(_loc2_);
         var _loc3_:int = Math.ceil(_loc2_ / 22500 * 10);
         if(this._txt)
         {
            this._txt.text = this._recordTxt + _loc3_ * 10 + "%";
         }
      }
      
      public function dispose() : void
      {
         ObjectUtils.disposeAllChildren(this);
         this._bg = null;
         this._txt = null;
         this._recordTxt = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
