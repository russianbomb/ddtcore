package escort.view
{
   import bagAndInfo.cell.BaseCell;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.manager.ItemManager;
   import ddt.utils.PositionUtils;
   import escort.EscortManager;
   import flash.display.Sprite;
   
   public class EscortRankCell extends Sprite implements Disposeable
   {
       
      
      private var _rankTxt:FilterFrameText;
      
      private var _nameTxt1:FilterFrameText;
      
      private var _nameTxt2:FilterFrameText;
      
      private var _rateTxt:FilterFrameText;
      
      private var _awardCell:BaseCell;
      
      public function EscortRankCell(param1:int)
      {
         super();
         this._rankTxt = ComponentFactory.Instance.creatComponentByStylename("escort.rankView.cellTxt");
         this._rankTxt.text = (param1 + 1).toString();
         this._nameTxt1 = ComponentFactory.Instance.creatComponentByStylename("escort.rankView.cellTxt");
         this._nameTxt1.text = "-------";
         PositionUtils.setPos(this._nameTxt1,"escort.rankView.cellNameTxtPos");
         this._nameTxt2 = ComponentFactory.Instance.creatComponentByStylename("escort.rankView.cellNameTxt");
         this._nameTxt2.visible = false;
         this._rateTxt = ComponentFactory.Instance.creatComponentByStylename("escort.rankView.cellTxt");
         this._rateTxt.text = EscortManager.instance.rankAddInfo[param1] + "%";
         PositionUtils.setPos(this._rateTxt,"escort.rankView.cellRateTxtPos");
         var _loc2_:Sprite = new Sprite();
         _loc2_.graphics.beginFill(16711680,0);
         _loc2_.graphics.drawRect(0,0,30,30);
         _loc2_.graphics.endFill();
         var _loc3_:ItemTemplateInfo = ItemManager.Instance.getTemplateById(EscortManager.instance.sprintAwardInfo[param1]);
         this._awardCell = new BaseCell(_loc2_,_loc3_);
         PositionUtils.setPos(this._awardCell,"escort.rankView.cellAwardCellPos");
         addChild(this._rankTxt);
         addChild(this._nameTxt1);
         addChild(this._nameTxt2);
         addChild(this._rateTxt);
         addChild(this._awardCell);
      }
      
      public function setName(param1:String, param2:int) : void
      {
         this._nameTxt2.text = param1;
         if(param2 == 0)
         {
            this._nameTxt2.textColor = 16777215;
         }
         else if(param2 == 1)
         {
            this._nameTxt2.textColor = 710173;
         }
         else
         {
            this._nameTxt2.textColor = 16711680;
         }
         this._nameTxt2.visible = true;
         this._nameTxt1.visible = false;
      }
      
      public function dispose() : void
      {
         ObjectUtils.disposeAllChildren(this);
         this._rankTxt = null;
         this._nameTxt1 = null;
         this._nameTxt2 = null;
         this._rateTxt = null;
         this._awardCell = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
