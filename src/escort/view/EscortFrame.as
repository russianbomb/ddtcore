package escort.view
{
   import baglocked.BaglockedManager;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.InviteManager;
   import ddt.manager.LanguageMgr;
   import ddt.manager.LeavePageManager;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.manager.StateManager;
   import ddt.states.StateType;
   import escort.EscortManager;
   import flash.display.Bitmap;
   import flash.events.Event;
   import flash.events.MouseEvent;
   
   public class EscortFrame extends Frame
   {
       
      
      private var _bg:ScaleBitmapImage;
      
      private var _bottomBg:ScaleBitmapImage;
      
      private var _cellList:Vector.<EscortFrameItemCell>;
      
      private var _btn:SimpleBitmapButton;
      
      private var _countTxt:FilterFrameText;
      
      private var _helpBtn:EscortHelpBtn;
      
      private var _doubleTagIcon:Bitmap;
      
      private var _matchView:EscortMatchView;
      
      public function EscortFrame()
      {
         super();
         InviteManager.Instance.enabled = false;
         this._cellList = new Vector.<EscortFrameItemCell>();
         this.initView();
         this.initEvent();
      }
      
      private function initView() : void
      {
         var _loc2_:EscortFrameItemCell = null;
         titleText = LanguageMgr.GetTranslation("escort.frame.titleTxt");
         this._bg = ComponentFactory.Instance.creatComponentByStylename("escort.frame.bg");
         addToContent(this._bg);
         this._bottomBg = ComponentFactory.Instance.creatComponentByStylename("escort.frame.bottomBg");
         addToContent(this._bottomBg);
         var _loc1_:int = 0;
         while(_loc1_ < 3)
         {
            _loc2_ = new EscortFrameItemCell(_loc1_,EscortManager.instance.dataInfo.carInfo[_loc1_]);
            _loc2_.x = 8 + (_loc2_.width + 4) * _loc1_;
            _loc2_.y = 10;
            addToContent(_loc2_);
            this._cellList.push(_loc2_);
            _loc1_++;
         }
         this._countTxt = ComponentFactory.Instance.creatComponentByStylename("escort.frame.countTxt");
         this.refreshEnterCountHandler(null);
         addToContent(this._countTxt);
         this._helpBtn = new EscortHelpBtn(false);
         addToContent(this._helpBtn);
         this._doubleTagIcon = ComponentFactory.Instance.creatBitmap("asset.escort.doubleScoreIcon");
         addToContent(this._doubleTagIcon);
         this.refreshDoubleTagIcon();
      }
      
      private function refreshDoubleTagIcon() : void
      {
         if(EscortManager.instance.isInDoubleTime)
         {
            this._doubleTagIcon.visible = true;
         }
         else
         {
            this._doubleTagIcon.visible = false;
         }
      }
      
      private function initEvent() : void
      {
         addEventListener(FrameEvent.RESPONSE,this.__responseHandler);
         this._btn.addEventListener(MouseEvent.CLICK,this.clickHandler,false,0,true);
         EscortManager.instance.addEventListener(EscortManager.START_GAME,this.startGameHandler);
         EscortManager.instance.addEventListener(EscortManager.ENTER_GAME,this.enterGameHandler);
         EscortManager.instance.addEventListener(EscortManager.END,this.endHandler);
         EscortManager.instance.addEventListener(EscortManager.REFRESH_ENTER_COUNT,this.refreshEnterCountHandler);
      }
      
      private function refreshEnterCountHandler(param1:Event) : void
      {
         var _loc3_:int = 0;
         if(this._btn)
         {
            this._btn.removeEventListener(MouseEvent.CLICK,this.clickHandler);
            ObjectUtils.disposeObject(this._btn);
         }
         var _loc2_:int = EscortManager.instance.freeCount;
         if(_loc2_ > 0)
         {
            this._btn = ComponentFactory.Instance.creatComponentByStylename("escort.frame.freeBtn");
            _loc3_ = _loc2_;
         }
         else
         {
            this._btn = ComponentFactory.Instance.creatComponentByStylename("escort.frame.startBtn");
            _loc3_ = EscortManager.instance.usableCount;
         }
         addToContent(this._btn);
         this._countTxt.text = "(" + _loc3_ + ")";
      }
      
      private function endHandler(param1:Event) : void
      {
         SocketManager.Instance.out.sendEscortCancelGame();
         this.dispose();
      }
      
      private function enterGameHandler(param1:Event) : void
      {
         this.dispose();
         StateManager.setState(StateType.ESCORT);
      }
      
      private function startGameHandler(param1:Event) : void
      {
         this._matchView = ComponentFactory.Instance.creatComponentByStylename("escort.frame.matchView");
         LayerManager.Instance.addToLayer(this._matchView,LayerManager.GAME_TOP_LAYER,true,LayerManager.ALPHA_BLOCKGOUND);
      }
      
      private function clickHandler(param1:MouseEvent) : void
      {
         var _loc2_:int = 0;
         var _loc3_:Object = null;
         var _loc4_:BaseAlerFrame = null;
         SoundManager.instance.play("008");
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         if(EscortManager.instance.freeCount > 0)
         {
            SocketManager.Instance.out.sendEscortStartGame(false);
         }
         else
         {
            if(EscortManager.instance.usableCount <= 0)
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("escort.noEnoughUsableCount"));
               return;
            }
            _loc2_ = EscortManager.instance.startGameNeedMoney;
            _loc3_ = EscortManager.instance.getBuyRecordStatus(1);
            if(_loc3_.isNoPrompt)
            {
               if(_loc3_.isBand && PlayerManager.Instance.Self.BandMoney < _loc2_)
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("bindMoneyPoorNote"));
                  _loc3_.isNoPrompt = false;
               }
               else if(!_loc3_.isBand && PlayerManager.Instance.Self.Money < _loc2_)
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("moneyPoorNote"));
                  _loc3_.isNoPrompt = false;
               }
               else
               {
                  SocketManager.Instance.out.sendEscortStartGame(_loc3_.isBand);
                  return;
               }
            }
            _loc4_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("escort.frame.startGameConfirmTxt",_loc2_),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),true,true,true,LayerManager.BLCAK_BLOCKGOUND,null,"EscortBuyConfirmView",30,true,AlertManager.SELECTBTN);
            _loc4_.moveEnable = false;
            _loc4_.addEventListener(FrameEvent.RESPONSE,this.startConfirm,false,0,true);
         }
      }
      
      private function startConfirm(param1:FrameEvent) : void
      {
         var _loc3_:int = 0;
         var _loc4_:BaseAlerFrame = null;
         var _loc5_:Object = null;
         SoundManager.instance.play("008");
         var _loc2_:BaseAlerFrame = param1.currentTarget as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.startConfirm);
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            _loc3_ = EscortManager.instance.startGameNeedMoney;
            if(_loc2_.isBand && PlayerManager.Instance.Self.BandMoney < _loc3_)
            {
               _loc4_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("escort.game.useSkillNoEnoughReConfirm"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),true,true,true,LayerManager.BLCAK_BLOCKGOUND);
               _loc4_.moveEnable = false;
               _loc4_.addEventListener(FrameEvent.RESPONSE,this.startGameReConfirm,false,0,true);
               return;
            }
            if(!_loc2_.isBand && PlayerManager.Instance.Self.Money < _loc3_)
            {
               LeavePageManager.showFillFrame();
               return;
            }
            if((_loc2_ as EscortBuyConfirmView).isNoPrompt)
            {
               _loc5_ = EscortManager.instance.getBuyRecordStatus(1);
               _loc5_.isNoPrompt = true;
               _loc5_.isBand = _loc2_.isBand;
            }
            SocketManager.Instance.out.sendEscortStartGame(_loc2_.isBand);
         }
      }
      
      private function startGameReConfirm(param1:FrameEvent) : void
      {
         var _loc3_:int = 0;
         SoundManager.instance.play("008");
         var _loc2_:BaseAlerFrame = param1.currentTarget as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.startGameReConfirm);
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            _loc3_ = EscortManager.instance.startGameNeedMoney;
            if(PlayerManager.Instance.Self.Money < _loc3_)
            {
               LeavePageManager.showFillFrame();
               return;
            }
            SocketManager.Instance.out.sendEscortStartGame(false);
         }
      }
      
      private function __responseHandler(param1:FrameEvent) : void
      {
         if(param1.responseCode == FrameEvent.CLOSE_CLICK || param1.responseCode == FrameEvent.ESC_CLICK)
         {
            SoundManager.instance.play("008");
            this.dispose();
         }
      }
      
      private function removeEvent() : void
      {
         removeEventListener(FrameEvent.RESPONSE,this.__responseHandler);
         if(this._btn)
         {
            this._btn.removeEventListener(MouseEvent.CLICK,this.clickHandler);
         }
         EscortManager.instance.removeEventListener(EscortManager.START_GAME,this.startGameHandler);
         EscortManager.instance.removeEventListener(EscortManager.ENTER_GAME,this.enterGameHandler);
         EscortManager.instance.removeEventListener(EscortManager.END,this.endHandler);
         EscortManager.instance.removeEventListener(EscortManager.REFRESH_ENTER_COUNT,this.refreshEnterCountHandler);
      }
      
      override public function dispose() : void
      {
         InviteManager.Instance.enabled = true;
         this.removeEvent();
         super.dispose();
         this._bg = null;
         this._bottomBg = null;
         this._cellList = null;
         this._btn = null;
         this._countTxt = null;
         this._helpBtn = null;
         this._doubleTagIcon = null;
         ObjectUtils.disposeObject(this._matchView);
         this._matchView = null;
      }
   }
}
