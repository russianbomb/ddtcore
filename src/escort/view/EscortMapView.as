package escort.view
{
   import com.greensock.TweenLite;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.SoundManager;
   import escort.EscortManager;
   import escort.data.EscortPlayerInfo;
   import escort.event.EscortEvent;
   import escort.player.EscortGameItem;
   import escort.player.EscortGamePlayer;
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   import road7th.data.DictionaryData;
   
   public class EscortMapView extends Sprite implements Disposeable
   {
       
      
      private var _mapLayer:Sprite;
      
      private var _playerLayer:Sprite;
      
      private var _playerList:Vector.<EscortGamePlayer>;
      
      private var _selfPlayer:EscortGamePlayer;
      
      private var _itemList:DictionaryData;
      
      private var _playerItemList:Array;
      
      private var _rankArriveList:Array;
      
      private var _needRankList:DictionaryData;
      
      private var _isStartGame:Boolean = false;
      
      private var _mapBitmapData:BitmapData;
      
      private var _startMc:MovieClip;
      
      private var _endMc:MovieClip;
      
      private var _finishTag:Bitmap;
      
      private var _runPercent:EscortRunPercentView;
      
      private var _enterFrameCount:int = 0;
      
      private var _isTween:Boolean = false;
      
      public function EscortMapView()
      {
         this._itemList = new DictionaryData();
         this._playerItemList = [];
         this._rankArriveList = [];
         super();
         this.initView();
         this.initEvent();
      }
      
      public function set runPercent(param1:EscortRunPercentView) : void
      {
         this._runPercent = param1;
      }
      
      private function initView() : void
      {
         this.initMapLayer();
         this.initPlayer();
      }
      
      private function initMapLayer() : void
      {
         var _loc3_:Bitmap = null;
         var _loc4_:Bitmap = null;
         this._mapLayer = new Sprite();
         addChild(this._mapLayer);
         this._mapBitmapData = ComponentFactory.Instance.creatBitmapData("asset.escort.mapBg");
         var _loc1_:int = 0;
         while(_loc1_ < 12)
         {
            _loc4_ = new Bitmap(this._mapBitmapData);
            _loc4_.x = _loc1_ * 1978;
            this._mapLayer.addChild(_loc4_);
            _loc1_++;
         }
         var _loc2_:BitmapData = new BitmapData(884,600);
         _loc2_.copyPixels(this._mapBitmapData,new Rectangle(0,0,884,600),new Point(0,0));
         _loc3_ = new Bitmap(_loc2_);
         _loc3_.x = 12 * 1978;
         this._mapLayer.addChild(_loc3_);
         this._startMc = ComponentFactory.Instance.creat("asset.escort.gameStartTagMC");
         this._startMc.gotoAndStop(1);
         this._startMc.x = 67;
         this._startMc.y = -5;
         this._endMc = ComponentFactory.Instance.creat("asset.escort.gameEndTagMC");
         this._endMc.gotoAndStop(1);
         this._endMc.x = 22739;
         this._endMc.y = -5;
         this._mapLayer.addChild(this._startMc);
         this._mapLayer.addChild(this._endMc);
      }
      
      private function initPlayer() : void
      {
         var _loc4_:EscortPlayerInfo = null;
         var _loc5_:EscortGamePlayer = null;
         this._playerLayer = new Sprite();
         addChild(this._playerLayer);
         var _loc1_:Vector.<EscortPlayerInfo> = EscortManager.instance.playerList;
         if(!_loc1_)
         {
            return;
         }
         this._playerList = new Vector.<EscortGamePlayer>();
         this._needRankList = new DictionaryData();
         var _loc2_:int = _loc1_.length;
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_)
         {
            _loc4_ = _loc1_[_loc3_];
            _loc5_ = new EscortGamePlayer(_loc4_);
            this._playerLayer.addChild(_loc5_);
            this._playerList.push(_loc5_);
            this._playerItemList.push(_loc5_);
            this._needRankList.add(_loc3_.toString(),_loc3_);
            if(_loc4_.isSelf)
            {
               this._selfPlayer = _loc5_;
            }
            _loc3_++;
         }
         this.playerItemDepth();
         this.refreshNeedRankList();
         this.setCenter(false);
      }
      
      private function initEvent() : void
      {
         addEventListener(Event.ENTER_FRAME,this.updateMap,false,0,true);
         EscortManager.instance.addEventListener(EscortManager.MOVE,this.moveHandler);
         EscortManager.instance.addEventListener(EscortManager.REFRESH_ITEM,this.refreshItemHandler);
         EscortManager.instance.addEventListener(EscortManager.REFRESH_BUFF,this.refreshBuffHandler);
         EscortManager.instance.addEventListener(EscortManager.USE_SKILL,this.useSkillHandler);
         EscortManager.instance.addEventListener(EscortManager.RE_ENTER_ALL_INFO,this.createPlayerHandler);
         EscortManager.instance.addEventListener(EscortManager.FIGHT_STATE_CHANGE,this.playerFightStateChangeHandler);
         EscortManager.instance.addEventListener(EscortManager.RANK_ARRIVE_LIST,this.rankArriveListChangeHandler);
      }
      
      private function rankArriveListChangeHandler(param1:EscortEvent) : void
      {
         this._rankArriveList = param1.data as Array;
         this.refreshNeedRankList();
      }
      
      private function refreshNeedRankList() : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:Object = null;
         if(!this._playerList || !this._rankArriveList)
         {
            return;
         }
         var _loc1_:Array = [];
         for each(_loc2_ in this._needRankList)
         {
            for each(_loc4_ in this._rankArriveList)
            {
               if(_loc4_.id == this._playerList[_loc2_].playerInfo.id && _loc4_.zoneId == this._playerList[_loc2_].playerInfo.zoneId)
               {
                  _loc1_.push(_loc2_);
                  break;
               }
            }
         }
         for each(_loc3_ in _loc1_)
         {
            this._needRankList.remove(_loc3_.toString());
         }
      }
      
      private function updateRankList() : void
      {
         var _loc2_:int = 0;
         var _loc3_:Array = null;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         if(!this._playerList)
         {
            return;
         }
         var _loc1_:Array = [];
         for each(_loc2_ in this._needRankList)
         {
            _loc1_.push(this._playerList[_loc2_]);
         }
         _loc1_.sortOn("x",Array.NUMERIC | Array.DESCENDING);
         _loc3_ = [];
         _loc4_ = _loc1_.length;
         _loc5_ = 0;
         while(_loc5_ < _loc4_)
         {
            _loc3_.push({
               "name":_loc1_[_loc5_].playerInfo.name,
               "carType":_loc1_[_loc5_].playerInfo.carType
            });
            _loc5_++;
         }
         var _loc6_:EscortEvent = new EscortEvent(EscortManager.RANK_LIST);
         _loc6_.data = this._rankArriveList.concat(_loc3_);
         EscortManager.instance.dispatchEvent(_loc6_);
      }
      
      private function playerFightStateChangeHandler(param1:EscortEvent) : void
      {
         var _loc3_:EscortGamePlayer = null;
         var _loc2_:Object = param1.data;
         for each(_loc3_ in this._playerList)
         {
            if(_loc3_.playerInfo.zoneId == _loc2_.zoneId && _loc3_.playerInfo.id == _loc2_.id)
            {
               _loc3_.playerInfo.fightState = _loc2_.fightState;
               _loc3_.refreshFightMc();
               _loc3_.x = _loc2_.posX + 280;
               break;
            }
         }
      }
      
      private function createPlayerHandler(param1:Event) : void
      {
         this.initPlayer();
      }
      
      private function useSkillHandler(param1:EscortEvent) : void
      {
         var _loc3_:EscortGamePlayer = null;
         var _loc2_:Object = param1.data;
         for each(_loc3_ in this._playerList)
         {
            if(_loc3_.playerInfo.zoneId == _loc2_.zoneId && _loc3_.playerInfo.id == _loc2_.id)
            {
               if(_loc2_.sound)
               {
                  SoundManager.instance.play("escort04");
               }
               _loc3_.x = _loc2_.leapX + 280;
               break;
            }
         }
      }
      
      private function refreshBuffHandler(param1:EscortEvent) : void
      {
         var _loc3_:EscortGamePlayer = null;
         var _loc2_:Object = param1.data;
         for each(_loc3_ in this._playerList)
         {
            if(_loc3_.playerInfo.zoneId == _loc2_.zoneId && _loc3_.playerInfo.id == _loc2_.id)
            {
               if(_loc3_.playerInfo.isSelf)
               {
                  if((_loc2_.acceleEndTime as Date).getTime() - _loc3_.playerInfo.acceleEndTime.getTime() > 1000)
                  {
                     SoundManager.instance.play("escort01");
                  }
                  if((_loc2_.deceleEndTime as Date).getTime() - _loc3_.playerInfo.deceleEndTime.getTime() > 1000)
                  {
                     SoundManager.instance.play("escort02");
                  }
                  else if((_loc2_.deceleEndTime as Date).getTime() - _loc3_.playerInfo.deceleEndTime.getTime() < -1000)
                  {
                     SoundManager.instance.play("escort05");
                  }
                  if((_loc2_.invisiEndTime as Date).getTime() - _loc3_.playerInfo.invisiEndTime.getTime() > 1000)
                  {
                     SoundManager.instance.play("escort03");
                  }
               }
               _loc3_.playerInfo.acceleEndTime = _loc2_.acceleEndTime;
               _loc3_.playerInfo.deceleEndTime = _loc2_.deceleEndTime;
               _loc3_.playerInfo.invisiEndTime = _loc2_.invisiEndTime;
               _loc3_.refreshBuffCountDown();
               break;
            }
         }
      }
      
      private function refreshItemHandler(param1:EscortEvent) : void
      {
         var _loc4_:Object = null;
         var _loc5_:EscortGameItem = null;
         var _loc6_:int = 0;
         var _loc7_:EscortGameItem = null;
         var _loc2_:Object = param1.data;
         var _loc3_:Array = _loc2_.itemList;
         for each(_loc4_ in _loc3_)
         {
            _loc5_ = this._itemList[_loc4_.index];
            ObjectUtils.disposeObject(_loc5_);
            if(_loc5_)
            {
               this._playerItemList.splice(this._playerItemList.indexOf(_loc5_),1);
            }
            _loc6_ = _loc4_.tag;
            if(_loc6_ == 0)
            {
               this._itemList.remove(_loc4_.index);
            }
            else
            {
               _loc7_ = new EscortGameItem(_loc4_.index,_loc4_.type,_loc4_.posX);
               this._playerLayer.addChild(_loc7_);
               this._itemList.add(_loc4_.index,_loc7_);
               this._playerItemList.push(_loc7_);
            }
         }
         this.playerItemDepth();
      }
      
      private function playerItemDepth() : void
      {
         this._playerItemList.sortOn("y",Array.NUMERIC);
         var _loc1_:int = this._playerItemList.length;
         var _loc2_:int = 0;
         while(_loc2_ < _loc1_)
         {
            this._playerLayer.addChild(this._playerItemList[_loc2_]);
            _loc2_++;
         }
      }
      
      private function moveHandler(param1:EscortEvent) : void
      {
         var _loc3_:EscortGamePlayer = null;
         var _loc2_:Object = param1.data;
         for each(_loc3_ in this._playerList)
         {
            if(_loc3_.playerInfo.zoneId == _loc2_.zoneId && _loc3_.playerInfo.id == _loc2_.id)
            {
               _loc3_.destinationX = _loc2_.destX;
               break;
            }
         }
      }
      
      private function updateMap(param1:Event) : void
      {
         var _loc2_:EscortGamePlayer = null;
         if(!this._isStartGame)
         {
            return;
         }
         this._enterFrameCount++;
         if(this._enterFrameCount > 25)
         {
            this.updateRankList();
            this._enterFrameCount = 0;
         }
         for each(_loc2_ in this._playerList)
         {
            _loc2_.updatePlayer();
         }
         this.setCenter();
         if(this._selfPlayer && this._runPercent)
         {
            this._runPercent.refreshView(this._selfPlayer.x);
         }
      }
      
      private function setCenter(param1:Boolean = true) : void
      {
         if(!this._selfPlayer)
         {
            return;
         }
         if(this._isTween)
         {
            return;
         }
         var _loc2_:Number = 350 - this._selfPlayer.x;
         if(_loc2_ > 0)
         {
            _loc2_ = 0;
         }
         if(_loc2_ < 1000 - this._mapLayer.width)
         {
            _loc2_ = 1000 - this._mapLayer.width;
         }
         var _loc3_:Number = Math.abs(x - _loc2_);
         if(param1 && _loc3_ > 14)
         {
            TweenLite.to(this,_loc3_ / 400 * 0.5,{
               "x":_loc2_,
               "onComplete":this.tweenComplete
            });
            this._isTween = true;
         }
         else
         {
            x = _loc2_;
         }
      }
      
      private function tweenComplete() : void
      {
         this._isTween = false;
      }
      
      public function startGame() : void
      {
         var _loc1_:EscortGamePlayer = null;
         this._isStartGame = true;
         for each(_loc1_ in this._playerList)
         {
            _loc1_.startGame();
         }
      }
      
      public function endGame() : void
      {
         var _loc1_:EscortGamePlayer = null;
         this._isStartGame = false;
         for each(_loc1_ in this._playerList)
         {
            _loc1_.endGame();
         }
      }
      
      private function removeEvent() : void
      {
         removeEventListener(Event.ENTER_FRAME,this.updateMap);
         EscortManager.instance.removeEventListener(EscortManager.MOVE,this.moveHandler);
         EscortManager.instance.removeEventListener(EscortManager.REFRESH_ITEM,this.refreshItemHandler);
         EscortManager.instance.removeEventListener(EscortManager.REFRESH_BUFF,this.refreshBuffHandler);
         EscortManager.instance.removeEventListener(EscortManager.USE_SKILL,this.useSkillHandler);
         EscortManager.instance.removeEventListener(EscortManager.RE_ENTER_ALL_INFO,this.createPlayerHandler);
         EscortManager.instance.removeEventListener(EscortManager.FIGHT_STATE_CHANGE,this.playerFightStateChangeHandler);
         EscortManager.instance.removeEventListener(EscortManager.RANK_ARRIVE_LIST,this.rankArriveListChangeHandler);
      }
      
      public function dispose() : void
      {
         this.removeEvent();
         if(this._startMc)
         {
            this._startMc.gotoAndStop(2);
         }
         if(this._endMc)
         {
            this._endMc.gotoAndStop(2);
         }
         ObjectUtils.disposeAllChildren(this._mapLayer);
         ObjectUtils.disposeAllChildren(this._playerLayer);
         ObjectUtils.disposeAllChildren(this);
         this._mapLayer = null;
         this._playerLayer = null;
         this._playerList = null;
         this._selfPlayer = null;
         this._itemList = null;
         this._rankArriveList = null;
         if(this._mapBitmapData)
         {
            this._mapBitmapData.dispose();
         }
         this._mapBitmapData = null;
         this._runPercent = null;
         this._startMc = null;
         this._endMc = null;
         this._finishTag = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
