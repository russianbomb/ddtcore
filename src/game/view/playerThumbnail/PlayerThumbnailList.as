package game.view.playerThumbnail
{
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.ShowTipManager;
   import com.pickgliss.ui.core.Disposeable;
   import ddt.events.GameEvent;
   import ddt.manager.PlayerManager;
   import ddt.utils.PositionUtils;
   import ddt.view.tips.PlayerThumbnailTip;
   import fightFootballTime.manager.FightFootballTimeManager;
   import flash.display.DisplayObject;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.geom.Point;
   import game.GameManager;
   import game.model.GameInfo;
   import game.model.Player;
   import road7th.data.DictionaryData;
   import road7th.data.DictionaryEvent;
   import room.model.RoomInfo;
   import trainer.data.Step;
   
   public class PlayerThumbnailList extends Sprite implements Disposeable
   {
       
      
      private var _info:DictionaryData;
      
      private var _players:DictionaryData;
      
      private var _dirct:int;
      
      private var _gm:GameInfo;
      
      private var _index:String;
      
      private var _list:Array;
      
      private var _thumbnailTip:PlayerThumbnailTip;
      
      public function PlayerThumbnailList(param1:DictionaryData, param2:GameInfo, param3:int = 1)
      {
         super();
         this._dirct = param3;
         this._info = param1;
         this._gm = param2;
         this.initView();
         this.initEvents();
      }
      
      private function initView() : void
      {
         var _loc1_:int = 0;
         var _loc2_:String = null;
         var _loc3_:PlayerThumbnail = null;
         this._list = [];
         this._players = new DictionaryData();
         if(this._info)
         {
            _loc1_ = 0;
            for(this._index in this._info)
            {
               if(this._gm.gameMode != RoomInfo.TRANSNATIONALFIGHT_ROOM)
               {
                  _loc3_ = new PlayerThumbnail(this._info[_index],this._dirct);
                  _loc3_.addEventListener("playerThumbnailEvent",this.__onTipClick);
                  _loc3_.addEventListener(GameEvent.WISH_SELECT,this.__thumbnailHandle);
               }
               else
               {
                  _loc3_ = new PlayerThumbnail(this._info[_loc2_],this._dirct,false);
               }
               this._players.add(_loc2_,_loc3_);
               addChild(_loc3_);
               this._list.push(_loc3_);
            }
         }
         this.arrange();
      }
      
      private function __onTipClick(param1:Event) : void
      {
         var __addTip:Function = null;
         var e:Event = param1;
         __addTip = function(param1:Event):void
         {
            if((param1.currentTarget as PlayerThumbnailTip).tipDisplay)
            {
               (param1.currentTarget as PlayerThumbnailTip).tipDisplay.recoverTip();
            }
         };
         if(!PlayerManager.Instance.Self.IsWeakGuildFinish(Step.GAIN_TEN_PERSENT))
         {
            return;
         }
         this._thumbnailTip = ShowTipManager.Instance.getTipInstanceByStylename("ddt.view.tips.PlayerThumbnailTip") as PlayerThumbnailTip;
         if(this._thumbnailTip == null)
         {
            this._thumbnailTip = ShowTipManager.Instance.createTipByStyleName("ddt.view.tips.PlayerThumbnailTip");
            this._thumbnailTip.addEventListener("playerThumbnailTipItemClick",__addTip);
         }
         this._thumbnailTip.tipDisplay = e.currentTarget as PlayerThumbnail;
         this._thumbnailTip.x = this.mouseX;
         this._thumbnailTip.y = e.currentTarget.height + e.currentTarget.y + 12;
         PositionUtils.setPos(this._thumbnailTip,localToGlobal(new Point(this._thumbnailTip.x,this._thumbnailTip.y)));
         if(GameManager.Instance.Current.roomType != FightFootballTimeManager.FIGHTFOOTBALLTIME_ROOM)
         {
            LayerManager.Instance.addToLayer(this._thumbnailTip,LayerManager.GAME_DYNAMIC_LAYER,false);
         }
      }
      
      private function __thumbnailHandle(param1:GameEvent) : void
      {
         dispatchEvent(new GameEvent(GameEvent.WISH_SELECT,param1.data));
      }
      
      private function arrange() : void
      {
         var _loc2_:DisplayObject = null;
         var _loc1_:int = 0;
         while(_loc1_ < numChildren)
         {
            _loc2_ = getChildAt(_loc1_);
            if(this._dirct < 0)
            {
               _loc2_.x = (_loc1_ + 1) * (_loc2_.width + 4) * this._dirct;
            }
            else
            {
               _loc2_.x = _loc1_ * (_loc2_.width + 4) * this._dirct;
            }
            _loc1_++;
         }
      }
      
      private function initEvents() : void
      {
         this._info.addEventListener(DictionaryEvent.REMOVE,this.__removePlayer);
         this._info.addEventListener(DictionaryEvent.ADD,this.__addLiving);
      }
      
      private function removeEvents() : void
      {
         this._info.removeEventListener(DictionaryEvent.REMOVE,this.__removePlayer);
         this._info.removeEventListener(DictionaryEvent.ADD,this.__addLiving);
      }
      
      private function __addLiving(param1:DictionaryEvent) : void
      {
         var _loc2_:Player = null;
         var _loc3_:PlayerThumbnail = null;
         if(param1.data is Player)
         {
            _loc2_ = param1.data as Player;
            _loc3_ = new PlayerThumbnail(_loc2_,this._dirct);
            _loc3_.addEventListener("playerThumbnailEvent",this.__onTipClick);
            _loc3_.addEventListener(GameEvent.WISH_SELECT,this.__thumbnailHandle);
            this._players.add(String(_loc2_.playerInfo.ID),_loc3_);
            addChild(_loc3_);
            this._list.push(_loc3_);
            this.arrange();
         }
      }
      
      private function delePlayer(param1:int) : void
      {
         var _loc4_:PlayerThumbnail = null;
         var _loc2_:int = this._list.length;
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_)
         {
            _loc4_ = this._list[_loc3_] as PlayerThumbnail;
            if(_loc4_.info.ID == param1)
            {
               removeChild(_loc4_);
               this._list.splice(_loc3_,1);
               break;
            }
            _loc3_++;
         }
      }
      
      public function __removePlayer(param1:DictionaryEvent) : void
      {
         var _loc2_:Player = param1.data as Player;
         if(_loc2_ && _loc2_.playerInfo)
         {
            if(this._players[_loc2_.playerInfo.ID])
            {
               this._players[_loc2_.playerInfo.ID].removeEventListener("playerThumbnailEvent",this.__onTipClick);
               this._players[_loc2_.playerInfo.ID].removeEventListener(GameEvent.WISH_SELECT,this.__thumbnailHandle);
               this._players[_loc2_.playerInfo.ID].dispose();
               delete this._players[_loc2_.playerInfo.ID];
            }
         }
         this.arrange();
      }
      
      public function dispose() : void
      {
         var _loc1_:* = null;
         this.removeEvents();
         for(_loc1_ in this._players)
         {
            if(this._players[_loc1_])
            {
               this._players[_loc1_].removeEventListener("playerThumbnailEvent",this.__onTipClick);
               this._players[_loc1_].removeEventListener(GameEvent.WISH_SELECT,this.__thumbnailHandle);
               this._players[_loc1_].dispose();
            }
         }
         this._players = null;
         if(this._thumbnailTip)
         {
            this._thumbnailTip.tipDisplay = null;
         }
         this._thumbnailTip = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
