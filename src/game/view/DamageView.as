package game.view
{
   import com.greensock.TweenLite;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.text.FilterFrameText;
   import ddt.manager.LanguageMgr;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import game.GameManager;
   import game.model.Living;
   
   public class DamageView extends Sprite
   {
       
      
      private var _viewDamageBtn:BaseButton;
      
      private var _infoSprite:Sprite;
      
      private var _bg:Bitmap;
      
      private var _title:FilterFrameText;
      
      private var _listTxt:FilterFrameText;
      
      private var _userNameVec:Vector.<FilterFrameText>;
      
      private var _damageNumVec:Vector.<FilterFrameText>;
      
      private var _openFlag:Boolean = false;
      
      public function DamageView()
      {
         super();
         this.initView();
         this.initEvent();
      }
      
      private function initView() : void
      {
         var _loc2_:FilterFrameText = null;
         var _loc3_:FilterFrameText = null;
         this._viewDamageBtn = ComponentFactory.Instance.creatComponentByStylename("game.view.damageView.viewDamageBtn");
         addChild(this._viewDamageBtn);
         this._infoSprite = new Sprite();
         addChild(this._infoSprite);
         PositionUtils.setPos(this._infoSprite,"asset.game.damageView.SpritePos");
         this._bg = ComponentFactory.Instance.creat("asset.game.damageView.bg");
         this._infoSprite.addChild(this._bg);
         this._title = ComponentFactory.Instance.creatComponentByStylename("game.view.damageView.titleTxt");
         this._title.text = LanguageMgr.GetTranslation("ddt.game.view.damageView.titleText");
         this._infoSprite.addChild(this._title);
         this._listTxt = ComponentFactory.Instance.creatComponentByStylename("game.view.damageView.listTxt");
         this._listTxt.text = LanguageMgr.GetTranslation("ddt.game.view.damageView.listText");
         this._infoSprite.addChild(this._listTxt);
         this._userNameVec = new Vector.<FilterFrameText>();
         this._damageNumVec = new Vector.<FilterFrameText>();
         var _loc1_:int = 0;
         while(_loc1_ < 4)
         {
            _loc2_ = ComponentFactory.Instance.creatComponentByStylename("game.view.damageView.userInfo");
            PositionUtils.setPos(_loc2_,"game.view.damageView.userNamePos" + _loc1_);
            this._userNameVec.push(_loc2_);
            this._infoSprite.addChild(_loc2_);
            _loc3_ = ComponentFactory.Instance.creatComponentByStylename("game.view.damageView.userInfo");
            PositionUtils.setPos(_loc3_,"game.view.damageView.damageNumPos" + _loc1_);
            this._damageNumVec.push(_loc3_);
            this._infoSprite.addChild(_loc3_);
            _loc1_++;
         }
         this.updateView();
      }
      
      private function initEvent() : void
      {
         this._viewDamageBtn.addEventListener(MouseEvent.CLICK,this.__onMouseClick);
      }
      
      public function updateView() : void
      {
         var _loc2_:Living = null;
         var _loc1_:int = 0;
         this.clearTextInfo();
         for each(_loc2_ in GameManager.Instance.Current.livings)
         {
            if(_loc2_.isPlayer())
            {
               this._userNameVec[_loc1_].text = _loc2_.playerInfo.NickName;
               this._damageNumVec[_loc1_].text = _loc2_.damageNum.toString();
               _loc2_.damageNum = 0;
               _loc1_++;
            }
         }
      }
      
      private function clearTextInfo() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ < this._userNameVec.length)
         {
            this._userNameVec[_loc1_].text = "";
            this._damageNumVec[_loc1_].text = "";
            _loc1_++;
         }
      }
      
      protected function __onMouseClick(param1:MouseEvent) : void
      {
         if(this._openFlag)
         {
            TweenLite.to(this._infoSprite,0.3,{
               "x":0,
               "scaleX":1,
               "scaleY":1,
               "alpha":1
            });
         }
         else
         {
            TweenLite.to(this._infoSprite,0.5,{
               "x":this._viewDamageBtn.x + this._viewDamageBtn.width / 2,
               "scaleX":0,
               "scaleY":0,
               "alpha":0
            });
         }
         this._openFlag = !this._openFlag;
      }
      
      private function removeEvent() : void
      {
         this._viewDamageBtn.removeEventListener(MouseEvent.CLICK,this.__onMouseClick);
      }
      
      public function dispose() : void
      {
         this.removeEvent();
         if(this._viewDamageBtn)
         {
            this._viewDamageBtn.dispose();
            this._viewDamageBtn = null;
         }
         if(this._bg)
         {
            this._bg.bitmapData.dispose();
            this._bg = null;
         }
         if(this._title)
         {
            this._title.dispose();
            this._title = null;
         }
         if(this._listTxt)
         {
            this._listTxt.dispose();
            this._listTxt = null;
         }
         var _loc1_:int = 0;
         while(_loc1_ < 4)
         {
            this._userNameVec[_loc1_].dispose();
            this._userNameVec[_loc1_] = null;
            _loc1_++;
         }
         this._userNameVec.length = 0;
         this._userNameVec = null;
         var _loc2_:int = 0;
         while(_loc2_ < 4)
         {
            this._damageNumVec[_loc2_].dispose();
            this._damageNumVec[_loc2_] = null;
            _loc2_++;
         }
         this._damageNumVec.length = 0;
         this._damageNumVec = null;
      }
   }
}
