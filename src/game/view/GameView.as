package game.view
{
   import bagAndInfo.info.PlayerInfoViewControl;
   import campbattle.CampBattleManager;
   import christmas.controller.ChristmasRoomController;
   import christmas.manager.ChristmasManager;
   import com.pickgliss.loader.BaseLoader;
   import com.pickgliss.loader.LoadResourceManager;
   import com.pickgliss.loader.LoaderEvent;
   import com.pickgliss.loader.ModuleLoader;
   import com.pickgliss.manager.CacheSysManager;
   import com.pickgliss.toplevel.StageReferance;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ClassUtils;
   import com.pickgliss.utils.ObjectUtils;
   import consortion.ConsortionModelControl;
   import ddt.constants.CacheConsts;
   import ddt.data.BallInfo;
   import ddt.data.BuffType;
   import ddt.data.EquipType;
   import ddt.data.FightAchievModel;
   import ddt.data.FightBuffInfo;
   import ddt.data.PropInfo;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.data.player.PlayerInfo;
   import ddt.events.BagEvent;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.events.GameEvent;
   import ddt.events.LivingEvent;
   import ddt.events.PhyobjEvent;
   import ddt.loader.StartupResourceLoader;
   import ddt.manager.BallManager;
   import ddt.manager.BuffManager;
   import ddt.manager.ChatManager;
   import ddt.manager.GameInSocketOut;
   import ddt.manager.InviteManager;
   import ddt.manager.ItemManager;
   import ddt.manager.KeyboardShortcutsManager;
   import ddt.manager.LanguageMgr;
   import ddt.manager.LoadBombManager;
   import ddt.manager.LogManager;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PageInterfaceManager;
   import ddt.manager.PathManager;
   import ddt.manager.PetInfoManager;
   import ddt.manager.PetSkillManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerConfigManager;
   import ddt.manager.SkillManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.manager.StateManager;
   import ddt.manager.StatisticManager;
   import ddt.states.BaseStateView;
   import ddt.states.StateType;
   import ddt.utils.MenoryUtil;
   import ddt.utils.PositionUtils;
   import ddt.view.BackgoundView;
   import ddt.view.PropItemView;
   import ddt.view.character.CharactoryFactory;
   import ddt.view.character.GameCharacter;
   import ddt.view.character.ICharacter;
   import ddt.view.character.ShowCharacter;
   import escort.EscortManager;
   import fightFootballTime.manager.FightFootballTimeManager;
   import flash.display.Bitmap;
   import flash.display.DisplayObject;
   import flash.display.DisplayObjectContainer;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.events.TimerEvent;
   import flash.geom.Point;
   import flash.net.URLLoader;
   import flash.net.URLRequest;
   import flash.net.URLRequestMethod;
   import flash.net.URLVariables;
   import flash.system.ApplicationDomain;
   import flash.ui.Mouse;
   import flash.utils.Dictionary;
   import flash.utils.Timer;
   import flash.utils.getDefinitionByName;
   import flash.utils.getTimer;
   import flash.utils.setTimeout;
   import game.GameManager;
   import game.TryAgain;
   import game.actions.ChangeBallAction;
   import game.actions.ChangeNpcAction;
   import game.actions.ChangePlayerAction;
   import game.actions.GameOverAction;
   import game.actions.MissionOverAction;
   import game.actions.PickBoxAction;
   import game.actions.PrepareShootAction;
   import game.actions.ViewEachObjectAction;
   import game.model.GameInfo;
   import game.model.GameNeedMovieInfo;
   import game.model.Living;
   import game.model.LocalPlayer;
   import game.model.MissionAgainInfo;
   import game.model.Pet;
   import game.model.Player;
   import game.model.SimpleBoss;
   import game.model.SmallEnemy;
   import game.model.TurnedLiving;
   import game.objects.ActionType;
   import game.objects.ActivityDungeonNextView;
   import game.objects.AnimationObject;
   import game.objects.BombAction;
   import game.objects.GameLiving;
   import game.objects.GameLocalPlayer;
   import game.objects.GamePlayer;
   import game.objects.GameSimpleBoss;
   import game.objects.GameSmallEnemy;
   import game.objects.GameSysMsgType;
   import game.objects.SimpleBox;
   import game.objects.SimpleObject;
   import game.objects.TransmissionGate;
   import game.view.control.FightControlBar;
   import game.view.experience.ExpView;
   import hall.GameLoadingManager;
   import kingDivision.KingDivisionManager;
   import org.aswing.KeyStroke;
   import org.aswing.KeyboardManager;
   import pet.date.PetInfo;
   import pet.date.PetSkillTemplateInfo;
   import phy.object.PhysicalObj;
   import road7th.comm.PackageIn;
   import road7th.data.DictionaryData;
   import road7th.data.DictionaryEvent;
   import road7th.utils.AutoDisappear;
   import road7th.utils.MovieClipWrapper;
   import room.RoomManager;
   import room.model.RoomInfo;
   import room.model.RoomPlayer;
   import room.transnational.TransnationalFightManager;
   import sevenDouble.SevenDoubleManager;
   
   public class GameView extends GameViewBase
   {
       
      
      private const ZXC_OFFSET:int = 24;
      
      protected var _msg:String = "";
      
      protected var _tipItems:Dictionary;
      
      protected var _tipLayers:Sprite;
      
      protected var _result:ExpView;
      
      private var _gameLivingArr:Array;
      
      private var _gameLivingIdArr:Array;
      
      private var _objectDic:Dictionary;
      
      private var _evtArray:Array;
      
      private var _evtFuncArray:Array;
      
      private var _animationArray:Array;
      
      private var nextBg:Bitmap;
      
      private var nextPlayerTxtRed:FilterFrameText;
      
      private var nextPlayerTxtBlue:FilterFrameText;
      
      private var _campBattleTerrace:Bitmap;
      
      private var _terraces:Dictionary;
      
      private var _firstEnter:Boolean = false;
      
      private var numCh:Number;
      
      private var _soundPlayFlag:Boolean;
      
      private var _ignoreSmallEnemy:Boolean;
      
      private var _boxArr:Array;
      
      private var finished:int = 0;
      
      private var total:int = 0;
      
      private var props:Array;
      
      private var _logTimer:Timer;
      
      private var _missionAgain:MissionAgainInfo;
      
      protected var _expView:ExpView;
      
      private var _isPVPover:Boolean;
      
      public function GameView()
      {
         this.props = [10467,10468,10469];
         this._evtArray = [GameManager.Instance.addLivingEvtVec,GameManager.Instance.setPropertyEvtVec,GameManager.Instance.livingFallingEvtVec,GameManager.Instance.livingShowBloodEvtVec];
         this._evtFuncArray = [this.addliving,this.objectSetProperty,this.livingFalling,this.livingShowBlood];
         super();
         Mouse.show();
      }
      
      private function loadResource() : void
      {
         var _loc1_:int = 0;
         if(!StartupResourceLoader.firstEnterHall)
         {
            _loc1_ = 0;
            while(_loc1_ < _gameInfo.neededMovies.length)
            {
               if(_gameInfo.neededMovies[_loc1_].type == 2)
               {
                  _gameInfo.neededMovies[_loc1_].addEventListener(LoaderEvent.COMPLETE,this.__loaderComplete);
                  _gameInfo.neededMovies[_loc1_].startLoad();
               }
               _loc1_++;
            }
         }
      }
      
      private function __loaderComplete(param1:LoaderEvent) : void
      {
         var _loc3_:SimpleObject = null;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc2_:Boolean = false;
         param1.target.removeEventListener(LoaderEvent.COMPLETE,this.__loaderComplete);
         for each(_loc3_ in this._objectDic)
         {
            if(_loc3_.shouldReCreate)
            {
               _loc3_.createMovieAfterLoadComplete();
            }
         }
         if(this._gameLivingArr && this._gameLivingIdArr)
         {
            _loc4_ = 0;
            while(_loc4_ < this._gameLivingArr.length)
            {
               (this._gameLivingArr[_loc4_] as GameLiving).replaceMovie();
               _loc5_ = 0;
               while(_loc5_ < this._gameLivingIdArr.length)
               {
                  if(this._gameLivingArr[_loc4_].Id == this._gameLivingIdArr[_loc5_])
                  {
                     _loc2_ = true;
                     break;
                  }
                  _loc5_++;
               }
               _playerThumbnailLController.updateHeadFigure(this._gameLivingArr[_loc4_],_loc2_);
               _loc4_++;
            }
         }
      }
      
      override public function enter(param1:BaseStateView, param2:Object = null) : void
      {
         var _loc5_:int = 0;
         GameManager.Instance.gameView = this;
         this._gameLivingArr = new Array();
         this._gameLivingIdArr = new Array();
         this._objectDic = new Dictionary();
         this._animationArray = new Array();
         GameLoadingManager.Instance.hide();
         super.enter(param1,param2);
         if(GameManager.Instance.Current && GameManager.Instance.Current.roomType == FightFootballTimeManager.FIGHTFOOTBALLTIME_ROOM)
         {
            this.initShowNextPlayer();
         }
         InviteManager.Instance.enabled = false;
         this.loadResource();
         KeyboardManager.getInstance().isStopDispatching = false;
         KeyboardShortcutsManager.Instance.forbiddenSection(KeyboardShortcutsManager.GAME,false);
         _gameInfo.resetResultCard();
         _gameInfo.livings.addEventListener(DictionaryEvent.REMOVE,this.__removePlayer);
         _gameInfo.addEventListener(GameEvent.WIND_CHANGED,this.__windChanged);
         GameManager.Instance.addEventListener(LoaderEvent.COMPLETE,this.__loaderComplete);
         PlayerManager.Instance.Self.FightBag.addEventListener(BagEvent.UPDATE,this.__selfObtainItem);
         PlayerManager.Instance.Self.TempBag.addEventListener(BagEvent.UPDATE,this.__getTempItem);
         GameManager.Instance.addEventListener(GameEvent.ADDTERRACE,this.addTerraceHander);
         GameManager.Instance.addEventListener(GameEvent.DELTERRACE,this.delTerraceHander);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.MISSION_OVE,this.__missionOver);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GAME_OVER,this.__gameOver);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PLAYER_SHOOT,this.__shoot);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PLAYER_START_MOVE,this.__startMove);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PLAYER_CHANGE,this.__playerChange);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PLAYER_BLOOD,this.__playerBlood);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PLAYER_VANE,this.__changWind);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PLAYER_HIDE,this.__playerHide);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PLAYER_NONOLE,this.__playerNoNole);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PLAYER_PROP,this.__playerUsingItem);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PLAYER_DANDER,this.__dander);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.REDUCE_DANDER,this.__reduceDander);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PLAYER_ADDATTACK,this.__changeShootCount);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.SUICIDE,this.__suicide);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PLAYER_SHOOT_TAG,this.__beginShoot);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.CHANGE_BALL,this.__changeBall);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PLAYER_FROST,this.__forstPlayer);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PLAY_MOVIE,this.__playMovie);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.LIVING_CHAGEANGLE,this.__livingTurnRotation);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PLAY_SOUND,this.__playSound);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.LIVING_MOVETO,this.__livingMoveto);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.LIVING_JUMP,this.__livingJump);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.LIVING_BEAT,this.__livingBeat);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.LIVING_SAY,this.__livingSay);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.LIVING_RANGEATTACKING,this.__livingRangeAttacking);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.DIRECTION_CHANGED,this.__livingDirChanged);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.FOCUS_ON_OBJECT,this.__focusOnObject);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.CHANGE_STATE,this.__changeState);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.BARRIER_INFO,this.__barrierInfoHandler);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.ADD_MAP_THINGS,this.__addMapThing);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.UPDATE_BOARD_STATE,this.__updatePhysicObject);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.BOX_DISAPPEAR,this.__removePhysicObject);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.ADD_TIP_LAYER,this.__addTipLayer);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.FORBID_DRAG,this.__forbidDragFocus);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.TOP_LAYER,this.__topLayer);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.CONTROL_BGM,this.__controlBGM);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.LIVING_BOLTMOVE,this.__onLivingBoltmove);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.CHANGE_TARGET,this.__onChangePlayerTarget);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.ACTION_MAPPING,this.__livingActionMapping);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.FIGHT_ACHIEVEMENT,this.__fightAchievement);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.UPDATE_BUFF,this.__updateBuff);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PET_BUFF,this.__updatePetBuff);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GAMESYSMESSAGE,this.__gameSysMessage);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.USE_PET_SKILL,this.__usePetSkill);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PICK_BOX,this.__pickBox);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GAME_IN_COLOR_CHANGE,this.__livingSmallColorChange);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GAME_TRUSTEESHIP,this.__gameTrusteeship);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GAME_REVIVE,this.__revivePlayer);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.FIGHT_STATUS,this.__fightStatusChange);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.SKIPNEXT,this.__skipNextHandler);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.CLEAR_DEBUFF,this.__clearDebuff);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.ADD_ANIMATION,this.__addAnimation);
         StatisticManager.Instance().startAction(StatisticManager.GAME,"yes");
         this._tipItems = new Dictionary(true);
         CacheSysManager.lock(CacheConsts.ALERT_IN_FIGHT);
         PlayerManager.Instance.Self.isUpGradeInGame = false;
         BackgoundView.Instance.hide();
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.FIGHTFOOTBALLTIME_TAKEOUTALL,this.__takeoutAll);
         GameManager.Instance.Current.addEventListener("showNextPlayer",this._showNextPlayer);
         KeyboardManager.getInstance().registerKeyAction(KeyStroke.VK_END,this.__turnEnd);
         KeyboardManager.getInstance().registerKeyAction(KeyStroke.VK_HOME,this.__turnHome);
         if(StartupResourceLoader.firstEnterHall)
         {
            this._firstEnter = true;
            StartupResourceLoader.Instance.addThirdGameUI();
            StartupResourceLoader.Instance.startLoadRelatedInfo();
         }
         else
         {
            this._firstEnter = false;
            BuffManager.startLoadBuffEffect();
         }
         this._terraces = new Dictionary();
         if(GameManager.Instance.isAddTerrce)
         {
            this.addTerrce(GameManager.Instance.terrceX,GameManager.Instance.terrceY,GameManager.Instance.terrceID);
         }
         var _loc3_:int = RoomManager.Instance.current.type;
         if(this.guideTip() && _loc3_ != RoomInfo.DUNGEON_ROOM && StateManager.currentStateType != StateType.FIGHT_LIB_GAMEVIEW && _loc3_ != RoomInfo.ACADEMY_DUNGEON_ROOM && !this.isNewHand())
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("tank.MessageTip.AutoGuidFightBegin"));
         }
         GameManager.Instance.viewCompleteFlag = true;
         var _loc4_:int = 0;
         while(_loc4_ < this._evtFuncArray.length)
         {
            _loc5_ = 0;
            while(_loc5_ < this._evtArray[_loc4_].length)
            {
               this._evtFuncArray[_loc4_](this._evtArray[_loc4_][_loc5_]);
               _loc5_++;
            }
            this._evtArray[_loc4_].length = 0;
            _loc4_++;
         }
      }
      
      private function __turnEnd() : void
      {
      }
      
      private function __turnHome() : void
      {
      }
      
      private function addTerraceHander(param1:GameEvent) : void
      {
         this.addTerrce(param1.data[0],param1.data[1],param1.data[2]);
      }
      
      private function addTerrce(param1:int, param2:int, param3:int) : void
      {
         if(this._terraces[param3])
         {
            return;
         }
         var _loc4_:Bitmap = ComponentFactory.Instance.creat("camp.battle.terrace");
         _loc4_.x = param1 - _loc4_.width / 2;
         _loc4_.y = param2;
         _map.addChild(_loc4_);
         this._terraces[param3] = _loc4_;
      }
      
      private function delTerraceHander(param1:GameEvent) : void
      {
         var _loc2_:Bitmap = this._terraces[param1.data[0]];
         if(_loc2_)
         {
            _loc2_.bitmapData.dispose();
            _loc2_ = null;
            delete this._terraces[param1.data[0]];
         }
      }
      
      private function initShowNextPlayer() : void
      {
         this.nextBg = ComponentFactory.Instance.creatBitmap("fightFootballTime.gameView.nextPlayer");
         this.nextPlayerTxtBlue = ComponentFactory.Instance.creatComponentByStylename("fightFootballTime.gameView.nextPlayerBlue");
         this.nextPlayerTxtRed = ComponentFactory.Instance.creatComponentByStylename("fightFootballTime.gameView.nextPlayerRed");
         this.nextBg.visible = false;
         this.nextPlayerTxtBlue.visible = false;
         this.nextPlayerTxtRed.visible = false;
         addChild(this.nextBg);
         addChild(this.nextPlayerTxtBlue);
         addChild(this.nextPlayerTxtRed);
      }
      
      private function _showNextPlayer(param1:Event) : void
      {
         var _loc5_:String = null;
         var _loc6_:int = 0;
         var _loc2_:GameInfo = param1.currentTarget as GameInfo;
         var _loc3_:int = _loc2_.nextPlayerId;
         var _loc4_:int = _loc2_.currentPlayerId;
         if(_loc3_ == 0)
         {
            this.nextPlayerTxtBlue.visible = false;
            this.nextPlayerTxtRed.visible = false;
            this.nextBg.visible = false;
            return;
         }
         _loc5_ = _loc2_.findPlayerByPlayerID(_loc3_).playerInfo.NickName;
         _loc6_ = _loc2_.findPlayerByPlayerID(_loc3_).team;
         if(_loc4_ == _loc2_.selfGamePlayer.playerInfo.ID)
         {
            this.nextPlayerTxtBlue.visible = false;
            this.nextPlayerTxtRed.visible = false;
            this.nextBg.visible = false;
         }
         else if(_loc6_ == 1)
         {
            this.nextPlayerTxtBlue.text = _loc5_;
            this.nextPlayerTxtBlue.visible = true;
            this.nextPlayerTxtRed.visible = false;
            this.nextBg.visible = true;
         }
         else if(_loc6_ == 2)
         {
            this.nextPlayerTxtRed.text = _loc5_;
            this.nextPlayerTxtRed.visible = true;
            this.nextPlayerTxtBlue.visible = false;
            this.nextBg.visible = true;
         }
      }
      
      private function __takeoutAll(param1:CrazyTankSocketEvent) : void
      {
         if(FightFootballTimeManager.instance.takeoutAll)
         {
         }
      }
      
      private function dioposeTerraces() : void
      {
         var _loc1_:Bitmap = null;
         for each(_loc1_ in this._terraces)
         {
            if(_loc1_)
            {
               _loc1_.bitmapData.dispose();
            }
            _loc1_ = null;
         }
         this._terraces = null;
      }
      
      private function retrunPlayer(param1:int) : GamePlayer
      {
         var _loc2_:GamePlayer = null;
         for each(_loc2_ in _players)
         {
            if(_loc2_.info.LivingID == param1)
            {
               return _loc2_;
            }
         }
         return null;
      }
      
      private function addNewPlayerHander(param1:CrazyTankSocketEvent) : void
      {
         var _loc39_:Boolean = false;
         var _loc40_:int = 0;
         var _loc41_:String = null;
         var _loc42_:String = null;
         var _loc43_:String = null;
         var _loc44_:int = 0;
         var _loc45_:String = null;
         var _loc46_:String = null;
         var _loc47_:int = 0;
         var _loc48_:PetInfo = null;
         var _loc49_:int = 0;
         var _loc50_:int = 0;
         var _loc51_:int = 0;
         var _loc52_:int = 0;
         var _loc53_:int = 0;
         var _loc54_:int = 0;
         var _loc55_:FightBuffInfo = null;
         var _loc56_:int = 0;
         var _loc57_:FightBuffInfo = null;
         var _loc58_:String = null;
         var _loc59_:String = null;
         var _loc2_:GameInfo = GameManager.Instance.Current;
         var _loc3_:PackageIn = param1.pkg;
         var _loc4_:int = _loc3_.readInt();
         var _loc5_:String = _loc3_.readUTF();
         var _loc6_:int = _loc3_.readInt();
         var _loc7_:PlayerInfo = new PlayerInfo();
         _loc7_.beginChanges();
         var _loc8_:RoomPlayer = RoomManager.Instance.current.findPlayerByID(_loc6_,_loc4_);
         if(_loc8_ == null)
         {
            _loc8_ = new RoomPlayer(_loc7_);
         }
         _loc7_.ID = _loc6_;
         _loc7_.ZoneID = _loc4_;
         var _loc9_:String = _loc3_.readUTF();
         var _loc10_:Boolean = _loc3_.readBoolean();
         if(_loc10_ && _loc8_.place < 8)
         {
            _loc8_.place = 8;
         }
         _loc7_.NickName = _loc9_;
         _loc7_.typeVIP = _loc3_.readByte();
         _loc7_.VIPLevel = _loc3_.readInt();
         if(PlayerManager.Instance.isChangeStyleTemp(_loc7_.ID))
         {
            _loc3_.readBoolean();
            _loc3_.readInt();
            _loc3_.readUTF();
            _loc3_.readUTF();
            _loc3_.readUTF();
         }
         else
         {
            _loc39_ = _loc3_.readBoolean();
            _loc40_ = _loc3_.readInt();
            _loc41_ = _loc3_.readUTF();
            _loc42_ = _loc3_.readUTF();
            _loc43_ = _loc3_.readUTF();
            _loc7_.Sex = _loc39_;
            _loc7_.Hide = _loc40_;
            _loc7_.Style = _loc41_;
            _loc7_.Colors = _loc42_;
            _loc7_.Skin = _loc43_;
         }
         _loc7_.Grade = _loc3_.readInt();
         _loc7_.Repute = _loc3_.readInt();
         _loc7_.WeaponID = _loc3_.readInt();
         if(_loc7_.WeaponID != 0)
         {
            _loc44_ = _loc3_.readInt();
            _loc45_ = _loc3_.readUTF();
            _loc46_ = _loc3_.readDateString();
         }
         _loc7_.DeputyWeaponID = _loc3_.readInt();
         _loc7_.pvpBadgeId = _loc3_.readInt();
         _loc7_.Nimbus = _loc3_.readInt();
         _loc7_.IsShowConsortia = _loc3_.readBoolean();
         _loc7_.ConsortiaID = _loc3_.readInt();
         _loc7_.ConsortiaName = _loc3_.readUTF();
         _loc7_.badgeID = _loc3_.readInt();
         var _loc11_:int = _loc3_.readInt();
         var _loc12_:int = _loc3_.readInt();
         _loc7_.WinCount = _loc3_.readInt();
         _loc7_.TotalCount = _loc3_.readInt();
         _loc7_.FightPower = _loc3_.readInt();
         _loc7_.apprenticeshipState = _loc3_.readInt();
         _loc7_.masterID = _loc3_.readInt();
         _loc7_.setMasterOrApprentices(_loc3_.readUTF());
         _loc7_.AchievementPoint = _loc3_.readInt();
         _loc7_.honor = _loc3_.readUTF();
         _loc7_.Offer = _loc3_.readInt();
         _loc7_.DailyLeagueFirst = _loc3_.readBoolean();
         _loc7_.DailyLeagueLastScore = _loc3_.readInt();
         _loc7_.commitChanges();
         _loc8_.playerInfo.IsMarried = _loc3_.readBoolean();
         if(_loc8_.playerInfo.IsMarried)
         {
            _loc8_.playerInfo.SpouseID = _loc3_.readInt();
            _loc8_.playerInfo.SpouseName = _loc3_.readUTF();
         }
         _loc8_.additionInfo.resetAddition();
         _loc8_.additionInfo.GMExperienceAdditionType = Number(_loc3_.readInt() / 100);
         _loc8_.additionInfo.AuncherExperienceAddition = Number(_loc3_.readInt() / 100);
         _loc8_.additionInfo.GMOfferAddition = Number(_loc3_.readInt() / 100);
         _loc8_.additionInfo.AuncherOfferAddition = Number(_loc3_.readInt() / 100);
         _loc8_.additionInfo.GMRichesAddition = Number(_loc3_.readInt() / 100);
         _loc8_.additionInfo.AuncherRichesAddition = Number(_loc3_.readInt() / 100);
         _loc8_.team = _loc3_.readInt();
         _loc2_.addRoomPlayer(_loc8_);
         var _loc13_:int = _loc3_.readInt();
         var _loc14_:int = _loc3_.readInt();
         var _loc15_:Player = new Player(_loc8_.playerInfo,_loc13_,_loc8_.team,_loc14_);
         var _loc16_:int = _loc3_.readInt();
         var _loc17_:int = 0;
         while(_loc17_ < _loc16_)
         {
            _loc47_ = _loc3_.readInt();
            _loc48_ = _loc7_.pets[_loc47_];
            _loc49_ = _loc3_.readInt();
            if(_loc48_ == null)
            {
               _loc48_ = new PetInfo();
               _loc48_.TemplateID = _loc49_;
               PetInfoManager.fillPetInfo(_loc48_);
            }
            _loc48_.ID = _loc3_.readInt();
            _loc48_.Name = _loc3_.readUTF();
            _loc48_.UserID = _loc3_.readInt();
            _loc48_.Level = _loc3_.readInt();
            _loc48_.IsEquip = true;
            _loc48_.clearEquipedSkills();
            _loc50_ = _loc3_.readInt();
            _loc51_ = 0;
            while(_loc51_ < _loc50_)
            {
               _loc52_ = _loc3_.readInt();
               _loc53_ = _loc3_.readInt();
               _loc48_.equipdSkills.add(_loc52_,_loc53_);
               _loc51_++;
            }
            _loc48_.Place = _loc47_;
            _loc7_.pets.add(_loc48_.Place,_loc48_);
            _loc17_++;
         }
         _loc15_.zoneName = _loc5_;
         _loc15_.currentWeapInfo.refineryLevel = _loc44_;
         if(!_loc8_.isViewer)
         {
            _loc2_.addGamePlayer(_loc15_);
         }
         else
         {
            if(_loc8_.isSelf)
            {
               _loc2_.setSelfGamePlayer(_loc15_);
            }
            _loc2_.addGameViewer(_loc15_);
         }
         var _loc18_:int = _loc3_.readInt();
         var _loc19_:int = _loc3_.readInt();
         var _loc20_:int = _loc3_.readInt();
         _loc15_.pos = new Point(_loc19_,_loc20_);
         _loc15_.energy = 1;
         _loc15_.direction = _loc3_.readInt();
         var _loc21_:int = _loc3_.readInt();
         var _loc22_:int = _loc3_.readInt();
         _loc15_.team = _loc3_.readInt();
         var _loc23_:int = _loc3_.readInt();
         if(_loc15_ is LocalPlayer)
         {
            (_loc15_ as LocalPlayer).deputyWeaponCount = _loc3_.readInt();
         }
         else
         {
            _loc54_ = _loc3_.readInt();
         }
         _loc15_.powerRatio = _loc3_.readInt();
         _loc15_.dander = _loc3_.readInt();
         _loc15_.maxBlood = _loc22_;
         _loc15_.updateBlood(_loc14_,0,0);
         _loc15_.wishKingCount = _loc3_.readInt();
         _loc15_.wishKingEnergy = _loc3_.readInt();
         _loc15_.currentWeapInfo.refineryLevel = _loc23_;
         var _loc24_:int = _loc3_.readInt();
         var _loc25_:int = 0;
         while(_loc25_ < _loc24_)
         {
            _loc55_ = BuffManager.creatBuff(_loc3_.readInt());
            _loc56_ = _loc3_.readInt();
            if(_loc55_)
            {
               _loc55_.data = _loc56_;
               _loc15_.addBuff(_loc55_);
            }
            _loc25_++;
         }
         var _loc26_:int = _loc3_.readInt();
         var _loc27_:Vector.<FightBuffInfo> = new Vector.<FightBuffInfo>();
         var _loc28_:int = 0;
         while(_loc28_ < _loc26_)
         {
            _loc57_ = BuffManager.creatBuff(_loc3_.readInt());
            _loc15_.outTurnBuffs.push(_loc57_);
            _loc28_++;
         }
         var _loc29_:Boolean = _loc3_.readBoolean();
         var _loc30_:Boolean = _loc3_.readBoolean();
         var _loc31_:Boolean = _loc3_.readBoolean();
         var _loc32_:int = _loc3_.readInt();
         var _loc33_:Dictionary = new Dictionary();
         var _loc34_:int = 0;
         while(_loc34_ < _loc32_)
         {
            _loc58_ = _loc3_.readUTF();
            _loc59_ = _loc3_.readUTF();
            _loc33_[_loc58_] = _loc59_;
            _loc34_++;
         }
         if(_loc29_)
         {
            _loc29_ = false;
         }
         _loc15_.isFrozen = _loc29_;
         _loc15_.isHidden = _loc30_;
         _loc15_.isNoNole = _loc31_;
         _loc15_.outProperty = _loc33_;
         if(RoomManager.Instance.current.type != 5 && _loc15_.playerInfo.currentPet)
         {
            _loc15_.currentPet = new Pet(_loc15_.playerInfo.currentPet);
            this.petResLoad(_loc15_.playerInfo.currentPet);
         }
         var _loc35_:Array = [_loc8_];
         LoadBombManager.Instance.loadFullRoomPlayersBomb(_loc35_);
         var _loc36_:ICharacter = CharactoryFactory.createCharacter(_loc15_.playerInfo,"game");
         _loc36_.show();
         var _loc37_:ICharacter = CharactoryFactory.createCharacter(_loc15_.playerInfo,"show");
         ShowCharacter(_loc37_).show();
         _loc15_.character = ShowCharacter(_loc37_);
         var _loc38_:GamePlayer = new GamePlayer(_loc15_,_loc15_.character,GameCharacter(_loc36_));
         _map.addPhysical(_loc38_);
         _players[_loc15_] = _loc38_;
         _playerThumbnailLController.addNewLiving(_loc15_);
      }
      
      private function petResLoad(param1:PetInfo) : void
      {
         var _loc2_:int = 0;
         var _loc3_:PetSkillTemplateInfo = null;
         var _loc4_:BallInfo = null;
         if(param1)
         {
            LoadResourceManager.Instance.creatAndStartLoad(PathManager.solvePetGameAssetUrl(param1.GameAssetUrl),BaseLoader.MODULE_LOADER);
            for each(_loc2_ in param1.equipdSkills)
            {
               if(_loc2_ > 0)
               {
                  _loc3_ = PetSkillManager.getSkillByID(_loc2_);
                  if(_loc3_.EffectPic)
                  {
                     LoadResourceManager.Instance.creatAndStartLoad(PathManager.solvePetSkillEffect(_loc3_.EffectPic),BaseLoader.MODULE_LOADER);
                  }
                  if(_loc3_.NewBallID != -1)
                  {
                     _loc4_ = BallManager.findBall(_loc3_.NewBallID);
                     _loc4_.loadBombAsset();
                     _loc4_.loadCraterBitmap();
                  }
               }
            }
         }
      }
      
      protected function __pickBox(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:Array = [];
         var _loc4_:int = _loc2_.readInt();
         var _loc5_:int = 0;
         while(_loc5_ < _loc4_)
         {
            _loc3_.push(_loc2_.readInt());
            _loc5_++;
         }
         _map.dropOutBox(_loc3_);
         this.hideAllOther();
      }
      
      private function guideTip() : Boolean
      {
         var _loc2_:Living = null;
         var _loc3_:Player = null;
         if(RoomManager.Instance.current.type == RoomInfo.DUNGEON_ROOM)
         {
            return false;
         }
         var _loc1_:DictionaryData = GameManager.Instance.Current.livings;
         if(!_loc1_)
         {
            return false;
         }
         for each(_loc2_ in _loc1_)
         {
            _loc3_ = _loc2_ as Player;
            if(_loc3_ && (_loc2_ as Player).playerInfo.Grade <= 15)
            {
               return true;
            }
         }
         return false;
      }
      
      private function isNewHand() : Boolean
      {
         var _loc1_:int = RoomManager.Instance.current.mapId;
         if(_loc1_ == 112 || _loc1_ == 113 || _loc1_ == 114 || _loc1_ == 115 || _loc1_ == 116)
         {
            return true;
         }
         return false;
      }
      
      private function __gameSysMessage(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readInt();
         var _loc4_:String = _loc2_.readUTF();
         var _loc5_:int = _loc2_.readInt();
         switch(_loc3_)
         {
            case GameSysMsgType.GET_ITEM_INVENTORY_FULL:
               MessageTipManager.getInstance().show(String(_loc5_),2);
         }
      }
      
      private function __fightAchievement(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:AchieveAnimation = null;
         var _loc3_:PackageIn = null;
         var _loc4_:Living = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:AchieveAnimation = null;
         if(PathManager.getFightAchieveEnable())
         {
            if(_achievBar == null)
            {
               _achievBar = ComponentFactory.Instance.creatCustomObject("FightAchievBar");
               addChild(_achievBar);
            }
            _loc3_ = param1.pkg;
            _loc4_ = GameManager.Instance.Current.findLiving(_loc3_.clientId);
            _loc5_ = _loc3_.readInt();
            _loc6_ = _loc3_.readInt();
            _loc7_ = _loc3_.readInt();
            _loc8_ = getTimer();
            _loc9_ = _achievBar.getAnimate(_loc5_);
            if(_loc9_ == null)
            {
               _achievBar.addAnimate(ComponentFactory.Instance.creatCustomObject("AchieveAnimation",[_loc5_,_loc6_,_loc7_,_loc8_]));
            }
            else if(FightAchievModel.getInstance().isNumAchiev(_loc5_))
            {
               _loc9_.setNum(_loc6_);
            }
            else
            {
               _achievBar.rePlayAnimate(_loc9_);
            }
         }
      }
      
      private function onClick(param1:MouseEvent) : void
      {
         var _loc3_:DisplayObject = null;
         this.numCh = 0;
         var _loc2_:int = 0;
         while(_loc2_ < stage.numChildren)
         {
            _loc3_ = StageReferance.stage.getChildAt(_loc2_);
            _loc3_.visible = true;
            this.numCh++;
            if(_loc3_ is DisplayObjectContainer)
            {
               this.show(DisplayObjectContainer(_loc3_));
            }
            _loc2_++;
         }
      }
      
      private function show(param1:DisplayObjectContainer) : void
      {
         var _loc3_:DisplayObject = null;
         var _loc2_:int = 0;
         while(_loc2_ < param1.numChildren)
         {
            _loc3_ = param1.getChildAt(_loc2_);
            _loc3_.visible = true;
            this.numCh++;
            if(_loc3_ is DisplayObjectContainer)
            {
               this.show(DisplayObjectContainer(_loc3_));
            }
            _loc2_++;
         }
      }
      
      private function __windChanged(param1:GameEvent) : void
      {
         _map.wind = param1.data.wind;
         _vane.update(_map.wind,param1.data.isSelfTurn,param1.data.windNumArr);
      }
      
      override public function getType() : String
      {
         return StateType.FIGHTING;
      }
      
      override public function leaving(param1:BaseStateView) : void
      {
         var _loc2_:SimpleObject = null;
         var _loc3_:SimpleObject = null;
         var _loc4_:int = 0;
         GameManager.Instance.viewCompleteFlag = false;
         InviteManager.Instance.enabled = true;
         StageReferance.stage.removeEventListener(MouseEvent.CLICK,this.onClick);
         SoundManager.instance.stopMusic();
         PageInterfaceManager.restorePageTitle();
         KeyboardShortcutsManager.Instance.forbiddenSection(KeyboardShortcutsManager.GAME,true);
         if(PlayerManager.Instance.hasTempStyle)
         {
            PlayerManager.Instance.readAllTempStyleEvent();
         }
         _gameInfo.removeEventListener(GameEvent.WIND_CHANGED,this.__windChanged);
         _gameInfo.livings.removeEventListener(DictionaryEvent.REMOVE,this.__removePlayer);
         _gameInfo.removeAllMonsters();
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.PLAYER_SHOOT,this.__shoot);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.PLAYER_START_MOVE,this.__startMove);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.PLAYER_CHANGE,this.__playerChange);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.PLAYER_BLOOD,this.__playerBlood);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.PLAYER_VANE,this.__changWind);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.PLAYER_HIDE,this.__playerHide);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.PLAYER_NONOLE,this.__playerNoNole);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.PLAYER_PROP,this.__playerUsingItem);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.PLAYER_DANDER,this.__dander);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.REDUCE_DANDER,this.__reduceDander);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.PLAYER_ADDATTACK,this.__changeShootCount);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.SUICIDE,this.__suicide);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.PLAYER_SHOOT_TAG,this.__beginShoot);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.CHANGE_BALL,this.__changeBall);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.PLAYER_FROST,this.__forstPlayer);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.MISSION_OVE,this.__missionOver);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.GAME_OVER,this.__gameOver);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.PLAY_MOVIE,this.__playMovie);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.LIVING_CHAGEANGLE,this.__livingTurnRotation);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.PLAY_SOUND,this.__playSound);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.LIVING_MOVETO,this.__livingMoveto);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.LIVING_JUMP,this.__livingJump);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.LIVING_BEAT,this.__livingBeat);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.LIVING_SAY,this.__livingSay);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.LIVING_RANGEATTACKING,this.__livingRangeAttacking);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.DIRECTION_CHANGED,this.__livingDirChanged);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.FOCUS_ON_OBJECT,this.__focusOnObject);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.CHANGE_STATE,this.__changeState);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.BARRIER_INFO,this.__barrierInfoHandler);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.ADD_MAP_THINGS,this.__addMapThing);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.UPDATE_BOARD_STATE,this.__updatePhysicObject);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.BOX_DISAPPEAR,this.__removePhysicObject);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.ADD_TIP_LAYER,this.__addTipLayer);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.FORBID_DRAG,this.__forbidDragFocus);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.TOP_LAYER,this.__topLayer);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.CONTROL_BGM,this.__controlBGM);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.LIVING_BOLTMOVE,this.__onLivingBoltmove);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.CHANGE_TARGET,this.__onChangePlayerTarget);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.ACTION_MAPPING,this.__livingActionMapping);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.FIGHT_ACHIEVEMENT,this.__fightAchievement);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.UPDATE_BUFF,this.__updateBuff);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.PET_BUFF,this.__updatePetBuff);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.GAMESYSMESSAGE,this.__gameSysMessage);
         PlayerManager.Instance.Self.FightBag.removeEventListener(BagEvent.UPDATE,this.__selfObtainItem);
         PlayerManager.Instance.Self.TempBag.removeEventListener(BagEvent.UPDATE,this.__getTempItem);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.USE_PET_SKILL,this.__usePetSkill);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.PICK_BOX,this.__pickBox);
         GameManager.Instance.removeEventListener(GameEvent.ADDTERRACE,this.addTerraceHander);
         GameManager.Instance.removeEventListener(GameEvent.DELTERRACE,this.delTerraceHander);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.GAME_IN_COLOR_CHANGE,this.__livingSmallColorChange);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.GAME_TRUSTEESHIP,this.__gameTrusteeship);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.GAME_IN_COLOR_CHANGE,this.__revivePlayer);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.FIGHT_STATUS,this.__fightStatusChange);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.SKIPNEXT,this.__skipNextHandler);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.CLEAR_DEBUFF,this.__clearDebuff);
         for each(_loc2_ in this._tipItems)
         {
            delete this._tipLayers[_loc2_.Id];
            _loc2_.dispose();
            _loc2_ = null;
         }
         this._tipItems = null;
         if(this._tipLayers)
         {
            if(this._tipLayers.parent)
            {
               this._tipLayers.parent.removeChild(this._tipLayers);
            }
         }
         this._tipLayers = null;
         _gameInfo.resetBossCardCnt();
         if(this._expView)
         {
            this._expView.removeEventListener(GameEvent.EXPSHOWED,this.__expShowed);
         }
         super.leaving(param1);
         if(StateManager.isExitRoom(param1.getType()) && RoomManager.Instance.isReset(RoomManager.Instance.current.type))
         {
            GameManager.Instance.reset();
            RoomManager.Instance.reset();
         }
         else if(StateManager.isExitGame(param1.getType()) && RoomManager.Instance.isReset(RoomManager.Instance.current.type))
         {
            GameManager.Instance.reset();
         }
         BallManager.clearAsset();
         BackgoundView.Instance.show();
         PlayerInfoViewControl._isBattle = false;
         PlayerInfoViewControl.isOpenFromBag = false;
         CampBattleManager.instance.model.isFighting = false;
         this.dioposeTerraces();
         this._gameLivingArr = null;
         this._gameLivingIdArr = null;
         for each(_loc3_ in this._objectDic)
         {
            delete this._objectDic[_loc3_.Id];
            _loc3_.dispose();
            _loc3_ = null;
         }
         this._objectDic = null;
         _loc4_ = 0;
         while(_loc4_ < this._animationArray.length)
         {
            this._animationArray[_loc4_].dispose();
            this._animationArray[_loc4_] = null;
            _loc4_++;
         }
         GameManager.Instance.isAddTerrce = false;
         if(this._logTimer)
         {
            this._logTimer.removeEventListener(TimerEvent.TIMER,this.logTimeHandler);
            this._logTimer.stop();
         }
         this._logTimer = null;
      }
      
      override public function addedToStage() : void
      {
         super.addedToStage();
         stage.focus = _map;
      }
      
      override public function getBackType() : String
      {
         if(_gameInfo.roomType == RoomInfo.CHALLENGE_ROOM)
         {
            return StateType.CHALLENGE_ROOM;
         }
         if(_gameInfo.roomType == RoomInfo.MATCH_ROOM)
         {
            return StateType.MATCH_ROOM;
         }
         if(_gameInfo.roomType == RoomInfo.FIGHT_LIB_ROOM)
         {
            return StateType.FIGHT_LIB;
         }
         if(_gameInfo.roomType == RoomInfo.FRESHMAN_ROOM)
         {
            if(StartupResourceLoader.firstEnterHall)
            {
               return StateType.FRESHMAN_ROOM2;
            }
            return StateType.FRESHMAN_ROOM1;
         }
         return StateType.DUNGEON_ROOM;
      }
      
      protected function __playerChange(param1:CrazyTankSocketEvent) : void
      {
         var _loc5_:Living = null;
         var _loc6_:Player = null;
         var _loc7_:GameLiving = null;
         PageInterfaceManager.restorePageTitle();
         if(_selfMarkBar)
         {
            _selfMarkBar.shutdown();
         }
         _map.currentFocusedLiving = null;
         var _loc2_:int = param1.pkg.extend1;
         var _loc3_:Living = _gameInfo.findLiving(_loc2_);
         if(GameManager.Instance.Current && GameManager.Instance.Current.roomType == FightFootballTimeManager.FIGHTFOOTBALLTIME_ROOM)
         {
            _loc6_ = _loc3_ as Player;
            GameManager.Instance.Current.currentPlayerId = _loc6_.playerInfo.ID;
         }
         _gameInfo.currentLiving = _loc3_;
         if(_loc3_ is TurnedLiving)
         {
            this._ignoreSmallEnemy = false;
            if(!_loc3_.isLiving)
            {
               setCurrentPlayer(null);
               return;
            }
            if(_loc3_.isBoss)
            {
               if(RoomManager.Instance.current.type == RoomInfo.ACTIVITY_DUNGEON_ROOM)
               {
                  updateDamageView();
               }
            }
            if(_loc3_.playerInfo == PlayerManager.Instance.Self)
            {
               PageInterfaceManager.changePageTitle();
            }
            param1.executed = false;
            this._soundPlayFlag = true;
            if(_loc3_ is LocalPlayer && _gameTrusteeshipView && _gameTrusteeshipView.trusteeshipState)
            {
               new ChangePlayerAction(_map,_loc3_ as TurnedLiving,param1,param1.pkg,0,0).executeAtOnce();
            }
            else
            {
               _map.act(new ChangePlayerAction(_map,_loc3_ as TurnedLiving,param1,param1.pkg));
            }
         }
         else
         {
            _map.act(new ChangeNpcAction(this,_map,_loc3_ as Living,param1,param1.pkg,this._ignoreSmallEnemy));
            if(!this._ignoreSmallEnemy)
            {
               this._ignoreSmallEnemy = true;
            }
         }
         var _loc4_:DictionaryData = GameManager.Instance.Current.livings;
         for each(_loc5_ in _loc4_)
         {
            _loc7_ = this.getGameLivingByID(_loc5_.LivingID) as GameLiving;
            if(_loc7_)
            {
               _loc7_.fightPowerVisible = false;
            }
         }
         PrepareShootAction.hasDoSkillAnimation = false;
      }
      
      private function __playMovie(param1:CrazyTankSocketEvent) : void
      {
         var _loc3_:String = null;
         var _loc2_:Living = _gameInfo.findLiving(param1.pkg.extend1);
         if(_loc2_)
         {
            _loc3_ = param1.pkg.readUTF();
            _loc2_.playMovie(_loc3_);
            _map.bringToFront(_loc2_);
         }
      }
      
      private function __livingTurnRotation(param1:CrazyTankSocketEvent) : void
      {
         var _loc3_:int = 0;
         var _loc4_:Number = NaN;
         var _loc5_:String = null;
         var _loc2_:Living = _gameInfo.findLiving(param1.pkg.extend1);
         if(_loc2_)
         {
            _loc3_ = param1.pkg.readInt() / 10;
            _loc4_ = param1.pkg.readInt() / 10;
            _loc5_ = param1.pkg.readUTF();
            _loc2_.turnRotation(_loc3_,_loc4_,_loc5_);
            _map.bringToFront(_loc2_);
         }
      }
      
      public function addliving(param1:CrazyTankSocketEvent) : void
      {
         var _loc30_:Living = null;
         var _loc31_:GameLiving = null;
         var _loc33_:* = null;
         var _loc34_:* = null;
         var _loc35_:String = null;
         var _loc36_:String = null;
         var _loc37_:FightBuffInfo = null;
         var _loc38_:String = null;
         var _loc39_:String = null;
         var _loc40_:int = 0;
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readByte();
         var _loc4_:int = _loc2_.readInt();
         var _loc5_:String = _loc2_.readUTF();
         var _loc6_:String = _loc2_.readUTF();
         var _loc7_:String = _loc2_.readUTF();
         var _loc8_:Point = new Point(_loc2_.readInt(),_loc2_.readInt());
         var _loc9_:int = _loc2_.readInt();
         var _loc10_:int = _loc2_.readInt();
         var _loc11_:int = _loc2_.readInt();
         var _loc12_:int = _loc2_.readByte();
         var _loc13_:int = _loc2_.readByte();
         var _loc14_:Boolean = _loc13_ == 0?Boolean(true):Boolean(false);
         var _loc15_:Boolean = _loc2_.readBoolean();
         var _loc16_:Boolean = _loc2_.readBoolean();
         var _loc17_:int = _loc2_.readInt();
         var _loc18_:Dictionary = new Dictionary();
         var _loc19_:int = 0;
         while(_loc19_ < _loc17_)
         {
            _loc35_ = _loc2_.readUTF();
            _loc36_ = _loc2_.readUTF();
            _loc18_[_loc35_] = _loc36_;
            _loc19_++;
         }
         var _loc20_:int = _loc2_.readInt();
         var _loc21_:Vector.<FightBuffInfo> = new Vector.<FightBuffInfo>();
         var _loc22_:int = 0;
         while(_loc22_ < _loc20_)
         {
            _loc37_ = BuffManager.creatBuff(_loc2_.readInt());
            _loc21_.push(_loc37_);
            _loc22_++;
         }
         var _loc23_:Boolean = _loc2_.readBoolean();
         var _loc24_:Boolean = _loc2_.readBoolean();
         var _loc25_:Boolean = _loc2_.readBoolean();
         var _loc26_:Boolean = _loc2_.readBoolean();
         var _loc27_:int = _loc2_.readInt();
         var _loc28_:Dictionary = new Dictionary();
         var _loc29_:int = 0;
         while(_loc29_ < _loc27_)
         {
            _loc38_ = _loc2_.readUTF();
            _loc39_ = _loc2_.readUTF();
            _loc28_[_loc38_] = _loc39_;
            _loc29_++;
         }
         if(_map && _map.getPhysical(_loc4_))
         {
            _map.getPhysical(_loc4_).dispose();
         }
         if(_loc3_ != 4 && _loc3_ != 5 && _loc3_ != 6 && _loc3_ != 12)
         {
            _loc30_ = new SmallEnemy(_loc4_,_loc11_,_loc10_);
            _loc30_.typeLiving = _loc3_;
            _loc30_.actionMovieName = _loc6_;
            _loc30_.direction = _loc12_;
            _loc30_.pos = _loc8_;
            _loc30_.name = _loc5_;
            _loc30_.isBottom = _loc14_;
            _loc30_.isShowBlood = _loc15_;
            _loc30_.isShowSmallMapPoint = _loc16_;
            _gameInfo.addGamePlayer(_loc30_);
            _loc31_ = new GameSmallEnemy(_loc30_ as SmallEnemy);
            this._gameLivingArr.push(_loc31_);
            if(_loc9_ != _loc30_.maxBlood)
            {
               _loc30_.initBlood(_loc9_);
            }
            if(RoomManager.Instance.current.type == RoomInfo.ACTIVITY_DUNGEON_ROOM)
            {
               explorersLiving = _gameInfo.findLivingByName(LanguageMgr.GetTranslation("activity.dungeonView.explorers"));
            }
         }
         else
         {
            _loc30_ = new SimpleBoss(_loc4_,_loc11_,_loc10_);
            _loc30_.typeLiving = _loc3_;
            _loc30_.actionMovieName = _loc6_;
            _loc30_.direction = _loc12_;
            _loc30_.pos = _loc8_;
            _loc30_.name = _loc5_;
            _loc30_.isBottom = _loc14_;
            _loc30_.isShowBlood = _loc15_;
            _loc30_.isShowSmallMapPoint = _loc16_;
            _gameInfo.addGamePlayer(_loc30_);
            _loc31_ = new GameSimpleBoss(_loc30_ as SimpleBoss);
            this._gameLivingArr.push(_loc31_);
            if(_loc9_ != _loc30_.maxBlood)
            {
               _loc30_.initBlood(_loc9_);
            }
            if(RoomManager.Instance.current.type == RoomInfo.ACTIVITY_DUNGEON_ROOM)
            {
               _loc40_ = _loc2_.readInt();
               GameManager.Instance.bossName = _loc5_;
               GameManager.Instance.currentNum = _loc40_ - (_loc40_ > 70003?70002:70000);
               addMessageBtn();
               if(!this.barrier)
               {
                  fadingComplete();
               }
               this.barrier.dispatchEvent(new CrazyTankSocketEvent(CrazyTankSocketEvent.UPDATE_ACTIVITYDUNGEON_INFO));
            }
         }
         var _loc32_:int = 0;
         while(_loc22_ < _loc21_.length)
         {
            _loc30_.addBuff(_loc21_[_loc22_]);
            _loc22_++;
         }
         for(_loc33_ in _loc18_)
         {
            _loc31_.setActionMapping(_loc33_,_loc18_[_loc33_]);
         }
         _loc31_.name = _loc5_;
         _map.addPhysical(_loc31_);
         if(_loc7_.length > 0)
         {
            _loc31_.doAction(_loc7_);
         }
         else if(!_loc18_[Living.STAND_ACTION])
         {
            _loc31_.doAction(Living.BORN_ACTION);
         }
         else
         {
            _loc31_.doAction(Living.STAND_ACTION);
         }
         _loc31_.info.isFrozen = _loc23_;
         _loc31_.info.isHidden = _loc24_;
         _loc31_.info.isNoNole = _loc25_;
         for(_loc34_ in _loc28_)
         {
            setProperty(_loc31_,_loc34_,_loc28_[_loc34_]);
         }
         _playerThumbnailLController.addLiving(_loc31_);
         addChild(_playerThumbnailLController);
         if(_loc30_ is SimpleBoss)
         {
            _map.setCenter(_loc31_.x,_loc31_.y - 150,false);
         }
         else
         {
            _map.setCenter(_loc31_.x,_loc31_.y - 150,true);
         }
      }
      
      protected function __addAnimation(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = null;
         var _loc3_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:ActivityDungeonNextView = null;
         var _loc10_:AnimationObject = null;
         _loc2_ = param1.pkg;
         _loc3_ = _loc2_.readInt();
         var _loc4_:Boolean = _loc2_.readBoolean();
         var _loc5_:int = _loc2_.readInt();
         var _loc6_:RoomPlayer = RoomManager.Instance.findRoomPlayer(_loc5_);
         var _loc7_:Player = _gameInfo.findLivingByPlayerID(_loc5_,_loc6_.playerInfo.ZoneID);
         if(_loc4_)
         {
            switch(_loc3_)
            {
               case 1:
                  _loc9_ = new ActivityDungeonNextView(_loc3_,_loc2_.readDate().time);
                  PositionUtils.setPos(_loc9_,"game.view.activityDungeonNextView.viewPos");
                  if(_loc6_.isViewer || !_loc7_.isLiving)
                  {
                     _loc9_.setBtnEnable();
                  }
                  this.hideView(false);
                  updateDamageView();
                  this._animationArray.push(_loc9_);
                  this.deleteAnimation(2);
                  break;
               case 2:
                  if(_loc7_.isLiving)
                  {
                     _loc10_ = new AnimationObject(2,"asset.game.lightning");
                     PositionUtils.setPos(_loc10_,"game.view.activityDungeon.lightningPos");
                     this._animationArray.push(_loc10_);
                  }
            }
            _loc8_ = 0;
            while(_loc8_ < this._animationArray.length)
            {
               addChild(this._animationArray[_loc8_]);
               _loc8_++;
            }
         }
         else
         {
            this.deleteAnimation(_loc3_);
            this.hideView(true);
            StageReferance.stage.focus = _map;
         }
      }
      
      public function deleteAnimation(param1:int) : void
      {
         var _loc2_:int = 0;
         while(_loc2_ < this._animationArray.length)
         {
            if(param1 == this._animationArray[_loc2_].Id)
            {
               removeChild(this._animationArray[_loc2_]);
               this._animationArray[_loc2_].dispose();
               this._animationArray[_loc2_] = null;
               this._animationArray.splice(_loc2_,1);
            }
            _loc2_++;
         }
      }
      
      public function hideView(param1:Boolean) : void
      {
         if(_selfMarkBar)
         {
            _selfMarkBar.visible = param1;
         }
         if(_cs)
         {
            _cs.visible = param1;
         }
         if(_selfBuffBar)
         {
            _selfBuffBar.visible = param1;
         }
         if(_kingblessIcon)
         {
            _kingblessIcon.visible = param1;
         }
         if(_playerThumbnailLController)
         {
            _playerThumbnailLController.visible = param1;
         }
         if(ChatManager.Instance.view)
         {
            ChatManager.Instance.view.visible = param1;
         }
         if(_leftPlayerView)
         {
            _leftPlayerView.visible = param1;
         }
         if(_vane)
         {
            _vane.visible = param1;
         }
         if(_barrier)
         {
            _barrier.visible = param1;
         }
      }
      
      private function __addTipLayer(param1:CrazyTankSocketEvent) : void
      {
         var _loc10_:MovieClip = null;
         var _loc11_:Class = null;
         var _loc12_:MovieClipWrapper = null;
         var _loc13_:SimpleObject = null;
         var _loc2_:int = param1.pkg.readInt();
         var _loc3_:int = param1.pkg.readInt();
         var _loc4_:int = param1.pkg.readInt();
         var _loc5_:int = param1.pkg.readInt();
         var _loc6_:String = param1.pkg.readUTF();
         var _loc7_:String = param1.pkg.readUTF();
         var _loc8_:int = param1.pkg.readInt();
         var _loc9_:int = param1.pkg.readInt();
         if(_loc3_ == 10)
         {
            if(ModuleLoader.hasDefinition(_loc6_))
            {
               _loc11_ = ModuleLoader.getDefinition(_loc6_) as Class;
               _loc10_ = new _loc11_() as MovieClip;
               _loc12_ = new MovieClipWrapper(_loc10_,false,true);
               this.addTipSprite(_loc12_.movie);
               _loc12_.gotoAndPlay(1);
            }
         }
         else
         {
            if(this._tipItems[_loc2_])
            {
               _loc13_ = this._tipItems[_loc2_] as SimpleObject;
            }
            else
            {
               _loc13_ = new SimpleObject(_loc2_,_loc3_,_loc6_,_loc7_);
               this.addTipSprite(_loc13_);
            }
            _loc13_.playAction(_loc7_);
            this._tipItems[_loc2_] = _loc13_;
         }
      }
      
      private function addTipSprite(param1:Sprite) : void
      {
         if(!this._tipLayers)
         {
            this._tipLayers = new Sprite();
            this._tipLayers.mouseChildren = this._tipLayers.mouseEnabled = false;
            addChild(this._tipLayers);
         }
         this._tipLayers.addChild(param1);
      }
      
      private function hideAllOther() : void
      {
         ObjectUtils.disposeObject(_selfMarkBar);
         _selfMarkBar = null;
         ObjectUtils.disposeObject(_cs);
         _cs = null;
         ObjectUtils.disposeObject(_selfBuffBar);
         _selfBuffBar = null;
         ObjectUtils.disposeObject(_kingblessIcon);
         _kingblessIcon = null;
         _playerThumbnailLController.visible = false;
         ChatManager.Instance.view.visible = false;
         _leftPlayerView.visible = false;
         _vane.visible = false;
         _barrier.visible = false;
      }
      
      private function __addMapThing(param1:CrazyTankSocketEvent) : void
      {
         var _loc17_:* = null;
         var _loc18_:String = null;
         var _loc19_:String = null;
         var _loc2_:int = param1.pkg.readInt();
         var _loc3_:int = param1.pkg.readInt();
         var _loc4_:int = param1.pkg.readInt();
         var _loc5_:int = param1.pkg.readInt();
         var _loc6_:String = param1.pkg.readUTF();
         var _loc7_:String = param1.pkg.readUTF();
         var _loc8_:int = param1.pkg.readInt();
         var _loc9_:int = param1.pkg.readInt();
         var _loc10_:int = param1.pkg.readInt();
         var _loc11_:int = param1.pkg.readInt();
         var _loc12_:int = param1.pkg.readInt();
         var _loc13_:int = param1.pkg.readInt();
         var _loc14_:Dictionary = new Dictionary();
         var _loc15_:int = 0;
         while(_loc15_ < _loc13_)
         {
            _loc18_ = param1.pkg.readUTF();
            _loc19_ = param1.pkg.readUTF();
            _loc14_[_loc18_] = _loc19_;
            _loc15_++;
         }
         var _loc16_:SimpleObject = null;
         switch(_loc3_)
         {
            case 1:
               _loc16_ = new SimpleBox(_loc2_,_loc6_);
               break;
            case 2:
               _loc16_ = new SimpleObject(_loc2_,1,_loc6_,_loc7_);
               break;
            case 3:
               _loc16_ = new TransmissionGate(_loc2_,_loc3_,"asset.game.transmitted",_loc7_);
               this.hideAllOther();
               break;
            default:
               _loc16_ = new SimpleObject(_loc2_,0,_loc6_,_loc7_,_loc11_ == 6);
         }
         _loc16_.x = _loc4_;
         _loc16_.y = _loc5_;
         _loc16_.scaleX = _loc8_;
         _loc16_.scaleY = _loc9_;
         _loc16_.rotation = _loc10_;
         for(_loc17_ in _loc14_)
         {
            _loc16_.setActionMapping(_loc17_,_loc14_[_loc17_]);
         }
         if(_loc3_ == 1)
         {
            this.addBox(_loc16_);
         }
         this.addEffect(_loc16_,_loc12_,_loc11_);
      }
      
      private function addBox(param1:SimpleObject) : void
      {
         if(GameManager.Instance.Current.selfGamePlayer.isLiving)
         {
            if(!this._boxArr)
            {
               this._boxArr = new Array();
            }
            this._boxArr.push(param1);
         }
         else
         {
            this.addEffect(param1);
         }
      }
      
      private function addEffect(param1:SimpleObject, param2:int = 0, param3:int = 0) : void
      {
         switch(param2)
         {
            case -1:
               this.addStageCurtain(param1);
               break;
            case 0:
               _map.addPhysical(param1);
               this._objectDic[param1.Id] = param1;
               if(param3 > 0 && param3 != 6)
               {
                  _map.phyBringToFront(param1);
               }
               break;
            default:
               _map.addObject(param1);
               this.getGameLivingByID(param2 - 1).addChild(param1);
         }
      }
      
      private function __updatePhysicObject(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:int = param1.pkg.readInt();
         var _loc3_:SimpleObject = _map.getPhysical(_loc2_) as SimpleObject;
         if(!_loc3_)
         {
            _loc3_ = this._tipItems[_loc2_] as SimpleObject;
         }
         var _loc4_:String = param1.pkg.readUTF();
         if(_loc3_)
         {
            _loc3_.playAction(_loc4_);
         }
         var _loc5_:PhyobjEvent = new PhyobjEvent(_loc4_);
         dispatchEvent(_loc5_);
      }
      
      private function __applySkill(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readInt();
         var _loc4_:int = _loc2_.readInt();
         SkillManager.applySkillToLiving(_loc3_,_loc4_,_loc2_);
      }
      
      private function __removeSkill(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readInt();
         var _loc4_:int = _loc2_.readInt();
         SkillManager.removeSkillFromLiving(_loc3_,_loc4_,_loc2_);
      }
      
      private function __removePhysicObject(param1:CrazyTankSocketEvent) : void
      {
         var _loc5_:SimpleObject = null;
         var _loc2_:int = param1.pkg.readInt();
         var _loc3_:PhysicalObj = this.getGameLivingByID(_loc2_);
         if(_loc3_ is GameSimpleBoss)
         {
            LogManager.getInstance().sendLog(GameSimpleBoss(_loc3_).simpleBoss.actionMovieName);
         }
         var _loc4_:Boolean = true;
         if(_loc3_ && _loc3_.parent)
         {
            _map.removePhysical(_loc3_);
         }
         if(_loc3_ && _loc3_.parent)
         {
            _loc3_.parent.removeChild(_loc3_);
         }
         if(_loc4_ && _loc3_)
         {
            if(!(_loc3_ is GameLiving) || GameLiving(_loc3_).isExist)
            {
               _loc3_.dispose();
            }
         }
         if(this._objectDic[_loc2_])
         {
            _loc5_ = this._objectDic[_loc2_];
            delete this._objectDic[_loc2_];
            _loc5_.dispose();
            _loc5_ = null;
         }
      }
      
      private function __focusOnObject(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:int = param1.pkg.readInt();
         var _loc3_:Array = [];
         var _loc4_:Object = new Object();
         _loc4_.x = param1.pkg.readInt();
         _loc4_.y = param1.pkg.readInt();
         _loc3_.push(_loc4_);
         _map.act(new ViewEachObjectAction(_map,_loc3_,_loc2_));
      }
      
      private function __barrierInfoHandler(param1:CrazyTankSocketEvent) : void
      {
         barrierInfo = param1;
      }
      
      private function __livingMoveto(param1:CrazyTankSocketEvent) : void
      {
         var _loc3_:Point = null;
         var _loc4_:Point = null;
         var _loc5_:int = 0;
         var _loc6_:String = null;
         var _loc7_:String = null;
         var _loc2_:Living = _gameInfo.findLiving(param1.pkg.extend1);
         if(_loc2_)
         {
            _loc3_ = new Point(param1.pkg.readInt(),param1.pkg.readInt());
            _loc4_ = new Point(param1.pkg.readInt(),param1.pkg.readInt());
            _loc5_ = param1.pkg.readInt();
            _loc6_ = param1.pkg.readUTF();
            _loc7_ = param1.pkg.readUTF();
            _loc2_.pos = _loc3_;
            _loc2_.moveTo(0,_loc4_,0,true,_loc6_,_loc5_,_loc7_);
            _map.bringToFront(_loc2_);
         }
      }
      
      public function livingFalling(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:Living = _gameInfo.findLiving(param1.pkg.extend1);
         var _loc3_:Point = new Point(param1.pkg.readInt(),param1.pkg.readInt());
         var _loc4_:int = param1.pkg.readInt();
         var _loc5_:String = param1.pkg.readUTF();
         var _loc6_:int = param1.pkg.readInt();
         if(_loc2_)
         {
            _loc2_.fallTo(_loc3_,_loc4_,_loc5_,_loc6_);
            if(_loc3_.y - _loc2_.pos.y > 50)
            {
               _map.setCenter(_loc3_.x,_loc3_.y - 150,false);
            }
            _map.bringToFront(_loc2_);
         }
      }
      
      private function __livingJump(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:Living = _gameInfo.findLiving(param1.pkg.extend1);
         var _loc3_:Point = new Point(param1.pkg.readInt(),param1.pkg.readInt());
         var _loc4_:int = param1.pkg.readInt();
         var _loc5_:String = param1.pkg.readUTF();
         var _loc6_:int = param1.pkg.readInt();
         _loc2_.jumpTo(_loc3_,_loc4_,_loc5_,_loc6_);
         _map.bringToFront(_loc2_);
      }
      
      private function __livingBeat(param1:CrazyTankSocketEvent) : void
      {
         var _loc8_:Living = null;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         var _loc12_:int = 0;
         var _loc13_:Object = null;
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:Living = _gameInfo.findLiving(_loc2_.extend1);
         var _loc4_:String = _loc2_.readUTF();
         var _loc5_:uint = _loc2_.readInt();
         var _loc6_:Array = new Array();
         var _loc7_:uint = 0;
         while(_loc7_ < _loc5_)
         {
            _loc8_ = _gameInfo.findLiving(_loc2_.readInt());
            _loc9_ = _loc2_.readInt();
            _loc10_ = _loc2_.readInt();
            _loc11_ = _loc2_.readInt();
            _loc12_ = _loc2_.readInt();
            _loc13_ = new Object();
            _loc13_["action"] = _loc4_;
            _loc13_["target"] = _loc8_;
            _loc13_["damage"] = _loc9_;
            _loc13_["targetBlood"] = _loc10_;
            _loc13_["dander"] = _loc11_;
            _loc13_["attackEffect"] = _loc12_;
            _loc6_.push(_loc13_);
            if(_loc8_ && _loc8_.isPlayer() && _loc8_.isLiving)
            {
               (_loc8_ as Player).dander = _loc11_;
            }
            _loc7_++;
         }
         if(_loc3_)
         {
            _loc3_.beat(_loc6_);
         }
      }
      
      private function __livingSay(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:Living = _gameInfo.findLiving(param1.pkg.extend1);
         if(!_loc2_ || !_loc2_.isLiving)
         {
            return;
         }
         var _loc3_:String = param1.pkg.readUTF();
         var _loc4_:int = param1.pkg.readInt();
         _map.bringToFront(_loc2_);
         _loc2_.say(_loc3_,_loc4_);
      }
      
      private function __livingRangeAttacking(param1:CrazyTankSocketEvent) : void
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:Living = null;
         var _loc2_:int = param1.pkg.readInt();
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_)
         {
            _loc4_ = param1.pkg.readInt();
            _loc5_ = param1.pkg.readInt();
            _loc6_ = param1.pkg.readInt();
            _loc7_ = param1.pkg.readInt();
            _loc8_ = param1.pkg.readInt();
            _loc9_ = _gameInfo.findLiving(_loc4_);
            if(_loc9_)
            {
               _loc9_.isHidden = false;
               _loc9_.isFrozen = false;
               _loc9_.updateBlood(_loc6_,_loc8_);
               _loc9_.showAttackEffect(1);
               _map.bringToFront(_loc9_);
               if(_loc9_.isSelf)
               {
                  _map.setCenter(_loc9_.pos.x,_loc9_.pos.y,false);
               }
               if(_loc9_.isPlayer() && _loc9_.isLiving)
               {
                  (_loc9_ as Player).dander = _loc7_;
               }
            }
            _loc3_++;
         }
      }
      
      private function __livingDirChanged(param1:CrazyTankSocketEvent) : void
      {
         var _loc3_:int = 0;
         var _loc2_:Living = _gameInfo.findLiving(param1.pkg.extend1);
         if(_loc2_)
         {
            _loc3_ = param1.pkg.readInt();
            _loc2_.direction = _loc3_;
            _map.bringToFront(_loc2_);
         }
      }
      
      private function __removePlayer(param1:DictionaryEvent) : void
      {
         this._msg = RoomManager.Instance._removeRoomMsg;
         var _loc2_:Player = param1.data as Player;
         var _loc3_:GamePlayer = _players[_loc2_];
         if(_loc3_ && _loc2_)
         {
            if(_map.currentPlayer == _loc2_)
            {
               setCurrentPlayer(null);
            }
            if(_loc2_.isSelf)
            {
               if(RoomManager.Instance.current.type == RoomInfo.MATCH_ROOM || RoomManager.Instance.current.type == RoomInfo.CHALLENGE_ROOM)
               {
                  StateManager.setState(StateType.ROOM_LIST);
               }
               else if(RoomManager.Instance.current.type == RoomInfo.DUNGEON_ROOM)
               {
                  StateManager.setState(StateType.DUNGEON_LIST);
               }
               else if(RoomManager.Instance.current.type == RoomInfo.ENTERTAINMENT_ROOM || RoomManager.Instance.current.type == RoomInfo.ENTERTAINMENT_ROOM_PK)
               {
                  StateManager.setState(StateType.MATCH_ROOM);
               }
               else if(RoomManager.Instance.current.type == RoomInfo.CHRISTMAS_ROOM)
               {
                  if(ChristmasRoomController.isTimeOver)
                  {
                     ChristmasRoomController.isTimeOver = false;
                     StateManager.setState(StateType.MAIN);
                  }
                  else
                  {
                     ChristmasManager.isToRoom = true;
                     StateManager.setState(StateType.CHRISTMAS_ROOM);
                  }
               }
               else if(GameManager.Instance.Current.roomType == RoomInfo.CONSORTIA_MATCH_SCORE || GameManager.Instance.Current.roomType == RoomInfo.CONSORTIA_MATCH_RANK || GameManager.Instance.Current.roomType == RoomInfo.CONSORTIA_MATCH_SCORE_WHOLE || GameManager.Instance.Current.roomType == RoomInfo.CONSORTIA_MATCH_RANK_WHOLE)
               {
                  KingDivisionManager.Instance.openFrame = true;
                  StateManager.setState(StateType.MAIN);
               }
               else if(RoomManager.Instance.current.type == RoomInfo.CATCH_BEAST)
               {
                  StateManager.setState(StateType.MAIN);
               }
            }
            _map.removePhysical(_loc3_);
            _loc3_.dispose();
            delete _players[_loc2_];
         }
      }
      
      private function __beginShoot(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:GamePlayer = null;
         if(_map.currentPlayer && _map.currentPlayer.isPlayer() && param1.pkg.clientId != _map.currentPlayer.playerInfo.ID && _gameInfo.roomType != RoomInfo.ACTIVITY_DUNGEON_ROOM)
         {
            _map.executeAtOnce();
            _map.setCenter(_map.currentPlayer.pos.x,_map.currentPlayer.pos.y - 150,false);
            _loc2_ = _players[_map.currentPlayer];
         }
         if(_gameInfo.roomType != RoomInfo.ACTIVITY_DUNGEON_ROOM || _map.currentPlayer && _map.currentPlayer.isPlayer() && param1.pkg.clientId == _map.currentPlayer.playerInfo.ID)
         {
            setPropBarClickEnable(false,false);
            PrepareShootAction.hasDoSkillAnimation = false;
         }
      }
      
      protected function __shoot(param1:CrazyTankSocketEvent) : void
      {
         var _loc4_:LocalPlayer = null;
         var _loc5_:Number = NaN;
         var _loc6_:Boolean = false;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:Array = null;
         var _loc11_:Array = null;
         var _loc12_:Number = NaN;
         var _loc13_:uint = 0;
         var _loc14_:int = 0;
         var _loc15_:Array = null;
         var _loc16_:int = 0;
         var _loc17_:int = 0;
         var _loc18_:String = null;
         var _loc19_:Boolean = false;
         var _loc20_:Bomb = null;
         var _loc21_:int = 0;
         var _loc22_:Number = NaN;
         var _loc23_:Number = NaN;
         var _loc24_:Number = NaN;
         var _loc25_:int = 0;
         var _loc26_:int = 0;
         var _loc27_:BombAction = null;
         var _loc28_:int = 0;
         var _loc29_:int = 0;
         var _loc30_:Living = null;
         var _loc31_:int = 0;
         var _loc32_:int = 0;
         var _loc33_:int = 0;
         var _loc34_:Object = null;
         var _loc35_:Point = null;
         var _loc36_:Dictionary = null;
         var _loc37_:Bomb = null;
         var _loc38_:int = 0;
         var _loc39_:int = 0;
         var _loc40_:int = 0;
         var _loc41_:int = 0;
         var _loc42_:int = 0;
         var _loc43_:int = 0;
         EnergyView.canPower = false;
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:Living = _gameInfo.findLiving(param1.pkg.extend1);
         if(_loc3_)
         {
            _loc4_ = GameManager.Instance.Current.selfGamePlayer;
            _loc5_ = _loc2_.readInt() / 10;
            _loc6_ = _loc2_.readBoolean();
            _loc7_ = _loc2_.readByte();
            _loc8_ = _loc2_.readByte();
            _loc9_ = _loc2_.readByte();
            _loc10_ = [_loc6_,_loc7_,_loc8_,_loc9_];
            GameManager.Instance.Current.setWind(_loc5_,_loc3_.isSelf,_loc10_);
            _loc11_ = new Array();
            _loc12_ = _loc2_.readInt();
            _loc13_ = 0;
            while(_loc13_ < _loc12_)
            {
               _loc20_ = new Bomb();
               _loc20_.number = _loc2_.readInt();
               _loc20_.shootCount = _loc2_.readInt();
               _loc20_.IsHole = _loc2_.readBoolean();
               _loc20_.Id = _loc2_.readInt();
               _loc20_.X = _loc2_.readInt();
               _loc20_.Y = _loc2_.readInt();
               _loc20_.VX = _loc2_.readInt();
               _loc20_.VY = _loc2_.readInt();
               _loc21_ = _loc2_.readInt();
               _loc20_.Template = BallManager.findBall(_loc21_);
               _loc20_.Actions = new Array();
               _loc20_.changedPartical = _loc2_.readUTF();
               _loc22_ = _loc2_.readInt() / 1000;
               _loc23_ = _loc2_.readInt() / 1000;
               _loc24_ = _loc22_ * _loc23_;
               _loc20_.damageMod = _loc24_;
               _loc25_ = _loc2_.readInt();
               _loc28_ = 0;
               while(_loc28_ < _loc25_)
               {
                  _loc26_ = _loc2_.readInt();
                  _loc27_ = new BombAction(_loc26_,_loc2_.readInt(),_loc2_.readInt(),_loc2_.readInt(),_loc2_.readInt(),_loc2_.readInt());
                  _loc20_.Actions.push(_loc27_);
                  if(RoomManager.Instance.current.type == RoomInfo.ACTIVITY_DUNGEON_ROOM && _loc3_.isPlayer() && _loc27_.type == 5)
                  {
                     _loc3_.damageNum = _loc3_.damageNum + _loc27_.param2;
                  }
                  _loc28_++;
               }
               _loc11_.push(_loc20_);
               _loc13_++;
            }
            _loc3_.shoot(_loc11_,param1);
            _loc14_ = _loc2_.readInt();
            _loc15_ = [];
            _loc16_ = 0;
            while(_loc16_ < _loc14_)
            {
               _loc29_ = _loc2_.readInt();
               _loc30_ = _gameInfo.findLiving(_loc29_);
               _loc31_ = _loc2_.readInt();
               _loc32_ = _loc2_.readInt();
               _loc33_ = _loc2_.readInt();
               _loc34_ = {
                  "target":_loc30_,
                  "hp":_loc32_,
                  "damage":_loc31_,
                  "dander":_loc33_
               };
               _loc15_.push(_loc34_);
               _loc16_++;
            }
            _loc17_ = _loc2_.readInt();
            _loc18_ = "attack" + _loc17_.toString();
            if(_gameInfo.roomType == FightFootballTimeManager.FIGHTFOOTBALLTIME_ROOM)
            {
               _loc17_ = 0;
            }
            if(_loc17_ != 0)
            {
               _loc35_ = null;
               if(_loc11_.length == 3)
               {
                  _loc35_ = Bomb(_loc11_[1]).target;
               }
               else if(_loc11_.length == 1)
               {
                  _loc35_ = Bomb(_loc11_[0]).target;
               }
               _loc36_ = Player(_loc3_).currentPet.petBeatInfo;
               _loc36_["actionName"] = _loc18_;
               _loc36_["targetPoint"] = _loc35_;
               _loc36_["targets"] = _loc15_;
               _loc37_ = Bomb(_loc11_[_loc11_.length == 3?1:0]);
               _loc37_.Actions.push(new BombAction(0,ActionType.PET,param1.pkg.extend1,0,0,0));
            }
            _loc19_ = _loc2_.readBoolean();
            if(_gameInfo.roomType == FightFootballTimeManager.FIGHTFOOTBALLTIME_ROOM)
            {
               redScore = _loc2_.readInt();
               blueScore = _loc2_.readInt();
               _loc38_ = _loc2_.readInt();
               _loc39_ = _loc2_.readInt();
               _loc40_ = _loc2_.readInt();
               _loc41_ = 0;
               while(_loc41_ < _loc40_)
               {
                  _loc42_ = _loc2_.readInt();
                  _loc43_ = _loc2_.readInt();
                  if(_loc4_.playerInfo.ID == _loc38_)
                  {
                     PlayerManager.Instance.Self.scoreArr.push(_loc43_);
                  }
                  _loc41_++;
               }
            }
            else
            {
               _loc2_.readInt();
               _loc2_.readInt();
               _loc2_.readInt();
               _loc2_.readInt();
               _loc2_.readInt();
            }
         }
      }
      
      private function __suicide(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:Living = _gameInfo.findLiving(param1.pkg.extend1);
         if(_loc2_)
         {
            _loc2_.die();
         }
      }
      
      private function __changeBall(param1:CrazyTankSocketEvent) : void
      {
         var _loc3_:Player = null;
         var _loc4_:Boolean = false;
         var _loc5_:int = 0;
         var _loc2_:Living = _gameInfo.findLiving(param1.pkg.extend1);
         if(_loc2_ && _loc2_ is Player)
         {
            _loc3_ = _loc2_ as Player;
            _loc4_ = param1.pkg.readBoolean();
            _loc5_ = param1.pkg.readInt();
            _map.act(new ChangeBallAction(_loc3_,_loc4_,_loc5_));
         }
      }
      
      private function __playerUsingItem(param1:CrazyTankSocketEvent) : void
      {
         var _loc9_:DisplayObject = null;
         var _loc10_:String = null;
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readByte();
         var _loc4_:int = _loc2_.readInt();
         var _loc5_:ItemTemplateInfo = ItemManager.Instance.getTemplateById(_loc2_.readInt());
         if(this.props.indexOf(_loc5_.TemplateID) != -1)
         {
            EnergyView.canPower = true;
         }
         var _loc6_:Living = _gameInfo.findLiving(_loc2_.extend1);
         var _loc7_:Living = _gameInfo.findLiving(_loc2_.readInt());
         var _loc8_:Boolean = _loc2_.readBoolean();
         if(_loc6_ && _loc5_)
         {
            if(_loc6_.isPlayer())
            {
               if(_loc5_.CategoryID == EquipType.Freeze)
               {
                  Player(_loc6_).skill == -1;
               }
               if(!(_loc6_ as Player).isSelf)
               {
                  if(_loc5_.CategoryID == EquipType.OFFHAND || _loc5_.CategoryID == EquipType.TEMP_OFFHAND)
                  {
                     _loc9_ = (_loc6_ as Player).currentDeputyWeaponInfo.getDeputyWeaponIcon();
                     _loc9_.x = _loc9_.x + 7;
                     (_loc6_ as Player).useItemByIcon(_loc9_);
                  }
                  else
                  {
                     (_loc6_ as Player).useItem(_loc5_);
                     _loc10_ = EquipType.hasPropAnimation(_loc5_);
                     if(_loc10_ != null && _loc7_ && _loc7_.LivingID != _loc6_.LivingID)
                     {
                        _loc7_.showEffect(_loc10_);
                     }
                  }
               }
            }
            if(_map.currentPlayer && _loc7_.team == _map.currentPlayer.team && (RoomManager.Instance.current.type != RoomInfo.ACTIVITY_DUNGEON_ROOM || (_loc6_ as Player).isSelf))
            {
               _map.currentPlayer.addState(_loc5_.TemplateID);
            }
            if(!_loc7_.isLiving)
            {
               if(_loc7_.isPlayer())
               {
                  (_loc7_ as Player).addState(_loc5_.TemplateID);
               }
            }
            if(RoomManager.Instance.current.type != RoomInfo.ACTIVITY_DUNGEON_ROOM)
            {
               if(!_loc6_.isLiving && _loc7_ && _loc6_.team == _loc7_.team)
               {
                  MessageTipManager.getInstance().show(_loc6_.LivingID + "|" + _loc5_.TemplateID,1);
               }
               if(_loc8_)
               {
                  MessageTipManager.getInstance().show(String(_loc7_.LivingID),3);
               }
            }
         }
      }
      
      private function __updateBuff(param1:CrazyTankSocketEvent) : void
      {
         var _loc7_:FightBuffInfo = null;
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.extend1;
         var _loc4_:int = _loc2_.readInt();
         var _loc5_:Boolean = _loc2_.readBoolean();
         var _loc6_:Living = _gameInfo.findLiving(_loc3_);
         if(_loc6_ is LocalPlayer)
         {
            if(_loc5_)
            {
               (_loc6_ as LocalPlayer).usePassBall = true;
            }
            else
            {
               (_loc6_ as LocalPlayer).usePassBall = false;
            }
         }
         if(_loc6_ && _loc4_ != -1)
         {
            if(_loc5_)
            {
               _loc7_ = BuffManager.creatBuff(_loc4_);
               _loc6_.addBuff(_loc7_);
            }
            else
            {
               _loc6_.removeBuff(_loc4_);
            }
         }
      }
      
      private function __updatePetBuff(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.extend1;
         var _loc4_:int = _loc2_.readInt();
         var _loc5_:String = _loc2_.readUTF();
         var _loc6_:String = _loc2_.readUTF();
         var _loc7_:String = _loc2_.readUTF();
         var _loc8_:String = _loc2_.readUTF();
         var _loc9_:Boolean = _loc2_.readBoolean();
         var _loc10_:Living = _gameInfo.findLiving(_loc3_);
         var _loc11_:FightBuffInfo = new FightBuffInfo(_loc4_);
         _loc11_.buffPic = _loc7_;
         _loc11_.buffEffect = _loc8_;
         _loc11_.type = BuffType.PET_BUFF;
         _loc11_.buffName = _loc5_;
         _loc11_.description = _loc6_;
         if(_loc10_)
         {
            if(_loc9_)
            {
               _loc10_.addPetBuff(_loc11_);
            }
            else
            {
               _loc10_.removePetBuff(_loc11_);
            }
         }
      }
      
      private function __startMove(param1:CrazyTankSocketEvent) : void
      {
         var _loc8_:Array = null;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:PickBoxAction = null;
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:Player = _gameInfo.findPlayer(param1.pkg.extend1);
         var _loc4_:int = _loc2_.readByte();
         var _loc5_:Point = new Point(_loc2_.readInt(),_loc2_.readInt());
         var _loc6_:int = _loc2_.readByte();
         var _loc7_:Boolean = _loc2_.readBoolean();
         if(_loc4_ == 2)
         {
            _loc8_ = [];
            _loc9_ = _loc2_.readInt();
            _loc10_ = 0;
            while(_loc10_ < _loc9_)
            {
               _loc11_ = new PickBoxAction(_loc2_.readInt(),_loc2_.readInt());
               _loc8_.push(_loc11_);
               _loc10_++;
            }
            if(_loc3_)
            {
               _loc3_.playerMoveTo(_loc4_,_loc5_,_loc6_,_loc7_,_loc8_);
            }
         }
         else if(_loc3_)
         {
            _loc3_.playerMoveTo(_loc4_,_loc5_,_loc6_,_loc7_);
         }
      }
      
      private function __onLivingBoltmove(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:Living = _gameInfo.findLiving(param1.pkg.extend1);
         if(_loc3_)
         {
            _loc3_.pos = new Point(_loc2_.readInt(),_loc2_.readInt());
         }
      }
      
      private function __playerBlood(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readByte();
         var _loc4_:int = _loc2_.readInt();
         var _loc5_:int = _loc2_.readInt();
         var _loc6_:Living = _gameInfo.findLiving(param1.pkg.extend1);
         if(_loc6_)
         {
            _loc6_.updateBlood(_loc4_,_loc3_,_loc5_);
         }
      }
      
      private function __changWind(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         _map.wind = _loc2_.readInt() / 10;
         var _loc3_:Boolean = _loc2_.readBoolean();
         var _loc4_:int = _loc2_.readByte();
         var _loc5_:int = _loc2_.readByte();
         var _loc6_:int = _loc2_.readByte();
         var _loc7_:Array = new Array();
         _loc7_ = [_loc3_,_loc4_,_loc5_,_loc6_];
         _vane.update(_map.wind,false,_loc7_);
      }
      
      private function __playerNoNole(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:Living = _gameInfo.findLiving(param1.pkg.extend1);
         if(_loc2_)
         {
            _loc2_.isNoNole = param1.pkg.readBoolean();
         }
      }
      
      private function __onChangePlayerTarget(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:int = param1.pkg.readInt();
         if(_loc2_ == 0)
         {
            if(_playerThumbnailLController)
            {
               _playerThumbnailLController.currentBoss = null;
            }
            return;
         }
         var _loc3_:Living = _gameInfo.findLiving(_loc2_);
         this._gameLivingIdArr.push(_loc2_);
         _playerThumbnailLController.currentBoss = _loc3_;
      }
      
      public function objectSetProperty(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:GameLiving = this.getGameLivingByID(param1.pkg.extend1) as GameLiving;
         if(!_loc2_)
         {
            return;
         }
         var _loc3_:String = param1.pkg.readUTF();
         var _loc4_:String = param1.pkg.readUTF();
         setProperty(_loc2_,_loc3_,_loc4_);
      }
      
      private function __usePetSkill(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.extend1;
         var _loc4_:int = _loc2_.readInt();
         var _loc5_:Boolean = _loc2_.readBoolean();
         var _loc6_:Player = _gameInfo.findPlayer(_loc3_);
         if(_loc6_ && _loc6_.currentPet && _loc5_)
         {
            _loc6_.usePetSkill(_loc4_,_loc5_);
            if(PetSkillManager.getSkillByID(_loc4_).BallType == PetSkillTemplateInfo.BALL_TYPE_2)
            {
               _loc6_.isAttacking = false;
               GameManager.Instance.Current.selfGamePlayer.beginShoot();
            }
         }
         if(!_loc5_)
         {
            GameManager.Instance.dispatchEvent(new LivingEvent(LivingEvent.PETSKILL_USED_FAIL));
         }
      }
      
      private function __petBeat(param1:CrazyTankSocketEvent) : void
      {
         var _loc11_:int = 0;
         var _loc12_:Living = null;
         var _loc13_:int = 0;
         var _loc14_:int = 0;
         var _loc15_:int = 0;
         var _loc16_:Object = null;
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.extend1;
         var _loc4_:Player = _gameInfo.findPlayer(_loc3_);
         var _loc5_:int = _loc2_.readInt();
         var _loc6_:Array = [];
         var _loc7_:int = 0;
         while(_loc7_ < _loc5_)
         {
            _loc11_ = _loc2_.readInt();
            _loc12_ = _gameInfo.findLiving(_loc11_);
            _loc13_ = _loc2_.readInt();
            _loc14_ = _loc2_.readInt();
            _loc15_ = _loc2_.readInt();
            _loc16_ = {
               "target":_loc12_,
               "hp":_loc14_,
               "damage":_loc13_,
               "dander":_loc15_
            };
            _loc6_.push(_loc16_);
            _loc7_++;
         }
         var _loc8_:int = _loc2_.readInt();
         var _loc9_:String = "attack" + _loc8_.toString();
         var _loc10_:Point = new Point(_loc2_.readInt(),_loc2_.readInt());
         _loc4_.petBeat(_loc9_,_loc10_,_loc6_);
      }
      
      private function __playerHide(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:Living = _gameInfo.findLiving(param1.pkg.extend1);
         if(_loc2_)
         {
            _loc2_.isHidden = param1.pkg.readBoolean();
         }
      }
      
      private function __gameOver(param1:CrazyTankSocketEvent) : void
      {
         GameManager.Instance.currentNum = 0;
         this.gameOver();
         _map.act(new GameOverAction(_map,param1,this.showExpView));
      }
      
      public function logTimeHandler(param1:TimerEvent = null) : void
      {
         _map.traceCurrentAction();
      }
      
      private function __missionOver(param1:CrazyTankSocketEvent) : void
      {
         var _loc3_:* = null;
         this.gameOver();
         this._missionAgain = new MissionAgainInfo();
         this._missionAgain.value = _gameInfo.missionInfo.tryagain;
         var _loc2_:DictionaryData = RoomManager.Instance.current.players;
         for(_loc3_ in _loc2_)
         {
            if(RoomPlayer(_loc2_[_loc3_]).isHost)
            {
               this._missionAgain.host = RoomPlayer(_loc2_[_loc3_]).playerInfo.NickName;
            }
            if(RoomPlayer(_loc2_[_loc3_]).isSelf)
            {
               if(!GameManager.Instance.Current.selfGamePlayer.petSkillEnabled)
               {
                  GameManager.Instance.Current.selfGamePlayer.petSkillEnabled = true;
               }
            }
         }
         _map.act(new MissionOverAction(_map,param1,this.showExpView));
         if(GameManager.GAME_CAN_NOT_EXIT_SEND_LOG == 1 && _gameInfo.roomType == RoomInfo.ACTIVITY_DUNGEON_ROOM)
         {
            if(!this._logTimer)
            {
               this._logTimer = new Timer(15000,10);
               this._logTimer.addEventListener(TimerEvent.TIMER,this.logTimeHandler,false,0,true);
            }
            this._logTimer.start();
         }
      }
      
      override protected function gameOver() : void
      {
         PageInterfaceManager.restorePageTitle();
         super.gameOver();
         KeyboardManager.getInstance().isStopDispatching = true;
      }
      
      private function showTryAgain() : void
      {
         var _loc1_:TryAgain = new TryAgain(this._missionAgain);
         _loc1_.addEventListener(GameEvent.TRYAGAIN,this.__tryAgain);
         _loc1_.addEventListener(GameEvent.GIVEUP,this.__giveup);
         _loc1_.addEventListener(GameEvent.TIMEOUT,this.__tryAgainTimeOut);
         _loc1_.show();
         addChild(_loc1_);
      }
      
      private function __tryAgainTimeOut(param1:GameEvent) : void
      {
         param1.currentTarget.removeEventListener(GameEvent.TRYAGAIN,this.__tryAgain);
         param1.currentTarget.removeEventListener(GameEvent.GIVEUP,this.__giveup);
         param1.currentTarget.removeEventListener(GameEvent.TIMEOUT,this.__tryAgainTimeOut);
         ObjectUtils.disposeObject(param1.currentTarget);
         if(this._expView)
         {
            this._expView.close();
         }
         this._expView = null;
      }
      
      private function showExpView() : void
      {
         var _loc2_:int = 0;
         var _loc3_:URLVariables = null;
         var _loc4_:URLRequest = null;
         var _loc5_:URLLoader = null;
         var _loc1_:int = GameManager.Instance.Current.roomType;
         if(ChatManager.Instance.input.parent)
         {
            ChatManager.Instance.switchVisible();
         }
         disposeUI();
         MenoryUtil.clearMenory();
         if(GameManager.Instance.Current.roomType == 14)
         {
            StateManager.setState(StateType.WORLDBOSS_ROOM);
            return;
         }
         if(RoomManager.Instance.current.type == RoomInfo.TRANSNATIONALFIGHT_ROOM)
         {
            StateManager.setState(StateType.MAIN);
            TransnationalFightManager.Instance.isfromTransnational = true;
            return;
         }
         if(GameManager.Instance.Current.roomType == RoomInfo.CONSORTIA_BOSS)
         {
            StateManager.setState(StateType.CONSORTIA,ConsortionModelControl.Instance.openBossFrame);
            return;
         }
         if(GameManager.Instance.Current.roomType == RoomInfo.CONSORTIA_BATTLE)
         {
            StateManager.setState(StateType.CONSORTIA_BATTLE_SCENE);
            return;
         }
         if(RoomManager.Instance.current.type == RoomInfo.ENTERTAINMENT_ROOM || RoomManager.Instance.current.type == RoomInfo.ENTERTAINMENT_ROOM_PK)
         {
            StateManager.setState(StateType.MATCH_ROOM);
            return;
         }
         if(GameManager.Instance.Current.roomType == RoomInfo.CONSORTIA_MATCH_SCORE || GameManager.Instance.Current.roomType == RoomInfo.CONSORTIA_MATCH_RANK || GameManager.Instance.Current.roomType == RoomInfo.CONSORTIA_MATCH_SCORE_WHOLE || GameManager.Instance.Current.roomType == RoomInfo.CONSORTIA_MATCH_RANK_WHOLE)
         {
            KingDivisionManager.Instance.openFrame = true;
            StateManager.setState(StateType.MAIN);
            return;
         }
         if(GameManager.Instance.Current.roomType == RoomInfo.CHRISTMAS_ROOM)
         {
            if(ChristmasRoomController.isTimeOver)
            {
               ChristmasRoomController.isTimeOver = false;
               StateManager.setState(StateType.MAIN);
               return;
            }
            ChristmasManager.isToRoom = true;
            StateManager.setState(StateType.CHRISTMAS_ROOM);
            return;
         }
         if(GameManager.Instance.Current.roomType == RoomInfo.SEVEN_DOUBLE)
         {
            if(SevenDoubleManager.instance.isStart)
            {
               StateManager.setState(StateType.SEVEN_DOUBLE_SCENE);
            }
            else if(EscortManager.instance.isStart)
            {
               StateManager.setState(StateType.ESCORT);
            }
            else
            {
               StateManager.setState(StateType.MAIN);
            }
            return;
         }
         if(GameManager.Instance.Current.roomType == RoomInfo.CATCH_BEAST)
         {
            StateManager.setState(StateType.MAIN);
            return;
         }
         if(GameManager.Instance.Current.roomType == RoomInfo.RING_STATION)
         {
            StateManager.setState(StateType.MAIN);
            return;
         }
         if(GameManager.Instance.Current.roomType == RoomInfo.ACTIVITY_DUNGEON_ROOM)
         {
            StateManager.setState(StateType.DUNGEON_LIST);
            return;
         }
         if(GameManager.Instance.Current.roomType == RoomInfo.CAMPBATTLE_BATTLE)
         {
            SocketManager.Instance.out.returnToPve();
            return;
         }
         if(!this._firstEnter)
         {
            this._expView = new ExpView(_map.mapBitmap);
            this._expView.addEventListener(GameEvent.EXPSHOWED,this.__expShowed);
            addChild(this._expView);
            this._expView.show();
         }
         else
         {
            if(ApplicationDomain.currentDomain.hasDefinition("RegisterLuncher"))
            {
               if(getDefinitionByName("RegisterLuncher").newBieTime != -1)
               {
                  _loc2_ = getTimer() - getDefinitionByName("RegisterLuncher").newBieTime;
                  _loc3_ = new URLVariables();
                  _loc3_.id = PlayerManager.Instance.Self.ID;
                  _loc3_.type = 3;
                  _loc3_.time = _loc2_;
                  _loc3_.grade = PlayerManager.Instance.Self.Grade;
                  _loc3_.serverID = PlayerManager.Instance.Self.ZoneID;
                  _loc4_ = new URLRequest(PathManager.solveRequestPath("LogTime.ashx"));
                  _loc4_.method = URLRequestMethod.POST;
                  _loc4_.data = _loc3_;
                  _loc5_ = new URLLoader();
                  _loc5_.load(_loc4_);
               }
            }
            StateManager.setState(StateType.MAIN);
         }
      }
      
      private function __expShowed(param1:GameEvent) : void
      {
         var _loc2_:Living = null;
         var _loc3_:Living = null;
         this._expView.removeEventListener(GameEvent.EXPSHOWED,this.__expShowed);
         for each(_loc2_ in _gameInfo.livings.list)
         {
            if(_loc2_.isSelf)
            {
               if(Player(_loc2_).isWin && this._missionAgain)
               {
                  this._missionAgain.win = true;
               }
               if(Player(_loc2_).hasLevelAgain && this._missionAgain)
               {
                  this._missionAgain.hasLevelAgain = true;
               }
            }
         }
         for each(_loc3_ in _gameInfo.viewers.list)
         {
            if(_loc3_.isSelf)
            {
               if(Player(_loc3_).isWin && this._missionAgain)
               {
                  this._missionAgain.win = true;
               }
               if(Player(_loc3_).hasLevelAgain && this._missionAgain)
               {
                  this._missionAgain.hasLevelAgain = true;
               }
            }
         }
         if((GameManager.isDungeonRoom(_gameInfo) || GameManager.isAcademyRoom(_gameInfo)) && _gameInfo.missionInfo.tryagain > 0)
         {
            if(RoomManager.Instance.current.selfRoomPlayer.isViewer && !this._missionAgain.win)
            {
               this.showTryAgain();
               if(this._expView)
               {
                  this._expView.visible = false;
               }
            }
            else if(RoomManager.Instance.current.selfRoomPlayer.isViewer && this._missionAgain.win)
            {
               if(this._expView)
               {
                  this._expView.close();
               }
               this._expView = null;
            }
            else if(!_gameInfo.selfGamePlayer.isWin)
            {
               if(GameManager.isLanbyrinthRoom(_gameInfo))
               {
                  GameInSocketOut.sendGamePlayerExit();
                  if(this._expView)
                  {
                     this._expView.visible = false;
                  }
                  this._expView = null;
               }
               else
               {
                  this.showTryAgain();
                  if(this._expView)
                  {
                     this._expView.visible = false;
                  }
               }
            }
            else
            {
               this._expView.showCard();
               this._expView = null;
            }
         }
         else if(GameManager.isFightLib(_gameInfo))
         {
            this._expView.close();
            this._expView = null;
         }
         else if(RoomManager.Instance.current.selfRoomPlayer.isViewer)
         {
            this._expView.close();
            this._expView = null;
         }
         else
         {
            this._expView.showCard();
            this._expView = null;
         }
      }
      
      private function __giveup(param1:GameEvent) : void
      {
         param1.currentTarget.removeEventListener(GameEvent.TRYAGAIN,this.__tryAgain);
         param1.currentTarget.removeEventListener(GameEvent.GIVEUP,this.__giveup);
         param1.currentTarget.removeEventListener(GameEvent.TIMEOUT,this.__tryAgainTimeOut);
         ObjectUtils.disposeObject(param1.currentTarget);
         if(RoomManager.Instance.current.selfRoomPlayer.isHost)
         {
            GameInSocketOut.sendMissionTryAgain(GameManager.MissionGiveup,true);
         }
         if(this._expView)
         {
            this._expView.close();
            this._expView = null;
         }
      }
      
      private function __tryAgain(param1:GameEvent) : void
      {
         param1.currentTarget.removeEventListener(GameEvent.TRYAGAIN,this.__tryAgain);
         param1.currentTarget.removeEventListener(GameEvent.GIVEUP,this.__giveup);
         param1.currentTarget.removeEventListener(GameEvent.TIMEOUT,this.__tryAgainTimeOut);
         ObjectUtils.disposeObject(param1.currentTarget);
         if(!RoomManager.Instance.current.selfRoomPlayer.isViewer || GameManager.Instance.TryAgain == GameManager.MissionAgain)
         {
            GameManager.Instance.Current.hasNextMission = true;
         }
         if(RoomManager.Instance.current.type != RoomInfo.LANBYRINTH_ROOM)
         {
            this._expView.close();
         }
         this._expView = null;
      }
      
      private function __dander(param1:CrazyTankSocketEvent) : void
      {
         var _loc3_:int = 0;
         var _loc2_:Living = _gameInfo.findLiving(param1.pkg.extend1);
         if(_loc2_ && _loc2_ is Player)
         {
            _loc3_ = param1.pkg.readInt();
            (_loc2_ as Player).dander = _loc3_;
         }
      }
      
      private function __reduceDander(param1:CrazyTankSocketEvent) : void
      {
         var _loc3_:int = 0;
         var _loc2_:Living = _gameInfo.findLiving(param1.pkg.extend1);
         if(_loc2_ && _loc2_ is Player)
         {
            _loc3_ = param1.pkg.readInt();
            (_loc2_ as Player).reduceDander(_loc3_);
         }
      }
      
      private function __changeState(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:Living = _gameInfo.findLiving(param1.pkg.extend1);
         if(_loc2_)
         {
            _loc2_.State = param1.pkg.readInt();
            _map.setCenter(_loc2_.pos.x,_loc2_.pos.y,true);
         }
      }
      
      private function __selfObtainItem(param1:BagEvent) : void
      {
         var _loc2_:InventoryItemInfo = null;
         var _loc3_:PropInfo = null;
         var _loc4_:MovieClipWrapper = null;
         var _loc5_:AutoDisappear = null;
         var _loc6_:AutoDisappear = null;
         var _loc7_:AutoDisappear = null;
         if(_gameInfo.roomType == RoomInfo.ACTIVITY_DUNGEON_ROOM)
         {
            return;
         }
         for each(_loc2_ in param1.changedSlots)
         {
            _loc3_ = new PropInfo(_loc2_);
            _loc3_.Place = _loc2_.Place;
            if(PlayerManager.Instance.Self.FightBag.getItemAt(_loc2_.Place))
            {
               if(_gameInfo.gameMode != RoomInfo.ENTERTAINMENT_ROOM && _gameInfo.gameMode != RoomInfo.ENTERTAINMENT_ROOM_PK)
               {
                  _loc5_ = new AutoDisappear(ComponentFactory.Instance.creatBitmap("asset.game.getPropBgAsset"),3);
                  _loc5_.x = _vane.x - _loc5_.width / 2 + 48;
                  _loc5_.y = _selfMarkBar.y + _selfMarkBar.height + 70;
                  LayerManager.Instance.addToLayer(_loc5_,LayerManager.GAME_DYNAMIC_LAYER,false);
                  _loc6_ = new AutoDisappear(PropItemView.createView(_loc3_.Template.Pic,62,62),3);
                  _loc6_.x = _vane.x - _loc6_.width / 2 + 47;
                  _loc6_.y = _selfMarkBar.y + _selfMarkBar.height + 70;
                  LayerManager.Instance.addToLayer(_loc6_,LayerManager.GAME_DYNAMIC_LAYER,false);
                  _loc7_ = new AutoDisappear(ComponentFactory.Instance.creatBitmap("asset.game.getPropCiteAsset"),3);
                  _loc7_.x = _vane.x - _loc7_.width / 2 + 45;
                  _loc7_.y = _selfMarkBar.y + _selfMarkBar.height + 70;
                  LayerManager.Instance.addToLayer(_loc7_,LayerManager.GAME_DYNAMIC_LAYER,false);
               }
               _loc4_ = new MovieClipWrapper(ClassUtils.CreatInstance("asset.game.zxcTip"),true,true);
               _loc4_.movie.x = _loc4_.movie.x + (_loc4_.movie.width * _loc2_.Place - this.ZXC_OFFSET * _loc2_.Place);
               LayerManager.Instance.addToLayer(_loc4_.movie,LayerManager.GAME_UI_LAYER,false);
            }
         }
      }
      
      private function __getTempItem(param1:BagEvent) : void
      {
         var _loc2_:Boolean = GameManager.Instance.selfGetItemShowAndSound(param1.changedSlots);
         if(_loc2_ && this._soundPlayFlag)
         {
            this._soundPlayFlag = false;
            SoundManager.instance.play("1001");
         }
      }
      
      private function __forstPlayer(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:Living = _gameInfo.findLiving(param1.pkg.extend1);
         if(_loc2_)
         {
            _loc2_.isFrozen = param1.pkg.readBoolean();
         }
      }
      
      private function __changeShootCount(param1:CrazyTankSocketEvent) : void
      {
         if(_gameInfo.roomType != RoomInfo.ACTIVITY_DUNGEON_ROOM || param1.pkg.extend1 == _gameInfo.selfGamePlayer.LivingID)
         {
            _gameInfo.selfGamePlayer.shootCount = param1.pkg.readByte();
         }
      }
      
      private function __playSound(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:String = param1.pkg.readUTF();
         SoundManager.instance.initSound(_loc2_);
         SoundManager.instance.play(_loc2_);
      }
      
      private function __controlBGM(param1:CrazyTankSocketEvent) : void
      {
         if(param1.pkg.readBoolean())
         {
            SoundManager.instance.resumeMusic();
         }
         else
         {
            SoundManager.instance.pauseMusic();
         }
      }
      
      private function __forbidDragFocus(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:Boolean = param1.pkg.readBoolean();
         _map.smallMap.allowDrag = _loc2_;
         _arrowLeft.allowDrag = _arrowDown.allowDrag = _arrowRight.allowDrag = _arrowUp.allowDrag = _loc2_;
      }
      
      override protected function defaultForbidDragFocus() : void
      {
         _map.smallMap.allowDrag = true;
         _arrowLeft.allowDrag = _arrowDown.allowDrag = _arrowRight.allowDrag = _arrowUp.allowDrag = true;
      }
      
      private function __topLayer(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:Living = _gameInfo.findLiving(param1.pkg.readInt());
         if(_loc2_)
         {
            _map.bringToFront(_loc2_);
         }
      }
      
      private function __loadResource(param1:CrazyTankSocketEvent) : void
      {
         var _loc4_:GameNeedMovieInfo = null;
         var _loc2_:int = param1.pkg.readInt();
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_)
         {
            _loc4_ = new GameNeedMovieInfo();
            _loc4_.type = param1.pkg.readInt();
            _loc4_.path = param1.pkg.readUTF();
            _loc4_.classPath = param1.pkg.readUTF();
            _loc4_.startLoad();
            _loc3_++;
         }
      }
      
      public function livingShowBlood(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:int = param1.pkg.readInt();
         var _loc3_:Boolean = Boolean(param1.pkg.readInt());
         if(_map)
         {
            if(_map.getPhysical(_loc2_))
            {
               (_map.getPhysical(_loc2_) as GameLiving).showBlood(_loc3_);
            }
         }
      }
      
      private function __livingActionMapping(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:int = param1.pkg.readInt();
         var _loc3_:String = param1.pkg.readUTF();
         var _loc4_:String = param1.pkg.readUTF();
         if(_map.getPhysical(_loc2_))
         {
            _map.getPhysical(_loc2_).setActionMapping(_loc3_,_loc4_);
         }
      }
      
      private function __livingSmallColorChange(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:int = param1.pkg.readInt();
         var _loc3_:int = param1.pkg.readByte();
         if(_map.getPhysical(_loc2_) && _map.getPhysical(_loc2_) is GameLiving)
         {
            (_map.getPhysical(_loc2_) as GameLiving).changeSmallViewColor(_loc3_);
         }
      }
      
      private function __revivePlayer(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readInt();
         var _loc4_:Number = _loc2_.readLong();
         var _loc5_:int = _loc2_.readInt();
         var _loc6_:int = _loc2_.readInt();
         var _loc7_:GamePlayer = this.retrunPlayer(_loc3_);
         if(_loc7_ && !_loc7_.info.isLiving)
         {
            _loc7_.info.revive();
            _loc7_.pos = new Point(_loc5_,_loc6_);
            _loc7_.info.updateBlood(_loc4_,0,_loc4_);
            _loc7_.revive();
            _loc7_.info.dispatchEvent(new LivingEvent(LivingEvent.REVIVE));
            if(_loc7_ is GameLocalPlayer)
            {
               _fightControlBar.setState(FightControlBar.LIVE);
               _selfBuffBar = ComponentFactory.Instance.creatCustomObject("SelfBuffBar",[this,_arrowDown]);
               if(GameManager.Instance.Current.mapIndex != 1405 && RoomManager.Instance.current.type != RoomInfo.FIGHTGROUND_ROOM)
               {
                  addChildAt(_selfBuffBar,this.numChildren - 1);
               }
               kingBlessIconInit();
            }
         }
      }
      
      public function revivePlayerChangePlayer(param1:int) : void
      {
         var _loc2_:GamePlayer = this.retrunPlayer(param1);
         if(_loc2_ && !_loc2_.info.isLiving)
         {
            _loc2_.info.revive();
            _loc2_.revive();
            _loc2_.info.dispatchEvent(new LivingEvent(LivingEvent.REVIVE));
            if(_loc2_ is GameLocalPlayer)
            {
               _fightControlBar.setState(FightControlBar.LIVE);
               _selfBuffBar = ComponentFactory.Instance.creatCustomObject("SelfBuffBar",[this,_arrowDown]);
               if(RoomManager.Instance.current.type != RoomInfo.FIGHTGROUND_ROOM && GameManager.Instance.Current.mapIndex != 1405)
               {
                  addChildAt(_selfBuffBar,this.numChildren - 1);
               }
               kingBlessIconInit();
            }
         }
      }
      
      private function __gameTrusteeship(param1:CrazyTankSocketEvent) : void
      {
         var _loc4_:int = 0;
         var _loc5_:Player = null;
         var _loc2_:int = param1.pkg.readInt();
         if(_loc2_ == 0)
         {
            return;
         }
         var _loc3_:int = 0;
         while(_loc3_ <= _loc2_ - 1)
         {
            _loc4_ = param1.pkg.readInt();
            _loc5_ = _gameInfo.findPlayer(_loc4_);
            _loc5_.playerInfo.isTrusteeship = param1.pkg.readBoolean();
            _loc3_++;
         }
      }
      
      private function __fightStatusChange(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:int = param1.pkg.extend1;
         var _loc3_:Player = _gameInfo.findPlayer(_loc2_);
         if(_loc3_)
         {
            _loc3_.playerInfo.fightStatus = param1.pkg.readInt();
         }
      }
      
      private function __skipNextHandler(param1:CrazyTankSocketEvent) : void
      {
         if(_gameInfo.roomType == RoomInfo.ACTIVITY_DUNGEON_ROOM)
         {
            if(ServerConfigManager.instance.isTanabataTreasure)
            {
               setTimeout(this.delayFocusSimpleBoss,250);
            }
         }
      }
      
      private function __clearDebuff(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:int = param1.pkg.readInt();
         var _loc3_:GamePlayer = this.retrunPlayer(_loc2_);
         if(_loc3_)
         {
            _loc3_.clearDebuff();
         }
      }
      
      private function delayFocusSimpleBoss() : void
      {
         if(!_map)
         {
            return;
         }
         var _loc1_:GameSimpleBoss = _map.getOneSimpleBoss;
         if(_loc1_)
         {
            _loc1_.needFocus(0,0,{"priority":3});
         }
      }
      
      private function getGameLivingByID(param1:int) : PhysicalObj
      {
         if(!_map)
         {
            return null;
         }
         return _map.getPhysical(param1);
      }
      
      private function addStageCurtain(param1:SimpleObject) : void
      {
         var obj:SimpleObject = param1;
         obj.movie.addEventListener("playEnd",function():void
         {
            obj.movie.stop();
            if(obj.parent)
            {
               obj.parent.removeChild(obj);
            }
            obj.dispose();
            obj = null;
         });
         addChild(obj);
      }
   }
}
