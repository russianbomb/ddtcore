package game
{
   import campbattle.CampBattleManager;
   import catchbeast.CatchBeastManager;
   import com.pickgliss.loader.BaseLoader;
   import com.pickgliss.loader.LoadResourceManager;
   import ddt.data.BallInfo;
   import ddt.data.BuffInfo;
   import ddt.data.FightBuffInfo;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.data.map.MissionInfo;
   import ddt.data.player.PlayerInfo;
   import ddt.data.player.SelfInfo;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.events.GameEvent;
   import ddt.loader.StartupResourceLoader;
   import ddt.manager.BallManager;
   import ddt.manager.BeadTemplateManager;
   import ddt.manager.BuffManager;
   import ddt.manager.ChatManager;
   import ddt.manager.LanguageMgr;
   import ddt.manager.LoadBombManager;
   import ddt.manager.PathManager;
   import ddt.manager.PetInfoManager;
   import ddt.manager.PetSkillManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.QueueManager;
   import ddt.manager.SocketManager;
   import ddt.manager.StateManager;
   import ddt.manager.TimeManager;
   import ddt.states.StateType;
   import ddt.view.chat.ChatData;
   import ddt.view.chat.ChatFormats;
   import ddt.view.chat.ChatInputView;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.geom.Point;
   import flash.utils.Dictionary;
   import game.model.GameInfo;
   import game.model.GameNeedMovieInfo;
   import game.model.GameNeedPetSkillInfo;
   import game.model.LocalPlayer;
   import game.model.Pet;
   import game.model.Player;
   import game.model.SimpleBoxInfo;
   import game.view.Bomb;
   import game.view.GameView;
   import game.view.effects.BloodNumberCreater;
   import game.view.experience.ExpTweenManager;
   import pet.date.PetInfo;
   import pet.date.PetSkillTemplateInfo;
   import ringStation.RingStationManager;
   import road7th.comm.PackageIn;
   import road7th.data.DictionaryData;
   import room.RoomManager;
   import room.model.RoomInfo;
   import room.model.RoomPlayer;
   import trainer.controller.WeakGuildManager;
   
   public class GameManager extends EventDispatcher
   {
      
      public static const CAMP_BATTLE_MODEL_PVE:int = 24;
      
      public static const CAMP_BATTLE_MODEL_PVP:int = 23;
      
      public static const RING_STATION_MODEL:int = 26;
      
      public static const CATCH_BEAST_MODEL:int = 28;
      
      public static const CHRISTMAS_MODEL:int = 40;
      
      public static const START_LOAD:String = "StartLoading";
      
      public static const START_MATCH:String = "StartMatch";
      
      public static var MinLevelDuplicate:int = 8;
      
      public static var MinLevelActivity:int = 25;
      
      public static const ENTER_MISSION_RESULT:String = "EnterMissionResult";
      
      public static const ENTER_ROOM:String = "EnterRoom";
      
      public static const LEAVE_MISSION:String = "leaveMission";
      
      public static const ENTER_DUNGEON:String = "EnterDungeon";
      
      public static const PLAYER_CLICK_PAY:String = "PlayerClickPay";
      
      private static var _instance:GameManager;
      
      public static const MissionGiveup:int = 0;
      
      public static const MissionAgain:int = 1;
      
      public static const MissionTimeout:int = 2;
      
      public static var GAME_CAN_NOT_EXIT_SEND_LOG:int = 1;
       
      
      private var _current:GameInfo;
      
      private var _numCreater:BloodNumberCreater;
      
      public var currentNum:int = 0;
      
      public var bossName:String = "";
      
      public var isAddTerrce:Boolean;
      
      public var terrceX:int;
      
      public var terrceY:int;
      
      public var terrceID:int;
      
      private var _loaderArray:Array;
      
      private var _gameView:GameView;
      
      private var _addLivingEvtVec:Vector.<CrazyTankSocketEvent>;
      
      private var _setPropertyEvtVec:Vector.<CrazyTankSocketEvent>;
      
      private var _livingFallingEvtVec:Vector.<CrazyTankSocketEvent>;
      
      private var _livingShowBloodEvtVec:Vector.<CrazyTankSocketEvent>;
      
      public var viewCompleteFlag:Boolean;
      
      public var selectList:Array;
      
      public var TryAgain:int = 0;
      
      private var _recevieLoadSocket:Boolean;
      
      private var _outBombs:DictionaryData;
      
      public function GameManager()
      {
         super();
      }
      
      public static function isAcademyRoom(param1:GameInfo) : Boolean
      {
         return param1.roomType == RoomInfo.ACADEMY_DUNGEON_ROOM;
      }
      
      public static function isDungeonRoom(param1:GameInfo) : Boolean
      {
         return param1.roomType == RoomInfo.DUNGEON_ROOM || param1.roomType == RoomInfo.LANBYRINTH_ROOM || param1.roomType == RoomInfo.SPECIAL_ACTIVITY_DUNGEON;
      }
      
      public static function isLanbyrinthRoom(param1:GameInfo) : Boolean
      {
         return param1.roomType == RoomInfo.LANBYRINTH_ROOM;
      }
      
      public static function isFightLib(param1:GameInfo) : Boolean
      {
         return param1.roomType == RoomInfo.FIGHT_LIB_ROOM;
      }
      
      public static function isFreshMan(param1:GameInfo) : Boolean
      {
         return param1.roomType == RoomInfo.FRESHMAN_ROOM;
      }
      
      public static function get Instance() : GameManager
      {
         if(_instance == null)
         {
            _instance = new GameManager();
         }
         return _instance;
      }
      
      public function get Current() : GameInfo
      {
         return this._current;
      }
      
      public function set Current(param1:GameInfo) : void
      {
         this._current = param1;
      }
      
      private function initData() : void
      {
         this._loaderArray = new Array();
         this._addLivingEvtVec = new Vector.<CrazyTankSocketEvent>();
         this._setPropertyEvtVec = new Vector.<CrazyTankSocketEvent>();
         this._livingFallingEvtVec = new Vector.<CrazyTankSocketEvent>();
         this._livingShowBloodEvtVec = new Vector.<CrazyTankSocketEvent>();
      }
      
      public function setup() : void
      {
         this.initData();
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.ADD_LIVING,this.__addLiving);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PLAYER_PROPERTY,this.__objectSetProperty);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.LIVING_FALLING,this.__livingFalling);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.LIVING_SHOW_BLOOD,this.__livingShowBlood);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GAME_CREATE,this.__createGame);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GAME_START,this.__gameStart);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GAME_LOAD,this.__beginLoad);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.SINGLEBATTLE_STARTMATCH,this.__singleBattleStartMatch);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.LOAD,this.__loadprogress);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GAME_ALL_MISSION_OVER,this.__missionAllOver);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GAME_TAKE_OUT,this.__takeOut);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.SHOW_CARDS,this.__showAllCard);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GAME_MISSION_INFO,this.__gameMissionInfo);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GAME_MISSION_START,this.__gameMissionStart);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GAME_MISSION_PREPARE,this.__gameMissionPrepare);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GAME_ROOM_INFO,this.__missionInviteRoomInfo);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PLAY_INFO_IN_GAME,this.__updatePlayInfoInGame);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GAME_MISSION_TRY_AGAIN,this.__missionTryAgain);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.LOAD_RESOURCE,this.__loadResource);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.LOAD_RESOURCE,this.__loadResource);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.SELECT_OBJECT,this.__selectObject);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.BUFF_UPDATE,this.__buffUpdate);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.ADD_NEW_PLAYER,this.addNewPlayerHander);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.ADD_TERRACE,this.addTerrace);
      }
      
      protected function __addLiving(param1:CrazyTankSocketEvent) : void
      {
         if(!this.viewCompleteFlag)
         {
            this._addLivingEvtVec.push(param1);
         }
         else
         {
            this._gameView.addliving(param1);
         }
      }
      
      protected function __objectSetProperty(param1:CrazyTankSocketEvent) : void
      {
         if(!this.viewCompleteFlag)
         {
            this._setPropertyEvtVec.push(param1);
         }
         else
         {
            this._gameView.objectSetProperty(param1);
         }
      }
      
      protected function __livingFalling(param1:CrazyTankSocketEvent) : void
      {
         if(!this.viewCompleteFlag)
         {
            this._livingFallingEvtVec.push(param1);
         }
         else
         {
            this._gameView.livingFalling(param1);
         }
      }
      
      protected function __livingShowBlood(param1:CrazyTankSocketEvent) : void
      {
         if(!this.viewCompleteFlag)
         {
            this._livingShowBloodEvtVec.push(param1);
         }
         else
         {
            this._gameView.livingShowBlood(param1);
         }
      }
      
      private function addTerrace(param1:CrazyTankSocketEvent) : void
      {
         var _loc5_:int = 0;
         var _loc6_:Player = null;
         var _loc7_:Boolean = false;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         if(!this.Current || this.Current.gameMode != 23)
         {
            return;
         }
         var _loc2_:int = param1.pkg.readInt();
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         while(_loc4_ < _loc2_)
         {
            _loc5_ = param1.pkg.readInt();
            _loc6_ = this.Current.findPlayer(_loc5_);
            _loc7_ = param1.pkg.readBoolean();
            _loc8_ = param1.pkg.readInt();
            _loc9_ = param1.pkg.readInt();
            _loc6_.isLocked = _loc7_;
            this.terrceID = _loc5_;
            if(_loc7_)
            {
               this.isAddTerrce = true;
               this.terrceX = _loc8_;
               this.terrceY = _loc9_ + 10;
               GameManager.Instance.dispatchEvent(new GameEvent(GameEvent.ADDTERRACE,[this.terrceX,this.terrceY,_loc5_]));
            }
            else
            {
               this.isAddTerrce = false;
               GameManager.Instance.dispatchEvent(new GameEvent(GameEvent.DELTERRACE,[_loc5_]));
            }
            _loc4_++;
         }
      }
      
      private function addNewPlayerHander(param1:CrazyTankSocketEvent) : void
      {
         var _loc37_:Boolean = false;
         var _loc38_:int = 0;
         var _loc39_:String = null;
         var _loc40_:String = null;
         var _loc41_:String = null;
         var _loc42_:int = 0;
         var _loc43_:String = null;
         var _loc44_:String = null;
         var _loc45_:int = 0;
         var _loc46_:PetInfo = null;
         var _loc47_:int = 0;
         var _loc48_:int = 0;
         var _loc49_:int = 0;
         var _loc50_:int = 0;
         var _loc51_:int = 0;
         var _loc52_:int = 0;
         var _loc53_:FightBuffInfo = null;
         var _loc54_:int = 0;
         var _loc55_:FightBuffInfo = null;
         var _loc56_:String = null;
         var _loc57_:String = null;
         var _loc2_:GameInfo = this.Current;
         var _loc3_:PackageIn = param1.pkg;
         var _loc4_:int = _loc3_.readInt();
         var _loc5_:String = _loc3_.readUTF();
         var _loc6_:int = _loc3_.readInt();
         var _loc7_:PlayerInfo = new PlayerInfo();
         _loc7_.beginChanges();
         var _loc8_:RoomPlayer = RoomManager.Instance.current.findPlayerByID(_loc6_,_loc4_);
         if(_loc8_ == null)
         {
            _loc8_ = new RoomPlayer(_loc7_);
         }
         _loc7_.ID = _loc6_;
         _loc7_.ZoneID = _loc4_;
         var _loc9_:String = _loc3_.readUTF();
         var _loc10_:Boolean = _loc3_.readBoolean();
         if(_loc10_ && _loc8_.place < 8)
         {
            _loc8_.place = 8;
         }
         _loc7_.NickName = _loc9_;
         _loc7_.typeVIP = _loc3_.readByte();
         _loc7_.VIPLevel = _loc3_.readInt();
         if(PlayerManager.Instance.isChangeStyleTemp(_loc7_.ID))
         {
            _loc3_.readBoolean();
            _loc3_.readInt();
            _loc3_.readUTF();
            _loc3_.readUTF();
            _loc3_.readUTF();
         }
         else
         {
            _loc37_ = _loc3_.readBoolean();
            _loc38_ = _loc3_.readInt();
            _loc39_ = _loc3_.readUTF();
            _loc40_ = _loc3_.readUTF();
            _loc41_ = _loc3_.readUTF();
            _loc7_.Sex = _loc37_;
            _loc7_.Hide = _loc38_;
            _loc7_.Style = _loc39_;
            _loc7_.Colors = _loc40_;
            _loc7_.Skin = _loc41_;
         }
         _loc7_.Grade = _loc3_.readInt();
         _loc7_.Repute = _loc3_.readInt();
         _loc7_.WeaponID = _loc3_.readInt();
         if(_loc7_.WeaponID != 0)
         {
            _loc42_ = _loc3_.readInt();
            _loc43_ = _loc3_.readUTF();
            _loc44_ = _loc3_.readDateString();
         }
         _loc7_.DeputyWeaponID = _loc3_.readInt();
         _loc7_.pvpBadgeId = _loc3_.readInt();
         _loc7_.Nimbus = _loc3_.readInt();
         _loc7_.IsShowConsortia = _loc3_.readBoolean();
         _loc7_.ConsortiaID = _loc3_.readInt();
         _loc7_.ConsortiaName = _loc3_.readUTF();
         _loc7_.badgeID = _loc3_.readInt();
         var _loc11_:int = _loc3_.readInt();
         var _loc12_:int = _loc3_.readInt();
         _loc7_.WinCount = _loc3_.readInt();
         _loc7_.TotalCount = _loc3_.readInt();
         _loc7_.FightPower = _loc3_.readInt();
         _loc7_.apprenticeshipState = _loc3_.readInt();
         _loc7_.masterID = _loc3_.readInt();
         _loc7_.setMasterOrApprentices(_loc3_.readUTF());
         _loc7_.AchievementPoint = _loc3_.readInt();
         _loc7_.honor = _loc3_.readUTF();
         _loc7_.Offer = _loc3_.readInt();
         _loc7_.DailyLeagueFirst = _loc3_.readBoolean();
         _loc7_.DailyLeagueLastScore = _loc3_.readInt();
         _loc7_.commitChanges();
         _loc8_.playerInfo.IsMarried = _loc3_.readBoolean();
         if(_loc8_.playerInfo.IsMarried)
         {
            _loc8_.playerInfo.SpouseID = _loc3_.readInt();
            _loc8_.playerInfo.SpouseName = _loc3_.readUTF();
         }
         _loc8_.additionInfo.resetAddition();
         _loc8_.additionInfo.GMExperienceAdditionType = Number(_loc3_.readInt() / 100);
         _loc8_.additionInfo.AuncherExperienceAddition = Number(_loc3_.readInt() / 100);
         _loc8_.additionInfo.GMOfferAddition = Number(_loc3_.readInt() / 100);
         _loc8_.additionInfo.AuncherOfferAddition = Number(_loc3_.readInt() / 100);
         _loc8_.additionInfo.GMRichesAddition = Number(_loc3_.readInt() / 100);
         _loc8_.additionInfo.AuncherRichesAddition = Number(_loc3_.readInt() / 100);
         _loc8_.team = _loc3_.readInt();
         _loc2_.addRoomPlayer(_loc8_);
         var _loc13_:int = _loc3_.readInt();
         var _loc14_:int = _loc3_.readInt();
         var _loc15_:Player = new Player(_loc8_.playerInfo,_loc13_,_loc8_.team,_loc14_);
         var _loc16_:int = _loc3_.readInt();
         var _loc17_:int = 0;
         while(_loc17_ < _loc16_)
         {
            _loc45_ = _loc3_.readInt();
            _loc46_ = _loc7_.pets[_loc45_];
            _loc47_ = _loc3_.readInt();
            if(_loc46_ == null)
            {
               _loc46_ = new PetInfo();
               _loc46_.TemplateID = _loc47_;
               PetInfoManager.fillPetInfo(_loc46_);
            }
            _loc46_.ID = _loc3_.readInt();
            _loc46_.Name = _loc3_.readUTF();
            _loc46_.UserID = _loc3_.readInt();
            _loc46_.Level = _loc3_.readInt();
            _loc46_.IsEquip = true;
            _loc46_.clearEquipedSkills();
            _loc48_ = _loc3_.readInt();
            _loc49_ = 0;
            while(_loc49_ < _loc48_)
            {
               _loc50_ = _loc3_.readInt();
               _loc51_ = _loc3_.readInt();
               _loc46_.equipdSkills.add(_loc50_,_loc51_);
               _loc49_++;
            }
            _loc46_.Place = _loc45_;
            _loc7_.pets.add(_loc46_.Place,_loc46_);
            _loc17_++;
         }
         _loc15_.zoneName = _loc5_;
         _loc15_.currentWeapInfo.refineryLevel = _loc42_;
         var _loc18_:int = _loc3_.readInt();
         var _loc19_:int = _loc3_.readInt();
         var _loc20_:int = _loc3_.readInt();
         _loc15_.pos = new Point(_loc19_,_loc20_);
         _loc15_.energy = 1;
         _loc15_.direction = _loc3_.readInt();
         var _loc21_:int = _loc3_.readInt();
         var _loc22_:int = _loc3_.readInt();
         _loc15_.team = _loc3_.readInt();
         var _loc23_:int = _loc3_.readInt();
         if(_loc15_ is LocalPlayer)
         {
            (_loc15_ as LocalPlayer).deputyWeaponCount = _loc3_.readInt();
         }
         else
         {
            _loc52_ = _loc3_.readInt();
         }
         _loc15_.powerRatio = _loc3_.readInt();
         _loc15_.dander = _loc3_.readInt();
         _loc15_.maxBlood = _loc22_;
         _loc15_.updateBlood(_loc14_,0,0);
         _loc15_.wishKingCount = _loc3_.readInt();
         _loc15_.wishKingEnergy = _loc3_.readInt();
         _loc15_.currentWeapInfo.refineryLevel = _loc23_;
         var _loc24_:int = _loc3_.readInt();
         var _loc25_:int = 0;
         while(_loc25_ < _loc24_)
         {
            _loc53_ = BuffManager.creatBuff(_loc3_.readInt());
            _loc54_ = _loc3_.readInt();
            if(_loc53_)
            {
               _loc53_.data = _loc54_;
               _loc15_.addBuff(_loc53_);
            }
            _loc25_++;
         }
         var _loc26_:int = _loc3_.readInt();
         var _loc27_:Vector.<FightBuffInfo> = new Vector.<FightBuffInfo>();
         var _loc28_:int = 0;
         while(_loc28_ < _loc26_)
         {
            _loc55_ = BuffManager.creatBuff(_loc3_.readInt());
            _loc15_.outTurnBuffs.push(_loc55_);
            _loc28_++;
         }
         var _loc29_:Boolean = _loc3_.readBoolean();
         var _loc30_:Boolean = _loc3_.readBoolean();
         var _loc31_:Boolean = _loc3_.readBoolean();
         var _loc32_:Boolean = _loc3_.readBoolean();
         var _loc33_:int = _loc3_.readInt();
         var _loc34_:Dictionary = new Dictionary();
         var _loc35_:int = 0;
         while(_loc35_ < _loc33_)
         {
            _loc56_ = _loc3_.readUTF();
            _loc57_ = _loc3_.readUTF();
            _loc34_[_loc56_] = _loc57_;
            _loc35_++;
         }
         if(_loc29_)
         {
            _loc29_ = false;
         }
         _loc15_.isFrozen = _loc29_;
         _loc15_.isHidden = _loc30_;
         _loc15_.isNoNole = _loc31_;
         _loc15_.outProperty = _loc34_;
         if(RoomManager.Instance.current.type != 5 && _loc15_.playerInfo.currentPet)
         {
            _loc15_.currentPet = new Pet(_loc15_.playerInfo.currentPet);
            this.petResLoad(_loc15_.playerInfo.currentPet);
         }
         var _loc36_:Array = [_loc8_];
         LoadBombManager.Instance.loadFullRoomPlayersBomb(_loc36_);
         if(!_loc8_.isViewer)
         {
            _loc2_.addGamePlayer(_loc15_);
         }
         else
         {
            if(_loc8_.isSelf)
            {
               _loc2_.setSelfGamePlayer(_loc15_);
            }
            _loc2_.addGameViewer(_loc15_);
         }
      }
      
      protected function __selectObject(param1:CrazyTankSocketEvent) : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:Object = null;
         this.selectList = [];
         var _loc2_:int = param1.pkg.readInt();
         while(_loc3_ < _loc2_)
         {
            _loc4_ = param1.pkg.readInt();
            _loc5_ = param1.pkg.readInt();
            this.Current.getRoomPlayerByID(_loc4_,_loc5_).team = param1.pkg.readInt();
            _loc6_ = param1.pkg.readInt();
            _loc7_ = param1.pkg.readInt();
            _loc8_ = new Object();
            _loc8_["id"] = _loc4_;
            _loc8_["zoneID"] = _loc5_;
            _loc8_["selectID"] = _loc6_;
            _loc8_["selectZoneID"] = _loc7_;
            this.selectList.push(_loc8_);
            _loc3_++;
         }
         dispatchEvent(new GameEvent(GameEvent.SELECT_COMPLETE,null));
      }
      
      private function petResLoad(param1:PetInfo) : void
      {
         var _loc2_:int = 0;
         var _loc3_:PetSkillTemplateInfo = null;
         var _loc4_:BallInfo = null;
         if(param1)
         {
            LoadResourceManager.Instance.creatAndStartLoad(PathManager.solvePetGameAssetUrl(param1.GameAssetUrl),BaseLoader.MODULE_LOADER);
            for each(_loc2_ in param1.equipdSkills)
            {
               if(_loc2_ > 0)
               {
                  _loc3_ = PetSkillManager.getSkillByID(_loc2_);
                  if(_loc3_.EffectPic)
                  {
                     LoadResourceManager.Instance.creatAndStartLoad(PathManager.solvePetSkillEffect(_loc3_.EffectPic),BaseLoader.MODULE_LOADER);
                  }
                  if(_loc3_.NewBallID != -1)
                  {
                     _loc4_ = BallManager.findBall(_loc3_.NewBallID);
                     _loc4_.loadBombAsset();
                     _loc4_.loadCraterBitmap();
                  }
               }
            }
         }
      }
      
      private function __missionTryAgain(param1:CrazyTankSocketEvent) : void
      {
         this.TryAgain = param1.pkg.readInt();
         dispatchEvent(new GameEvent(GameEvent.MISSIONAGAIN,this.TryAgain));
      }
      
      private function __updatePlayInfoInGame(param1:CrazyTankSocketEvent) : void
      {
         var _loc11_:Player = null;
         var _loc2_:RoomInfo = RoomManager.Instance.current;
         if(_loc2_ == null)
         {
            return;
         }
         var _loc3_:PackageIn = param1.pkg;
         var _loc4_:int = _loc3_.readInt();
         var _loc5_:int = _loc3_.readInt();
         var _loc6_:int = _loc3_.readInt();
         var _loc7_:int = _loc3_.readInt();
         var _loc8_:int = _loc3_.readInt();
         var _loc9_:Boolean = _loc3_.readBoolean();
         var _loc10_:RoomPlayer = RoomManager.Instance.current.findPlayerByID(_loc5_);
         if(_loc4_ != PlayerManager.Instance.Self.ZoneID || _loc10_ == null || this.Current == null)
         {
            return;
         }
         if(_loc10_.isSelf)
         {
            _loc11_ = new LocalPlayer(PlayerManager.Instance.Self,_loc7_,_loc6_,_loc8_);
         }
         else
         {
            _loc11_ = new Player(_loc10_.playerInfo,_loc7_,_loc6_,_loc8_);
         }
         _loc11_.isReady = _loc9_;
         if(_loc11_.movie)
         {
            _loc11_.movie.setDefaultAction(_loc11_.movie.standAction);
         }
         this.Current.addRoomPlayer(_loc10_);
         if(_loc10_.isViewer)
         {
            this.Current.addGameViewer(_loc11_);
         }
         else
         {
            this.Current.addGamePlayer(_loc11_);
         }
         if(_loc10_.isSelf && this.Current.roomType != 20)
         {
            StateManager.setState(StateType.MISSION_ROOM);
         }
      }
      
      private function __missionInviteRoomInfo(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = null;
         var _loc3_:GameInfo = null;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:MissionInfo = null;
         var _loc7_:int = 0;
         var _loc8_:PlayerInfo = null;
         var _loc9_:RoomPlayer = null;
         var _loc10_:Boolean = false;
         var _loc11_:int = 0;
         var _loc12_:String = null;
         var _loc13_:String = null;
         var _loc14_:int = 0;
         var _loc15_:int = 0;
         var _loc16_:int = 0;
         var _loc17_:int = 0;
         var _loc18_:Boolean = false;
         var _loc19_:Player = null;
         if(RoomManager.Instance.current)
         {
            _loc2_ = param1.pkg;
            _loc3_ = new GameInfo();
            _loc3_.mapIndex = _loc2_.readInt();
            _loc3_.roomType = _loc2_.readInt();
            _loc3_.gameMode = _loc2_.readInt();
            _loc3_.timeType = _loc2_.readInt();
            RoomManager.Instance.current.timeType = _loc3_.timeType;
            _loc4_ = _loc2_.readInt();
            _loc5_ = 0;
            while(_loc5_ < _loc4_)
            {
               _loc7_ = _loc2_.readInt();
               _loc8_ = PlayerManager.Instance.findPlayer(_loc7_);
               _loc8_.beginChanges();
               _loc9_ = RoomManager.Instance.current.findPlayerByID(_loc7_);
               if(_loc9_ == null)
               {
                  _loc9_ = new RoomPlayer(_loc8_);
                  _loc8_.ID = _loc7_;
               }
               _loc8_.ZoneID = PlayerManager.Instance.Self.ZoneID;
               _loc8_.NickName = _loc2_.readUTF();
               _loc10_ = _loc2_.readBoolean();
               _loc8_.typeVIP = _loc2_.readByte();
               _loc8_.VIPLevel = _loc2_.readInt();
               _loc8_.Sex = _loc2_.readBoolean();
               _loc8_.Hide = _loc2_.readInt();
               _loc8_.Style = _loc2_.readUTF();
               _loc8_.Colors = _loc2_.readUTF();
               _loc8_.Skin = _loc2_.readUTF();
               _loc8_.Grade = _loc2_.readInt();
               _loc8_.Repute = _loc2_.readInt();
               _loc8_.WeaponID = _loc2_.readInt();
               if(_loc8_.WeaponID > 0)
               {
                  _loc11_ = _loc2_.readInt();
                  _loc12_ = _loc2_.readUTF();
                  _loc13_ = _loc2_.readDateString();
               }
               _loc8_.DeputyWeaponID = _loc2_.readInt();
               _loc8_.ConsortiaID = _loc2_.readInt();
               _loc8_.ConsortiaName = _loc2_.readUTF();
               _loc8_.badgeID = _loc2_.readInt();
               _loc14_ = _loc2_.readInt();
               _loc15_ = _loc2_.readInt();
               _loc8_.DailyLeagueFirst = _loc2_.readBoolean();
               _loc8_.DailyLeagueLastScore = _loc2_.readInt();
               _loc8_.commitChanges();
               _loc9_.team = _loc2_.readInt();
               _loc3_.addRoomPlayer(_loc9_);
               _loc16_ = _loc2_.readInt();
               _loc17_ = _loc2_.readInt();
               _loc18_ = _loc2_.readBoolean();
               if(_loc9_.isSelf)
               {
                  _loc19_ = new LocalPlayer(PlayerManager.Instance.Self,_loc16_,_loc9_.team,_loc17_);
               }
               else
               {
                  _loc19_ = new Player(_loc9_.playerInfo,_loc16_,_loc9_.team,_loc17_);
               }
               _loc19_.isReady = _loc18_;
               _loc19_.currentWeapInfo.refineryLevel = _loc11_;
               if(!_loc10_)
               {
                  _loc3_.addGamePlayer(_loc19_);
               }
               else
               {
                  if(_loc9_.isSelf)
                  {
                     _loc3_.setSelfGamePlayer(_loc19_);
                  }
                  _loc3_.addGameViewer(_loc19_);
               }
               _loc5_++;
            }
            this.Current = _loc3_;
            _loc6_ = new MissionInfo();
            _loc6_.name = _loc2_.readUTF();
            _loc6_.pic = _loc2_.readUTF();
            _loc6_.success = _loc2_.readUTF();
            _loc6_.failure = _loc2_.readUTF();
            _loc6_.description = _loc2_.readUTF();
            _loc6_.totalMissiton = _loc2_.readInt();
            _loc6_.missionIndex = _loc2_.readInt();
            _loc6_.nextMissionIndex = _loc6_.missionIndex + 1;
            this.Current.missionInfo = _loc6_;
            this.Current.hasNextMission = true;
         }
      }
      
      private function __createGame(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = null;
         if(RoomManager.Instance.current)
         {
            _loc2_ = param1.pkg;
            this.createGameInfo(_loc2_);
         }
      }
      
      private function createGameInfo(param1:PackageIn, param2:Boolean = false) : void
      {
         var _loc6_:int = 0;
         var _loc7_:String = null;
         var _loc8_:int = 0;
         var _loc9_:PlayerInfo = null;
         var _loc10_:RoomPlayer = null;
         var _loc11_:String = null;
         var _loc12_:Boolean = false;
         var _loc13_:Boolean = false;
         var _loc14_:int = 0;
         var _loc15_:int = 0;
         var _loc16_:int = 0;
         var _loc17_:int = 0;
         var _loc18_:Player = null;
         var _loc19_:int = 0;
         var _loc20_:int = 0;
         var _loc21_:int = 0;
         var _loc22_:String = null;
         var _loc23_:String = null;
         var _loc24_:int = 0;
         var _loc25_:PetInfo = null;
         var _loc26_:int = 0;
         var _loc27_:int = 0;
         var _loc28_:int = 0;
         var _loc29_:int = 0;
         var _loc30_:int = 0;
         var _loc3_:GameInfo = new GameInfo();
         _loc3_.roomType = param1.readInt();
         _loc3_.gameMode = param1.readInt();
         if(_loc3_.gameMode == 20)
         {
            _loc3_.roomType = 18;
         }
         if(_loc3_.roomType == 20 || _loc3_.roomType == 24)
         {
            _loc3_.missionInfo = new MissionInfo();
         }
         else if(_loc3_.gameMode == RoomInfo.TRANSNATIONALFIGHT_ROOM)
         {
            RoomManager.Instance.current.type = _loc3_.gameMode;
         }
         if(_loc3_.gameMode == RoomInfo.CONSORTIA_MATCH_SCORE || _loc3_.gameMode == RoomInfo.CONSORTIA_MATCH_RANK || _loc3_.gameMode == RoomInfo.CONSORTIA_MATCH_SCORE_WHOLE || _loc3_.gameMode == RoomInfo.CONSORTIA_MATCH_RANK_WHOLE)
         {
            RoomManager.Instance.current.type = _loc3_.gameMode;
         }
         if(_loc3_.gameMode == RoomInfo.FIGHTFOOTBALLTIME_ROOM)
         {
            RoomManager.Instance.current.type = _loc3_.gameMode;
         }
         _loc3_.timeType = param1.readInt();
         RoomManager.Instance.current.timeType = _loc3_.timeType;
         var _loc4_:int = param1.readInt();
         var _loc5_:int = 0;
         while(_loc5_ < _loc4_)
         {
            _loc6_ = param1.readInt();
            _loc7_ = param1.readUTF();
            _loc8_ = param1.readInt();
            _loc9_ = PlayerManager.Instance.findPlayer(_loc8_,_loc6_);
            _loc9_.beginChanges();
            _loc10_ = RoomManager.Instance.current.findPlayerByID(_loc8_,_loc6_);
            if(_loc10_ == null)
            {
               _loc10_ = new RoomPlayer(_loc9_);
            }
            _loc9_.ID = _loc8_;
            _loc9_.ZoneID = _loc6_;
            _loc11_ = param1.readUTF();
            _loc12_ = param1.readBoolean();
            if(_loc12_ && _loc10_.place < 8)
            {
               _loc10_.place = 8;
            }
            if(!(_loc10_ is SelfInfo))
            {
               _loc9_.NickName = _loc11_;
            }
            _loc9_.typeVIP = param1.readByte();
            _loc9_.VIPLevel = param1.readInt();
            if(PlayerManager.Instance.isChangeStyleTemp(_loc9_.ID))
            {
               param1.readBoolean();
               param1.readInt();
               param1.readUTF();
               param1.readUTF();
               param1.readUTF();
            }
            else
            {
               _loc9_.Sex = param1.readBoolean();
               _loc9_.Hide = param1.readInt();
               _loc9_.Style = param1.readUTF();
               _loc9_.Colors = param1.readUTF();
               _loc9_.Skin = param1.readUTF();
            }
            _loc9_.Grade = param1.readInt();
            _loc9_.Repute = param1.readInt();
            _loc9_.WeaponID = param1.readInt();
            if(_loc9_.WeaponID != 0)
            {
               _loc21_ = param1.readInt();
               _loc22_ = param1.readUTF();
               _loc23_ = param1.readDateString();
            }
            if(_loc9_.isSelf && _loc3_.gameMode == RoomInfo.TRANSNATIONALFIGHT_ROOM)
            {
               _loc9_.snapDeputyWeaponID = param1.readInt();
            }
            else
            {
               _loc9_.DeputyWeaponID = param1.readInt();
            }
            _loc9_.pvpBadgeId = param1.readInt();
            _loc9_.Nimbus = param1.readInt();
            _loc13_ = param1.readBoolean();
            _loc9_.ConsortiaID = param1.readInt();
            _loc9_.ConsortiaName = param1.readUTF();
            _loc9_.badgeID = param1.readInt();
            _loc14_ = param1.readInt();
            _loc15_ = param1.readInt();
            _loc9_.WinCount = param1.readInt();
            _loc9_.TotalCount = param1.readInt();
            _loc9_.FightPower = param1.readInt();
            _loc9_.apprenticeshipState = param1.readInt();
            _loc9_.masterID = param1.readInt();
            _loc9_.setMasterOrApprentices(param1.readUTF());
            _loc9_.AchievementPoint = param1.readInt();
            _loc9_.honor = param1.readUTF();
            _loc9_.Offer = param1.readInt();
            _loc9_.DailyLeagueFirst = param1.readBoolean();
            _loc9_.DailyLeagueLastScore = param1.readInt();
            _loc9_.fightStatus = 0;
            _loc9_.isTrusteeship = false;
            _loc9_.commitChanges();
            _loc10_.playerInfo.IsMarried = param1.readBoolean();
            if(_loc10_.playerInfo.IsMarried)
            {
               _loc10_.playerInfo.SpouseID = param1.readInt();
               _loc10_.playerInfo.SpouseName = param1.readUTF();
            }
            _loc10_.additionInfo.resetAddition();
            _loc10_.additionInfo.GMExperienceAdditionType = Number(param1.readInt() / 100);
            _loc10_.additionInfo.AuncherExperienceAddition = Number(param1.readInt() / 100);
            _loc10_.additionInfo.GMOfferAddition = Number(param1.readInt() / 100);
            _loc10_.additionInfo.AuncherOfferAddition = Number(param1.readInt() / 100);
            _loc10_.additionInfo.GMRichesAddition = Number(param1.readInt() / 100);
            _loc10_.additionInfo.AuncherRichesAddition = Number(param1.readInt() / 100);
            _loc10_.team = param1.readInt();
            _loc3_.addRoomPlayer(_loc10_);
            _loc16_ = param1.readInt();
            if(param2)
            {
               _loc16_ = _loc5_;
            }
            _loc17_ = param1.readInt();
            if(_loc10_.isSelf)
            {
               _loc18_ = new LocalPlayer(PlayerManager.Instance.Self,_loc16_,_loc10_.team,_loc17_);
            }
            else
            {
               _loc18_ = new Player(_loc10_.playerInfo,_loc16_,_loc10_.team,_loc17_);
            }
            _loc19_ = param1.readInt();
            if(_loc3_.gameMode == RoomInfo.TRANSNATIONALFIGHT_ROOM && _loc9_.isSelf)
            {
               _loc9_.pets.clear();
            }
            _loc20_ = 0;
            while(_loc20_ < _loc19_)
            {
               _loc24_ = param1.readInt();
               _loc25_ = _loc9_.pets[_loc24_];
               _loc26_ = param1.readInt();
               if(_loc3_.gameMode == RoomInfo.TRANSNATIONALFIGHT_ROOM)
               {
                  _loc25_ = new PetInfo();
                  _loc25_.TemplateID = _loc26_;
                  PetInfoManager.fillPetInfo(_loc25_);
               }
               else
               {
                  _loc25_ = _loc9_.pets[_loc24_];
                  if(_loc25_ == null)
                  {
                     _loc25_ = new PetInfo();
                     _loc25_.TemplateID = _loc26_;
                     PetInfoManager.fillPetInfo(_loc25_);
                  }
               }
               _loc25_.ID = param1.readInt();
               _loc25_.Name = param1.readUTF();
               _loc25_.UserID = param1.readInt();
               _loc25_.Level = param1.readInt();
               _loc25_.IsEquip = true;
               _loc25_.clearEquipedSkills();
               _loc27_ = param1.readInt();
               _loc28_ = 0;
               while(_loc28_ < _loc27_)
               {
                  _loc29_ = param1.readInt();
                  _loc30_ = param1.readInt();
                  _loc25_.equipdSkills.add(_loc29_,_loc30_);
                  _loc28_++;
               }
               _loc25_.Place = _loc24_;
               _loc9_.pets.add(_loc25_.Place,_loc25_);
               if(RoomManager.Instance.isTransnationalFight())
               {
                  _loc9_.flagID = param1.readInt();
               }
               if(_loc9_.isSelf && _loc3_.gameMode == RoomInfo.TRANSNATIONALFIGHT_ROOM)
               {
                  _loc9_.snapPet = _loc25_;
               }
               _loc20_++;
            }
            _loc18_.zoneName = _loc7_;
            _loc18_.currentWeapInfo.refineryLevel = _loc21_;
            if(!_loc10_.isViewer)
            {
               _loc3_.addGamePlayer(_loc18_);
            }
            else
            {
               if(_loc10_.isSelf)
               {
                  _loc3_.setSelfGamePlayer(_loc18_);
               }
               _loc3_.addGameViewer(_loc18_);
            }
            _loc5_++;
         }
         this._current = _loc3_;
         if(!param2)
         {
            QueueManager.setLifeTime(0);
         }
         RingStationManager.instance.RoomType = _loc3_.roomType;
         CatchBeastManager.instance.RoomType = _loc3_.roomType;
      }
      
      private function __singleBattleStartMatch(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = null;
         if(RoomManager.Instance.current)
         {
            _loc2_ = param1.pkg;
            this.createGameInfo(_loc2_,true);
            dispatchEvent(new Event(START_MATCH));
         }
      }
      
      private function __buffObtain(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = null;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:Boolean = false;
         var _loc7_:Date = null;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:BuffInfo = null;
         if(this.Current)
         {
            _loc2_ = param1.pkg;
            if(_loc2_.extend1 == this.Current.selfGamePlayer.LivingID)
            {
               return;
            }
            if(this.Current.findPlayer(_loc2_.extend1) != null)
            {
               _loc3_ = _loc2_.readInt();
               _loc4_ = 0;
               while(_loc4_ < _loc3_)
               {
                  _loc5_ = _loc2_.readInt();
                  _loc6_ = _loc2_.readBoolean();
                  _loc7_ = _loc2_.readDate();
                  _loc8_ = _loc2_.readInt();
                  _loc9_ = _loc2_.readInt();
                  _loc10_ = new BuffInfo(_loc5_,_loc6_,_loc7_,_loc8_,_loc9_);
                  this.Current.findPlayer(_loc2_.extend1).playerInfo.buffInfo.add(_loc10_.Type,_loc10_);
                  _loc4_++;
               }
               param1.stopImmediatePropagation();
            }
         }
      }
      
      private function __buffUpdate(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = null;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:Boolean = false;
         var _loc6_:Date = null;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:BuffInfo = null;
         if(this.Current)
         {
            _loc2_ = param1.pkg;
            if(_loc2_.extend1 == this.Current.selfGamePlayer.LivingID)
            {
               return;
            }
            if(this.Current.findPlayer(_loc2_.extend1) != null)
            {
               _loc3_ = _loc2_.readInt();
               _loc4_ = _loc2_.readInt();
               _loc5_ = _loc2_.readBoolean();
               _loc6_ = _loc2_.readDate();
               _loc7_ = _loc2_.readInt();
               _loc8_ = _loc2_.readInt();
               _loc9_ = new BuffInfo(_loc4_,_loc5_,_loc6_,_loc7_,_loc8_);
               if(_loc5_)
               {
                  this.Current.findPlayer(_loc2_.extend1).playerInfo.buffInfo.add(_loc9_.Type,_loc9_);
               }
               else
               {
                  this.Current.findPlayer(_loc2_.extend1).playerInfo.buffInfo.remove(_loc9_.Type);
               }
               param1.stopImmediatePropagation();
            }
         }
      }
      
      private function __beginLoad(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:GameNeedMovieInfo = null;
         var _loc7_:GameNeedPetSkillInfo = null;
         StateManager.getInGame_Step_3 = true;
         this._recevieLoadSocket = true;
         if(this.Current)
         {
            StateManager.getInGame_Step_4 = true;
            this.Current.maxTime = param1.pkg.readInt();
            this.Current.mapIndex = param1.pkg.readInt();
            _loc2_ = param1.pkg.readInt();
            _loc3_ = 1;
            while(_loc3_ <= _loc2_)
            {
               _loc6_ = new GameNeedMovieInfo();
               _loc6_.type = param1.pkg.readInt();
               _loc6_.path = param1.pkg.readUTF();
               _loc6_.classPath = param1.pkg.readUTF();
               this.Current.neededMovies.push(_loc6_);
               _loc3_++;
            }
            _loc4_ = param1.pkg.readInt();
            _loc5_ = 0;
            while(_loc5_ < _loc4_)
            {
               _loc7_ = new GameNeedPetSkillInfo();
               _loc7_.pic = param1.pkg.readUTF();
               _loc7_.effect = param1.pkg.readUTF();
               this.Current.neededPetSkillResource.push(_loc7_);
               _loc5_++;
            }
         }
         this.checkCanToLoader();
      }
      
      private function checkCanToLoader() : void
      {
         if(this._recevieLoadSocket && this.Current && (this.Current.missionInfo || !this.getRoomTypeNeedMissionInfo(this.Current.roomType)))
         {
            dispatchEvent(new Event(START_LOAD));
            StateManager.getInGame_Step_5 = true;
            this._recevieLoadSocket = false;
         }
      }
      
      private function getRoomTypeNeedMissionInfo(param1:int) : Boolean
      {
         return param1 == 2 || param1 == 3 || param1 == 4 || param1 == 5 || param1 == 8 || param1 == 10 || param1 == 11 || param1 == 14 || param1 == 17 || param1 == 20 || param1 == 21;
      }
      
      private function __gameMissionStart(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:Object = new Object();
         _loc3_.id = _loc2_.clientId;
         var _loc4_:Boolean = _loc2_.readBoolean();
      }
      
      public function dispatchAllGameReadyState(param1:Array) : void
      {
         var _loc2_:CrazyTankSocketEvent = null;
         var _loc3_:PackageIn = null;
         var _loc4_:Object = null;
         var _loc5_:int = 0;
         var _loc6_:Player = null;
         var _loc7_:RoomPlayer = null;
         for each(_loc2_ in param1)
         {
            _loc3_ = _loc2_.pkg;
            _loc4_ = new Object();
            _loc5_ = _loc3_.clientId;
            if(this.Current)
            {
               _loc6_ = this.Current.findPlayerByPlayerID(_loc5_);
               _loc6_.isReady = _loc3_.readBoolean();
               if(!_loc6_.isSelf && _loc6_.isReady)
               {
                  _loc7_ = RoomManager.Instance.current.findPlayerByID(_loc5_);
                  _loc7_.isReady = true;
               }
            }
            _loc3_.position = SocketManager.PACKAGE_CONTENT_START_INDEX;
         }
      }
      
      private function __gameMissionPrepare(param1:CrazyTankSocketEvent) : void
      {
         if(RoomManager.Instance.current)
         {
            RoomManager.Instance.current.setPlayerReadyState(param1.pkg.clientId,param1.pkg.readBoolean());
         }
      }
      
      private function __gameMissionInfo(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:String = null;
         var _loc3_:MissionInfo = null;
         if(this.Current == null)
         {
            return;
         }
         if(!this.Current.missionInfo)
         {
            _loc3_ = this.Current.missionInfo = new MissionInfo();
         }
         else
         {
            _loc3_ = this.Current.missionInfo;
         }
         _loc3_.name = param1.pkg.readUTF();
         _loc3_.success = param1.pkg.readUTF();
         _loc3_.failure = param1.pkg.readUTF();
         _loc3_.description = param1.pkg.readUTF();
         _loc2_ = param1.pkg.readUTF();
         _loc3_.totalMissiton = param1.pkg.readInt();
         _loc3_.missionIndex = param1.pkg.readInt();
         _loc3_.totalValue1 = param1.pkg.readInt();
         _loc3_.totalValue2 = param1.pkg.readInt();
         _loc3_.totalValue3 = param1.pkg.readInt();
         _loc3_.totalValue4 = param1.pkg.readInt();
         _loc3_.nextMissionIndex = _loc3_.missionIndex + 1;
         _loc3_.parseString(_loc2_);
         _loc3_.tryagain = param1.pkg.readInt();
         _loc3_.pic = param1.pkg.readUTF();
         this.checkCanToLoader();
      }
      
      private function __loadprogress(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:RoomPlayer = null;
         if(this.Current)
         {
            _loc2_ = param1.pkg.readInt();
            _loc3_ = param1.pkg.readInt();
            _loc4_ = param1.pkg.readInt();
            _loc5_ = this.Current.findRoomPlayer(_loc4_,_loc3_);
            if(_loc5_ && !_loc5_.isSelf)
            {
               _loc5_.progress = _loc2_;
            }
         }
      }
      
      private function __gameStart(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = null;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:uint = 0;
         var _loc9_:int = 0;
         var _loc10_:Player = null;
         var _loc11_:int = 0;
         var _loc12_:int = 0;
         var _loc13_:int = 0;
         var _loc14_:int = 0;
         var _loc15_:int = 0;
         var _loc16_:int = 0;
         var _loc17_:Vector.<FightBuffInfo> = null;
         var _loc18_:int = 0;
         var _loc19_:Boolean = false;
         var _loc20_:Boolean = false;
         var _loc21_:Boolean = false;
         var _loc22_:Boolean = false;
         var _loc23_:int = 0;
         var _loc24_:Dictionary = null;
         var _loc25_:int = 0;
         var _loc26_:int = 0;
         var _loc27_:FightBuffInfo = null;
         var _loc28_:int = 0;
         var _loc29_:FightBuffInfo = null;
         var _loc30_:String = null;
         var _loc31_:String = null;
         var _loc32_:Bomb = null;
         var _loc33_:SimpleBoxInfo = null;
         this.TryAgain = -1;
         ExpTweenManager.Instance.deleteTweens();
         if(this.Current)
         {
            param1.executed = false;
            _loc2_ = param1.pkg;
            _loc3_ = _loc2_.readInt();
            _loc4_ = 1;
            while(_loc4_ <= _loc3_)
            {
               _loc9_ = _loc2_.readInt();
               _loc10_ = this.Current.findPlayer(_loc9_);
               if(_loc10_ != null)
               {
                  _loc10_.reset();
                  _loc10_.pos = new Point(_loc2_.readInt(),_loc2_.readInt());
                  _loc10_.energy = 1;
                  _loc10_.direction = _loc2_.readInt();
                  _loc11_ = _loc2_.readInt();
                  _loc12_ = _loc2_.readInt();
                  _loc10_.team = _loc2_.readInt();
                  _loc13_ = _loc2_.readInt();
                  if(_loc10_ is LocalPlayer)
                  {
                     (_loc10_ as LocalPlayer).deputyWeaponCount = _loc2_.readInt();
                  }
                  else
                  {
                     _loc26_ = _loc2_.readInt();
                  }
                  _loc10_.powerRatio = _loc2_.readInt();
                  _loc10_.dander = _loc2_.readInt();
                  _loc10_.maxBlood = _loc12_;
                  _loc10_.updateBlood(_loc11_,0,0);
                  _loc10_.wishKingCount = _loc2_.readInt();
                  _loc10_.wishKingEnergy = _loc2_.readInt();
                  _loc10_.currentWeapInfo.refineryLevel = _loc13_;
                  _loc14_ = _loc2_.readInt();
                  _loc15_ = 0;
                  while(_loc15_ < _loc14_)
                  {
                     _loc27_ = BuffManager.creatBuff(_loc2_.readInt());
                     _loc28_ = _loc2_.readInt();
                     if(_loc27_)
                     {
                        _loc27_.data = _loc28_;
                        _loc10_.addBuff(_loc27_);
                     }
                     _loc15_++;
                  }
                  _loc16_ = _loc2_.readInt();
                  _loc17_ = new Vector.<FightBuffInfo>();
                  _loc18_ = 0;
                  while(_loc18_ < _loc16_)
                  {
                     _loc29_ = BuffManager.creatBuff(_loc2_.readInt());
                     _loc10_.outTurnBuffs.push(_loc29_);
                     _loc18_++;
                  }
                  _loc19_ = _loc2_.readBoolean();
                  _loc20_ = _loc2_.readBoolean();
                  _loc21_ = _loc2_.readBoolean();
                  _loc22_ = _loc2_.readBoolean();
                  _loc23_ = _loc2_.readInt();
                  _loc24_ = new Dictionary();
                  _loc25_ = 0;
                  while(_loc25_ < _loc23_)
                  {
                     _loc30_ = _loc2_.readUTF();
                     _loc31_ = _loc2_.readUTF();
                     _loc24_[_loc30_] = _loc31_;
                     _loc25_++;
                  }
                  _loc10_.isFrozen = _loc19_;
                  _loc10_.isHidden = _loc20_;
                  _loc10_.isNoNole = _loc21_;
                  _loc10_.outProperty = _loc24_;
                  if(RoomManager.Instance.current.type != 5 && _loc10_.playerInfo.currentPet)
                  {
                     if(RoomManager.Instance.current.type == RoomInfo.TRANSNATIONALFIGHT_ROOM && _loc10_.playerInfo.snapPet)
                     {
                        _loc10_.currentPet = new Pet(_loc10_.playerInfo.snapPet);
                     }
                     else if(RoomManager.Instance.current.type != 5 && _loc10_.playerInfo.currentPet)
                     {
                        _loc10_.currentPet = new Pet(_loc10_.playerInfo.currentPet);
                     }
                  }
               }
               _loc4_++;
            }
            _loc5_ = _loc2_.readInt();
            _loc6_ = 0;
            while(_loc6_ < _loc5_)
            {
               _loc32_ = new Bomb();
               _loc32_.Id = _loc2_.readInt();
               _loc32_.X = _loc2_.readInt();
               _loc32_.Y = _loc2_.readInt();
               this.Current.outBombs.add(_loc6_,_loc32_);
               _loc6_++;
            }
            _loc7_ = _loc2_.readInt();
            _loc8_ = 0;
            while(_loc8_ < _loc7_)
            {
               _loc33_ = new SimpleBoxInfo();
               _loc33_.bid = _loc2_.readInt();
               _loc33_.bx = _loc2_.readInt();
               _loc33_.by = _loc2_.readInt();
               _loc33_.subType = _loc2_.readInt();
               this.Current.outBoxs.add(_loc33_.bid,_loc33_);
               _loc8_++;
            }
            this.Current.startTime = _loc2_.readDate();
            if(RoomManager.Instance.current.type == 5)
            {
               StateManager.setState(StateType.FIGHT_LIB_GAMEVIEW,this.Current);
               if(PathManager.isStatistics)
               {
                  WeakGuildManager.Instance.statistics(4,TimeManager.Instance.enterFightTime);
               }
            }
            else if(RoomManager.Instance.current.type == RoomInfo.FRESHMAN_ROOM)
            {
               if(StartupResourceLoader.firstEnterHall)
               {
                  StateManager.setState(StateType.TRAINER2,this.Current);
               }
               else
               {
                  StateManager.setState(StateType.TRAINER1,this.Current);
               }
               if(PathManager.isStatistics)
               {
                  WeakGuildManager.Instance.statistics(4,TimeManager.Instance.enterFightTime);
               }
            }
            else if(_loc3_ == 0)
            {
               if(RoomManager.Instance.current.type == RoomInfo.DUNGEON_ROOM)
               {
                  StateManager.setState(StateType.DUNGEON_LIST);
               }
               else
               {
                  StateManager.setState(StateType.ROOM_LIST);
               }
            }
            else
            {
               StateManager.setState(StateType.FIGHTING,this.Current);
               this.Current.IsOneOnOne = _loc3_ == 2;
               if(PathManager.isStatistics)
               {
                  WeakGuildManager.Instance.statistics(4,TimeManager.Instance.enterFightTime);
               }
            }
            RoomManager.Instance.resetAllPlayerState();
         }
         CampBattleManager.instance.model.isFighting = true;
      }
      
      private function __missionAllOver(param1:CrazyTankSocketEvent) : void
      {
         var _loc4_:int = 0;
         var _loc7_:Object = null;
         var _loc8_:int = 0;
         var _loc9_:Player = null;
         var _loc10_:SelfInfo = null;
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readInt();
         if(this.Current == null)
         {
            return;
         }
         while(_loc4_ < _loc3_)
         {
            _loc8_ = _loc2_.readInt();
            _loc9_ = this.Current.findGamerbyPlayerId(_loc8_);
            if(_loc9_.expObj)
            {
               _loc7_ = _loc9_.expObj;
            }
            else
            {
               _loc7_ = new Object();
            }
            if(_loc9_)
            {
               _loc7_.killGP = _loc2_.readInt();
               _loc7_.hertGP = _loc2_.readInt();
               _loc7_.fightGP = _loc2_.readInt();
               _loc7_.ghostGP = _loc2_.readInt();
               _loc7_.gpForVIP = _loc2_.readInt();
               _loc7_.gpForSpouse = _loc2_.readInt();
               _loc7_.gpForServer = _loc2_.readInt();
               _loc7_.gpForApprenticeOnline = _loc2_.readInt();
               _loc7_.gpForApprenticeTeam = _loc2_.readInt();
               _loc7_.gpForDoubleCard = _loc2_.readInt();
               _loc7_.consortiaSkill = _loc2_.readInt();
               _loc7_.luckyExp = _loc2_.readInt();
               _loc7_.gainGP = _loc2_.readInt();
               _loc7_.gpCSMUser = _loc2_.readInt();
               _loc9_.isWin = _loc2_.readBoolean();
               _loc9_.expObj = _loc7_;
            }
            _loc4_++;
         }
         if(PathManager.solveExternalInterfaceEnabel() && this.Current.selfGamePlayer.isWin)
         {
            _loc10_ = PlayerManager.Instance.Self;
         }
         this.Current.missionInfo.missionOverNPCMovies = [];
         var _loc5_:int = _loc2_.readInt();
         var _loc6_:int = 0;
         while(_loc6_ < _loc5_)
         {
            this.Current.missionInfo.missionOverNPCMovies.push(_loc2_.readUTF());
            _loc6_++;
         }
      }
      
      private function __takeOut(param1:CrazyTankSocketEvent) : void
      {
         if(this.Current)
         {
            this.Current.resultCard.push(param1);
         }
      }
      
      private function __showAllCard(param1:CrazyTankSocketEvent) : void
      {
         if(this.Current)
         {
            this.Current.showAllCard.push(param1);
         }
      }
      
      public function reset() : void
      {
         if(this.Current)
         {
            this.Current.dispose();
            this.Current = null;
         }
      }
      
      public function startLoading() : void
      {
         StateManager.setState(StateType.GAME_LOADING);
      }
      
      public function dispatchEnterRoom() : void
      {
         dispatchEvent(new Event(ENTER_ROOM));
      }
      
      public function dispatchLeaveMission() : void
      {
         dispatchEvent(new Event(LEAVE_MISSION));
      }
      
      public function dispatchPaymentConfirm() : void
      {
         dispatchEvent(new Event(PLAYER_CLICK_PAY));
      }
      
      public function selfGetItemShowAndSound(param1:Dictionary) : Boolean
      {
         var _loc3_:InventoryItemInfo = null;
         var _loc4_:ChatData = null;
         var _loc5_:String = null;
         var _loc6_:Array = null;
         var _loc7_:String = null;
         var _loc2_:Boolean = false;
         for each(_loc3_ in param1)
         {
            _loc4_ = new ChatData();
            _loc4_.channel = ChatInputView.SYS_NOTICE;
            _loc5_ = LanguageMgr.GetTranslation("tank.data.player.FightingPlayerInfo.your");
            _loc6_ = ChatFormats.getTagsByChannel(_loc4_);
            _loc7_ = ChatFormats.creatGoodTag(_loc3_.Property1 != "31"?"[" + _loc3_.Name + "]":"[" + _loc3_.Name + "-" + BeadTemplateManager.Instance.GetBeadInfobyID(_loc3_.TemplateID).Name + "Ур." + BeadTemplateManager.Instance.GetBeadInfobyID(_loc3_.TemplateID).BaseLevel + "]",ChatFormats.CLICK_GOODS,_loc3_.TemplateID,_loc3_.Quality,_loc3_.IsBinds,_loc4_);
            _loc4_.htmlMessage = _loc6_[0] + _loc5_ + _loc7_ + _loc6_[1] + "<BR>";
            ChatManager.Instance.chat(_loc4_,false);
            if(_loc3_.Quality >= 3)
            {
               _loc2_ = true;
            }
         }
         return _loc2_;
      }
      
      public function isIdenticalGame(param1:int = 0) : Boolean
      {
         var _loc4_:RoomPlayer = null;
         var _loc2_:DictionaryData = RoomManager.Instance.current.players;
         var _loc3_:SelfInfo = PlayerManager.Instance.Self;
         if(param1 == _loc3_.ID)
         {
            return false;
         }
         for each(_loc4_ in _loc2_)
         {
            if(_loc4_.playerInfo.ID == param1 && _loc4_.playerInfo.ZoneID == _loc3_.ZoneID)
            {
               return true;
            }
         }
         return false;
      }
      
      private function __loadResource(param1:CrazyTankSocketEvent) : void
      {
         var _loc4_:GameNeedMovieInfo = null;
         this.setLoaderStop();
         var _loc2_:int = param1.pkg.readInt();
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_)
         {
            _loc4_ = new GameNeedMovieInfo();
            _loc4_.type = param1.pkg.readInt();
            _loc4_.path = param1.pkg.readUTF();
            _loc4_.classPath = param1.pkg.readUTF();
            _loc4_.startLoad();
            _loc3_++;
         }
      }
      
      private function setLoaderStop() : void
      {
         var _loc1_:int = this._loaderArray.length;
         var _loc2_:int = 0;
         while(_loc2_ < _loc1_)
         {
            if(!(this._loaderArray[_loc2_] as BaseLoader).isComplete)
            {
               (this._loaderArray[_loc2_] as BaseLoader).unload();
            }
            _loc2_++;
         }
         this._loaderArray.length = 0;
      }
      
      public function get numCreater() : BloodNumberCreater
      {
         if(this._numCreater)
         {
            return this._numCreater;
         }
         this._numCreater = new BloodNumberCreater();
         return this._numCreater;
      }
      
      public function disposeNumCreater() : void
      {
         if(this._numCreater)
         {
            this._numCreater.dispose();
         }
         this._numCreater = null;
      }
      
      public function get gameView() : GameView
      {
         return this._gameView;
      }
      
      public function set gameView(param1:GameView) : void
      {
         this._gameView = param1;
      }
      
      public function get addLivingEvtVec() : Vector.<CrazyTankSocketEvent>
      {
         return this._addLivingEvtVec;
      }
      
      public function get setPropertyEvtVec() : Vector.<CrazyTankSocketEvent>
      {
         return this._setPropertyEvtVec;
      }
      
      public function get livingFallingEvtVec() : Vector.<CrazyTankSocketEvent>
      {
         return this._livingFallingEvtVec;
      }
      
      public function get livingShowBloodEvtVec() : Vector.<CrazyTankSocketEvent>
      {
         return this._livingShowBloodEvtVec;
      }
   }
}
