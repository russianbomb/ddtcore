package game.actions
{
   import com.pickgliss.utils.ClassUtils;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.SoundManager;
   import fightFootballTime.manager.FightFootballTimeManager;
   import flash.display.MovieClip;
   import game.GameManager;
   import game.model.GameInfo;
   import game.model.Living;
   import game.model.LocalPlayer;
   import game.model.Player;
   import game.view.experience.ExpView;
   import game.view.map.MapView;
   import road7th.comm.PackageIn;
   import road7th.utils.MovieClipWrapper;
   import room.RoomManager;
   import room.model.RoomInfo;
   
   public class GameOverAction extends BaseAction
   {
       
      
      private var _event:CrazyTankSocketEvent;
      
      private var _executed:Boolean;
      
      private var _count:int;
      
      private var _map:MapView;
      
      private var _current:GameInfo;
      
      private var _func:Function;
      
      public function GameOverAction(param1:MapView, param2:CrazyTankSocketEvent, param3:Function, param4:Number = 3000)
      {
         super();
         this._func = param3;
         this._event = param2;
         this._map = param1;
         this._count = param4 / 40;
         this._current = GameManager.Instance.Current;
         this.readInfo(param2);
         if(RoomManager.Instance.current.selfRoomPlayer.isViewer)
         {
            this._executed = true;
         }
      }
      
      private function readInfo(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = null;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:Living = null;
         var _loc7_:int = 0;
         var _loc8_:Boolean = false;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:Object = null;
         var _loc12_:Player = null;
         var _loc13_:int = 0;
         var _loc14_:int = 0;
         if(this._current)
         {
            _loc2_ = param1.pkg;
            _loc3_ = _loc2_.readInt();
            _loc4_ = _loc2_.readInt();
            _loc5_ = 0;
            while(_loc5_ < _loc4_)
            {
               _loc7_ = _loc2_.readInt();
               _loc8_ = _loc2_.readBoolean();
               _loc9_ = _loc2_.readInt();
               _loc10_ = _loc2_.readInt();
               _loc11_ = {};
               _loc11_.killGP = _loc2_.readInt();
               _loc11_.hertGP = _loc2_.readInt();
               _loc11_.fightGP = _loc2_.readInt();
               _loc11_.ghostGP = _loc2_.readInt();
               _loc11_.gpForVIP = _loc2_.readInt();
               _loc11_.gpForConsortia = _loc2_.readInt();
               _loc11_.gpForSpouse = _loc2_.readInt();
               _loc11_.gpForServer = _loc2_.readInt();
               _loc11_.gpForApprenticeOnline = _loc2_.readInt();
               _loc11_.gpForApprenticeTeam = _loc2_.readInt();
               _loc11_.gpForDoubleCard = _loc2_.readInt();
               _loc11_.gpForPower = _loc2_.readInt();
               _loc11_.consortiaSkill = _loc2_.readInt();
               _loc11_.luckyExp = _loc2_.readInt();
               _loc11_.gainGP = _loc2_.readInt();
               _loc11_.gpCSMUser = _loc2_.readInt();
               _loc11_.offerFight = _loc2_.readInt();
               _loc11_.offerDoubleCard = _loc2_.readInt();
               _loc11_.offerVIP = _loc2_.readInt();
               _loc11_.offerService = _loc2_.readInt();
               _loc11_.offerBuff = _loc2_.readInt();
               _loc11_.offerConsortia = _loc2_.readInt();
               _loc11_.luckyOffer = _loc2_.readInt();
               _loc11_.gainOffer = _loc2_.readInt();
               _loc11_.offerCSMUser = _loc2_.readInt();
               _loc11_.canTakeOut = _loc2_.readInt();
               if(GameManager.Instance.Current.roomType == FightFootballTimeManager.FIGHTFOOTBALLTIME_ROOM)
               {
                  if(_loc8_)
                  {
                     _loc11_.canTakeOut = 3;
                  }
                  else
                  {
                     _loc11_.canTakeOut = 1;
                  }
               }
               _loc11_.gameOverType = ExpView.GAME_OVER_TYPE_1;
               _loc12_ = this._current.findPlayer(_loc7_);
               if(_loc12_)
               {
                  _loc12_.isWin = _loc8_;
                  _loc12_.CurrentGP = _loc10_;
                  _loc12_.CurrentLevel = _loc9_;
                  _loc12_.expObj = _loc11_;
                  _loc12_.GainGP = _loc11_.gainGP;
                  _loc12_.GainOffer = _loc11_.gainOffer;
                  _loc12_.GetCardCount = _loc11_.canTakeOut;
               }
               if(_loc12_ is LocalPlayer)
               {
                  _loc12_.tieStatus = _loc3_;
               }
               _loc5_++;
            }
            this._current.GainRiches = _loc2_.readInt();
            if(GameManager.Instance.Current.roomType == FightFootballTimeManager.FIGHTFOOTBALLTIME_ROOM)
            {
               _loc13_ = _loc2_.readInt();
               _loc14_ = _loc2_.readInt();
            }
            for each(_loc6_ in this._current.livings)
            {
               if(_loc6_.character)
               {
                  _loc6_.character.resetShowBitmapBig();
               }
            }
         }
      }
      
      override public function cancel() : void
      {
         if(this._event)
         {
            this._event.executed = true;
         }
         this._current = null;
         this._map = null;
         this._event = null;
         this._func = null;
      }
      
      override public function execute() : void
      {
         var _loc1_:MovieClipWrapper = null;
         if(!this._executed)
         {
            if(this._map.hasSomethingMoving() == false && (this._map.currentPlayer == null || this._map.currentPlayer.actionCount == 0))
            {
               this._executed = true;
               this._event.executed = true;
               if(this._map.currentPlayer && this._map.currentPlayer.isExist)
               {
                  this._map.currentPlayer.beginNewTurn();
               }
               if(RoomManager.Instance.current.type == RoomInfo.CONSORTIA_BATTLE)
               {
                  if(GameManager.Instance.Current.selfGamePlayer.tieStatus == -1)
                  {
                     _loc1_ = new MovieClipWrapper(MovieClip(ClassUtils.CreatInstance("asset.game.newTieAsset")),true,true);
                     _loc1_.movie.x = 434;
                     _loc1_.movie.y = 244;
                  }
                  else if(GameManager.Instance.Current.selfGamePlayer.isWin)
                  {
                     _loc1_ = new MovieClipWrapper(MovieClip(ClassUtils.CreatInstance("asset.game.newWinAsset")),true,true);
                     _loc1_.movie.x = 444;
                     _loc1_.movie.y = 217;
                  }
                  else
                  {
                     _loc1_ = new MovieClipWrapper(MovieClip(ClassUtils.CreatInstance("asset.game.newLostAsset")),true,true);
                     _loc1_.movie.x = 438;
                     _loc1_.movie.y = 244;
                  }
               }
               else
               {
                  if(GameManager.Instance.Current.selfGamePlayer.isWin)
                  {
                     _loc1_ = new MovieClipWrapper(MovieClip(ClassUtils.CreatInstance("asset.game.winAsset")),true,true);
                  }
                  else if(!GameManager.Instance.Current.selfGamePlayer.isWin)
                  {
                     if(GameManager.Instance.Current.redScore == GameManager.Instance.Current.blueScore && GameManager.Instance.Current.roomType == FightFootballTimeManager.FIGHTFOOTBALLTIME_ROOM)
                     {
                        _loc1_ = new MovieClipWrapper(MovieClip(ClassUtils.CreatInstance("asset.game.pingAsset")),true,true);
                     }
                     else
                     {
                        _loc1_ = new MovieClipWrapper(MovieClip(ClassUtils.CreatInstance("asset.game.lostAsset")),true,true);
                     }
                  }
                  _loc1_.movie.x = 500;
                  _loc1_.movie.y = 360;
               }
               SoundManager.instance.play("040");
               this._map.gameView.addChild(_loc1_.movie);
            }
         }
         else
         {
            this._count--;
            if(this._count <= 0)
            {
               this._func();
               _isFinished = true;
               this.cancel();
            }
         }
      }
   }
}
