package game.actions
{
   import com.pickgliss.utils.ClassUtils;
   import ddt.data.PathInfo;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.GameInSocketOut;
   import ddt.manager.SoundManager;
   import fightFootballTime.manager.FightFootballTimeManager;
   import flash.display.MovieClip;
   import flash.geom.Point;
   import game.GameManager;
   import game.model.Living;
   import game.model.LocalPlayer;
   import game.model.TurnedLiving;
   import game.objects.GameLiving;
   import game.objects.SimpleBox;
   import game.view.Bomb;
   import game.view.GameView;
   import game.view.map.MapView;
   import org.aswing.KeyboardManager;
   import road7th.comm.PackageIn;
   import road7th.data.DictionaryData;
   import road7th.utils.MovieClipWrapper;
   import room.RoomManager;
   
   public class ChangePlayerAction extends BaseAction
   {
       
      
      private var _map:MapView;
      
      private var _info:Living;
      
      private var _count:int;
      
      private var _changed:Boolean;
      
      private var _pkg:PackageIn;
      
      private var _event:CrazyTankSocketEvent;
      
      private var _turnTime:int;
      
      public function ChangePlayerAction(param1:MapView, param2:Living, param3:CrazyTankSocketEvent, param4:PackageIn, param5:Number = 200, param6:int = -1)
      {
         super();
         this._event = param3;
         this._event.executed = false;
         this._pkg = param4;
         this._map = param1;
         this._info = param2;
         this._count = param5 / 40;
         this._turnTime = param6;
      }
      
      private function syncMap() : void
      {
         var _loc7_:LocalPlayer = null;
         var _loc13_:int = 0;
         var _loc14_:int = 0;
         var _loc15_:int = 0;
         var _loc16_:int = 0;
         var _loc17_:SimpleBox = null;
         var _loc18_:int = 0;
         var _loc19_:Boolean = false;
         var _loc20_:int = 0;
         var _loc21_:int = 0;
         var _loc22_:int = 0;
         var _loc23_:Boolean = false;
         var _loc24_:int = 0;
         var _loc25_:int = 0;
         var _loc26_:int = 0;
         var _loc27_:int = 0;
         var _loc28_:int = 0;
         var _loc29_:int = 0;
         var _loc30_:int = 0;
         var _loc31_:Living = null;
         var _loc32_:int = 0;
         var _loc33_:DictionaryData = null;
         var _loc34_:int = 0;
         var _loc35_:Bomb = null;
         var _loc1_:Boolean = this._pkg.readBoolean();
         var _loc2_:int = this._pkg.readByte();
         var _loc3_:int = this._pkg.readByte();
         var _loc4_:int = this._pkg.readByte();
         var _loc5_:Array = new Array();
         _loc5_ = [_loc1_,_loc2_,_loc3_,_loc4_];
         GameManager.Instance.Current.setWind(GameManager.Instance.Current.wind,this._info.LivingID == GameManager.Instance.Current.selfGamePlayer.LivingID,_loc5_);
         this._info.isHidden = this._pkg.readBoolean();
         var _loc6_:int = this._pkg.readInt();
         if(this._info is LocalPlayer)
         {
            _loc7_ = LocalPlayer(this._info);
            if(this._turnTime == -1)
            {
               if(_loc6_ > 0)
               {
                  _loc7_.turnTime = _loc6_;
               }
               else
               {
                  _loc7_.turnTime = RoomManager.getTurnTimeByType(RoomManager.Instance.current.timeType);
               }
            }
            else
            {
               _loc7_.turnTime = this._turnTime;
               GameInSocketOut.sendGameSkipNext(0);
            }
            if(_loc6_ != RoomManager.getTurnTimeByType(RoomManager.Instance.current.timeType))
            {
            }
         }
         var _loc8_:int = this._pkg.readInt();
         var _loc9_:uint = 0;
         while(_loc9_ < _loc8_)
         {
            _loc13_ = this._pkg.readInt();
            _loc14_ = this._pkg.readInt();
            _loc15_ = this._pkg.readInt();
            _loc16_ = this._pkg.readInt();
            _loc17_ = new SimpleBox(_loc13_,String(PathInfo.GAME_BOXPIC),_loc16_);
            _loc17_.x = _loc14_;
            _loc17_.y = _loc15_;
            this._map.addPhysical(_loc17_);
            _loc9_++;
         }
         var _loc10_:int = this._pkg.readInt();
         var _loc11_:int = 0;
         while(_loc11_ < _loc10_)
         {
            _loc18_ = this._pkg.readInt();
            _loc19_ = this._pkg.readBoolean();
            _loc20_ = this._pkg.readInt();
            _loc21_ = this._pkg.readInt();
            _loc22_ = this._pkg.readInt();
            _loc23_ = this._pkg.readBoolean();
            _loc24_ = this._pkg.readInt();
            _loc25_ = this._pkg.readInt();
            _loc26_ = this._pkg.readInt();
            _loc27_ = this._pkg.readInt();
            _loc28_ = this._pkg.readInt();
            _loc29_ = this._pkg.readInt();
            _loc30_ = this._pkg.readInt();
            _loc31_ = GameManager.Instance.Current.livings[_loc18_];
            if(_loc31_)
            {
               if(!_loc31_.isLiving && _loc19_)
               {
                  (this._map.gameView as GameView).revivePlayerChangePlayer(_loc31_.LivingID);
               }
               _loc31_.updateBlood(_loc22_,5);
               _loc31_.isNoNole = _loc23_;
               _loc31_.maxEnergy = _loc24_;
               _loc31_.psychic = _loc25_;
               if(_loc31_.isSelf)
               {
                  _loc7_ = LocalPlayer(_loc31_);
                  _loc7_.energy = _loc31_.maxEnergy;
                  _loc7_.shootCount = _loc29_;
                  _loc7_.dander = _loc26_;
                  if(_loc7_.currentPet)
                  {
                     _loc7_.currentPet.MaxMP = _loc27_;
                     _loc7_.currentPet.MP = _loc28_;
                  }
                  _loc7_.soulPropCount = 0;
                  _loc7_.flyCount = _loc30_;
               }
               if(!_loc19_)
               {
                  _loc31_.die();
               }
               else
               {
                  _loc31_.onChange = false;
                  _loc31_.pos = new Point(_loc20_,_loc21_);
                  _loc31_.onChange = true;
               }
            }
            _loc11_++;
         }
         this._map.currentTurn = this._pkg.readInt();
         var _loc12_:Boolean = this._pkg.readBoolean();
         if(_loc12_)
         {
            _loc32_ = this._pkg.readInt();
            _loc33_ = new DictionaryData();
            _loc34_ = 0;
            while(_loc34_ < _loc32_)
            {
               _loc35_ = new Bomb();
               _loc35_.Id = this._pkg.readInt();
               _loc35_.X = this._pkg.readInt();
               _loc35_.Y = this._pkg.readInt();
               _loc33_.add(_loc34_,_loc35_);
               _loc34_++;
            }
            this._map.DigOutCrater(_loc33_);
         }
         if(GameManager.Instance.Current && GameManager.Instance.Current.roomType == FightFootballTimeManager.FIGHTFOOTBALLTIME_ROOM)
         {
            GameManager.Instance.Current.nextPlayerId = this._pkg.readInt();
         }
      }
      
      override public function execute() : void
      {
         if(!this._changed)
         {
            if(this._map.hasSomethingMoving() == false && (this._map.currentPlayer == null || this._map.currentPlayer.actionCount == 0))
            {
               this.executeImp(false);
            }
         }
         else
         {
            this._count--;
            if(this._count <= 0)
            {
               this.changePlayer();
            }
         }
      }
      
      private function changePlayer() : void
      {
         if(this._info is TurnedLiving)
         {
            TurnedLiving(this._info).isAttacking = true;
         }
         this._map.gameView.updateControlBarState(this._info);
         _isFinished = true;
      }
      
      override public function cancel() : void
      {
         this._event.executed = true;
      }
      
      private function executeImp(param1:Boolean) : void
      {
         var _loc2_:Living = null;
         var _loc3_:MovieClipWrapper = null;
         if(this._info == null)
         {
            return;
         }
         if(!this._info.isExist)
         {
            _isFinished = true;
            this._map.gameView.updateControlBarState(null);
            return;
         }
         if(!this._changed)
         {
            this._event.executed = true;
            this._changed = true;
            if(this._pkg)
            {
               this.syncMap();
            }
            for each(_loc2_ in GameManager.Instance.Current.livings)
            {
               _loc2_.beginNewTurn();
            }
            this._map.gameView.setCurrentPlayer(this._info);
            if(this._map.getPhysical(this._info.LivingID))
            {
               (this._map.getPhysical(this._info.LivingID) as GameLiving).needFocus(0,0,{"priority":3});
            }
            this._info.gemDefense = false;
            if(this._info is LocalPlayer && !param1)
            {
               KeyboardManager.getInstance().reset();
               SoundManager.instance.play("016");
               _loc3_ = new MovieClipWrapper(MovieClip(ClassUtils.CreatInstance("asset.game.TurnAsset")),true,true);
               _loc3_.repeat = false;
               _loc3_.movie.mouseChildren = _loc3_.movie.mouseEnabled = false;
               _loc3_.movie.x = 410;
               _loc3_.movie.y = 200;
               this._map.gameView.addChild(_loc3_.movie);
            }
            else
            {
               SoundManager.instance.play("038");
               this.changePlayer();
            }
         }
      }
      
      override public function executeAtOnce() : void
      {
         super.executeAtOnce();
         this.executeImp(false);
         this.changePlayer();
      }
   }
}
