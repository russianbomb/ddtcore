package game.objects
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.BaseButton;
   import ddt.manager.SocketManager;
   import ddt.manager.TimeManager;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import flash.events.TimerEvent;
   import flash.utils.Timer;
   
   public class ActivityDungeonNextView extends Sprite
   {
       
      
      private var ACTIVITYDUNGEONPOINTSNUM:String = "asset.game.nextView.count_";
      
      private var _bg:Bitmap;
      
      private var _nextBtn:BaseButton;
      
      private var _pointsNum:Sprite;
      
      private var _numBitmapArray:Array;
      
      private var _cdData:Number = 0;
      
      private var _timer:Timer;
      
      private var _id:int;
      
      private var _offX:int = 8;
      
      public function ActivityDungeonNextView(param1:int, param2:Number)
      {
         super();
         this._id = param1;
         this._cdData = (param2 - TimeManager.Instance.Now().time) / 1000;
         this.initView();
         this.initEvent();
      }
      
      private function initView() : void
      {
         mouseChildren = true;
         this._bg = ComponentFactory.Instance.creat("asset.game.nextView.bg");
         addChild(this._bg);
         this._nextBtn = ComponentFactory.Instance.creat("activyDungeon.nextView.btn");
         addChild(this._nextBtn);
         this._pointsNum = new Sprite();
         PositionUtils.setPos(this._pointsNum,"game.view.activityDungeonNextView.pointsNumPos");
         addChild(this._pointsNum);
         this._timer = new Timer(1000);
         this._timer.addEventListener(TimerEvent.TIMER,this.__onTimer);
         this._timer.start();
         this.setCountDownNumber(this._cdData);
      }
      
      protected function __onTimer(param1:TimerEvent) : void
      {
         if(this._cdData > 0)
         {
            this._cdData--;
            this.setCountDownNumber(this._cdData);
         }
         else
         {
            this._timer.stop();
            this.visible = false;
         }
      }
      
      private function setCountDownNumber(param1:int) : void
      {
         var _loc4_:Bitmap = null;
         var _loc2_:String = String("0" + Math.floor(param1)).substr(-2);
         var _loc3_:String = "";
         this.deleteBitmapArray();
         this._numBitmapArray = new Array();
         var _loc5_:int = 0;
         while(_loc5_ < _loc2_.length)
         {
            _loc3_ = _loc2_.charAt(_loc5_);
            _loc4_ = ComponentFactory.Instance.creatBitmap(this.ACTIVITYDUNGEONPOINTSNUM + _loc3_);
            _loc4_.x = _loc4_.bitmapData.width * _loc5_ - (_loc5_ == 0?0:this._offX);
            this._pointsNum.addChild(_loc4_);
            this._numBitmapArray.push(_loc4_);
            _loc5_++;
         }
      }
      
      private function deleteBitmapArray() : void
      {
         var _loc1_:int = 0;
         if(this._numBitmapArray)
         {
            _loc1_ = 0;
            while(_loc1_ < this._numBitmapArray.length)
            {
               this._numBitmapArray[_loc1_].bitmapData.dispose();
               this._numBitmapArray[_loc1_] = null;
               _loc1_++;
            }
            this._numBitmapArray.length = 0;
            this._numBitmapArray = null;
         }
      }
      
      private function initEvent() : void
      {
         this._nextBtn.addEventListener(MouseEvent.CLICK,this.__onMouseClick);
      }
      
      public function setBtnEnable() : void
      {
         this._nextBtn.enable = false;
      }
      
      protected function __onMouseClick(param1:MouseEvent) : void
      {
         this._timer.stop();
         SocketManager.Instance.out.sendActivityDungeonNextPoints(this._id,true);
      }
      
      public function dispose() : void
      {
         if(this._bg)
         {
            this._bg.bitmapData.dispose();
            this._bg = null;
         }
         if(this._nextBtn)
         {
            this._nextBtn.removeEventListener(MouseEvent.CLICK,this.__onMouseClick);
            this._nextBtn.dispose();
            this._nextBtn = null;
         }
         this.deleteBitmapArray();
         if(this._pointsNum)
         {
            this._pointsNum = null;
         }
         if(this._timer)
         {
            this._timer.reset();
            this._timer.stop();
            this._timer.removeEventListener(TimerEvent.TIMER,this.__onTimer);
            this._timer = null;
         }
      }
      
      public function get Id() : int
      {
         return this._id;
      }
   }
}
