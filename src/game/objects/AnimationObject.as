package game.objects
{
   import com.pickgliss.ui.ComponentFactory;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   
   public class AnimationObject extends Sprite
   {
       
      
      private var _id:int;
      
      private var _linkName:String;
      
      private var _animation:MovieClip;
      
      public function AnimationObject(param1:int, param2:String)
      {
         super();
         this._id = param1;
         this._linkName = param2;
         this.initView();
      }
      
      private function initView() : void
      {
         this._animation = ComponentFactory.Instance.creat(this._linkName);
         addChild(this._animation);
      }
      
      public function dispose() : void
      {
         this._id = -1;
         if(this._animation)
         {
            this._animation = null;
         }
      }
      
      public function get Id() : int
      {
         return this._id;
      }
   }
}
