package hallIcon.view
{
import DDPlay.DDPlayManaer;
import Dice.DiceManager;
import GodSyah.SyahManager;

import HappyRecharge.HappyRechargeManager;

import accumulativeLogin.AccumulativeManager;
import baglocked.BaglockedManager;
import battleGroud.BattleGroudManager;
import campbattle.CampBattleManager;
import catchbeast.CatchBeastManager;
import chickActivation.ChickActivationManager;
import christmas.manager.ChristmasManager;
import com.pickgliss.ui.ComponentSetting;
import com.pickgliss.ui.controls.container.VBox;
import com.pickgliss.ui.core.Disposeable;
import com.pickgliss.utils.ClassUtils;
import com.pickgliss.utils.ObjectUtils;
import consortionBattle.ConsortiaBattleManager;
import dayActivity.DayActivityManager;
import ddt.manager.GameInSocketOut;
import ddt.manager.LanguageMgr;
import ddt.manager.MessageTipManager;
import ddt.manager.PathManager;
import ddt.manager.PlayerManager;
import ddt.manager.RouletteManager;
import ddt.manager.SharedManager;
import ddt.manager.SocketManager;
import ddt.manager.SoundManager;
import ddt.manager.StateManager;
import ddt.manager.TaskManager;
import ddt.states.StateType;
import entertainmentMode.EntertainmentModeManager;
import escort.EscortManager;
import fightFootballTime.manager.FightFootballTimeManager;
import firstRecharge.FirstRechargeManger;
import flash.display.DisplayObject;
import flash.display.MovieClip;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.utils.Dictionary;
import flash.utils.getTimer;
import flowerGiving.FlowerGivingManager;
import godsRoads.manager.GodsRoadsManager;
import groupPurchase.GroupPurchaseManager;
import growthPackage.GrowthPackageManager;
import guildMemberWeek.manager.GuildMemberWeekManager;
import hallIcon.HallIconManager;
import hallIcon.HallIconType;
import hallIcon.event.HallIconEvent;
import hallIcon.info.HallIconInfo;
import kingDivision.KingDivisionManager;
import lanternriddles.LanternRiddlesManager;
import lightRoad.manager.LightRoadManager;
import littleGame.LittleGameManager;
import luckStar.manager.LuckStarManager;
import mysteriousRoullete.MysteriousManager;
import newChickenBox.controller.NewChickenBoxManager;
import oldPlayerRegress.RegressManager;
import pyramid.PyramidManager;
import ringStation.RingStationManager;
import room.transnational.TransnationalFightManager;
import roulette.LeftGunRouletteManager;
import sevenDayTarget.controller.NewSevenDayAndNewPlayerManager;
import sevenDouble.SevenDoubleManager;
import shop.manager.ShopSaleManager;
import superWinner.manager.SuperWinnerManager;
import trainer.controller.WeakGuildManager;
import trainer.data.ArrowType;
import trainer.data.Step;
import trainer.view.NewHandContainer;
import treasureHunting.TreasureManager;
import wantstrong.WantStrongManager;
import wonderfulActivity.WonderfulActivityManager;

public class HallRightIconView extends Sprite implements Disposeable
{


   private var _iconVBox:VBox;

   private var _wonderFulPlay:HallIconPanel;

   private var _everyDayActivityIcon:MovieClip;

   private var _activity:HallIconPanel;

   private var _wantstrongIcon:MovieClip;

   private var _firstRechargeIcon:MovieClip;

   private var _lastCreatTime:Number;

   private var _showArrowSp:Sprite;

   public function HallRightIconView()
   {
      super();
      this.initView();
      this.initEvent();
   }

   private function initView() : void
   {
      this._showArrowSp = new Sprite();
      addChild(this._showArrowSp);
      this._iconVBox = new VBox();
      this._iconVBox.spacing = 5;
      addChild(this._iconVBox);
      this.updateFirstRechargeIcon();
      this.updateWantstrongIcon();
      this.updateEveryDayActivityIcon();
      this.updateWonderfulPlayIcon();
      this.updateActivityIcon();
   }

   private function initEvent() : void
   {
      HallIconManager.instance.model.addEventListener(HallIconEvent.UPDATE_RIGHTICON_VIEW,this.__updateIconViewHandler);
      HallIconManager.instance.model.addEventListener(HallIconEvent.UPDATE_BATCH_RIGHTICON_VIEW,this.__updateBatchIconViewHandler);
      HallIconManager.instance.addEventListener(HallIconEvent.CHECK_HALLICONEXPERIENCEOPEN,this.__checkHallIconExperienceOpenHandler);
   }

   private function addChildBox(param1:DisplayObject) : void
   {
      this._iconVBox.addChild(param1);
      this._iconVBox.arrange();
   }

   private function __updateBatchIconViewHandler(param1:HallIconEvent) : void
   {
      var _loc3_:HallIconInfo = null;
      var _loc2_:Dictionary = HallIconManager.instance.model.cacheRightIconDic;
      for each(_loc3_ in _loc2_)
      {
         this.updateIconView(_loc3_);
      }
   }

   private function __updateIconViewHandler(param1:HallIconEvent) : void
   {
      var _loc2_:HallIconInfo = HallIconInfo(param1.data);
      this.updateIconView(_loc2_);
   }

   private function updateIconView(param1:HallIconInfo) : void
   {
      if(param1.halltype == HallIcon.WONDERFULPLAY && this._wonderFulPlay)
      {
         this.commonUpdateIconPanelView(this._wonderFulPlay,param1);
         if(!param1.isopen)
         {
            this.removeWonderFulPlayChildHandler(param1.icontype);
         }
      }
      else if(param1.halltype == HallIcon.ACTIVITY && this._activity)
      {
         this.commonUpdateIconPanelView(this._activity,param1);
      }
      else
      {
         switch(param1.icontype)
         {
            case HallIconType.WONDERFULPLAY:
               this.updateWonderfulPlayIcon();
               break;
            case HallIconType.EVERYDAYACTIVITY:
               this.updateEveryDayActivityIcon();
               break;
            case HallIconType.ACTIVITY:
               this.updateActivityIcon();
               break;
            case HallIconType.WANTSTRONG:
               this.updateWantstrongIcon();
               break;
            case HallIconType.FIRSTRECHARGE:
               this.updateFirstRechargeIcon();
         }
      }
   }

   private function commonUpdateIconPanelView(param1:HallIconPanel, param2:HallIconInfo) : void
   {
      var _loc3_:HallIcon = null;
      if(param2.isopen)
      {
         _loc3_ = param1.getIconByType(param2.icontype) as HallIcon;
         if(!_loc3_)
         {
            _loc3_ = param1.addIcon(this.createHallIconPanelIcon(param2),param2.icontype,param2.orderid) as HallIcon;
         }
         _loc3_.updateIcon(param2);
      }
      else
      {
         param1.removeIconByType(param2.icontype);
      }
      param1.arrange();
   }

   private function updateEveryDayActivityIcon() : void
   {
      if(HallIconManager.instance.model.everyDayActivityIsOpen)
      {
         if(this._everyDayActivityIcon == null)
         {
            this._everyDayActivityIcon = ClassUtils.CreatInstance("assets.hallIcon.everyDayActivityIcon");
            this._everyDayActivityIcon.addEventListener(MouseEvent.CLICK,this.__everyDayActivityIconClickHandler);
            this._everyDayActivityIcon.buttonMode = true;
            this._everyDayActivityIcon.mouseChildren = false;
            this.addChildBox(this._everyDayActivityIcon);
         }
      }
      else
      {
         this.removeEveryDayActivityIcon();
      }
   }

   private function __everyDayActivityIconClickHandler(param1:MouseEvent) : void
   {
      SoundManager.instance.play("008");
      DayActivityManager.Instance.initActivityFrame();
   }

   private function updateWantstrongIcon() : void
   {
      if(HallIconManager.instance.model.wantstrongIsOpen)
      {
         if(this._wantstrongIcon == null)
         {
            if(PathManager.russiaEdition == "mail.ru")
            {
               this._wantstrongIcon = ClassUtils.CreatInstance("assets.hallIcon.wantstrongIcon");
            }
            else
            {
               this._wantstrongIcon = ClassUtils.CreatInstance("assets.hallIcon.wantstrongIcon2");
            }
            this._wantstrongIcon.addEventListener(MouseEvent.CLICK,this.__wantstrongIconClickHandler);
            this._wantstrongIcon.buttonMode = true;
            this._wantstrongIcon.mouseChildren = false;
            this.addChildBox(this._wantstrongIcon);
         }
      }
      else
      {
         this.removeWantstrongIcon();
      }
   }

   private function __wantstrongIconClickHandler(param1:MouseEvent) : void
   {
      SoundManager.instance.playButtonSound();
      WantStrongManager.Instance.setup();
   }

   private function updateFirstRechargeIcon() : void
   {
      if(HallIconManager.instance.model.firstRechargeIsOpen)
      {
         if(this._firstRechargeIcon == null)
         {
            this._firstRechargeIcon = ClassUtils.CreatInstance("assets.hallIcon.firstRechargeIcon");
            this._firstRechargeIcon.addEventListener(MouseEvent.CLICK,this.__firstRechargeIconClickHandler);
            this._firstRechargeIcon.buttonMode = true;
            this._firstRechargeIcon.mouseChildren = false;
            this.addChildBox(this._firstRechargeIcon);
         }
      }
      else
      {
         this.removeFirstRechargeIcon();
      }
   }

   private function __firstRechargeIconClickHandler(param1:MouseEvent) : void
   {
      SoundManager.instance.play("008");
      FirstRechargeManger.Instance.showView();
   }

   private function updateWonderfulPlayIcon() : void
   {
      if(HallIconManager.instance.model.wonderFulPlayIsOpen)
      {
         if(this._wonderFulPlay == null)
         {
            this._wonderFulPlay = new HallIconPanel("assets.hallIcon.wonderfulPlayIcon",HallIconPanel.LEFT,6);
            this._wonderFulPlay.addEventListener(MouseEvent.CLICK,this.__wonderFulPlayClickHandler);
            this.addChildBox(this._wonderFulPlay);
         }
      }
      else
      {
         this.removeWonderfulPlayIcon();
      }
   }

   private function __wonderFulPlayClickHandler(param1:MouseEvent) : void
   {
      var _loc2_:HallIcon = null;
      if(this._wonderFulPlay && param1.target == this._wonderFulPlay.mainIcon)
      {
         this.topIndex();
         this.checkNoneActivity(this._wonderFulPlay.count);
         this.checkRightIconTaskClickHandler(HallIcon.WONDERFULPLAY);
         return;
      }
      if(getTimer() - this._lastCreatTime < 1000)
      {
         return;
      }
      this._lastCreatTime = getTimer();
      if(param1.target is HallIcon)
      {
         _loc2_ = param1.target as HallIcon;
         if(_loc2_.iconInfo.halltype == HallIcon.WONDERFULPLAY)
         {
            switch(_loc2_.iconInfo.icontype)
            {
               case HallIconType.WORLDBOSSENTRANCE1:
               case HallIconType.WORLDBOSSENTRANCE4:
                  SoundManager.instance.play("003");
                  StateManager.setState(StateType.WORLDBOSS_AWARD);
                  break;
               case HallIconType.CAMP:
                  CampBattleManager.instance.onCampBtnHander(param1);
                  break;
               case HallIconType.BATTLE:
                  SoundManager.instance.play("008");
                  if(PlayerManager.Instance.Self.Grade < 16)
                  {
                     MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.functionLimitTip",16));
                     return;
                  }
                  if(PlayerManager.Instance.Self.Bag.getItemAt(6) == null)
                  {
                     MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("tank.room.RoomIIController.weapon"));
                     return;
                  }
                  BattleGroudManager.Instance.addBattleSingleRoom();
                  break;
               case HallIconType.SEVENDOUBLE:
                  SoundManager.instance.play("008");
                  if(SevenDoubleManager.instance.isInGame)
                  {
                     SevenDoubleManager.instance.addEventListener(SevenDoubleManager.CAN_ENTER,this.sevenDoubleCanEnterHandler);
                     SocketManager.Instance.out.sendSevenDoubleCanEnter();
                  }
                  else
                  {
                     SevenDoubleManager.instance.loadSevenDoubleModule();
                  }
                  break;
               case HallIconType.LEAGUE:
                  SoundManager.instance.playButtonSound();
                  if(!WeakGuildManager.Instance.checkOpen(Step.GAME_ROOM_OPEN,20))
                  {
                     MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.functionLimitTip",20));
                     return;
                  }
                  StateManager.setState(StateType.ROOM_LIST);
                  ComponentSetting.SEND_USELOG_ID(3);
                  if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.GAME_ROOM_OPEN) && !PlayerManager.Instance.Self.IsWeakGuildFinish(Step.GAME_ROOM_CLICKED))
                  {
                     SocketManager.Instance.out.syncWeakStep(Step.GAME_ROOM_CLICKED);
                  }
                  break;
               case HallIconType.RINGSTATION:
                  SoundManager.instance.playButtonSound();
                  RingStationManager.instance.show();
                  break;
               case HallIconType.TRANSNATIONAL:
                  SoundManager.instance.play("008");
                  if(PlayerManager.Instance.Self.Bag.getItemAt(6) == null)
                  {
                     MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("tank.room.RoomIIController.weapon"));
                     return;
                  }
                  TransnationalFightManager.Instance.enterTransnationalFight();
                  break;
               case HallIconType.FIGHTFOOTBALLTIME:
                  SoundManager.instance.play("008");
                  if(PlayerManager.Instance.Self.Grade < 20)
                  {
                     MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("fightFootballTime.match.levelMsg"));
                     return;
                  }
                  FightFootballTimeManager.instance.enterFightFootballTime();
                  break;
               case HallIconType.CONSORTIABATTLE:
                  SoundManager.instance.play("008");
                  if(ConsortiaBattleManager.instance.isCanEnter)
                  {
                     GameInSocketOut.sendSingleRoomBegin(4);
                  }
                  else if(PlayerManager.Instance.Self.ConsortiaID != 0)
                  {
                     MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.consortiaBattle.cannotEnterTxt"));
                  }
                  else
                  {
                     MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.consortiaBattle.cannotEnterTxt2"));
                  }
                  break;
               case HallIconType.LITTLEGAMENOTE:
                  SoundManager.instance.play("008");
                  if(LittleGameManager.Instance.hasActive())
                  {
                     StateManager.setState(StateType.LITTLEHALL);
                  }
                  else
                  {
                     MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.labyrinth.LabyrinthBoxIconTips.labelII"));
                  }
                  break;
               case HallIconType.FLOWERGIVING:
                  FlowerGivingManager.instance.onIconClick(param1);
                  break;
               case HallIconType.ESCORT:
                  if(EscortManager.instance.isInGame)
                  {
                     EscortManager.instance.addEventListener(EscortManager.CAN_ENTER,this.canEnterHandler);
                     SocketManager.Instance.out.sendEscortCanEnter();
                  }
                  else
                  {
                     EscortManager.instance.loadEscortModule();
                  }
                  break;
               case HallIconType.BURIED:
                  SoundManager.instance.play("003");
                  SocketManager.Instance.out.enterBuried();
            }
         }
      }
   }

   public function removeWonderFulPlayChildHandler(param1:String) : void
   {
      switch(param1)
      {
         case HallIconType.SEVENDOUBLE:
            SevenDoubleManager.instance.removeEventListener(SevenDoubleManager.CAN_ENTER,this.sevenDoubleCanEnterHandler);
            break;
         case HallIconType.ESCORT:
            EscortManager.instance.removeEventListener(EscortManager.CAN_ENTER,this.canEnterHandler);
      }
   }

   private function sevenDoubleCanEnterHandler(param1:Event) : void
   {
      SevenDoubleManager.instance.removeEventListener(SevenDoubleManager.CAN_ENTER,this.sevenDoubleCanEnterHandler);
      StateManager.setState(StateType.SEVEN_DOUBLE_SCENE);
   }

   private function canEnterHandler(param1:Event) : void
   {
      EscortManager.instance.removeEventListener(EscortManager.CAN_ENTER,this.canEnterHandler);
      StateManager.setState(StateType.ESCORT);
   }

   private function updateActivityIcon() : void
   {
      if(HallIconManager.instance.model.activityIsOpen)
      {
         if(this._activity == null)
         {
            this._activity = new HallIconPanel("assets.hallIcon.activityIcon",HallIconPanel.LEFT,5);
            this._activity.addEventListener(MouseEvent.CLICK,this.__activityClickHandler);
            this.addChildBox(this._activity);
         }
      }
      else
      {
         this.removeActivityIcon();
      }
   }

   private function __activityClickHandler(param1:MouseEvent) : void
   {
      var _loc2_:HallIcon = null;
      if(this._activity && param1.target == this._activity.mainIcon)
      {
         this.topIndex();
         this.checkNoneActivity(this._activity.count);
         this.checkRightIconTaskClickHandler(HallIcon.ACTIVITY);
         return;
      }
      if(getTimer() - this._lastCreatTime < 1000)
      {
         return;
      }
      this._lastCreatTime = getTimer();
      if(param1.target is HallIcon)
      {
         _loc2_ = param1.target as HallIcon;
         if(_loc2_.iconInfo.halltype == HallIcon.ACTIVITY)
         {
            switch(_loc2_.iconInfo.icontype)
            {
               case HallIconType.CHRISTMAS:
                  ChristmasManager.instance.onClickChristmasIcon(param1);
                  break;
               case HallIconType.CATCHBEAST:
                  CatchBeastManager.instance.onCatchBeastShow(param1);
                  break;
               case HallIconType.PYRAMID:
                  PyramidManager.instance.onClickPyramidIcon(param1);
                  break;
               case HallIconType.SUPERWINNER:
                  SuperWinnerManager.instance.openSuperWinner(param1);
                  break;
               case HallIconType.LUCKSTAR:
                  LuckStarManager.Instance.onClickLuckyStarIocn(param1);
                  break;
               case HallIconType.GROWTHPACKAGE:
                  GrowthPackageManager.instance.onClickIcon(param1);
                  break;
               case HallIconType.DICE:
                  DiceManager.Instance.EnterDice();
                  break;
               case HallIconType.ACCUMULATIVE_LOGIN:
                  AccumulativeManager.instance.showFrame();
                  break;
               case HallIconType.GUILDMEMBERWEEK:
                  GuildMemberWeekManager.instance.onClickguildMemberWeekIcon(param1);
                  break;
               case HallIconType.LANTERNRIDDLES:
                  LanternRiddlesManager.instance.onLanternShow(param1);
                  break;
               case HallIconType.NEWCHICKENBOX:
                  NewChickenBoxManager.instance.enterNewBoxView(param1);
                  break;
               case HallIconType.LEFTGUNROULETTE:
                  LeftGunRouletteManager.instance.onGunBtnClick(param1);
                  break;
               case HallIconType.GROUPPURCHASE:
                  GroupPurchaseManager.instance.showFrame();
                  break;
               case HallIconType.LUCKSTONE:
                  SoundManager.instance.play("008");
                  if(PlayerManager.Instance.Self.bagLocked)
                  {
                     BaglockedManager.Instance.show();
                     return;
                  }
                  RouletteManager.instance.useBless();
                  break;
               case HallIconType.MYSTERIOUROULETTE:
                  MysteriousManager.instance.showFrame();
                  break;
               case HallIconType.SYAH:
                  SyahManager.Instance.showFrame();
                  break;
               case HallIconType.TREASUREHUNTING:
                  TreasureManager.instance.showFrame();
                  break;
               case HallIconType.OLDPLAYERREGRESS:
                  RegressManager.instance.show();
                  break;
               case HallIconType.LIMITACTIVITY:
                  SoundManager.instance.play("008");
                  WonderfulActivityManager.Instance.clickWonderfulActView = true;
                  SocketManager.Instance.out.requestWonderfulActInit(1);
                  break;
               case HallIconType.LIGHTROAD:
                  LightRoadManager.instance.onClicklightRoadIcon(param1);
                  break;
               case HallIconType.SEVENDAYTARGET:
                  NewSevenDayAndNewPlayerManager.Instance.onClickSevenDayTargetIcon(param1);
                  break;
               case HallIconType.GODSROADS:
                  GodsRoadsManager.instance.openGodsRoads(param1);
                  break;
               case HallIconType.ENTERTAINMENT:
                  SoundManager.instance.play("008");
                  EntertainmentModeManager.instance.show();
                  break;
               case HallIconType.SALESHOP:
                  SoundManager.instance.play("008");
                  SocketManager.Instance.out.sendGetShopBuyLimitedCount();
                  ShopSaleManager.Instance.show();
                  break;
               case HallIconType.KINGDIVISION:
                  SoundManager.instance.play("008");
                  KingDivisionManager.Instance.onClickIcon();
                  break;
               case HallIconType.CHICKACTIVATION:
                  SoundManager.instance.play("008");
                  ChickActivationManager.instance.showFrame();
                  break;
               case HallIconType.DDPLAY:
                  SoundManager.instance.play("008");
                  DDPlayManaer.Instance.show();
                  break;
               case HallIconType.BOGUADVENTURE:
                  SoundManager.instance.playButtonSound();
                  StateManager.setState(StateType.BOGU_ADVENTURE);
                  break;
               case HallIconType.HAPPYRECHARGE:
                  SoundManager.instance.play("008");
                  HappyRechargeManager.instance.loadResource();
            }
         }
      }
   }

   public function createHallIconPanelIcon(param1:HallIconInfo) : HallIcon
   {
      var _loc2_:String = null;
      switch(param1.icontype)
      {
         case HallIconType.WORLDBOSSENTRANCE1:
            _loc2_ = "assets.hallIcon.worldBossEntrance_1";
            break;
         case HallIconType.WORLDBOSSENTRANCE4:
            _loc2_ = "assets.hallIcon.worldBossEntrance_4";
            break;
         case HallIconType.CAMP:
            _loc2_ = "assets.hallIcon.campIcon";
            break;
         case HallIconType.BATTLE:
            _loc2_ = "assets.hallIcon.battleIcon";
            break;
         case HallIconType.SEVENDOUBLE:
            _loc2_ = "assets.hallIcon.sevenDoubleEntryIcon";
            break;
         case HallIconType.LEAGUE:
            _loc2_ = "assets.hallIcon.leagueIcon";
            break;
         case HallIconType.RINGSTATION:
            _loc2_ = "assets.hallIcon.ringStationIcon";
            break;
         case HallIconType.TRANSNATIONAL:
            _loc2_ = "assets.hallIcon.transnationalIcon";
            break;
         case HallIconType.FIGHTFOOTBALLTIME:
            _loc2_ = "assets.hallIcon.fightFootballTimeIcon";
            break;
         case HallIconType.CONSORTIABATTLE:
            _loc2_ = "assets.hallIcon.consortiaBattleEntryIcon";
            break;
         case HallIconType.LITTLEGAMENOTE:
            _loc2_ = "assets.hallIcon.littleGameNoteIcon";
            break;
         case HallIconType.FLOWERGIVING:
            _loc2_ = "assets.hallIcon.flowerGivingIcon";
            break;
         case HallIconType.ESCORT:
            _loc2_ = "assets.hallIcon.escortEntryIcon";
            break;
         case HallIconType.BURIED:
            _loc2_ = "assets.hallIcon.buriedIcon";
            break;
         case HallIconType.CHRISTMAS:
            _loc2_ = "assets.hallIcon.christmasIcon";
            break;
         case HallIconType.CATCHBEAST:
            _loc2_ = "asset.hallIcon.catchBeastIcon";
            break;
         case HallIconType.PYRAMID:
            _loc2_ = "assets.hallIcon.pyramidIcon";
            break;
         case HallIconType.SUPERWINNER:
            _loc2_ = "assets.hallIcon.superWinnerEntryIcon";
            break;
         case HallIconType.LUCKSTAR:
            _loc2_ = "assets.hallIcon.luckyStarIcon";
            break;
         case HallIconType.GROWTHPACKAGE:
            _loc2_ = "assets.hallIcon.growthPachageIcon";
            break;
         case HallIconType.DICE:
            _loc2_ = "assets.hallIcon.diceIcon";
            break;
         case HallIconType.ACCUMULATIVE_LOGIN:
            _loc2_ = "assets.hallIcon.accumulativeLoginIcon";
            break;
         case HallIconType.GUILDMEMBERWEEK:
            _loc2_ = "assets.hallIcon.guildmemberweekIcon";
            break;
         case HallIconType.LANTERNRIDDLES:
            _loc2_ = "assets.hallIcon.lanternriddlesIcon";
            break;
         case HallIconType.NEWCHICKENBOX:
            _loc2_ = "assets.hallIcon.newChickenBoxIcon";
            break;
         case HallIconType.LEFTGUNROULETTE:
            _loc2_ = "assets.hallIcon.rouletteGunIcon";
            break;
         case HallIconType.GROUPPURCHASE:
            _loc2_ = "assets.hallIcon.groupPurchaseIcon";
            break;
         case HallIconType.LUCKSTONE:
            _loc2_ = "assets.hallIcon.luckStoneIcon";
            break;
         case HallIconType.MYSTERIOUROULETTE:
            _loc2_ = "assets.hallIcon.mysteriousRouletteIcon";
            break;
         case HallIconType.SYAH:
            _loc2_ = "assets.hallIcon.syahIcon";
            break;
         case HallIconType.TREASUREHUNTING:
            _loc2_ = "assets.hallIcon.treasureHuntingIcon";
            break;
         case HallIconType.OLDPLAYERREGRESS:
            _loc2_ = "assets.hallIcon.oldPlayerRegressIcon";
            break;
         case HallIconType.LIMITACTIVITY:
            _loc2_ = "assets.hallIcon.limitActivityIcon";
            break;
         case HallIconType.LIGHTROAD:
            _loc2_ = "assets.hallIcon.lightRoadIcon";
            break;
         case HallIconType.SEVENDAYTARGET:
            _loc2_ = "assets.hallIcon.sevenDayTargetIcon";
            break;
         case HallIconType.GODSROADS:
            _loc2_ = "assets.hallIcon.godsRoadsIcon";
            break;
         case HallIconType.ENTERTAINMENT:
            _loc2_ = "assets.hallIcon.entertainmentIcon";
            break;
         case HallIconType.SALESHOP:
            _loc2_ = "assets.hallIcon.saleShopIcon";
            break;
         case HallIconType.KINGDIVISION:
            _loc2_ = "assets.hallIcon.kingDivisionIcon";
            break;
         case HallIconType.CHICKACTIVATION:
            _loc2_ = "assets.hallIcon.chickActivationIcon";
            break;
         case HallIconType.DDPLAY:
            _loc2_ = "assets.hallIcon.DDPlayIcon";
            break;
         case HallIconType.BOGUADVENTURE:
            _loc2_ = "assets.hallIcon.boguAdventureIcon";
            break;
         case HallIconType.HAPPYRECHARGE:
            _loc2_ = "assets.hallIcon.happyRecharge";
            break;
      }
      return new HallIcon(_loc2_,param1);
   }

   public function getIconByType(param1:int, param2:String) : DisplayObject
   {
      if(param1 == HallIcon.WONDERFULPLAY && this._wonderFulPlay)
      {
         return this._wonderFulPlay.getIconByType(param2);
      }
      if(param1 == HallIcon.ACTIVITY && this._activity)
      {
         return this._activity.getIconByType(param2);
      }
      return null;
   }

   private function topIndex() : void
   {
      if(this.parent && this.parent.numChildren > 1)
      {
         this.parent.setChildIndex(this,this.parent.numChildren - 1);
      }
   }

   private function checkNoneActivity(param1:int) : void
   {
      if(param1 <= 0)
      {
         MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("tank.calendar.NoneActivity"));
      }
   }

   public function __checkHallIconExperienceOpenHandler(param1:HallIconEvent) : void
   {
      this.updateRightIconTaskArrow();
   }

   private function updateRightIconTaskArrow() : void
   {
      var _loc2_:int = 0;
      var _loc3_:int = 0;
      var _loc1_:Object = HallIconManager.instance.model.cacheRightIconTask;
      if(_loc1_ && !_loc1_.isCompleted && SharedManager.Instance.halliconExperienceStep < 2)
      {
         _loc2_ = SharedManager.Instance.halliconExperienceStep;
         _loc3_ = 1;
         if(this._iconVBox.numChildren == 3)
         {
            _loc3_ = 2;
         }
         else if(this._iconVBox.numChildren == 4)
         {
            _loc3_ = 3;
         }
         else if(this._iconVBox.numChildren == 5)
         {
            _loc3_ = 4;
         }
         if(_loc2_ == 1)
         {
            _loc3_ = _loc3_ + 1;
         }
         NewHandContainer.Instance.showArrow(ArrowType.HALLICON_EXPERIENCE,-90,"hallIcon.hallIconExperiencePos" + _loc3_,"assets.hallIcon.experienceClickTxt","hallIcon.hallIconExperienceTxt" + _loc3_,this._showArrowSp,0,true);
      }
      else if(NewHandContainer.Instance.hasArrow(ArrowType.HALLICON_EXPERIENCE))
      {
         NewHandContainer.Instance.clearArrowByID(ArrowType.HALLICON_EXPERIENCE);
      }
   }

   private function checkRightIconTaskClickHandler(param1:int) : void
   {
      if(!HallIconManager.instance.model.cacheRightIconTask)
      {
         return;
      }
      if(param1 == HallIcon.WONDERFULPLAY && SharedManager.Instance.halliconExperienceStep == 0)
      {
         SharedManager.Instance.halliconExperienceStep = 1;
         this.updateRightIconTaskArrow();
         SharedManager.Instance.save();
      }
      else if(param1 == HallIcon.ACTIVITY && SharedManager.Instance.halliconExperienceStep == 1)
      {
         SharedManager.Instance.halliconExperienceStep = 2;
         this.updateRightIconTaskArrow();
         TaskManager.instance.checkQuest(TaskManager.HALLICON_TASKTYPE,1,0);
         SharedManager.Instance.halliconExperienceStep = 0;
         SharedManager.Instance.save();
      }
   }

   private function removeEvent() : void
   {
      HallIconManager.instance.model.removeEventListener(HallIconEvent.UPDATE_RIGHTICON_VIEW,this.__updateIconViewHandler);
      HallIconManager.instance.model.removeEventListener(HallIconEvent.UPDATE_BATCH_RIGHTICON_VIEW,this.__updateBatchIconViewHandler);
      HallIconManager.instance.removeEventListener(HallIconEvent.CHECK_HALLICONEXPERIENCEOPEN,this.__checkHallIconExperienceOpenHandler);
   }

   private function removeWonderfulPlayIcon() : void
   {
      if(this._wonderFulPlay)
      {
         this._wonderFulPlay.removeEventListener(MouseEvent.CLICK,this.__wonderFulPlayClickHandler);
         ObjectUtils.disposeObject(this._wonderFulPlay);
         this._wonderFulPlay = null;
      }
   }

   private function removeEveryDayActivityIcon() : void
   {
      if(this._everyDayActivityIcon)
      {
         this._everyDayActivityIcon.stop();
         this._everyDayActivityIcon.removeEventListener(MouseEvent.CLICK,this.__everyDayActivityIconClickHandler);
         ObjectUtils.disposeObject(this._everyDayActivityIcon);
         this._everyDayActivityIcon = null;
      }
   }

   private function removeWantstrongIcon() : void
   {
      if(this._wantstrongIcon)
      {
         this._wantstrongIcon.stop();
         this._wantstrongIcon.removeEventListener(MouseEvent.CLICK,this.__wantstrongIconClickHandler);
         ObjectUtils.disposeObject(this._wantstrongIcon);
         this._wantstrongIcon = null;
      }
   }

   private function removeFirstRechargeIcon() : void
   {
      if(this._firstRechargeIcon)
      {
         this._firstRechargeIcon.stop();
         this._firstRechargeIcon.removeEventListener(MouseEvent.CLICK,this.__firstRechargeIconClickHandler);
         ObjectUtils.disposeObject(this._firstRechargeIcon);
         this._firstRechargeIcon = null;
      }
   }

   private function removeActivityIcon() : void
   {
      if(this._activity)
      {
         this._activity.removeEventListener(MouseEvent.CLICK,this.__activityClickHandler);
         ObjectUtils.disposeObject(this._activity);
         this._activity = null;
      }
   }

   public function dispose() : void
   {
      this.removeEvent();
      this.removeWonderfulPlayIcon();
      this.removeEveryDayActivityIcon();
      this.removeActivityIcon();
      this.removeWantstrongIcon();
      this.removeFirstRechargeIcon();
      if(NewHandContainer.Instance.hasArrow(ArrowType.HALLICON_EXPERIENCE))
      {
         NewHandContainer.Instance.clearArrowByID(ArrowType.HALLICON_EXPERIENCE);
      }
      if(this._showArrowSp)
      {
         ObjectUtils.disposeAllChildren(this._showArrowSp);
         ObjectUtils.disposeObject(this._showArrowSp);
         this._showArrowSp = null;
      }
      if(this._iconVBox)
      {
         ObjectUtils.disposeAllChildren(this._iconVBox);
         ObjectUtils.disposeObject(this._iconVBox);
         this._iconVBox = null;
      }
      if(parent)
      {
         parent.removeChild(this);
      }
   }
}
}
