package hallIcon.view
{
   import com.greensock.TweenLite;
   import com.pickgliss.toplevel.StageReferance;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.SoundManager;
   import flash.display.Bitmap;
   import flash.display.DisplayObject;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   
   public class HallIconPanel extends Sprite implements Disposeable
   {
      
      public static const LEFT:String = "left";
      
      public static const RIGHT:String = "right";
       
      
      private var _mainIcon:DisplayObject;
      
      private var _mainIconString:String;
      
      private var _hotNumBg:Bitmap;
      
      private var _hotNum:FilterFrameText;
      
      private var _iconArray:Array;
      
      private var _iconBox:HallIconBox;
      
      private var direction:String;
      
      private var vNum:int;
      
      private var hNum:int;
      
      private var WHSize:Array;
      
      private var tweenLiteMax:TweenLite;
      
      private var tweenLiteSmall:TweenLite;
      
      private var isExpand:Boolean;
      
      public function HallIconPanel(param1:String, param2:String = "left", param3:int = -1, param4:int = -1, param5:Array = null)
      {
         super();
         this._mainIconString = param1;
         this.direction = param2;
         this.hNum = param3;
         this.vNum = param4;
         if(this.hNum == -1 && this.vNum == -1)
         {
            this.vNum = 1;
         }
         if(param5 == null)
         {
            param5 = [78,78];
         }
         this.WHSize = param5;
         this.initView();
         this.initEvent();
      }
      
      private function initView() : void
      {
         this._mainIcon = ComponentFactory.Instance.creat(this._mainIconString);
         if(this._mainIcon is Sprite)
         {
            Sprite(this._mainIcon).buttonMode = true;
            Sprite(this._mainIcon).mouseChildren = false;
         }
         addChild(this._mainIcon);
         this._hotNumBg = ComponentFactory.Instance.creatBitmap("assets.hallIcon.hotNumBg");
         addChild(this._hotNumBg);
         this._hotNum = ComponentFactory.Instance.creatComponentByStylename("hallicon.hallIconPanel.hotNum");
         this._hotNum.text = "0";
         addChild(this._hotNum);
         this.updateHotNum();
         this._iconArray = new Array();
         this._iconBox = new HallIconBox();
         this._iconBox.visible = false;
         addChild(this._iconBox);
      }
      
      private function initEvent() : void
      {
         StageReferance.stage.addEventListener(MouseEvent.CLICK,this.__mainIconHandler);
      }
      
      public function addIcon(param1:DisplayObject, param2:String, param3:int = 0) : DisplayObject
      {
         this._iconBox.addChild(param1);
         var _loc4_:Object = {};
         _loc4_.icon = param1;
         _loc4_.icontype = param2;
         _loc4_.orderId = param3;
         this._iconArray.push(_loc4_);
         this.arrange();
         return param1;
      }
      
      public function getIconByType(param1:String) : DisplayObject
      {
         var _loc2_:DisplayObject = null;
         var _loc4_:String = null;
         var _loc3_:int = 0;
         while(_loc3_ < this._iconArray.length)
         {
            _loc4_ = this._iconArray[_loc3_].icontype;
            if(_loc4_ == param1)
            {
               _loc2_ = this._iconArray[_loc3_].icon;
               break;
            }
            _loc3_++;
         }
         return _loc2_;
      }
      
      public function removeIconByType(param1:String) : void
      {
         var _loc2_:DisplayObject = null;
         var _loc4_:String = null;
         var _loc5_:int = 0;
         var _loc3_:int = 0;
         while(_loc3_ < this._iconArray.length)
         {
            _loc4_ = this._iconArray[_loc3_].icontype;
            if(_loc4_ == param1)
            {
               _loc2_ = this._iconArray[_loc3_].icon;
               this._iconArray.splice(_loc3_,1);
               break;
            }
            _loc3_++;
         }
         if(_loc2_)
         {
            _loc5_ = this._iconBox.getChildIndex(_loc2_);
            if(_loc5_ != -1)
            {
               this._iconBox.removeChildAt(_loc5_);
            }
         }
         if(_loc2_)
         {
            ObjectUtils.disposeObject(_loc2_);
         }
         this.arrange();
      }
      
      public function arrange() : void
      {
         this.iconSortOn();
         this.updateIconsPos();
         if(this.isExpand)
         {
            this.updateDirectionPos();
         }
         this.updateHotNum();
      }
      
      private function updateIconsPos() : void
      {
         var _loc2_:DisplayObject = null;
         var _loc1_:int = 0;
         while(_loc1_ < this._iconArray.length)
         {
            _loc2_ = this._iconArray[_loc1_].icon;
            if(this.hNum == -1)
            {
               _loc2_.x = _loc1_ * this.WHSize[0];
            }
            else
            {
               _loc2_.x = int(_loc1_ % this.hNum) * this.WHSize[0];
            }
            if(this.vNum == -1)
            {
               _loc2_.y = int(_loc1_ / this.hNum) * this.WHSize[1];
            }
            else
            {
               _loc2_.y = _loc1_ * this.WHSize[1];
            }
            _loc1_++;
         }
      }
      
      private function updateDirectionPos() : void
      {
         if(this.direction == LEFT)
         {
            this._iconBox.x = -this.getIconSpriteWidth() - 10;
         }
         else
         {
            this._iconBox.x = this._mainIcon.x + this._mainIcon.width + 10;
         }
         this._iconBox.y = -(this.getIconSpriteHeight() - this.WHSize[1]) / 2;
      }
      
      public function iconSortOn() : void
      {
         if(this._iconArray.length > 1)
         {
            this._iconArray.sort(this.sortFunctin);
         }
      }
      
      private function sortFunctin(param1:Object, param2:Object) : Number
      {
         if(param1.orderId > param2.orderId)
         {
            return 1;
         }
         if(param1.orderId < param2.orderId)
         {
            return -1;
         }
         return 0;
      }
      
      public function expand(param1:Boolean) : void
      {
         var _loc2_:Number = NaN;
         var _loc3_:Number = NaN;
         if(this.isExpand != param1 && this._iconArray && this._iconArray.length > 0)
         {
            this.isExpand = param1;
            if(this.isExpand)
            {
               _loc2_ = 0;
               if(this.direction == LEFT)
               {
                  _loc2_ = -this.getIconSpriteWidth() - 10;
               }
               else
               {
                  _loc2_ = this._mainIcon.x + this._mainIcon.width + 10;
               }
               _loc3_ = -(this.getIconSpriteHeight() - this.WHSize[1]) / 2;
               this._iconBox.x = this._mainIcon.x;
               this._iconBox.y = 0;
               this._iconBox.scaleX = 0;
               this._iconBox.scaleY = 0;
               this._iconBox.alpha = 0;
               this._iconBox.visible = true;
               this.tweenLiteMax = TweenLite.to(this._iconBox,0.2,{
                  "x":_loc2_,
                  "y":_loc3_,
                  "alpha":1,
                  "scaleX":1,
                  "scaleY":1,
                  "onComplete":this.tweenLiteMaxCloseComplete
               });
            }
            else
            {
               this.tweenLiteSmall = TweenLite.to(this._iconBox,0.2,{
                  "x":this._mainIcon.x,
                  "y":0,
                  "alpha":0,
                  "scaleX":0,
                  "scaleY":0,
                  "onComplete":this.tweenLiteSmallCloseComplete
               });
            }
         }
      }
      
      private function tweenLiteSmallCloseComplete() : void
      {
         this.killTweenLiteSmall();
         this._iconBox.visible = false;
      }
      
      private function tweenLiteMaxCloseComplete() : void
      {
         this.killTweenLiteMax();
      }
      
      private function getIconSpriteWidth() : Number
      {
         var _loc1_:Number = 0;
         if(this._iconArray.length == 0)
         {
            _loc1_ = 0;
         }
         else if(this.hNum == -1)
         {
            _loc1_ = this._iconArray.length * this.WHSize[0];
         }
         else if(this._iconArray.length >= this.hNum)
         {
            _loc1_ = this.hNum * this.WHSize[0];
         }
         else
         {
            _loc1_ = this._iconArray.length * this.WHSize[0];
         }
         return _loc1_;
      }
      
      private function getIconSpriteHeight() : Number
      {
         var _loc2_:int = 0;
         var _loc1_:Number = 0;
         if(this._iconArray.length == 0)
         {
            _loc1_ = 0;
         }
         else if(this.hNum == -1)
         {
            _loc1_ = this.WHSize[1];
         }
         else if(this._iconArray.length >= this.hNum)
         {
            _loc2_ = this._iconArray.length / this.hNum;
            if(this._iconArray.length % this.hNum)
            {
               _loc2_ = _loc2_ + 1;
            }
            _loc1_ = _loc2_ * this.WHSize[1];
         }
         else
         {
            _loc1_ = this.WHSize[1];
         }
         return _loc1_;
      }
      
      public function removeChildren() : void
      {
         this._iconBox.removeChildren();
         this._iconArray = [];
      }
      
      private function __mainIconHandler(param1:MouseEvent) : void
      {
         if(param1.target == this._mainIcon)
         {
            SoundManager.instance.play("008");
            if(this._iconArray && this._iconArray.length > 0)
            {
               if(this._iconBox.visible)
               {
                  this.expand(false);
               }
               else
               {
                  this.expand(true);
               }
            }
         }
         else
         {
            this.expand(false);
         }
      }
      
      private function updateHotNum() : void
      {
         var _loc1_:Boolean = false;
         if(this._iconArray && this._iconArray.length > 0)
         {
            _loc1_ = true;
            this._hotNum.text = this._iconArray.length + "";
         }
         else
         {
            this._hotNum.text = "0";
         }
         this._hotNumBg.visible = this._hotNum.visible = _loc1_;
      }
      
      public function get mainIcon() : DisplayObject
      {
         return this._mainIcon;
      }
      
      public function get count() : int
      {
         return this._iconArray.length;
      }
      
      override public function get height() : Number
      {
         if(this._mainIcon)
         {
            return this._mainIcon.height;
         }
         return 0;
      }
      
      override public function get width() : Number
      {
         if(this._mainIcon)
         {
            return this._mainIcon.width;
         }
         return 0;
      }
      
      private function killTweenLiteMax() : void
      {
         if(!this.tweenLiteMax)
         {
            return;
         }
         this.tweenLiteMax.kill();
         this.tweenLiteMax = null;
      }
      
      private function killTweenLiteSmall() : void
      {
         if(!this.tweenLiteSmall)
         {
            return;
         }
         this.tweenLiteSmall.kill();
         this.tweenLiteSmall = null;
      }
      
      private function removeEvent() : void
      {
         StageReferance.stage.removeEventListener(MouseEvent.CLICK,this.__mainIconHandler);
      }
      
      public function dispose() : void
      {
         this.killTweenLiteMax();
         this.killTweenLiteSmall();
         this.removeEvent();
         ObjectUtils.disposeObject(this._mainIcon);
         this._mainIcon = null;
         ObjectUtils.disposeObject(this._hotNumBg);
         this._hotNumBg = null;
         ObjectUtils.disposeObject(this._hotNum);
         this._hotNum = null;
         ObjectUtils.disposeObject(this._iconBox);
         this._iconBox = null;
         if(parent)
         {
            parent.removeChild(this);
         }
         this._iconArray = null;
         this.WHSize = null;
         this.direction = null;
         this._mainIconString = null;
         this.vNum = 0;
         this.hNum = 0;
      }
   }
}
