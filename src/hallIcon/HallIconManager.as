package hallIcon
{
import com.pickgliss.events.FrameEvent;
import com.pickgliss.ui.ComponentFactory;
import com.pickgliss.ui.LayerManager;
import com.pickgliss.ui.controls.Frame;
import com.pickgliss.utils.ObjectUtils;
import ddt.events.CrazyTankSocketEvent;
import ddt.manager.LanguageMgr;
import ddt.manager.PlayerManager;
import ddt.manager.SocketManager;
import firstRecharge.FirstRechargeManger;
import flash.display.DisplayObject;
import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import hallIcon.event.HallIconEvent;
import hallIcon.info.HallIconInfo;
import hallIcon.model.HallIconModel;
import hallIcon.view.HallIcon;
import kingBless.KingBlessManager;
import road7th.comm.PackageIn;
import worldboss.WorldBossManager;

public class HallIconManager extends EventDispatcher
{

   private static var _instance:HallIconManager;


   public var model:HallIconModel;

   public function HallIconManager(param1:IEventDispatcher = null)
   {
      super(param1);
   }

   public static function get instance() : HallIconManager
   {
      if(_instance == null)
      {
         _instance = new HallIconManager();
      }
      return _instance;
   }

   public function setup() : void
   {
      this.model = new HallIconModel();
      this.initEvent();
   }

   public function checkDefaultIconShow() : void
   {
      this.model.expblessedIsOpen = true;
      if(PlayerManager.Instance.Self.Grade < 30 && !PlayerManager.Instance.Self.IsVIP)
      {
         this.model.vipLvlIsOpen = true;
      }
      if(KingBlessManager.instance.openType == 0 && PlayerManager.Instance.Self.Grade < 30)
      {
         this.model.kingBlessIsOpen = true;
      }
      this.model.wonderFulPlayIsOpen = true;
      this.model.activityIsOpen = true;
      this.model.everyDayActivityIsOpen = true;
      if(PlayerManager.Instance.Self.Grade >= 8)
      {
         this.model.wantstrongIsOpen = false;
      }
      this.model.firstRechargeIsOpen = FirstRechargeManger.Instance.isOpen;
   }

   public function checkIconCall() : void
   {
      SocketManager.Instance.out.sendExpBlessedData();
      FirstRechargeManger.Instance.openFun = this.firstRechargeOpenHandler;
   }

   private function initEvent() : void
   {
      SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GET_EXPBLESSED_DATA,this.__onSetExpBlessedData);
      PlayerManager.Instance.addEventListener(PlayerManager.VIP_STATE_CHANGE,this.__vipLvlIsOpenHandler);
   }

   private function __onSetExpBlessedData(param1:CrazyTankSocketEvent) : void
   {
      var _loc2_:PackageIn = param1.pkg;
      this.model.expblessedValue = _loc2_.readInt();
      if(this.model.expblessedValue > 0)
      {
         this.model.expblessedIsOpen = true;
      }
      else
      {
         this.model.expblessedIsOpen = false;
      }
      this.model.dataChange(HallIconEvent.UPDATE_LEFTICON_VIEW,new HallIconInfo(HallIconType.EXPBLESSED));
   }

   private function firstRechargeOpenHandler() : void
   {
      this.model.firstRechargeIsOpen = FirstRechargeManger.Instance.isOpen;
      this.model.dataChange(HallIconEvent.UPDATE_LEFTICON_VIEW,new HallIconInfo(HallIconType.FIRSTRECHARGE));
   }

   private function __vipLvlIsOpenHandler(param1:Event) : void
   {
      if(PlayerManager.Instance.Self.Grade < 30 && !PlayerManager.Instance.Self.IsVIP)
      {
         this.model.vipLvlIsOpen = true;
      }
      else
      {
         this.model.vipLvlIsOpen = false;
      }
      this.model.dataChange(HallIconEvent.UPDATE_LEFTICON_VIEW,new HallIconInfo(HallIconType.VIPLVL));
   }

   private function cacheRightIcon(param1:String, param2:HallIconInfo) : void
   {
      if(param2.isopen)
      {
         this.model.cacheRightIconDic[param1] = param2;
      }
      else if(this.model.cacheRightIconDic[param1])
      {
         delete this.model.cacheRightIconDic[param1];
      }
   }

   public function checkCacheRightIconShow() : void
   {
      this.model.dispatchEvent(new HallIconEvent(HallIconEvent.UPDATE_BATCH_RIGHTICON_VIEW));
   }

   public function updateSwitchHandler(param1:String, param2:Boolean, param3:String = null) : void
   {
      var _loc4_:HallIconInfo = this.convertIconInfo(param1,param2,param3);
      this.cacheRightIcon(param1,_loc4_);
      this.model.dispatchEvent(new HallIconEvent(HallIconEvent.UPDATE_RIGHTICON_VIEW,_loc4_));
   }

   private function convertIconInfo(param1:String, param2:Boolean, param3:String) : HallIconInfo
   {
      var _loc6_:Boolean = false;
      var _loc4_:int = 0;
      var _loc5_:int = 99;
      switch(param1)
      {
         case HallIconType.WORLDBOSSENTRANCE1:
            _loc4_ = HallIcon.WONDERFULPLAY;
            if(WorldBossManager.Instance.bossInfo)
            {
               _loc6_ = WorldBossManager.Instance.bossInfo.fightOver;
            }
            _loc5_ = 1;
            break;
         case HallIconType.WORLDBOSSENTRANCE4:
            _loc4_ = HallIcon.WONDERFULPLAY;
            if(WorldBossManager.Instance.bossInfo)
            {
               _loc6_ = WorldBossManager.Instance.bossInfo.fightOver;
            }
            _loc5_ = 2;
            break;
         case HallIconType.RINGSTATION:
            _loc4_ = HallIcon.WONDERFULPLAY;
            _loc5_ = 3;
            break;
         case HallIconType.LEAGUE:
            _loc4_ = HallIcon.WONDERFULPLAY;
            _loc5_ = 4;
            break;
         case HallIconType.BATTLE:
            _loc4_ = HallIcon.WONDERFULPLAY;
            _loc5_ = 5;
            break;
         case HallIconType.TRANSNATIONAL:
            _loc4_ = HallIcon.WONDERFULPLAY;
            _loc5_ = 6;
            break;
         case HallIconType.CONSORTIABATTLE:
            _loc4_ = HallIcon.WONDERFULPLAY;
            _loc5_ = 7;
            break;
         case HallIconType.CAMP:
            _loc4_ = HallIcon.WONDERFULPLAY;
            _loc5_ = 8;
            break;
         case HallIconType.FIGHTFOOTBALLTIME:
            _loc4_ = HallIcon.WONDERFULPLAY;
            _loc5_ = 9;
            break;
         case HallIconType.SEVENDOUBLE:
            _loc4_ = HallIcon.WONDERFULPLAY;
            _loc5_ = 10;
            break;
         case HallIconType.LITTLEGAMENOTE:
            _loc4_ = HallIcon.WONDERFULPLAY;
            _loc5_ = 11;
            break;
         case HallIconType.FLOWERGIVING:
            _loc4_ = HallIcon.WONDERFULPLAY;
            _loc5_ = 12;
            break;
         case HallIconType.ESCORT:
            _loc4_ = HallIcon.WONDERFULPLAY;
            _loc5_ = 13;
            break;
         case HallIconType.BURIED:
            _loc4_ = HallIcon.WONDERFULPLAY;
            _loc5_ = 14;
            break;
         case HallIconType.ACCUMULATIVE_LOGIN:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 1;
            break;
         case HallIconType.SEVENDAYTARGET:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 2;
            break;
         case HallIconType.GODSROADS:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 3;
            break;
         case HallIconType.LIMITACTIVITY:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 4;
            break;
         case HallIconType.OLDPLAYERREGRESS:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 5;
            break;
         case HallIconType.GROWTHPACKAGE:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 6;
            break;
         case HallIconType.LEFTGUNROULETTE:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 7;
            break;
         case HallIconType.GROUPPURCHASE:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 8;
            break;
         case HallIconType.NEWCHICKENBOX:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 9;
            break;
         case HallIconType.SUPERWINNER:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 10;
            break;
         case HallIconType.LUCKSTAR:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 11;
            break;
         case HallIconType.DICE:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 12;
            break;
         case HallIconType.TREASUREHUNTING:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 13;
            break;
         case HallIconType.GUILDMEMBERWEEK:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 14;
            break;
         case HallIconType.PYRAMID:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 15;
            break;
         case HallIconType.SYAH:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 16;
            break;
         case HallIconType.MYSTERIOUROULETTE:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 17;
            break;
         case HallIconType.CATCHBEAST:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 18;
            break;
         case HallIconType.LANTERNRIDDLES:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 19;
            break;
         case HallIconType.CHRISTMAS:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 20;
            break;
         case HallIconType.LUCKSTONE:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 21;
            break;
         case HallIconType.LIGHTROAD:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 22;
            break;
         case HallIconType.ENTERTAINMENT:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 23;
            break;
         case HallIconType.SALESHOP:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 24;
            break;
         case HallIconType.KINGDIVISION:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 25;
            break;
         case HallIconType.CHICKACTIVATION:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 26;
            break;
         case HallIconType.DDPLAY:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 27;
            break;
         case HallIconType.BOGUADVENTURE:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 28;
            break;
         case HallIconType.HAPPYRECHARGE:
            _loc4_ = HallIcon.ACTIVITY;
            _loc5_ = 29;
      }
      var _loc7_:HallIconInfo = new HallIconInfo();
      _loc7_.halltype = _loc4_;
      _loc7_.icontype = param1;
      _loc7_.isopen = param2;
      _loc7_.timemsg = param3;
      _loc7_.fightover = _loc6_;
      _loc7_.orderid = _loc5_;
      return _loc7_;
   }

   public function showCommonFrame(param1:DisplayObject, param2:String = "", param3:Number = 530, param4:Number = 545) : Frame
   {
      var _loc5_:Frame = ComponentFactory.Instance.creatCustomObject("hallIcon.commonFrame");
      _loc5_.titleText = LanguageMgr.GetTranslation(param2);
      _loc5_.width = param3;
      _loc5_.height = param4;
      _loc5_.addToContent(param1);
      _loc5_.addEventListener(FrameEvent.RESPONSE,this.__commonFrameResponse);
      LayerManager.Instance.addToLayer(_loc5_,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND,true);
      return _loc5_;
   }

   private function __commonFrameResponse(param1:FrameEvent) : void
   {
      var _loc2_:Frame = param1.currentTarget as Frame;
      if(_loc2_)
      {
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.__commonFrameResponse);
         ObjectUtils.disposeAllChildren(_loc2_);
         ObjectUtils.disposeObject(_loc2_);
         _loc2_ = null;
      }
   }

   public function checkHallIconExperienceTask(param1:Boolean = true) : void
   {
      if(param1)
      {
         this.model.cacheRightIconTask = null;
      }
      else
      {
         this.model.cacheRightIconTask = {"isCompleted":param1};
      }
      dispatchEvent(new HallIconEvent(HallIconEvent.CHECK_HALLICONEXPERIENCEOPEN));
   }

   public function checkCacheRightIconTask() : void
   {
      if(this.model.cacheRightIconTask)
      {
         dispatchEvent(new HallIconEvent(HallIconEvent.CHECK_HALLICONEXPERIENCEOPEN,this.model.cacheRightIconTask.isCompleted));
      }
   }
}
}
