package org.aswing.util
{
   public class ASWingVector implements List
   {
      
      public static const CASEINSENSITIVE:int = 1;
      
      public static const DESCENDING:int = 2;
      
      public static const UNIQUESORT:int = 4;
      
      public static const RETURNINDEXEDARRAY:int = 8;
      
      public static const NUMERIC:int = 16;
       
      
      protected var _elements:Array;
      
      public function ASWingVector()
      {
         super();
         this._elements = new Array();
      }
      
      public function each(operation:Function) : void
      {
         for(var i:int = 0; i < this._elements.length; i++)
         {
            operation(this._elements[i]);
         }
      }
      
      public function eachWithout(obj:Object, operation:Function) : void
      {
         for(var i:int = 0; i < this._elements.length; i++)
         {
            if(this._elements[i] != obj)
            {
               operation(this._elements[i]);
            }
         }
      }
      
      public function get(i:int) : *
      {
         return this._elements[i];
      }
      
      public function elementAt(i:int) : *
      {
         return this.get(i);
      }
      
      public function append(obj:*, index:int = -1) : void
      {
         if(index == -1)
         {
            this._elements.push(obj);
         }
         else
         {
            this._elements.splice(index,0,obj);
         }
      }
      
      public function appendAll(arr:Array, index:int = -1) : void
      {
         var right:Array = null;
         if(arr == null || arr.length <= 0)
         {
            return;
         }
         if(index == -1 || index == this._elements.length)
         {
            this._elements = this._elements.concat(arr);
         }
         else if(index == 0)
         {
            this._elements = arr.concat(this._elements);
         }
         else
         {
            right = this._elements.splice(index);
            this._elements = this._elements.concat(arr);
            this._elements = this._elements.concat(right);
         }
      }
      
      public function replaceAt(index:int, obj:*) : *
      {
         var oldObj:Object = null;
         if(index < 0 || index >= this.size())
         {
            return null;
         }
         oldObj = this._elements[index];
         this._elements[index] = obj;
         return oldObj;
      }
      
      public function removeAt(index:int) : *
      {
         var obj:Object = null;
         if(index < 0 || index >= this.size())
         {
            return null;
         }
         obj = this._elements[index];
         this._elements.splice(index,1);
         return obj;
      }
      
      public function remove(obj:*) : *
      {
         var i:int = this.indexOf(obj);
         if(i >= 0)
         {
            return this.removeAt(i);
         }
         return null;
      }
      
      public function removeRange(fromIndex:int, toIndex:int) : Array
      {
         fromIndex = Math.max(0,fromIndex);
         toIndex = Math.min(toIndex,this._elements.length - 1);
         if(fromIndex > toIndex)
         {
            return [];
         }
         return this._elements.splice(fromIndex,toIndex - fromIndex + 1);
      }
      
      public function indexOf(obj:*) : int
      {
         for(var i:int = 0; i < this._elements.length; i++)
         {
            if(this._elements[i] === obj)
            {
               return i;
            }
         }
         return -1;
      }
      
      public function appendList(list:List, index:int = -1) : void
      {
         this.appendAll(list.toArray(),index);
      }
      
      public function pop() : *
      {
         if(this.size() > 0)
         {
            return this._elements.pop();
         }
         return null;
      }
      
      public function shift() : *
      {
         if(this.size() > 0)
         {
            return this._elements.shift();
         }
         return undefined;
      }
      
      public function lastIndexOf(obj:*) : int
      {
         for(var i:int = this._elements.length - 1; i >= 0; i--)
         {
            if(this._elements[i] === obj)
            {
               return i;
            }
         }
         return -1;
      }
      
      public function contains(obj:*) : Boolean
      {
         return this.indexOf(obj) >= 0;
      }
      
      public function first() : *
      {
         return this._elements[0];
      }
      
      public function last() : *
      {
         return this._elements[this._elements.length - 1];
      }
      
      public function size() : int
      {
         return this._elements.length;
      }
      
      public function setElementAt(index:int, element:*) : void
      {
         this.replaceAt(index,element);
      }
      
      public function getSize() : int
      {
         return this.size();
      }
      
      public function clear() : void
      {
         if(!this.isEmpty())
         {
            this._elements.splice(0);
            this._elements = new Array();
         }
      }
      
      public function clone() : ASWingVector
      {
         var cloned:ASWingVector = new ASWingVector();
         for(var i:int = 0; i < this._elements.length; i++)
         {
            cloned.append(this._elements[i]);
         }
         return cloned;
      }
      
      public function isEmpty() : Boolean
      {
         if(this._elements.length > 0)
         {
            return false;
         }
         return true;
      }
      
      public function toArray() : Array
      {
         return this._elements.concat();
      }
      
      public function subArray(startIndex:int, length:int) : Array
      {
         return this._elements.slice(startIndex,Math.min(startIndex + length,this.size()));
      }
      
      public function sort(compare:Object, options:int) : Array
      {
         return this._elements.sort(compare,options);
      }
      
      public function sortOn(key:Object, options:int) : Array
      {
         return this._elements.sortOn(key,options);
      }
      
      public function toString() : String
      {
         return "Vector : " + this._elements.toString();
      }
   }
}
