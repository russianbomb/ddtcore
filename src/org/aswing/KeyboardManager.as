package org.aswing
{
   import flash.display.Stage;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.events.KeyboardEvent;
   import flash.ui.Keyboard;
   import org.aswing.util.ASWingVector;
   
   [Event(name="keyUp",type="flash.events.KeyboardEvent")]
   [Event(name="keyDown",type="flash.events.KeyboardEvent")]
   public class KeyboardManager extends EventDispatcher
   {
      
      private static var instance:KeyboardManager;
       
      
      private var keymaps:ASWingVector;
      
      private var keySequence:ASWingVector;
      
      private var selfKeyMap:KeyMap;
      
      private var inited:Boolean;
      
      private var keyJustActed:Boolean;
      
      public var isStopDispatching:Boolean;
      
      private var mnemonicModifier:Array;
      
      public function KeyboardManager()
      {
         super();
         this.inited = false;
         this.keyJustActed = false;
         this.keymaps = new ASWingVector();
         this.keySequence = new ASWingVector();
         this.selfKeyMap = new KeyMap();
         this.mnemonicModifier = [Keyboard.CONTROL,Keyboard.SHIFT];
         this.registerKeyMap(this.selfKeyMap);
      }
      
      public static function getInstance() : KeyboardManager
      {
         if(instance == null)
         {
            instance = new KeyboardManager();
         }
         return instance;
      }
      
      public static function isDown(keyCode:uint) : Boolean
      {
         return getInstance().isKeyDown(keyCode);
      }
      
      public function init(stage:Stage) : void
      {
         if(!this.inited)
         {
            this.inited = true;
            stage.addEventListener(KeyboardEvent.KEY_DOWN,this.__onKeyDown,false,0,true);
            stage.addEventListener(KeyboardEvent.KEY_UP,this.__onKeyUp,false,0,true);
            stage.addEventListener(Event.DEACTIVATE,this.__deactived,false,0,true);
         }
      }
      
      override public function dispatchEvent(event:Event) : Boolean
      {
         if(this.isStopDispatching)
         {
            return false;
         }
         return super.dispatchEvent(event);
      }
      
      public function registerKeyMap(keyMap:KeyMap) : void
      {
         if(!this.keymaps.contains(keyMap))
         {
            this.keymaps.append(keyMap);
         }
      }
      
      public function unregisterKeyMap(keyMap:KeyMap) : void
      {
         this.keymaps.remove(keyMap);
      }
      
      public function registerKeyAction(key:KeyType, action:Function) : void
      {
         this.selfKeyMap.registerKeyAction(key,action);
      }
      
      public function unregisterKeyAction(key:KeyType, action:Function) : void
      {
         this.selfKeyMap.unregisterKeyAction(key,action);
      }
      
      public function isKeyDown(keyCode:uint) : Boolean
      {
         return this.keySequence.contains(keyCode);
      }
      
      public function setMnemonicModifier(keyCodes:Array) : void
      {
         this.mnemonicModifier = keyCodes.concat();
      }
      
      public function isMnemonicModifierDown() : Boolean
      {
         for(var i:int = 0; i < this.mnemonicModifier.length; i++)
         {
            if(!this.isKeyDown(this.mnemonicModifier[i]))
            {
               return false;
            }
         }
         return this.mnemonicModifier.length > 0;
      }
      
      public function isKeyJustActed() : Boolean
      {
         return this.keyJustActed;
      }
      
      private function __onKeyDown(e:KeyboardEvent) : void
      {
         var keymap:KeyMap = null;
         this.dispatchEvent(e);
         var code:uint = e.keyCode;
         if(!this.keySequence.contains(code) && code < 139)
         {
            this.keySequence.append(code);
         }
         this.keyJustActed = false;
         var n:int = this.keymaps.size();
         for(var i:int = 0; i < n; i++)
         {
            keymap = KeyMap(this.keymaps.get(i));
            if(keymap.fireKeyAction(this.keySequence.toArray()))
            {
               this.keyJustActed = true;
            }
         }
      }
      
      private function __onKeyUp(e:KeyboardEvent) : void
      {
         this.dispatchEvent(e);
         var code:uint = e.keyCode;
         this.keySequence.remove(code);
         if(!e.ctrlKey)
         {
            this.keySequence.remove(Keyboard.CONTROL);
         }
         if(!e.shiftKey)
         {
            this.keySequence.remove(Keyboard.SHIFT);
         }
      }
      
      private function __deactived(e:Event) : void
      {
         this.keySequence.clear();
      }
      
      public function reset() : void
      {
         this.keySequence.clear();
      }
   }
}
