package org.bytearray.display
{
   import com.pickgliss.ui.core.Disposeable;
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.geom.Matrix;
   import flash.geom.Rectangle;
   
   public class ScaleBitmap extends Bitmap implements Disposeable
   {
       
      
      protected var _originalBitmap:BitmapData;
      
      protected var _scale9Grid:Rectangle = null;
      
      public function ScaleBitmap(bmpData:BitmapData = null, pixelSnapping:String = "auto", smoothing:Boolean = false)
      {
         super(bmpData,pixelSnapping,smoothing);
         this._originalBitmap = bmpData.clone();
      }
      
      override public function set bitmapData(bmpData:BitmapData) : void
      {
         if(this._originalBitmap)
         {
            this._originalBitmap.dispose();
         }
         this._originalBitmap = bmpData.clone();
         if(this._scale9Grid != null)
         {
            if(!this.validGrid(this._scale9Grid))
            {
               this._scale9Grid = null;
            }
            this.setSize(bmpData.width,bmpData.height);
         }
         else
         {
            this.assignBitmapData(this._originalBitmap.clone());
         }
      }
      
      public function dispose() : void
      {
         if(this._originalBitmap)
         {
            this._originalBitmap.dispose();
         }
         this._originalBitmap = null;
         bitmapData.dispose();
         if(this._scale9Grid)
         {
            this._scale9Grid = null;
         }
         if(parent)
         {
            parent.removeChild(this);
         }
      }
      
      public function getOriginalBitmapData() : BitmapData
      {
         return this._originalBitmap;
      }
      
      override public function set height(h:Number) : void
      {
         if(h != height)
         {
            this.setSize(width,h);
         }
      }
      
      override public function get scale9Grid() : Rectangle
      {
         return this._scale9Grid;
      }
      
      override public function set scale9Grid(r:Rectangle) : void
      {
         var currentWidth:Number = NaN;
         var currentHeight:Number = NaN;
         if(this._scale9Grid == null && r != null || this._scale9Grid != null && !this._scale9Grid.equals(r))
         {
            if(r == null)
            {
               currentWidth = width;
               currentHeight = height;
               this._scale9Grid = null;
               this.assignBitmapData(this._originalBitmap.clone());
               this.setSize(currentWidth,currentHeight);
            }
            else
            {
               if(!this.validGrid(r))
               {
                  throw new Error("#001 - The _scale9Grid does not match the original BitmapData");
               }
               this._scale9Grid = r.clone();
               this.resizeBitmap(width,height);
               scaleX = 1;
               scaleY = 1;
            }
         }
      }
      
      public function setSize(w:Number, h:Number) : void
      {
         if(this._scale9Grid == null)
         {
            super.width = w;
            super.height = h;
         }
         else
         {
            w = Math.max(w,this._originalBitmap.width - this._scale9Grid.width);
            h = Math.max(h,this._originalBitmap.height - this._scale9Grid.height);
            this.resizeBitmap(w,h);
         }
      }
      
      override public function set width(w:Number) : void
      {
         if(w != width)
         {
            this.setSize(w,height);
         }
      }
      
      protected function resizeBitmap(w:Number, h:Number) : void
      {
         var origin:Rectangle = null;
         var draw:Rectangle = null;
         var cy:int = 0;
         var bmpData:BitmapData = new BitmapData(w,h,true,0);
         var rows:Array = [0,this._scale9Grid.top,this._scale9Grid.bottom,this._originalBitmap.height];
         var cols:Array = [0,this._scale9Grid.left,this._scale9Grid.right,this._originalBitmap.width];
         var dRows:Array = [0,this._scale9Grid.top,h - (this._originalBitmap.height - this._scale9Grid.bottom),h];
         var dCols:Array = [0,this._scale9Grid.left,w - (this._originalBitmap.width - this._scale9Grid.right),w];
         var mat:Matrix = new Matrix();
         for(var cx:int = 0; cx < 3; cx++)
         {
            for(cy = 0; cy < 3; cy++)
            {
               origin = new Rectangle(cols[cx],rows[cy],cols[cx + 1] - cols[cx],rows[cy + 1] - rows[cy]);
               draw = new Rectangle(dCols[cx],dRows[cy],dCols[cx + 1] - dCols[cx],dRows[cy + 1] - dRows[cy]);
               mat.identity();
               mat.a = draw.width / origin.width;
               mat.d = draw.height / origin.height;
               mat.tx = draw.x - origin.x * mat.a;
               mat.ty = draw.y - origin.y * mat.d;
               bmpData.draw(this._originalBitmap,mat,null,null,draw,smoothing);
            }
         }
         this.assignBitmapData(bmpData);
      }
      
      private function assignBitmapData(bmp:BitmapData) : void
      {
         super.bitmapData.dispose();
         super.bitmapData = bmp;
      }
      
      private function validGrid(r:Rectangle) : Boolean
      {
         return r.right <= this._originalBitmap.width && r.bottom <= this._originalBitmap.height;
      }
   }
}
