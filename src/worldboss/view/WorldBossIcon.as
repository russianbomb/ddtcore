package worldboss.view
{
   import com.pickgliss.loader.BaseLoader;
   import com.pickgliss.loader.LoadResourceManager;
   import com.pickgliss.loader.LoaderEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.SoundManager;
   import ddt.manager.StateManager;
   import ddt.states.StateType;
   import ddtBuried.BuriedManager;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import worldboss.WorldBossManager;
   
   public class WorldBossIcon extends Sprite
   {
       
      
      private var _dragon:MovieClip;
      
      private var _isOpen:Boolean;
      
      public function WorldBossIcon()
      {
         super();
         this.init();
      }
      
      private function init() : void
      {
         var _loc1_:BaseLoader = LoadResourceManager.Instance.createLoader(WorldBossManager.Instance.iconEnterPath,BaseLoader.MODULE_LOADER);
         _loc1_.addEventListener(LoaderEvent.COMPLETE,this.onIconLoadedComplete);
         LoadResourceManager.Instance.startLoad(_loc1_);
      }
      
      private function onIconLoadedComplete(param1:Event) : void
      {
         this._dragon = ComponentFactory.Instance.creat("asset.hall.worldBossEntrance");
         var _loc2_:String = WorldBossManager.Instance.BossResourceId;
         this._dragon = ComponentFactory.Instance.creat("assets.hallIcon.worldBossEntrance_" + WorldBossManager.Instance.BossResourceId);
         this._dragon.buttonMode = true;
         addChild(this._dragon);
         if(WorldBossManager.Instance.bossInfo)
         {
            this._dragon.gotoAndStop(!!WorldBossManager.Instance.bossInfo.fightOver?2:1);
         }
         else
         {
            this._dragon.gotoAndStop(1);
         }
         this._dragon.y = 38;
         this.addEvent();
         if(!this._isOpen)
         {
            if(this._dragon.currentFrame == 2)
            {
               this.x = 41;
            }
            else
            {
               this.x = 50;
            }
         }
      }
      
      private function stopAllMc(param1:MovieClip) : void
      {
         var _loc3_:MovieClip = null;
         var _loc2_:int = 0;
         while(param1.numChildren - _loc2_)
         {
            if(param1.getChildAt(_loc2_) is MovieClip)
            {
               _loc3_ = param1.getChildAt(_loc2_) as MovieClip;
               _loc3_.stop();
               this.stopAllMc(_loc3_);
            }
            _loc2_++;
         }
      }
      
      private function playAllMc(param1:MovieClip) : void
      {
         var _loc3_:MovieClip = null;
         var _loc2_:int = 0;
         while(param1.numChildren - _loc2_)
         {
            if(param1.getChildAt(_loc2_) is MovieClip)
            {
               _loc3_ = param1.getChildAt(_loc2_) as MovieClip;
               _loc3_.play();
               this.playAllMc(_loc3_);
            }
            _loc2_++;
         }
      }
      
      public function set enble(param1:Boolean) : void
      {
         this._isOpen = param1;
         mouseEnabled = param1;
         mouseChildren = param1;
         if(!param1)
         {
            BuriedManager.Instance.applyGray(this);
         }
         else
         {
            BuriedManager.Instance.reGray(this);
            if(this._dragon)
            {
               this.playAllMc(this._dragon);
            }
         }
      }
      
      override public function get height() : Number
      {
         return 112;
      }
      
      private function addEvent() : void
      {
         this._dragon.addEventListener(MouseEvent.CLICK,this.__enterBossRoom);
      }
      
      private function removeEvent() : void
      {
         if(this._dragon)
         {
            this._dragon.removeEventListener(MouseEvent.CLICK,this.__enterBossRoom);
         }
      }
      
      private function __enterBossRoom(param1:MouseEvent) : void
      {
         SoundManager.instance.play("003");
         StateManager.setState(StateType.WORLDBOSS_AWARD);
      }
      
      public function setFrame(param1:int) : void
      {
         if(this._dragon)
         {
            this._dragon.gotoAndStop(param1);
         }
      }
      
      public function dispose() : void
      {
         this.removeEvent();
         ObjectUtils.disposeAllChildren(this);
         if(parent)
         {
            this.parent.removeChild(this);
         }
         this._dragon = null;
      }
   }
}
