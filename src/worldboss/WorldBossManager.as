package worldboss
{
   import baglocked.BaglockedManager;
   import com.pickgliss.loader.BaseLoader;
   import com.pickgliss.loader.LoadResourceManager;
   import com.pickgliss.loader.LoaderEvent;
   import com.pickgliss.manager.CacheSysManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.constants.CacheConsts;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.GameInSocketOut;
   import ddt.manager.LanguageMgr;
   import ddt.manager.LeavePageManager;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PathManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.StateManager;
   import ddt.manager.TimeManager;
   import ddt.states.StateType;
   import ddt.utils.Helpers;
   import ddt.view.UIModuleSmallLoading;
   import ddtActivityIcon.DdtActivityIconManager;
   import flash.display.MovieClip;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.geom.Point;
   import hallIcon.HallIconManager;
   import hallIcon.HallIconType;
   import road7th.comm.PackageIn;
   import road7th.data.DictionaryData;
   import worldBossHelper.WorldBossHelperManager;
   import worldBossHelper.event.WorldBossHelperEvent;
   import worldboss.event.WorldBossRoomEvent;
   import worldboss.model.WorldBossBuffInfo;
   import worldboss.model.WorldBossInfo;
   import worldboss.player.PlayerVO;
   import worldboss.player.RankingPersonInfo;
   import worldboss.view.WorldBossRankingFram;
   
   public class WorldBossManager extends EventDispatcher
   {
      
      private static var _instance:WorldBossManager;
      
      public static var IsSuccessStartGame:Boolean = false;
       
      
      private var _isOpen:Boolean = false;
      
      private var _mapload:BaseLoader;
      
      private var _bossInfo:WorldBossInfo;
      
      private var _currentPVE_ID:int;
      
      private var _sky:MovieClip;
      
      public var iconEnterPath:String;
      
      public var mapPath:String;
      
      private var _autoBuyBuffs:DictionaryData;
      
      private var _appearPos:Array;
      
      private var _isShowBlood:Boolean = false;
      
      private var _isBuyBuffAlert:Boolean = false;
      
      private var _bossResourceId:String;
      
      private var _rankingInfos:Vector.<RankingPersonInfo>;
      
      private var _autoBlood:Boolean = false;
      
      private var _mapLoader:BaseLoader;
      
      private var _isLoadingState:Boolean = false;
      
      public var worldBossNum:int;
      
      public function WorldBossManager()
      {
         this.iconEnterPath = this.getWorldbossResource() + "/icon/worldbossIcon.swf";
         this._autoBuyBuffs = new DictionaryData();
         this._appearPos = new Array();
         this._rankingInfos = new Vector.<RankingPersonInfo>();
         super();
      }
      
      public static function get Instance() : WorldBossManager
      {
         if(!WorldBossManager._instance)
         {
            WorldBossManager._instance = new WorldBossManager();
         }
         return WorldBossManager._instance;
      }
      
      public function setup() : void
      {
         this.worldBossNum = 0;
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.WORLDBOSS_INIT,this.__init);
      }
      
      public function get BossResourceId() : String
      {
         return this._bossResourceId;
      }
      
      public function get isBuyBuffAlert() : Boolean
      {
         return this._isBuyBuffAlert;
      }
      
      public function set isBuyBuffAlert(param1:Boolean) : void
      {
         this._isBuyBuffAlert = param1;
      }
      
      public function get autoBuyBuffs() : DictionaryData
      {
         return this._autoBuyBuffs;
      }
      
      public function get isShowBlood() : Boolean
      {
         return this._isShowBlood;
      }
      
      public function get isAutoBlood() : Boolean
      {
         return this._autoBlood;
      }
      
      public function get rankingInfos() : Vector.<RankingPersonInfo>
      {
         return this._rankingInfos;
      }
      
      private function __init(param1:CrazyTankSocketEvent) : void
      {
         var _loc8_:WorldBossBuffInfo = null;
         this._bossResourceId = param1.pkg.readUTF();
         this._currentPVE_ID = param1.pkg.readInt();
         param1.pkg.readUTF();
         this.addSocketEvent();
         this._bossInfo = new WorldBossInfo();
         this._bossInfo.myPlayerVO = new PlayerVO();
         this._bossInfo.name = param1.pkg.readUTF();
         this._bossInfo.total_Blood = param1.pkg.readLong();
         this._bossInfo.current_Blood = this._bossInfo.total_Blood;
         var _loc2_:int = param1.pkg.readInt();
         var _loc3_:int = param1.pkg.readInt();
         this.mapPath = this.getWorldbossResource() + "/map/worldbossMap.swf";
         this._appearPos.length = 0;
         var _loc4_:int = param1.pkg.readInt();
         var _loc5_:int = 0;
         while(_loc5_ < _loc4_)
         {
            this._appearPos.push(new Point(param1.pkg.readInt(),param1.pkg.readInt()));
            _loc5_++;
         }
         this._bossInfo.playerDefaultPos = Helpers.randomPick(this._appearPos);
         this._bossInfo.begin_time = param1.pkg.readDate();
         this._bossInfo.end_time = param1.pkg.readDate();
         this._bossInfo.fight_time = param1.pkg.readInt();
         this._bossInfo.fightOver = param1.pkg.readBoolean();
         this._bossInfo.roomClose = param1.pkg.readBoolean();
         this._bossInfo.ticketID = param1.pkg.readInt();
         this._bossInfo.need_ticket_count = param1.pkg.readInt();
         this._bossInfo.timeCD = param1.pkg.readInt();
         this._bossInfo.reviveMoney = param1.pkg.readInt();
         this._bossInfo.reFightMoney = param1.pkg.readInt();
         this._bossInfo.addInjureBuffMoney = param1.pkg.readInt();
         this._bossInfo.addInjureValue = param1.pkg.readInt();
         this._bossInfo.addInjureBuffMoney = this._bossInfo.addInjureBuffMoney * 20;
         this._bossInfo.addInjureValue = this._bossInfo.addInjureValue * 20;
         this._bossInfo.buffArray.length = 0;
         var _loc6_:int = param1.pkg.readInt();
         var _loc7_:int = 0;
         while(_loc7_ < _loc6_)
         {
            _loc8_ = new WorldBossBuffInfo();
            _loc8_.ID = param1.pkg.readInt();
            _loc8_.name = param1.pkg.readUTF();
            _loc8_.price = param1.pkg.readInt();
            _loc8_.decription = param1.pkg.readUTF();
            _loc8_.costID = param1.pkg.readInt();
            this._bossInfo.buffArray.push(_loc8_);
            _loc7_++;
         }
         this._isShowBlood = param1.pkg.readBoolean();
         this._autoBlood = param1.pkg.readBoolean();
         this.isOpen = true;
         this.addshowHallEntranceBtn();
         WorldBossManager.Instance.isLoadingState = false;
         this.worldBossNum++;
         dispatchEvent(new WorldBossRoomEvent(WorldBossRoomEvent.GAME_INIT));
         if(WorldBossHelperManager.Instance.isInWorldBossHelperFrame && WorldBossHelperManager.Instance.helperOpen)
         {
            if(WorldBossHelperManager.Instance.isHelperOnlyOnce)
            {
               if(this.worldBossNum > 1)
               {
                  return;
               }
               WorldBossHelperManager.Instance.dispatchEvent(new WorldBossHelperEvent(WorldBossHelperEvent.BOSS_OPEN));
            }
            else
            {
               WorldBossHelperManager.Instance.dispatchEvent(new WorldBossHelperEvent(WorldBossHelperEvent.BOSS_OPEN));
            }
         }
      }
      
      private function addSocketEvent() : void
      {
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.WORLDBOSS_ENTER,this.__enter);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.WORLDBOSS_BLOOD_UPDATE,this.__update);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.WORLDBOSS_FIGHTOVER,this.__fightOver);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.WORLDBOSS_ROOMCLOSE,this.__leaveRoom);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.WORLDBOSS_RANKING,this.__showRanking);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.WORLDBOSS_OVER,this.__allOver);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GAME_ROOM_FULL,this.__gameRoomFull);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.UPDATE_BUFF_LEVEL,this.__updateBuffLevel);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.WORLDBOSS_PRIVATE_INFO,this.__updatePrivateInfo);
      }
      
      private function removeSocketEvent() : void
      {
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.WORLDBOSS_ENTER,this.__enter);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.WORLDBOSS_BLOOD_UPDATE,this.__update);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.WORLDBOSS_FIGHTOVER,this.__fightOver);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.WORLDBOSS_ROOMCLOSE,this.__leaveRoom);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.WORLDBOSS_RANKING,this.__showRanking);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.WORLDBOSS_OVER,this.__allOver);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.GAME_ROOM_FULL,this.__gameRoomFull);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.UPDATE_BUFF_LEVEL,this.__updateBuffLevel);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.WORLDBOSS_PRIVATE_INFO,this.__updatePrivateInfo);
      }
      
      protected function __updatePrivateInfo(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         WorldBossManager.Instance.bossInfo.myPlayerVO.myDamage = _loc2_.readInt();
         WorldBossManager.Instance.bossInfo.myPlayerVO.myHonor = _loc2_.readInt();
         dispatchEvent(new Event(Event.CHANGE));
      }
      
      protected function __updateBuffLevel(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         WorldBossManager.Instance.bossInfo.myPlayerVO.buffLevel = _loc2_.readInt();
         WorldBossManager.Instance.bossInfo.myPlayerVO.buffInjure = _loc2_.readInt();
         dispatchEvent(new Event(Event.CHANGE));
      }
      
      private function __gameRoomFull(param1:CrazyTankSocketEvent) : void
      {
         dispatchEvent(new WorldBossRoomEvent(WorldBossRoomEvent.WORLDBOSS_ROOM_FULL));
      }
      
      public function creatEnterIcon(param1:Boolean = true, param2:int = 1, param3:String = null) : void
      {
         this._bossResourceId = param2.toString();
         HallIconManager.instance.updateSwitchHandler(HallIconType["WORLDBOSSENTRANCE" + this._bossResourceId],true,param3);
      }
      
      public function disposeEnterBtn() : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType["WORLDBOSSENTRANCE" + this._bossResourceId],false);
      }
      
      private function __enter(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         if(param1.pkg.bytesAvailable > 0 && param1.pkg.readBoolean())
         {
            this._bossInfo.isLiving = !param1.pkg.readBoolean();
            this._bossInfo.myPlayerVO.reviveCD = param1.pkg.readInt();
            _loc2_ = param1.pkg.readInt();
            _loc3_ = 0;
            while(_loc3_ < _loc2_)
            {
               this._bossInfo.myPlayerVO.buffID = param1.pkg.readInt();
               _loc3_++;
            }
            if(this._bossInfo.myPlayerVO.reviveCD > 0)
            {
               this._bossInfo.myPlayerVO.playerStauts = 3;
               this._bossInfo.myPlayerVO.playerPos = new Point(int(Math.random() * 300 + 300),int(Math.random() * 850) + 350);
            }
            else
            {
               this._bossInfo.myPlayerVO.playerPos = this._bossInfo.playerDefaultPos;
               this._bossInfo.myPlayerVO.playerStauts = 1;
            }
            dispatchEvent(new WorldBossRoomEvent(WorldBossRoomEvent.ALLOW_ENTER));
            this.loadMap();
         }
      }
      
      private function loadMap() : void
      {
         this._mapLoader = LoadResourceManager.Instance.createLoader(WorldBossManager.Instance.mapPath,BaseLoader.MODULE_LOADER);
         this._mapLoader.addEventListener(LoaderEvent.COMPLETE,this.onMapSrcLoadedComplete);
         LoadResourceManager.Instance.startLoad(this._mapLoader);
      }
      
      private function onMapSrcLoadedComplete(param1:Event) : void
      {
         if(StateManager.getState(StateType.WORLDBOSS_ROOM) == null)
         {
            UIModuleSmallLoading.Instance.addEventListener(Event.CLOSE,this.__loadingIsCloseRoom);
         }
         StateManager.setState(StateType.WORLDBOSS_ROOM);
      }
      
      private function __loadingIsCloseRoom(param1:Event) : void
      {
         UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this.__loadingIsCloseRoom);
      }
      
      private function __update(param1:CrazyTankSocketEvent) : void
      {
         this._autoBlood = param1.pkg.readBoolean();
         this._bossInfo.total_Blood = param1.pkg.readLong();
         this._bossInfo.current_Blood = param1.pkg.readLong();
         dispatchEvent(new WorldBossRoomEvent(WorldBossRoomEvent.BOSS_HP_UPDATA));
      }
      
      private function __fightOver(param1:CrazyTankSocketEvent) : void
      {
         this._bossInfo.fightOver = true;
         this._bossInfo.isLiving = !param1.pkg.readBoolean();
         dispatchEvent(new WorldBossRoomEvent(WorldBossRoomEvent.FIGHT_OVER));
      }
      
      private function __leaveRoom(param1:Event) : void
      {
         this._bossInfo.roomClose = true;
         if(StateManager.currentStateType == StateType.WORLDBOSS_ROOM)
         {
            StateManager.setState(StateType.MAIN);
            WorldBossRoomController.Instance.dispose();
         }
         dispatchEvent(new WorldBossRoomEvent(WorldBossRoomEvent.ROOM_CLOSE));
      }
      
      private function __showRanking(param1:CrazyTankSocketEvent) : void
      {
         if(param1.pkg.readBoolean())
         {
            this.showRankingFrame(param1.pkg);
         }
         else
         {
            this.showRankingInRoom(param1.pkg);
         }
      }
      
      private function showRankingFrame(param1:PackageIn) : void
      {
         var _loc5_:RankingPersonInfo = null;
         this._rankingInfos.length = 0;
         WorldBossRankingFram._rankingPersons = new Array();
         var _loc2_:WorldBossRankingFram = ComponentFactory.Instance.creat("worldboss.ranking.frame");
         var _loc3_:int = param1.readInt();
         var _loc4_:int = 0;
         while(_loc4_ < _loc3_)
         {
            _loc5_ = new RankingPersonInfo();
            _loc5_.id = param1.readInt();
            _loc5_.name = param1.readUTF();
            _loc5_.damage = param1.readInt();
            _loc2_.addPersonRanking(_loc5_);
            this._rankingInfos.push(_loc5_);
            _loc4_++;
         }
         if(!(CacheSysManager.isLock(CacheConsts.ALERT_IN_FIGHT) && StateManager.currentStateType != StateType.WORLDBOSS_ROOM))
         {
            _loc2_.show();
         }
      }
      
      private function showRankingInRoom(param1:PackageIn) : void
      {
         dispatchEvent(new CrazyTankSocketEvent(CrazyTankSocketEvent.WORLDBOSS_RANKING_INROOM,param1));
      }
      
      private function __allOver(param1:CrazyTankSocketEvent) : void
      {
         this._bossInfo.fightOver = true;
         this._bossInfo.roomClose = true;
         this.isOpen = false;
         this.disposeEnterBtn();
         this.removeSocketEvent();
         DdtActivityIconManager.Instance.currObj = null;
      }
      
      public function enterGame() : void
      {
         SocketManager.Instance.out.enterUserGuide(WorldBossManager.Instance.currentPVE_ID,14);
         if(!WorldBossManager.Instance.bossInfo.fightOver)
         {
            IsSuccessStartGame = true;
            GameInSocketOut.sendGameStart();
            SocketManager.Instance.out.sendWorldBossRoomStauts(2);
            dispatchEvent(new Event(WorldBossRoomEvent.ENTERING_GAME));
         }
         else
         {
            StateManager.setState(StateType.WORLDBOSS_ROOM);
         }
      }
      
      public function exitGame() : void
      {
         IsSuccessStartGame = false;
         GameInSocketOut.sendGamePlayerExit();
      }
      
      public function addshowHallEntranceBtn() : void
      {
         if(this.isOpen && StateManager.currentStateType == StateType.MAIN)
         {
            HallIconManager.instance.updateSwitchHandler(HallIconType["WORLDBOSSENTRANCE" + this._bossResourceId],true);
         }
      }
      
      public function showHallSkyEffort(param1:MovieClip) : void
      {
         if(param1 == this._sky)
         {
            return;
         }
         ObjectUtils.disposeObject(this._sky);
         this._sky = param1;
      }
      
      public function set isOpen(param1:Boolean) : void
      {
         this._isOpen = param1;
         if(StateManager.currentStateType == StateType.MAIN)
         {
            if(this._sky)
            {
               this._sky.visible = !this._bossInfo.fightOver;
            }
         }
         if((StateManager.currentStateType == StateType.WORLDBOSS_AWARD || StateManager.currentStateType == StateType.WORLDBOSS_ROOM) && !this.isOpen)
         {
            StateManager.setState(StateType.MAIN);
         }
      }
      
      public function get isOpen() : Boolean
      {
         return this._isOpen;
      }
      
      public function get currentPVE_ID() : int
      {
         return this._currentPVE_ID;
      }
      
      public function get bossInfo() : WorldBossInfo
      {
         return this._bossInfo;
      }
      
      public function getWorldbossResource() : String
      {
         var _loc1_:String = this._bossResourceId == null?"1":this._bossResourceId;
         return PathManager.SITE_MAIN + "image/worldboss/" + _loc1_;
      }
      
      public function showRankingText() : void
      {
         var _loc3_:RankingPersonInfo = null;
         WorldBossRankingFram._rankingPersons = new Array();
         var _loc1_:WorldBossRankingFram = ComponentFactory.Instance.creat("worldboss.ranking.frame");
         var _loc2_:int = 0;
         while(_loc2_ < 10)
         {
            _loc3_ = new RankingPersonInfo();
            _loc3_.id = 1;
            _loc3_.name = "hawang" + _loc2_;
            _loc3_.damage = 2 * _loc2_ + _loc2_ * _loc2_ + 50;
            _loc1_.addPersonRanking(_loc3_);
            _loc2_++;
         }
         _loc1_.show();
      }
      
      public function buyNewBuff(param1:int, param2:Boolean) : void
      {
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         var _loc3_:int = this.bossInfo.myPlayerVO.buffLevel;
         if(_loc3_ >= 20)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.worldboss.buffLevelMax"));
            return;
         }
         var _loc4_:int = WorldBossManager.Instance.bossInfo.addInjureBuffMoney;
         var _loc5_:int = _loc4_;
         if(param2)
         {
            _loc5_ = (20 - _loc3_) * _loc4_;
         }
         if(param1 == 1)
         {
            if(PlayerManager.Instance.Self.Money < _loc5_)
            {
               LeavePageManager.showFillFrame();
               return;
            }
         }
         else if(param1 == 2)
         {
            if(PlayerManager.Instance.Self.BandMoney < _loc5_)
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("store.view.transfer.StoreIITransferBG.lijinbuzu"));
               return;
            }
         }
         SocketManager.Instance.out.sendNewBuyWorldBossBuff(param1,!!param2?20 - _loc3_:1);
      }
      
      public function set isLoadingState(param1:Boolean) : void
      {
         this._isLoadingState = param1;
      }
      
      public function get isLoadingState() : Boolean
      {
         return this._isLoadingState;
      }
      
      public function get beforeStartTime() : int
      {
         if(!this._bossInfo || !this._bossInfo.begin_time)
         {
            return 0;
         }
         return this.getDateHourTime(this._bossInfo.begin_time) + 300 - this.getDateHourTime(TimeManager.Instance.Now());
      }
      
      private function getDateHourTime(param1:Date) : int
      {
         return int(param1.hours * 3600 + param1.minutes * 60 + param1.seconds);
      }
   }
}
