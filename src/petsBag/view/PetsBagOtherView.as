package petsBag.view
{
   import com.pickgliss.ui.ComponentFactory;
   
   public class PetsBagOtherView extends PetsBagView
   {
       
      
      public function PetsBagOtherView()
      {
         super();
      }
      
      override protected function initView() : void
      {
         super.initView();
         _petSkillPnl = ComponentFactory.Instance.creat("petsBag.petSkillPnl",[true]);
         addChild(_petSkillPnl);
         _bgSkillPnl.width = 408;
         _fightBtn.visible = false;
      }
      
      override public function dispose() : void
      {
         super.dispose();
      }
   }
}
