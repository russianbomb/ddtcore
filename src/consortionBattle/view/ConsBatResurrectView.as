package consortionBattle.view
{
   import baglocked.BaglockedManager;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import consortionBattle.ConsortiaBattleManager;
   import consortionBattle.player.ConsortiaBattlePlayerInfo;
   import ddt.manager.LanguageMgr;
   import ddt.manager.LeavePageManager;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.manager.TimeManager;
   import ddt.utils.PositionUtils;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.events.TimerEvent;
   import flash.utils.Timer;
   import flash.utils.getTimer;
   
   public class ConsBatResurrectView extends Sprite implements Disposeable
   {
      
      public static const FIGHT:int = 2;
       
      
      private var _bg:ScaleBitmapImage;
      
      private var _resurrectBtn:SimpleBitmapButton;
      
      private var _stayResBtn:SimpleBitmapButton;
      
      private var _timeCD:MovieClip;
      
      private var _txtProp:FilterFrameText;
      
      private var _totalCount:int;
      
      private var timer:Timer;
      
      private var _consumeCount:int = 5;
      
      private var _lastCreatTime2:int = 0;
      
      private var _lastCreatTime:int = 0;
      
      public function ConsBatResurrectView(param1:int)
      {
         super();
         this.x = -113;
         this.y = -121;
         this._totalCount = param1;
         this.init();
         this.addEvent();
      }
      
      private function init() : void
      {
         this._bg = ComponentFactory.Instance.creatComponentByStylename("consortiaBattle.resurrectBg");
         addChild(this._bg);
         this._txtProp = ComponentFactory.Instance.creatComponentByStylename("consortiaBattle.resurrect.txtProp");
         addChild(this._txtProp);
         this._txtProp.text = LanguageMgr.GetTranslation("worldboss.resurrectView.prop");
         this._timeCD = ComponentFactory.Instance.creat("asset.consortiaBattle.resurrectTimeCD");
         PositionUtils.setPos(this._timeCD,"consortiaBattle.timeCDPos");
         addChild(this._timeCD);
         this._resurrectBtn = ComponentFactory.Instance.creatComponentByStylename("consortiaBattle.resurrect.btn");
         addChild(this._resurrectBtn);
         this._stayResBtn = ComponentFactory.Instance.creatComponentByStylename("consortiaBattle.stayRes.btn");
         addChild(this._stayResBtn);
         this.timer = new Timer(1000,this._totalCount + 1);
         this.timer.addEventListener(TimerEvent.TIMER,this.__startCount);
         this.timer.start();
         this.__startCount(null);
      }
      
      private function __startCount(param1:TimerEvent) : void
      {
         if(this._totalCount < 0)
         {
            this.__timerComplete();
            return;
         }
         var _loc2_:String = this.setFormat(int(this._totalCount / 3600)) + ":" + this.setFormat(int(this._totalCount / 60 % 60)) + ":" + this.setFormat(int(this._totalCount % 60));
         (this._timeCD["timeHour2"] as MovieClip).gotoAndStop("num_" + _loc2_.charAt(0));
         (this._timeCD["timeHour"] as MovieClip).gotoAndStop("num_" + _loc2_.charAt(1));
         (this._timeCD["timeMint2"] as MovieClip).gotoAndStop("num_" + _loc2_.charAt(3));
         (this._timeCD["timeMint"] as MovieClip).gotoAndStop("num_" + _loc2_.charAt(4));
         (this._timeCD["timeSecond2"] as MovieClip).gotoAndStop("num_" + _loc2_.charAt(6));
         (this._timeCD["timeSecond"] as MovieClip).gotoAndStop("num_" + _loc2_.charAt(7));
         this._totalCount--;
      }
      
      private function setFormat(param1:int) : String
      {
         var _loc2_:String = param1.toString();
         if(param1 < 10)
         {
            _loc2_ = "0" + _loc2_;
         }
         return _loc2_;
      }
      
      protected function __timerComplete() : void
      {
         if(this._consumeCount >= 5)
         {
            SocketManager.Instance.out.sendConsBatConsume(5,true);
            this._consumeCount = 0;
         }
         else
         {
            this._consumeCount++;
         }
      }
      
      private function addEvent() : void
      {
         this._resurrectBtn.addEventListener(MouseEvent.CLICK,this.__resurrect,false,0,true);
         this._stayResBtn.addEventListener(MouseEvent.CLICK,this.__stayRes,false,0,true);
      }
      
      private function __stayRes(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         if(getTimer() - this._lastCreatTime > 1000)
         {
            this._lastCreatTime = getTimer();
            this.promptlyStayRevive();
         }
      }
      
      protected function promptlyStayRevive() : void
      {
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         var _loc1_:Object = ConsortiaBattleManager.instance.getBuyRecordStatus(3);
         if(_loc1_.isNoPrompt)
         {
            if(_loc1_.isBand && PlayerManager.Instance.Self.BandMoney < 350)
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.consortiaBattle.buy.noEnoughBindMoneyTxt"));
               _loc1_.isNoPrompt = false;
            }
            else if(!_loc1_.isBand && PlayerManager.Instance.Self.Money < 350)
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.consortiaBattle.buy.noEnoughMoneyTxt"));
               _loc1_.isNoPrompt = false;
            }
            else
            {
               SocketManager.Instance.out.sendConsBatConsume(4,_loc1_.isBand);
               return;
            }
         }
         var _loc2_:BaseAlerFrame = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("ddt.consortiaBattle.buyStayResurrect.promptTxt"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),true,true,true,LayerManager.BLCAK_BLOCKGOUND,null,"ConsBatBuyConfirmView",30,true);
         _loc2_.moveEnable = false;
         _loc2_.addEventListener(FrameEvent.RESPONSE,this.__StayResurrectConfirm,false,0,true);
      }
      
      protected function __StayResurrectConfirm(param1:FrameEvent) : void
      {
         var _loc3_:BaseAlerFrame = null;
         var _loc4_:Object = null;
         SoundManager.instance.play("008");
         var _loc2_:BaseAlerFrame = param1.currentTarget as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.__StayResurrectConfirm);
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            if(_loc2_.isBand && PlayerManager.Instance.Self.BandMoney < 50)
            {
               _loc3_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("ddt.consortiaBattle.changeMoneyCostTxt"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),true,true,true,LayerManager.BLCAK_BLOCKGOUND);
               _loc3_.moveEnable = false;
               _loc3_.addEventListener(FrameEvent.RESPONSE,this.__StayResurrectTwoConfirm,false,0,true);
               return;
            }
            if(!_loc2_.isBand && PlayerManager.Instance.Self.Money < 50)
            {
               LeavePageManager.showFillFrame();
               return;
            }
            if((_loc2_ as ConsBatBuyConfirmView).isNoPrompt)
            {
               _loc4_ = ConsortiaBattleManager.instance.getBuyRecordStatus(3);
               _loc4_.isNoPrompt = true;
               _loc4_.isBand = _loc2_.isBand;
            }
            SocketManager.Instance.out.sendConsBatConsume(4,_loc2_.isBand);
         }
      }
      
      protected function __StayResurrectTwoConfirm(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         var _loc2_:BaseAlerFrame = param1.currentTarget as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.__StayResurrectTwoConfirm);
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            if(PlayerManager.Instance.Self.Money < 80)
            {
               LeavePageManager.showFillFrame();
               return;
            }
            SocketManager.Instance.out.sendConsBatConsume(4,false);
         }
      }
      
      private function __resurrect(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         if(getTimer() - this._lastCreatTime > 1000)
         {
            this._lastCreatTime = getTimer();
            this.promptlyRevive();
         }
      }
      
      protected function promptlyRevive() : void
      {
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         var _loc1_:Object = ConsortiaBattleManager.instance.getBuyRecordStatus(2);
         if(_loc1_.isNoPrompt)
         {
            if(_loc1_.isBand && PlayerManager.Instance.Self.BandMoney < 5)
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.consortiaBattle.buy.noEnoughBindMoneyTxt"));
               _loc1_.isNoPrompt = false;
            }
            else if(!_loc1_.isBand && PlayerManager.Instance.Self.Money < 5)
            {
               LeavePageManager.showFillFrame();
               _loc1_.isNoPrompt = false;
            }
            else
            {
               ConsortiaBattleManager.instance.addEventListener(ConsortiaBattleManager.UPDATE_SCENE_INFO,this.updateInfo);
               SocketManager.Instance.out.sendConsBatConsume(3,_loc1_.isBand);
               return;
            }
         }
         var _loc2_:BaseAlerFrame = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("ddt.consortiaBattle.buyResurrect.promptTxt",5),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),true,true,true,LayerManager.BLCAK_BLOCKGOUND,null,"ConsBatBuyConfirmView",30,true);
         _loc2_.moveEnable = false;
         _loc2_.addEventListener(FrameEvent.RESPONSE,this.__resurrectConfirm,false,0,true);
      }
      
      protected function __resurrectConfirm(param1:FrameEvent) : void
      {
         var _loc3_:Object = null;
         SoundManager.instance.play("008");
         var _loc2_:BaseAlerFrame = param1.currentTarget as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.__resurrectConfirm);
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            if(_loc2_.isBand && PlayerManager.Instance.Self.BandMoney < 5)
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.consortiaBattle.buy.noEnoughBindMoneyTxt"));
               return;
            }
            if(!_loc2_.isBand && PlayerManager.Instance.Self.Money < 5)
            {
               LeavePageManager.showFillFrame();
               return;
            }
            if((_loc2_ as ConsBatBuyConfirmView).isNoPrompt)
            {
               _loc3_ = ConsortiaBattleManager.instance.getBuyRecordStatus(2);
               _loc3_.isNoPrompt = true;
               _loc3_.isBand = _loc2_.isBand;
            }
            ConsortiaBattleManager.instance.addEventListener(ConsortiaBattleManager.UPDATE_SCENE_INFO,this.updateInfo);
            SocketManager.Instance.out.sendConsBatConsume(3,_loc2_.isBand);
         }
      }
      
      private function updateInfo(param1:Event) : void
      {
         var _loc2_:ConsortiaBattlePlayerInfo = ConsortiaBattleManager.instance.getPlayerInfo(PlayerManager.Instance.Self.ID);
         if(_loc2_.tombstoneEndTime.getTime() < TimeManager.Instance.Now().getTime())
         {
            ConsortiaBattleManager.instance.removeEventListener(ConsortiaBattleManager.UPDATE_SCENE_INFO,this.updateInfo);
            this.__timerComplete();
         }
      }
      
      protected function __resurrectTwoConfirm(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         var _loc2_:BaseAlerFrame = param1.currentTarget as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.__resurrectTwoConfirm);
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            if(PlayerManager.Instance.Self.Money < 50)
            {
               LeavePageManager.showFillFrame();
               return;
            }
            SocketManager.Instance.out.sendConsBatConsume(3,false);
         }
      }
      
      private function removeEvent() : void
      {
         if(this._resurrectBtn)
         {
            this._resurrectBtn.removeEventListener(MouseEvent.CLICK,this.__resurrect);
         }
         if(this._stayResBtn)
         {
            this._stayResBtn.removeEventListener(MouseEvent.CLICK,this.__stayRes);
         }
      }
      
      public function dispose() : void
      {
         this.removeEvent();
         if(this.timer)
         {
            this.timer.stop();
            this.timer.removeEventListener(TimerEvent.TIMER,this.__startCount);
         }
         ConsortiaBattleManager.instance.removeEventListener(ConsortiaBattleManager.UPDATE_SCENE_INFO,this.updateInfo);
         ObjectUtils.disposeAllChildren(this);
         if(parent)
         {
            this.parent.removeChild(this);
         }
         this._bg = null;
         this._txtProp = null;
         this._resurrectBtn = null;
         this._stayResBtn = null;
         this._timeCD = null;
      }
   }
}
