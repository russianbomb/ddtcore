package com.demonsters.debugger 
{
    import flash.utils.*;
    
    public class MonsterDebuggerData extends Object
    {
        public function MonsterDebuggerData(arg1:String, arg2:Object)
        {
            super();
            _id = arg1;
            _data = arg2;
            return;
        }

        public function get data():Object
        {
            return _data;
        }

        public function set bytes(arg1:flash.utils.ByteArray):void
        {
            var value:flash.utils.ByteArray;
            var bytesData:flash.utils.ByteArray;
            var bytesId:flash.utils.ByteArray;

            var loc1:*;
            value = arg1;
            bytesId = new flash.utils.ByteArray();
            bytesData = new flash.utils.ByteArray();
            try 
            {
                value.readBytes(bytesId, 0, value.readUnsignedInt());
                value.readBytes(bytesData, 0, value.readUnsignedInt());
                _id = bytesId.readObject() as String;
                _data = bytesData.readObject() as Object;
            }
            catch (e:Error)
            {
                _id = null;
                _data = null;
            }
            bytesId = null;
            bytesData = null;
            return;
        }

        public function get id():String
        {
            return _id;
        }

        public function get bytes():flash.utils.ByteArray
        {
            var loc1:*=new flash.utils.ByteArray();
            var loc2:*=new flash.utils.ByteArray();
            loc1.writeObject(_id);
            loc2.writeObject(_data);
            var loc3:*=new flash.utils.ByteArray();
            loc3.writeUnsignedInt(loc1.length);
            loc3.writeBytes(loc1);
            loc3.writeUnsignedInt(loc2.length);
            loc3.writeBytes(loc2);
            loc3.position = 0;
            loc1 = null;
            loc2 = null;
            return loc3;
        }

        public static function read(arg1:flash.utils.ByteArray):com.demonsters.debugger.MonsterDebuggerData
        {
            var loc1:*=new MonsterDebuggerData(null, null);
            loc1.bytes = arg1;
            return loc1;
        }

        internal var _data:Object;

        internal var _id:String;
    }
}
