package com.demonsters.debugger
{
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Stage;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.system.System;
	import flash.utils.Dictionary;
	import flash.utils.getQualifiedClassName;
	
	class MonsterDebuggerUtils
	{
		
		private static var _references:Dictionary = new Dictionary(true);
		
		private static var _reference:int = 0;
		
		
		function MonsterDebuggerUtils()
		{
			super();
		}
		
		public static function snapshot(param1:DisplayObject, param2:Rectangle = null) : BitmapData
		{
			var bitmapData:BitmapData = null;
			var m:Matrix = null;
			var scaled:Rectangle = null;
			var s:Number = NaN;
			var b:BitmapData = null;
			var object:DisplayObject = param1;
			var rectangle:Rectangle = param2;
			if(object == null)
			{
				return null;
			}
			var visible:Boolean = object.visible;
			var alpha:Number = object.alpha;
			var rotation:Number = object.rotation;
			var scaleX:Number = object.scaleX;
			var scaleY:Number = object.scaleY;
			try
			{
				object.visible = true;
				object.alpha = 1;
				object.rotation = 0;
				object.scaleX = 1;
				object.scaleY = 1;
			}
			catch(e1:Error)
			{
			}
			var bounds:Rectangle = object.getBounds(object);
			bounds.x = int(bounds.x + 0.5);
			bounds.y = int(bounds.y + 0.5);
			bounds.width = int(bounds.width + 0.5);
			bounds.height = int(bounds.height + 0.5);
			if(object is Stage)
			{
				bounds.x = 0;
				bounds.y = 0;
				bounds.width = Stage(object).stageWidth;
				bounds.height = Stage(object).stageHeight;
			}
			bitmapData = null;
			if(bounds.width <= 0 || bounds.height <= 0)
			{
				return null;
			}
			try
			{
				bitmapData = new BitmapData(bounds.width,bounds.height,false,16777215);
				m = new Matrix();
				m.tx = -bounds.x;
				m.ty = -bounds.y;
				bitmapData.draw(object,m,null,null,null,false);
			}
			catch(e2:Error)
			{
				bitmapData = null;
			}
			try
			{
				object.visible = visible;
				object.alpha = alpha;
				object.rotation = rotation;
				object.scaleX = scaleX;
				object.scaleY = scaleY;
			}
			catch(e3:Error)
			{
			}
			if(bitmapData == null)
			{
				return null;
			}
			if(rectangle != null)
			{
				if(bounds.width <= rectangle.width && bounds.height <= rectangle.height)
				{
					return bitmapData;
				}
				scaled = bounds.clone();
				scaled.width = rectangle.width;
				scaled.height = rectangle.width * (bounds.height / bounds.width);
				if(scaled.height > rectangle.height)
				{
					scaled = bounds.clone();
					scaled.width = rectangle.height * (bounds.width / bounds.height);
					scaled.height = rectangle.height;
				}
				s = scaled.width / bounds.width;
				try
				{
					b = new BitmapData(scaled.width,scaled.height,false,0);
					m = new Matrix();
					m.scale(s,s);
					b.draw(bitmapData,m,null,null,null,true);
					bitmapData.dispose();
					bitmapData = b;
				}
				catch(e4:Error)
				{
					bitmapData.dispose();
					bitmapData = null;
				}
			}
			return bitmapData;
		}
		
		private static function parseClass(param1:*, param2:String, param3:XML, param4:int = 1, param5:int = 5, param6:Boolean = true) : XML
		{
			var key:String = null;
			var itemsArrayLength:int = 0;
			var item:* = undefined;
			var itemXML:XML = null;
			var itemAccess:String = null;
			var itemPermission:String = null;
			var itemIcon:String = null;
			var itemType:String = null;
			var itemName:String = null;
			var itemTarget:String = null;
			var i:int = 0;
			var prop:* = undefined;
			var displayObject:DisplayObjectContainer = null;
			var displayObjects:Array = null;
			var child:DisplayObject = null;
			var object:* = param1;
			var target:String = param2;
			var description:XML = param3;
			var currentDepth:int = param4;
			var maxDepth:int = param5;
			var includeDisplayObjects:Boolean = param6;
			var rootXML:XML = <root/>;
			var nodeXML:XML = <node/>;
			var variables:XMLList = description..variable;
			var accessors:XMLList = description..accessor;
			var constants:XMLList = description..constant;
			var isDynamic:Boolean = description.@isDynamic;
			var variablesLength:int = variables.length();
			var accessorsLength:int = accessors.length();
			var constantsLength:int = constants.length();
			var childLength:int = 0;
			var keys:Object = {};
			var itemsArray:Array = [];
			if(isDynamic)
			{
				for(prop in object)
				{
					key = String(prop);
					if(!keys.hasOwnProperty(key))
					{
						keys[key] = key;
						itemName = key;
						itemType = parseType(getQualifiedClassName(object[key]));
						itemTarget = target + "." + key;
						itemAccess = MonsterDebuggerConstants.ACCESS_VARIABLE;
						itemPermission = MonsterDebuggerConstants.PERMISSION_READWRITE;
						itemIcon = MonsterDebuggerConstants.ICON_VARIABLE;
						itemsArray[itemsArray.length] = {
							"name":itemName,
							"type":itemType,
							"target":itemTarget,
							"access":itemAccess,
							"permission":itemPermission,
							"icon":itemIcon
						};
					}
				}
			}
			i = 0;
			while(i < variablesLength)
			{
				key = variables[i].@name;
				if(!keys.hasOwnProperty(key))
				{
					keys[key] = key;
					itemName = key;
					itemType = parseType(variables[i].@type);
					itemTarget = target + "." + key;
					itemAccess = MonsterDebuggerConstants.ACCESS_VARIABLE;
					itemPermission = MonsterDebuggerConstants.PERMISSION_READWRITE;
					itemIcon = MonsterDebuggerConstants.ICON_VARIABLE;
					itemsArray[itemsArray.length] = {
						"name":itemName,
						"type":itemType,
						"target":itemTarget,
						"access":itemAccess,
						"permission":itemPermission,
						"icon":itemIcon
					};
				}
				i++;
			}
			i = 0;
			while(i < accessorsLength)
			{
				key = accessors[i].@name;
				if(!keys.hasOwnProperty(key))
				{
					keys[key] = key;
					itemName = key;
					itemType = parseType(accessors[i].@type);
					itemTarget = target + "." + key;
					itemAccess = MonsterDebuggerConstants.ACCESS_ACCESSOR;
					itemPermission = MonsterDebuggerConstants.PERMISSION_READWRITE;
					itemIcon = MonsterDebuggerConstants.ICON_VARIABLE;
					if(accessors[i].@access == MonsterDebuggerConstants.PERMISSION_READONLY)
					{
						itemPermission = MonsterDebuggerConstants.PERMISSION_READONLY;
						itemIcon = MonsterDebuggerConstants.ICON_VARIABLE_READONLY;
					}
					if(accessors[i].@access == MonsterDebuggerConstants.PERMISSION_WRITEONLY)
					{
						itemPermission = MonsterDebuggerConstants.PERMISSION_WRITEONLY;
						itemIcon = MonsterDebuggerConstants.ICON_VARIABLE_WRITEONLY;
					}
					itemsArray[itemsArray.length] = {
						"name":itemName,
						"type":itemType,
						"target":itemTarget,
						"access":itemAccess,
						"permission":itemPermission,
						"icon":itemIcon
					};
				}
				i++;
			}
			i = 0;
			while(i < constantsLength)
			{
				key = constants[i].@name;
				if(!keys.hasOwnProperty(key))
				{
					keys[key] = key;
					itemName = key;
					itemType = parseType(constants[i].@type);
					itemTarget = target + "." + key;
					itemAccess = MonsterDebuggerConstants.ACCESS_CONSTANT;
					itemPermission = MonsterDebuggerConstants.PERMISSION_READONLY;
					itemIcon = MonsterDebuggerConstants.ICON_VARIABLE_READONLY;
					itemsArray[itemsArray.length] = {
						"name":itemName,
						"type":itemType,
						"target":itemTarget,
						"access":itemAccess,
						"permission":itemPermission,
						"icon":itemIcon
					};
				}
				i++;
			}
			itemsArray.sortOn("name",Array.CASEINSENSITIVE);
			if(includeDisplayObjects && object is DisplayObjectContainer)
			{
				displayObject = DisplayObjectContainer(object);
				displayObjects = [];
				childLength = displayObject.numChildren;
				i = 0;
				while(i < childLength)
				{
					child = null;
					try
					{
						child = displayObject.getChildAt(i);
					}
					catch(e1:Error)
					{
					}
					if(child != null)
					{
						itemXML = MonsterDebuggerDescribeType.get(child);
						itemType = parseType(itemXML.@name);
						itemName = "DisplayObject";
						if(child.name != null)
						{
							itemName = itemName + (" - " + child.name);
						}
						itemTarget = target + "." + "getChildAt(" + i + ")";
						itemAccess = MonsterDebuggerConstants.ACCESS_DISPLAY_OBJECT;
						itemPermission = MonsterDebuggerConstants.PERMISSION_READWRITE;
						itemIcon = child is DisplayObjectContainer?MonsterDebuggerConstants.ICON_ROOT:MonsterDebuggerConstants.ICON_DISPLAY_OBJECT;
						displayObjects[displayObjects.length] = {
							"name":itemName,
							"type":itemType,
							"target":itemTarget,
							"access":itemAccess,
							"permission":itemPermission,
							"icon":itemIcon,
							"index":i
						};
					}
					i++;
				}
				displayObjects.sortOn("name",Array.CASEINSENSITIVE);
				itemsArray = displayObjects.concat(itemsArray);
			}
			itemsArrayLength = itemsArray.length;
			i = 0;
			while(i < itemsArrayLength)
			{
				itemType = itemsArray[i].type;
				itemName = itemsArray[i].name;
				itemTarget = itemsArray[i].target;
				itemPermission = itemsArray[i].permission;
				itemAccess = itemsArray[i].access;
				itemIcon = itemsArray[i].icon;
				if(itemPermission != MonsterDebuggerConstants.PERMISSION_WRITEONLY)
				{
					try
					{
						if(itemAccess == MonsterDebuggerConstants.ACCESS_DISPLAY_OBJECT)
						{
							item = DisplayObjectContainer(object).getChildAt(itemsArray[i].index);
						}
						else
						{
							item = object[itemName];
						}
					}
					catch(e2:Error)
					{
						item = null;
					}
					if(itemType == MonsterDebuggerConstants.TYPE_STRING || itemType == MonsterDebuggerConstants.TYPE_BOOLEAN || itemType == MonsterDebuggerConstants.TYPE_NUMBER || itemType == MonsterDebuggerConstants.TYPE_INT || itemType == MonsterDebuggerConstants.TYPE_UINT || itemType == MonsterDebuggerConstants.TYPE_FUNCTION)
					{
						nodeXML = <node/>;
						nodeXML.@icon = itemIcon;
						nodeXML.@label = itemName + " (" + itemType + ") = " + printValue(item,itemType,true);
						nodeXML.@name = itemName;
						nodeXML.@type = itemType;
						nodeXML.@value = printValue(item,itemType);
						nodeXML.@target = itemTarget;
						nodeXML.@access = itemAccess;
						nodeXML.@permission = itemPermission;
						rootXML.appendChild(nodeXML);
					}
					else
					{
						nodeXML = <node/>;
						nodeXML.@icon = itemIcon;
						nodeXML.@label = itemName + " (" + itemType + ")";
						nodeXML.@name = itemName;
						nodeXML.@type = itemType;
						nodeXML.@target = itemTarget;
						nodeXML.@access = itemAccess;
						nodeXML.@permission = itemPermission;
						if(item == null)
						{
							nodeXML.@icon = MonsterDebuggerConstants.ICON_WARNING;
							nodeXML.@label = nodeXML.@label + " = null";
						}
						nodeXML.appendChild(parse(item,itemTarget,currentDepth + 1,maxDepth,includeDisplayObjects).children());
						rootXML.appendChild(nodeXML);
					}
				}
				i++;
			}
			return rootXML;
		}
		
		private static function parseArray(param1:*, param2:String, param3:int = 1, param4:int = 5, param5:Boolean = true) : XML
		{
			var _loc7_:XML = null;
			var _loc13_:* = undefined;
			var _loc6_:XML = <root/>;
			var _loc8_:String = "";
			var _loc9_:String = "";
			var _loc10_:int = 0;
			var _loc11_:Array = [];
			var _loc12_:Boolean = true;
			for(_loc13_ in param1)
			{
				if(!(_loc13_ is int))
				{
					_loc12_ = false;
				}
				_loc11_.push(_loc13_);
			}
			if(_loc12_)
			{
				_loc11_.sort(Array.NUMERIC);
			}
			else
			{
				_loc11_.sort(Array.CASEINSENSITIVE);
			}
			_loc10_ = 0;
			while(_loc10_ < _loc11_.length)
			{
				_loc8_ = parseType(MonsterDebuggerDescribeType.get(param1[_loc11_[_loc10_]]).@name);
				_loc9_ = param2 + "." + String(_loc11_[_loc10_]);
				if(_loc8_ == MonsterDebuggerConstants.TYPE_STRING || _loc8_ == MonsterDebuggerConstants.TYPE_BOOLEAN || _loc8_ == MonsterDebuggerConstants.TYPE_NUMBER || _loc8_ == MonsterDebuggerConstants.TYPE_INT || _loc8_ == MonsterDebuggerConstants.TYPE_UINT || _loc8_ == MonsterDebuggerConstants.TYPE_FUNCTION)
				{
					_loc7_ = <node/>;
					_loc7_.@icon = MonsterDebuggerConstants.ICON_VARIABLE;
					_loc7_.@access = MonsterDebuggerConstants.ACCESS_VARIABLE;
					_loc7_.@permission = MonsterDebuggerConstants.PERMISSION_READWRITE;
					_loc7_.@label = "[" + _loc11_[_loc10_] + "] (" + _loc8_ + ") = " + printValue(param1[_loc11_[_loc10_]],_loc8_,true);
					_loc7_.@name = "[" + _loc11_[_loc10_] + "]";
					_loc7_.@type = _loc8_;
					_loc7_.@value = printValue(param1[_loc11_[_loc10_]],_loc8_);
					_loc7_.@target = _loc9_;
					_loc6_.appendChild(_loc7_);
				}
				else
				{
					_loc7_ = <node/>;
					_loc7_.@icon = MonsterDebuggerConstants.ICON_VARIABLE;
					_loc7_.@access = MonsterDebuggerConstants.ACCESS_VARIABLE;
					_loc7_.@permission = MonsterDebuggerConstants.PERMISSION_READWRITE;
					_loc7_.@label = "[" + _loc11_[_loc10_] + "] (" + _loc8_ + ")";
					_loc7_.@name = "[" + _loc11_[_loc10_] + "]";
					_loc7_.@type = _loc8_;
					_loc7_.@value = "";
					_loc7_.@target = _loc9_;
					if(param1[_loc11_[_loc10_]] == null)
					{
						_loc7_.@icon = MonsterDebuggerConstants.ICON_WARNING;
						_loc7_.@label = _loc7_.@label + " = null";
					}
					_loc7_.appendChild(parse(param1[_loc11_[_loc10_]],_loc9_,param3 + 1,param4,param5).children());
					_loc6_.appendChild(_loc7_);
				}
				_loc10_++;
			}
			return _loc6_;
		}
		
		public static function parseFunctions(param1:*, param2:String = "") : XML
		{
			var itemXML:XML = null;
			var key:String = null;
			var returnType:String = null;
			var parameters:XMLList = null;
			var parametersLength:int = 0;
			var args:Array = null;
			var argsString:String = null;
			var methodXML:XML = null;
			var parameterXML:XML = null;
			var object:* = param1;
			var target:String = param2;
			var rootXML:XML = <root/>;
			var description:XML = MonsterDebuggerDescribeType.get(object);
			var type:String = parseType(description.@name);
			var itemType:String = "";
			var itemName:String = "";
			var itemTarget:String = "";
			var keys:Object = {};
			var methods:XMLList = description..method;
			var methodsArr:Array = [];
			var methodsLength:int = methods.length();
			var optional:Boolean = false;
			var i:int = 0;
			var n:int = 0;
			itemXML = <node/>;
			itemXML.@icon = MonsterDebuggerConstants.ICON_DEFAULT;
			itemXML.@label = "(" + type + ")";
			itemXML.@target = target;
			i = 0;
			while(i < methodsLength)
			{
				key = methods[i].@name;
				try
				{
					if(!keys.hasOwnProperty(key))
					{
						keys[key] = key;
						methodsArr[methodsArr.length] = {
							"name":key,
							"xml":methods[i],
							"access":MonsterDebuggerConstants.ACCESS_METHOD
						};
					}
				}
				catch(e:Error)
				{
				}
				i++;
			}
			methodsArr.sortOn("name",Array.CASEINSENSITIVE);
			methodsLength = methodsArr.length;
			i = 0;
			while(i < methodsLength)
			{
				itemType = MonsterDebuggerConstants.TYPE_FUNCTION;
				itemName = methodsArr[i].xml.@name;
				itemTarget = target + MonsterDebuggerConstants.DELIMITER + itemName;
				returnType = parseType(methodsArr[i].xml.@returnType);
				parameters = methodsArr[i].xml..parameter;
				parametersLength = parameters.length();
				args = [];
				argsString = "";
				optional = false;
				n = 0;
				while(n < parametersLength)
				{
					if(parameters[n].@optional == "true" && !optional)
					{
						optional = true;
						args[args.length] = "[";
					}
					args[args.length] = parseType(parameters[n].@type);
					n++;
				}
				if(optional)
				{
					args[args.length] = "]";
				}
				argsString = args.join(", ");
				argsString = argsString.replace("[, ","[");
				argsString = argsString.replace(", ]","]");
				methodXML = <node/>;
				methodXML.@icon = MonsterDebuggerConstants.ICON_FUNCTION;
				methodXML.@type = MonsterDebuggerConstants.TYPE_FUNCTION;
				methodXML.@access = MonsterDebuggerConstants.ACCESS_METHOD;
				methodXML.@label = itemName + "(" + argsString + "):" + returnType;
				methodXML.@name = itemName;
				methodXML.@target = itemTarget;
				methodXML.@args = argsString;
				methodXML.@returnType = returnType;
				n = 0;
				while(n < parametersLength)
				{
					parameterXML = <node/>;
					parameterXML.@type = parseType(parameters[n].@type);
					parameterXML.@index = parameters[n].@index;
					parameterXML.@optional = parameters[n].@optional;
					methodXML.appendChild(parameterXML);
					n++;
				}
				itemXML.appendChild(methodXML);
				i++;
			}
			rootXML.appendChild(itemXML);
			return rootXML;
		}
		
		public static function parseXMLList(param1:*, param2:String = "", param3:int = 1, param4:int = -1) : XML
		{
			var _loc5_:XML = <root/>;
			if(param4 != -1 && param3 > param4)
			{
				return _loc5_;
			}
			var _loc6_:int = 0;
			while(_loc6_ < param1.length())
			{
				_loc5_.appendChild(parseXML(param1[_loc6_],param2 + "." + String(_loc6_) + ".children()",param3,param4).children());
				_loc6_++;
			}
			return _loc5_;
		}
		
		public static function parseXML(param1:*, param2:String = "", param3:int = 1, param4:int = -1) : XML
		{
			var _loc6_:XML = null;
			var _loc7_:XML = null;
			var _loc9_:String = null;
			var _loc5_:XML = <root/>;
			var _loc8_:int = 0;
			if(param4 != -1 && param3 > param4)
			{
				return _loc5_;
			}
			if(param2.indexOf("@") != -1)
			{
				_loc6_ = <node/>;
				_loc6_.@icon = MonsterDebuggerConstants.ICON_XMLATTRIBUTE;
				_loc6_.@type = MonsterDebuggerConstants.TYPE_XMLATTRIBUTE;
				_loc6_.@access = MonsterDebuggerConstants.ACCESS_VARIABLE;
				_loc6_.@permission = MonsterDebuggerConstants.PERMISSION_READWRITE;
				_loc6_.@label = param1;
				_loc6_.@name = "";
				_loc6_.@value = param1;
				_loc6_.@target = param2;
				_loc5_.appendChild(_loc6_);
			}
			else if("name" in param1 && param1.name() == null)
			{
				_loc6_ = <node/>;
				_loc6_.@icon = MonsterDebuggerConstants.ICON_XMLVALUE;
				_loc6_.@type = MonsterDebuggerConstants.TYPE_XMLVALUE;
				_loc6_.@access = MonsterDebuggerConstants.ACCESS_VARIABLE;
				_loc6_.@permission = MonsterDebuggerConstants.PERMISSION_READWRITE;
				_loc6_.@label = "(" + MonsterDebuggerConstants.TYPE_XMLVALUE + ") = " + printValue(param1,MonsterDebuggerConstants.TYPE_XMLVALUE,true);
				_loc6_.@name = "";
				_loc6_.@value = printValue(param1,MonsterDebuggerConstants.TYPE_XMLVALUE);
				_loc6_.@target = param2;
				_loc5_.appendChild(_loc6_);
			}
			else if("hasSimpleContent" in param1 && param1.hasSimpleContent())
			{
				_loc6_ = <node/>;
				_loc6_.@icon = MonsterDebuggerConstants.ICON_XMLNODE;
				_loc6_.@type = MonsterDebuggerConstants.TYPE_XMLNODE;
				_loc6_.@access = MonsterDebuggerConstants.ACCESS_VARIABLE;
				_loc6_.@permission = MonsterDebuggerConstants.PERMISSION_READWRITE;
				_loc6_.@label = param1.name() + " (" + MonsterDebuggerConstants.TYPE_XMLNODE + ")";
				_loc6_.@name = param1.name();
				_loc6_.@value = "";
				_loc6_.@target = param2;
				if(param1 != "")
				{
					_loc7_ = <node/>;
					_loc7_.@icon = MonsterDebuggerConstants.ICON_XMLVALUE;
					_loc7_.@type = MonsterDebuggerConstants.TYPE_XMLVALUE;
					_loc7_.@access = MonsterDebuggerConstants.ACCESS_VARIABLE;
					_loc7_.@permission = MonsterDebuggerConstants.PERMISSION_READWRITE;
					_loc7_.@label = "(" + MonsterDebuggerConstants.TYPE_XMLVALUE + ") = " + printValue(param1,MonsterDebuggerConstants.TYPE_XMLVALUE);
					_loc7_.@name = "";
					_loc7_.@value = printValue(param1,MonsterDebuggerConstants.TYPE_XMLVALUE);
					_loc7_.@target = param2;
					_loc6_.appendChild(_loc7_);
				}
				_loc8_ = 0;
				while(_loc8_ < param1.attributes().length())
				{
					_loc7_ = <node/>;
					_loc7_.@icon = MonsterDebuggerConstants.ICON_XMLATTRIBUTE;
					_loc7_.@type = MonsterDebuggerConstants.TYPE_XMLATTRIBUTE;
					_loc7_.@access = MonsterDebuggerConstants.ACCESS_VARIABLE;
					_loc7_.@permission = MonsterDebuggerConstants.PERMISSION_READWRITE;
					_loc7_.@label = "@" + param1.attributes()[_loc8_].name() + " (" + MonsterDebuggerConstants.TYPE_XMLATTRIBUTE + ") = " + param1.attributes()[_loc8_];
					_loc7_.@name = "";
					_loc7_.@value = param1.attributes()[_loc8_];
					_loc7_.@target = param2 + "." + "@" + param1.attributes()[_loc8_].name();
					_loc6_.appendChild(_loc7_);
					_loc8_++;
				}
				_loc5_.appendChild(_loc6_);
			}
			else
			{
				_loc6_ = <node/>;
				_loc6_.@icon = MonsterDebuggerConstants.ICON_XMLNODE;
				_loc6_.@type = MonsterDebuggerConstants.TYPE_XMLNODE;
				_loc6_.@access = MonsterDebuggerConstants.ACCESS_VARIABLE;
				_loc6_.@permission = MonsterDebuggerConstants.PERMISSION_READWRITE;
				_loc6_.@label = param1.name() + " (" + MonsterDebuggerConstants.TYPE_XMLNODE + ")";
				_loc6_.@name = param1.name();
				_loc6_.@value = "";
				_loc6_.@target = param2;
				_loc8_ = 0;
				while(_loc8_ < param1.attributes().length())
				{
					_loc7_ = <node/>;
					_loc7_.@icon = MonsterDebuggerConstants.ICON_XMLATTRIBUTE;
					_loc7_.@type = MonsterDebuggerConstants.TYPE_XMLATTRIBUTE;
					_loc7_.@access = MonsterDebuggerConstants.ACCESS_VARIABLE;
					_loc7_.@permission = MonsterDebuggerConstants.PERMISSION_READWRITE;
					_loc7_.@label = "@" + param1.attributes()[_loc8_].name() + " (" + MonsterDebuggerConstants.TYPE_XMLATTRIBUTE + ") = " + param1.attributes()[_loc8_];
					_loc7_.@name = "";
					_loc7_.@value = param1.attributes()[_loc8_];
					_loc7_.@target = param2 + "." + "@" + param1.attributes()[_loc8_].name();
					_loc6_.appendChild(_loc7_);
					_loc8_++;
				}
				_loc8_ = 0;
				while(_loc8_ < param1.children().length())
				{
					_loc9_ = param2 + "." + "children()" + "." + _loc8_;
					_loc6_.appendChild(parseXML(param1.children()[_loc8_],_loc9_,param3 + 1,param4).children());
					_loc8_++;
				}
				_loc5_.appendChild(_loc6_);
			}
			return _loc5_;
		}
		
		public static function resume() : Boolean
		{
			try
			{
				System.resume();
				return true;
			}
			catch(e:Error)
			{
			}
			return false;
		}
		
		public static function getObjectUnderPoint(param1:DisplayObjectContainer, param2:Point) : DisplayObject
		{
			var _loc3_:Array = null;
			var _loc4_:DisplayObject = null;
			var _loc6_:DisplayObject = null;
			if(param1.areInaccessibleObjectsUnderPoint(param2))
			{
				return param1;
			}
			_loc3_ = param1.getObjectsUnderPoint(param2);
			_loc3_.reverse();
			if(_loc3_ == null || _loc3_.length == 0)
			{
				return param1;
			}
			_loc4_ = _loc3_[0];
			_loc3_.length = 0;
			while(true)
			{
				_loc3_[_loc3_.length] = _loc4_;
				if(_loc4_.parent == null)
				{
					break;
				}
				_loc4_ = _loc4_.parent;
			}
			_loc3_.reverse();
			var _loc5_:int = 0;
			while(_loc5_ < _loc3_.length)
			{
				_loc6_ = _loc3_[_loc5_];
				if(_loc6_ is DisplayObjectContainer)
				{
					_loc4_ = _loc6_;
					if(!DisplayObjectContainer(_loc6_).mouseChildren)
					{
						break;
					}
					_loc5_++;
					continue;
				}
				break;
			}
			return _loc4_;
		}
		
		public static function getReferenceID(param1:*) : String
		{
			if(param1 in _references)
			{
				return _references[param1];
			}
			var _loc2_:String = "#" + String(_reference);
			_references[param1] = _loc2_;
			_reference++;
			return _loc2_;
		}
		
		public static function printValue(param1:*, param2:String, param3:Boolean = false) : String
		{
			if(param2 == MonsterDebuggerConstants.TYPE_BYTEARRAY)
			{
				return param1["length"] + " bytes";
			}
			if(param1 == null)
			{
				return "null";
			}
			var _loc4_:* = String(param1);
			if(param3 && _loc4_.length > 140)
			{
				_loc4_ = _loc4_.substr(0,140) + "...";
			}
			return _loc4_;
		}
		
		private static function parseObject(param1:*, param2:String, param3:int = 1, param4:int = 5, param5:Boolean = true) : XML
		{
			var _loc8_:XML = null;
			var _loc14_:* = undefined;
			var _loc6_:XML = <root/>;
			var _loc7_:XML = <node/>;
			var _loc9_:String = "";
			var _loc10_:String = "";
			var _loc11_:int = 0;
			var _loc12_:Array = [];
			var _loc13_:Boolean = true;
			for(_loc14_ in param1)
			{
				if(!(_loc14_ is int))
				{
					_loc13_ = false;
				}
				_loc12_.push(_loc14_);
			}
			if(_loc13_)
			{
				_loc12_.sort(Array.NUMERIC);
			}
			else
			{
				_loc12_.sort(Array.CASEINSENSITIVE);
			}
			_loc11_ = 0;
			while(_loc11_ < _loc12_.length)
			{
				_loc9_ = parseType(MonsterDebuggerDescribeType.get(param1[_loc12_[_loc11_]]).@name);
				_loc10_ = param2 + "." + _loc12_[_loc11_];
				if(_loc9_ == MonsterDebuggerConstants.TYPE_STRING || _loc9_ == MonsterDebuggerConstants.TYPE_BOOLEAN || _loc9_ == MonsterDebuggerConstants.TYPE_NUMBER || _loc9_ == MonsterDebuggerConstants.TYPE_INT || _loc9_ == MonsterDebuggerConstants.TYPE_UINT || _loc9_ == MonsterDebuggerConstants.TYPE_FUNCTION)
				{
					_loc8_ = <node/>;
					_loc8_.@icon = MonsterDebuggerConstants.ICON_VARIABLE;
					_loc8_.@access = MonsterDebuggerConstants.ACCESS_VARIABLE;
					_loc8_.@permission = MonsterDebuggerConstants.PERMISSION_READWRITE;
					_loc8_.@label = _loc12_[_loc11_] + " (" + _loc9_ + ") = " + printValue(param1[_loc12_[_loc11_]],_loc9_,true);
					_loc8_.@name = _loc12_[_loc11_];
					_loc8_.@type = _loc9_;
					_loc8_.@value = printValue(param1[_loc12_[_loc11_]],_loc9_);
					_loc8_.@target = _loc10_;
					_loc7_.appendChild(_loc8_);
				}
				else
				{
					_loc8_ = <node/>;
					_loc8_.@icon = MonsterDebuggerConstants.ICON_VARIABLE;
					_loc8_.@access = MonsterDebuggerConstants.ACCESS_VARIABLE;
					_loc8_.@permission = MonsterDebuggerConstants.PERMISSION_READWRITE;
					_loc8_.@label = _loc12_[_loc11_] + " (" + _loc9_ + ")";
					_loc8_.@name = _loc12_[_loc11_];
					_loc8_.@type = _loc9_;
					_loc8_.@value = "";
					_loc8_.@target = _loc10_;
					if(param1[_loc12_[_loc11_]] == null)
					{
						_loc8_.@icon = MonsterDebuggerConstants.ICON_WARNING;
						_loc8_.@label = _loc8_.@label + " = null";
					}
					_loc8_.appendChild(parse(param1[_loc12_[_loc11_]],_loc10_,param3 + 1,param4,param5).children());
					_loc7_.appendChild(_loc8_);
				}
				_loc11_++;
			}
			_loc6_.appendChild(_loc7_.children());
			return _loc6_;
		}
		
		public static function parse(param1:*, param2:String = "", param3:int = 1, param4:int = 5, param5:Boolean = true) : XML
		{
			var _loc14_:XML = null;
			var _loc6_:XML = <root/>;
			var _loc7_:XML = <node/>;
			var _loc8_:XML = new XML();
			var _loc9_:String = "";
			var _loc10_:String = "";
			var _loc11_:Boolean = false;
			var _loc12_:* = null;
			var _loc13_:String = MonsterDebuggerConstants.ICON_ROOT;
			if(param4 != -1 && param3 > param4)
			{
				return _loc6_;
			}
			if(param1 == null)
			{
				_loc9_ = "null";
				_loc12_ = "null";
				_loc13_ = MonsterDebuggerConstants.ICON_WARNING;
			}
			else
			{
				_loc8_ = MonsterDebuggerDescribeType.get(param1);
				_loc9_ = parseType(_loc8_.@name);
				_loc10_ = parseType(_loc8_.@base);
				_loc11_ = _loc8_.@isDynamic;
				if(param1 is Class)
				{
					_loc12_ = "Class = " + _loc9_;
					_loc9_ = "Class";
					_loc7_.appendChild(parseClass(param1,param2,_loc8_,param3,param4,param5).children());
				}
				else if(_loc9_ == MonsterDebuggerConstants.TYPE_XML)
				{
					_loc7_.appendChild(parseXML(param1,param2 + ".children()",param3,param4).children());
				}
				else if(_loc9_ == MonsterDebuggerConstants.TYPE_XMLLIST)
				{
					_loc12_ = _loc9_ + " [" + String(param1.length()) + "]";
					_loc7_.appendChild(parseXMLList(param1,param2,param3,param4).children());
				}
				else if(_loc9_ == MonsterDebuggerConstants.TYPE_ARRAY || _loc9_.indexOf(MonsterDebuggerConstants.TYPE_VECTOR) == 0)
				{
					_loc12_ = _loc9_ + " [" + String(param1["length"]) + "]";
					_loc7_.appendChild(parseArray(param1,param2,param3,param4).children());
				}
				else if(_loc9_ == MonsterDebuggerConstants.TYPE_STRING || _loc9_ == MonsterDebuggerConstants.TYPE_BOOLEAN || _loc9_ == MonsterDebuggerConstants.TYPE_NUMBER || _loc9_ == MonsterDebuggerConstants.TYPE_INT || _loc9_ == MonsterDebuggerConstants.TYPE_UINT)
				{
					_loc7_.appendChild(parseBasics(param1,param2,_loc9_).children());
				}
				else if(_loc9_ == MonsterDebuggerConstants.TYPE_OBJECT)
				{
					_loc7_.appendChild(parseObject(param1,param2,param3,param4,param5).children());
				}
				else
				{
					_loc7_.appendChild(parseClass(param1,param2,_loc8_,param3,param4,param5).children());
				}
			}
			if(param3 == 1)
			{
				_loc14_ = <node/>;
				_loc14_.@icon = _loc13_;
				_loc14_.@label = _loc9_;
				_loc14_.@type = _loc9_;
				_loc14_.@target = param2;
				if(_loc12_ != null)
				{
					_loc14_.@label = _loc12_;
				}
				_loc14_.appendChild(_loc7_.children());
				_loc6_.appendChild(_loc14_);
			}
			else
			{
				_loc6_.appendChild(_loc7_.children());
			}
			return _loc6_;
		}
		
		public static function parseType(param1:String) : String
		{
			var _loc2_:String = null;
			var _loc3_:String = null;
			if(param1.indexOf("::") != -1)
			{
				param1 = param1.substring(param1.indexOf("::") + 2,param1.length);
			}
			if(param1.indexOf("::") != -1)
			{
				_loc2_ = param1.substring(0,param1.indexOf("<") + 1);
				_loc3_ = param1.substring(param1.indexOf("::") + 2,param1.length);
				param1 = _loc2_ + _loc3_;
			}
			param1 = param1.replace("()","");
			param1 = param1.replace(MonsterDebuggerConstants.TYPE_METHOD,MonsterDebuggerConstants.TYPE_FUNCTION);
			return param1;
		}
		
		public static function getReference(param1:String) : *
		{
			var _loc2_:* = undefined;
			var _loc3_:String = null;
			if(param1.charAt(0) != "#")
			{
				return null;
			}
			for(_loc2_ in _references)
			{
				_loc3_ = _references[_loc2_];
				if(_loc3_ == param1)
				{
					return _loc2_;
				}
			}
			return null;
		}
		
		public static function pause() : Boolean
		{
			try
			{
				System.pause();
				return true;
			}
			catch(e:Error)
			{
			}
			return false;
		}
		
		public static function getMemory() : uint
		{
			return System.totalMemory;
		}
		
		public static function getObject(param1:*, param2:String = "", param3:int = 0) : *
		{
			var index:Number = NaN;
			var base:* = param1;
			var target:String = param2;
			var parent:int = param3;
			if(target == null || target == "")
			{
				return base;
			}
			if(target.charAt(0) == "#")
			{
				return getReference(target);
			}
			var object:* = base;
			var splitted:Array = target.split(MonsterDebuggerConstants.DELIMITER);
			var i:int = 0;
			while(i < splitted.length - parent)
			{
				if(splitted[i] != "")
				{
					try
					{
						if(splitted[i] == "children()")
						{
							object = object.children();
						}
						else if(object is DisplayObjectContainer && splitted[i].indexOf("getChildAt(") == 0)
						{
							index = splitted[i].substring(11,splitted[i].indexOf(")",11));
							object = DisplayObjectContainer(object).getChildAt(index);
						}
						else
						{
							object = object[splitted[i]];
						}
					}
					catch(e:Error)
					{
						break;
					}
				}
				i++;
			}
			return object;
		}
		
		public static function stackTrace() : XML
		{
			var childXML:XML = null;
			var stack:String = null;
			var lines:Array = null;
			var i:int = 0;
			var s:String = null;
			var bracketIndex:int = 0;
			var methodIndex:int = 0;
			var classname:String = null;
			var method:String = null;
			var file:String = null;
			var line:String = null;
			var functionXML:XML = null;
			var rootXML:XML = <root/>;
			childXML = <node/>;
			try
			{
				throw new Error();
			}
			catch(e:Error)
			{
				stack = e.getStackTrace();
				if(stack == null || stack == "")
				{
					return <root><error>Stack unavailable</error></root>;
				}
				stack = stack.split("\t").join("");
				lines = stack.split("\n");
				if(lines.length <= 4)
				{
					return <root><error>Stack to short</error></root>;
				}
				lines.shift();
				lines.shift();
				lines.shift();
				lines.shift();
				i = 0;
				while(i < lines.length)
				{
					s = lines[i];
					s = s.substring(3,s.length);
					bracketIndex = s.indexOf("[");
					methodIndex = s.indexOf("/");
					if(bracketIndex == -1)
					{
						bracketIndex = s.length;
					}
					if(methodIndex == -1)
					{
						methodIndex = bracketIndex;
					}
					classname = MonsterDebuggerUtils.parseType(s.substring(0,methodIndex));
					method = "";
					file = "";
					line = "";
					if(methodIndex != s.length && methodIndex != bracketIndex)
					{
						method = s.substring(methodIndex + 1,bracketIndex);
					}
					if(bracketIndex != s.length)
					{
						file = s.substring(bracketIndex + 1,s.lastIndexOf(":"));
						line = s.substring(s.lastIndexOf(":") + 1,s.length - 1);
					}
					functionXML = <node/>;
					functionXML.@classname = classname;
					functionXML.@method = method;
					functionXML.@file = file;
					functionXML.@line = line;
					childXML.appendChild(functionXML);
					i++;
				}
			}
			rootXML.appendChild(childXML.children());
			return rootXML;
		}
		
		public static function isDisplayObject(param1:*) : Boolean
		{
			return param1 is DisplayObject || param1 is DisplayObjectContainer;
		}
		
		private static function parseBasics(param1:*, param2:String, param3:String) : XML
		{
			var _loc4_:XML = <root/>;
			var _loc5_:XML = <node/>;
			_loc5_.@icon = MonsterDebuggerConstants.ICON_VARIABLE;
			_loc5_.@access = MonsterDebuggerConstants.ACCESS_VARIABLE;
			_loc5_.@permission = MonsterDebuggerConstants.PERMISSION_READWRITE;
			_loc5_.@label = param3 + " = " + printValue(param1,param3,true);
			_loc5_.@name = "";
			_loc5_.@type = param3;
			_loc5_.@value = printValue(param1,param3);
			_loc5_.@target = param2;
			_loc4_.appendChild(_loc5_);
			return _loc4_;
		}
	}
}
