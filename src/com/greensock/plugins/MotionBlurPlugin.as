package com.greensock.plugins
{
   import com.greensock.TweenLite;
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.DisplayObject;
   import flash.display.Sprite;
   import flash.filters.BlurFilter;
   import flash.geom.ColorTransform;
   import flash.geom.Matrix;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   import flash.utils.getDefinitionByName;
   
   public class MotionBlurPlugin extends TweenPlugin
   {
      
      public static const API:Number = 1;
      
      private static const _DEG2RAD:Number = Math.PI / 180;
      
      private static const _RAD2DEG:Number = 180 / Math.PI;
      
      private static const _point:Point = new Point(0,0);
      
      private static const _ct:ColorTransform = new ColorTransform();
      
      private static const _blankArray:Array = [];
      
      private static var _classInitted:Boolean;
      
      private static var _isFlex:Boolean;
       
      
      protected var _target:DisplayObject;
      
      protected var _time:Number;
      
      protected var _xCurrent:Number;
      
      protected var _yCurrent:Number;
      
      protected var _bd:BitmapData;
      
      protected var _bitmap:Bitmap;
      
      protected var _strength:Number = 0.05;
      
      protected var _tween:TweenLite;
      
      protected var _blur:BlurFilter;
      
      protected var _matrix:Matrix;
      
      protected var _sprite:Sprite;
      
      protected var _rect:Rectangle;
      
      protected var _angle:Number;
      
      protected var _alpha:Number;
      
      protected var _xRef:Number;
      
      protected var _yRef:Number;
      
      protected var _mask:DisplayObject;
      
      protected var _parentMask:DisplayObject;
      
      protected var _padding:int;
      
      protected var _bdCache:BitmapData;
      
      protected var _rectCache:Rectangle;
      
      protected var _cos:Number;
      
      protected var _sin:Number;
      
      protected var _smoothing:Boolean;
      
      protected var _xOffset:Number;
      
      protected var _yOffset:Number;
      
      protected var _cached:Boolean;
      
      protected var _fastMode:Boolean;
      
      public function MotionBlurPlugin()
      {

         super();
          this._blur = new BlurFilter(0,0,2);
          this._matrix = new Matrix();
         this.propName = "motionBlur";
         this.overwriteProps = ["motionBlur"];
         this.onComplete = this.disable;
         this.onDisable = this.onTweenDisable;
         this.priority = -2;
         this.activeDisable = true;
      }
      
      override public function onInitTween(target:Object, value:*, tween:TweenLite) : Boolean
      {
         var x:Number = NaN;
         var y:Number = NaN;
         if(!(target is DisplayObject))
         {
            throw new Error("motionBlur tweens only work for DisplayObjects.");
         }
         if(value == false)
         {
            this._strength = 0;
         }
         else if(typeof value == "object")
         {
            this._strength = (value.strength || 1) * 0.05;
            this._blur.quality = int(int(value.quality)) || int(2);
            this._fastMode = Boolean(value.fastMode == true);
         }
         if(!_classInitted)
         {
            try
            {
               _isFlex = Boolean(getDefinitionByName("mx.managers.SystemManager"));
            }
            catch(e:Error)
            {
               _isFlex = false;
            }
            _classInitted = true;
         }
         this._target = target as DisplayObject;
         this._tween = tween;
         this._time = 0;
         this._padding = "padding" in value?int(int(value.padding)):int(10);
         this._smoothing = Boolean(this._blur.quality > 1);
         this._xCurrent = this._xRef = this._target.x;
         this._yCurrent = this._yRef = this._target.y;
         this._alpha = this._target.alpha;
         if("x" in this._tween.propTweenLookup && "y" in this._tween.propTweenLookup && !this._tween.propTweenLookup.x.isPlugin && !this._tween.propTweenLookup.y.isPlugin)
         {
            this._angle = Math.PI - Math.atan2(this._tween.propTweenLookup.y.change,this._tween.propTweenLookup.x.change);
         }
         else if(this._tween.vars.x != null || this._tween.vars.y != null)
         {
            x = Number(this._tween.vars.x) || Number(this._target.x);
            y = Number(this._tween.vars.y) || Number(this._target.y);
            this._angle = Math.PI - Math.atan2(y - this._target.y,x - this._target.x);
         }
         else
         {
            this._angle = 0;
         }
         this._cos = Math.cos(this._angle);
         this._sin = Math.sin(this._angle);
         this._bd = new BitmapData(this._target.width + this._padding * 2,this._target.height + this._padding * 2,true,16777215);
         this._bdCache = this._bd.clone();
         this._bitmap = new Bitmap(this._bd);
         this._bitmap.smoothing = this._smoothing;
         this._sprite = !!_isFlex?new getDefinitionByName("mx.core.UIComponent")():new Sprite();
         this._sprite.mouseEnabled = this._sprite.mouseChildren = false;
         this._sprite.addChild(this._bitmap);
         this._rectCache = new Rectangle(0,0,this._bd.width,this._bd.height);
         this._rect = this._rectCache.clone();
         if(this._target.mask)
         {
            this._mask = this._target.mask;
         }
         if(this._target.parent && this._target.parent.mask)
         {
            this._parentMask = this._target.parent.mask;
         }
         return true;
      }
      
      private function disable() : void
      {
         if(this._strength != 0)
         {
            this._target.alpha = this._alpha;
         }
         if(this._sprite.parent)
         {
            if(_isFlex && this._sprite.parent.hasOwnProperty("removeElement"))
            {
               (this._sprite.parent as Object).removeElement(this._sprite);
            }
            else
            {
               this._sprite.parent.removeChild(this._sprite);
            }
         }
         if(this._mask)
         {
            this._target.mask = this._mask;
         }
      }
      
      private function onTweenDisable() : void
      {
         if(this._tween.cachedTime != this._tween.cachedDuration && this._tween.cachedTime != 0)
         {
            this.disable();
         }
      }
      
      override public function set changeFactor(n:Number) : void
      {
         var width:Number = NaN;
         var height:Number = NaN;
         var val:Number = NaN;
         var parentFilters:Array = null;
         var prevVisible:Boolean = false;
         var bounds:Rectangle = null;
         var time:Number = this._tween.cachedTime - this._time;
         if(time < 0)
         {
            time = -time;
         }
         if(time < 1.0e-7)
         {
            return;
         }
         var dx:Number = this._target.x - this._xCurrent;
         var dy:Number = this._target.y - this._yCurrent;
         var rx:Number = this._target.x - this._xRef;
         var ry:Number = this._target.y - this._yRef;
         _changeFactor = n;
         if(rx > 2 || ry > 2 || rx < -2 || ry < -2)
         {
            this._angle = Math.PI - Math.atan2(ry,rx);
            this._cos = Math.cos(this._angle);
            this._sin = Math.sin(this._angle);
            this._xRef = this._target.x;
            this._yRef = this._target.y;
         }
         this._blur.blurX = Math.sqrt(dx * dx + dy * dy) * this._strength / time;
         this._xCurrent = this._target.x;
         this._yCurrent = this._target.y;
         this._time = this._tween.cachedTime;
         if(n == 0 || this._target.parent == null)
         {
            this.disable();
            return;
         }
         if(this._sprite.parent != this._target.parent && this._target.parent)
         {
            if(_isFlex && this._target.parent.hasOwnProperty("addElement"))
            {
               (this._target.parent as Object).addElementAt(this._sprite,(this._target.parent as Object).getElementIndex(this._target));
            }
            else
            {
               this._target.parent.addChildAt(this._sprite,this._target.parent.getChildIndex(this._target));
            }
            if(this._mask)
            {
               this._sprite.mask = this._mask;
            }
         }
         if(!this._fastMode || !this._cached)
         {
            parentFilters = this._target.parent.filters;
            if(parentFilters.length != 0)
            {
               this._target.parent.filters = _blankArray;
            }
            this._target.x = this._target.y = 20000;
            prevVisible = this._target.visible;
            this._target.visible = true;
            bounds = this._target.getBounds(this._target.parent);
            if(bounds.width + this._blur.blurX * 2 > 2870)
            {
               this._blur.blurX = bounds.width >= 2870?Number(0):Number((2870 - bounds.width) * 0.5);
            }
            this._xOffset = 20000 - bounds.x + this._padding;
            this._yOffset = 20000 - bounds.y + this._padding;
            bounds.width = bounds.width + this._padding * 2;
            bounds.height = bounds.height + this._padding * 2;
            if(bounds.height > this._bdCache.height || bounds.width > this._bdCache.width)
            {
               this._bdCache = new BitmapData(bounds.width,bounds.height,true,16777215);
               this._rectCache = new Rectangle(0,0,this._bdCache.width,this._bdCache.height);
            }
            this._matrix.tx = this._padding - bounds.x;
            this._matrix.ty = this._padding - bounds.y;
            this._matrix.a = this._matrix.d = 1;
            this._matrix.b = this._matrix.c = 0;
            bounds.x = bounds.y = 0;
            if(this._target.alpha == 0.00390625)
            {
               this._target.alpha = this._alpha;
            }
            else
            {
               this._alpha = this._target.alpha;
            }
            if(this._parentMask)
            {
               this._target.parent.mask = null;
            }
            this._bdCache.fillRect(this._rectCache,16777215);
            this._bdCache.draw(this._target.parent,this._matrix,_ct,"normal",bounds,this._smoothing);
            if(this._parentMask)
            {
               this._target.parent.mask = this._parentMask;
            }
            this._target.visible = prevVisible;
            this._target.x = this._xCurrent;
            this._target.y = this._yCurrent;
            if(parentFilters.length != 0)
            {
               this._target.parent.filters = parentFilters;
            }
            this._cached = true;
         }
         else if(this._target.alpha != 0.00390625)
         {
            this._alpha = this._target.alpha;
         }
         this._target.alpha = 0.00390625;
         this._matrix.tx = this._matrix.ty = 0;
         this._matrix.a = this._cos;
         this._matrix.b = this._sin;
         this._matrix.c = -this._sin;
         this._matrix.d = this._cos;
         if((width = this._matrix.a * this._bdCache.width) < 0)
         {
            this._matrix.tx = -width;
            width = -width;
         }
         if((val = this._matrix.c * this._bdCache.height) < 0)
         {
            this._matrix.tx = this._matrix.tx - val;
            width = width - val;
         }
         else
         {
            width = width + val;
         }
         if((height = this._matrix.d * this._bdCache.height) < 0)
         {
            this._matrix.ty = -height;
            height = -height;
         }
         if((val = this._matrix.b * this._bdCache.width) < 0)
         {
            this._matrix.ty = this._matrix.ty - val;
            height = height - val;
         }
         else
         {
            height = height + val;
         }
         width = width + this._blur.blurX * 2;
         this._matrix.tx = this._matrix.tx + this._blur.blurX;
         if(width > this._bd.width || height > this._bd.height)
         {
            this._bd = this._bitmap.bitmapData = new BitmapData(width,height,true,16777215);
            this._rect = new Rectangle(0,0,this._bd.width,this._bd.height);
            this._bitmap.smoothing = this._smoothing;
         }
         this._bd.fillRect(this._rect,16777215);
         this._bd.draw(this._bdCache,this._matrix,_ct,"normal",this._rect,this._smoothing);
         this._bd.applyFilter(this._bd,this._rect,_point,this._blur);
         this._bitmap.x = 0 - (this._matrix.a * this._xOffset + this._matrix.c * this._yOffset + this._matrix.tx);
         this._bitmap.y = 0 - (this._matrix.d * this._yOffset + this._matrix.b * this._xOffset + this._matrix.ty);
         this._matrix.b = -this._sin;
         this._matrix.c = this._sin;
         this._matrix.tx = this._xCurrent;
         this._matrix.ty = this._yCurrent;
         this._sprite.transform.matrix = this._matrix;
      }
   }
}
