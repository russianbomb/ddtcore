package com.greensock
{
   import flash.display.DisplayObject;
   import flash.display.Sprite;
   import flash.geom.Matrix;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   import flash.utils.Dictionary;
   import flash.utils.Proxy;
   import flash.utils.flash_proxy;
   
   public dynamic class TweenProxy extends Proxy
   {
      
      public static const VERSION:Number = 0.94;
      
      private static const _DEG2RAD:Number = Math.PI / 180;
      
      private static const _RAD2DEG:Number = 180 / Math.PI;
      
      private static var _dict:Dictionary = new Dictionary(false);
      
      private static var _addedProps:String = " tint tintPercent scale skewX skewY skewX2 skewY2 target registration registrationX registrationY localRegistration localRegistrationX localRegistrationY ";
       
      
      private var _target:DisplayObject;
      
      private var _angle:Number;
      
      private var _scaleX:Number;
      
      private var _scaleY:Number;
      
      private var _proxies:Array;
      
      private var _localRegistration:Point;
      
      private var _registration:Point;
      
      private var _regAt0:Boolean;
      
      public var ignoreSiblingUpdates:Boolean = false;
      
      public var isTweenProxy:Boolean = true;
      
      public function TweenProxy(target:DisplayObject, ignoreSiblingUpdates:Boolean = false)
      {
         super();
         this._target = target;
         if(_dict[this._target] == undefined)
         {
            _dict[this._target] = [];
         }
         this._proxies = _dict[this._target];
         this._proxies.push(this);
         this._localRegistration = new Point(0,0);
         this.ignoreSiblingUpdates = ignoreSiblingUpdates;
         this.calibrate();
      }
      
      public static function create(target:DisplayObject, allowRecycle:Boolean = true) : TweenProxy
      {
         if(_dict[target] != null && allowRecycle)
         {
            return _dict[target][0];
         }
         return new TweenProxy(target);
      }
      
      public function getCenter() : Point
      {
         var s:Sprite = null;
         var remove:Boolean = false;
         if(this._target.parent == null)
         {
            remove = true;
            s = new Sprite();
            s.addChild(this._target);
         }
         var b:Rectangle = this._target.getBounds(this._target.parent);
         var p:Point = new Point(b.x + b.width / 2,b.y + b.height / 2);
         if(remove)
         {
            this._target.parent.removeChild(this._target);
         }
         return p;
      }
      
      public function get target() : DisplayObject
      {
         return this._target;
      }
      
      public function calibrate() : void
      {
         this._scaleX = this._target.scaleX;
         this._scaleY = this._target.scaleY;
         this._angle = this._target.rotation * _DEG2RAD;
         this.calibrateRegistration();
      }
      
      public function destroy() : void
      {
         var i:int = 0;
         var a:Array = _dict[this._target];
         for(i = a.length - 1; i > -1; i--)
         {
            if(a[i] == this)
            {
               a.splice(i,1);
            }
         }
         if(a.length == 0)
         {
            delete _dict[this._target];
         }
         this._target = null;
         this._localRegistration = null;
         this._registration = null;
         this._proxies = null;
      }
      
      override flash_proxy function callProperty(name:*, ... args) : *
      {
         return this._target[name].apply(null,args);
      }
      
      override flash_proxy function getProperty(prop:*) : *
      {
         return this._target[prop];
      }
      
      override flash_proxy function setProperty(prop:*, value:*) : void
      {
         this._target[prop] = value;
      }
      
      override flash_proxy function hasProperty(name:*) : Boolean
      {
         if(this._target.hasOwnProperty(name))
         {
            return true;
         }
         if(_addedProps.indexOf(" " + name + " ") != -1)
         {
            return true;
         }
         return false;
      }
      
      public function moveRegX(n:Number) : void
      {
         this._registration.x = this._registration.x + n;
      }
      
      public function moveRegY(n:Number) : void
      {
         this._registration.y = this._registration.y + n;
      }
      
      private function reposition() : void
      {
         var p:Point = null;
         p = this._target.parent.globalToLocal(this._target.localToGlobal(this._localRegistration));
         this._target.x = this._target.x + (this._registration.x - p.x);
         this._target.y = this._target.y + (this._registration.y - p.y);
      }
      
      private function updateSiblingProxies() : void
      {
         for(var i:int = this._proxies.length - 1; i > -1; i--)
         {
            if(this._proxies[i] != this)
            {
               this._proxies[i].onSiblingUpdate(this._scaleX,this._scaleY,this._angle);
            }
         }
      }
      
      private function calibrateLocal() : void
      {
         this._localRegistration = this._target.globalToLocal(this._target.parent.localToGlobal(this._registration));
         this._regAt0 = this._localRegistration.x == 0 && this._localRegistration.y == 0;
      }
      
      private function calibrateRegistration() : void
      {
         this._registration = this._target.parent.globalToLocal(this._target.localToGlobal(this._localRegistration));
         this._regAt0 = this._localRegistration.x == 0 && this._localRegistration.y == 0;
      }
      
      public function onSiblingUpdate(scaleX:Number, scaleY:Number, angle:Number) : void
      {
         this._scaleX = scaleX;
         this._scaleY = scaleY;
         this._angle = angle;
         if(this.ignoreSiblingUpdates)
         {
            this.calibrateLocal();
         }
         else
         {
            this.calibrateRegistration();
         }
      }
      
      public function get localRegistration() : Point
      {
         return this._localRegistration;
      }
      
      public function set localRegistration(p:Point) : void
      {
         this._localRegistration = p;
         this.calibrateRegistration();
      }
      
      public function get localRegistrationX() : Number
      {
         return this._localRegistration.x;
      }
      
      public function set localRegistrationX(n:Number) : void
      {
         this._localRegistration.x = n;
         this.calibrateRegistration();
      }
      
      public function get localRegistrationY() : Number
      {
         return this._localRegistration.y;
      }
      
      public function set localRegistrationY(n:Number) : void
      {
         this._localRegistration.y = n;
         this.calibrateRegistration();
      }
      
      public function get registration() : Point
      {
         return this._registration;
      }
      
      public function set registration(p:Point) : void
      {
         this._registration = p;
         this.calibrateLocal();
      }
      
      public function get registrationX() : Number
      {
         return this._registration.x;
      }
      
      public function set registrationX(n:Number) : void
      {
         this._registration.x = n;
         this.calibrateLocal();
      }
      
      public function get registrationY() : Number
      {
         return this._registration.y;
      }
      
      public function set registrationY(n:Number) : void
      {
         this._registration.y = n;
         this.calibrateLocal();
      }
      
      public function get x() : Number
      {
         return this._registration.x;
      }
      
      public function set x(n:Number) : void
      {
         var tx:Number = n - this._registration.x;
         this._target.x = this._target.x + tx;
         for(var i:int = this._proxies.length - 1; i > -1; i--)
         {
            if(this._proxies[i] == this || !this._proxies[i].ignoreSiblingUpdates)
            {
               this._proxies[i].moveRegX(tx);
            }
         }
      }
      
      public function get y() : Number
      {
         return this._registration.y;
      }
      
      public function set y(n:Number) : void
      {
         var ty:Number = n - this._registration.y;
         this._target.y = this._target.y + ty;
         for(var i:int = this._proxies.length - 1; i > -1; i--)
         {
            if(this._proxies[i] == this || !this._proxies[i].ignoreSiblingUpdates)
            {
               this._proxies[i].moveRegY(ty);
            }
         }
      }
      
      public function get rotation() : Number
      {
         return this._angle * _RAD2DEG;
      }
      
      public function set rotation(n:Number) : void
      {
         var radians:Number = n * _DEG2RAD;
         var m:Matrix = this._target.transform.matrix;
         m.rotate(radians - this._angle);
         this._target.transform.matrix = m;
         this._angle = radians;
         this.reposition();
         if(this._proxies.length > 1)
         {
            this.updateSiblingProxies();
         }
      }
      
      public function get skewX() : Number
      {
         var m:Matrix = this._target.transform.matrix;
         return (Math.atan2(-m.c,m.d) - this._angle) * _RAD2DEG;
      }
      
      public function set skewX(n:Number) : void
      {
         var radians:Number = n * _DEG2RAD;
         var m:Matrix = this._target.transform.matrix;
         var sy:Number = this._scaleY < 0?Number(-this._scaleY):Number(this._scaleY);
         m.c = -sy * Math.sin(radians + this._angle);
         m.d = sy * Math.cos(radians + this._angle);
         this._target.transform.matrix = m;
         if(!this._regAt0)
         {
            this.reposition();
         }
         if(this._proxies.length > 1)
         {
            this.updateSiblingProxies();
         }
      }
      
      public function get skewY() : Number
      {
         var m:Matrix = this._target.transform.matrix;
         return (Math.atan2(m.b,m.a) - this._angle) * _RAD2DEG;
      }
      
      public function set skewY(n:Number) : void
      {
         var radians:Number = n * _DEG2RAD;
         var m:Matrix = this._target.transform.matrix;
         var sx:Number = this._scaleX < 0?Number(-this._scaleX):Number(this._scaleX);
         m.a = sx * Math.cos(radians + this._angle);
         m.b = sx * Math.sin(radians + this._angle);
         this._target.transform.matrix = m;
         if(!this._regAt0)
         {
            this.reposition();
         }
         if(this._proxies.length > 1)
         {
            this.updateSiblingProxies();
         }
      }
      
      public function get skewX2() : Number
      {
         return this.skewX2Radians * _RAD2DEG;
      }
      
      public function set skewX2(n:Number) : void
      {
         this.skewX2Radians = n * _DEG2RAD;
      }
      
      public function get skewY2() : Number
      {
         return this.skewY2Radians * _RAD2DEG;
      }
      
      public function set skewY2(n:Number) : void
      {
         this.skewY2Radians = n * _DEG2RAD;
      }
      
      public function get skewX2Radians() : Number
      {
         return -Math.atan(this._target.transform.matrix.c);
      }
      
      public function set skewX2Radians(n:Number) : void
      {
         var m:Matrix = this._target.transform.matrix;
         m.c = Math.tan(-n);
         this._target.transform.matrix = m;
         if(!this._regAt0)
         {
            this.reposition();
         }
         if(this._proxies.length > 1)
         {
            this.updateSiblingProxies();
         }
      }
      
      public function get skewY2Radians() : Number
      {
         return Math.atan(this._target.transform.matrix.b);
      }
      
      public function set skewY2Radians(n:Number) : void
      {
         var m:Matrix = this._target.transform.matrix;
         m.b = Math.tan(n);
         this._target.transform.matrix = m;
         if(!this._regAt0)
         {
            this.reposition();
         }
         if(this._proxies.length > 1)
         {
            this.updateSiblingProxies();
         }
      }
      
      public function get scaleX() : Number
      {
         return this._scaleX;
      }
      
      public function set scaleX(n:Number) : void
      {
         if(n == 0)
         {
            n = 0.0001;
         }
         var m:Matrix = this._target.transform.matrix;
         m.rotate(-this._angle);
         m.scale(n / this._scaleX,1);
         m.rotate(this._angle);
         this._target.transform.matrix = m;
         this._scaleX = n;
         this.reposition();
         if(this._proxies.length > 1)
         {
            this.updateSiblingProxies();
         }
      }
      
      public function get scaleY() : Number
      {
         return this._scaleY;
      }
      
      public function set scaleY(n:Number) : void
      {
         if(n == 0)
         {
            n = 0.0001;
         }
         var m:Matrix = this._target.transform.matrix;
         m.rotate(-this._angle);
         m.scale(1,n / this._scaleY);
         m.rotate(this._angle);
         this._target.transform.matrix = m;
         this._scaleY = n;
         this.reposition();
         if(this._proxies.length > 1)
         {
            this.updateSiblingProxies();
         }
      }
      
      public function get scale() : Number
      {
         return (this._scaleX + this._scaleY) / 2;
      }
      
      public function set scale(n:Number) : void
      {
         if(n == 0)
         {
            n = 0.0001;
         }
         var m:Matrix = this._target.transform.matrix;
         m.rotate(-this._angle);
         m.scale(n / this._scaleX,n / this._scaleY);
         m.rotate(this._angle);
         this._target.transform.matrix = m;
         this._scaleX = this._scaleY = n;
         this.reposition();
         if(this._proxies.length > 1)
         {
            this.updateSiblingProxies();
         }
      }
      
      public function get alpha() : Number
      {
         return this._target.alpha;
      }
      
      public function set alpha(n:Number) : void
      {
         this._target.alpha = n;
      }
      
      public function get width() : Number
      {
         return this._target.width;
      }
      
      public function set width(n:Number) : void
      {
         this._target.width = n;
         if(!this._regAt0)
         {
            this.reposition();
         }
         if(this._proxies.length > 1)
         {
            this.updateSiblingProxies();
         }
      }
      
      public function get height() : Number
      {
         return this._target.height;
      }
      
      public function set height(n:Number) : void
      {
         this._target.height = n;
         if(!this._regAt0)
         {
            this.reposition();
         }
         if(this._proxies.length > 1)
         {
            this.updateSiblingProxies();
         }
      }
   }
}
