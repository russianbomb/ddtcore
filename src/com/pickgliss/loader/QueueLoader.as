package com.pickgliss.loader
{
   import com.pickgliss.ui.core.Disposeable;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   
   [Event(name="change",type="flash.events.Event")]
   [Event(name="complete",type="flash.events.Event")]
   public class QueueLoader extends EventDispatcher implements Disposeable
   {
       
      
      private var _loaders:Vector.<BaseLoader>;
      
      public function QueueLoader()
      {
         super();
         this._loaders = new Vector.<BaseLoader>();
      }
      
      public function addLoader(loader:BaseLoader) : void
      {
         this._loaders.push(loader);
      }
      
      public function dispose() : void
      {
         this.removeEvent();
         this._loaders = null;
      }
      
      public function removeEvent() : void
      {
         for(var i:int = 0; i < this._loaders.length; i++)
         {
            this._loaders[i].removeEventListener(LoaderEvent.COMPLETE,this.__loadNext);
         }
      }
      
      public function isAllComplete() : Boolean
      {
         for(var i:int = 0; i < this._loaders.length; i++)
         {
            if(!this._loaders[i].isComplete)
            {
               return false;
            }
         }
         return true;
      }
      
      public function isLoading() : Boolean
      {
         for(var i:int = 0; i < this._loaders.length; i++)
         {
            if(this._loaders[i].isLoading)
            {
               return true;
            }
         }
         return false;
      }
      
      public function get completeCount() : int
      {
         var result:int = 0;
         for(var i:int = 0; i < this._loaders.length; i++)
         {
            if(this._loaders[i].isComplete)
            {
               result++;
            }
         }
         return result;
      }
      
      public function get length() : int
      {
         return this._loaders.length;
      }
      
      public function get loaders() : Vector.<BaseLoader>
      {
         return this._loaders;
      }
      
      public function start() : void
      {
         this.tryLoadNext();
      }
      
      private function __loadNext(event:LoaderEvent) : void
      {
         var loader:BaseLoader = event.loader as BaseLoader;
         loader.removeEventListener(LoaderEvent.COMPLETE,this.__loadNext);
         dispatchEvent(new Event(Event.CHANGE));
         this.tryLoadNext();
      }
      
      private function tryLoadNext() : void
      {
         if(this._loaders == null)
         {
            return;
         }
         var loaderCount:int = this._loaders.length;
         for(var i:int = 0; i < loaderCount; i++)
         {
            if(!this._loaders[i].isComplete)
            {
               this._loaders[i].addEventListener(LoaderEvent.COMPLETE,this.__loadNext);
               LoaderManager.Instance.startLoad(this._loaders[i]);
               return;
            }
         }
         dispatchEvent(new Event(Event.COMPLETE));
      }
   }
}
