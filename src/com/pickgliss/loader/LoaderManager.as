package com.pickgliss.loader
{
   import flash.events.EventDispatcher;
   import flash.external.ExternalInterface;
   import flash.net.URLVariables;
   import flash.system.ApplicationDomain;
   import flash.system.LoaderContext;
   import flash.utils.ByteArray;
   import flash.utils.Dictionary;
   import flash.utils.setTimeout;
   
   public class LoaderManager extends EventDispatcher
   {
      
      public static const ALLOW_MUTI_LOAD_COUNT:int = 8;
      
      public static const LOAD_FROM_LOCAL:int = 2;
      
      public static const LOAD_FROM_WEB:int = 1;
      
      public static const LOAD_NOT_SET:int = 0;
      
      private static var _instance:LoaderManager;
       
      
      private var _loadMode:int = 0;
      
      private var _loaderIdCounter:int = 0;
      
      private var _loaderSaveByID:Dictionary;
      
      private var _loaderSaveByPath:Dictionary;
      
      private var _loadingLoaderList:Vector.<BaseLoader>;
      
      private var _waitingLoaderList:Vector.<BaseLoader>;
      
      public function LoaderManager()
      {
         super();
         this._loaderSaveByID = new Dictionary();
         this._loaderSaveByPath = new Dictionary();
         this._loadingLoaderList = new Vector.<BaseLoader>();
         this._waitingLoaderList = new Vector.<BaseLoader>();
         this.initLoadMode();
      }
      
      public static function get Instance() : LoaderManager
      {
         if(_instance == null)
         {
            _instance = new LoaderManager();
         }
         return _instance;
      }
      
      public function creatLoaderByType(filePath:String, type:int, args:URLVariables, requestMethod:String, domain:ApplicationDomain) : BaseLoader
      {
         var loader:BaseLoader = null;
         switch(type)
         {
            case BaseLoader.BITMAP_LOADER:
               loader = new BitmapLoader(this.getNextLoaderID(),filePath);
               break;
            case BaseLoader.TEXT_LOADER:
               loader = new TextLoader(this.getNextLoaderID(),filePath,args);
               break;
            case BaseLoader.DISPLAY_LOADER:
               loader = new DisplayLoader(this.getNextLoaderID(),filePath);
               break;
            case BaseLoader.BYTE_LOADER:
               loader = new BaseLoader(this.getNextLoaderID(),filePath);
               break;
            case BaseLoader.COMPRESS_TEXT_LOADER:
               loader = new CompressTextLoader(this.getNextLoaderID(),filePath,args);
               break;
            case BaseLoader.MODULE_LOADER:
               loader = new ModuleLoader(this.getNextLoaderID(),filePath,domain);
               break;
            case BaseLoader.REQUEST_LOADER:
               loader = new RequestLoader(this.getNextLoaderID(),filePath,args,requestMethod);
               break;
            case BaseLoader.COMPRESS_REQUEST_LOADER:
               loader = new CompressRequestLoader(this.getNextLoaderID(),filePath,args,requestMethod);
         }
         return loader;
      }
      
      public function getLoadMode() : int
      {
         return this._loadMode;
      }
      
      public function creatLoader(filePath:String, type:int, args:URLVariables = null, requestMethod:String = "GET", domain:ApplicationDomain = null) : *
      {
         var loader:BaseLoader = null;
         filePath = filePath.toLowerCase();
         var fixedVariablesURL:String = this.fixedVariablesURL(filePath,type,args);
         loader = this.getLoaderByURL(fixedVariablesURL,args);
         if(loader == null)
         {
            loader = this.creatLoaderByType(fixedVariablesURL,type,args,requestMethod,domain);
         }
         else
         {
            loader.domain = domain;
         }
         if(type != BaseLoader.REQUEST_LOADER && type != BaseLoader.COMPRESS_REQUEST_LOADER && type != BaseLoader.BITMAP_LOADER)
         {
            this._loaderSaveByID[loader.id] = loader;
            this._loaderSaveByPath[loader.url] = loader;
         }
         return loader;
      }
      
      public function creatLoaderOriginal(filePath:String, type:int, args:URLVariables = null, requestMethod:String = "GET") : *
      {
         var loader:BaseLoader = null;
         var fixedVariablesURL:String = this.fixedVariablesURL(filePath,type,args);
         loader = this.getLoaderByURL(fixedVariablesURL,args);
         if(loader == null)
         {
            loader = this.creatLoaderByType(fixedVariablesURL,type,args,requestMethod,null);
         }
         if(type != BaseLoader.REQUEST_LOADER && type != BaseLoader.COMPRESS_REQUEST_LOADER && type != BaseLoader.BITMAP_LOADER)
         {
            this._loaderSaveByID[loader.id] = loader;
            this._loaderSaveByPath[loader.url] = loader;
         }
         return loader;
      }
      
      public function creatAndStartLoad(filePath:String, type:int, args:URLVariables = null) : BaseLoader
      {
         var loader:BaseLoader = this.creatLoader(filePath,type,args);
         this.startLoad(loader);
         return loader;
      }
      
      public function getLoaderByID(id:int) : BaseLoader
      {
         return this._loaderSaveByID[id];
      }
      
      public function getLoaderByURL(url:String, args:URLVariables) : BaseLoader
      {
         var loader:BaseLoader = this._loaderSaveByPath[url];
         return loader;
      }
      
      public function getNextLoaderID() : int
      {
         return this._loaderIdCounter++;
      }
      
      public function saveFileToLocal(loader:BaseLoader) : void
      {
      }
      
      public function startLoad(loader:BaseLoader, loadImp:Boolean = false) : void
      {
         if(loader)
         {
            loader.addEventListener(LoaderEvent.COMPLETE,this.__onLoadFinish);
         }
         if(loader.isComplete)
         {
            loader.dispatchEvent(new LoaderEvent(LoaderEvent.COMPLETE,loader));
            return;
         }
         var ba:ByteArray = LoaderSavingManager.loadCachedFile(loader.url,true);
         if(ba)
         {
            loader.loadFromBytes(ba);
            return;
         }
         if(!LoadResourceManager.Instance.isMicroClient && (this._loadingLoaderList.length >= ALLOW_MUTI_LOAD_COUNT && !loadImp || this.getLoadMode() == LOAD_NOT_SET))
         {
            if(this._waitingLoaderList.indexOf(loader) == -1)
            {
               this._waitingLoaderList.push(loader);
            }
         }
         else
         {
            if(this._loadingLoaderList.indexOf(loader) == -1)
            {
               this._loadingLoaderList.push(loader);
            }
            if(this.getLoadMode() == LOAD_FROM_WEB || loader.type == BaseLoader.TEXT_LOADER)
            {
               loader.loadFromWeb();
            }
            else if(this.getLoadMode() == LOAD_FROM_LOCAL)
            {
               loader.getFilePathFromExternal();
            }
         }
      }
      
      private function __onLoadFinish(event:LoaderEvent) : void
      {
         event.loader.removeEventListener(LoaderEvent.COMPLETE,this.__onLoadFinish);
         this._loadingLoaderList.splice(this._loadingLoaderList.indexOf(event.loader),1);
         this.tryLoadWaiting();
      }
      
      private function initLoadMode() : void
      {
         if(!ExternalInterface.available)
         {
            this.setFlashLoadWeb();
            return;
         }
         ExternalInterface.addCallback("SetFlashLoadExternal",this.setFlashLoadExternal);
         setTimeout(this.setFlashLoadWeb,200);
      }
      
      private function onExternalLoadStop(id:int, path:String) : void
      {
         var loader:BaseLoader = this.getLoaderByID(id);
         loader.loadFromExternal(path);
      }
      
      private function setFlashLoadExternal() : void
      {
         this._loadMode = LOAD_FROM_LOCAL;
         ExternalInterface.addCallback("ExternalLoadStop",this.onExternalLoadStop);
         this.tryLoadWaiting();
      }
      
      public function setFlashLoadWeb() : void
      {
         this._loadMode = LOAD_FROM_WEB;
         this.tryLoadWaiting();
      }
      
      private function tryLoadWaiting() : void
      {
         var loader:BaseLoader = null;
         for(var i:int = 0; i < this._waitingLoaderList.length; i++)
         {
            if(this._loadingLoaderList.length < ALLOW_MUTI_LOAD_COUNT)
            {
               loader = this._waitingLoaderList.shift();
               this.startLoad(loader);
            }
         }
      }
      
      public function setup(context:LoaderContext, textLoaderKey:String) : void
      {
         DisplayLoader.Context = context;
         TextLoader.TextLoaderKey = textLoaderKey;
         LoaderSavingManager.setup();
      }
      
      public function fixedVariablesURL(path:String, type:int, variables:URLVariables) : String
      {
         var variableString:String = null;
         var i:int = 0;
         var p:* = null;
         if(type != BaseLoader.REQUEST_LOADER && type != BaseLoader.COMPRESS_REQUEST_LOADER)
         {
            variableString = "";
            if(variables == null)
            {
               variables = new URLVariables();
            }
            if(type == BaseLoader.BYTE_LOADER || type == BaseLoader.DISPLAY_LOADER || type == BaseLoader.BITMAP_LOADER)
            {
               if(!variables["lv"])
               {
                  variables["lv"] = LoaderSavingManager.Version;
               }
            }
            else if(type == BaseLoader.COMPRESS_TEXT_LOADER || type == BaseLoader.TEXT_LOADER)
            {
               if(!variables["rnd"])
               {
                  variables["rnd"] = TextLoader.TextLoaderKey;
               }
            }
            else if(type == BaseLoader.MODULE_LOADER)
            {
               if(!variables["lv"])
               {
                  variables["lv"] = LoaderSavingManager.Version;
               }
               if(!variables["rnd"])
               {
                  variables["rnd"] = TextLoader.TextLoaderKey;
               }
            }
            i = 0;
            for(p in variables)
            {
               if(i >= 1)
               {
                  variableString = variableString + ("&" + p + "=" + variables[p]);
               }
               else
               {
                  variableString = variableString + (p + "=" + variables[p]);
               }
               i++;
            }
            return path + "?" + variableString;
         }
         return path;
      }
      
      public function fixedNewVariablesURL(path:String, type:int, variables:URLVariables, argsCount:int) : String
      {
         var variableString:String = null;
         var i:int = 0;
         var p:* = null;
         if(type != BaseLoader.REQUEST_LOADER && type != BaseLoader.COMPRESS_REQUEST_LOADER)
         {
            variableString = "";
            if(variables == null)
            {
               variables = new URLVariables();
            }
            if(type == BaseLoader.BYTE_LOADER || type == BaseLoader.DISPLAY_LOADER || type == BaseLoader.BITMAP_LOADER || type == BaseLoader.MODULE_LOADER)
            {
               variables["lv"] = LoaderSavingManager.Version + argsCount;
            }
            else if(type == BaseLoader.COMPRESS_TEXT_LOADER || type == BaseLoader.TEXT_LOADER)
            {
               variables["rnd"] = TextLoader.TextLoaderKey + argsCount.toString();
            }
            i = 0;
            for(p in variables)
            {
               if(i >= 1)
               {
                  variableString = variableString + ("&" + p + "=" + variables[p]);
               }
               else
               {
                  variableString = variableString + (p + "=" + variables[p]);
               }
               i++;
            }
            return path + "?" + variableString;
         }
         return path;
      }
   }
}
