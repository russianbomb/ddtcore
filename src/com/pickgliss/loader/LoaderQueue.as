package com.pickgliss.loader
{
   import flash.events.Event;
   import flash.events.EventDispatcher;
   
   public class LoaderQueue extends EventDispatcher
   {
       
      
      private var _loaders:Vector.<BaseLoader>;
      
      public function LoaderQueue()
      {
         super();
         this._loaders = new Vector.<BaseLoader>();
      }
      
      public function addLoader(loader:BaseLoader) : void
      {
         this._loaders.push(loader);
      }
      
      public function start() : void
      {
         var count:int = this._loaders.length;
         for(var i:int = 0; i < count; i++)
         {
            if(this._loaders == null)
            {
               return;
            }
            this._loaders[i].addEventListener(LoaderEvent.COMPLETE,this.__loadComplete);
            LoaderManager.Instance.startLoad(this._loaders[i]);
         }
         if(count == 0)
         {
            dispatchEvent(new Event(Event.COMPLETE));
         }
      }
      
      public function dispose() : void
      {
         this.removeEvent();
         this._loaders = null;
      }
      
      public function removeEvent() : void
      {
         for(var i:int = 0; i < this._loaders.length; i++)
         {
            this._loaders[i].removeEventListener(LoaderEvent.COMPLETE,this.__loadComplete);
         }
      }
      
      public function get length() : int
      {
         return this._loaders.length;
      }
      
      public function get loaders() : Vector.<BaseLoader>
      {
         return this._loaders;
      }
      
      private function __loadComplete(event:LoaderEvent) : void
      {
         event.loader.removeEventListener(LoaderEvent.COMPLETE,this.__loadComplete);
         if(this.isComplete)
         {
            this.removeEvent();
            dispatchEvent(new Event(Event.COMPLETE));
         }
      }
      
      public function get isComplete() : Boolean
      {
         for(var i:int = 0; i < this._loaders.length; i++)
         {
            if(!this._loaders[i].isComplete)
            {
               return false;
            }
         }
         return true;
      }
   }
}
