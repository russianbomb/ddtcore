package com.pickgliss.loader
{
   import flash.display.Loader;
   import flash.events.Event;
   import flash.events.IOErrorEvent;
   import flash.net.URLLoaderDataFormat;
   import flash.system.LoaderContext;
   import flash.utils.ByteArray;
   import flash.utils.getTimer;
   
   public class DisplayLoader extends BaseLoader
   {
      
      public static var Context:LoaderContext;
      
      public static var isDebug:Boolean = false;
       
      
      protected var _displayLoader:Loader;
      
      public function DisplayLoader(id:int, url:String)
      {
         this._displayLoader = new Loader();
         super(id,url,null);
      }
      
      override public function loadFromBytes(data:ByteArray) : void
      {
         super.loadFromBytes(data);
         this._displayLoader.contentLoaderInfo.addEventListener(Event.COMPLETE,this.__onContentLoadComplete);
         this._displayLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR,this.__onIoError);
         if(this.type == BaseLoader.MODULE_LOADER)
         {
            this._displayLoader.loadBytes(data,Context);
         }
         else
         {
            this._displayLoader.loadBytes(data);
         }
      }
      
      private function __onIoError(event:IOErrorEvent) : void
      {
         this._displayLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR,this.__onIoError);
         throw new Error(event.text + " url: " + _url);
      }
      
      protected function __onContentLoadComplete(event:Event) : void
      {
         trace("load useTime : " + String(getTimer() - _starTime) + "  url:" + url);
         this._displayLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE,this.__onContentLoadComplete);
         this._displayLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR,this.__onIoError);
         fireCompleteEvent();
         this._displayLoader.unload();
         this._displayLoader = null;
      }
      
      override protected function __onDataLoadComplete(event:Event) : void
      {
         removeEvent();
         _loader.close();
         if(_loader.data.length == 0)
         {
            return;
         }
         LoaderSavingManager.cacheFile(_url,_loader.data,true);
         this.loadFromBytes(_loader.data);
      }
      
      override public function get content() : *
      {
         return this._displayLoader.content;
      }
      
      override protected function getLoadDataFormat() : String
      {
         return URLLoaderDataFormat.BINARY;
      }
      
      override public function get type() : int
      {
         return BaseLoader.DISPLAY_LOADER;
      }
   }
}
