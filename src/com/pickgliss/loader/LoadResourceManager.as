package com.pickgliss.loader
{
   import com.pickgliss.events.LoaderResourceEvent;
   import com.pickgliss.events.UIModuleEvent;
   import flash.events.EventDispatcher;
   import flash.net.URLVariables;
   import flash.system.ApplicationDomain;
   import flash.system.LoaderContext;
   import flash.utils.Dictionary;
   
   [Event(name="progress",type="com.pickgliss.events.LoaderResourceEvent")]
   [Event(name="loadError",type="com.pickgliss.events.LoaderResourceEvent")]
   [Event(name="delete",type="com.pickgliss.events.LoaderResourceEvent")]
   [Event(name="complete",type="com.pickgliss.events.LoaderResourceEvent")]
   [Event(name="init complete",type="com.pickgliss.events.LoaderResourceEvent")]
   public class LoadResourceManager extends EventDispatcher
   {
      
      private static var _instance:LoadResourceManager;
       
      
      private var _infoSite:String = "";
      
      private var _loadingUrl:String = "";
      
      private var _clientType:int;
      
      private var _loadDic:Dictionary;
      
      private var _loadUrlDic:Dictionary;
      
      private var _deleteList:Vector.<String>;
      
      private var _currentDeletePath:String;
      
      private var _isLoading:Boolean;
      
      private var _progress:Number;
      
      public function LoadResourceManager(single:Singleton)
      {
         super();
         if(!single)
         {
            throw Error("单例无法实例化");
         }
      }
      
      public static function get Instance() : LoadResourceManager
      {
         return _instance = _instance || new LoadResourceManager(new Singleton());
      }
      
      public function init(infoSite:String = "") : void
      {
         this._infoSite = infoSite;
         var loaderContext:LoaderContext = new LoaderContext(false,ApplicationDomain.currentDomain);
         LoaderManager.Instance.setup(loaderContext,String(Math.random()));
         this.addMicroClientEvent();
         LoadInterfaceManager.initAppInterface();
      }
      
      public function addMicroClientEvent() : void
      {
         LoadInterfaceManager.eventDispatcher.addEventListener(LoadInterfaceEvent.CHECK_COMPLETE,this.__checkComplete);
         LoadInterfaceManager.eventDispatcher.addEventListener(LoadInterfaceEvent.DELETE_COMPLETE,this.__deleteComplete);
         LoadInterfaceManager.eventDispatcher.addEventListener(LoadInterfaceEvent.FLASH_GOTO_AND_PLAY,this.__flashGotoAndPlay);
      }
      
      public function setLoginType(type:Number, loadingUrl:String = "", version:String = "-1") : void
      {
         this._clientType = int(type);
         this._loadingUrl = loadingUrl;
         LoaderSavingManager.Version = int(version);
      }
      
      public function setup(context:LoaderContext, textLoaderKey:String) : void
      {
         this._loadDic = new Dictionary();
         this._loadUrlDic = new Dictionary();
         this._deleteList = new Vector.<String>();
         LoaderManager.Instance.setup(context,textLoaderKey);
      }
      
      public function createLoader(filePath:String, type:int, args:URLVariables = null, requestMethod:String = "GET", domain:ApplicationDomain = null, useClient:Boolean = true) : *
      {
         return this.createOriginLoader(filePath,this._infoSite,type,args,requestMethod,domain,useClient);
      }
      
      public function createOriginLoader(filePath:String, rootSite:String, type:int, args:URLVariables = null, requestMethod:String = "GET", domain:ApplicationDomain = null, useClient:Boolean = false) : *
      {
         var loader:BaseLoader = null;
         var index:int = 0;
         var path:String = null;
         if(useClient && this._clientType == 1 && [BaseLoader.TEXT_LOADER,BaseLoader.COMPRESS_TEXT_LOADER,BaseLoader.REQUEST_LOADER,BaseLoader.COMPRESS_REQUEST_LOADER].indexOf(type) == -1)
         {
            filePath = this.fixedVariablesURL(filePath.toLowerCase(),type,args);
            index = rootSite.length;
            if(filePath.indexOf(rootSite) == -1)
            {
               LoadInterfaceManager.traceMsg("filePath = " + filePath + "路径有问题");
            }
            path = filePath.substring(index,filePath.length);
            loader = LoaderManager.Instance.creatLoaderByType(path,type,args,requestMethod,domain);
            this._loadDic[loader.id] = loader;
            this._loadUrlDic[loader.id] = filePath;
         }
         else
         {
            loader = LoaderManager.Instance.creatLoader(filePath,type,args,requestMethod,domain);
         }
         return loader;
      }
      
      public function creatAndStartLoad(filePath:String, type:int, args:URLVariables = null) : BaseLoader
      {
         var loader:BaseLoader = this.createLoader(filePath,type,args);
         this.startLoad(loader);
         return loader;
      }
      
      public function startLoad(loader:BaseLoader, loadImp:Boolean = false, useClient:Boolean = true) : void
      {
         this.startLoadFromLoadingUrl(loader,this._infoSite,loadImp,useClient);
      }
      
      public function startLoadFromLoadingUrl(loader:BaseLoader, rootSite:String, loadImp:Boolean = false, useClient:Boolean = true) : void
      {
         var filePath:String = loader.url;
         filePath = filePath.replace(/\?.*/,"");
         if(useClient && this._clientType == 1 && [BaseLoader.TEXT_LOADER,BaseLoader.COMPRESS_TEXT_LOADER,BaseLoader.REQUEST_LOADER,BaseLoader.COMPRESS_REQUEST_LOADER].indexOf(loader.type) == -1)
         {
            LoadInterfaceManager.checkResource(loader.id,rootSite,filePath,loadImp);
         }
         else
         {
            this.beginLoad(loader,loadImp);
         }
      }
      
      private function beginLoad(loader:BaseLoader, loadImp:Boolean = false) : void
      {
         LoaderManager.Instance.startLoad(loader,loadImp);
      }
      
      public function addDeleteRequest(filePath:String) : void
      {
         if(!this._deleteList)
         {
            this._deleteList = new Vector.<String>();
         }
         this._deleteList.push(filePath);
      }
      
      public function startDelete() : void
      {
         if(this._clientType != 1)
         {
            if(this._deleteList)
            {
               this._deleteList.length = 0;
            }
         }
         this.deleteNext();
      }
      
      private function deleteNext() : void
      {
         var evt:LoaderResourceEvent = null;
         if(this._deleteList)
         {
            if(this._deleteList.length > 0)
            {
               this._currentDeletePath = this._deleteList.shift();
               this.deleteResource(this._currentDeletePath);
            }
            else
            {
               evt = new LoaderResourceEvent(LoaderResourceEvent.DELETE);
               dispatchEvent(evt);
            }
         }
      }
      
      public function deleteResource(filePath:String) : void
      {
         LoadInterfaceManager.deleteResource(filePath);
      }
      
      protected function __checkComplete(event:LoadInterfaceEvent) : void
      {
         this.checkComplete(event.paras[0],event.paras[1],event.paras[2],event.paras[3]);
      }
      
      protected function __deleteComplete(event:LoadInterfaceEvent) : void
      {
         if(this._currentDeletePath == event.paras[1])
         {
            this.deleteComlete(event.paras[0],event.paras[1]);
         }
      }
      
      protected function __flashGotoAndPlay(event:LoadInterfaceEvent) : void
      {
         this.flashGotoAndPlay(int(event.paras[0]),event.paras[1]);
      }
      
      public function checkComplete(loaderID:String, bFlag:String, httpUrl:String, fileName:String) : void
      {
         var evt:LoaderResourceEvent = null;
         if(!this._loadDic)
         {
            return;
         }
         var loader:BaseLoader = this._loadDic[int(loaderID)];
         if(loader)
         {
            LoaderManager.Instance.setFlashLoadWeb();
            if(bFlag == "true")
            {
               this.beginLoad(loader);
            }
            else
            {
               loader.url = this._loadUrlDic[loader.id];
               this.beginLoad(loader);
            }
            if(this._loadDic)
            {
               delete this._loadDic[loader.id];
               delete this._loadUrlDic[loader.id];
            }
            if(loader.url.indexOf("2.png") != -1)
            {
               this._isLoading = false;
               this._progress = 1;
            }
         }
         else
         {
            LoadInterfaceManager.traceMsg("loader为空：" + loaderID + "* " + fileName);
         }
      }
      
      public function deleteComlete(bFlag:String, fileName:String) : void
      {
         this.deleteNext();
      }
      
      public function flashGotoAndPlay(loaderID:int, progress:Number) : void
      {
         if(!this._loadDic)
         {
            return;
         }
         var loader:BaseLoader = this._loadDic[int(loaderID)];
         if(loader)
         {
            if(loader.url.indexOf("2.png") != -1)
            {
               this._isLoading = true;
               this._progress = progress * 0.01;
            }
            else
            {
               UIModuleLoader.Instance.dispatchEvent(new UIModuleEvent(UIModuleEvent.UI_MODULE_PROGRESS,loader));
            }
         }
      }
      
      public function fixedVariablesURL(path:String, type:int, variables:URLVariables) : String
      {
         var variableString:String = null;
         var i:int = 0;
         var p:* = null;
         if(type != BaseLoader.REQUEST_LOADER && type != BaseLoader.COMPRESS_REQUEST_LOADER)
         {
            variableString = "";
            if(variables == null)
            {
               variables = new URLVariables();
            }
            if(type == BaseLoader.BYTE_LOADER || type == BaseLoader.DISPLAY_LOADER || type == BaseLoader.BITMAP_LOADER)
            {
               if(!variables["lv"])
               {
                  variables["lv"] = LoaderSavingManager.Version;
               }
            }
            else if(type == BaseLoader.COMPRESS_TEXT_LOADER || type == BaseLoader.TEXT_LOADER)
            {
               if(!variables["rnd"])
               {
                  variables["rnd"] = TextLoader.TextLoaderKey;
               }
            }
            else if(type == BaseLoader.MODULE_LOADER)
            {
               if(!variables["lv"])
               {
                  variables["lv"] = LoaderSavingManager.Version;
               }
               if(!variables["rnd"])
               {
                  variables["rnd"] = TextLoader.TextLoaderKey;
               }
            }
            i = 0;
            for(p in variables)
            {
               if(i >= 1)
               {
                  variableString = variableString + ("&" + p + "=" + variables[p]);
               }
               else
               {
                  variableString = variableString + (p + "=" + variables[p]);
               }
               i++;
            }
            return path + "?" + variableString;
         }
         return path;
      }
      
      public function get isMicroClient() : Boolean
      {
         return this._clientType == 1;
      }
      
      public function get clientType() : int
      {
         return this._clientType;
      }
      
      public function get infoSite() : String
      {
         return this._infoSite;
      }
      
      public function set infoSite(value:String) : void
      {
         this._infoSite = value;
      }
      
      public function get loadingUrl() : String
      {
         return this._loadingUrl;
      }
      
      public function get progress() : Number
      {
         return this._progress;
      }
      
      public function get isLoading() : Boolean
      {
         return this._isLoading;
      }
   }
}

class Singleton
{
    
   
   function Singleton()
   {
      super();
   }
}
