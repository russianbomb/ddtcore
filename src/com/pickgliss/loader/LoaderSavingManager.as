package com.pickgliss.loader
{
   import flash.display.Shape;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.events.NetStatusEvent;
   import flash.net.SharedObject;
   import flash.net.SharedObjectFlushStatus;
   import flash.utils.ByteArray;
   import flash.utils.Timer;
   import flash.utils.getTimer;
   
   public class LoaderSavingManager extends EventDispatcher
   {
      
      private static const LOCAL_FILE:String = "7road/files";
      
      private static var _cacheFile:Boolean = false;
      
      private static var _version:int;
      
      private static var _files:Object;
      
      private static var _saveTimer:Timer;
      
      private static var _so:SharedObject;
      
      private static var _changed:Boolean;
      
      private static var _save:Array;
      
      private static const READ_ERROR_ID:int = 2030;
      
      public static var ReadShareError:Boolean = false;
      
      private static const _reg1:RegExp = /http:\/\/[\w|.|:]+\//i;
      
      private static const _reg2:RegExp = /[:|.|\/]/g;
      
      private static var _isSaving:Boolean = false;
      
      private static var _shape:Shape = new Shape();
      
      private static var _retryCount:int = 0;
       
      
      public function LoaderSavingManager()
      {
         super();
      }
      
      public static function get Version() : int
      {
         return _version;
      }
      
      public static function set Version(value:int) : void
      {
         _version = value;
      }
      
      public static function set cacheAble(value:Boolean) : void
      {
         _cacheFile = value;
      }
      
      public static function get cacheAble() : Boolean
      {
         return _cacheFile;
      }
      
      public static function setup() : void
      {
         _cacheFile = false;
         _save = new Array();
         loadFilesInLocal();
      }
      
      public static function applyUpdate(fromv:int, tov:int, updatelist:Array) : void
      {
         var updated:Array = null;
         var s:String = null;
         var temp:Array = null;
         var f:* = null;
         var n:String = null;
         var t:String = null;
         if(tov <= fromv)
         {
            return;
         }
         if(_version < tov)
         {
            if(_version < fromv)
            {
               _so.data["data"] = _files = new Object();
            }
            else
            {
               updated = new Array();
               for each(s in updatelist)
               {
                  t = getPath(s);
                  t = t.replace("*","\\w*");
                  updated.push(new RegExp("^" + t));
               }
               temp = new Array();
               for(f in _files)
               {
                  f = f.toLocaleLowerCase();
                  if(hasUpdate(f,updated))
                  {
                     temp.push(f);
                  }
               }
               for each(n in temp)
               {
                  delete _files[n];
               }
            }
            _version = tov;
            _files["version"] = tov;
            _changed = true;
         }
      }
      
      public static function clearFiles(filter:String) : void
      {
         var updated:Array = null;
         var t:String = null;
         var temp:Array = null;
         var f:String = null;
         var n:String = null;
         if(_files)
         {
            updated = new Array();
            t = getPath(filter);
            t = t.replace("*","\\w*");
            updated.push(new RegExp("^" + t));
            temp = new Array();
            for(f in _files)
            {
               f = f.toLocaleLowerCase();
               if(hasUpdate(f,updated))
               {
                  temp.push(f);
               }
            }
            for each(n in temp)
            {
               delete _files[n];
            }
            try
            {
               if(_cacheFile)
               {
                  _so.flush(20 * 1024 * 1024);
               }
            }
            catch(e:Error)
            {
            }
         }
      }
      
      public static function loadFilesInLocal() : void
      {
         try
         {
            _so = SharedObject.getLocal(LOCAL_FILE,"/");
            _so.addEventListener(NetStatusEvent.NET_STATUS,__netStatus);
            _files = _so.data["data"];
            if(_files == null)
            {
               _files = new Object();
               _so.data["data"] = _files;
               _files["version"] = _version = -1;
               _cacheFile = false;
            }
            else
            {
               _version = _files["version"];
               _cacheFile = true;
            }
         }
         catch(e:Error)
         {
            if(e.errorID == READ_ERROR_ID)
            {
               resetErrorVersion();
            }
         }
      }
      
      public static function resetErrorVersion() : void
      {
         _version = Math.random() * -777777;
         ReadShareError = true;
      }
      
      private static function getPath(path:String) : String
      {
         var index:int = path.indexOf("?");
         if(index != -1)
         {
            path = path.substring(0,index);
         }
         path = path.replace(_reg1,"");
         return path.replace(_reg2,"-").toLocaleLowerCase();
      }
      
      public static function saveFilesToLocal() : void
      {
         try
         {
            if(_files && _changed && _cacheFile && !_isSaving)
            {
               _isSaving = true;
               _shape.addEventListener(Event.ENTER_FRAME,save);
            }
         }
         catch(e:Error)
         {
         }
      }
      
      private static function save(event:Event) : void
      {
         var state:String = null;
         var tick:int = 0;
         var obj:Object = null;
         var so:SharedObject = null;
         try
         {
            state = _so.flush(20 * 1024 * 1024);
            if(state != SharedObjectFlushStatus.PENDING)
            {
               tick = getTimer();
               if(_save.length > 0)
               {
                  obj = _save[0];
                  so = SharedObject.getLocal(obj.p,"/");
                  so.data["data"] = obj.d;
                  so.flush();
                  _files[obj.p] = true;
                  _so.flush();
                  _save.shift();
                  trace("save local data spend:",getTimer() - tick,"  left:",_save.length,"  file:",obj.p);
               }
               if(_save.length == 0)
               {
                  _shape.removeEventListener(Event.ENTER_FRAME,save);
                  _changed = false;
                  _isSaving = false;
               }
            }
         }
         catch(e:Error)
         {
            _shape.removeEventListener(Event.ENTER_FRAME,save);
         }
      }
      
      private static function hasUpdate(path:String, updateList:Array) : Boolean
      {
         var s:RegExp = null;
         for each(s in updateList)
         {
            if(path.match(s))
            {
               return true;
            }
         }
         return false;
      }
      
      public static function loadCachedFile(path:String, cacheInMem:Boolean) : ByteArray
      {
         var p:String = null;
         var tick:int = 0;
         var f:ByteArray = null;
         var so:SharedObject = null;
         if(_files)
         {
            p = getPath(path);
            tick = getTimer();
            f = findInSave(p);
            if(f == null && _files[p])
            {
               so = SharedObject.getLocal(p,"/");
               f = ByteArray(so.data["data"]);
            }
            if(f)
            {
               trace("get{local:",getTimer() - tick,"ms}",path);
               return f;
            }
         }
         trace("get{network}",path);
         return null;
      }
      
      private static function findInSave(path:String) : ByteArray
      {
         var cache:Object = null;
         for each(cache in _save)
         {
            if(cache.p == path)
            {
               return ByteArray(cache.d);
            }
         }
         return null;
      }
      
      public static function cacheFile(path:String, data:ByteArray, cacheInMem:Boolean) : void
      {
         var p:String = null;
         if(_files)
         {
            p = getPath(path);
            _save.push({
               "p":p,
               "d":data
            });
            _changed = true;
         }
      }
      
      private static function __netStatus(event:NetStatusEvent) : void
      {
         trace(event.info.code);
         switch(event.info.code)
         {
            case "SharedObject.Flush.Failed":
               if(_retryCount < 1)
               {
                  _so.flush(20 * 1024 * 1024);
                  _retryCount++;
               }
               else
               {
                  cacheAble = false;
               }
               break;
            default:
               _retryCount = 0;
         }
      }
      
      public static function parseUpdate(config:XML) : void
      {
         var vs:XMLList = null;
         var unode:XML = null;
         var fromv:int = 0;
         var tov:int = 0;
         var fs:XMLList = null;
         var updatelist:Array = null;
         var fn:XML = null;
         try
         {
            vs = config..version;
            for each(unode in vs)
            {
               fromv = int(unode.@from);
               tov = int(unode.@to);
               fs = unode..file;
               updatelist = new Array();
               for each(fn in fs)
               {
                  updatelist.push(String(fn.@value));
               }
               applyUpdate(fromv,tov,updatelist);
            }
         }
         catch(e:Error)
         {
            _version = -1;
            if(_so)
            {
               _so.data["data"] = _files = new Object();
            }
            _changed = true;
         }
         saveFilesToLocal();
      }
      
      public static function get hasFileToSave() : Boolean
      {
         return _cacheFile && _changed;
      }
      
      public static function clearAllCache() : void
      {
         var a:* = null;
         var file:String = null;
         var fileSO:SharedObject = null;
         if(!_so)
         {
            return;
         }
         var fileList:Array = [];
         for(a in _files)
         {
            if(a != "version")
            {
               a = a.toLocaleLowerCase();
               fileList.push(a);
               delete _files[a];
            }
         }
         while(file = fileList.pop())
         {
            fileSO = SharedObject.getLocal(file,"/");
            fileSO.data["data"] = new Object();
            fileSO.flush();
         }
         _version = -1;
         _so.data["data"] = _files = new Object();
         _so["version"] = -1;
         _so.flush();
      }
   }
}
