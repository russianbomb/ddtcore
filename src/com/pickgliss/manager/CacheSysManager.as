package com.pickgliss.manager
{
   import com.pickgliss.action.IAction;
   import com.pickgliss.action.TickOrderQueueAction;
   import flash.utils.Dictionary;
   
   public class CacheSysManager
   {
      
      private static var instance:CacheSysManager;
      
      private static var _lockDic:Dictionary = new Dictionary();
       
      
      private var _cacheDic:Dictionary;
      
      public function CacheSysManager()
      {
         super();
         this._cacheDic = new Dictionary();
         _lockDic = new Dictionary();
      }
      
      public static function getInstance() : CacheSysManager
      {
         if(instance == null)
         {
            instance = new CacheSysManager();
         }
         return instance;
      }
      
      private static function getReleaseAction(actions:Array, delay:uint = 0) : IAction
      {
         var action:IAction = new TickOrderQueueAction(actions,100,delay);
         return action;
      }
      
      public static function lock(flag:String) : void
      {
         _lockDic[flag] = true;
      }
      
      public static function unlock(flag:String) : void
      {
         delete _lockDic[flag];
      }
      
      public static function isLock(flag:String) : Boolean
      {
         return Boolean(_lockDic[flag])?Boolean(true):Boolean(false);
      }
      
      public function cache(flag:String, action:IAction) : void
      {
         if(!this._cacheDic[flag])
         {
            this._cacheDic[flag] = new Array();
         }
         this._cacheDic[flag].push(action);
      }
      
      public function release(flag:String, delay:uint = 0) : void
      {
         var action:IAction = null;
         if(this._cacheDic[flag])
         {
            action = getReleaseAction(this._cacheDic[flag] as Array,delay);
            action.act();
            delete this._cacheDic[flag];
         }
      }
      
      public function singleRelease(flag:String) : void
      {
         var action:IAction = null;
         var actQueue:Array = null;
         if(this._cacheDic[flag])
         {
            actQueue = this._cacheDic[flag];
            if(actQueue[0])
            {
               (actQueue[0] as IAction).act();
            }
            actQueue.shift();
         }
      }
      
      public function cacheFunction(flag:String, action:IAction) : void
      {
         if(!this._cacheDic[flag])
         {
            this._cacheDic[flag] = new Array();
         }
         this._cacheDic[flag].push(action);
      }
   }
}
