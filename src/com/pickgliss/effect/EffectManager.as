package com.pickgliss.effect
{
   import flash.display.DisplayObject;
   import flash.utils.Dictionary;
   
   public final class EffectManager
   {
      
      private static var _instance:EffectManager;
       
      
      private var _effects:Dictionary;
      
      private var _effectIDCounter:int = 0;
      
      public function EffectManager()
      {
         super();
         this._effects = new Dictionary();
      }
      
      public static function get Instance() : EffectManager
      {
         if(_instance == null)
         {
            _instance = new EffectManager();
         }
         return _instance;
      }
      
      public function getEffectID() : int
      {
         return this._effectIDCounter++;
      }
      
      public function creatEffect(type:int, target:DisplayObject, ... args) : IEffect
      {
         var effect:IEffect = this.creatEffectByEffectType(type);
         effect.initEffect(target,args);
         this._effects[effect.id] = effect;
         return effect;
      }
      
      public function getEffectByTarget($target:DisplayObject) : IEffect
      {
         var effect:IEffect = null;
         for each(effect in this._effects)
         {
            if($target == effect.target)
            {
               return effect;
            }
         }
         return null;
      }
      
      public function removeEffect(effect:IEffect) : void
      {
         effect.dispose();
         delete this._effects[effect.id];
      }
      
      public function creatEffectByEffectType(type:int) : IEffect
      {
         var effect:IEffect = null;
         switch(type)
         {
            case EffectTypes.ADD_MOVIE_EFFECT:
               effect = new AddMovieEffect(this.getEffectID());
               break;
            case EffectTypes.SHINER_ANIMATION:
               effect = new ShinerAnimation(this.getEffectID());
               break;
            case EffectTypes.ALPHA_SHINER_ANIMATION:
               effect = new AlphaShinerAnimation(this.getEffectID());
               break;
            case EffectTypes.Linear_SHINER_ANIMATION:
               effect = new LinearShinerAnimation(this.getEffectID());
         }
         return effect;
      }
   }
}
