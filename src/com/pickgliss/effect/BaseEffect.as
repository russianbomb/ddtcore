package com.pickgliss.effect
{
   import flash.display.DisplayObject;
   
   public class BaseEffect implements IEffect
   {
       
      
      protected var _target:DisplayObject;
      
      private var _id:int;
      
      public function BaseEffect($id:int)
      {
         super();
         this._id = $id;
      }
      
      public function dispose() : void
      {
         this._target = null;
      }
      
      public function get id() : int
      {
         return this._id;
      }
      
      public function set id($id:int) : void
      {
         this._id = $id;
      }
      
      public function initEffect(target:DisplayObject, data:Array) : void
      {
         this._target = target;
      }
      
      public function play() : void
      {
      }
      
      public function stop() : void
      {
      }
      
      public function get target() : DisplayObject
      {
         return this._target;
      }
   }
}
