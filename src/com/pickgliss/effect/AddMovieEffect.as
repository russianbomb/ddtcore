package com.pickgliss.effect
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.DisplayObject;
   import flash.display.DisplayObjectContainer;
   import flash.display.InteractiveObject;
   import flash.display.MovieClip;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   
   public class AddMovieEffect extends BaseEffect implements IEffect
   {
       
      
      private var _movies:Vector.<DisplayObject>;
      
      private var _rectangles:Vector.<Rectangle>;
      
      private var _data:Array;
      
      public function AddMovieEffect($id:int)
      {
         super($id);
      }
      
      override public function initEffect(target:DisplayObject, data:Array) : void
      {
         super.initEffect(target,data);
         this._data = data;
         this.creatMovie();
      }
      
      public function get movie() : Vector.<DisplayObject>
      {
         return this._movies;
      }
      
      override public function dispose() : void
      {
         super.dispose();
         for(var i:int = 0; i < this._movies.length; i++)
         {
            if(this._movies[i] is MovieClip)
            {
               MovieClip(this._movies[i]).stop();
            }
            if(this._movies[i])
            {
               ObjectUtils.disposeObject(this._movies[i]);
            }
         }
         this._movies = null;
      }
      
      override public function play() : void
      {
         var i:int = 0;
         super.play();
         for(i = 0; i < this._movies.length; i++)
         {
            if(this._movies[i] is MovieClip)
            {
               MovieClip(this._movies[i]).play();
            }
            if(_target.parent)
            {
               _target.parent.addChild(this._movies[i]);
            }
            this._movies[i].x = _target.x;
            this._movies[i].y = _target.y;
            if(this._rectangles.length - 1 >= i)
            {
               this._movies[i].x = this._movies[i].x + this._rectangles[i].x;
               this._movies[i].y = this._movies[i].y + this._rectangles[i].y;
            }
         }
      }
      
      override public function stop() : void
      {
         super.stop();
         for(var i:int = 0; i < this._movies.length; i++)
         {
            if(this._movies[i] is MovieClip)
            {
               MovieClip(this._movies[i]).stop();
            }
            if(this._movies[i].parent)
            {
               this._movies[i].parent.removeChild(this._movies[i]);
            }
         }
      }
      
      private function creatMovie() : void
      {
         this._movies = new Vector.<DisplayObject>();
         this._rectangles = new Vector.<Rectangle>();
         for(var i:int = 0; i < this._data.length; i++)
         {
            if(this._data[i] is DisplayObject)
            {
               this._movies.push(this._data[i]);
            }
            else if(this._data[i] is String)
            {
               this._movies.push(ComponentFactory.Instance.creat(this._data[i]));
            }
         }
         for(var j:int = 0; j < this._data.length; j++)
         {
            if(this._data[j] is Point)
            {
               this._rectangles.push(new Rectangle(this._data[j].x,this._data[j].y,0,0));
            }
            else if(this._data[j] is Rectangle)
            {
               this._rectangles.push(this._data[j]);
            }
         }
         for(var k:int = 0; k < this._movies.length; k++)
         {
            if(this._movies[k] is InteractiveObject)
            {
               InteractiveObject(this._movies[k]).mouseEnabled = false;
            }
            if(this._movies[k] is DisplayObjectContainer)
            {
               DisplayObjectContainer(this._movies[k]).mouseChildren = false;
               DisplayObjectContainer(this._movies[k]).mouseEnabled = false;
            }
         }
      }
   }
}
