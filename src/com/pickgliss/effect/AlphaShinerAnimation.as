package com.pickgliss.effect
{
   import com.greensock.TweenMax;
   import com.greensock.easing.Sine;
   import com.pickgliss.utils.EffectUtils;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.Bitmap;
   import flash.display.DisplayObject;
   import flash.display.DisplayObjectContainer;
   import flash.display.GradientType;
   import flash.display.InterpolationMethod;
   import flash.display.Shape;
   import flash.display.SpreadMethod;
   import flash.display.Sprite;
   import flash.geom.Matrix;
   
   public class AlphaShinerAnimation extends BaseEffect
   {
      
      public static const SPEED:String = "speed";
      
      public static const INTENSITY:String = "intensity";
      
      public static const WIDTH:String = "width";
      
      public static const EFFECT:String = "effect";
      
      public static const COLOR:String = "color";
      
      public static const BLUR_WIDTH:String = "blurWidth";
      
      public static const IS_LOOP:String = "isLoop";
      
      public static const STRENGTH:String = "strength";
       
      
      private var _addGlowEffect:Boolean = true;
      
      private var _alphas:Array;
      
      private var _colors:Array;
      
      private var _glowBlurWidth:Number = 3;
      
      private var _glowColorName:String = "blue";
      
      private var _glowStrength:Number = 1;
      
      protected var _isLoop:Boolean = true;
      
      protected var _maskHeight:Number;
      
      protected var _maskShape:Shape;
      
      protected var _maskWidth:Number;
      
      private var _percent:Array;
      
      protected var _shineAnimationContainer:Sprite;
      
      private var _sourceBitmap:Bitmap;
      
      private var _shineBitmapContainer:Sprite;
      
      private var _shineIntensity:Number = 30;
      
      protected var _shineMoveSpeed:Number = 0.75;
      
      private var _shineWidth:Number = 100;
      
      public function AlphaShinerAnimation($id:int)
      {
         this._maskShape = new Shape();
         super($id);
      }
      
      override public function dispose() : void
      {
         TweenMax.killTweensOf(this._maskShape);
         ObjectUtils.disposeObject(this._shineAnimationContainer);
         ObjectUtils.disposeObject(this._sourceBitmap);
         ObjectUtils.disposeObject(this._shineBitmapContainer);
         this._shineAnimationContainer = null;
         this._sourceBitmap = null;
         this._shineBitmapContainer = null;
         super.dispose();
      }
      
      override public function initEffect(target:DisplayObject, datas:Array) : void
      {
         super.initEffect(target,datas);
         var data:Object = datas[0];
         if(data)
         {
            if(data[SPEED])
            {
               this._shineMoveSpeed = data[SPEED];
            }
            if(data[INTENSITY])
            {
               this._shineIntensity = data[INTENSITY];
            }
            if(data[WIDTH])
            {
               this._shineWidth = data[WIDTH];
            }
            if(data[EFFECT])
            {
               this._addGlowEffect = data[EFFECT];
            }
            if(data[COLOR])
            {
               this._glowColorName = data[COLOR];
            }
            if(data[BLUR_WIDTH])
            {
               this._glowBlurWidth = data[BLUR_WIDTH];
            }
            if(data[IS_LOOP])
            {
               this._isLoop = data[IS_LOOP];
            }
            if(data[STRENGTH])
            {
               this._glowStrength = data[STRENGTH];
            }
         }
         this.image_shiner(this._shineMoveSpeed,this._shineIntensity,this._shineWidth,this._addGlowEffect,this._glowColorName,this._glowBlurWidth,this._glowStrength,this._isLoop);
      }
      
      override public function play() : void
      {
         if(TweenMax.isTweening(this._maskShape))
         {
            return;
         }
         super.play();
         if(!(target is DisplayObjectContainer))
         {
            this._shineAnimationContainer.x = target.x;
            this._shineAnimationContainer.y = target.y;
            target.parent.addChild(this._shineAnimationContainer);
         }
         else
         {
            DisplayObjectContainer(target).addChild(this._shineAnimationContainer);
         }
         if(this._isLoop)
         {
            TweenMax.to(this._maskShape,this._shineMoveSpeed,{
               "startAt":{"alpha":0},
               "alpha":1,
               "yoyo":true,
               "repeat":-1,
               "ease":Sine.easeOut
            });
         }
         else
         {
            TweenMax.to(this._maskShape,this._shineMoveSpeed,{
               "startAt":{"alpha":0},
               "alpha":1,
               "ease":Sine.easeOut
            });
         }
      }
      
      override public function stop() : void
      {
         super.stop();
         if(!this._shineAnimationContainer.parent)
         {
            return;
         }
         this._shineAnimationContainer.parent.removeChild(this._shineAnimationContainer);
         this._maskShape.alpha = 0;
         TweenMax.killTweensOf(this._maskShape);
      }
      
      private function image_shiner(shine_move_speed:Number, shine_intensity:Number, shine_width:Number, add_glow_effect:Boolean, glow_color_name:String, glow_blur_width:Number, glow_strength:Number, loop:Boolean) : void
      {
         this._shineAnimationContainer = new Sprite();
         this._shineBitmapContainer = new Sprite();
         this._sourceBitmap = EffectUtils.creatMcToBitmap(target,16711680);
         this._shineBitmapContainer.addChild(this._sourceBitmap);
         this._shineAnimationContainer.addChild(this._shineBitmapContainer);
         EffectUtils.imageShiner(this._shineAnimationContainer,shine_intensity);
         EffectUtils.imageGlower(this._shineBitmapContainer,glow_strength,glow_blur_width,15,glow_color_name);
         this.linear_fade(shine_width,shine_move_speed,60);
      }
      
      private function linear_fade(shine_width:Number, shine_move_speed:Number, mask_space:Number) : void
      {
         this._maskShape.cacheAsBitmap = true;
         this._shineAnimationContainer.cacheAsBitmap = true;
         this._shineAnimationContainer.mask = this._maskShape;
         this._maskWidth = this._shineAnimationContainer.width + mask_space;
         this._maskHeight = this._shineAnimationContainer.height + mask_space;
         this._maskShape.x = this._shineAnimationContainer.x - mask_space / 2;
         this._maskShape.y = this._shineAnimationContainer.y - mask_space / 2;
         this._colors = [16777215,16777215];
         this._alphas = [100,100];
         this._percent = [0,255];
         var matrix:Matrix = new Matrix();
         matrix.createGradientBox(this._maskWidth,this._maskHeight,0,(this._maskWidth - this._shineWidth) / 2,0);
         this._maskShape.graphics.beginGradientFill(GradientType.RADIAL,this._colors,this._alphas,this._percent,matrix,SpreadMethod.PAD,InterpolationMethod.LINEAR_RGB);
         this._maskShape.graphics.drawRect(0,0,this._maskWidth,this._maskHeight);
         this._maskShape.graphics.endFill();
         this._shineAnimationContainer.addChild(this._maskShape);
      }
   }
}
