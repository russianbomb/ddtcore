package com.pickgliss.action
{
   import com.pickgliss.toplevel.StageReferance;
   import com.pickgliss.ui.ComponentSetting;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   
   public class AlertAction extends BaseAction
   {
       
      
      private var _alert:BaseAlerFrame;
      
      private var _layerType:int;
      
      private var _blockBackgound:int;
      
      private var _soundStr:String;
      
      private var _center:Boolean;
      
      public function AlertAction(alert:BaseAlerFrame, layerType:int, blockBackgound:int, soundStr:String = null, center:Boolean = true)
      {
         super();
         this._alert = alert;
         this._layerType = layerType;
         this._blockBackgound = blockBackgound;
         this._soundStr = soundStr;
         this._center = center;
      }
      
      override public function act() : void
      {
         if(this._soundStr && ComponentSetting.PLAY_SOUND_FUNC is Function)
         {
            ComponentSetting.PLAY_SOUND_FUNC(this._soundStr);
         }
         if(this._alert && this._alert.info)
         {
            LayerManager.Instance.addToLayer(this._alert,this._layerType,this._alert.info.frameCenter,this._blockBackgound);
            StageReferance.stage.focus = this._alert;
         }
      }
   }
}
