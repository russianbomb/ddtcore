package com.pickgliss.action
{
   import flash.events.TimerEvent;
   import flash.utils.Timer;
   
   public class TickOrderQueueAction extends OrderQueueAction
   {
       
      
      private var _interval:uint;
      
      private var _tickTimer:Timer;
      
      private var _delay:uint;
      
      private var _delayTimer:Timer;
      
      public function TickOrderQueueAction(actList:Array, interval:uint = 100, delay:uint = 0, timeOut:uint = 0)
      {
         this._interval = interval;
         this._delay = delay;
         super(actList,timeOut);
      }
      
      override public function act() : void
      {
         if(this._delay > 0)
         {
            this._delayTimer = new Timer(this._delay,1);
            this._delayTimer.addEventListener(TimerEvent.TIMER_COMPLETE,this.onDelayTimerComplete);
            this._delayTimer.start();
         }
         else
         {
            super.act();
         }
      }
      
      private function onDelayTimerComplete(event:TimerEvent) : void
      {
         this.removeDelayTimer();
         super.act();
      }
      
      private function removeDelayTimer() : void
      {
         if(this._delayTimer)
         {
            this._delayTimer.stop();
            this._delayTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,this.onDelayTimerComplete);
            this._delayTimer = null;
         }
      }
      
      override protected function actOne() : void
      {
         var action:IAction = _actList[_count] as IAction;
         this.startTickTimer();
         action.act();
      }
      
      private function startTickTimer() : void
      {
         this._tickTimer = new Timer(this._interval,1);
         this._tickTimer.addEventListener(TimerEvent.TIMER_COMPLETE,this.onTickTimerComplete);
         this._tickTimer.start();
      }
      
      private function onTickTimerComplete(event:TimerEvent) : void
      {
         this.removeTickTimer();
         actNext();
      }
      
      private function removeTickTimer() : void
      {
         if(this._tickTimer)
         {
            this._tickTimer.stop();
            this._tickTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,this.onTickTimerComplete);
            this._tickTimer = null;
         }
      }
      
      override protected function onLimitTimerComplete(event:TimerEvent) : void
      {
         this.removeTickTimer();
         super.onLimitTimerComplete(event);
      }
      
      override public function cancel() : void
      {
         this.removeTickTimer();
         super.cancel();
      }
   }
}
