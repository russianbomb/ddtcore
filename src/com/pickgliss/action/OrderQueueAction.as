package com.pickgliss.action
{
   public class OrderQueueAction extends BaseAction
   {
       
      
      protected var _actList:Array;
      
      protected var _count:int;
      
      public function OrderQueueAction(actList:Array, timeOut:uint = 0)
      {
         this._actList = actList;
         super(timeOut);
      }
      
      override public function act() : void
      {
         this.cancel();
         this.startAct();
         super.act();
      }
      
      private function startAct() : void
      {
         this._count = 0;
         if(this._actList.length > 0)
         {
            this.actOne();
         }
      }
      
      protected function actOne() : void
      {
         var action:IAction = this._actList[this._count] as IAction;
         action.setCompleteFun(this.actOneComplete);
         action.act();
      }
      
      private function actOneComplete(target:IAction) : void
      {
         this.actNext();
      }
      
      protected function actNext() : void
      {
         this._count++;
         if(this._count < this._actList.length)
         {
            this.actOne();
         }
         else
         {
            execComplete();
         }
      }
      
      override public function cancel() : void
      {
         var action:IAction = null;
         if(_acting)
         {
            action = this._actList[this._count] as IAction;
            if(action)
            {
               action.setCompleteFun(null);
               action.cancel();
            }
         }
         super.cancel();
      }
      
      public function getActions() : Array
      {
         return this._actList;
      }
   }
}
