package com.pickgliss.ui.core
{
   import com.pickgliss.ui.ShowTipManager;
   
   public class TransformableComponent extends Component implements ITransformableTipedDisplay
   {
      
      public static const P_tipWidth:String = "tipWidth";
      
      public static const P_tipHeight:String = "tipHeight";
       
      
      protected var _tipWidth:int;
      
      protected var _tipHeight:int;
      
      public function TransformableComponent()
      {
         super();
      }
      
      public function get tipWidth() : int
      {
         return this._tipWidth;
      }
      
      public function set tipWidth(w:int) : void
      {
         if(this._tipWidth == w)
         {
            return;
         }
         this._tipWidth = w;
         onPropertiesChanged(P_tipWidth);
      }
      
      public function get tipHeight() : int
      {
         return this._tipHeight;
      }
      
      public function set tipHeight(h:int) : void
      {
         if(this._tipHeight == h)
         {
            return;
         }
         this._tipHeight = h;
         onPropertiesChanged(P_tipHeight);
      }
      
      override protected function onProppertiesUpdate() : void
      {
         if(_changedPropeties[P_tipWidth] || _changedPropeties[P_tipHeight] || _changedPropeties[P_tipDirction] || _changedPropeties[P_tipGap] || _changedPropeties[P_tipStyle] || _changedPropeties[P_tipData])
         {
            ShowTipManager.Instance.addTip(this);
         }
      }
   }
}
