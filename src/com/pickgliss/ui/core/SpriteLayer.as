package com.pickgliss.ui.core
{
   import com.pickgliss.toplevel.StageReferance;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.ComponentSetting;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.utils.DisplayUtils;
   import flash.display.DisplayObject;
   import flash.display.InteractiveObject;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   
   public class SpriteLayer extends Sprite
   {
       
      
      private var _blackGoundList:Vector.<DisplayObject>;
      
      private var _alphaGoundList:Vector.<DisplayObject>;
      
      private var _blackGound:Sprite;
      
      private var _alphaGound:Sprite;
      
      private var _autoClickTotop:Boolean;
      
      public function SpriteLayer(enableMouse:Boolean = false)
      {
         this.init();
         super();
         mouseEnabled = enableMouse;
      }
      
      private function init() : void
      {
         this._blackGoundList = new Vector.<DisplayObject>();
         this._alphaGoundList = new Vector.<DisplayObject>();
      }
      
      public function addTolayer(child:DisplayObject, blockGound:int, focusTop:Boolean) : void
      {
         if(blockGound == LayerManager.BLCAK_BLOCKGOUND)
         {
            if(this._blackGoundList.indexOf(child) != -1)
            {
               this._blackGoundList.splice(this._blackGoundList.indexOf(child),1);
            }
            this._blackGoundList.push(child);
         }
         else if(blockGound == LayerManager.ALPHA_BLOCKGOUND)
         {
            if(this._alphaGoundList.indexOf(child) != -1)
            {
               this._alphaGoundList.splice(this._alphaGoundList.indexOf(child),1);
            }
            this._alphaGoundList.push(child);
         }
         child.addEventListener(Event.REMOVED_FROM_STAGE,this.__onChildRemoveFromStage);
         if(focusTop)
         {
            child.addEventListener(Event.REMOVED_FROM_STAGE,this.__onFocusChange);
         }
         if(this._autoClickTotop)
         {
            child.addEventListener(MouseEvent.MOUSE_DOWN,this.__onClickToTop);
         }
         addChild(child);
         this.arrangeBlockGound();
         if(focusTop)
         {
            this.focusTopLayerDisplay();
         }
      }
      
      private function __onClickToTop(event:MouseEvent) : void
      {
         var child:DisplayObject = event.currentTarget as DisplayObject;
         addChild(child);
         this.focusTopLayerDisplay();
      }
      
      private function __onFocusChange(event:Event) : void
      {
         var child:DisplayObject = event.currentTarget as DisplayObject;
         child.removeEventListener(Event.REMOVED_FROM_STAGE,this.__onFocusChange);
         this.focusTopLayerDisplay(child);
      }
      
      private function __onChildRemoveFromStage(event:Event) : void
      {
         var child:DisplayObject = event.currentTarget as DisplayObject;
         child.removeEventListener(Event.REMOVED_FROM_STAGE,this.__onChildRemoveFromStage);
         child.removeEventListener(MouseEvent.MOUSE_DOWN,this.__onClickToTop);
         if(this._blackGoundList.indexOf(child) != -1)
         {
            this._blackGoundList.splice(this._blackGoundList.indexOf(child),1);
         }
         if(this._alphaGoundList.indexOf(child) != -1)
         {
            this._alphaGoundList.splice(this._alphaGoundList.indexOf(child),1);
         }
         this.arrangeBlockGound();
      }
      
      private function arrangeBlockGound() : void
      {
         var child:DisplayObject = null;
         var childIndex:int = 0;
         if(this.blackGound.parent)
         {
            this.blackGound.parent.removeChild(this.blackGound);
         }
         if(this.alphaGound.parent)
         {
            this.alphaGound.parent.removeChild(this.alphaGound);
         }
         if(this._blackGoundList.length > 0)
         {
            child = this._blackGoundList[this._blackGoundList.length - 1];
            childIndex = getChildIndex(child);
            addChildAt(this.blackGound,childIndex);
         }
         if(this._alphaGoundList.length > 0)
         {
            child = this._alphaGoundList[this._alphaGoundList.length - 1];
            childIndex = getChildIndex(child);
            addChildAt(this.alphaGound,childIndex);
         }
      }
      
      private function focusTopLayerDisplay(exclude:DisplayObject = null) : void
      {
         var lastFocus:InteractiveObject = null;
         var child:DisplayObject = null;
         for(var i:int = 0; i < numChildren; i++)
         {
            child = getChildAt(i);
            if(child != exclude)
            {
               lastFocus = child as InteractiveObject;
            }
         }
         if(!DisplayUtils.isTargetOrContain(StageReferance.stage.focus,lastFocus))
         {
            StageReferance.stage.focus = lastFocus;
         }
      }
      
      public function get backGroundInParent() : Boolean
      {
         if(this._blackGoundList.length > 0 || this._alphaGoundList.length > 0)
         {
            return true;
         }
         return false;
      }
      
      private function get blackGound() : Sprite
      {
         if(this._blackGound == null)
         {
            this._blackGound = new Sprite();
            this._blackGound.graphics.beginFill(0,0.4);
            this._blackGound.graphics.drawRect(-500,-200,2000,1000);
            this._blackGound.graphics.endFill();
            this._blackGound.addEventListener(MouseEvent.MOUSE_DOWN,this.__onBlackGoundMouseDown);
         }
         return this._blackGound;
      }
      
      private function __onBlackGoundMouseDown(event:MouseEvent) : void
      {
         event.stopImmediatePropagation();
         StageReferance.stage.focus = this._blackGoundList[this._blackGoundList.length - 1] as InteractiveObject;
      }
      
      private function get alphaGound() : Sprite
      {
         if(this._alphaGound == null)
         {
            this._alphaGound = new Sprite();
            this._alphaGound.graphics.beginFill(0,0.001);
            this._alphaGound.graphics.drawRect(-500,-200,2000,1000);
            this._alphaGound.graphics.endFill();
            this._alphaGound.addEventListener(MouseEvent.MOUSE_DOWN,this.__onAlphaGoundDownClicked);
            this._alphaGound.addEventListener(MouseEvent.MOUSE_UP,this.__onAlphaGoundUpClicked);
         }
         return this._alphaGound;
      }
      
      private function __onAlphaGoundDownClicked(event:MouseEvent) : void
      {
         var target:DisplayObject = null;
         target = this._alphaGoundList[this._alphaGoundList.length - 1];
         target.filters = ComponentFactory.Instance.creatFilters(ComponentSetting.ALPHA_LAYER_FILTER);
         StageReferance.stage.focus = target as InteractiveObject;
      }
      
      private function __onAlphaGoundUpClicked(event:MouseEvent) : void
      {
         var target:DisplayObject = this._alphaGoundList[this._alphaGoundList.length - 1];
         target.filters = null;
      }
      
      public function set autoClickTotop(value:Boolean) : void
      {
         if(this._autoClickTotop == value)
         {
            return;
         }
         this._autoClickTotop = value;
      }
   }
}
