package com.pickgliss.ui.image
{
   import com.pickgliss.ui.core.Component;
   import com.pickgliss.utils.ClassUtils;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.BitmapData;
   import flash.geom.Matrix;
   
   public class CloneImage extends Image
   {
      
      public static const P_direction:String = "direction";
      
      public static const P_gape:String = "gape";
       
      
      protected var _brush:BitmapData;
      
      protected var _direction:int = -1;
      
      protected var _gape:int = 0;
      
      private var _brushHeight:Number;
      
      private var _brushWidth:Number;
      
      public function CloneImage()
      {
         super();
      }
      
      public function set direction(value:int) : void
      {
         if(this._direction == value)
         {
            return;
         }
         this._direction = value;
         onPropertiesChanged(P_direction);
      }
      
      override public function dispose() : void
      {
         graphics.clear();
         ObjectUtils.disposeObject(this._brush);
         this._brush = null;
         super.dispose();
      }
      
      public function set gape(value:int) : void
      {
         if(this._gape == value)
         {
            return;
         }
         this._gape = value;
         onPropertiesChanged(P_gape);
      }
      
      override protected function resetDisplay() : void
      {
         graphics.clear();
         this._brushWidth = _width;
         this._brushHeight = _height;
         this._brush = ClassUtils.CreatInstance(_resourceLink,[0,0]);
      }
      
      override protected function updateSize() : void
      {
         var len:int = 0;
         var startMatrix:Matrix = null;
         var i:int = 0;
         if(_changedPropeties[Component.P_width] || _changedPropeties[Component.P_height] || _changedPropeties[P_direction] || _changedPropeties[P_gape])
         {
            len = 0;
            if(this._direction != -1)
            {
               if(this._direction == 1)
               {
                  len = _height / this._brush.height;
               }
               else
               {
                  len = _width / this._brush.width;
               }
            }
            graphics.clear();
            graphics.beginBitmapFill(this._brush);
            startMatrix = new Matrix();
            for(i = 0; i < len; i++)
            {
               if(this._direction == 1)
               {
                  graphics.drawRect(0,i * this._brush.height + this._gape,this._brush.width,this._brush.height);
               }
               else if(this._direction > 1)
               {
                  graphics.drawRect(i * this._brush.width + this._gape,0,this._brush.width,this._brush.height);
               }
               else
               {
                  graphics.drawRect(0,0,this._brush.width,this._brush.height);
               }
            }
            graphics.endFill();
         }
      }
   }
}
