package com.pickgliss.ui.image
{
   import com.pickgliss.geom.InnerRectangle;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Component;
   import com.pickgliss.utils.ClassUtils;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.DisplayObject;
   import flash.display.Sprite;
   import flash.geom.Rectangle;
   
   public class MutipleImage extends Image
   {
      
      public static const P_imageRect:String = "imagesRect";
       
      
      private var _imageLinks:Array;
      
      private var _imageRectString:String;
      
      private var _images:Vector.<DisplayObject>;
      
      private var _imagesRect:Vector.<InnerRectangle>;
      
      public function MutipleImage()
      {
         super();
      }
      
      override public function dispose() : void
      {
         var i:int = 0;
         if(this._images)
         {
            for(i = 0; i < this._images.length; i++)
            {
               ObjectUtils.disposeObject(this._images[i]);
            }
         }
         this._images = null;
         this._imagesRect = null;
         super.dispose();
      }
      
      public function set imageRectString(value:String) : void
      {
         if(this._imageRectString == value)
         {
            return;
         }
         this._imagesRect = new Vector.<InnerRectangle>();
         this._imageRectString = value;
         var rectsData:Array = ComponentFactory.parasArgs(this._imageRectString);
         for(var i:int = 0; i < rectsData.length; i++)
         {
            if(String(rectsData[i]) == "")
            {
               this._imagesRect.push(null);
            }
            else
            {
               this._imagesRect.push(ClassUtils.CreatInstance(ClassUtils.INNERRECTANGLE,String(rectsData[i]).split("|")));
            }
         }
         onPropertiesChanged(P_imageRect);
      }
      
      override protected function addChildren() : void
      {
         super.addChildren();
         if(this._images == null)
         {
            return;
         }
         for(var i:int = 0; i < this._images.length; i++)
         {
            Sprite(_display).addChild(this._images[i]);
         }
      }
      
      override protected function init() : void
      {
         _display = new Sprite();
         super.init();
      }
      
      override protected function resetDisplay() : void
      {
         this._imageLinks = ComponentFactory.parasArgs(_resourceLink);
         this.removeImages();
         this.creatImages();
      }
      
      override protected function updateSize() : void
      {
         var i:int = 0;
         var innerRect:Rectangle = null;
         if(this._images == null)
         {
            return;
         }
         if(_changedPropeties[Component.P_width] || _changedPropeties[Component.P_height] || _changedPropeties[P_imageRect])
         {
            for(i = 0; i < this._images.length; i++)
            {
               if(this._imagesRect && this._imagesRect[i])
               {
                  innerRect = this._imagesRect[i].getInnerRect(_width,_height);
                  this._images[i].x = innerRect.x;
                  this._images[i].y = innerRect.y;
                  this._images[i].height = innerRect.height;
                  this._images[i].width = innerRect.width;
                  _width = Math.max(_width,this._images[i].width);
                  _height = Math.max(_height,this._images[i].height);
               }
               else
               {
                  this._images[i].width = _width;
                  this._images[i].height = _height;
               }
            }
         }
      }
      
      private function creatImages() : void
      {
         var image:DisplayObject = null;
         var imageArgs:Array = null;
         this._images = new Vector.<DisplayObject>();
         for(var i:int = 0; i < this._imageLinks.length; i++)
         {
            imageArgs = String(this._imageLinks[i]).split("|");
            image = ComponentFactory.Instance.creat(imageArgs[0]);
            this._images.push(image);
         }
      }
      
      private function removeImages() : void
      {
         if(this._images == null)
         {
            return;
         }
         for(var i:int = 0; i < this._images.length; i++)
         {
            ObjectUtils.disposeObject(this._images[i]);
         }
      }
   }
}
