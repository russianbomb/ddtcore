package com.pickgliss.ui.image
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Component;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.BitmapData;
   import flash.geom.Matrix;
   
   public class Scale9CornerImage extends Image
   {
       
      
      private var _imageLinks:Array;
      
      private var _images:Vector.<BitmapData>;
      
      public function Scale9CornerImage()
      {
         super();
      }
      
      override public function dispose() : void
      {
         graphics.clear();
         this.removeImages();
         this._images = null;
         this._imageLinks = null;
         super.dispose();
      }
      
      override protected function resetDisplay() : void
      {
         this._imageLinks = ComponentFactory.parasArgs(_resourceLink);
         this.removeImages();
         this.creatImages();
      }
      
      override protected function updateSize() : void
      {
         if(_changedPropeties[Component.P_width] || _changedPropeties[Component.P_height])
         {
            this.drawImage();
         }
      }
      
      private function creatImages() : void
      {
         this._images = new Vector.<BitmapData>();
         for(var i:int = 0; i < this._imageLinks.length; i++)
         {
            this._images.push(ComponentFactory.Instance.creatBitmapData(this._imageLinks[i]));
         }
      }
      
      private function drawImage() : void
      {
         graphics.clear();
         var startMatrix:Matrix = new Matrix();
         startMatrix.tx = 0;
         startMatrix.ty = 0;
         graphics.beginBitmapFill(this._images[0],startMatrix);
         graphics.drawRect(0,0,this._images[0].width,this._images[0].height);
         startMatrix.tx = this._images[0].width;
         startMatrix.ty = 0;
         graphics.beginBitmapFill(this._images[1],startMatrix);
         graphics.drawRect(this._images[0].width,0,_width - this._images[0].width - this._images[2].width,this._images[1].height);
         startMatrix.tx = _width - this._images[2].width;
         startMatrix.ty = 0;
         graphics.beginBitmapFill(this._images[2],startMatrix);
         graphics.drawRect(_width - this._images[2].width,0,this._images[2].width,this._images[2].height);
         startMatrix.tx = 0;
         startMatrix.ty = this._images[0].height;
         graphics.beginBitmapFill(this._images[3],startMatrix);
         graphics.drawRect(0,this._images[0].height,this._images[3].width,_height - this._images[0].height - this._images[6].height);
         startMatrix.tx = this._images[0].width;
         startMatrix.ty = this._images[0].height;
         graphics.beginBitmapFill(this._images[4],startMatrix);
         graphics.drawRect(this._images[0].width,this._images[0].height,_width - this._images[0].width - this._images[2].width,_height - this._images[0].height - this._images[6].height);
         startMatrix.tx = _width - this._images[5].width;
         startMatrix.ty = this._images[2].height;
         graphics.beginBitmapFill(this._images[5],startMatrix);
         graphics.drawRect(_width - this._images[5].width,this._images[2].height,this._images[5].width,_height - this._images[2].height - this._images[8].height);
         startMatrix.tx = 0;
         startMatrix.ty = _height - this._images[6].height;
         graphics.beginBitmapFill(this._images[6],startMatrix);
         graphics.drawRect(0,_height - this._images[6].height,this._images[6].width,this._images[6].height);
         startMatrix.tx = this._images[6].width;
         startMatrix.ty = _height - this._images[7].height;
         graphics.beginBitmapFill(this._images[7],startMatrix);
         graphics.drawRect(this._images[6].width,_height - this._images[7].height,_width - this._images[6].width - this._images[8].width,this._images[7].height);
         startMatrix.tx = _width - this._images[8].width;
         startMatrix.ty = _height - this._images[8].height;
         graphics.beginBitmapFill(this._images[8],startMatrix);
         graphics.drawRect(_width - this._images[8].width,_height - this._images[8].height,this._images[8].width,this._images[8].height);
         graphics.endFill();
      }
      
      private function removeImages() : void
      {
         if(this._images == null)
         {
            return;
         }
         graphics.clear();
         for(var i:int = 0; i < this._images.length; i++)
         {
            ObjectUtils.disposeObject(this._images[i]);
         }
      }
   }
}
