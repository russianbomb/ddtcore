package com.pickgliss.ui.image
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Component;
   import com.pickgliss.utils.ObjectUtils;
   import com.pickgliss.utils.StringUtils;
   import flash.display.DisplayObject;
   import flash.display.MovieClip;
   
   public class ScaleFrameImage extends Image
   {
      
      public static const P_fillAlphaRect:String = "fillAlphaRect";
       
      
      private var _currentFrame:uint;
      
      private var _fillAlphaRect:Boolean;
      
      private var _imageLinks:Array;
      
      private var _images:Vector.<DisplayObject>;
      
      public function ScaleFrameImage()
      {
         super();
      }
      
      override public function dispose() : void
      {
         this.removeImages();
         this._images = null;
         this._imageLinks = null;
         super.dispose();
      }
      
      public function set fillAlphaRect(value:Boolean) : void
      {
         if(this._fillAlphaRect == value)
         {
            return;
         }
         this._fillAlphaRect = value;
         onPropertiesChanged(P_fillAlphaRect);
      }
      
      public function get getFrame() : uint
      {
         return this._currentFrame;
      }
      
      override public function setFrame(frameIndex:int) : void
      {
         super.setFrame(frameIndex);
         this._currentFrame = frameIndex;
         for(var i:int = 0; i < this._images.length; i++)
         {
            if(this._images[i] != null)
            {
               if(frameIndex - 1 == i)
               {
                  addChild(this._images[i]);
                  if(this._images[i] is MovieImage)
                  {
                     ((this._images[i] as MovieImage).display as MovieClip).gotoAndPlay(1);
                  }
                  if(_width != Math.round(this._images[i].width))
                  {
                     _width = Math.round(this._images[i].width);
                     _changedPropeties[Component.P_width] = true;
                  }
               }
               else if(this._images[i] && this._images[i].parent)
               {
                  removeChild(this._images[i]);
               }
            }
         }
         this.fillRect();
      }
      
      private function fillRect() : void
      {
         if(this._fillAlphaRect)
         {
            graphics.beginFill(16711935,0);
            graphics.drawRect(0,0,_width,_height);
            graphics.endFill();
         }
      }
      
      override protected function init() : void
      {
         super.init();
      }
      
      override protected function resetDisplay() : void
      {
         this._imageLinks = ComponentFactory.parasArgs(_resourceLink);
         this.removeImages();
         this.fillImages();
         this.creatFrameImage(0);
      }
      
      override protected function updateSize() : void
      {
         var i:int = 0;
         if(this._images == null)
         {
            return;
         }
         if(_changedPropeties[Component.P_width] || _changedPropeties[Component.P_height])
         {
            for(i = 0; i < this._images.length; i++)
            {
               if(this._images[i] != null)
               {
                  this._images[i].width = _width;
                  this._images[i].height = _height;
               }
            }
         }
      }
      
      private function fillImages() : void
      {
         this._images = new Vector.<DisplayObject>();
         for(var i:int = 0; i < this._imageLinks.length; i++)
         {
            this._images.push(null);
         }
      }
      
      public function creatFrameImage(index:int) : void
      {
         var i:int = 0;
         var image:DisplayObject = null;
         for(i = 0; i < this._imageLinks.length; i++)
         {
            if(!StringUtils.isEmpty(this._imageLinks[i]) && this._images[i] == null)
            {
               image = ComponentFactory.Instance.creat(this._imageLinks[i]);
               _width = Math.max(_width,image.width);
               _height = Math.max(_height,image.height);
               this._images[i] = image;
               addChild(image);
            }
         }
      }
      
      public function getFrameImage(frameIndex:int) : DisplayObject
      {
         return this._images[frameIndex];
      }
      
      private function removeImages() : void
      {
         if(this._images == null)
         {
            return;
         }
         for(var i:int = 0; i < this._images.length; i++)
         {
            ObjectUtils.disposeObject(this._images[i]);
         }
      }
      
      public function get totalFrames() : int
      {
         return this._images.length;
      }
   }
}
