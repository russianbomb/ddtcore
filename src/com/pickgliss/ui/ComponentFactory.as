package com.pickgliss.ui
{
   import com.pickgliss.utils.ClassUtils;
   import com.pickgliss.utils.ObjectUtils;
   import com.pickgliss.utils.StringUtils;
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.system.ApplicationDomain;
   import flash.utils.Dictionary;
   
   public final class ComponentFactory
   {
      
      private static var _instance:ComponentFactory;
      
      private static var COMPONENT_COUNTER:int = 1;
       
      
      private var _allComponents:Dictionary;
      
      private var _model:ComponentModel;
      
      public function ComponentFactory(enforcer:ComponentFactoryEnforcer)
      {
         super();
         this._model = new ComponentModel();
         this._allComponents = new Dictionary();
         ClassUtils.uiSourceDomain = ApplicationDomain.currentDomain;
      }
      
      public static function get Instance() : ComponentFactory
      {
         if(_instance == null)
         {
            _instance = new ComponentFactory(new ComponentFactoryEnforcer());
         }
         return _instance;
      }
      
      public static function parasArgs(args:String) : Array
      {
         var resultArgs:Array = args.split(",");
         for(var i:int = 0; i < resultArgs.length; i++)
         {
            StringUtils.trim(resultArgs[i]);
         }
         return resultArgs;
      }
      
      public function creat(stylename:String, args:Array = null) : *
      {
         var com:* = undefined;
         if(this._model.getComonentStyle(stylename))
         {
            com = this.creatComponentByStylename(stylename);
         }
         else if(this._model.getBitmapSet(stylename) || ClassUtils.classIsBitmapData(stylename))
         {
            com = this.creatBitmap(stylename);
         }
         else if(this._model.getCustomObjectStyle(stylename))
         {
            com = this.creatCustomObject(stylename,args);
         }
         else
         {
            com = ClassUtils.CreatInstance(stylename,args);
         }
         return com;
      }
      
      public function creatBitmap(classname:String) : Bitmap
      {
         var bitmapData:BitmapData = null;
         var bitmap:Bitmap = null;
         var bitmapSet:XML = this._model.getBitmapSet(classname);
         if(bitmapSet == null)
         {
            if(ClassUtils.uiSourceDomain.hasDefinition(classname))
            {
               bitmapData = ClassUtils.CreatInstance(classname,[0,0]);
               bitmap = new Bitmap(bitmapData);
               this._model.addBitmapSet(classname,new XML("<bitmapData resourceLink=\'" + classname + "\' width=\'" + bitmap.width + "\' height=\'" + bitmap.height + "\' />"));
            }
            else
            {
               throw new Error("Bitmap:" + classname + " is Not Found!",888);
            }
         }
         else
         {
            if(bitmapSet.name() == ComponentSetting.BITMAPDATA_TAG_NAME)
            {
               bitmapData = this.creatBitmapData(classname);
               bitmap = new Bitmap(bitmapData,"auto",String(bitmapSet.@smoothing) == "true");
            }
            else
            {
               bitmap = ClassUtils.CreatInstance(classname);
            }
            ObjectUtils.copyPorpertiesByXML(bitmap,bitmapSet);
         }
         return bitmap;
      }
      
      public function creatBitmapData(classname:String) : BitmapData
      {
         var bitmapData:BitmapData = null;
         var bitmapSet:XML = this._model.getBitmapSet(classname);
         if(bitmapSet == null)
         {
            return ClassUtils.CreatInstance(classname,[0,0]);
         }
         if(bitmapSet.name() == ComponentSetting.BITMAPDATA_TAG_NAME)
         {
            bitmapData = ClassUtils.CreatInstance(classname,[int(bitmapSet.@width),int(bitmapSet.@height)]);
         }
         else
         {
            bitmapData = ClassUtils.CreatInstance(classname)["btimapData"];
         }
         return bitmapData;
      }
      
      public function creatComponentByStylename(stylename:String, args:Array = null) : *
      {
         var style:XML = this.getComponentStyle(stylename);
         var classname:String = style.@classname;
         var component:* = ClassUtils.CreatInstance(classname,args);
         component.id = this.componentID;
         this._allComponents[component.id] = component;
         if(ClassUtils.classIsComponent(classname))
         {
            component.beginChanges();
            ObjectUtils.copyPorpertiesByXML(component,style);
            component.commitChanges();
         }
         else
         {
            ObjectUtils.copyPorpertiesByXML(component,style);
         }
         component["stylename"] = stylename;
         return component;
      }
      
      private function getComponentStyle(stylename:String) : XML
      {
         var parentStyle:XML = null;
         var style:XML = this._model.getComonentStyle(stylename);
         while(style.hasOwnProperty("@parentStyle"))
         {
            parentStyle = this._model.getComonentStyle(String(style.@parentStyle));
            delete style.@parentStyle;
            ObjectUtils.combineXML(style,parentStyle);
         }
         return style;
      }
      
      public function getCustomStyle(stylename:String) : XML
      {
         var parentStyle:XML = null;
         var style:XML = this._model.getCustomObjectStyle(stylename);
         if(style == null)
         {
            return null;
         }
         while(style && style.hasOwnProperty("@parentStyle"))
         {
            parentStyle = this._model.getCustomObjectStyle(String(style.@parentStyle));
            delete style.@parentStyle;
            ObjectUtils.combineXML(style,parentStyle);
         }
         return style;
      }
      
      public function creatCustomObject(stylename:String, args:Array = null) : *
      {
         var style:XML = this.getCustomStyle(stylename);
         var classname:String = style.@classname;
         var custom:* = ClassUtils.CreatInstance(classname,args);
         ObjectUtils.copyPorpertiesByXML(custom,style);
         return custom;
      }
      
      public function getComponentByID(componentid:int) : *
      {
         return this._allComponents[componentid];
      }
      
      public function checkAllComponentDispose(moduleArr:Array) : void
      {
         var item:XML = null;
         var compo:* = undefined;
         var dicStyle:Dictionary = this._model.allComponentStyle;
         var len:int = moduleArr.length;
         var k:int = 1;
         for(var i:int = 0; i < moduleArr.length; i++)
         {
            for each(item in dicStyle)
            {
               if(item.@componentModule != null && item.@componentModule == moduleArr[i])
               {
                  for each(compo in this._allComponents)
                  {
                     if(compo && compo.stylename == item.@stylename)
                     {
                        trace(k.toString() + ". " + String(item.@stylename) + " =================> 可能未释放...请注意!");
                        k++;
                     }
                  }
               }
            }
         }
      }
      
      public function removeComponent(componentid:int) : void
      {
         delete this._allComponents[componentid];
      }
      
      public function creatFrameFilters(filterString:String) : Array
      {
         var filterStyles:Array = parasArgs(filterString);
         var resultFilters:Array = [];
         for(var i:int = 0; i < filterStyles.length; i++)
         {
            if(filterStyles[i] == "null")
            {
               resultFilters.push(null);
            }
            else
            {
               resultFilters.push(this.creatFilters(filterStyles[i]));
            }
         }
         return resultFilters;
      }
      
      public function creatFilters(filtersStyle:String) : Array
      {
         var frameFilterStyle:Array = filtersStyle.split("|");
         var frameFilter:Array = [];
         for(var i:int = 0; i < frameFilterStyle.length; i++)
         {
            frameFilter.push(ComponentFactory.Instance.model.getSet(frameFilterStyle[i]));
         }
         return frameFilter;
      }
      
      public function get model() : ComponentModel
      {
         return this._model;
      }
      
      public function setup(config:XML) : void
      {
         this._model.addComponentSet(config);
      }
      
      public function get componentID() : int
      {
         return COMPONENT_COUNTER++;
      }
   }
}

class ComponentFactoryEnforcer
{
    
   
   function ComponentFactoryEnforcer()
   {
      super();
   }
}
