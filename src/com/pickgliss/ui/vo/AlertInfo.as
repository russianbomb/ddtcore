package com.pickgliss.ui.vo
{
   import com.pickgliss.events.InteractiveEvent;
   import com.pickgliss.ui.ComponentSetting;
   import flash.events.EventDispatcher;
   import flash.geom.Point;
   
   public class AlertInfo extends EventDispatcher
   {
      
      public static const CANCEL_LABEL:String = "Отмена";
      
      public static const SUBMIT_LABEL:String = "Подтвердить";
       
      
      private var _type:int;
      
      private var _customPos:Point;
      
      private var _autoButtonGape:Boolean = true;
      
      private var _autoDispose:Boolean = false;
      
      private var _bottomGap:int;
      
      private var _buttonGape:int;
      
      private var _cancelLabel:String = "Отмена";
      
      private var _data:Object;
      
      private var _enableHtml:Boolean;
      
      private var _enterEnable:Boolean = true;
      
      private var _escEnable:Boolean = true;
      
      private var _frameCenter:Boolean = true;
      
      private var _moveEnable:Boolean = true;
      
      private var _mutiline:Boolean;
      
      private var _showCancel:Boolean = true;
      
      private var _showSubmit:Boolean = true;
      
      private var _submitEnabled:Boolean = true;
      
      private var _cancelEnabled:Boolean = true;
      
      private var _submitLabel:String = "Подтвердить";
      
      private var _textShowHeight:int;
      
      private var _textShowWidth:int;
      
      private var _title:String;
      
      private var _soundID;
      
      public function AlertInfo($title:String = "", $submitLabel:String = "Подтвердить", $cancelLabel:String = "Отмена", $showSubmit:Boolean = true, $showCancel:Boolean = true, $data:Object = null, $center:Boolean = true, $movable:Boolean = true, $enableEnter:Boolean = true, $enableEsc:Boolean = true, $bottomGap:int = 20, $buttonGape:int = 30, $autoDispose:Boolean = false, $type:int = 0)
      {
         this._buttonGape = ComponentSetting.ALERT_BUTTON_GAPE;
         super();
         this.title = $title;
         this.type = $type;
         this.submitLabel = $submitLabel;
         this.cancelLabel = $cancelLabel;
         this.showSubmit = $showSubmit;
         this.showCancel = $showCancel;
         this.data = $data;
         this.frameCenter = $center;
         this.moveEnable = $movable;
         this.enterEnable = $enableEnter;
         this.escEnable = $enableEsc;
         this.bottomGap = $bottomGap;
         this.buttonGape = $buttonGape;
         this.autoDispose = $autoDispose;
      }
      
      public function get autoButtonGape() : Boolean
      {
         return this._autoButtonGape;
      }
      
      public function set autoButtonGape(value:Boolean) : void
      {
         if(this._autoButtonGape == value)
         {
            return;
         }
         this._autoButtonGape = value;
         this.fireChange();
      }
      
      public function get autoDispose() : Boolean
      {
         return this._autoDispose;
      }
      
      public function set autoDispose(value:Boolean) : void
      {
         if(this._autoDispose == value)
         {
            return;
         }
         this._autoDispose = value;
         this.fireChange();
      }
      
      public function get type() : int
      {
         return this._type;
      }
      
      public function set type(value:int) : void
      {
         if(this._type == value)
         {
            return;
         }
         this._type = value;
         this.fireChange();
      }
      
      public function get bottomGap() : int
      {
         return this._bottomGap;
      }
      
      public function set bottomGap(value:int) : void
      {
         if(this._bottomGap == value)
         {
            return;
         }
         this._bottomGap = value;
         this.fireChange();
      }
      
      public function get buttonGape() : int
      {
         return this._buttonGape;
      }
      
      public function set buttonGape(value:int) : void
      {
         if(this._buttonGape == value)
         {
            return;
         }
         this._buttonGape = value;
         this.fireChange();
      }
      
      public function get cancelLabel() : String
      {
         return this._cancelLabel;
      }
      
      public function set cancelLabel(value:String) : void
      {
         if(this._cancelLabel == value)
         {
            return;
         }
         this._cancelLabel = value;
         this.fireChange();
      }
      
      public function get submitEnabled() : Boolean
      {
         return this._submitEnabled;
      }
      
      public function set submitEnabled(val:Boolean) : void
      {
         if(this._submitEnabled != val)
         {
            this._submitEnabled = val;
            this.fireChange();
         }
      }
      
      public function get cancelEnabled() : Boolean
      {
         return this._cancelEnabled;
      }
      
      public function set cancelEnabled(val:Boolean) : void
      {
         if(this._cancelEnabled != val)
         {
            this._cancelEnabled = val;
            this.fireChange();
         }
      }
      
      public function get data() : Object
      {
         return this._data;
      }
      
      public function set data(value:Object) : void
      {
         if(this._data == value)
         {
            return;
         }
         this._data = value;
         this.fireChange();
      }
      
      public function get enableHtml() : Boolean
      {
         return this._enableHtml;
      }
      
      public function set enableHtml(value:Boolean) : void
      {
         if(this._enableHtml == value)
         {
            return;
         }
         this._enableHtml = value;
         this.fireChange();
      }
      
      public function get enterEnable() : Boolean
      {
         return this._enterEnable;
      }
      
      public function set enterEnable(value:Boolean) : void
      {
         if(this._enterEnable == value)
         {
            return;
         }
         this._enterEnable = value;
         this.fireChange();
      }
      
      public function get escEnable() : Boolean
      {
         return this._escEnable;
      }
      
      public function set escEnable(value:Boolean) : void
      {
         if(this._escEnable == value)
         {
            return;
         }
         this._escEnable = value;
         this.fireChange();
      }
      
      public function get frameCenter() : Boolean
      {
         return this._frameCenter;
      }
      
      public function set frameCenter(value:Boolean) : void
      {
         if(this._frameCenter == value)
         {
            return;
         }
         this._frameCenter = value;
         this.fireChange();
      }
      
      public function get moveEnable() : Boolean
      {
         return this._moveEnable;
      }
      
      public function set moveEnable(value:Boolean) : void
      {
         if(this._moveEnable == value)
         {
            return;
         }
         this._moveEnable = value;
         this.fireChange();
      }
      
      public function get mutiline() : Boolean
      {
         return this._mutiline;
      }
      
      public function set mutiline(value:Boolean) : void
      {
         if(this._mutiline == value)
         {
            return;
         }
         this._mutiline = value;
         this.fireChange();
      }
      
      public function get showCancel() : Boolean
      {
         return this._showCancel;
      }
      
      public function set showCancel(value:Boolean) : void
      {
         if(this._showCancel == value)
         {
            return;
         }
         this._showCancel = value;
         this.fireChange();
      }
      
      public function get showSubmit() : Boolean
      {
         return this._showSubmit;
      }
      
      public function set showSubmit(value:Boolean) : void
      {
         if(this._showSubmit == value)
         {
            return;
         }
         this._showSubmit = value;
         this.fireChange();
      }
      
      public function get submitLabel() : String
      {
         return this._submitLabel;
      }
      
      public function set submitLabel(value:String) : void
      {
         if(this._submitLabel == value)
         {
            return;
         }
         this._submitLabel = value;
         this.fireChange();
      }
      
      public function get textShowHeight() : int
      {
         return this._textShowHeight;
      }
      
      public function set textShowHeight(value:int) : void
      {
         if(this._textShowHeight == value)
         {
            return;
         }
         this._textShowHeight = value;
         this.fireChange();
      }
      
      public function get textShowWidth() : int
      {
         return this._textShowWidth;
      }
      
      public function set textShowWidth(value:int) : void
      {
         if(this._textShowWidth == value)
         {
            return;
         }
         this._textShowWidth = value;
         this.fireChange();
      }
      
      public function get title() : String
      {
         return this._title;
      }
      
      public function set title(value:String) : void
      {
         if(this._title == value)
         {
            return;
         }
         this._title = value;
         this.fireChange();
      }
      
      public function get sound() : *
      {
         return this._soundID;
      }
      
      public function set sound(soundid:*) : void
      {
         if(this._soundID == soundid)
         {
            return;
         }
         this._soundID = soundid;
         this.fireChange();
      }
      
      private function fireChange() : void
      {
         dispatchEvent(new InteractiveEvent(InteractiveEvent.STATE_CHANGED));
      }
      
      public function get customPos() : Point
      {
         return this._customPos;
      }
      
      public function set customPos(value:Point) : void
      {
         this._customPos = value;
      }
   }
}
