package com.pickgliss.ui
{
   import com.pickgliss.loader.BaseLoader;
   import com.pickgliss.loader.LoaderEvent;
   import com.pickgliss.utils.ClassUtils;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.Shader;
   import flash.filters.ShaderFilter;
   import flash.system.Capabilities;
   import flash.utils.Dictionary;
   
   public final class ComponentModel
   {
       
      
      private var _allTipStyles:Vector.<String>;
      
      private var _bitmapSets:Dictionary;
      
      private var _componentStyle:Dictionary;
      
      private var _currentComponentSet:XML;
      
      private var _customObjectStyle:Dictionary;
      
      private var _loadedComponentSet:Vector.<String>;
      
      private var _shaderData:Dictionary;
      
      private var _styleSets:Dictionary;
      
      private var _allComponentSet:Dictionary;
      
      public function ComponentModel()
      {
         super();
         this._componentStyle = new Dictionary();
         this._customObjectStyle = new Dictionary();
         this._styleSets = new Dictionary();
         this._allTipStyles = new Vector.<String>();
         this._shaderData = new Dictionary();
         this._bitmapSets = new Dictionary();
         this._loadedComponentSet = new Vector.<String>();
         if(Capabilities.isDebugger)
         {
            this._allComponentSet = new Dictionary();
         }
         XML.ignoreComments = XML.prettyPrinting = XML.ignoreWhitespace = XML.ignoreProcessingInstructions = false;
         XML.prettyIndent = 0;
      }
      
      public function get allComponentStyle() : Dictionary
      {
         return this._componentStyle;
      }
      
      public function addBitmapSet(classpath:String, tagData:XML) : void
      {
         this._bitmapSets[classpath] = tagData;
      }
      
      public function addComponentSet(componentSet:XML) : void
      {
         if(this._loadedComponentSet.indexOf(String(componentSet.@name)) != -1)
         {
            return;
         }
         this._currentComponentSet = componentSet;
         this._loadedComponentSet.push(String(this._currentComponentSet.@name));
         if(Capabilities.isDebugger)
         {
            this._allComponentSet[String(componentSet.@name)] = componentSet;
         }
         this.paras();
      }
      
      public function get allTipStyles() : Vector.<String>
      {
         return this._allTipStyles;
      }
      
      public function getBitmapSet(classpath:String) : XML
      {
         return this._bitmapSets[classpath];
      }
      
      public function getComonentStyle(stylename:String) : XML
      {
         return this._componentStyle[stylename];
      }
      
      public function getCustomObjectStyle(stylename:String) : XML
      {
         return this._customObjectStyle[stylename];
      }
      
      public function getSet(key:String) : *
      {
         return this._styleSets[key];
      }
      
      private function __onShaderLoadComplete(event:LoaderEvent) : void
      {
         var para:Array = null;
         var shaderDataObject:Object = this.findShaderDataByLoader(event.loader);
         var shader:Shader = new Shader(shaderDataObject.loader.content);
         var shaderFilter:ShaderFilter = new ShaderFilter(shader);
         var shaderParas:Array = ComponentFactory.parasArgs(shaderDataObject.xml.@paras);
         for(var i:int = 0; i < shaderParas.length; i++)
         {
            para = String(shaderParas[i]).split(":");
            if(shaderFilter.shader.data.hasOwnProperty(para[0]))
            {
               shaderFilter.shader.data[para[0]].value = [int(para[1])];
            }
         }
         this._styleSets[String(shaderDataObject.xml.@shadername)] = shaderFilter;
      }
      
      private function findShaderDataByLoader(loader:BaseLoader) : Object
      {
         var shaderDataObject:Object = null;
         for each(shaderDataObject in this._shaderData)
         {
            if(shaderDataObject.loader == loader)
            {
               return shaderDataObject;
            }
         }
         return null;
      }
      
      private function paras() : void
      {
         this.parasSets(this._currentComponentSet..set);
         this.parasComponent(this._currentComponentSet..component);
         this.parasCustomObject(this._currentComponentSet..custom);
         if(this._currentComponentSet.hasOwnProperty("tips"))
         {
            this.parasTipStyle(this._currentComponentSet.tips);
         }
         if(this._currentComponentSet.shaderFilters.length() > 0)
         {
         }
         this.parasBitmapDataSets(this._currentComponentSet..bitmapData);
         this.parasBitmapSets(this._currentComponentSet..bitmap);
      }
      
      private function parasBitmapDataSets(list:XMLList) : void
      {
         for(var i:int = 0; i < list.length(); i++)
         {
            this._bitmapSets[String(list[i].@resourceLink)] = list[i];
         }
      }
      
      private function parasBitmapSets(list:XMLList) : void
      {
         for(var i:int = 0; i < list.length(); i++)
         {
            this._bitmapSets[String(list[i].@resourceLink)] = list[i];
         }
      }
      
      private function parasComponent(list:XMLList) : void
      {
         var error:Error = null;
         for(var i:int = 0; i < list.length(); i++)
         {
            if(this._componentStyle[String(list[i].@stylename)] != null)
            {
               error = new Error("样式重名。。。请注意检查!!!!!!!!" + String(list[i].@stylename));
               throw error;
            }
            list[i].@componentModule = this._currentComponentSet.@name;
            this._componentStyle[String(list[i].@stylename)] = list[i];
         }
      }
      
      private function parasCustomObject(list:XMLList) : void
      {
         var error:Error = null;
         for(var i:int = 0; i < list.length(); i++)
         {
            if(this._customObjectStyle[String(list[i].@stylename)] != null)
            {
               error = new Error("样式重名。。。请注意检查!!!!!!!!" + String(list[i].@stylename));
               throw error;
            }
            this._customObjectStyle[String(list[i].@stylename)] = list[i];
         }
      }
      
      private function parasSets(list:XMLList) : void
      {
         for(var i:int = 0; i < list.length(); i++)
         {
            if(String(list[i].@type) == ClassUtils.COLOR_MATIX_FILTER)
            {
               this._styleSets[String(list[i].@stylename)] = ClassUtils.CreatInstance(list[i].@type,[ComponentFactory.parasArgs(list[i].@args)]);
            }
            else
            {
               this._styleSets[String(list[i].@stylename)] = ClassUtils.CreatInstance(list[i].@type,ComponentFactory.parasArgs(list[i].@args));
            }
            ObjectUtils.copyPorpertiesByXML(this._styleSets[String(list[i].@stylename)],list[i]);
         }
      }
      
      private function parasTipStyle(styles:XMLList) : void
      {
         var allStyles:Array = String(styles[0].@alltips).split(",");
         for(var i:int = 0; i < allStyles.length; i++)
         {
            if(this._allTipStyles.indexOf(allStyles[i]) == -1)
            {
               this._allTipStyles.push(allStyles[i]);
            }
         }
      }
      
      public function get allComponentSet() : Dictionary
      {
         return this._allComponentSet;
      }
      
      public function set allComponentSet(value:Dictionary) : void
      {
         this._allComponentSet = value;
      }
   }
}
