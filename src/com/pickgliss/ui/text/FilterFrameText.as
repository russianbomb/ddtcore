package com.pickgliss.ui.text
{
   import com.pickgliss.toplevel.StageReferance;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Disposeable;
   import flash.text.TextField;
   import flash.text.TextFieldAutoSize;
   import flash.text.TextFieldType;
   import flash.text.TextFormat;
   
   public class FilterFrameText extends TextField implements Disposeable
   {
      
      public static const P_frameFilters:String = "frameFilters";
      
      public static var isInputChangeWmode:Boolean = true;
       
      
      protected var _currentFrameIndex:int;
      
      protected var _filterString:String;
      
      protected var _frameFilter:Array;
      
      protected var _frameTextFormat:Vector.<TextFormat>;
      
      protected var _id:int;
      
      protected var _width:Number;
      
      protected var _height:Number;
      
      protected var _isAutoFitLength:Boolean = false;
      
      public var stylename:String;
      
      protected var _textFormatStyle:String;
      
      public function FilterFrameText()
      {
         super();
         autoSize = TextFieldAutoSize.LEFT;
         mouseEnabled = false;
         this._currentFrameIndex = 1;
         this._frameTextFormat = new Vector.<TextFormat>();
      }
      
      public function dispose() : void
      {
         this._frameFilter = null;
         if(parent)
         {
            parent.removeChild(this);
         }
         ComponentFactory.Instance.removeComponent(this._id);
      }
      
      public function set filterString(filters:String) : void
      {
         if(this._filterString == filters)
         {
            return;
         }
         this._filterString = filters;
         this.frameFilters = ComponentFactory.Instance.creatFrameFilters(this._filterString);
         this.setFrame(1);
      }
      
      public function set frameFilters(filter:Array) : void
      {
         if(this._frameFilter == filter)
         {
            return;
         }
         this._frameFilter = filter;
      }
      
      public function get id() : int
      {
         return this._id;
      }
      
      public function set id(value:int) : void
      {
         this._id = value;
      }
      
      public function setFocus() : void
      {
         StageReferance.stage.focus = this;
      }
      
      public function setFrame(frameIndex:int) : void
      {
         this._currentFrameIndex = frameIndex;
         if(this._frameFilter != null && this._frameFilter.length >= frameIndex)
         {
            filters = this._frameFilter[frameIndex - 1];
         }
         if(this._frameTextFormat != null && this._frameTextFormat.length >= frameIndex)
         {
            this.textFormat = this._frameTextFormat[frameIndex - 1];
         }
      }
      
      protected function set textFormat(tf:TextFormat) : void
      {
         setTextFormat(tf);
         defaultTextFormat = tf;
      }
      
      public function set textFormatStyle(stylename:String) : void
      {
         var format:TextFormat = null;
         if(this._textFormatStyle == stylename)
         {
            return;
         }
         this._textFormatStyle = stylename;
         var frameTextFormats:Array = ComponentFactory.parasArgs(this._textFormatStyle);
         this._frameTextFormat = new Vector.<TextFormat>();
         for(var i:int = 0; i < frameTextFormats.length; i++)
         {
            format = ComponentFactory.Instance.model.getSet(frameTextFormats[i]);
            this._frameTextFormat.push(format);
         }
         this.setFrame(1);
      }
      
      public function set isAutoFitLength(value:Boolean) : void
      {
         this._isAutoFitLength = value;
      }
      
      override public function set visible(value:Boolean) : void
      {
         super.visible = value;
      }
      
      override public function set text(value:String) : void
      {
         var tempIndex:int = 0;
         super.text = value;
         if(this._isAutoFitLength && textWidth > width)
         {
            tempIndex = getCharIndexAtPoint(width - 22,5);
            super.text = text.substring(0,tempIndex) + "...";
         }
         if(this._width > 0 || this._height > 0)
         {
            if(wordWrap == true)
            {
               this.width = this._width;
            }
         }
         this.setFrame(this._currentFrameIndex);
      }
      
      override public function set htmlText(value:String) : void
      {
         if(this._width > 0 || this._height > 0)
         {
            if(wordWrap == true)
            {
               this.width = this._width;
            }
         }
         this.setFrame(this._currentFrameIndex);
         super.htmlText = value;
      }
      
      override public function set width(value:Number) : void
      {
         this._width = value;
         super.width = value;
      }
      
      override public function set height(value:Number) : void
      {
         this._height = value;
         super.height = value;
      }
      
      override public function set type(value:String) : void
      {
         if(value == TextFieldType.INPUT)
         {
            mouseEnabled = true;
         }
         super.type = value;
      }
   }
}
