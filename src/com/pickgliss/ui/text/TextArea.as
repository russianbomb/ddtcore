package com.pickgliss.ui.text
{
   import com.pickgliss.geom.IntPoint;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.DisplayObjectViewport;
   import com.pickgliss.ui.controls.ScrollPanel;
   import com.pickgliss.utils.DisplayUtils;
   import com.pickgliss.utils.ObjectUtils;
   import flash.events.Event;
   import flash.events.KeyboardEvent;
   import flash.events.MouseEvent;
   import flash.geom.Rectangle;
   import flash.text.TextFieldType;
   import flash.ui.Keyboard;
   import flash.ui.Mouse;
   import flash.ui.MouseCursor;
   
   public class TextArea extends ScrollPanel
   {
      
      public static const P_textField:String = "textField";
       
      
      protected var _currentTextHeight:int = 0;
      
      protected var _enable:Boolean = true;
      
      protected var _textField:FilterFrameText;
      
      protected var _textStyle:String;
      
      public function TextArea()
      {
         super();
         _viewSource.addEventListener(MouseEvent.CLICK,this.__onTextAreaClick);
         _viewSource.addEventListener(MouseEvent.MOUSE_OVER,this.__onTextAreaOver);
         _viewSource.addEventListener(MouseEvent.MOUSE_OUT,this.__onTextAreaOut);
      }
      
      override public function dispose() : void
      {
         Mouse.cursor = MouseCursor.AUTO;
         _viewSource.removeEventListener(MouseEvent.CLICK,this.__onTextAreaClick);
         _viewSource.removeEventListener(MouseEvent.MOUSE_OVER,this.__onTextAreaOver);
         _viewSource.removeEventListener(MouseEvent.MOUSE_OUT,this.__onTextAreaOut);
         this._textField.removeEventListener(KeyboardEvent.KEY_DOWN,this.__onTextKeyDown);
         this._textField.removeEventListener(Event.CHANGE,this.__onTextChanged);
         ObjectUtils.disposeObject(this._textField);
         this._textField = null;
         super.dispose();
      }
      
      public function get editable() : Boolean
      {
         return this._textField.type == TextFieldType.INPUT;
      }
      
      public function set editable(value:Boolean) : void
      {
         if(value)
         {
            this._textField.type = TextFieldType.INPUT;
            _viewSource.addEventListener(MouseEvent.MOUSE_OVER,this.__onTextAreaOver);
            _viewSource.addEventListener(MouseEvent.MOUSE_OUT,this.__onTextAreaOut);
         }
         else
         {
            this._textField.type = TextFieldType.DYNAMIC;
            _viewSource.removeEventListener(MouseEvent.MOUSE_OVER,this.__onTextAreaOver);
            _viewSource.removeEventListener(MouseEvent.MOUSE_OUT,this.__onTextAreaOut);
         }
      }
      
      public function get enable() : Boolean
      {
         return this._enable;
      }
      
      public function set enable(value:Boolean) : void
      {
         this._textField.mouseEnabled = this._enable;
         if(this._enable)
         {
            _viewSource.addEventListener(MouseEvent.MOUSE_OVER,this.__onTextAreaOver);
            _viewSource.addEventListener(MouseEvent.MOUSE_OUT,this.__onTextAreaOut);
         }
         else
         {
            _viewSource.removeEventListener(MouseEvent.MOUSE_OVER,this.__onTextAreaOver);
            _viewSource.removeEventListener(MouseEvent.MOUSE_OUT,this.__onTextAreaOut);
         }
      }
      
      public function get maxChars() : int
      {
         return this._textField.maxChars;
      }
      
      public function set maxChars(value:int) : void
      {
         this._textField.maxChars = value;
      }
      
      public function get text() : String
      {
         return this._textField.text;
      }
      
      public function set text(value:String) : void
      {
         this._textField.text = value;
         DisplayObjectViewport(_viewSource).invalidateView();
      }
      
      public function set htmlText(value:String) : void
      {
         this._textField.htmlText = value;
         DisplayObjectViewport(_viewSource).invalidateView();
      }
      
      public function get htmlText() : String
      {
         return this._textField.htmlText;
      }
      
      public function get textField() : FilterFrameText
      {
         return this._textField;
      }
      
      public function set textField(tf:FilterFrameText) : void
      {
         if(this._textField == tf)
         {
            return;
         }
         if(this._textField)
         {
            this._textField.removeEventListener(Event.CHANGE,this.__onTextChanged);
            this._textField.removeEventListener(KeyboardEvent.KEY_DOWN,this.__onTextKeyDown);
            ObjectUtils.disposeObject(this._textField);
         }
         this._textField = tf;
         this._textField.multiline = true;
         this._textField.mouseWheelEnabled = false;
         this._textField.addEventListener(KeyboardEvent.KEY_DOWN,this.__onTextKeyDown);
         this._textField.addEventListener(Event.CHANGE,this.__onTextChanged);
         onPropertiesChanged(P_textField);
      }
      
      public function set textStyle(stylename:String) : void
      {
         if(this._textStyle == stylename)
         {
            return;
         }
         this._textStyle = stylename;
         this.textField = ComponentFactory.Instance.creat(this._textStyle);
      }
      
      override protected function layoutComponent() : void
      {
         var texFieldRect:Rectangle = null;
         super.layoutComponent();
         texFieldRect = _viewportInnerRect.getInnerRect(_width,_height);
         this._textField.x = texFieldRect.x;
         this._textField.y = texFieldRect.y;
         this._textField.width = _viewSource.width;
      }
      
      override protected function onProppertiesUpdate() : void
      {
         super.onProppertiesUpdate();
         if(_changedPropeties[P_textField])
         {
            DisplayObjectViewport(_viewSource).setView(this._textField);
         }
      }
      
      private function __onTextAreaClick(event:MouseEvent) : void
      {
         this._textField.setFocus();
      }
      
      private function __onTextAreaOut(event:MouseEvent) : void
      {
         Mouse.cursor = MouseCursor.AUTO;
      }
      
      private function __onTextAreaOver(event:MouseEvent) : void
      {
         Mouse.cursor = MouseCursor.IBEAM;
      }
      
      private function __onTextChanged(event:Event) : void
      {
         this.upScrollArea();
      }
      
      private function __onTextKeyDown(event:KeyboardEvent) : void
      {
         if(event.keyCode == Keyboard.ENTER)
         {
            event.stopPropagation();
         }
         else if(event.keyCode == Keyboard.UP)
         {
            this.upScrollArea();
         }
         else if(event.keyCode == Keyboard.DOWN)
         {
            this.upScrollArea();
         }
         else if(event.keyCode == Keyboard.DELETE)
         {
            this.upScrollArea();
         }
      }
      
      public function upScrollArea() : void
      {
         var lineHeight:Number = NaN;
         var currentPos:IntPoint = null;
         var careX:Number = NaN;
         var careY:Number = NaN;
         var offsetX:Number = NaN;
         var offsetY:Number = NaN;
         var resultX:Number = NaN;
         var resultY:Number = NaN;
         DisplayObjectViewport(_viewSource).invalidateView();
         if(this._textField.caretIndex <= 0)
         {
            viewPort.viewPosition = new IntPoint(0,0);
         }
         else
         {
            lineHeight = DisplayUtils.getTextFieldLineHeight(this._textField);
            currentPos = viewPort.viewPosition;
            careX = DisplayUtils.getTextFieldCareLinePosX(this._textField);
            careY = DisplayUtils.getTextFieldCareLinePosY(this._textField);
            offsetX = careX - currentPos.x;
            offsetY = careY + lineHeight - currentPos.y;
            DisplayObjectViewport(_viewSource).invalidateView();
            resultX = currentPos.x;
            resultY = currentPos.y;
            if(offsetX < 0)
            {
               resultX = careX;
            }
            else if(offsetX > viewPort.getExtentSize().width)
            {
               resultX = careX + viewPort.getExtentSize().width;
            }
            if(offsetY < lineHeight)
            {
               resultY = careY;
            }
            else if(offsetY > viewPort.getExtentSize().height)
            {
               resultY = careY + viewPort.getExtentSize().height;
            }
            if(resultX > 0 || resultY > 0)
            {
               viewPort.viewPosition = new IntPoint(resultX,resultY);
            }
         }
      }
   }
}
