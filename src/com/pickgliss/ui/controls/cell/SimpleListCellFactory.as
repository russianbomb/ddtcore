package com.pickgliss.ui.controls.cell
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.utils.StringUtils;
   
   public class SimpleListCellFactory implements IListCellFactory
   {
       
      
      private var _ViewWidthNoCount:int;
      
      private var _allCellHasSameHeight:Boolean;
      
      private var _cellHeight:int;
      
      private var _cellStyle:String;
      
      private var _shareCells:Boolean;
      
      public function SimpleListCellFactory(cellStyle:String, cellHeight:int, viewWidthNoCount:int = -1, allCellHasSameHeight:String = "true", shareCells:String = "true")
      {
         super();
         this._cellStyle = cellStyle;
         this._allCellHasSameHeight = StringUtils.converBoolean(allCellHasSameHeight);
         this._shareCells = StringUtils.converBoolean(shareCells);
         this._cellHeight = cellHeight;
         this._ViewWidthNoCount = viewWidthNoCount;
      }
      
      public function createNewCell() : IListCell
      {
         var cell:IListCell = ComponentFactory.Instance.creat(this._cellStyle);
         return cell;
      }
      
      public function getCellHeight() : int
      {
         return this._cellHeight;
      }
      
      public function getViewWidthNoCount() : int
      {
         return this._ViewWidthNoCount;
      }
      
      public function isAllCellHasSameHeight() : Boolean
      {
         return this._allCellHasSameHeight;
      }
      
      public function isShareCells() : Boolean
      {
         return this._shareCells;
      }
   }
}
