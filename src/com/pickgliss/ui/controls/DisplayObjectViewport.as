package com.pickgliss.ui.controls
{
   import com.pickgliss.events.ComponentEvent;
   import com.pickgliss.events.InteractiveEvent;
   import com.pickgliss.geom.IntDimension;
   import com.pickgliss.geom.IntPoint;
   import com.pickgliss.geom.IntRectangle;
   import com.pickgliss.ui.ComponentSetting;
   import com.pickgliss.ui.core.Component;
   import com.pickgliss.ui.core.IViewprot;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.DisplayObject;
   import flash.display.Shape;
   
   public class DisplayObjectViewport extends Component implements IViewprot
   {
      
      public static const P_horizontalBlockIncrement:String = "horizontalBlockIncrement";
      
      public static const P_horizontalUnitIncrement:String = "horizontalUnitIncrement";
      
      public static const P_verticalBlockIncrement:String = "verticalBlockIncrement";
      
      public static const P_verticalUnitIncrement:String = "verticalUnitIncrement";
      
      public static const P_view:String = "view";
      
      public static const P_viewPosition:String = "viewPosition";
       
      
      protected var _horizontalBlockIncrement:int;
      
      protected var _horizontalUnitIncrement:int;
      
      protected var _maskShape:Shape;
      
      protected var _mouseActiveObjectShape:Shape;
      
      protected var _verticalBlockIncrement:int;
      
      protected var _verticalUnitIncrement:int;
      
      protected var _viewHeight:int;
      
      protected var _viewPosition:IntPoint;
      
      protected var _viewWidth:int;
      
      private var _view:DisplayObject;
      
      public function DisplayObjectViewport()
      {
         this._horizontalBlockIncrement = ComponentSetting.SCROLL_BLOCK_INCREMENT;
         this._horizontalUnitIncrement = ComponentSetting.SCROLL_UINT_INCREMENT;
         this._verticalBlockIncrement = ComponentSetting.SCROLL_BLOCK_INCREMENT;
         this._verticalUnitIncrement = ComponentSetting.SCROLL_UINT_INCREMENT;
         super();
      }
      
      public function addStateListener(listener:Function, priority:int = 0, useWeakReference:Boolean = false) : void
      {
         addEventListener(InteractiveEvent.STATE_CHANGED,listener);
      }
      
      override public function dispose() : void
      {
         if(this._view is Component)
         {
            Component(this._view).removeEventListener(ComponentEvent.PROPERTIES_CHANGED,this.__onResize);
         }
         ObjectUtils.disposeObject(this._view);
         this._view = null;
         if(this._mouseActiveObjectShape)
         {
            ObjectUtils.disposeObject(this._mouseActiveObjectShape);
         }
         this._mouseActiveObjectShape = null;
         if(this._maskShape)
         {
            ObjectUtils.disposeObject(this._maskShape);
         }
         this._maskShape = null;
         super.dispose();
      }
      
      public function getExtentSize() : IntDimension
      {
         return new IntDimension(_width,_height);
      }
      
      public function getViewSize() : IntDimension
      {
         return new IntDimension(this._viewWidth,this._viewHeight);
      }
      
      public function getViewportPane() : Component
      {
         return this;
      }
      
      public function get horizontalBlockIncrement() : int
      {
         return this._horizontalBlockIncrement;
      }
      
      public function set horizontalBlockIncrement(increment:int) : void
      {
         if(this._horizontalBlockIncrement == increment)
         {
            return;
         }
         this._horizontalBlockIncrement = increment;
         onPropertiesChanged(P_horizontalBlockIncrement);
      }
      
      public function get horizontalUnitIncrement() : int
      {
         return this._horizontalUnitIncrement;
      }
      
      public function set horizontalUnitIncrement(increment:int) : void
      {
         if(this._horizontalUnitIncrement == increment)
         {
            return;
         }
         this._horizontalUnitIncrement = increment;
         onPropertiesChanged(P_horizontalUnitIncrement);
      }
      
      public function removeStateListener(listener:Function) : void
      {
         removeEventListener(InteractiveEvent.STATE_CHANGED,listener);
      }
      
      public function scrollRectToVisible(contentRect:IntRectangle) : void
      {
         this.viewPosition = new IntPoint(contentRect.x,contentRect.y);
      }
      
      public function setView(view:DisplayObject) : void
      {
         if(this._view == view)
         {
            return;
         }
         if(this._view is Component)
         {
            Component(this._view).removeEventListener(ComponentEvent.PROPERTIES_CHANGED,this.__onResize);
         }
         if(this._view)
         {
            ObjectUtils.disposeObject(this._view);
         }
         this._view = view;
         if(this._view is Component)
         {
            Component(this._view).addEventListener(ComponentEvent.PROPERTIES_CHANGED,this.__onResize);
         }
         onPropertiesChanged(P_view);
      }
      
      public function setViewportTestSize(s:IntDimension) : void
      {
      }
      
      public function get verticalBlockIncrement() : int
      {
         return this._verticalBlockIncrement;
      }
      
      public function set verticalBlockIncrement(increment:int) : void
      {
         if(this._verticalBlockIncrement == increment)
         {
            return;
         }
         this._verticalBlockIncrement = increment;
         onPropertiesChanged(P_verticalBlockIncrement);
      }
      
      public function get verticalUnitIncrement() : int
      {
         return this._verticalUnitIncrement;
      }
      
      public function set verticalUnitIncrement(increment:int) : void
      {
         if(this._verticalUnitIncrement == increment)
         {
            return;
         }
         this._verticalUnitIncrement = increment;
         onPropertiesChanged(P_verticalUnitIncrement);
      }
      
      public function get viewPosition() : IntPoint
      {
         return this._viewPosition;
      }
      
      public function set viewPosition(p:IntPoint) : void
      {
         if(this._viewPosition.equals(p))
         {
            return;
         }
         this._viewPosition.setLocation(p);
         onPropertiesChanged(P_viewPosition);
      }
      
      public function invalidateView() : void
      {
         onPropertiesChanged(P_view);
      }
      
      override protected function addChildren() : void
      {
         super.addChildren();
         addChild(this._mouseActiveObjectShape);
         addChild(this._maskShape);
      }
      
      protected function creatMaskShape() : void
      {
         this._maskShape = new Shape();
         this._maskShape.graphics.beginFill(16711680,1);
         this._maskShape.graphics.drawRect(0,0,100,100);
         this._maskShape.graphics.endFill();
         this._mouseActiveObjectShape = new Shape();
         this._mouseActiveObjectShape.graphics.beginFill(16711680,0);
         this._mouseActiveObjectShape.graphics.drawRect(0,0,100,100);
         this._mouseActiveObjectShape.graphics.endFill();
      }
      
      protected function fireStateChanged(programmatic:Boolean = true) : void
      {
         dispatchEvent(new InteractiveEvent(InteractiveEvent.STATE_CHANGED));
      }
      
      protected function getViewMaxPos() : IntPoint
      {
         var showSize:IntDimension = this.getExtentSize();
         var viewSize:IntDimension = this.getViewSize();
         var p:IntPoint = new IntPoint(viewSize.width - showSize.width,viewSize.height - showSize.height);
         if(p.x < 0)
         {
            p.x = 0;
         }
         if(p.y < 0)
         {
            p.y = 0;
         }
         return p;
      }
      
      override protected function init() : void
      {
         this.creatMaskShape();
         this._viewPosition = new IntPoint(0,0);
         super.init();
      }
      
      override protected function onProppertiesUpdate() : void
      {
         super.onProppertiesUpdate();
         if(_changedPropeties[Component.P_height] || _changedPropeties[Component.P_width])
         {
            this.updateShowMask();
         }
         if(_changedPropeties[P_view] || _changedPropeties[P_viewPosition])
         {
            this._viewWidth = this._view.width;
            this._viewHeight = this._view.height;
            addChild(this._view);
            this._view.mask = this._maskShape;
            this.updatePos();
            this.fireStateChanged();
         }
      }
      
      protected function restrictionViewPos(p:IntPoint) : IntPoint
      {
         var maxPos:IntPoint = this.getViewMaxPos();
         p.x = Math.max(0,Math.min(maxPos.x,p.x));
         p.y = Math.max(0,Math.min(maxPos.y,p.y));
         return p;
      }
      
      protected function updatePos() : void
      {
         this.restrictionViewPos(this._viewPosition);
         this._view.x = -this._viewPosition.x;
         this._view.y = -this._viewPosition.y;
      }
      
      protected function updateShowMask() : void
      {
         this._mouseActiveObjectShape.width = this._maskShape.width = _width;
         this._mouseActiveObjectShape.height = this._maskShape.height = _height;
      }
      
      private function __onResize(event:ComponentEvent) : void
      {
         if(event.changedProperties[Component.P_height] || event.changedProperties[Component.P_width])
         {
            onPropertiesChanged(P_view);
         }
      }
   }
}
