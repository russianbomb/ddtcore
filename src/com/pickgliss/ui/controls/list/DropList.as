package com.pickgliss.ui.controls.list
{
   import com.pickgliss.toplevel.StageReferance;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.ComponentSetting;
   import com.pickgliss.ui.controls.cell.IDropListCell;
   import com.pickgliss.ui.controls.container.BoxContainer;
   import com.pickgliss.ui.core.Component;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.DisplayObject;
   import flash.display.InteractiveObject;
   import flash.events.Event;
   import flash.events.KeyboardEvent;
   import flash.events.MouseEvent;
   import flash.ui.Keyboard;
   
   public class DropList extends Component implements Disposeable
   {
      
      public static const SELECTED:String = "selected";
      
      public static const P_backgound:String = "backgound";
      
      public static const P_container:String = "container";
       
      
      private var _backStyle:String;
      
      private var _backGround:DisplayObject;
      
      private var _cellStyle:String;
      
      private var _containerStyle:String;
      
      private var _container:BoxContainer;
      
      private var _targetDisplay:IDropListTarget;
      
      private var _showLength:int;
      
      private var _dataList:Array;
      
      private var _items:Vector.<IDropListCell>;
      
      private var _currentSelectedIndex:int;
      
      private var _preItemIdx:int;
      
      private var _cellHeight:int;
      
      private var _cellWidth:int;
      
      private var _isListening:Boolean;
      
      private var _canUseEnter:Boolean = true;
      
      public function DropList()
      {
         super();
      }
      
      override protected function init() : void
      {
         this._items = new Vector.<IDropListCell>();
      }
      
      public function set container(value:BoxContainer) : void
      {
         if(this._container)
         {
            ObjectUtils.disposeObject(this._container);
            this._container = null;
         }
         this._container = value;
         onPropertiesChanged(P_container);
      }
      
      public function set containerStyle(value:String) : void
      {
         if(this._containerStyle == value)
         {
            return;
         }
         this._containerStyle = value;
         if(this._container)
         {
            ObjectUtils.disposeObject(this._container);
            this._container = null;
         }
         this.container = ComponentFactory.Instance.creat(this._containerStyle);
      }
      
      public function set cellStyle(value:String) : void
      {
         if(this._cellStyle == value)
         {
            return;
         }
         this._cellStyle = value;
      }
      
      public function set dataList(value:Array) : void
      {
         if(!value)
         {
            if(parent)
            {
               parent.removeChild(this);
            }
            return;
         }
         if(this._targetDisplay.parent)
         {
            this._targetDisplay.parent.addChild(this);
         }
         this._dataList = value;
         var len:int = Math.min(this._dataList.length,this._showLength);
         for(var i:int = 0; i < len; i++)
         {
            this._items[i].setCellValue(this._dataList[i]);
            if(!this._container.contains(this._items[i].asDisplayObject()))
            {
               this._container.addChild(this._items[i].asDisplayObject());
            }
         }
         if(len == 0)
         {
            this._items[0].setCellValue(null);
            if(!this._container.contains(this._items[i].asDisplayObject()))
            {
               this._container.addChild(this._items[i].asDisplayObject());
            }
            len = 1;
         }
         for(i = len; i < this._showLength; i++)
         {
            if(this._container.contains(this._items[i].asDisplayObject()))
            {
               this._container.removeChild(this._items[i].asDisplayObject());
            }
         }
         this.updateBg();
         this.unSelectedAllItems();
         this._currentSelectedIndex = 0;
         this._items[this._currentSelectedIndex].selected = true;
      }
      
      private function updateBg() : void
      {
         if(this._container.numChildren == 0)
         {
            if(contains(this._backGround))
            {
               removeChild(this._backGround);
            }
         }
         else
         {
            this._backGround.width = this._cellWidth + 2 * this._container.x;
            this._backGround.height = this._container.numChildren * (this._cellHeight + this._container.spacing) - this._container.spacing + 2 * this._container.y;
            addChildAt(this._backGround,0);
         }
      }
      
      private function getHightLightItemIdx() : int
      {
         for(var i:int = 0; i < this._showLength; i++)
         {
            if(this._items[i].selected)
            {
               return i;
            }
         }
         return 0;
      }
      
      private function unSelectedAllItems() : int
      {
         var idx:int = 0;
         for(var i:int = 0; i < this._showLength; i++)
         {
            if(this._items[i].selected)
            {
               idx = i;
            }
            this._items[i].selected = false;
         }
         return idx;
      }
      
      private function updateItemValue(isUpWard:Boolean = false) : void
      {
         for(var i:int = 0; i < this._showLength; i++)
         {
            this._items[i].setCellValue(this._dataList[this._currentSelectedIndex - this.getHightLightItemIdx() + i]);
         }
      }
      
      private function setHightLightItem(isUpWard:Boolean = false) : void
      {
         var idx:int = 0;
         if(this._dataList.length > 0)
         {
            idx = this.getHightLightItemIdx();
            if(!isUpWard)
            {
               if(idx < this._showLength - 1)
               {
                  this.unSelectedAllItems();
                  idx++;
               }
               else if(idx >= this._showLength - 1)
               {
                  this.updateItemValue();
               }
            }
            if(isUpWard)
            {
               if(idx > 0)
               {
                  this.unSelectedAllItems();
                  idx--;
               }
               else if(idx == 0)
               {
                  this.updateItemValue(true);
               }
            }
            this._items[idx].selected = true;
         }
         else
         {
            this._currentSelectedIndex = 0;
         }
         this.setTargetValue();
      }
      
      override protected function addChildren() : void
      {
         super.addChildren();
         if(this._backGround)
         {
            addChild(this._backGround);
         }
         if(this._container)
         {
            addChild(this._container);
         }
      }
      
      public function set targetDisplay(target:IDropListTarget) : void
      {
         if(target == this._targetDisplay)
         {
            return;
         }
         this._targetDisplay = target;
         this._targetDisplay.addEventListener(KeyboardEvent.KEY_DOWN,this.__onKeyDown);
         this._targetDisplay.addEventListener(Event.REMOVED_FROM_STAGE,this.__onRemoveFromStage);
      }
      
      private function __onRemoveFromStage(event:Event) : void
      {
         if(parent)
         {
            parent.removeChild(this);
         }
      }
      
      public function set showLength(value:int) : void
      {
         var cell:* = undefined;
         if(this._showLength == value)
         {
            return;
         }
         this._showLength = value;
         while(this._container.numChildren > this._showLength)
         {
            this._container.removeChild(this._items.pop());
         }
         while(this._container.numChildren < this._showLength)
         {
            if(this._items.length > this._container.numChildren)
            {
               this._container.addChild(this._items[this._container.numChildren].asDisplayObject());
            }
            else
            {
               cell = ComponentFactory.Instance.creat(this._cellStyle);
               cell.addEventListener(MouseEvent.MOUSE_OVER,this.__onCellMouseOver);
               cell.addEventListener(MouseEvent.CLICK,this.__onCellMouseClick);
               this._items.push(cell);
               this._container.addChild(cell);
            }
         }
         this._cellHeight = cell.height;
         this._cellWidth = cell.width;
         this.updateBg();
      }
      
      private function __onCellMouseClick(event:MouseEvent) : void
      {
         ComponentSetting.PLAY_SOUND_FUNC("008");
         this.setTargetValue();
         if(parent)
         {
            parent.removeChild(this);
         }
         dispatchEvent(new Event(SELECTED));
      }
      
      private function __onCellMouseOver(event:MouseEvent) : void
      {
         var preIdx:int = this.unSelectedAllItems();
         var newIdx:int = this._items.indexOf(event.currentTarget);
         this._currentSelectedIndex = this._currentSelectedIndex + (newIdx - preIdx);
         event.currentTarget.selected = true;
      }
      
      override protected function onProppertiesUpdate() : void
      {
         super.onProppertiesUpdate();
         if(_changedPropeties[P_backgound] || _changedPropeties[P_container])
         {
            this.addChildren();
         }
      }
      
      private function __onKeyDown(event:KeyboardEvent) : void
      {
         if(this._dataList == null)
         {
            return;
         }
         event.stopImmediatePropagation();
         event.stopPropagation();
         if(!this._isListening && event.keyCode == Keyboard.ENTER)
         {
            this._isListening = true;
            StageReferance.stage.addEventListener(Event.ENTER_FRAME,this.__setSelection);
         }
         switch(event.keyCode)
         {
            case Keyboard.UP:
               ComponentSetting.PLAY_SOUND_FUNC("008");
               if(this._currentSelectedIndex == 0)
               {
                  return;
               }
               this._currentSelectedIndex--;
               this.setHightLightItem(true);
               break;
            case Keyboard.DOWN:
               ComponentSetting.PLAY_SOUND_FUNC("008");
               if(this._currentSelectedIndex == this._dataList.length - 1)
               {
                  return;
               }
               this._currentSelectedIndex++;
               this.setHightLightItem();
               break;
            case Keyboard.ENTER:
               if(this._canUseEnter == false)
               {
                  return;
               }
               ComponentSetting.PLAY_SOUND_FUNC("008");
               if(parent)
               {
                  parent.removeChild(this);
               }
               this._targetDisplay.setValue(this._dataList[this._currentSelectedIndex]);
               dispatchEvent(new Event(SELECTED));
               break;
         }
      }
      
      public function set canUseEnter(boo:Boolean) : void
      {
         this._canUseEnter = boo;
      }
      
      public function get canUseEnter() : Boolean
      {
         return this._canUseEnter;
      }
      
      public function set currentSelectedIndex(idx:int) : void
      {
         if(this._dataList == null)
         {
            return;
         }
         ComponentSetting.PLAY_SOUND_FUNC("008");
         if(this._currentSelectedIndex == this._dataList.length - 1 || this._currentSelectedIndex == 0)
         {
            return;
         }
         this._currentSelectedIndex = this._currentSelectedIndex + idx;
         this.setHightLightItem();
      }
      
      private function setTargetValue() : void
      {
         if(!this._targetDisplay.parent)
         {
            this._targetDisplay.parent.addChild(this);
         }
         if(this._dataList)
         {
            this._targetDisplay.setValue(this._dataList[this._currentSelectedIndex]);
         }
      }
      
      private function __setSelection(event:Event) : void
      {
         if(this._targetDisplay.caretIndex == this._targetDisplay.getValueLength())
         {
            this._isListening = false;
            StageReferance.stage.removeEventListener(Event.ENTER_FRAME,this.__setSelection);
         }
         else
         {
            this._targetDisplay.setCursor(this._targetDisplay.getValueLength());
         }
      }
      
      public function set backStyle(stylename:String) : void
      {
         if(this._backStyle == stylename)
         {
            return;
         }
         this._backStyle = stylename;
         this.backgound = ComponentFactory.Instance.creat(this._backStyle);
      }
      
      public function set backgound(image:DisplayObject) : void
      {
         if(this._backGround == image)
         {
            return;
         }
         ObjectUtils.disposeObject(this._backGround);
         this._backGround = image;
         if(this._backGround is InteractiveObject)
         {
            InteractiveObject(this._backGround).mouseEnabled = true;
         }
         onPropertiesChanged(P_backgound);
      }
      
      override public function dispose() : void
      {
         if(parent)
         {
            parent.removeChild(this);
         }
         StageReferance.stage.removeEventListener(KeyboardEvent.KEY_DOWN,this.__onKeyDown);
         StageReferance.stage.removeEventListener(Event.ENTER_FRAME,this.__setSelection);
         this._targetDisplay.removeEventListener(KeyboardEvent.KEY_DOWN,this.__onKeyDown);
         if(this._backGround)
         {
            ObjectUtils.disposeObject(this._backGround);
         }
         this._backGround = null;
         if(this._container)
         {
            ObjectUtils.disposeObject(this._container);
         }
         this._container = null;
         if(this._targetDisplay)
         {
            ObjectUtils.disposeObject(this._targetDisplay);
         }
         this._targetDisplay = null;
         for(var i:int = 0; i < this._items.length; i++)
         {
            if(this._items[i])
            {
               ObjectUtils.disposeObject(this._items[i]);
            }
            this._items[i] = null;
         }
         this._dataList = null;
         super.dispose();
      }
   }
}
