package com.pickgliss.ui.controls.list
{
   import com.pickgliss.utils.ArrayUtils;
   
   public class BaseListModel
   {
       
      
      private var listeners:Array;
      
      public function BaseListModel()
      {
         super();
         this.listeners = new Array();
      }
      
      public function addListDataListener(l:ListDataListener) : void
      {
         this.listeners.push(l);
      }
      
      public function removeListDataListener(l:ListDataListener) : void
      {
         ArrayUtils.removeFromArray(this.listeners,l);
      }
      
      protected function fireContentsChanged(target:Object, index0:int, index1:int, removedItems:Array) : void
      {
         var lis:ListDataListener = null;
         var e:ListDataEvent = new ListDataEvent(target,index0,index1,removedItems);
         for(var i:int = this.listeners.length - 1; i >= 0; i--)
         {
            lis = ListDataListener(this.listeners[i]);
            lis.contentsChanged(e);
         }
      }
      
      protected function fireIntervalAdded(target:Object, index0:int, index1:int) : void
      {
         var lis:ListDataListener = null;
         var e:ListDataEvent = new ListDataEvent(target,index0,index1,[]);
         for(var i:int = this.listeners.length - 1; i >= 0; i--)
         {
            lis = ListDataListener(this.listeners[i]);
            lis.intervalAdded(e);
         }
      }
      
      protected function fireIntervalRemoved(target:Object, index0:int, index1:int, removedItems:Array) : void
      {
         var lis:ListDataListener = null;
         var e:ListDataEvent = new ListDataEvent(target,index0,index1,removedItems);
         for(var i:int = this.listeners.length - 1; i >= 0; i--)
         {
            lis = ListDataListener(this.listeners[i]);
            lis.intervalRemoved(e);
         }
      }
   }
}
