package com.pickgliss.ui.controls.list
{
   import com.pickgliss.ui.controls.cell.INotSameHeightListCellData;
   import com.pickgliss.utils.IListData;
   
   public class VectorListModel extends BaseListModel implements IMutableListModel, IListData
   {
      
      public static const CASEINSENSITIVE:int = 1;
      
      public static const DESCENDING:int = 2;
      
      public static const NUMERIC:int = 16;
      
      public static const RETURNINDEXEDARRAY:int = 8;
      
      public static const UNIQUESORT:int = 4;
       
      
      protected var _elements:Array;
      
      public function VectorListModel(initalData:Array = null)
      {
         super();
         if(initalData != null)
         {
            this._elements = initalData.concat();
         }
         else
         {
            this._elements = new Array();
         }
      }
      
      public function append(obj:*, index:int = -1) : void
      {
         if(index == -1)
         {
            index = this._elements.length;
            this._elements.push(obj);
         }
         else
         {
            this._elements.splice(index,0,obj);
         }
         fireIntervalAdded(this,index,index);
      }
      
      public function appendAll(arr:Array, index:int = -1) : void
      {
         var right:Array = null;
         if(arr == null || arr.length <= 0)
         {
            return;
         }
         if(index == -1)
         {
            index = this._elements.length;
         }
         if(index == 0)
         {
            this._elements = arr.concat(this._elements);
         }
         else if(index == this._elements.length)
         {
            this._elements = this._elements.concat(arr);
         }
         else
         {
            right = this._elements.splice(index);
            this._elements = this._elements.concat(arr);
            this._elements = this._elements.concat(right);
         }
         fireIntervalAdded(this,index,index + arr.length - 1);
      }
      
      public function appendList(list:IListData, index:int = -1) : void
      {
         this.appendAll(list.toArray(),index);
      }
      
      public function clear() : void
      {
         var temp:Array = null;
         var ei:int = this.size() - 1;
         if(ei >= 0)
         {
            temp = this.toArray();
            this._elements.splice(0);
            fireIntervalRemoved(this,0,ei,temp);
         }
      }
      
      public function contains(obj:*) : Boolean
      {
         return this.indexOf(obj) >= 0;
      }
      
      public function first() : *
      {
         return this._elements[0];
      }
      
      public function get(i:int) : *
      {
         return this._elements[i];
      }
      
      public function getElementAt(i:int) : *
      {
         return this._elements[i];
      }
      
      public function getSize() : int
      {
         return this.size();
      }
      
      public function indexOf(obj:*) : int
      {
         for(var i:int = 0; i < this._elements.length; i++)
         {
            if(this._elements[i] == obj)
            {
               return i;
            }
         }
         return -1;
      }
      
      public function insertElementAt(item:*, index:int) : void
      {
         this.append(item,index);
      }
      
      public function isEmpty() : Boolean
      {
         return this._elements.length <= 0;
      }
      
      public function last() : *
      {
         return this._elements[this._elements.length - 1];
      }
      
      public function pop() : *
      {
         if(this.size() > 0)
         {
            return this.removeAt(this.size() - 1);
         }
         return null;
      }
      
      public function remove(obj:*) : *
      {
         var i:int = this.indexOf(obj);
         if(i >= 0)
         {
            return this.removeAt(i);
         }
         return null;
      }
      
      public function removeAt(index:int) : *
      {
         if(index < 0 || index >= this.size())
         {
            return null;
         }
         var obj:* = this._elements[index];
         this._elements.splice(index,1);
         fireIntervalRemoved(this,index,index,[obj]);
         return obj;
      }
      
      public function removeElementAt(index:int) : void
      {
         this.removeAt(index);
      }
      
      public function removeRange(fromIndex:int, toIndex:int) : Array
      {
         var removed:Array = null;
         if(this._elements.length > 0)
         {
            fromIndex = Math.max(0,fromIndex);
            toIndex = Math.min(toIndex,this._elements.length - 1);
            if(fromIndex > toIndex)
            {
               return [];
            }
            removed = this._elements.splice(fromIndex,toIndex - fromIndex + 1);
            fireIntervalRemoved(this,fromIndex,toIndex,removed);
            return removed;
         }
         return [];
      }
      
      public function replaceAt(index:int, obj:*) : *
      {
         if(index < 0 || index >= this.size())
         {
            return null;
         }
         var oldObj:* = this._elements[index];
         this._elements[index] = obj;
         fireContentsChanged(this,index,index,[oldObj]);
         return oldObj;
      }
      
      public function shift() : *
      {
         if(this.size() > 0)
         {
            return this.removeAt(0);
         }
         return null;
      }
      
      public function size() : int
      {
         return this._elements.length;
      }
      
      public function sort(compare:Object, options:int) : Array
      {
         var returned:Array = this._elements.sort(compare,options);
         fireContentsChanged(this,0,this._elements.length - 1,[]);
         return returned;
      }
      
      public function sortOn(key:Object, options:int) : Array
      {
         var returned:Array = this._elements.sortOn(key,options);
         fireContentsChanged(this,0,this._elements.length - 1,[]);
         return returned;
      }
      
      public function subArray(startIndex:int, length:int) : Array
      {
         if(this.size() == 0 || length <= 0)
         {
            return new Array();
         }
         return this._elements.slice(startIndex,Math.min(startIndex + length,this.size()));
      }
      
      public function toArray() : Array
      {
         return this._elements.concat();
      }
      
      public function toString() : String
      {
         return "VectorListModel : " + this._elements.toString();
      }
      
      public function valueChanged(obj:*) : void
      {
         this.valueChangedAt(this.indexOf(obj));
      }
      
      public function valueChangedAt(index:int) : void
      {
         if(index >= 0 && index < this._elements.length)
         {
            fireContentsChanged(this,index,index,[]);
         }
      }
      
      public function valueChangedRange(from:int, to:int) : void
      {
         fireContentsChanged(this,from,to,[]);
      }
      
      public function get elements() : Array
      {
         return this._elements;
      }
      
      public function getCellPosFromIndex(index:int) : Number
      {
         var cellData:INotSameHeightListCellData = null;
         var result:Number = 0;
         var cellSize:int = this.size();
         if(index > cellSize)
         {
            index = cellSize;
         }
         for(var i:int = 0; i < index; i++)
         {
            if(i == index)
            {
               break;
            }
            cellData = this.get(i);
            result = result + cellData.getCellHeight();
         }
         return result;
      }
      
      public function getAllCellHeight() : Number
      {
         var cellData:INotSameHeightListCellData = null;
         var result:Number = 0;
         var cellSize:int = this.size();
         for(var i:int = 0; i < cellSize; i++)
         {
            cellData = this.get(i);
            result = result + cellData.getCellHeight();
         }
         return result;
      }
      
      public function getStartIndexByPosY(posY:Number) : int
      {
         var cellData:INotSameHeightListCellData = null;
         var result:int = 0;
         var cellSize:int = this.size();
         var heightCounter:Number = 0;
         for(var i:int = 0; i < cellSize; i++)
         {
            cellData = this.get(i);
            heightCounter = heightCounter + cellData.getCellHeight();
            if(heightCounter >= posY)
            {
               result = i;
               break;
            }
         }
         return result;
      }
   }
}
