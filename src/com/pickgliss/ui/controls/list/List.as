package com.pickgliss.ui.controls.list
{
   import com.pickgliss.events.InteractiveEvent;
   import com.pickgliss.events.ListItemEvent;
   import com.pickgliss.geom.IntDimension;
   import com.pickgliss.geom.IntPoint;
   import com.pickgliss.geom.IntRectangle;
   import com.pickgliss.ui.ComponentSetting;
   import com.pickgliss.ui.controls.cell.IListCell;
   import com.pickgliss.ui.controls.cell.IListCellFactory;
   import com.pickgliss.ui.core.Component;
   import com.pickgliss.ui.core.IViewprot;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.Shape;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   
   [Event(name="listItemRollOut",type="com.pickgliss.events.ListItemEvent")]
   [Event(name="listItemRollOver",type="com.pickgliss.events.ListItemEvent")]
   [Event(name="listItemMouseUp",type="com.pickgliss.events.ListItemEvent")]
   [Event(name="listItemMouseDown",type="com.pickgliss.events.ListItemEvent")]
   [Event(name="listItemDoubleclick",type="com.pickgliss.events.ListItemEvent")]
   [Event(name="listItemClick",type="com.pickgliss.events.ListItemEvent")]
   public class List extends Component implements IViewprot, ListDataListener
   {
      
      public static const AUTO_INCREMENT:int = int.MIN_VALUE;
      
      public static const P_cellFactory:String = "cellFactory";
      
      public static const P_horizontalBlockIncrement:String = "horizontalBlockIncrement";
      
      public static const P_horizontalUnitIncrement:String = "horizontalUnitIncrement";
      
      public static const P_model:String = "model";
      
      public static const P_verticalBlockIncrement:String = "verticalBlockIncrement";
      
      public static const P_verticalUnitIncrement:String = "verticalUnitIncrement";
      
      public static const P_viewPosition:String = "viewPosition";
      
      public static const P_viewSize:String = "viewSize";
       
      
      protected var _cells:Vector.<IListCell>;
      
      protected var _cellsContainer:Sprite;
      
      protected var _factory:IListCellFactory;
      
      protected var _firstVisibleIndex:int;
      
      protected var _firstVisibleIndexOffset:int;
      
      protected var _horizontalBlockIncrement:int;
      
      protected var _horizontalUnitIncrement:int;
      
      protected var _lastVisibleIndex:int;
      
      protected var _lastVisibleIndexOffset:int;
      
      protected var _maskShape:Shape;
      
      protected var _model:IListModel;
      
      protected var _mouseActiveObjectShape:Shape;
      
      protected var _verticalBlockIncrement:int;
      
      protected var _verticalUnitIncrement:int;
      
      protected var _viewHeight:int;
      
      protected var _viewPosition:IntPoint;
      
      protected var _viewWidth:int;
      
      protected var _viewWidthNoCount:int;
      
      protected var _visibleCellWidth:int;
      
      protected var _visibleRowCount:int;
      
      protected var _currentSelectedIndex:int = -1;
      
      public function List()
      {
         this._horizontalBlockIncrement = ComponentSetting.SCROLL_BLOCK_INCREMENT;
         this._horizontalUnitIncrement = ComponentSetting.SCROLL_UINT_INCREMENT;
         this._verticalBlockIncrement = ComponentSetting.SCROLL_BLOCK_INCREMENT;
         this._verticalUnitIncrement = ComponentSetting.SCROLL_UINT_INCREMENT;
         super();
      }
      
      public function addStateListener(listener:Function, priority:int = 0, useWeakReference:Boolean = false) : void
      {
         addEventListener(InteractiveEvent.STATE_CHANGED,listener,false,priority);
      }
      
      public function get cellFactory() : IListCellFactory
      {
         return this._factory;
      }
      
      public function set cellFactory(factory:IListCellFactory) : void
      {
         if(this._factory == factory)
         {
            return;
         }
         this._factory = factory;
         onPropertiesChanged(P_cellFactory);
      }
      
      public function contentsChanged(event:ListDataEvent) : void
      {
         if(event.getIndex0() >= this._firstVisibleIndex && event.getIndex0() <= this._lastVisibleIndex || event.getIndex1() >= this._firstVisibleIndex && event.getIndex1() <= this._lastVisibleIndex || this._lastVisibleIndex == -1)
         {
            this.updateListView();
         }
      }
      
      override public function dispose() : void
      {
         this._mouseActiveObjectShape.graphics.clear();
         this._mouseActiveObjectShape = null;
         this._maskShape.graphics.clear();
         this._maskShape = null;
         this.removeAllCell();
         this._cells = null;
         if(this._model)
         {
            this._model.removeListDataListener(this);
         }
         this._model = null;
         super.dispose();
      }
      
      public function getExtentSize() : IntDimension
      {
         return new IntDimension(_width,_height);
      }
      
      public function getViewSize() : IntDimension
      {
         return new IntDimension(this._viewWidth,this._viewHeight);
      }
      
      public function getViewportPane() : Component
      {
         return this;
      }
      
      public function get horizontalBlockIncrement() : int
      {
         return this._horizontalBlockIncrement;
      }
      
      public function set horizontalBlockIncrement(increment:int) : void
      {
         if(this._horizontalBlockIncrement == increment)
         {
            return;
         }
         this._horizontalBlockIncrement = increment;
         onPropertiesChanged(P_horizontalBlockIncrement);
      }
      
      public function get horizontalUnitIncrement() : int
      {
         return this._horizontalUnitIncrement;
      }
      
      public function set horizontalUnitIncrement(increment:int) : void
      {
         if(this._horizontalUnitIncrement == increment)
         {
            return;
         }
         this._horizontalUnitIncrement = increment;
         onPropertiesChanged(P_horizontalUnitIncrement);
      }
      
      public function intervalAdded(event:ListDataEvent) : void
      {
         this.refreshViewSize();
         onPropertiesChanged(P_viewSize);
         var ih:int = this._factory.getCellHeight();
         var needNum:int = Math.floor(_height / ih);
         if(event.getIndex1() <= this._lastVisibleIndex || this._lastVisibleIndex == -1 || this.viewHeight < _height || this._lastVisibleIndex <= needNum)
         {
            this.updateListView();
         }
      }
      
      public function intervalRemoved(event:ListDataEvent) : void
      {
         this.refreshViewSize();
         onPropertiesChanged(P_viewSize);
         var ih:int = this._factory.getCellHeight();
         var needNum:int = Math.floor(_height / ih);
         if(event.getIndex1() <= this._lastVisibleIndex || this._lastVisibleIndex == -1 || this.viewHeight < _height || this._lastVisibleIndex <= needNum)
         {
            this.updateListView();
         }
      }
      
      public function isSelectedIndex(index:int) : Boolean
      {
         return this._currentSelectedIndex == index;
      }
      
      public function get model() : IListModel
      {
         return this._model;
      }
      
      public function set model(m:IListModel) : void
      {
         if(m != this.model)
         {
            if(this._model)
            {
               this._model.removeListDataListener(this);
            }
            this._model = m;
            this._model.addListDataListener(this);
            onPropertiesChanged(P_model);
         }
      }
      
      public function removeStateListener(listener:Function) : void
      {
         removeEventListener(InteractiveEvent.STATE_CHANGED,listener);
      }
      
      public function scrollRectToVisible(contentRect:IntRectangle) : void
      {
         this.viewPosition = new IntPoint(contentRect.x,contentRect.y);
      }
      
      public function setListData(ld:Array) : void
      {
         var m:IListModel = new VectorListModel(ld);
         this.model = m;
      }
      
      public function setViewportTestSize(s:IntDimension) : void
      {
      }
      
      public function updateListView() : void
      {
         if(this._factory == null)
         {
            return;
         }
         this.createCells();
         this.updateShowMask();
         this.updatePos();
      }
      
      public function get verticalBlockIncrement() : int
      {
         return this._verticalBlockIncrement;
      }
      
      public function set verticalBlockIncrement(increment:int) : void
      {
         if(this._verticalBlockIncrement == increment)
         {
            return;
         }
         this._verticalBlockIncrement = increment;
         onPropertiesChanged(P_verticalBlockIncrement);
      }
      
      public function get verticalUnitIncrement() : int
      {
         return this._verticalUnitIncrement;
      }
      
      public function set verticalUnitIncrement(increment:int) : void
      {
         if(this._verticalUnitIncrement == increment)
         {
            return;
         }
         this._verticalUnitIncrement = increment;
         onPropertiesChanged(P_verticalUnitIncrement);
      }
      
      public function get viewPosition() : IntPoint
      {
         return this._viewPosition;
      }
      
      public function set viewPosition(p:IntPoint) : void
      {
         if(this._viewPosition.equals(this.restrictionViewPos(p)))
         {
            return;
         }
         this._viewPosition.setLocation(p);
         onPropertiesChanged(P_viewPosition);
      }
      
      protected function __onItemInteractive(event:MouseEvent) : void
      {
         var type:String = null;
         var target:IListCell = event.currentTarget as IListCell;
         var index:int = this._model.indexOf(target.getCellValue());
         switch(event.type)
         {
            case MouseEvent.CLICK:
               type = ListItemEvent.LIST_ITEM_CLICK;
               this._currentSelectedIndex = index;
               this.updateListView();
               break;
            case MouseEvent.DOUBLE_CLICK:
               type = ListItemEvent.LIST_ITEM_DOUBLE_CLICK;
               break;
            case MouseEvent.MOUSE_DOWN:
               type = ListItemEvent.LIST_ITEM_MOUSE_DOWN;
               break;
            case MouseEvent.MOUSE_UP:
               type = ListItemEvent.LIST_ITEM_MOUSE_UP;
               break;
            case MouseEvent.ROLL_OVER:
               type = ListItemEvent.LIST_ITEM_ROLL_OVER;
               break;
            case MouseEvent.ROLL_OUT:
               type = ListItemEvent.LIST_ITEM_ROLL_OUT;
         }
         dispatchEvent(new ListItemEvent(target,target.getCellValue(),type,index));
      }
      
      public function getCellAt(val:int) : IListCell
      {
         return this._cells[val];
      }
      
      public function getAllCells() : Vector.<IListCell>
      {
         return this._cells;
      }
      
      public function get currentSelectedIndex() : int
      {
         return this._currentSelectedIndex;
      }
      
      public function set currentSelectedIndex(val:int) : void
      {
         if(val >= this._cells.length)
         {
            return;
         }
         var targetCell:IListCell = this._cells[val];
         if(targetCell != null)
         {
            this._currentSelectedIndex = val;
            this.updateListView();
            dispatchEvent(new ListItemEvent(targetCell,targetCell.getCellValue(),ListItemEvent.LIST_ITEM_CLICK,this._currentSelectedIndex));
         }
      }
      
      protected function addCellToContainer(cell:IListCell) : void
      {
         cell.addEventListener(MouseEvent.CLICK,this.__onItemInteractive);
         cell.addEventListener(MouseEvent.MOUSE_DOWN,this.__onItemInteractive);
         cell.addEventListener(MouseEvent.MOUSE_UP,this.__onItemInteractive);
         cell.addEventListener(MouseEvent.ROLL_OVER,this.__onItemInteractive);
         cell.addEventListener(MouseEvent.ROLL_OUT,this.__onItemInteractive);
         cell.addEventListener(MouseEvent.DOUBLE_CLICK,this.__onItemInteractive);
         this._cells.push(this._cellsContainer.addChild(cell.asDisplayObject()));
      }
      
      override protected function addChildren() : void
      {
         super.addChildren();
         addChild(this._mouseActiveObjectShape);
         addChild(this._maskShape);
         addChild(this._cellsContainer);
      }
      
      protected function creatMaskShape() : void
      {
         this._maskShape = new Shape();
         this._maskShape.graphics.beginFill(16711680,1);
         this._maskShape.graphics.drawRect(0,0,100,100);
         this._maskShape.graphics.endFill();
         this._mouseActiveObjectShape = new Shape();
         this._mouseActiveObjectShape.graphics.beginFill(16711680,0);
         this._mouseActiveObjectShape.graphics.drawRect(0,0,100,100);
         this._mouseActiveObjectShape.graphics.endFill();
      }
      
      protected function createCells() : void
      {
         if(this._factory.isShareCells())
         {
            this.createCellsWhenShareCells();
         }
         else
         {
            this.createCellsWhenNotShareCells();
         }
      }
      
      protected function createCellsWhenNotShareCells() : void
      {
      }
      
      protected function createCellsWhenShareCells() : void
      {
         var i:int = 0;
         var cell:IListCell = null;
         var maxCellWidth:int = 0;
         var addNum:int = 0;
         var removeIndex:int = 0;
         var removed:Vector.<IListCell> = null;
         var ih:int = this._factory.getCellHeight();
         var needNum:int = Math.floor(_height / ih) + 2;
         this._viewWidth = this._factory.getViewWidthNoCount();
         if(this._cells.length == needNum)
         {
            return;
         }
         if(this._cells.length < needNum)
         {
            addNum = needNum - this._cells.length;
            for(i = 0; i < addNum; i++)
            {
               cell = this.createNewCell();
               maxCellWidth = Math.max(cell.width,maxCellWidth);
               this.addCellToContainer(cell);
            }
         }
         else if(this._cells.length > needNum)
         {
            removeIndex = needNum;
            removed = this._cells.splice(removeIndex,this._cells.length - removeIndex);
            for(i = 0; i < removed.length; i++)
            {
               this.removeCellFromContainer(removed[i]);
            }
         }
      }
      
      protected function createNewCell() : IListCell
      {
         if(this._factory == null)
         {
            return null;
         }
         return this._factory.createNewCell();
      }
      
      protected function fireStateChanged(programmatic:Boolean = true) : void
      {
         dispatchEvent(new InteractiveEvent(InteractiveEvent.STATE_CHANGED));
      }
      
      protected function getListCellModelHeight(index:int) : int
      {
         return 0;
      }
      
      protected function getViewMaxPos() : IntPoint
      {
         var showSize:IntDimension = this.getExtentSize();
         var viewSize:IntDimension = this.getViewSize();
         var p:IntPoint = new IntPoint(viewSize.width - showSize.width,viewSize.height - showSize.height);
         if(p.x < 0)
         {
            p.x = 0;
         }
         if(p.y < 0)
         {
            p.y = 0;
         }
         return p;
      }
      
      override protected function init() : void
      {
         this.creatMaskShape();
         this._cellsContainer = new Sprite();
         addChild(this._cellsContainer);
         this._viewPosition = new IntPoint(0,0);
         this._firstVisibleIndex = 0;
         this._lastVisibleIndex = -1;
         this._firstVisibleIndexOffset = 0;
         this._lastVisibleIndexOffset = 0;
         this._visibleRowCount = -1;
         this._visibleCellWidth = -1;
         this._cells = new Vector.<IListCell>();
         super.init();
         this._model = new VectorListModel();
         this._model.addListDataListener(this);
      }
      
      protected function layoutWhenShareCellsHasNotSameHeight() : void
      {
         var cell:IListCell = null;
         var ldIndex:int = 0;
         this.createCellsWhenShareCells();
         this.restrictionViewPos(this._viewPosition);
         var x:int = this._viewPosition.x;
         var y:int = this._viewPosition.y;
         var startIndex:int = this._model.getStartIndexByPosY(y);
         var listSize:int = this._model.getSize();
         var cx:int = -x;
         var maxY:int = _height;
         if(listSize < 0)
         {
            this._lastVisibleIndex = -1;
         }
         for(var i:int = 0; i < this._cells.length; i++)
         {
            cell = this._cells[i];
            ldIndex = startIndex + i;
            if(ldIndex < listSize)
            {
               cell.setCellValue(this._model.getElementAt(ldIndex));
               cell.setListCellStatus(this,this.isSelectedIndex(ldIndex),ldIndex);
               cell.visible = true;
               cell.x = cx;
               cell.y = this._model.getCellPosFromIndex(ldIndex) - y;
               if(cell.y < maxY)
               {
                  this._lastVisibleIndex = ldIndex;
               }
            }
            else
            {
               cell.visible = false;
            }
         }
         this.refreshViewSize();
         this._firstVisibleIndex = startIndex;
      }
      
      protected function layoutWhenShareCellsHasSameHeight() : void
      {
         var cell:IListCell = null;
         var ldIndex:int = 0;
         this.createCellsWhenShareCells();
         this.restrictionViewPos(this._viewPosition);
         var x:int = this._viewPosition.x;
         var y:int = this._viewPosition.y;
         var ih:int = this._factory.getCellHeight();
         var startIndex:int = Math.floor(y / ih);
         var startY:int = startIndex * ih - y;
         var listSize:int = this._model.getSize();
         var cx:int = -x;
         var cy:int = startY;
         var maxY:int = _height;
         if(listSize < 0)
         {
            this._lastVisibleIndex = -1;
         }
         for(var i:int = 0; i < this._cells.length; i++)
         {
            cell = this._cells[i];
            ldIndex = startIndex + i;
            if(ldIndex < listSize)
            {
               cell.setCellValue(this._model.getElementAt(ldIndex));
               cell.setListCellStatus(this,this.isSelectedIndex(ldIndex),ldIndex);
               cell.visible = true;
               cell.x = cx;
               cell.y = cy;
               if(cy < maxY)
               {
                  this._lastVisibleIndex = ldIndex;
               }
               cy = cy + ih;
            }
            else
            {
               cell.visible = false;
            }
         }
         this.refreshViewSize();
         this._firstVisibleIndex = startIndex;
      }
      
      override protected function onProppertiesUpdate() : void
      {
         super.onProppertiesUpdate();
         var mustUpdateListView:Boolean = false;
         this._cellsContainer.mask = this._maskShape;
         if(_changedPropeties[P_model] || _changedPropeties[P_cellFactory] || _changedPropeties[P_viewPosition] || _changedPropeties[Component.P_width] || _changedPropeties[Component.P_height])
         {
            if(_changedPropeties[P_cellFactory])
            {
               this.removeAllCell();
            }
            mustUpdateListView = true;
         }
         if(mustUpdateListView)
         {
            this.updateListView();
         }
         if(_changedPropeties[P_verticalBlockIncrement] || _changedPropeties[P_verticalUnitIncrement] || _changedPropeties[P_horizontalBlockIncrement] || _changedPropeties[P_horizontalUnitIncrement] || _changedPropeties[Component.P_height] || _changedPropeties[Component.P_width] || _changedPropeties[P_viewPosition] || _changedPropeties[P_viewSize])
         {
            this.fireStateChanged();
         }
      }
      
      protected function refreshViewSize() : void
      {
         if(this._factory.isShareCells())
         {
            this._viewWidth = this._factory.getViewWidthNoCount();
            if(this._factory.isAllCellHasSameHeight())
            {
               this.viewHeight = this._model.getSize() * this._factory.getCellHeight();
            }
            else
            {
               this.viewHeight = this._model.getAllCellHeight();
            }
         }
      }
      
      public function get viewHeight() : Number
      {
         return this._viewHeight;
      }
      
      public function set viewHeight(value:Number) : void
      {
         if(this._viewHeight == value)
         {
            return;
         }
         this._viewHeight = value;
         onPropertiesChanged(P_viewSize);
      }
      
      public function get viewWidth() : Number
      {
         return this._viewWidth;
      }
      
      public function set viewWidth(value:Number) : void
      {
         if(this._viewWidth == value)
         {
            return;
         }
         this._viewWidth = value;
         onPropertiesChanged(P_viewSize);
      }
      
      public function unSelectedAll() : void
      {
         this._currentSelectedIndex = -1;
         this.updateListView();
      }
      
      protected function removeAllCell() : void
      {
         for(var i:int = 0; i < this._cells.length; i++)
         {
            this.removeCellFromContainer(this._cells[i]);
         }
         this._cells = new Vector.<IListCell>();
      }
      
      protected function removeCellFromContainer(cell:IListCell) : void
      {
         cell.removeEventListener(MouseEvent.CLICK,this.__onItemInteractive);
         cell.removeEventListener(MouseEvent.MOUSE_DOWN,this.__onItemInteractive);
         cell.removeEventListener(MouseEvent.MOUSE_UP,this.__onItemInteractive);
         cell.removeEventListener(MouseEvent.ROLL_OVER,this.__onItemInteractive);
         cell.removeEventListener(MouseEvent.ROLL_OUT,this.__onItemInteractive);
         cell.removeEventListener(MouseEvent.DOUBLE_CLICK,this.__onItemInteractive);
         ObjectUtils.disposeObject(cell);
      }
      
      protected function restrictionViewPos(p:IntPoint) : IntPoint
      {
         var maxPos:IntPoint = this.getViewMaxPos();
         p.x = Math.max(0,Math.min(maxPos.x,p.x));
         p.y = Math.max(0,Math.min(maxPos.y,p.y));
         return p;
      }
      
      protected function updatePos() : void
      {
         if(this._factory.isShareCells())
         {
            if(this._factory.isAllCellHasSameHeight())
            {
               this.layoutWhenShareCellsHasSameHeight();
            }
            else
            {
               this.layoutWhenShareCellsHasNotSameHeight();
            }
         }
      }
      
      public function get cell() : Vector.<IListCell>
      {
         return this._cells;
      }
      
      protected function updateShowMask() : void
      {
         this._mouseActiveObjectShape.width = this._maskShape.width = _width;
         this._mouseActiveObjectShape.height = this._maskShape.height = _height;
      }
   }
}
