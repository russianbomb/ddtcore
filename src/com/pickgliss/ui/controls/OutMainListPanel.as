package com.pickgliss.ui.controls
{
   import com.pickgliss.events.InteractiveEvent;
   import com.pickgliss.events.ListItemEvent;
   import com.pickgliss.geom.InnerRectangle;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.cell.IListCell;
   import com.pickgliss.ui.controls.cell.IListCellFactory;
   import com.pickgliss.ui.controls.list.ListDataEvent;
   import com.pickgliss.ui.controls.list.ListDataListener;
   import com.pickgliss.ui.controls.list.VectorListModel;
   import com.pickgliss.ui.core.Component;
   import com.pickgliss.utils.ClassUtils;
   import com.pickgliss.utils.DisplayUtils;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   
   public class OutMainListPanel extends Component implements ListDataListener
   {
       
      
      private var P_vScrollbar:String = "vScrollBar";
      
      private var P_vScrollbarInnerRect:String = "vScrollBarInnerRect";
      
      private var P_cellFactory:String = "cellFactory";
      
      private var _cellsContainer:Sprite;
      
      private var _vScrollbarStyle:String;
      
      private var _vScrollbar:Scrollbar;
      
      private var _vScrollbarInnerRectString:String;
      
      private var _vScrollbarInnerRect:InnerRectangle;
      
      private var _factoryStyle:String;
      
      private var _factory:IListCellFactory;
      
      private var _model:VectorListModel;
      
      private var _cells:Vector.<IListCell>;
      
      private var _presentPos:int;
      
      private var _needNum:int;
      
      public function OutMainListPanel()
      {
         super();
      }
      
      override protected function init() : void
      {
         this.initEvent();
         this._presentPos = 0;
         this._cells = new Vector.<IListCell>();
         this._model = new VectorListModel();
         this._model.addListDataListener(this);
         super.init();
      }
      
      override protected function addChildren() : void
      {
         super.addChildren();
         if(this._vScrollbar)
         {
            this._cellsContainer.addChild(this._vScrollbar);
         }
         this._cellsContainer = new Sprite();
         addChild(this._cellsContainer);
      }
      
      public function get vectorListModel() : VectorListModel
      {
         return this._model;
      }
      
      public function contentsChanged(event:ListDataEvent) : void
      {
         this.changeDate();
      }
      
      public function intervalAdded(event:ListDataEvent) : void
      {
         this.syncScrollBar();
      }
      
      public function intervalRemoved(event:ListDataEvent) : void
      {
         this.syncScrollBar();
      }
      
      private function syncScrollBar() : void
      {
         var extent:int = 0;
         var value:int = 0;
         var max:int = 0;
         var ih:int = this._factory.getCellHeight();
         this._needNum = Math.floor(_height / ih);
         if(this._vScrollbar != null)
         {
            extent = this._needNum * this._factory.getCellHeight();
            value = this._presentPos * this._factory.getCellHeight();
            max = this._factory.getCellHeight() * this._model.elements.length;
            this._vScrollbar.unitIncrement = this._factory.getCellHeight();
            this._vScrollbar.blockIncrement = this._factory.getCellHeight();
            this._vScrollbar.getModel().setRangeProperties(value,extent,0,max,false);
         }
         this.changeDate();
      }
      
      private function changeDate() : void
      {
         for(var i:int = 0; i < this._needNum; i++)
         {
            this._cells[i].setCellValue(this._model.elements[this._presentPos + i]);
         }
      }
      
      private function createCells() : void
      {
         var addNum:int = 0;
         var i:int = 0;
         var cell:IListCell = null;
         var removeIndex:int = 0;
         var removed:Vector.<IListCell> = null;
         var j:int = 0;
         var ih:int = this._factory.getCellHeight();
         this._needNum = Math.floor(_height / ih);
         if(this._cells.length == this._needNum)
         {
            return;
         }
         if(this._cells.length < this._needNum)
         {
            addNum = this._needNum - this._cells.length;
            for(i = 0; i < addNum; i++)
            {
               cell = this.createNewCell();
               cell.y = this._factory.getCellHeight() * i;
               this.addCellToContainer(cell);
            }
         }
         else
         {
            removeIndex = this._needNum;
            removed = this._cells.splice(removeIndex,this._cells.length - removeIndex);
            for(j = 0; i < removed.length; j++)
            {
               this.removeCellFromContainer(removed[j]);
            }
         }
      }
      
      protected function createNewCell() : IListCell
      {
         if(this._factory == null)
         {
            return null;
         }
         return this._factory.createNewCell();
      }
      
      protected function addCellToContainer(cell:IListCell) : void
      {
         cell.addEventListener(MouseEvent.CLICK,this.__onItemInteractive);
         cell.addEventListener(MouseEvent.MOUSE_DOWN,this.__onItemInteractive);
         cell.addEventListener(MouseEvent.MOUSE_UP,this.__onItemInteractive);
         cell.addEventListener(MouseEvent.ROLL_OVER,this.__onItemInteractive);
         cell.addEventListener(MouseEvent.ROLL_OUT,this.__onItemInteractive);
         cell.addEventListener(MouseEvent.DOUBLE_CLICK,this.__onItemInteractive);
         this._cells.push(this._cellsContainer.addChild(cell.asDisplayObject()));
      }
      
      protected function __onItemInteractive(event:MouseEvent) : void
      {
         var type:String = null;
         var target:IListCell = event.currentTarget as IListCell;
         var index:int = this._model.indexOf(target.getCellValue());
         switch(event.type)
         {
            case MouseEvent.CLICK:
               type = ListItemEvent.LIST_ITEM_CLICK;
               break;
            case MouseEvent.DOUBLE_CLICK:
               type = ListItemEvent.LIST_ITEM_DOUBLE_CLICK;
               break;
            case MouseEvent.MOUSE_DOWN:
               type = ListItemEvent.LIST_ITEM_MOUSE_DOWN;
               break;
            case MouseEvent.MOUSE_UP:
               type = ListItemEvent.LIST_ITEM_MOUSE_UP;
               break;
            case MouseEvent.ROLL_OVER:
               type = ListItemEvent.LIST_ITEM_ROLL_OVER;
               break;
            case MouseEvent.ROLL_OUT:
               type = ListItemEvent.LIST_ITEM_ROLL_OUT;
         }
         dispatchEvent(new ListItemEvent(target,target.getCellValue(),type,index));
      }
      
      protected function removeAllCell() : void
      {
         for(var i:int = 0; i < this._cells.length; i++)
         {
            this.removeCellFromContainer(this._cells[i]);
         }
         this._cells = new Vector.<IListCell>();
      }
      
      protected function removeCellFromContainer(cell:IListCell) : void
      {
         cell.removeEventListener(MouseEvent.CLICK,this.__onItemInteractive);
         cell.removeEventListener(MouseEvent.MOUSE_DOWN,this.__onItemInteractive);
         cell.removeEventListener(MouseEvent.MOUSE_UP,this.__onItemInteractive);
         cell.removeEventListener(MouseEvent.ROLL_OVER,this.__onItemInteractive);
         cell.removeEventListener(MouseEvent.ROLL_OUT,this.__onItemInteractive);
         cell.removeEventListener(MouseEvent.DOUBLE_CLICK,this.__onItemInteractive);
         ObjectUtils.disposeObject(cell);
      }
      
      protected function initEvent() : void
      {
         addEventListener(MouseEvent.MOUSE_WHEEL,this.onMouseWheel);
      }
      
      public function onMouseWheel(event:MouseEvent) : void
      {
         var incre:int = 0;
         var resulte:int = 0;
         if(this._needNum > 0)
         {
            incre = Math.floor(event.delta / this._needNum);
            resulte = this._presentPos - incre;
            if(resulte > this._model.elements.length - this._needNum)
            {
               resulte = this._model.elements.length - this._needNum;
            }
            else if(resulte < 0)
            {
               resulte = 0;
            }
            if(this._presentPos == resulte)
            {
               return;
            }
            this._presentPos = resulte;
            this.syncScrollBar();
         }
      }
      
      public function set vScrollbarInnerRectString(value:String) : void
      {
         if(this._vScrollbarInnerRectString == value)
         {
            return;
         }
         this._vScrollbarInnerRectString = value;
         this._vScrollbarInnerRect = ClassUtils.CreatInstance(ClassUtils.INNERRECTANGLE,ComponentFactory.parasArgs(this._vScrollbarInnerRectString));
         onPropertiesChanged(this.P_vScrollbarInnerRect);
      }
      
      public function set vScrollbarStyle(stylename:String) : void
      {
         if(this._vScrollbarStyle == stylename)
         {
            return;
         }
         this._vScrollbarStyle = stylename;
         this.vScrollbar = ComponentFactory.Instance.creat(this._vScrollbarStyle);
      }
      
      public function get vScrollbar() : Scrollbar
      {
         return this._vScrollbar;
      }
      
      public function set vScrollbar(bar:Scrollbar) : void
      {
         if(this._vScrollbar == bar)
         {
            return;
         }
         if(this._vScrollbar)
         {
            this._vScrollbar.removeStateListener(this.__onScrollValueChange);
            ObjectUtils.disposeObject(this._vScrollbar);
         }
         this._vScrollbar = bar;
         this._vScrollbar.addStateListener(this.__onScrollValueChange);
         onPropertiesChanged(this.P_vScrollbar);
      }
      
      protected function __onScrollValueChange(event:InteractiveEvent) : void
      {
         var newPos:int = Math.floor(this._vScrollbar.getModel().getValue() / this._factory.getCellHeight());
         if(newPos == this._presentPos)
         {
            return;
         }
         this._presentPos = newPos;
         this.syncScrollBar();
      }
      
      public function set factoryStyle(value:String) : void
      {
         if(this._factoryStyle == value)
         {
            return;
         }
         this._factoryStyle = value;
         var factoryAndArgs:Array = value.split("|");
         var classname:String = factoryAndArgs[0];
         var args:Array = ComponentFactory.parasArgs(factoryAndArgs[1]);
         this._factory = ClassUtils.CreatInstance(classname,args);
         onPropertiesChanged(this.P_cellFactory);
      }
      
      override protected function onProppertiesUpdate() : void
      {
         super.onProppertiesUpdate();
         if(_changedPropeties[this.P_cellFactory])
         {
            this.createCells();
         }
         if(_changedPropeties[this.P_vScrollbar] || _changedPropeties[this.P_vScrollbarInnerRect])
         {
            this.layoutComponent();
         }
      }
      
      protected function layoutComponent() : void
      {
         if(this._vScrollbar)
         {
            DisplayUtils.layoutDisplayWithInnerRect(this._vScrollbar,this._vScrollbarInnerRect,_width,_height);
         }
      }
      
      override public function dispose() : void
      {
         removeEventListener(MouseEvent.MOUSE_WHEEL,this.onMouseWheel);
         this.removeAllCell();
         if(this._vScrollbar)
         {
            this._vScrollbar.removeStateListener(this.__onScrollValueChange);
            ObjectUtils.disposeObject(this._vScrollbar);
         }
         this._vScrollbar = null;
         if(this._cellsContainer)
         {
            ObjectUtils.disposeObject(this._cellsContainer);
         }
         this._cellsContainer = null;
         if(this._model)
         {
            this._model.removeListDataListener(this);
         }
         this._model = null;
         super.dispose();
         if(this.parent)
         {
            this.parent.removeChild(this);
         }
      }
   }
}
