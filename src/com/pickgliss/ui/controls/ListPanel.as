package com.pickgliss.ui.controls
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.cell.IListCellFactory;
   import com.pickgliss.ui.controls.list.IListModel;
   import com.pickgliss.ui.controls.list.List;
   import com.pickgliss.ui.controls.list.VectorListModel;
   import com.pickgliss.utils.ClassUtils;
   
   public class ListPanel extends ScrollPanel
   {
      
      public static const P_backgound:String = "backgound";
      
      public static const P_backgoundInnerRect:String = "backgoundInnerRect";
      
      public static const P_factory:String = "factory";
       
      
      protected var _factory:IListCellFactory;
      
      protected var _factoryStrle:String;
      
      protected var _listStyle:String;
      
      public function ListPanel()
      {
         super(false);
      }
      
      override public function dispose() : void
      {
         this._factory = null;
         super.dispose();
      }
      
      public function set factoryStyle(value:String) : void
      {
         if(this._factoryStrle == value)
         {
            return;
         }
         this._factoryStrle = value;
         var factoryAndArgs:Array = value.split("|");
         var classname:String = factoryAndArgs[0];
         var args:Array = ComponentFactory.parasArgs(factoryAndArgs[1]);
         this._factory = ClassUtils.CreatInstance(classname,args);
         onPropertiesChanged(P_factory);
      }
      
      public function get list() : List
      {
         return _viewSource as List;
      }
      
      public function get listModel() : IListModel
      {
         return this.list.model;
      }
      
      public function set listStyle(stylename:String) : void
      {
         if(this._listStyle == stylename)
         {
            return;
         }
         this._listStyle = stylename;
         viewPort = ComponentFactory.Instance.creat(this._listStyle);
      }
      
      public function get vectorListModel() : VectorListModel
      {
         if(this.list == null)
         {
            return null;
         }
         return this.list.model as VectorListModel;
      }
      
      override protected function onProppertiesUpdate() : void
      {
         super.onProppertiesUpdate();
         if(_changedPropeties[P_factory] || _changedPropeties[ScrollPanel.P_viewSource])
         {
            if(this.list)
            {
               this.list.cellFactory = this._factory;
            }
         }
      }
   }
}
