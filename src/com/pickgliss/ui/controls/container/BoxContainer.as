package com.pickgliss.ui.controls.container
{
   import com.pickgliss.events.ComponentEvent;
   import com.pickgliss.ui.core.Component;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.DisplayObject;
   
   public class BoxContainer extends Component
   {
      
      public static const P_childRefresh:String = "childRefresh";
      
      public static const P_childResize:String = "childResize";
      
      public static const P_isReverAdd:String = "isReverAdd";
      
      public static const P_spacing:String = "spacing";
      
      public static const P_strictSize:String = "strictSize";
      
      public static const P_autoSize:String = "autoSize";
      
      public static const LEFT_OR_TOP:int = 0;
      
      public static const RIGHT_OR_BOTTOM:int = 1;
      
      public static const CENTER:int = 2;
       
      
      protected var _childrenList:Vector.<DisplayObject>;
      
      protected var _isReverAdd:Boolean;
      
      protected var _spacing:Number = 0;
      
      protected var _strictSize:Number = -1;
      
      protected var _autoSize:int = 0;
      
      public function BoxContainer()
      {
         this._childrenList = new Vector.<DisplayObject>();
         super();
      }
      
      override public function addChild(child:DisplayObject) : DisplayObject
      {
         if(this._childrenList.indexOf(child) > -1)
         {
            return child;
         }
         if(!this._isReverAdd)
         {
            this._childrenList.push(super.addChild(child));
         }
         else
         {
            this._childrenList.push(super.addChildAt(child,0));
         }
         child.addEventListener(ComponentEvent.PROPERTIES_CHANGED,this.__onResize);
         onPropertiesChanged(P_childRefresh);
         return child;
      }
      
      override public function dispose() : void
      {
         this.disposeAllChildren();
         this._childrenList = null;
         super.dispose();
      }
      
      public function disposeAllChildren() : void
      {
         var child:DisplayObject = null;
         for(var i:int = 0; i < numChildren; i++)
         {
            child = getChildAt(i);
            child.removeEventListener(ComponentEvent.PROPERTIES_CHANGED,this.__onResize);
         }
         ObjectUtils.disposeAllChildren(this);
      }
      
      public function set isReverAdd(value:Boolean) : void
      {
         if(this._isReverAdd == value)
         {
            return;
         }
         this._isReverAdd = value;
         onPropertiesChanged(P_isReverAdd);
      }
      
      public function refreshChildPos() : void
      {
         onPropertiesChanged(P_childResize);
      }
      
      public function removeAllChild() : void
      {
         while(numChildren > 0)
         {
            removeChildAt(0);
         }
         this._childrenList.length = 0;
      }
      
      override public function removeChild(child:DisplayObject) : DisplayObject
      {
         child.removeEventListener(ComponentEvent.PROPERTIES_CHANGED,this.__onResize);
         this._childrenList.splice(this._childrenList.indexOf(child),1);
         super.removeChild(child);
         onPropertiesChanged(P_childRefresh);
         return child;
      }
      
      public function reverChildren() : void
      {
         var tempAllChildren:Vector.<DisplayObject> = new Vector.<DisplayObject>();
         while(numChildren > 0)
         {
            tempAllChildren.push(removeChildAt(0));
         }
         for(var i:int = 0; i < tempAllChildren.length; i++)
         {
            this.addChild(tempAllChildren[i]);
         }
      }
      
      public function set autoSize(value:int) : void
      {
         if(this._autoSize == value)
         {
            return;
         }
         this._autoSize = value;
         onPropertiesChanged(P_autoSize);
      }
      
      public function get spacing() : Number
      {
         return this._spacing;
      }
      
      public function set spacing(value:Number) : void
      {
         if(this._spacing == value)
         {
            return;
         }
         this._spacing = value;
         onPropertiesChanged(P_spacing);
      }
      
      public function set strictSize(value:Number) : void
      {
         if(this._strictSize == value)
         {
            return;
         }
         this._strictSize = value;
         onPropertiesChanged(P_strictSize);
      }
      
      public function arrange() : void
      {
      }
      
      protected function get isStrictSize() : Boolean
      {
         return this._strictSize > 0;
      }
      
      override protected function onProppertiesUpdate() : void
      {
         this.arrange();
      }
      
      private function __onResize(event:ComponentEvent) : void
      {
         if(event.changedProperties[Component.P_height] || event.changedProperties[Component.P_width])
         {
            onPropertiesChanged(P_childRefresh);
         }
      }
   }
}
