package com.pickgliss.ui.controls.container
{
   import flash.display.DisplayObject;
   import flash.events.Event;
   
   public class HBox extends BoxContainer
   {
       
      
      public function HBox()
      {
         super();
      }
      
      override public function arrange() : void
      {
         var child:DisplayObject = null;
         _width = 0;
         _height = 0;
         var xpos:Number = 0;
         var ypos:Number = 0;
         for(var i:int = 0; i < _childrenList.length; i++)
         {
            child = _childrenList[i];
            child.x = xpos;
            xpos = xpos + this.getItemWidth(child);
            xpos = xpos + _spacing;
            if(_autoSize == CENTER && i != 0)
            {
               ypos = _childrenList[0].y - (child.height - _childrenList[0].height) / 2;
            }
            else if(_autoSize == RIGHT_OR_BOTTOM && i != 0)
            {
               ypos = _childrenList[0].y - (child.height - _childrenList[0].height);
            }
            else
            {
               ypos = _childrenList[0].y;
            }
            child.y = ypos;
            _width = _width + this.getItemWidth(child);
            _height = Math.max(_height,child.height);
         }
         _width = _width + _spacing * (numChildren - 1);
         _width = Math.max(0,_width);
         dispatchEvent(new Event(Event.RESIZE));
      }
      
      private function getItemWidth(child:DisplayObject) : Number
      {
         if(isStrictSize)
         {
            return _strictSize;
         }
         return child.width;
      }
   }
}
