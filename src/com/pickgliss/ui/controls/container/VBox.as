package com.pickgliss.ui.controls.container
{
   import flash.display.DisplayObject;
   import flash.events.Event;
   
   public class VBox extends BoxContainer
   {
       
      
      public function VBox()
      {
         super();
      }
      
      override public function arrange() : void
      {
         var ypos:Number = NaN;
         var i:int = 0;
         var child:DisplayObject = null;
         _width = 0;
         _height = 0;
         ypos = 0;
         var xpos:Number = 0;
         for(i = 0; i < _childrenList.length; i++)
         {
            child = _childrenList[i];
            child.y = ypos;
            ypos = ypos + this.getItemHeight(child);
            ypos = ypos + _spacing;
            if(_autoSize == CENTER && i != 0)
            {
               xpos = _childrenList[0].x - (child.width - _childrenList[0].width) / 2;
            }
            else if(_autoSize == RIGHT_OR_BOTTOM && i != 0)
            {
               xpos = _childrenList[0].x - (child.width - _childrenList[0].width);
            }
            else
            {
               xpos = child.x;
            }
            child.x = xpos;
            _height = _height + this.getItemHeight(child);
            _width = Math.max(_width,child.width);
         }
         _height = _height + _spacing * (numChildren - 1);
         _height = Math.max(0,_height);
         dispatchEvent(new Event(Event.RESIZE));
      }
      
      private function getItemHeight(child:DisplayObject) : Number
      {
         if(isStrictSize)
         {
            return _strictSize;
         }
         return child.height;
      }
   }
}
