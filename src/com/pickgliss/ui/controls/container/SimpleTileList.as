package com.pickgliss.ui.controls.container
{
   import flash.display.DisplayObject;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.geom.Point;
   
   public class SimpleTileList extends BoxContainer
   {
       
      
      public var startPos:Point;
      
      protected var _column:int;
      
      protected var _arrangeType:int;
      
      protected var _hSpace:Number = 0;
      
      protected var _rowNum:int;
      
      protected var _vSpace:Number = 0;
      
      private var _selectedIndex:int;
      
      public function SimpleTileList(columnNum:int = 1, arrangeType:int = 0)
      {
         this.startPos = new Point(0,0);
         super();
         this._column = columnNum;
         this._arrangeType = arrangeType;
      }
      
      public function get selectedIndex() : int
      {
         return this._selectedIndex;
      }
      
      public function set selectedIndex(value:int) : void
      {
         if(this._selectedIndex == value)
         {
            return;
         }
         this._selectedIndex = value;
      }
      
      public function get hSpace() : Number
      {
         return this._hSpace;
      }
      
      public function set hSpace(value:Number) : void
      {
         this._hSpace = value;
         onProppertiesUpdate();
      }
      
      public function get vSpace() : Number
      {
         return this._vSpace;
      }
      
      public function set vSpace(value:Number) : void
      {
         this._vSpace = value;
         onProppertiesUpdate();
      }
      
      override public function addChild(child:DisplayObject) : DisplayObject
      {
         child.addEventListener(MouseEvent.CLICK,this.__itemClick);
         super.addChild(child);
         return child;
      }
      
      private function __itemClick(e:MouseEvent) : void
      {
         var item:DisplayObject = e.currentTarget as DisplayObject;
         this._selectedIndex = getChildIndex(item);
      }
      
      override public function arrange() : void
      {
         this.caculateRows();
         if(this._arrangeType == 0)
         {
            this.horizontalArrange();
         }
         else
         {
            this.verticalArrange();
         }
      }
      
      private function horizontalArrange() : void
      {
         var posX:int = 0;
         var tempWidth:int = 0;
         var tempHeight:int = 0;
         var maxHeight:int = 0;
         var j:int = 0;
         var ch:DisplayObject = null;
         var n:int = 0;
         posX = this.startPos.x;
         var posY:int = this.startPos.y;
         tempWidth = 0;
         tempHeight = 0;
         for(var i:int = 0; i < this._rowNum; i++)
         {
            maxHeight = 0;
            for(j = 0; j < this._column; j++)
            {
               ch = getChildAt(n++);
               ch.x = posX;
               ch.y = posY;
               tempWidth = Math.max(tempWidth,posX + ch.width);
               tempHeight = Math.max(tempHeight,posY + ch.height);
               posX = posX + (ch.width + this._hSpace);
               if(maxHeight < ch.height)
               {
                  maxHeight = ch.height;
               }
               if(n >= numChildren)
               {
                  this.changeSize(tempWidth,tempHeight);
                  return;
               }
            }
            posX = this.startPos.x;
            posY = posY + (maxHeight + this._vSpace);
         }
         this.changeSize(tempWidth,tempHeight);
         dispatchEvent(new Event(Event.RESIZE));
      }
      
      private function verticalArrange() : void
      {
         var posX:int = 0;
         var posY:int = 0;
         var tempWidth:int = 0;
         var tempHeight:int = 0;
         var maxWidth:int = 0;
         var j:int = 0;
         var ch:DisplayObject = null;
         var n:int = 0;
         posX = this.startPos.x;
         posY = this.startPos.y;
         tempWidth = 0;
         tempHeight = 0;
         for(var i:int = 0; i < this._rowNum; i++)
         {
            maxWidth = 0;
            for(j = 0; j < this._column; j++)
            {
               ch = getChildAt(n++);
               ch.x = posX;
               ch.y = posY;
               tempWidth = Math.max(tempWidth,posX + ch.width);
               tempHeight = Math.max(tempHeight,posY + ch.height);
               posY = posY + (ch.height + this._vSpace);
               if(maxWidth < ch.width)
               {
                  maxWidth = ch.width;
               }
               if(n >= numChildren)
               {
                  this.changeSize(tempWidth,tempHeight);
                  return;
               }
            }
            posX = posX + (maxWidth + this._hSpace);
            posY = this.startPos.y;
         }
         this.changeSize(tempWidth,tempHeight);
         dispatchEvent(new Event(Event.RESIZE));
      }
      
      private function changeSize(w:int, h:int) : void
      {
         if(w != _width || h != _height)
         {
            width = w;
            height = h;
         }
      }
      
      private function caculateRows() : void
      {
         this._rowNum = Math.ceil(numChildren / this._column);
      }
      
      override public function dispose() : void
      {
         var item:DisplayObject = null;
         for(var i:int = 0; i < numChildren; i++)
         {
            item = getChildAt(i) as DisplayObject;
            item.removeEventListener(MouseEvent.CLICK,this.__itemClick);
         }
         super.dispose();
      }
   }
}
