package com.pickgliss.ui.controls
{
   import com.pickgliss.events.InteractiveEvent;
   import com.pickgliss.geom.InnerRectangle;
   import com.pickgliss.toplevel.StageReferance;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Component;
   import com.pickgliss.ui.core.IOrientable;
   import com.pickgliss.utils.ClassUtils;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.DisplayObject;
   import flash.display.Shape;
   import flash.events.MouseEvent;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   
   public class Silder extends Component implements IOrientable
   {
      
      public static const P_bar:String = "bar";
      
      public static const P_maskShowAreaInnerRect:String = "maskShowAreaInnerRect";
      
      public static const P_maximum:String = "maximum";
      
      public static const P_minimum:String = "minimum";
      
      public static const P_orientation:String = "orientation";
      
      public static const P_progressBar:String = "progressBar";
      
      public static const P_thumb:String = "thumb";
      
      public static const P_thumbShowInnerRect:String = "thumbShowInnerRect";
      
      public static const P_value:String = "value";
       
      
      protected var _bar:DisplayObject;
      
      protected var _barStyle:String;
      
      protected var _isDragging:Boolean;
      
      protected var _maskShape:Shape;
      
      protected var _maskShowAreaInnerRect:InnerRectangle;
      
      protected var _maskShowAreaInnerRectString:String;
      
      protected var _model:BoundedRangeModel;
      
      protected var _orientation:int = -1;
      
      protected var _progressBar:DisplayObject;
      
      protected var _progressBarStyle:String;
      
      protected var _thumb:BaseButton;
      
      protected var _thumbDownOffset:int;
      
      protected var _thumbShowInnerRect:InnerRectangle;
      
      protected var _thumbShowInnerRectString:String;
      
      protected var _thumbStyle:String;
      
      protected var _value:Number;
      
      public function Silder()
      {
         super();
      }
      
      public function set bar($bar:DisplayObject) : void
      {
         if(this._bar == $bar)
         {
            return;
         }
         this._bar = $bar;
         onPropertiesChanged(P_bar);
      }
      
      public function set barStyle(value:String) : void
      {
         if(this._barStyle == value)
         {
            return;
         }
         this._barStyle = value;
         this.bar = ComponentFactory.Instance.creat(this._barStyle);
      }
      
      override public function dispose() : void
      {
         removeEventListener(MouseEvent.CLICK,this.__onSilderClick);
         StageReferance.stage.removeEventListener(MouseEvent.MOUSE_UP,this.__onThumbMouseUp);
         StageReferance.stage.removeEventListener(MouseEvent.MOUSE_MOVE,this.__onThumbMouseMoved);
         if(this._thumb)
         {
            this._thumb.addEventListener(MouseEvent.MOUSE_DOWN,this.__onThumbMouseDown);
         }
         ObjectUtils.disposeObject(this._thumb);
         ObjectUtils.disposeObject(this._bar);
         ObjectUtils.disposeObject(this._progressBar);
         ObjectUtils.disposeObject(this._maskShape);
         super.dispose();
      }
      
      public function getModel() : BoundedRangeModel
      {
         return this._model;
      }
      
      public function isVertical() : Boolean
      {
         return this._orientation == 0;
      }
      
      public function set maskShowAreaInnerRectString(value:String) : void
      {
         if(this._maskShowAreaInnerRectString == value)
         {
            return;
         }
         this._maskShowAreaInnerRectString = value;
         this._maskShowAreaInnerRect = ClassUtils.CreatInstance(ClassUtils.INNERRECTANGLE,ComponentFactory.parasArgs(this._maskShowAreaInnerRectString));
         onPropertiesChanged(P_maskShowAreaInnerRect);
      }
      
      public function get maximum() : int
      {
         return this.getModel().getMaximum();
      }
      
      public function set maximum(maximum:int) : void
      {
         if(this.getModel().getMaximum() == maximum)
         {
            return;
         }
         this.getModel().setMaximum(maximum);
         onPropertiesChanged(P_maximum);
      }
      
      public function get minimum() : int
      {
         return this.getModel().getMinimum();
      }
      
      public function set minimum(minimum:int) : void
      {
         if(this.getModel().getMinimum() == minimum)
         {
            return;
         }
         this.getModel().setMinimum(minimum);
         onPropertiesChanged(P_minimum);
      }
      
      public function get orientation() : int
      {
         return this._orientation;
      }
      
      public function set orientation(ori:int) : void
      {
         if(this._orientation == ori)
         {
            return;
         }
         this._orientation = ori;
         onPropertiesChanged(P_orientation);
      }
      
      public function set progressBar($progressBar:DisplayObject) : void
      {
         if(this._progressBar == $progressBar)
         {
            return;
         }
         this._progressBar = $progressBar;
         onPropertiesChanged(P_progressBar);
      }
      
      public function set progressBarStyle(value:String) : void
      {
         if(this._progressBarStyle == value)
         {
            return;
         }
         this._progressBarStyle = value;
         this.progressBar = ComponentFactory.Instance.creat(this._progressBarStyle);
      }
      
      public function removeStateListener(listener:Function) : void
      {
         removeEventListener(InteractiveEvent.STATE_CHANGED,listener);
      }
      
      public function setupModel(model:BoundedRangeModel) : void
      {
         if(this._model)
         {
            this._model.removeStateListener(this.__onModelChange);
         }
         else
         {
            this._model = model;
         }
         this._model.addStateListener(this.__onModelChange);
      }
      
      public function set thumb($thumb:BaseButton) : void
      {
         if(this._thumb == $thumb)
         {
            return;
         }
         if(this._thumb)
         {
            ObjectUtils.disposeObject(this._thumb);
            this._thumb.removeEventListener(MouseEvent.MOUSE_DOWN,this.__onThumbMouseDown);
         }
         this._thumb = $thumb;
         this._thumb.addEventListener(MouseEvent.MOUSE_DOWN,this.__onThumbMouseDown);
         onPropertiesChanged(P_thumb);
      }
      
      public function set thumbShowInnerRectString(value:String) : void
      {
         if(this._thumbShowInnerRectString == value)
         {
            return;
         }
         this._thumbShowInnerRectString = value;
         this._thumbShowInnerRect = ClassUtils.CreatInstance(ClassUtils.INNERRECTANGLE,ComponentFactory.parasArgs(this._thumbShowInnerRectString));
         onPropertiesChanged(P_thumbShowInnerRect);
      }
      
      public function set thumbStyle(stylename:String) : void
      {
         if(this._thumbStyle == stylename)
         {
            return;
         }
         this._thumbStyle = stylename;
         this.thumb = ComponentFactory.Instance.creat(this._thumbStyle);
      }
      
      public function get value() : Number
      {
         return this.getModel().getValue();
      }
      
      public function set value($value:Number) : void
      {
         if(this._value == $value)
         {
            return;
         }
         this._value = $value;
         this.getModel().setValue(this._value);
         onPropertiesChanged(P_value);
      }
      
      protected function __onModelChange(event:InteractiveEvent) : void
      {
         dispatchEvent(new InteractiveEvent(InteractiveEvent.STATE_CHANGED));
      }
      
      protected function __onSilderClick(event:MouseEvent) : void
      {
         this.scrollThumbToCurrentMousePosition();
         event.updateAfterEvent();
      }
      
      protected function __onThumbMouseDown(event:MouseEvent) : void
      {
         var mp:Point = getMousePosition();
         var mx:int = mp.x;
         var my:int = mp.y;
         if(this.isVertical())
         {
            this._thumbDownOffset = my - this._thumb.y;
         }
         else
         {
            this._thumbDownOffset = mx - this._thumb.x;
         }
         this._isDragging = true;
         StageReferance.stage.addEventListener(MouseEvent.MOUSE_UP,this.__onThumbMouseUp);
         StageReferance.stage.addEventListener(MouseEvent.MOUSE_MOVE,this.__onThumbMouseMoved);
      }
      
      protected function __onThumbMouseMoved(event:MouseEvent) : void
      {
         this.scrollThumbToCurrentMousePosition();
         event.updateAfterEvent();
      }
      
      protected function __onThumbMouseUp(event:MouseEvent) : void
      {
         StageReferance.stage.removeEventListener(MouseEvent.MOUSE_UP,this.__onThumbMouseUp);
         StageReferance.stage.removeEventListener(MouseEvent.MOUSE_MOVE,this.__onThumbMouseMoved);
      }
      
      override protected function addChildren() : void
      {
         super.addChildren();
         if(this._bar)
         {
            addChild(this._bar);
         }
         if(this._progressBar)
         {
            addChild(this._progressBar);
            addChild(this._maskShape);
         }
         if(this._thumb)
         {
            addChild(this._thumb);
         }
      }
      
      override protected function init() : void
      {
         this.setupModel(new BoundedRangeModel());
         this.setupMask();
         addEventListener(MouseEvent.CLICK,this.__onSilderClick);
         super.init();
      }
      
      override protected function onProppertiesUpdate() : void
      {
         super.onProppertiesUpdate();
         if(_changedPropeties[Component.P_width] || _changedPropeties[Component.P_height])
         {
            this.updateSize();
         }
         if(_changedPropeties[P_value] || _changedPropeties[P_thumbShowInnerRect] || _changedPropeties[P_maskShowAreaInnerRect] || _changedPropeties[Component.P_width] || _changedPropeties[Component.P_height] || _changedPropeties[P_maximum] || _changedPropeties[P_minimum])
         {
            this.updateThumbPos();
            this.updateMask();
            if(this._bar)
            {
               this._bar.width = _width;
               this._bar.height = _height;
            }
         }
      }
      
      protected function scrollThumbToCurrentMousePosition() : void
      {
         var thumbMin:int = 0;
         var thumbMax:int = 0;
         var thumbPos:int = 0;
         var mp:Point = getMousePosition();
         var mx:int = mp.x;
         var my:int = mp.y;
         var thumbScrollRect:Rectangle = this._thumbShowInnerRect.getInnerRect(_width,_height);
         if(this.isVertical())
         {
            thumbMin = thumbScrollRect.y;
            thumbMax = thumbScrollRect.y + thumbScrollRect.height;
            thumbPos = Math.min(thumbMax,Math.max(thumbMin,my - this._thumbDownOffset));
         }
         else
         {
            thumbMin = thumbScrollRect.x;
            thumbMax = thumbScrollRect.x + thumbScrollRect.width;
            thumbPos = Math.min(thumbMax,Math.max(thumbMin,mx - this._thumbDownOffset));
         }
         this.value = this.getValueWithThumbMaxMinPos(thumbMin,thumbMax,thumbPos);
      }
      
      protected function setupMask() : void
      {
         this._maskShape = new Shape();
         this._maskShape.graphics.beginFill(16711680,1);
         this._maskShape.graphics.drawRect(0,0,100,100);
         this._maskShape.graphics.endFill();
      }
      
      protected function updateMask() : void
      {
         if(this._maskShowAreaInnerRect == null)
         {
            return;
         }
         var rect:Rectangle = this._maskShowAreaInnerRect.getInnerRect(_width,_height);
         this._maskShape.x = rect.x;
         this._maskShape.y = rect.y;
         var valuePercent:Number = this.calculateValuePercent();
         if(this.isVertical())
         {
            this._maskShape.height = rect.height * valuePercent;
            this._maskShape.width = rect.width;
         }
         else
         {
            this._maskShape.width = rect.width * valuePercent;
            this._maskShape.height = rect.height;
         }
         if(this._maskShape)
         {
            this._progressBar.mask = this._maskShape;
         }
      }
      
      protected function updateSize() : void
      {
         this._bar.width = _width;
         this._bar.height = _height;
         if(this._progressBar)
         {
            this._progressBar.width = _width;
            this._progressBar.height = _height;
         }
      }
      
      protected function updateThumbPos() : void
      {
         var rect:Rectangle = null;
         var rangePos:int = 0;
         var startPos:int = 0;
         var valuePercent:Number = NaN;
         if(this._thumbShowInnerRect == null)
         {
            return;
         }
         rect = this._thumbShowInnerRect.getInnerRect(_width,_height);
         valuePercent = this.calculateValuePercent();
         if(this.isVertical())
         {
            this._thumb.x = rect.x;
            rangePos = rect.height;
            startPos = rect.y;
            this._thumb.y = Math.round(valuePercent * rangePos) + startPos;
         }
         else
         {
            this._thumb.y = rect.y;
            rangePos = rect.width;
            startPos = rect.x;
            this._thumb.x = Math.round(valuePercent * rangePos) + startPos;
         }
         this._thumb.tipData = this._value;
      }
      
      private function calculateValuePercent() : Number
      {
         var valuePercent:Number = NaN;
         if(this._value < this.minimum || isNaN(this._value))
         {
            this._value = this.minimum;
         }
         if(this._value > this.maximum)
         {
            this._value = this.maximum;
         }
         if(this.maximum > this.minimum)
         {
            valuePercent = (this._value - this.minimum) / (this.maximum - this.minimum);
         }
         else
         {
            valuePercent = 1;
         }
         return valuePercent;
      }
      
      private function getValueWithThumbMaxMinPos(min:Number, max:Number, pos:Number) : Number
      {
         var range:Number = max - min;
         var percent:Number = (pos - min) / range;
         var valueRange:Number = this.maximum - this.minimum;
         return percent * valueRange + this.minimum;
      }
   }
}
