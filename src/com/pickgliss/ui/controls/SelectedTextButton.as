package com.pickgliss.ui.controls
{
   import com.pickgliss.geom.InnerRectangle;
   import com.pickgliss.geom.OuterRectPos;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.utils.ClassUtils;
   import com.pickgliss.utils.ObjectUtils;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   import flash.text.TextField;
   
   public class SelectedTextButton extends SelectedButton
   {
      
      public static const P_backgoundInnerRect:String = "backgoundInnerRect";
      
      public static const P_text:String = "text";
      
      public static const P_selectedTextField:String = "selectedtextField";
      
      public static const P_unSelectedTextField:String = "unselectedtextField";
      
      public static const P_selected:String = "selected";
      
      public static const P_unSelectedButtonOuterRectPos:String = "unSelectedButtonOuterRectPos";
       
      
      protected var _selectedTextField:TextField;
      
      protected var _unSelectedTextField:TextField;
      
      protected var _text:String = "";
      
      protected var _textStyle:String;
      
      protected var _selectedBackgoundInnerRect:InnerRectangle;
      
      protected var _unselectedBackgoundInnerRect:InnerRectangle;
      
      protected var _backgoundInnerRectString:String;
      
      protected var _unSelectedButtonOuterRectPosString:String;
      
      protected var _unSelectedButtonOuterRectPos:OuterRectPos;
      
      public function SelectedTextButton()
      {
         this._selectedBackgoundInnerRect = new InnerRectangle(0,0,0,0,-1);
         this._unselectedBackgoundInnerRect = new InnerRectangle(0,0,0,0,-1);
         super();
      }
      
      public function set unSelectedButtonOuterRectPosString(value:String) : void
      {
         if(this._unSelectedButtonOuterRectPosString == value)
         {
            return;
         }
         this._unSelectedButtonOuterRectPosString = value;
         this._unSelectedButtonOuterRectPos = ClassUtils.CreatInstance(ClassUtils.OUTTERRECPOS,ComponentFactory.parasArgs(this._unSelectedButtonOuterRectPosString));
         onPropertiesChanged(P_unSelectedButtonOuterRectPos);
      }
      
      public function set backgoundInnerRectString(value:String) : void
      {
         if(this._backgoundInnerRectString == value)
         {
            return;
         }
         this._backgoundInnerRectString = value;
         var rectsData:Array = ComponentFactory.parasArgs(this._backgoundInnerRectString);
         if(rectsData.length > 0 && rectsData[0] != "")
         {
            this._selectedBackgoundInnerRect = ClassUtils.CreatInstance(ClassUtils.INNERRECTANGLE,String(rectsData[0]).split("|"));
         }
         if(rectsData.length > 1 && rectsData[1] != "")
         {
            this._unselectedBackgoundInnerRect = ClassUtils.CreatInstance(ClassUtils.INNERRECTANGLE,String(rectsData[1]).split("|"));
         }
         onPropertiesChanged(P_backgoundInnerRect);
      }
      
      override public function dispose() : void
      {
         if(this._selectedTextField)
         {
            ObjectUtils.disposeObject(this._selectedTextField);
         }
         this._selectedTextField = null;
         if(this._unSelectedTextField)
         {
            ObjectUtils.disposeObject(this._unSelectedTextField);
         }
         this._unSelectedTextField = null;
         super.dispose();
      }
      
      public function set text(value:String) : void
      {
         if(this._text == value)
         {
            return;
         }
         this._text = value;
         onPropertiesChanged(P_text);
      }
      
      public function set selectedTextField(field:TextField) : void
      {
         if(this._selectedTextField == field)
         {
            return;
         }
         ObjectUtils.disposeObject(this._selectedTextField);
         this._selectedTextField = field;
         onPropertiesChanged(P_selectedTextField);
      }
      
      public function set unSelectedTextField(field:TextField) : void
      {
         if(this._unSelectedTextField == field)
         {
            return;
         }
         ObjectUtils.disposeObject(this._unSelectedTextField);
         this._unSelectedTextField = field;
         onPropertiesChanged(P_unSelectedTextField);
      }
      
      public function set textStyle(stylename:String) : void
      {
         if(this._textStyle == stylename)
         {
            return;
         }
         this._textStyle = stylename;
         var styles:Array = ComponentFactory.parasArgs(stylename);
         if(styles.length > 0 && styles[0] != "")
         {
            this.selectedTextField = ComponentFactory.Instance.creatComponentByStylename(styles[0]);
         }
         if(styles.length > 1 && styles[1] != "")
         {
            this.unSelectedTextField = ComponentFactory.Instance.creatComponentByStylename(styles[1]);
         }
      }
      
      override protected function addChildren() : void
      {
         super.addChildren();
         if(this._selectedTextField)
         {
            addChild(this._selectedTextField);
         }
         if(this._unSelectedTextField)
         {
            addChild(this._unSelectedTextField);
         }
      }
      
      override public function set selected(value:Boolean) : void
      {
         super.selected = value;
         if(this._selectedTextField)
         {
            this._selectedTextField.visible = value;
         }
         if(this._unSelectedTextField)
         {
            this._unSelectedTextField.visible = !value;
         }
         onPropertiesChanged(P_selected);
      }
      
      override protected function onProppertiesUpdate() : void
      {
         var rectangleSe:Rectangle = null;
         var rectangleUnSe:Rectangle = null;
         super.onProppertiesUpdate();
         if(_selected && this._selectedTextField)
         {
            this._selectedTextField.text = this._text;
         }
         else if(!_selected && this._unSelectedTextField)
         {
            this._unSelectedTextField.text = this._text;
         }
         if(_autoSizeAble)
         {
            if(_selected && this._selectedTextField)
            {
               rectangleSe = this._selectedBackgoundInnerRect.getInnerRect(this._selectedTextField.textWidth,this._selectedTextField.textHeight);
               _width = _selectedButton.width = rectangleSe.width;
               _height = _selectedButton.height = rectangleSe.height;
               this._selectedTextField.x = this._selectedBackgoundInnerRect.para1;
               this._selectedTextField.y = this._selectedBackgoundInnerRect.para3;
            }
            else if(!_selected && this._unSelectedTextField)
            {
               this.upUnselectedButtonPos();
               rectangleUnSe = this._unselectedBackgoundInnerRect.getInnerRect(this._unSelectedTextField.textWidth,this._unSelectedTextField.textHeight);
               _width = _unSelectedButton.width = rectangleUnSe.width;
               _height = _unSelectedButton.height = rectangleUnSe.height;
               this._unSelectedTextField.x = this._unselectedBackgoundInnerRect.para1 + _unSelectedButton.x;
               this._unSelectedTextField.y = this._unselectedBackgoundInnerRect.para3 + _unSelectedButton.y;
            }
            else if(_selected)
            {
               _width = _selectedButton.width;
               _height = _selectedButton.height;
            }
            else
            {
               this.upUnselectedButtonPos();
               _width = _unSelectedButton.width;
               _height = _unSelectedButton.height;
            }
         }
         else
         {
            this.upUnselectedButtonPos();
            _selectedButton.width = _unSelectedButton.width = _width;
            _selectedButton.height = _unSelectedButton.height = _height;
            if(this._selectedTextField)
            {
               this._selectedTextField.x = (_width - this._selectedTextField.textWidth) / 2;
               this._selectedTextField.y = (_height - this._selectedTextField.textHeight) / 2;
            }
            if(this._unSelectedTextField)
            {
               this._unSelectedTextField.x = (_width - this._unSelectedTextField.textWidth) / 2;
               this._unSelectedTextField.y = (_height - this._unSelectedTextField.textHeight) / 2;
            }
         }
      }
      
      private function upUnselectedButtonPos() : void
      {
         if(_unSelectedButton == null || _selectedButton == null)
         {
            return;
         }
         if(this._unSelectedButtonOuterRectPos == null)
         {
            return;
         }
         var posRect:Point = this._unSelectedButtonOuterRectPos.getPos(_unSelectedButton.width,_unSelectedButton.height,_selectedButton.width,_selectedButton.height);
         _unSelectedButton.x = posRect.x;
         _unSelectedButton.y = posRect.y;
      }
   }
}
