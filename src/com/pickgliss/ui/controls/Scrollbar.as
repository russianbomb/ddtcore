package com.pickgliss.ui.controls
{
   import com.pickgliss.events.InteractiveEvent;
   import com.pickgliss.geom.InnerRectangle;
   import com.pickgliss.toplevel.StageReferance;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.ComponentSetting;
   import com.pickgliss.ui.core.Component;
   import com.pickgliss.ui.core.IOrientable;
   import com.pickgliss.utils.ClassUtils;
   import com.pickgliss.utils.DisplayUtils;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.DisplayObject;
   import flash.display.InteractiveObject;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.events.TimerEvent;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   import flash.utils.Timer;
   
   public class Scrollbar extends Component implements IOrientable
   {
      
      public static const HORIZONTAL:int = 1;
      
      public static const P_decreaseButton:String = "decreaseButton";
      
      public static const P_decreaseButtonInnerRect:String = "decreaseButtonInnerRect";
      
      public static const P_increaseButton:String = "increaseButton";
      
      public static const P_increaseButtonInnerRect:String = "increaseButtonInnerRect";
      
      public static const P_maximum:String = "maximum";
      
      public static const P_minimum:String = "minimum";
      
      public static const P_orientation:String = "orientation";
      
      public static const P_scrollValue:String = "scrollValue";
      
      public static const P_thumb:String = "thumb";
      
      public static const P_thumbAreaInnerRect:String = "thumbAreaInnerRect";
      
      public static const P_thumbMinSize:String = "thumbMinSize";
      
      public static const P_track:String = "track";
      
      public static const P_trackInnerRect:String = "trackInnerRect";
      
      public static const P_valueIsAdjusting:String = "valueIsAdjusting";
      
      public static const P_visibleAmount:String = "visibleAmount";
      
      public static const VERTICAL:int = 0;
       
      
      protected var _blockIncrement:int = 20;
      
      protected var _currentTrackClickDirction:int = 0;
      
      protected var _decreaseButton:BaseButton;
      
      protected var _decreaseButtonInnerRect:InnerRectangle;
      
      protected var _decreaseButtonInnerRectString:String;
      
      protected var _decreaseButtonStyle:String;
      
      protected var _increaseButton:BaseButton;
      
      protected var _increaseButtonInnerRect:InnerRectangle;
      
      protected var _increaseButtonInnerRectString:String;
      
      protected var _increaseButtonStyle:String;
      
      protected var _isDragging:Boolean;
      
      protected var _model:BoundedRangeModel;
      
      protected var _orientation:int;
      
      protected var _thumb:BaseButton;
      
      protected var _thumbAreaInnerRect:InnerRectangle;
      
      protected var _thumbAreaInnerRectString:String;
      
      protected var _thumbDownOffset:int;
      
      protected var _thumbMinSize:int;
      
      protected var _thumbRect:Rectangle;
      
      protected var _thumbStyle:String;
      
      protected var _track:DisplayObject;
      
      protected var _trackClickTimer:Timer;
      
      protected var _trackInnerRect:InnerRectangle;
      
      protected var _trackInnerRectString:String;
      
      protected var _trackStyle:String;
      
      protected var _unitIncrement:int = 2;
      
      public function Scrollbar()
      {
         super();
      }
      
      public function addStateListener(listener:Function, priority:int = 0, useWeakReference:Boolean = false) : void
      {
         addEventListener(InteractiveEvent.STATE_CHANGED,listener,false,priority);
      }
      
      public function get blockIncrement() : int
      {
         return this._blockIncrement;
      }
      
      public function set blockIncrement(blockIncrement:int) : void
      {
         this._blockIncrement = blockIncrement;
      }
      
      public function set decreaseButton(display:BaseButton) : void
      {
         if(this._decreaseButton == display)
         {
            return;
         }
         if(this._decreaseButton)
         {
            this._decreaseButton.removeEventListener(Event.CHANGE,this.__increaseButtonClicked);
         }
         ObjectUtils.disposeObject(this._decreaseButton);
         this._decreaseButton = display;
         if(this._decreaseButton)
         {
            this._decreaseButton.pressEnable = true;
         }
         if(this._decreaseButton)
         {
            this._decreaseButton.addEventListener(Event.CHANGE,this.__increaseButtonClicked);
         }
         onPropertiesChanged(P_decreaseButton);
      }
      
      public function set decreaseButtonInnerRectString(value:String) : void
      {
         if(this._decreaseButtonInnerRectString == value)
         {
            return;
         }
         this._decreaseButtonInnerRectString = value;
         this._decreaseButtonInnerRect = ClassUtils.CreatInstance(ClassUtils.INNERRECTANGLE,ComponentFactory.parasArgs(this._decreaseButtonInnerRectString));
         onPropertiesChanged(P_decreaseButtonInnerRect);
      }
      
      public function set decreaseButtonStyle(stylename:String) : void
      {
         if(this._decreaseButtonStyle == stylename)
         {
            return;
         }
         this._decreaseButtonStyle = stylename;
         this.decreaseButton = ComponentFactory.Instance.creat(this._decreaseButtonStyle);
      }
      
      override public function dispose() : void
      {
         if(this._thumb)
         {
            this._thumb.removeEventListener(MouseEvent.MOUSE_DOWN,this.__onThumbDown);
         }
         ObjectUtils.disposeObject(this._thumb);
         this._thumb = null;
         if(this._decreaseButton)
         {
            this._decreaseButton.removeEventListener(Event.CHANGE,this.__decreaseButtonClicked);
         }
         ObjectUtils.disposeObject(this._decreaseButton);
         this._decreaseButton = null;
         if(this._increaseButton)
         {
            this._increaseButton.removeEventListener(Event.CHANGE,this.__increaseButtonClicked);
         }
         ObjectUtils.disposeObject(this._increaseButton);
         this._increaseButton = null;
         StageReferance.stage.removeEventListener(MouseEvent.MOUSE_UP,this.__onThumbUp);
         StageReferance.stage.removeEventListener(MouseEvent.MOUSE_MOVE,this.__onThumbMoved);
         if(this._track && this._track is InteractiveObject)
         {
            this._track.removeEventListener(MouseEvent.MOUSE_DOWN,this.__onTrackClickStart);
            this._track.removeEventListener(MouseEvent.MOUSE_UP,this.__onTrackClickStop);
            this._track.removeEventListener(MouseEvent.MOUSE_OUT,this.__onTrackClickStop);
            this._trackClickTimer.stop();
            this._trackClickTimer.removeEventListener(TimerEvent.TIMER,this.__onTrackPressed);
         }
         ObjectUtils.disposeObject(this._track);
         this._track = null;
         this._trackClickTimer = null;
         super.dispose();
      }
      
      public function set downButtonStyle(stylename:String) : void
      {
         if(this._decreaseButtonStyle == stylename)
         {
            return;
         }
         this._decreaseButtonStyle = stylename;
         this.increaseButton = ComponentFactory.Instance.creat(this._decreaseButtonStyle);
      }
      
      public function getModel() : BoundedRangeModel
      {
         return this._model;
      }
      
      public function getThumbVisible() : Boolean
      {
         return this._thumb.visible;
      }
      
      public function set increaseButton(display:BaseButton) : void
      {
         if(this._increaseButton == display)
         {
            return;
         }
         if(this._increaseButton)
         {
            this._increaseButton.removeEventListener(Event.CHANGE,this.__decreaseButtonClicked);
         }
         ObjectUtils.disposeObject(this._increaseButton);
         this._increaseButton = display;
         if(this._increaseButton)
         {
            this._increaseButton.pressEnable = true;
         }
         if(this._increaseButton)
         {
            this._increaseButton.addEventListener(Event.CHANGE,this.__decreaseButtonClicked);
         }
         onPropertiesChanged(P_increaseButton);
      }
      
      public function set increaseButtonInnerRectString(value:String) : void
      {
         if(this._increaseButtonInnerRectString == value)
         {
            return;
         }
         this._increaseButtonInnerRectString = value;
         this._increaseButtonInnerRect = ClassUtils.CreatInstance(ClassUtils.INNERRECTANGLE,ComponentFactory.parasArgs(this._increaseButtonInnerRectString));
         onPropertiesChanged(P_increaseButtonInnerRect);
      }
      
      public function set increaseButtonStyle(stylename:String) : void
      {
         if(this._increaseButtonStyle == stylename)
         {
            return;
         }
         this._increaseButtonStyle = stylename;
         this.increaseButton = ComponentFactory.Instance.creat(this._increaseButtonStyle);
      }
      
      public function isVertical() : Boolean
      {
         return this._orientation == 0;
      }
      
      public function get maximum() : int
      {
         return this.getModel().getMaximum();
      }
      
      public function set maximum(maximum:int) : void
      {
         if(this.getModel().getMaximum() == maximum)
         {
            return;
         }
         this.getModel().setMaximum(maximum);
         onPropertiesChanged(P_maximum);
      }
      
      public function get minimum() : int
      {
         return this.getModel().getMinimum();
      }
      
      public function set minimum(minimum:int) : void
      {
         if(this.getModel().getMinimum() == minimum)
         {
            return;
         }
         this.getModel().setMinimum(minimum);
         onPropertiesChanged(P_minimum);
      }
      
      public function get orientation() : int
      {
         return this._orientation;
      }
      
      public function set orientation(ori:int) : void
      {
         if(this._orientation == ori)
         {
            return;
         }
         this._orientation = ori;
         onPropertiesChanged(P_orientation);
      }
      
      public function removeStateListener(listener:Function) : void
      {
         removeEventListener(InteractiveEvent.STATE_CHANGED,listener);
      }
      
      public function get scrollValue() : int
      {
         return this.getModel().getValue();
      }
      
      public function set scrollValue(v:int) : void
      {
         this.getModel().setValue(v);
         onPropertiesChanged(P_scrollValue);
      }
      
      public function setupModel(model:BoundedRangeModel) : void
      {
         if(this._model)
         {
            this._model.removeStateListener(this.__onModelChange);
         }
         else
         {
            this._model = model;
         }
         this._model.addStateListener(this.__onModelChange);
      }
      
      public function set thumb(display:BaseButton) : void
      {
         if(this._thumb == display)
         {
            return;
         }
         if(this._thumb)
         {
            this._thumb.removeEventListener(MouseEvent.MOUSE_DOWN,this.__onThumbDown);
         }
         ObjectUtils.disposeObject(this._thumb);
         this._thumb = display;
         if(this._thumb)
         {
            this._thumb.addEventListener(MouseEvent.MOUSE_DOWN,this.__onThumbDown);
         }
         onPropertiesChanged(P_thumb);
      }
      
      public function set thumbAreaInnerRectString(value:String) : void
      {
         if(this._thumbAreaInnerRectString == value)
         {
            return;
         }
         this._thumbAreaInnerRectString = value;
         this._thumbAreaInnerRect = ClassUtils.CreatInstance(ClassUtils.INNERRECTANGLE,ComponentFactory.parasArgs(this._thumbAreaInnerRectString));
         onPropertiesChanged(P_thumbAreaInnerRect);
      }
      
      public function get thumbMinSize() : int
      {
         return this._thumbMinSize;
      }
      
      public function set thumbMinSize(value:int) : void
      {
         if(this._thumbMinSize == value)
         {
            return;
         }
         this._thumbMinSize = value;
         onPropertiesChanged(P_thumbMinSize);
      }
      
      public function set thumbStyle(stylename:String) : void
      {
         if(this._thumbStyle == stylename)
         {
            return;
         }
         this._thumbStyle = stylename;
         this.thumb = ComponentFactory.Instance.creat(this._thumbStyle);
      }
      
      public function set track(display:DisplayObject) : void
      {
         if(this._track == display)
         {
            return;
         }
         if(this._track && this._track is InteractiveObject)
         {
            this._track.removeEventListener(MouseEvent.MOUSE_DOWN,this.__onTrackClickStart);
            this._track.removeEventListener(MouseEvent.MOUSE_UP,this.__onTrackClickStop);
            this._track.removeEventListener(MouseEvent.MOUSE_OUT,this.__onTrackClickStop);
         }
         ObjectUtils.disposeObject(this._track);
         this._track = display;
         if(this._track is InteractiveObject)
         {
            InteractiveObject(this._track).mouseEnabled = true;
         }
         this._track.addEventListener(MouseEvent.MOUSE_DOWN,this.__onTrackClickStart);
         this._track.addEventListener(MouseEvent.MOUSE_UP,this.__onTrackClickStop);
         this._track.addEventListener(MouseEvent.MOUSE_OUT,this.__onTrackClickStop);
         onPropertiesChanged(P_track);
      }
      
      public function set trackInnerRectString(value:String) : void
      {
         if(this._trackInnerRectString == value)
         {
            return;
         }
         this._trackInnerRectString = value;
         this._trackInnerRect = ClassUtils.CreatInstance(ClassUtils.INNERRECTANGLE,ComponentFactory.parasArgs(this._trackInnerRectString));
         onPropertiesChanged(P_trackInnerRect);
      }
      
      public function set trackStyle(stylename:String) : void
      {
         if(this._trackStyle == stylename)
         {
            return;
         }
         this._trackStyle = stylename;
         this.track = ComponentFactory.Instance.creat(this._trackStyle);
      }
      
      public function get unitIncrement() : int
      {
         return this._unitIncrement;
      }
      
      public function set unitIncrement(unitIncrement:int) : void
      {
         this._unitIncrement = unitIncrement;
      }
      
      public function get valueIsAdjusting() : Boolean
      {
         return this.getModel().getValueIsAdjusting();
      }
      
      public function set valueIsAdjusting(b:Boolean) : void
      {
         if(this.getModel().getValueIsAdjusting() == b)
         {
            return;
         }
         this.getModel().setValueIsAdjusting(b);
         onPropertiesChanged(P_valueIsAdjusting);
      }
      
      public function get visibleAmount() : int
      {
         return this.getModel().getExtent();
      }
      
      public function set visibleAmount(extent:int) : void
      {
         if(this.getModel().getExtent() == extent)
         {
            return;
         }
         this.getModel().setExtent(extent);
         onPropertiesChanged(P_visibleAmount);
      }
      
      protected function __decreaseButtonClicked(event:Event) : void
      {
         this.scrollByIncrement(this._unitIncrement);
      }
      
      protected function __increaseButtonClicked(event:Event) : void
      {
         this.scrollByIncrement(-this._unitIncrement);
      }
      
      protected function __onModelChange(event:InteractiveEvent) : void
      {
         dispatchEvent(new InteractiveEvent(InteractiveEvent.STATE_CHANGED));
      }
      
      protected function __onScrollValueChange(event:InteractiveEvent) : void
      {
         if(!this._isDragging)
         {
            this.updatePosByScrollvalue();
         }
      }
      
      protected function __onThumbDown(event:MouseEvent) : void
      {
         this.valueIsAdjusting = true;
         var mp:Point = getMousePosition();
         var mx:int = mp.x;
         var my:int = mp.y;
         if(this.isVertical())
         {
            this._thumbDownOffset = my - this._thumb.y;
         }
         else
         {
            this._thumbDownOffset = mx - this._thumb.x;
         }
         this._isDragging = true;
         StageReferance.stage.addEventListener(MouseEvent.MOUSE_UP,this.__onThumbUp);
         StageReferance.stage.addEventListener(MouseEvent.MOUSE_MOVE,this.__onThumbMoved);
      }
      
      protected function __onThumbMoved(event:MouseEvent) : void
      {
         this.scrollThumbToCurrentMousePosition();
         event.updateAfterEvent();
      }
      
      protected function __onThumbUp(event:MouseEvent) : void
      {
         this._isDragging = false;
         StageReferance.stage.removeEventListener(MouseEvent.MOUSE_UP,this.__onThumbUp);
         StageReferance.stage.removeEventListener(MouseEvent.MOUSE_MOVE,this.__onThumbMoved);
      }
      
      protected function __onTrackClickStart(event:MouseEvent) : void
      {
         this._currentTrackClickDirction = this.getValueWithPosition(getMousePosition()) > this.scrollValue?int(1):int(-1);
         this.scrollToAimPoint(getMousePosition());
         this._trackClickTimer.addEventListener(TimerEvent.TIMER,this.__onTrackPressed);
         this._track.addEventListener(MouseEvent.MOUSE_UP,this.__onTrackClickStop);
         this._track.addEventListener(MouseEvent.MOUSE_OUT,this.__onTrackClickStop);
         this._trackClickTimer.start();
      }
      
      protected function __onTrackClickStop(event:MouseEvent) : void
      {
         this._trackClickTimer.stop();
         this._trackClickTimer.removeEventListener(TimerEvent.TIMER,this.__onTrackPressed);
         this._track.removeEventListener(MouseEvent.MOUSE_UP,this.__onTrackClickStop);
         this._track.removeEventListener(MouseEvent.MOUSE_OUT,this.__onTrackClickStop);
      }
      
      protected function __onTrackPressed(event:TimerEvent) : void
      {
         this.scrollToAimPoint(getMousePosition());
      }
      
      override protected function addChildren() : void
      {
         super.addChildren();
         if(this._track)
         {
            addChild(this._track);
         }
         if(this._increaseButton)
         {
            addChild(this._increaseButton);
         }
         if(this._decreaseButton)
         {
            addChild(this._decreaseButton);
         }
         if(this._thumb)
         {
            addChild(this._thumb);
         }
      }
      
      protected function getValueWithPosition(point:Point) : int
      {
         var thumbMin:int = 0;
         var thumbMax:int = 0;
         var thumbPos:int = 0;
         var mx:int = point.x;
         var my:int = point.y;
         var thumbScrollRect:Rectangle = this._thumbAreaInnerRect.getInnerRect(_width,_height);
         if(this.isVertical())
         {
            thumbMin = thumbScrollRect.y;
            thumbMax = thumbScrollRect.y + thumbScrollRect.height - this._thumbRect.height;
            thumbPos = my;
         }
         else
         {
            thumbMin = thumbScrollRect.x;
            thumbMax = thumbScrollRect.x + thumbScrollRect.width - this._thumbRect.width;
            thumbPos = mx;
         }
         return this.getValueWithThumbMaxMinPos(thumbMin,thumbMax,thumbPos);
      }
      
      override protected function init() : void
      {
         this.setupModel(new BoundedRangeModel());
         this._thumbRect = new Rectangle();
         this._trackClickTimer = new Timer(ComponentSetting.BUTTON_PRESS_STEP_TIME);
         this.addStateListener(this.__onScrollValueChange);
         super.init();
      }
      
      protected function layoutComponent() : void
      {
         DisplayUtils.layoutDisplayWithInnerRect(this._increaseButton,this._increaseButtonInnerRect,_width,_height);
         DisplayUtils.layoutDisplayWithInnerRect(this._decreaseButton,this._decreaseButtonInnerRect,_width,_height);
         DisplayUtils.layoutDisplayWithInnerRect(this._track,this._trackInnerRect,_width,_height);
      }
      
      override protected function onProppertiesUpdate() : void
      {
         super.onProppertiesUpdate();
         if(_changedPropeties[Component.P_height] || _changedPropeties[Component.P_width] || _changedPropeties[P_decreaseButtonInnerRect] || _changedPropeties[P_increaseButtonInnerRect] || _changedPropeties[P_trackInnerRect] || _changedPropeties[P_thumbAreaInnerRect])
         {
            this.layoutComponent();
         }
         if(_changedPropeties[P_maximum] || _changedPropeties[P_minimum] || _changedPropeties[P_scrollValue] || _changedPropeties[P_valueIsAdjusting] || _changedPropeties[P_visibleAmount] || _changedPropeties[P_thumbAreaInnerRect] || _changedPropeties[P_thumbMinSize] || _changedPropeties[P_thumb])
         {
            this.updatePosByScrollvalue();
         }
      }
      
      protected function scrollByIncrement(value:int) : void
      {
         this.scrollValue = this.scrollValue + value;
      }
      
      protected function scrollThumbToCurrentMousePosition() : void
      {
         var thumbMin:int = 0;
         var thumbMax:int = 0;
         var thumbPos:int = 0;
         var mp:Point = getMousePosition();
         var mx:int = mp.x;
         var my:int = mp.y;
         var thumbScrollRect:Rectangle = this._thumbAreaInnerRect.getInnerRect(_width,_height);
         if(this.isVertical())
         {
            thumbMin = thumbScrollRect.y;
            thumbMax = thumbScrollRect.y + thumbScrollRect.height - this._thumbRect.height;
            thumbPos = Math.min(thumbMax,Math.max(thumbMin,my - this._thumbDownOffset));
            this.setThumbPosAndSize(this._thumbRect.x,thumbPos,this._thumbRect.width,this._thumbRect.height);
         }
         else
         {
            thumbMin = thumbScrollRect.x;
            thumbMax = thumbScrollRect.x + thumbScrollRect.width - this._thumbRect.width;
            thumbPos = Math.min(thumbMax,Math.max(thumbMin,mx - this._thumbDownOffset));
            this.setThumbPosAndSize(thumbPos,this._thumbRect.y,this._thumbRect.width,this._thumbRect.height);
         }
         var scrollBarValue:int = this.getValueWithThumbMaxMinPos(thumbMin,thumbMax,thumbPos);
         this.scrollValue = scrollBarValue;
      }
      
      protected function scrollToAimPoint(pt:Point) : void
      {
         var currentIncrement:int = 0;
         var scrollContinueDestination:int = this.getValueWithPosition(pt);
         if(scrollContinueDestination > this.scrollValue && this._currentTrackClickDirction > 0)
         {
            currentIncrement = this.blockIncrement;
         }
         else if(scrollContinueDestination < this.scrollValue && this._currentTrackClickDirction < 0)
         {
            currentIncrement = -this.blockIncrement;
         }
         else
         {
            return;
         }
         this.scrollByIncrement(currentIncrement);
      }
      
      protected function setThumbPosAndSize(posX:int, posY:int, sizeW:int, sizeH:int) : void
      {
         this._thumbRect.x = this._thumb.x = posX;
         this._thumbRect.y = this._thumb.y = posY;
         this._thumbRect.width = this._thumb.width = sizeW;
         this._thumbRect.height = this._thumb.height = sizeH;
      }
      
      protected function updatePosByScrollvalue() : void
      {
         var trackLength:int = 0;
         var thumbLength:int = 0;
         var thumbPos:int = 0;
         var min:int = this.minimum;
         var extent:int = this.visibleAmount;
         var range:int = this.maximum - min;
         var value:int = this.scrollValue;
         var thumbRect:Rectangle = this._thumbAreaInnerRect.getInnerRect(_width,_height);
         if(range <= 0)
         {
            this._thumb.visible = false;
            return;
         }
         if(this.isVertical())
         {
            trackLength = thumbRect.height;
            thumbLength = Math.floor(trackLength * (extent / range));
            this._thumb.visible = thumbLength > 0 && thumbLength < thumbRect.height;
         }
         else
         {
            trackLength = thumbRect.width;
            thumbLength = Math.floor(trackLength * (extent / range));
            this._thumb.visible = thumbLength < thumbRect.width;
         }
         this._increaseButton.mouseEnabled = this._decreaseButton.mouseEnabled = this._thumb.visible;
         if(trackLength > this.thumbMinSize)
         {
            thumbLength = Math.max(thumbLength,this.thumbMinSize);
            this._increaseButton.mouseEnabled = this._decreaseButton.mouseEnabled = this._thumb.visible;
            var thumbRange:int = trackLength - thumbLength;
            if(range - extent == 0)
            {
               thumbPos = 0;
            }
            else
            {
               thumbPos = Math.round(thumbRange * ((value - min) / (range - extent)));
            }
            if(this.isVertical())
            {
               this.setThumbPosAndSize(thumbRect.x,thumbPos + thumbRect.y,thumbRect.width,thumbLength);
            }
            else
            {
               this.setThumbPosAndSize(thumbRect.x + thumbPos,thumbRect.y,thumbLength,thumbRect.height);
            }
            return;
         }
         this._thumb.visible = false;
      }
      
      private function getValueWithThumbMaxMinPos(thumbMin:int, thumbMax:int, thumbPos:int) : int
      {
         var scrollBarValue:int = 0;
         var valueMax:int = 0;
         var valueRange:int = 0;
         var thumbValue:int = 0;
         var thumbRange:int = 0;
         var value:int = 0;
         if(thumbPos >= thumbMax)
         {
            scrollBarValue = this._model.getMaximum() - this._model.getExtent();
         }
         else
         {
            valueMax = this._model.getMaximum() - this._model.getExtent();
            valueRange = valueMax - this._model.getMinimum();
            thumbValue = thumbPos - thumbMin;
            thumbRange = thumbMax - thumbMin;
            value = Math.round(thumbValue / thumbRange * valueRange);
            scrollBarValue = value + this._model.getMinimum();
         }
         return scrollBarValue;
      }
   }
}
