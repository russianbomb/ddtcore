package com.pickgliss.ui.controls
{
   import com.pickgliss.geom.InnerRectangle;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.utils.ClassUtils;
   import com.pickgliss.utils.DisplayUtils;
   import com.pickgliss.utils.ObjectUtils;
   import flash.geom.Rectangle;
   import flash.text.TextField;
   
   public class TextButton extends BaseButton
   {
      
      public static const P_backOuterRect:String = "backOuterRect";
      
      public static const P_text:String = "text";
      
      public static const P_textField:String = "textField";
       
      
      protected var _backgoundInnerRect:InnerRectangle;
      
      protected var _backgoundInnerRectString:String;
      
      protected var _text:String = "";
      
      protected var _textField:TextField;
      
      protected var _textStyle:String;
      
      public function TextButton()
      {
         this._backgoundInnerRect = new InnerRectangle(0,0,0,0,-1);
         super();
      }
      
      public function set backgoundInnerRectString(value:String) : void
      {
         if(this._backgoundInnerRectString == value)
         {
            return;
         }
         this._backgoundInnerRectString = value;
         this._backgoundInnerRect = ClassUtils.CreatInstance(ClassUtils.INNERRECTANGLE,ComponentFactory.parasArgs(this._backgoundInnerRectString));
         onPropertiesChanged(P_backOuterRect);
      }
      
      override public function dispose() : void
      {
         if(this._textField)
         {
            ObjectUtils.disposeObject(this._textField);
         }
         this._textField = null;
         super.dispose();
      }
      
      public function get text() : String
      {
         return this._text;
      }
      
      public function set text(value:String) : void
      {
         if(this._text == value)
         {
            return;
         }
         this._text = value;
         onPropertiesChanged(P_text);
      }
      
      public function set textField(f:TextField) : void
      {
         if(this._textField == f)
         {
            return;
         }
         ObjectUtils.disposeObject(this._textField);
         this._textField = f;
         onPropertiesChanged(P_textField);
      }
      
      public function set textStyle(stylename:String) : void
      {
         if(this._textStyle == stylename)
         {
            return;
         }
         this._textStyle = stylename;
         this.textField = ComponentFactory.Instance.creat(this._textStyle);
      }
      
      override protected function addChildren() : void
      {
         super.addChildren();
         if(this._textField)
         {
            addChild(this._textField);
         }
      }
      
      override protected function onProppertiesUpdate() : void
      {
         var rectangle:Rectangle = null;
         super.onProppertiesUpdate();
         if(this._textField == null)
         {
            return;
         }
         this._textField.text = this._text;
         if(_autoSizeAble)
         {
            rectangle = this._backgoundInnerRect.getInnerRect(this._textField.textWidth,this._textField.textHeight);
            _width = _back.width = rectangle.width;
            _height = _back.height = rectangle.height;
            this._textField.x = this._backgoundInnerRect.para1;
            this._textField.y = this._backgoundInnerRect.para3;
         }
         else
         {
            _back.width = _width;
            _back.height = _height;
            this._textField.x = this._backgoundInnerRect.para1;
            this._textField.y = this._backgoundInnerRect.para3;
         }
      }
      
      override public function setFrame(frameIndex:int) : void
      {
         super.setFrame(frameIndex);
         DisplayUtils.setFrame(this._textField,frameIndex);
      }
   }
}
