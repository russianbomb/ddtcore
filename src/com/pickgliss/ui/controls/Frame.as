package com.pickgliss.ui.controls
{
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.geom.InnerRectangle;
   import com.pickgliss.geom.OuterRectPos;
   import com.pickgliss.toplevel.StageReferance;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Component;
   import com.pickgliss.utils.ClassUtils;
   import com.pickgliss.utils.DisplayUtils;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.DisplayObject;
   import flash.display.InteractiveObject;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.KeyboardEvent;
   import flash.events.MouseEvent;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   import flash.text.TextField;
   import flash.text.TextFieldType;
   import flash.ui.Keyboard;
   
   [Event(name="response",type="com.pickgliss.events.FrameEvent")]
   public class Frame extends Component
   {
      
      public static const P_backgound:String = "backgound";
      
      public static const P_closeButton:String = "closeButton";
      
      public static const P_closeInnerRect:String = "closeInnerRect";
      
      public static const P_containerX:String = "containerX";
      
      public static const P_containerY:String = "containerY";
      
      public static const P_disposeChildren:String = "disposeChildren";
      
      public static const P_moveEnable:String = "moveEnable";
      
      public static const P_moveInnerRect:String = "moveInnerRect";
      
      public static const P_title:String = "title";
      
      public static const P_titleText:String = "titleText";
      
      public static const P_titleOuterRectPos:String = "titleOuterRectPos";
      
      public static const P_escEnable:String = "escEnable";
      
      public static const P_enterEnable:String = "enterEnable";
       
      
      protected var _backStyle:String;
      
      protected var _backgound:DisplayObject;
      
      protected var _closeButton:BaseButton;
      
      protected var _closeInnerRect:InnerRectangle;
      
      protected var _closeInnerRectString:String;
      
      protected var _closestyle:String;
      
      protected var _container:Sprite;
      
      protected var _containerPosString:String;
      
      protected var _containerX:Number;
      
      protected var _containerY:Number;
      
      protected var _moveEnable:Boolean;
      
      protected var _moveInnerRect:InnerRectangle;
      
      protected var _moveInnerRectString:String = "";
      
      protected var _moveRect:Sprite;
      
      protected var _title:TextField;
      
      protected var _titleStyle:String;
      
      protected var _titleText:String = "";
      
      protected var _disposeChildren:Boolean = true;
      
      protected var _titleOuterRectPosString:String;
      
      protected var _titleOuterRectPos:OuterRectPos;
      
      protected var _escEnable:Boolean;
      
      protected var _enterEnable:Boolean;
      
      public function Frame()
      {
         super();
         addEventListener(Event.ADDED_TO_STAGE,this.__onAddToStage);
         addEventListener(MouseEvent.MOUSE_DOWN,this.__onMouseClickSetFocus);
      }
      
      protected function __onMouseClickSetFocus(event:MouseEvent) : void
      {
         StageReferance.stage.focus = event.target as InteractiveObject;
      }
      
      public function addToContent(display:DisplayObject) : void
      {
         this._container.addChild(display);
      }
      
      public function set backStyle(stylename:String) : void
      {
         if(this._backStyle == stylename)
         {
            return;
         }
         this._backStyle = stylename;
         this.backgound = ComponentFactory.Instance.creat(this._backStyle);
      }
      
      public function set backgound(image:DisplayObject) : void
      {
         if(this._backgound == image)
         {
            return;
         }
         ObjectUtils.disposeObject(this._backgound);
         this._backgound = image;
         if(this._backgound is InteractiveObject)
         {
            InteractiveObject(this._backgound).mouseEnabled = true;
         }
         onPropertiesChanged(P_backgound);
      }
      
      public function get closeButton() : BaseButton
      {
         return this._closeButton;
      }
      
      public function set closeButton(button:BaseButton) : void
      {
         if(this._closeButton == button)
         {
            return;
         }
         if(this._closeButton)
         {
            this._closeButton.removeEventListener(MouseEvent.CLICK,this.__onCloseClick);
            ObjectUtils.disposeObject(this._closeButton);
         }
         this._closeButton = button;
         onPropertiesChanged(P_closeButton);
      }
      
      public function set closeInnerRectString(value:String) : void
      {
         if(this._closeInnerRectString == value)
         {
            return;
         }
         this._closeInnerRectString = value;
         this._closeInnerRect = ClassUtils.CreatInstance(ClassUtils.INNERRECTANGLE,ComponentFactory.parasArgs(this._closeInnerRectString));
         onPropertiesChanged(P_closeInnerRect);
      }
      
      public function set closestyle(stylename:String) : void
      {
         if(this._closestyle == stylename)
         {
            return;
         }
         this._closestyle = stylename;
         this.closeButton = ComponentFactory.Instance.creat(this._closestyle);
      }
      
      public function set containerX(value:Number) : void
      {
         if(this._containerX == value)
         {
            return;
         }
         this._containerX = value;
         onPropertiesChanged(P_containerX);
      }
      
      public function set containerY(value:Number) : void
      {
         if(this._containerY == value)
         {
            return;
         }
         this._containerY = value;
         onPropertiesChanged(P_containerY);
      }
      
      public function set titleOuterRectPosString(value:String) : void
      {
         if(this._titleOuterRectPosString == value)
         {
            return;
         }
         this._titleOuterRectPosString = value;
         this._titleOuterRectPos = ClassUtils.CreatInstance(ClassUtils.OUTTERRECPOS,ComponentFactory.parasArgs(this._titleOuterRectPosString));
         onPropertiesChanged(P_titleOuterRectPos);
      }
      
      override public function dispose() : void
      {
         var focusDisplay:DisplayObject = StageReferance.stage.focus as DisplayObject;
         if(focusDisplay && contains(focusDisplay))
         {
            StageReferance.stage.focus = null;
         }
         StageReferance.stage.removeEventListener(MouseEvent.MOUSE_UP,this.__onFrameMoveStop);
         StageReferance.stage.removeEventListener(MouseEvent.MOUSE_MOVE,this.__onMoveWindow);
         StageReferance.stage.removeEventListener(KeyboardEvent.KEY_DOWN,this.__onKeyDown);
         removeEventListener(MouseEvent.MOUSE_DOWN,this.__onMouseClickSetFocus);
         removeEventListener(Event.ADDED_TO_STAGE,this.__onAddToStage);
         if(this._backgound)
         {
            ObjectUtils.disposeObject(this._backgound);
         }
         this._backgound = null;
         if(this._closeButton)
         {
            this._closeButton.removeEventListener(MouseEvent.CLICK,this.__onCloseClick);
            ObjectUtils.disposeObject(this._closeButton);
         }
         this._closeButton = null;
         if(this._title)
         {
            ObjectUtils.disposeObject(this._title);
         }
         this._title = null;
         if(this._disposeChildren)
         {
            ObjectUtils.disposeAllChildren(this._container);
         }
         if(this._container)
         {
            ObjectUtils.disposeObject(this._container);
         }
         this._container = null;
         if(this._moveRect)
         {
            this._moveRect.removeEventListener(MouseEvent.MOUSE_DOWN,this.__onFrameMoveStart);
         }
         ObjectUtils.disposeObject(this._moveRect);
         this._moveRect = null;
         super.dispose();
      }
      
      public function get disposeChildren() : Boolean
      {
         return this._disposeChildren;
      }
      
      public function set disposeChildren(value:Boolean) : void
      {
         if(this._disposeChildren == value)
         {
            return;
         }
         this._disposeChildren = value;
         onPropertiesChanged(P_disposeChildren);
      }
      
      public function set moveEnable(value:Boolean) : void
      {
         if(this._moveEnable == value)
         {
            return;
         }
         this._moveEnable = value;
         onPropertiesChanged(P_moveEnable);
      }
      
      public function set moveInnerRectString(value:String) : void
      {
         if(this._moveInnerRectString == value)
         {
            return;
         }
         this._moveInnerRectString = value;
         this._moveInnerRect = ClassUtils.CreatInstance(ClassUtils.INNERRECTANGLE,ComponentFactory.parasArgs(this._moveInnerRectString));
         onPropertiesChanged(P_moveInnerRect);
      }
      
      public function set title(text:TextField) : void
      {
         if(this._title == text)
         {
            return;
         }
         this._title = text;
         onPropertiesChanged(P_title);
      }
      
      public function set titleStyle(stylename:String) : void
      {
         if(this._titleStyle == stylename)
         {
            return;
         }
         this._titleStyle = stylename;
         this.title = ComponentFactory.Instance.creat(this._titleStyle);
      }
      
      public function set titleText(value:String) : void
      {
         if(this._titleText == value)
         {
            return;
         }
         this._titleText = value;
         onPropertiesChanged(P_titleText);
      }
      
      protected function __onAddToStage(event:Event) : void
      {
         stage.focus = this;
      }
      
      protected function __onCloseClick(event:MouseEvent) : void
      {
         this.onResponse(FrameEvent.CLOSE_CLICK);
      }
      
      protected function __onKeyDown(event:KeyboardEvent) : void
      {
         var focusTarget:DisplayObject = StageReferance.stage.focus as DisplayObject;
         if(DisplayUtils.isTargetOrContain(focusTarget,this))
         {
            if(event.keyCode == Keyboard.ENTER && this.enterEnable)
            {
               if(focusTarget is TextField && TextField(focusTarget).type == TextFieldType.INPUT)
               {
                  return;
               }
               this.onResponse(FrameEvent.ENTER_CLICK);
               event.stopImmediatePropagation();
            }
            else if(event.keyCode == Keyboard.ESCAPE && this.escEnable)
            {
               this.onResponse(FrameEvent.ESC_CLICK);
               event.stopImmediatePropagation();
            }
         }
      }
      
      public function set escEnable(value:Boolean) : void
      {
         if(this._escEnable == value)
         {
            return;
         }
         this._escEnable = value;
         onPropertiesChanged(P_escEnable);
      }
      
      public function get escEnable() : Boolean
      {
         return this._escEnable;
      }
      
      public function set enterEnable(value:Boolean) : void
      {
         if(this._enterEnable == value)
         {
            return;
         }
         this._enterEnable = value;
         onPropertiesChanged(P_enterEnable);
      }
      
      public function get enterEnable() : Boolean
      {
         return this._enterEnable;
      }
      
      protected function onResponse(type:int) : void
      {
         dispatchEvent(new FrameEvent(type));
      }
      
      protected function __onFrameMoveStart(event:MouseEvent) : void
      {
         StageReferance.stage.addEventListener(MouseEvent.MOUSE_MOVE,this.__onMoveWindow);
         StageReferance.stage.addEventListener(MouseEvent.MOUSE_UP,this.__onFrameMoveStop);
         startDrag();
      }
      
      protected function __onFrameMoveStop(event:MouseEvent) : void
      {
         StageReferance.stage.removeEventListener(MouseEvent.MOUSE_UP,this.__onFrameMoveStop);
         StageReferance.stage.removeEventListener(MouseEvent.MOUSE_MOVE,this.__onMoveWindow);
         stopDrag();
      }
      
      override protected function addChildren() : void
      {
         if(this._backgound)
         {
            addChild(this._backgound);
         }
         if(this._title)
         {
            addChild(this._title);
         }
         addChild(this._moveRect);
         addChild(this._container);
         if(this._closeButton)
         {
            addChild(this._closeButton);
         }
      }
      
      override protected function init() : void
      {
         this._container = new Sprite();
         this._moveRect = new Sprite();
         super.init();
      }
      
      override protected function onProppertiesUpdate() : void
      {
         super.onProppertiesUpdate();
         if((_changedPropeties[Component.P_height] || _changedPropeties[Component.P_width]) && this._backgound != null)
         {
            this._backgound.width = _width;
            this._backgound.height = _height;
            this.updateClosePos();
         }
         if(_changedPropeties[Component.P_height] || _changedPropeties[Component.P_width] || _changedPropeties[P_moveInnerRect])
         {
            this.updateMoveRect();
         }
         if(_changedPropeties[P_closeButton])
         {
            this._closeButton.addEventListener(MouseEvent.CLICK,this.__onCloseClick);
         }
         if(_changedPropeties[P_closeButton] || _changedPropeties[P_closeInnerRect])
         {
            this.updateClosePos();
         }
         if(_changedPropeties[P_containerX] || _changedPropeties[P_containerY])
         {
            this.updateContainerPos();
         }
         if(_changedPropeties[P_titleOuterRectPos] || _changedPropeties[P_titleText] || _changedPropeties[Component.P_height] || _changedPropeties[Component.P_width])
         {
            if(this._title != null)
            {
               this._title.text = this._titleText;
            }
            this.updateTitlePos();
         }
         if(_changedPropeties[P_moveEnable])
         {
            if(this._moveEnable)
            {
               this._moveRect.addEventListener(MouseEvent.MOUSE_DOWN,this.__onFrameMoveStart);
            }
            else
            {
               this._moveRect.removeEventListener(MouseEvent.MOUSE_DOWN,this.__onFrameMoveStart);
            }
         }
         if(this._escEnable || this._enterEnable)
         {
            StageReferance.stage.addEventListener(KeyboardEvent.KEY_DOWN,this.__onKeyDown);
         }
         else
         {
            StageReferance.stage.removeEventListener(KeyboardEvent.KEY_DOWN,this.__onKeyDown);
         }
      }
      
      protected function updateClosePos() : void
      {
         if(this._closeButton && this._closeInnerRect)
         {
            DisplayUtils.layoutDisplayWithInnerRect(this._closeButton,this._closeInnerRect,_width,_height);
         }
      }
      
      protected function updateContainerPos() : void
      {
         this._container.x = this._containerX;
         this._container.y = this._containerY;
      }
      
      protected function updateMoveRect() : void
      {
         if(this._moveInnerRect == null)
         {
            return;
         }
         var resultRect:Rectangle = this._moveInnerRect.getInnerRect(_width,_height);
         this._moveRect.graphics.clear();
         this._moveRect.graphics.beginFill(0,0);
         this._moveRect.graphics.drawRect(resultRect.x,resultRect.y,resultRect.width,resultRect.height);
         this._moveRect.graphics.endFill();
      }
      
      protected function updateTitlePos() : void
      {
         var posRect:Point = null;
         if(this._title == null)
         {
            return;
         }
         if(this._titleOuterRectPos == null)
         {
            return;
         }
         posRect = this._titleOuterRectPos.getPos(this._title.width,this._title.height,_width,_height);
         this._title.x = posRect.x;
         this._title.y = posRect.y;
      }
      
      protected function __onMoveWindow(event:MouseEvent) : void
      {
         if(DisplayUtils.isInTheStage(new Point(event.localX,event.localY),this))
         {
            event.updateAfterEvent();
         }
         else
         {
            this.__onFrameMoveStop(null);
         }
      }
   }
}
