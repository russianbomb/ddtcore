package com.pickgliss.ui.controls
{
   import com.pickgliss.events.InteractiveEvent;
   import com.pickgliss.geom.InnerRectangle;
   import com.pickgliss.geom.IntDimension;
   import com.pickgliss.geom.IntPoint;
   import com.pickgliss.geom.IntRectangle;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Component;
   import com.pickgliss.ui.core.IViewprot;
   import com.pickgliss.utils.ClassUtils;
   import com.pickgliss.utils.DisplayUtils;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.DisplayObject;
   import flash.events.MouseEvent;
   import flash.geom.Rectangle;
   
   public class ScrollPanel extends Component
   {
      
      public static const AUTO:int = 1;
      
      public static const OFF:int = 2;
      
      public static const ON:int = 0;
      
      public static const P_backgound:String = "backgound";
      
      public static const P_backgoundInnerRect:String = "backgoundInnerRect";
      
      public static const P_hScrollProxy:String = "hScrollProxy";
      
      public static const P_hScrollbar:String = "hScrollbar";
      
      public static const P_hScrollbarInnerRect:String = "hScrollbarInnerRect";
      
      public static const P_vScrollProxy:String = "vScrollProxy";
      
      public static const P_vScrollbar:String = "vScrollbar";
      
      public static const P_vScrollbarInnerRect:String = "vScrollbarInnerRect";
      
      public static const P_viewSource:String = "viewSource";
      
      public static const P_viewportInnerRect:String = "viewportInnerRect";
       
      
      protected var _backgound:DisplayObject;
      
      protected var _backgoundInnerRect:InnerRectangle;
      
      protected var _backgoundInnerRectString:String;
      
      protected var _backgoundStyle:String;
      
      protected var _hScrollProxy:int;
      
      protected var _hScrollbar:Scrollbar;
      
      protected var _hScrollbarInnerRect:InnerRectangle;
      
      protected var _hScrollbarInnerRectString:String;
      
      protected var _hScrollbarStyle:String;
      
      protected var _vScrollProxy:int;
      
      protected var _vScrollbar:Scrollbar;
      
      protected var _vScrollbarInnerRect:InnerRectangle;
      
      protected var _vScrollbarInnerRectString:String;
      
      protected var _vScrollbarStyle:String;
      
      protected var _viewSource:IViewprot;
      
      protected var _viewportInnerRect:InnerRectangle;
      
      protected var _viewportInnerRectString:String;
      
      public function ScrollPanel(creatDefauleViewport:Boolean = true)
      {
         super();
         if(creatDefauleViewport)
         {
            this._viewSource = new DisplayObjectViewport();
            this._viewSource.addStateListener(this.__onViewportStateChanged);
         }
      }
      
      public function set backgound(back:DisplayObject) : void
      {
         if(this._backgound == back)
         {
            return;
         }
         if(this._backgound)
         {
            ObjectUtils.disposeObject(this._backgound);
         }
         this._backgound = back;
         onPropertiesChanged(P_backgound);
      }
      
      public function set backgoundInnerRectString(value:String) : void
      {
         if(this._backgoundInnerRectString == value)
         {
            return;
         }
         this._backgoundInnerRectString = value;
         this._backgoundInnerRect = ClassUtils.CreatInstance(ClassUtils.INNERRECTANGLE,ComponentFactory.parasArgs(this._backgoundInnerRectString));
         onPropertiesChanged(P_backgoundInnerRect);
      }
      
      public function set backgoundStyle(stylename:String) : void
      {
         if(this._backgoundStyle == stylename)
         {
            return;
         }
         this._backgoundStyle = stylename;
         this.backgound = ComponentFactory.Instance.creat(this._backgoundStyle);
      }
      
      public function get displayObjectViewport() : DisplayObjectViewport
      {
         return this._viewSource as DisplayObjectViewport;
      }
      
      override public function dispose() : void
      {
         if(this._vScrollbar)
         {
            this._vScrollbar.removeStateListener(this.__onScrollValueChange);
            ObjectUtils.disposeObject(this._vScrollbar);
         }
         this._vScrollbar = null;
         if(this._hScrollbar)
         {
            this._hScrollbar.removeStateListener(this.__onScrollValueChange);
            ObjectUtils.disposeObject(this._hScrollbar);
         }
         this._hScrollbar = null;
         if(this._backgound)
         {
            ObjectUtils.disposeObject(this._backgound);
         }
         this._backgound = null;
         if(this._viewSource)
         {
            ObjectUtils.disposeObject(this._viewSource);
         }
         this._viewSource = null;
         removeEventListener(MouseEvent.MOUSE_WHEEL,this.__onMouseWheel);
         super.dispose();
      }
      
      public function getShowHScrollbarExtendHeight() : Number
      {
         var rect:Rectangle = null;
         if(_height == 0 || this._hScrollbarInnerRect == null)
         {
            return 0;
         }
         var viewPortRect:Rectangle = this._viewportInnerRect.getInnerRect(_width,_height);
         var hbarRect:Rectangle = this._hScrollbarInnerRect.getInnerRect(_width,_height);
         rect = viewPortRect.union(hbarRect);
         return viewPortRect.height - rect.height;
      }
      
      public function getVisibleRect() : IntRectangle
      {
         var vStartValue:int = 0;
         var hStartValue:int = 0;
         var vAmount:int = 0;
         var hAmount:int = 0;
         var viewSize:IntDimension = this._viewSource.getViewSize();
         if(this._hScrollbar == null)
         {
            hStartValue = 0;
            hAmount = viewSize.width;
         }
         else
         {
            hStartValue = this._hScrollbar.scrollValue;
            hAmount = this._hScrollbar.visibleAmount;
         }
         if(this._vScrollbar == null)
         {
            vStartValue = 0;
            vAmount = viewSize.height;
         }
         else
         {
            vStartValue = this._vScrollbar.scrollValue;
            vAmount = this._vScrollbar.visibleAmount;
         }
         return new IntRectangle(hStartValue,vStartValue,hAmount,vAmount);
      }
      
      public function set hScrollProxy(proxy:int) : void
      {
         if(this._hScrollProxy == proxy)
         {
            return;
         }
         this._hScrollProxy = proxy;
         onPropertiesChanged(P_hScrollProxy);
      }
      
      public function get hScrollbar() : Scrollbar
      {
         return this._hScrollbar;
      }
      
      public function set hScrollbar(bar:Scrollbar) : void
      {
         if(this._hScrollbar == bar)
         {
            return;
         }
         if(this._hScrollbar)
         {
            this._hScrollbar.removeStateListener(this.__onScrollValueChange);
            ObjectUtils.disposeObject(this._hScrollbar);
         }
         this._hScrollbar = bar;
         this._hScrollbar.addStateListener(this.__onScrollValueChange);
         onPropertiesChanged(P_hScrollbar);
      }
      
      public function set hScrollbarInnerRectString(value:String) : void
      {
         if(this._hScrollbarInnerRectString == value)
         {
            return;
         }
         this._hScrollbarInnerRectString = value;
         this._hScrollbarInnerRect = ClassUtils.CreatInstance(ClassUtils.INNERRECTANGLE,ComponentFactory.parasArgs(this._hScrollbarInnerRectString));
         onPropertiesChanged(P_hScrollbarInnerRect);
      }
      
      public function set hScrollbarStyle(stylename:String) : void
      {
         if(this._hScrollbarStyle == stylename)
         {
            return;
         }
         this._hScrollbarStyle = stylename;
         this.hScrollbar = ComponentFactory.Instance.creat(this._hScrollbarStyle);
      }
      
      public function set hUnitIncrement(unitIncrement:int) : void
      {
         this._viewSource.horizontalUnitIncrement = unitIncrement;
      }
      
      public function invalidateViewport(toBottom:Boolean = false) : void
      {
         var max:int = 0;
         var pt:IntPoint = null;
         if(this._viewSource is DisplayObjectViewport)
         {
            if(toBottom)
            {
               this.displayObjectViewport.invalidateView();
               max = this.viewPort.getViewSize().height;
               pt = new IntPoint(0,max);
               this.viewPort.viewPosition = pt;
            }
            else
            {
               this.displayObjectViewport.invalidateView();
            }
         }
         else
         {
            trace("不允许对其他类型的viewport进行此操作");
         }
      }
      
      public function setView(view:DisplayObject) : void
      {
         if(this._viewSource is DisplayObjectViewport)
         {
            this.displayObjectViewport.setView(view);
         }
         else
         {
            trace("不允许对其他类型的viewport进行此操作");
         }
      }
      
      public function set vScrollProxy(proxy:int) : void
      {
         if(this._vScrollProxy == proxy)
         {
            return;
         }
         this._vScrollProxy = proxy;
         onPropertiesChanged(P_vScrollProxy);
      }
      
      public function get vScrollbar() : Scrollbar
      {
         return this._vScrollbar;
      }
      
      public function set vScrollbar(bar:Scrollbar) : void
      {
         if(this._vScrollbar == bar)
         {
            return;
         }
         if(this._vScrollbar)
         {
            this._vScrollbar.removeStateListener(this.__onScrollValueChange);
            ObjectUtils.disposeObject(this._vScrollbar);
         }
         this._vScrollbar = bar;
         this._vScrollbar.addStateListener(this.__onScrollValueChange);
         onPropertiesChanged(P_vScrollbar);
      }
      
      public function set vScrollbarInnerRectString(value:String) : void
      {
         if(this._vScrollbarInnerRectString == value)
         {
            return;
         }
         this._vScrollbarInnerRectString = value;
         this._vScrollbarInnerRect = ClassUtils.CreatInstance(ClassUtils.INNERRECTANGLE,ComponentFactory.parasArgs(this._vScrollbarInnerRectString));
         onPropertiesChanged(P_vScrollbarInnerRect);
      }
      
      public function set vScrollbarStyle(stylename:String) : void
      {
         if(this._vScrollbarStyle == stylename)
         {
            return;
         }
         this._vScrollbarStyle = stylename;
         this.vScrollbar = ComponentFactory.Instance.creat(this._vScrollbarStyle);
      }
      
      public function set vUnitIncrement(unitIncrement:int) : void
      {
         this._viewSource.verticalUnitIncrement = unitIncrement;
      }
      
      public function get viewPort() : IViewprot
      {
         return this._viewSource;
      }
      
      public function set viewPort(v:IViewprot) : void
      {
         if(this._viewSource == v)
         {
            return;
         }
         if(this._viewSource)
         {
            this._viewSource.removeStateListener(this.__onViewportStateChanged);
            ObjectUtils.disposeObject(this._viewSource);
         }
         this._viewSource = v;
         this._viewSource.addStateListener(this.__onViewportStateChanged);
         onPropertiesChanged(P_viewSource);
      }
      
      public function set viewportInnerRectString(value:String) : void
      {
         if(this._viewportInnerRectString == value)
         {
            return;
         }
         this._viewportInnerRectString = value;
         this._viewportInnerRect = ClassUtils.CreatInstance(ClassUtils.INNERRECTANGLE,ComponentFactory.parasArgs(this._viewportInnerRectString));
         onPropertiesChanged(P_viewportInnerRect);
      }
      
      protected function __onMouseWheel(event:MouseEvent) : void
      {
         var viewPos:IntPoint = this._viewSource.viewPosition.clone();
         viewPos.y = viewPos.y - event.delta * this._viewSource.verticalUnitIncrement;
         this._viewSource.viewPosition = viewPos;
         event.stopImmediatePropagation();
      }
      
      public function setViewPosition(offset:int) : void
      {
         var viewPos:IntPoint = this._viewSource.viewPosition.clone();
         viewPos.y = viewPos.y + offset * this._viewSource.verticalUnitIncrement;
         this._viewSource.viewPosition = viewPos;
      }
      
      protected function __onScrollValueChange(event:InteractiveEvent) : void
      {
         this.viewPort.scrollRectToVisible(this.getVisibleRect());
      }
      
      protected function __onViewportStateChanged(event:InteractiveEvent) : void
      {
         this.syncScrollPaneWithViewport();
      }
      
      override protected function addChildren() : void
      {
         super.addChildren();
         if(this._backgound)
         {
            addChild(this._backgound);
         }
         if(this._viewSource)
         {
            addChild(this._viewSource.asDisplayObject());
         }
         if(this._vScrollbar)
         {
            addChild(this._vScrollbar);
         }
         if(this._hScrollbar)
         {
            addChild(this._hScrollbar);
         }
      }
      
      override protected function init() : void
      {
         this.initEvent();
         super.init();
      }
      
      protected function initEvent() : void
      {
         addEventListener(MouseEvent.MOUSE_WHEEL,this.__onMouseWheel);
      }
      
      protected function layoutComponent() : void
      {
         if(this._vScrollbar)
         {
            DisplayUtils.layoutDisplayWithInnerRect(this._vScrollbar,this._vScrollbarInnerRect,_width,_height);
         }
         if(this._hScrollbar)
         {
            DisplayUtils.layoutDisplayWithInnerRect(this._hScrollbar,this._hScrollbarInnerRect,_width,_height);
         }
         if(this._backgound)
         {
            DisplayUtils.layoutDisplayWithInnerRect(this._backgound,this._backgoundInnerRect,_width,_height);
         }
         if(this._viewSource)
         {
            DisplayUtils.layoutDisplayWithInnerRect(this._viewSource.asDisplayObject(),this._viewportInnerRect,_width,_height);
         }
      }
      
      override protected function onProppertiesUpdate() : void
      {
         super.onProppertiesUpdate();
         if(_changedPropeties[Component.P_height] || _changedPropeties[Component.P_width] || _changedPropeties[P_vScrollbarInnerRect] || _changedPropeties[P_hScrollbarInnerRect] || _changedPropeties[P_vScrollbar] || _changedPropeties[P_hScrollbar] || _changedPropeties[P_viewportInnerRect] || _changedPropeties[P_viewSource])
         {
            this.layoutComponent();
         }
         if(_changedPropeties[P_viewSource])
         {
            this.syncScrollPaneWithViewport();
         }
         if(_changedPropeties[P_vScrollProxy] || _changedPropeties[P_hScrollProxy])
         {
            if(this._vScrollbar)
            {
               this._vScrollbar.visible = this._vScrollProxy == ON;
            }
            if(this._hScrollbar)
            {
               this._hScrollbar.visible = this._hScrollProxy == ON;
            }
         }
      }
      
      protected function syncScrollPaneWithViewport() : void
      {
         var extentSize:IntDimension = null;
         var viewSize:IntDimension = null;
         var viewPosition:IntPoint = null;
         var extent:int = 0;
         var max:int = 0;
         var value:int = 0;
         if(this._viewSource != null)
         {
            extentSize = this._viewSource.getExtentSize();
            if(extentSize.width <= 0 || extentSize.height <= 0)
            {
               return;
            }
            viewSize = this._viewSource.getViewSize();
            viewPosition = this._viewSource.viewPosition;
            if(this._vScrollbar != null)
            {
               extent = extentSize.height;
               max = viewSize.height;
               value = Math.max(0,Math.min(viewPosition.y,max - extent));
               this._vScrollbar.unitIncrement = this._viewSource.verticalUnitIncrement;
               this._vScrollbar.blockIncrement = this._viewSource.verticalBlockIncrement;
               this._vScrollbar.getModel().setRangeProperties(value,extent,0,max,false);
            }
            if(this._hScrollbar != null)
            {
               extent = extentSize.width;
               max = viewSize.width;
               value = Math.max(0,Math.min(viewPosition.x,max - extent));
               this._hScrollbar.unitIncrement = this._viewSource.horizontalUnitIncrement;
               this._hScrollbar.blockIncrement = this._viewSource.horizontalBlockIncrement;
               this._hScrollbar.getModel().setRangeProperties(value,extent,0,max,false);
            }
            this.updateAutoHiddenScroll();
         }
      }
      
      private function updateAutoHiddenScroll() : void
      {
         var rect:Rectangle = null;
         var viewPortRect:Rectangle = null;
         var vbarRect:Rectangle = null;
         var hbarRect:Rectangle = null;
         if(this._vScrollbar == null && this._hScrollbar == null)
         {
            return;
         }
         if(this._vScrollbar != null)
         {
            if(this._vScrollProxy == AUTO)
            {
               this._vScrollbar.visible = this._vScrollbar.getThumbVisible();
            }
            if(this._vScrollProxy == AUTO && this._vScrollbar.maximum == 0)
            {
               this._vScrollbar.visible = false;
            }
         }
         if(this._hScrollbar)
         {
            if(this._hScrollProxy == AUTO)
            {
               this._hScrollbar.visible = this._hScrollbar.getThumbVisible();
            }
            if(this._hScrollProxy == AUTO && this._hScrollbar.maximum == 0)
            {
               this._hScrollbar.visible = false;
            }
         }
         if(this._vScrollProxy == AUTO || this._hScrollProxy == AUTO)
         {
            viewPortRect = this._viewportInnerRect.getInnerRect(_width,_height);
            if(this._vScrollbarInnerRect)
            {
               vbarRect = this._vScrollbarInnerRect.getInnerRect(_width,_height);
            }
            if(this._hScrollbarInnerRect)
            {
               hbarRect = this._hScrollbarInnerRect.getInnerRect(_width,_height);
            }
            if(this._vScrollbar != null)
            {
               if(!this._vScrollbar.getThumbVisible() || this._vScrollbar.visible == false)
               {
                  rect = viewPortRect.union(vbarRect);
               }
            }
            if(this._hScrollbar != null)
            {
               if(!this._hScrollbar.getThumbVisible() || this._hScrollbar.visible == false)
               {
                  if(rect)
                  {
                     rect = rect.union(hbarRect);
                  }
                  else
                  {
                     rect = viewPortRect.union(hbarRect);
                  }
               }
            }
            if(rect == null)
            {
               rect = viewPortRect;
            }
            this._viewSource.x = rect.x;
            this._viewSource.y = rect.y;
            this._viewSource.width = rect.width;
            this._viewSource.height = rect.height;
         }
      }
   }
}
