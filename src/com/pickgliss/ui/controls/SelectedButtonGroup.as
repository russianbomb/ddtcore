package com.pickgliss.ui.controls
{
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.events.MouseEvent;
   
   public class SelectedButtonGroup extends EventDispatcher implements Disposeable
   {
       
      
      private var _canUnSelect:Boolean;
      
      private var _currentSelecetdIndex:int = -1;
      
      private var _items:Vector.<ISelectable>;
      
      private var _lastSelectedButton:ISelectable;
      
      private var _mutiSelectCount:int;
      
      public function SelectedButtonGroup(canUnSelect:Boolean = false, mutiSelectCount:int = 1)
      {
         super();
         this._mutiSelectCount = mutiSelectCount;
         this._canUnSelect = canUnSelect;
         this._items = new Vector.<ISelectable>();
      }
      
      public function addSelectItem(item:ISelectable) : void
      {
         item.addEventListener(MouseEvent.CLICK,this.__onItemClicked);
         item.autoSelect = false;
         this._items.push(item);
      }
      
      public function dispose() : void
      {
         for(var i:int = 0; i < this._items.length; )
         {
            this.removeItemByIndex(0);
         }
         this._lastSelectedButton = null;
         this._items = null;
      }
      
      public function getSelectIndexByItem(item:ISelectable) : int
      {
         return this._items.indexOf(item);
      }
      
      public function removeItemByIndex(index:int) : void
      {
         if(index != -1)
         {
            this._items[index].removeEventListener(MouseEvent.CLICK,this.__onItemClicked);
            ObjectUtils.disposeObject(this._items[index]);
            this._items.splice(index,1);
         }
      }
      
      public function removeSelectItem(item:ISelectable) : void
      {
         var index:int = this._items.indexOf(item);
         this.removeItemByIndex(index);
      }
      
      public function get selectIndex() : int
      {
         return this._items.indexOf(this._lastSelectedButton);
      }
      
      public function set selectIndex(index:int) : void
      {
         var item:ISelectable = null;
         if(index == -1)
         {
            this._currentSelecetdIndex = index;
            for each(item in this._items)
            {
               item.selected = false;
            }
            return;
         }
         var changed:Boolean = this._currentSelecetdIndex != index;
         var target:ISelectable = this._items[index];
         if(!target.selected)
         {
            if(this._lastSelectedButton && this.selectedCount == this._mutiSelectCount)
            {
               this._lastSelectedButton.selected = false;
            }
            target.selected = true;
            this._currentSelecetdIndex = index;
            this._lastSelectedButton = target;
         }
         else if(this._canUnSelect)
         {
            target.selected = false;
         }
         if(changed)
         {
            dispatchEvent(new Event(Event.CHANGE));
         }
      }
      
      public function get selectedCount() : int
      {
         var result:int = 0;
         for(var i:int = 0; i < this._items.length; i++)
         {
            if(this._items[i].selected)
            {
               result++;
            }
         }
         return result;
      }
      
      public function set selectedCount(value:int) : void
      {
         this._mutiSelectCount = value;
      }
      
      private function __onItemClicked(event:MouseEvent) : void
      {
         var target:ISelectable = event.currentTarget as ISelectable;
         this.selectIndex = this._items.indexOf(target);
      }
   }
}
