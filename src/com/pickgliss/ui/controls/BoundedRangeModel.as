package com.pickgliss.ui.controls
{
   import com.pickgliss.events.InteractiveEvent;
   import flash.events.EventDispatcher;
   
   public class BoundedRangeModel extends EventDispatcher
   {
       
      
      private var value:int;
      
      private var extent:int;
      
      private var min:int;
      
      private var max:int;
      
      private var isAdjusting:Boolean;
      
      public function BoundedRangeModel(value:int = 0, extent:int = 0, min:int = 0, max:int = 100)
      {
         super();
         this.isAdjusting = false;
         if(max >= min && value >= min && value + extent >= value && value + extent <= max)
         {
            this.value = value;
            this.extent = extent;
            this.min = min;
            this.max = max;
            return;
         }
         throw new RangeError("invalid range properties");
      }
      
      public function getValue() : int
      {
         return this.value;
      }
      
      public function getExtent() : int
      {
         return this.extent;
      }
      
      public function getMinimum() : int
      {
         return this.min;
      }
      
      public function getMaximum() : int
      {
         return this.max;
      }
      
      public function setValue(n:int) : void
      {
         n = Math.min(n,this.max - this.extent);
         var newValue:int = Math.max(n,this.min);
         this.setRangeProperties(newValue,this.extent,this.min,this.max,this.isAdjusting);
      }
      
      public function setExtent(n:int) : void
      {
         var newExtent:int = Math.max(0,n);
         if(this.value + newExtent > this.max)
         {
            newExtent = this.max - this.value;
         }
         this.setRangeProperties(this.value,newExtent,this.min,this.max,this.isAdjusting);
      }
      
      public function setMinimum(n:int) : void
      {
         var newMax:int = Math.max(n,this.max);
         var newValue:int = Math.max(n,this.value);
         var newExtent:int = Math.min(newMax - newValue,this.extent);
         this.setRangeProperties(newValue,newExtent,n,newMax,this.isAdjusting);
      }
      
      public function setMaximum(n:int) : void
      {
         var newMin:int = Math.min(n,this.min);
         var newExtent:int = Math.min(n - newMin,this.extent);
         var newValue:int = Math.min(n - newExtent,this.value);
         this.setRangeProperties(newValue,newExtent,newMin,n,this.isAdjusting);
      }
      
      public function setValueIsAdjusting(b:Boolean) : void
      {
         this.setRangeProperties(this.value,this.extent,this.min,this.max,b);
      }
      
      public function getValueIsAdjusting() : Boolean
      {
         return this.isAdjusting;
      }
      
      public function setRangeProperties(newValue:int, newExtent:int, newMin:int, newMax:int, adjusting:Boolean) : void
      {
         if(newMin > newMax)
         {
            newMin = newMax;
         }
         if(newValue > newMax)
         {
            newMax = newValue;
         }
         if(newValue < newMin)
         {
            newMin = newValue;
         }
         if(newExtent + newValue > newMax)
         {
            newExtent = newMax - newValue;
         }
         if(newExtent < 0)
         {
            newExtent = 0;
         }
         var isChange:Boolean = newValue != this.value || newExtent != this.extent || newMin != this.min || newMax != this.max || adjusting != this.isAdjusting;
         if(isChange)
         {
            this.value = newValue;
            this.extent = newExtent;
            this.min = newMin;
            this.max = newMax;
            this.isAdjusting = adjusting;
            this.fireStateChanged();
         }
      }
      
      public function addStateListener(listener:Function, priority:int = 0, useWeakReference:Boolean = false) : void
      {
         addEventListener(InteractiveEvent.STATE_CHANGED,listener,false,priority);
      }
      
      public function removeStateListener(listener:Function) : void
      {
         removeEventListener(InteractiveEvent.STATE_CHANGED,listener);
      }
      
      protected function fireStateChanged() : void
      {
         dispatchEvent(new InteractiveEvent(InteractiveEvent.STATE_CHANGED));
      }
      
      override public function toString() : String
      {
         var modelString:String = "value=" + this.getValue() + ", " + "extent=" + this.getExtent() + ", " + "min=" + this.getMinimum() + ", " + "max=" + this.getMaximum() + ", " + "adj=" + this.getValueIsAdjusting();
         return "BoundedRangeModel" + "[" + modelString + "]";
      }
   }
}
