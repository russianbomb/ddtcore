package com.pickgliss.ui.controls
{
   import com.pickgliss.geom.InnerRectangle;
   import com.pickgliss.toplevel.StageReferance;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Component;
   import com.pickgliss.utils.ClassUtils;
   import com.pickgliss.utils.DisplayUtils;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.DisplayObject;
   import flash.events.Event;
   import flash.events.FocusEvent;
   import flash.geom.Rectangle;
   import flash.text.TextField;
   import flash.text.TextFieldType;
   
   public class TextInput extends Component
   {
      
      public static const P_back:String = "back";
      
      public static const P_focusBack:String = "focusBack";
      
      public static const P_focusBackgoundInnerRect:String = "focusBackOuterRect";
      
      public static const P_textField:String = "textField";
      
      public static const P_textInnerRect:String = "textInnerRect";
      
      public static const P_backgroundColor:String = "backgroundColor";
      
      public static const P_enable:String = "enable";
       
      
      protected var _back:DisplayObject;
      
      protected var _backStyle:String;
      
      protected var _focusBack:DisplayObject;
      
      protected var _focusBackgoundInnerRect:InnerRectangle;
      
      protected var _focusBackgoundInnerRectString:String;
      
      protected var _focusBackStyle:String;
      
      protected var _textField:TextField;
      
      protected var _textInnerRect:InnerRectangle;
      
      protected var _textInnerRectString:String;
      
      protected var _textStyle:String;
      
      protected var _backgroundColor:uint;
      
      protected var _filterString:String;
      
      protected var _frameFilter:Array;
      
      protected var _enable:Boolean = true;
      
      protected var _currentFrameIndex:int = 1;
      
      public function TextInput()
      {
         super();
      }
      
      public function set back(display:DisplayObject) : void
      {
         if(this._back == display)
         {
            return;
         }
         ObjectUtils.disposeObject(this._back);
         this._back = display;
         onPropertiesChanged(P_back);
      }
      
      public function set backStyle(stylename:String) : void
      {
         if(this._backStyle == stylename)
         {
            return;
         }
         this._backStyle = stylename;
         this.back = ComponentFactory.Instance.creat(this._backStyle);
      }
      
      public function set backgroundColor(color:uint) : void
      {
         if(this._back || !color)
         {
            return;
         }
         this._backgroundColor = color;
         onPropertiesChanged(P_backgroundColor);
      }
      
      public function get backgroundColor() : uint
      {
         return this._backgroundColor;
      }
      
      public function set filterString(value:String) : void
      {
         if(this._filterString == value)
         {
            return;
         }
         this._filterString = value;
         this._frameFilter = ComponentFactory.Instance.creatFrameFilters(this._filterString);
      }
      
      public function get enable() : Boolean
      {
         return this._enable;
      }
      
      public function set enable(value:Boolean) : void
      {
         if(this._enable == value)
         {
            return;
         }
         this._enable = value;
         onPropertiesChanged(P_enable);
      }
      
      protected function setFrame(frameIndex:int) : void
      {
         this._currentFrameIndex = frameIndex;
         DisplayUtils.setFrame(this._back,this._currentFrameIndex);
         if(this._frameFilter == null || frameIndex <= 0 || frameIndex > this._frameFilter.length)
         {
            return;
         }
         filters = this._frameFilter[frameIndex - 1];
      }
      
      override public function dispose() : void
      {
         ObjectUtils.disposeObject(this._back);
         this._back = null;
         ObjectUtils.disposeObject(this._focusBack);
         this._focusBack = null;
         if(this._textField)
         {
            this._textField.removeEventListener(FocusEvent.FOCUS_IN,this.__onFocusText);
            this._textField.removeEventListener(FocusEvent.FOCUS_OUT,this.__onFocusText);
         }
         ObjectUtils.disposeObject(this._textField);
         this._textField = null;
         graphics.clear();
         super.dispose();
      }
      
      public function set focusBack(display:DisplayObject) : void
      {
         if(this._focusBack == display)
         {
            return;
         }
         ObjectUtils.disposeObject(this._focusBack);
         this._focusBack = display;
         this._focusBack.visible = false;
         onPropertiesChanged();
      }
      
      public function set focusBackgoundInnerRectString(value:String) : void
      {
         if(this._focusBackgoundInnerRectString == value)
         {
            return;
         }
         this._focusBackgoundInnerRectString = value;
         this._focusBackgoundInnerRect = ClassUtils.CreatInstance(ClassUtils.INNERRECTANGLE,ComponentFactory.parasArgs(this._focusBackgoundInnerRectString));
         onPropertiesChanged(P_focusBackgoundInnerRect);
      }
      
      public function set focusBackStyle(value:String) : void
      {
         if(this._focusBackStyle == value)
         {
            return;
         }
         this._focusBackStyle = value;
         this.focusBack = ComponentFactory.Instance.creat(this._focusBackStyle);
      }
      
      public function get textField() : TextField
      {
         return this._textField;
      }
      
      public function set textField(field:TextField) : void
      {
         if(this._textField == field)
         {
            return;
         }
         ObjectUtils.disposeObject(this._textField);
         this._textField = field;
         onPropertiesChanged(P_textField);
      }
      
      public function set textInnerRectString(value:String) : void
      {
         if(this._textInnerRectString == value)
         {
            return;
         }
         this._textInnerRectString = value;
         this._textInnerRect = ClassUtils.CreatInstance(ClassUtils.INNERRECTANGLE,ComponentFactory.parasArgs(this._textInnerRectString));
         onPropertiesChanged(P_textInnerRect);
      }
      
      public function set textStyle(stylename:String) : void
      {
         if(this._textStyle == stylename)
         {
            return;
         }
         this._textStyle = stylename;
         this.textField = ComponentFactory.Instance.creat(this._textStyle);
      }
      
      protected function __onFocusText(event:Event) : void
      {
         if(this._focusBack)
         {
            this._focusBack.visible = event.type == FocusEvent.FOCUS_IN;
         }
      }
      
      override protected function addChildren() : void
      {
         super.addChildren();
         if(this._back)
         {
            addChild(this._back);
         }
         if(this._focusBack)
         {
            addChild(this._focusBack);
         }
         if(this._textField)
         {
            addChild(this._textField);
         }
      }
      
      public function set displayAsPassword(value:Boolean) : void
      {
         this._textField.displayAsPassword = value;
      }
      
      public function get displayAsPassword() : Boolean
      {
         return this._textField.displayAsPassword;
      }
      
      public function set multiline(value:Boolean) : void
      {
         this._textField.multiline = value;
      }
      
      public function get multiline() : Boolean
      {
         return this._textField.multiline;
      }
      
      public function set maxChars(value:int) : void
      {
         this._textField.maxChars = value;
      }
      
      public function get maxChars() : int
      {
         return this._textField.maxChars;
      }
      
      public function set autoSize(value:String) : void
      {
         this._textField.autoSize = value;
      }
      
      public function get autoSize() : String
      {
         return this._textField.autoSize;
      }
      
      public function set text(value:String) : void
      {
         this._textField.text = value;
      }
      
      public function get text() : String
      {
         return this._textField.text;
      }
      
      public function setFocus() : void
      {
         StageReferance.stage.focus = this._textField;
      }
      
      override protected function onProppertiesUpdate() : void
      {
         var textInnerRect:Rectangle = null;
         super.onProppertiesUpdate();
         if(_changedPropeties[P_textField])
         {
            this._textField.type = TextFieldType.INPUT;
            this._textField.wordWrap = true;
            this._textField.addEventListener(FocusEvent.FOCUS_IN,this.__onFocusText);
            this._textField.addEventListener(FocusEvent.FOCUS_OUT,this.__onFocusText);
         }
         if(this._back)
         {
            this._back.width = _width;
            this._back.height = _height;
            textInnerRect = this._textInnerRect.getInnerRect(_width,_height);
            this._textField.width = textInnerRect.width;
            this._textField.height = textInnerRect.height;
            this._textField.x = textInnerRect.x;
            this._textField.y = textInnerRect.y;
            if(this._focusBack)
            {
               DisplayUtils.layoutDisplayWithInnerRect(this._focusBack,this._focusBackgoundInnerRect,_width,_height);
            }
         }
         else
         {
            this._textField.width = _width;
            this._textField.height = _height;
         }
         if(_changedPropeties[P_backgroundColor])
         {
            graphics.beginFill(this._backgroundColor);
            graphics.drawRect(0,0,_width,_height);
            graphics.endFill();
         }
         if(_changedPropeties[P_enable])
         {
            mouseChildren = mouseEnabled = this._enable;
            if(this._enable)
            {
               this.setFrame(1);
            }
            else
            {
               this.setFrame(2);
            }
         }
      }
   }
}
