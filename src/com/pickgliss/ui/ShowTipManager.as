package com.pickgliss.ui
{
   import com.pickgliss.toplevel.StageReferance;
   import com.pickgliss.ui.core.Component;
   import com.pickgliss.ui.core.ITipedDisplay;
   import com.pickgliss.ui.core.ITransformableTipedDisplay;
   import com.pickgliss.ui.tip.ITip;
   import com.pickgliss.ui.vo.DirectionPos;
   import com.pickgliss.utils.Directions;
   import com.pickgliss.utils.DisplayUtils;
   import flash.display.DisplayObjectContainer;
   import flash.events.MouseEvent;
   import flash.geom.Point;
   import flash.utils.Dictionary;
   
   public final class ShowTipManager
   {
      
      public static const StartPoint:Point = new Point(0,0);
      
      private static var _instance:ShowTipManager;
       
      
      private var _currentTipObject:ITipedDisplay;
      
      private var _simpleTipset:Object;
      
      private var _tipContainer:DisplayObjectContainer;
      
      private var _tipedObjects:Vector.<ITipedDisplay>;
      
      private var _tips:Dictionary;
      
      private var _updateTempTarget:ITipedDisplay;
      
      public function ShowTipManager()
      {
         super();
         this._tips = new Dictionary();
         this._tipedObjects = new Vector.<ITipedDisplay>();
      }
      
      public static function get Instance() : ShowTipManager
      {
         if(_instance == null)
         {
            _instance = new ShowTipManager();
         }
         return _instance;
      }
      
      public function addTip(tipedDisplay:ITipedDisplay) : void
      {
         if(tipedDisplay == null)
         {
            return;
         }
         this.removeTipedObject(tipedDisplay);
         this._tipedObjects.push(tipedDisplay);
         tipedDisplay.addEventListener(MouseEvent.ROLL_OVER,this.__onOver);
         tipedDisplay.addEventListener(MouseEvent.ROLL_OUT,this.__onOut);
         if(this._currentTipObject == tipedDisplay)
         {
            this.showTip(this._currentTipObject);
         }
      }
      
      public function getTipPosByDirction(tip:ITip, target:ITipedDisplay, direction:int) : Point
      {
         var resultPos:Point = new Point();
         if(direction == Directions.DIRECTION_T)
         {
            resultPos.y = -tip.height - target.tipGapV;
            resultPos.x = (target.width - tip.width) / 2;
         }
         else if(direction == Directions.DIRECTION_L)
         {
            resultPos.x = -tip.width - target.tipGapH;
            resultPos.y = (target.height - tip.height) / 2;
         }
         else if(direction == Directions.DIRECTION_R)
         {
            resultPos.x = target.width + target.tipGapH;
            resultPos.y = (target.height - tip.height) / 2;
         }
         else if(direction == Directions.DIRECTION_B)
         {
            resultPos.y = target.height + target.tipGapV;
            resultPos.x = (target.width - tip.width) / 2;
         }
         else if(direction == Directions.DIRECTION_TL)
         {
            resultPos.y = -tip.height - target.tipGapV;
            resultPos.x = -tip.width - target.tipGapH;
         }
         else if(direction == Directions.DIRECTION_TR)
         {
            resultPos.y = -tip.height - target.tipGapV;
            resultPos.x = target.width + target.tipGapH;
         }
         else if(direction == Directions.DIRECTION_BL)
         {
            resultPos.y = target.height + target.tipGapV;
            resultPos.x = -tip.width - target.tipGapH;
         }
         else if(direction == Directions.DIRECTION_BR)
         {
            resultPos.y = target.height + target.tipGapV;
            resultPos.x = target.width + target.tipGapH;
         }
         return resultPos;
      }
      
      public function hideTip(target:ITipedDisplay) : void
      {
         if(target == null)
         {
            return;
         }
         var tip:ITip = this._tips[target.tipStyle];
         if(tip == null)
         {
            return;
         }
         if(this._tipContainer.contains(tip.asDisplayObject()))
         {
            this._tipContainer.removeChild(tip.asDisplayObject());
         }
         this._currentTipObject = null;
      }
      
      public function removeCurrentTip() : void
      {
         this.hideTip(this._currentTipObject);
      }
      
      public function removeAllTip() : void
      {
         var tip:ITip = null;
         for each(tip in this._tips)
         {
            if(tip.asDisplayObject().parent)
            {
               tip.asDisplayObject().parent.removeChild(tip.asDisplayObject());
            }
         }
      }
      
      public function removeTip(tipedDisplay:ITipedDisplay) : void
      {
         this.removeTipedObject(tipedDisplay);
         if(this._currentTipObject == tipedDisplay)
         {
            this.hideTip(this._currentTipObject);
         }
      }
      
      public function setSimpleTip(target:ITipedDisplay, tipMsg:String = "") : void
      {
         if(this._simpleTipset == null)
         {
            return;
         }
         if(target is Component)
         {
            Component(target).beginChanges();
         }
         target.tipStyle = this._simpleTipset.tipStyle;
         target.tipData = tipMsg;
         target.tipDirctions = this._simpleTipset.tipDirctions;
         target.tipGapV = this._simpleTipset.tipGapV;
         target.tipGapH = this._simpleTipset.tipGapH;
         if(target is Component)
         {
            Component(target).commitChanges();
         }
      }
      
      public function setup() : void
      {
         this._tipContainer = LayerManager.Instance.getLayerByType(LayerManager.GAME_TOP_LAYER);
      }
      
      public function showTip(target:*) : void
      {
         var tip:* = this._tips[target.tipStyle];
         if(target is ITipedDisplay)
         {
            this.setCommonTip(target,tip);
            tip = this._tips[target.tipStyle];
         }
         if(target is ITransformableTipedDisplay)
         {
            tip.tipWidth = target.tipWidth;
            tip.tipHeight = target.tipHeight;
         }
         this.configPosition(target,tip);
         this._currentTipObject = target;
         this._tipContainer.addChild(tip.asDisplayObject());
      }
      
      public function getTipInstanceByStylename(stylename:String) : ITip
      {
         return this._tips[stylename];
      }
      
      public function updatePos() : void
      {
         if(!this._updateTempTarget)
         {
            return;
         }
         this.showTip(this._updateTempTarget);
      }
      
      private function setCommonTip(target:*, tip:*) : void
      {
         if(tip == null)
         {
            if(target.tipStyle == null)
            {
               return;
            }
            tip = this.createTipByStyleName(target.tipStyle);
            if(tip == null)
            {
               return;
            }
         }
         tip.tipData = target.tipData;
      }
      
      public function createTipByStyleName(stylename:String) : *
      {
         this._tips[stylename] = ComponentFactory.Instance.creat(stylename);
         return this._tips[stylename];
      }
      
      private function configPosition(target:*, tip:*) : void
      {
         var startPos:Point = this._tipContainer.globalToLocal(target.localToGlobal(StartPoint));
         var resultPos:Point = new Point();
         var resultDirection:DirectionPos = this.getTipPriorityDirction(tip,target,target.tipDirctions);
         resultPos = this.getTipPosByDirction(tip,target,resultDirection.direction);
         if(resultDirection.offsetX < int.MAX_VALUE / 2)
         {
            tip.x = resultPos.x + startPos.x + resultDirection.offsetX;
         }
         else
         {
            tip.x = resultPos.x + startPos.x;
         }
         if(resultDirection.offsetY < int.MAX_VALUE / 2)
         {
            tip.y = resultPos.y + startPos.y + resultDirection.offsetY;
         }
         else
         {
            tip.y = resultPos.y + startPos.y;
         }
      }
      
      private function __onOut(event:MouseEvent) : void
      {
         var target:ITipedDisplay = event.currentTarget as ITipedDisplay;
         this.hideTip(target);
         this._updateTempTarget = null;
      }
      
      private function __onOver(event:MouseEvent) : void
      {
         var target:ITipedDisplay = event.currentTarget as ITipedDisplay;
         if(target.tipStyle == null)
         {
            return;
         }
         this.showTip(target);
         this._updateTempTarget = target;
      }
      
      private function getTipPriorityDirction(tip:ITip, target:ITipedDisplay, directions:String) : DirectionPos
      {
         var resultDirectionPos:DirectionPos = null;
         var ordinaryPos:Point = null;
         var resultStartPos:Point = null;
         var resultEndPos:Point = null;
         var directionPos:DirectionPos = null;
         var dirs:Array = directions.split(",");
         var tempDirectionPos:Vector.<DirectionPos> = new Vector.<DirectionPos>();
         var startPos:Point = this._tipContainer.globalToLocal(target.localToGlobal(StartPoint));
         for(var i:int = 0; i < dirs.length; i++)
         {
            ordinaryPos = this.getTipPosByDirction(tip,target,dirs[i]);
            resultStartPos = new Point(ordinaryPos.x + startPos.x,ordinaryPos.y + startPos.y);
            resultEndPos = new Point(ordinaryPos.x + startPos.x + tip.width,ordinaryPos.y + startPos.y + tip.height);
            directionPos = this.creatDirectionPos(resultStartPos,resultEndPos,int(dirs[i]));
            if(directionPos.offsetX == 0 && directionPos.offsetY == 0)
            {
               resultDirectionPos = directionPos;
               break;
            }
            tempDirectionPos.push(directionPos);
         }
         if(resultDirectionPos == null)
         {
            resultDirectionPos = this.searchFixedDirectionPos(tempDirectionPos);
         }
         return resultDirectionPos;
      }
      
      private function searchFixedDirectionPos(tempDirections:Vector.<DirectionPos>) : DirectionPos
      {
         var result:DirectionPos = null;
         var current:int = 0;
         var last:int = 0;
         var reverDirections:Vector.<DirectionPos> = tempDirections.reverse();
         for(var i:int = 0; i < reverDirections.length; i++)
         {
            if(result == null)
            {
               result = reverDirections[i];
            }
            else
            {
               current = Math.abs(reverDirections[i].offsetX) + Math.abs(reverDirections[i].offsetY);
               last = Math.abs(result.offsetX) + Math.abs(result.offsetY);
               if(current <= last)
               {
                  result = reverDirections[i];
               }
            }
         }
         return result;
      }
      
      private function creatDirectionPos(startPos:Point, endPos:Point, direction:int) : DirectionPos
      {
         var directionPos:DirectionPos = new DirectionPos();
         directionPos.direction = direction;
         if(direction == Directions.DIRECTION_T)
         {
            if(startPos.y < 0)
            {
               directionPos.offsetY = int.MAX_VALUE / 2;
            }
            else
            {
               directionPos.offsetY = 0;
            }
            if(startPos.x < 0)
            {
               directionPos.offsetX = -startPos.x;
            }
            else if(endPos.x > StageReferance.stageWidth)
            {
               directionPos.offsetX = StageReferance.stageWidth - endPos.x;
            }
            else
            {
               directionPos.offsetX = 0;
            }
         }
         else if(direction == Directions.DIRECTION_L)
         {
            if(startPos.x < 0)
            {
               directionPos.offsetX = int.MAX_VALUE / 2;
            }
            else
            {
               directionPos.offsetX = 0;
            }
            if(startPos.y < 0)
            {
               directionPos.offsetY = -startPos.y;
            }
            else if(endPos.y > StageReferance.stageHeight)
            {
               directionPos.offsetY = StageReferance.stageHeight - endPos.y;
            }
            else
            {
               directionPos.offsetY = 0;
            }
         }
         else if(direction == Directions.DIRECTION_R)
         {
            if(endPos.x > StageReferance.stageWidth)
            {
               directionPos.offsetX = int.MAX_VALUE / 2;
            }
            else
            {
               directionPos.offsetX = 0;
            }
            if(startPos.y < 0)
            {
               directionPos.offsetY = -startPos.y;
            }
            else if(endPos.y > StageReferance.stageHeight)
            {
               directionPos.offsetY = StageReferance.stageHeight - endPos.y;
            }
            else
            {
               directionPos.offsetY = 0;
            }
         }
         else if(direction == Directions.DIRECTION_B)
         {
            if(endPos.y > StageReferance.stageHeight)
            {
               directionPos.offsetY = int.MAX_VALUE / 2;
            }
            else
            {
               directionPos.offsetY = 0;
            }
            if(startPos.x < 0)
            {
               directionPos.offsetX = -startPos.x;
            }
            else if(endPos.x > StageReferance.stageWidth)
            {
               directionPos.offsetX = StageReferance.stageWidth - endPos.x;
            }
            else
            {
               directionPos.offsetX = 0;
            }
         }
         else if(DisplayUtils.isInTheStage(startPos) && DisplayUtils.isInTheStage(endPos))
         {
            directionPos.offsetX = 0;
            directionPos.offsetY = 0;
         }
         else
         {
            directionPos.offsetY = int.MAX_VALUE / 2;
            directionPos.offsetX = int.MAX_VALUE / 2;
         }
         return directionPos;
      }
      
      private function removeTipedObject(tipedDisplay:ITipedDisplay) : void
      {
         var index:int = this._tipedObjects.indexOf(tipedDisplay);
         tipedDisplay.removeEventListener(MouseEvent.ROLL_OVER,this.__onOver);
         tipedDisplay.removeEventListener(MouseEvent.ROLL_OUT,this.__onOut);
         if(index != -1)
         {
            this._tipedObjects.splice(index,1);
         }
      }
   }
}
