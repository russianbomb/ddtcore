package com.pickgliss.geom
{
   import flash.geom.Point;
   
   public class OuterRectPos
   {
       
      
      private var _posX:int;
      
      private var _posY:int;
      
      private var _lockDirection:int;
      
      public function OuterRectPos(posX:int, posY:int, lockDirection:int)
      {
         super();
         this._posX = posX;
         this._posY = posY;
         this._lockDirection = lockDirection;
      }
      
      public function getPos(sourceW:int, sourceH:int, outerW:int, outerH:int) : Point
      {
         var result:Point = new Point();
         if(this._lockDirection == LockDirectionTypes.LOCK_T)
         {
            result.x = (outerW - sourceW) / 2 + this._posX;
            result.y = this._posY;
         }
         else if(this._lockDirection == LockDirectionTypes.LOCK_TL)
         {
            result.x = this._posX;
            result.y = this._posY;
         }
         else if(this._lockDirection == LockDirectionTypes.LOCK_TR)
         {
         }
         return result;
      }
   }
}
