package com.pickgliss.geom
{
   import flash.geom.Point;
   
   public class IntPoint
   {
       
      
      public var x:int = 0;
      
      public var y:int = 0;
      
      public function IntPoint(x:int = 0, y:int = 0)
      {
         super();
         this.x = x;
         this.y = y;
      }
      
      public static function creatWithPoint(p:Point) : IntPoint
      {
         return new IntPoint(p.x,p.y);
      }
      
      public function toPoint() : Point
      {
         return new Point(this.x,this.y);
      }
      
      public function setWithPoint(p:Point) : void
      {
         this.x = p.x;
         this.y = p.y;
      }
      
      public function setLocation(p:IntPoint) : void
      {
         this.x = p.x;
         this.y = p.y;
      }
      
      public function setLocationXY(x:int = 0, y:int = 0) : void
      {
         this.x = x;
         this.y = y;
      }
      
      public function move(dx:int, dy:int) : IntPoint
      {
         this.x = this.x + dx;
         this.y = this.y + dy;
         return this;
      }
      
      public function moveRadians(direction:int, distance:int) : IntPoint
      {
         this.x = this.x + Math.round(Math.cos(direction) * distance);
         this.y = this.y + Math.round(Math.sin(direction) * distance);
         return this;
      }
      
      public function nextPoint(direction:Number, distance:Number) : IntPoint
      {
         return new IntPoint(this.x + Math.cos(direction) * distance,this.y + Math.sin(direction) * distance);
      }
      
      public function distanceSq(p:IntPoint) : int
      {
         var xx:int = p.x;
         var yy:int = p.y;
         return (this.x - xx) * (this.x - xx) + (this.y - yy) * (this.y - yy);
      }
      
      public function distance(p:IntPoint) : int
      {
         return Math.sqrt(this.distanceSq(p));
      }
      
      public function equals(o:Object) : Boolean
      {
         var toCompare:IntPoint = o as IntPoint;
         if(toCompare == null)
         {
            return false;
         }
         return this.x === toCompare.x && this.y === toCompare.y;
      }
      
      public function clone() : IntPoint
      {
         return new IntPoint(this.x,this.y);
      }
      
      public function toString() : String
      {
         return "IntPoint[" + this.x + "," + this.y + "]";
      }
   }
}
