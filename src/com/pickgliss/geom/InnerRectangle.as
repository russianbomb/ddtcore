package com.pickgliss.geom
{
   import flash.geom.Rectangle;
   
   public class InnerRectangle
   {
       
      
      public var lockDirection:int;
      
      public var para1:int;
      
      public var para2:int;
      
      public var para3:int;
      
      public var para4:int;
      
      private var _outerHeight:int;
      
      private var _outerWidth:int;
      
      private var _resultRect:Rectangle;
      
      public function InnerRectangle(para1:int, para2:int, para3:int, para4:int, lockDirection:int = 0)
      {
         super();
         this.para1 = para1;
         this.para2 = para2;
         this.para3 = para3;
         this.para4 = para4;
         this.lockDirection = lockDirection;
      }
      
      public function equals(innerRect:InnerRectangle) : Boolean
      {
         if(innerRect == null)
         {
            return false;
         }
         return this.para4 == innerRect.para4 && this.para1 == innerRect.para1 && this.lockDirection == innerRect.lockDirection && this.para2 == innerRect.para2 && this.para3 == innerRect.para3;
      }
      
      public function getInnerRect(outerWidth:int, outerHeight:int) : Rectangle
      {
         this._outerWidth = outerWidth;
         this._outerHeight = outerHeight;
         return this.calculateCurrent();
      }
      
      private function calculateCurrent() : Rectangle
      {
         var resultRect:Rectangle = new Rectangle();
         if(this.lockDirection == LockDirectionTypes.UNLOCK)
         {
            resultRect.x = this.para1;
            resultRect.y = this.para3;
            resultRect.width = this._outerWidth - this.para1 - this.para2;
            resultRect.height = this._outerHeight - this.para3 - this.para4;
         }
         else if(this.lockDirection == LockDirectionTypes.LOCK_T)
         {
            resultRect.x = this.para1;
            resultRect.y = this.para3;
            resultRect.width = this._outerWidth - this.para1 - this.para2;
            resultRect.height = this.para4;
         }
         else if(this.lockDirection == LockDirectionTypes.LOCK_L)
         {
            resultRect.x = this.para1;
            resultRect.y = this.para3;
            resultRect.width = this.para2;
            resultRect.height = this._outerHeight - this.para3 - this.para4;
         }
         else if(this.lockDirection == LockDirectionTypes.LOCK_R)
         {
            resultRect.x = this._outerWidth - this.para1 - this.para2;
            resultRect.y = this.para3;
            resultRect.width = this.para1;
            resultRect.height = this._outerHeight - this.para3 - this.para4;
         }
         else if(this.lockDirection == LockDirectionTypes.LOCK_B)
         {
            resultRect.x = this.para1;
            resultRect.y = this._outerHeight - this.para3 - this.para4;
            resultRect.width = this._outerWidth - this.para1 - this.para2;
            resultRect.height = this.para3;
         }
         else if(this.lockDirection == LockDirectionTypes.NO_SCALE_T)
         {
            resultRect.x = (this._outerWidth - this.para1) / 2 + this.para4;
            resultRect.y = this.para3;
            resultRect.width = this.para1;
            resultRect.height = this.para2;
         }
         else if(this.lockDirection == LockDirectionTypes.NO_SCALE_B)
         {
            resultRect.x = (this._outerWidth - this.para1) / 2 + this.para3;
            resultRect.y = this._outerHeight - this.para4 - this.para2;
            resultRect.width = this.para1;
            resultRect.height = this.para2;
         }
         else if(this.lockDirection == LockDirectionTypes.NO_SCALE_L)
         {
            resultRect.x = this.para1;
            resultRect.y = (this._outerHeight - this.para4) / 2 + this.para2;
            resultRect.width = this.para3;
            resultRect.height = this.para4;
         }
         else if(this.lockDirection == LockDirectionTypes.NO_SCALE_R)
         {
            resultRect.x = this._outerWidth - this.para2 - this.para3;
            resultRect.y = (this._outerHeight - this.para3) / 2 + this.para1;
            resultRect.width = this.para3;
            resultRect.height = this.para4;
         }
         else if(this.lockDirection == LockDirectionTypes.NO_SCALE_TL)
         {
            resultRect.x = this.para1;
            resultRect.y = this.para3;
            resultRect.width = this.para2;
            resultRect.height = this.para4;
         }
         else if(this.lockDirection == LockDirectionTypes.NO_SCALE_TR)
         {
            resultRect.x = this._outerWidth - this.para1 - this.para2;
            resultRect.y = this.para3;
            resultRect.width = this.para1;
            resultRect.height = this.para4;
         }
         else if(this.lockDirection == LockDirectionTypes.NO_SCALE_BL)
         {
            resultRect.x = this.para1;
            resultRect.y = this._outerHeight - this.para4 - this.para3;
            resultRect.width = this.para2;
            resultRect.height = this.para3;
         }
         else if(this.lockDirection == LockDirectionTypes.NO_SCALE_BR)
         {
            resultRect.x = this._outerWidth - this.para1 - this.para2;
            resultRect.y = this._outerHeight - this.para4 - this.para3;
            resultRect.width = this.para1;
            resultRect.height = this.para3;
         }
         else if(this.lockDirection == LockDirectionTypes.UNLOCK_OUTSIDE)
         {
            resultRect.x = -this.para1;
            resultRect.y = -this.para3;
            resultRect.width = this._outerWidth + this.para1 + this.para2;
            resultRect.height = this._outerHeight + this.para4 + this.para3;
         }
         else if(this.lockDirection == LockDirectionTypes.NO_SCALE_C)
         {
            resultRect.x = (this._outerWidth - this.para2) / 2 + this.para1;
            resultRect.y = (this._outerHeight - this.para4) / 2 + this.para3;
            resultRect.width = this.para2;
            resultRect.height = this.para4;
         }
         else if(this.lockDirection == LockDirectionTypes.LOCK_TL)
         {
            resultRect.x = this.para1;
            resultRect.y = this.para2;
            resultRect.width = this._outerWidth - this.para3;
            resultRect.height = this._outerHeight - this.para4;
         }
         else if(this.lockDirection == LockDirectionTypes.LOCK_OUTER_DOWN)
         {
            resultRect.x = this.para1;
            resultRect.y = this.para2;
            resultRect.width = this._outerWidth - this.para3;
            resultRect.height = this.para4;
         }
         return resultRect;
      }
   }
}
