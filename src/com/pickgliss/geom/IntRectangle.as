package com.pickgliss.geom
{
   import flash.geom.Rectangle;
   
   public class IntRectangle
   {
       
      
      public var x:int = 0;
      
      public var y:int = 0;
      
      public var width:int = 0;
      
      public var height:int = 0;
      
      public function IntRectangle(x:int = 0, y:int = 0, width:int = 0, height:int = 0)
      {
         super();
         this.setRectXYWH(x,y,width,height);
      }
      
      public static function creatWithRectangle(r:Rectangle) : IntRectangle
      {
         return new IntRectangle(r.x,r.y,r.width,r.height);
      }
      
      public function toRectangle() : Rectangle
      {
         return new Rectangle(this.x,this.y,this.width,this.height);
      }
      
      public function setWithRectangle(r:Rectangle) : void
      {
         this.x = r.x;
         this.y = r.y;
         this.width = r.width;
         this.height = r.height;
      }
      
      public function setRect(rect:IntRectangle) : void
      {
         this.setRectXYWH(rect.x,rect.y,rect.width,rect.height);
      }
      
      public function setRectXYWH(x:int, y:int, width:int, height:int) : void
      {
         this.x = x;
         this.y = y;
         this.width = width;
         this.height = height;
      }
      
      public function setLocation(p:IntPoint) : void
      {
         this.x = p.x;
         this.y = p.y;
      }
      
      public function setSize(size:IntDimension) : void
      {
         this.width = size.width;
         this.height = size.height;
      }
      
      public function getSize() : IntDimension
      {
         return new IntDimension(this.width,this.height);
      }
      
      public function getLocation() : IntPoint
      {
         return new IntPoint(this.x,this.y);
      }
      
      public function union(r:IntRectangle) : IntRectangle
      {
         var x1:int = Math.min(this.x,r.x);
         var x2:int = Math.max(this.x + this.width,r.x + r.width);
         var y1:int = Math.min(this.y,r.y);
         var y2:int = Math.max(this.y + this.height,r.y + r.height);
         return new IntRectangle(x1,y1,x2 - x1,y2 - y1);
      }
      
      public function grow(h:int, v:int) : void
      {
         this.x = this.x - h;
         this.y = this.y - v;
         this.width = this.width + h * 2;
         this.height = this.height + v * 2;
      }
      
      public function move(dx:int, dy:int) : void
      {
         this.x = this.x + dx;
         this.y = this.y + dy;
      }
      
      public function resize(dwidth:int = 0, dheight:int = 0) : void
      {
         this.width = this.width + dwidth;
         this.height = this.height + dheight;
      }
      
      public function leftTop() : IntPoint
      {
         return new IntPoint(this.x,this.y);
      }
      
      public function rightTop() : IntPoint
      {
         return new IntPoint(this.x + this.width,this.y);
      }
      
      public function leftBottom() : IntPoint
      {
         return new IntPoint(this.x,this.y + this.height);
      }
      
      public function rightBottom() : IntPoint
      {
         return new IntPoint(this.x + this.width,this.y + this.height);
      }
      
      public function containsPoint(p:IntPoint) : Boolean
      {
         if(p.x < this.x || p.y < this.y || p.x > this.x + this.width || p.y > this.y + this.height)
         {
            return false;
         }
         return true;
      }
      
      public function equals(o:Object) : Boolean
      {
         var r:IntRectangle = o as IntRectangle;
         if(r == null)
         {
            return false;
         }
         return this.x === r.x && this.y === r.y && this.width === r.width && this.height === r.height;
      }
      
      public function clone() : IntRectangle
      {
         return new IntRectangle(this.x,this.y,this.width,this.height);
      }
      
      public function toString() : String
      {
         return "IntRectangle[x:" + this.x + ",y:" + this.y + ", width:" + this.width + ",height:" + this.height + "]";
      }
   }
}
