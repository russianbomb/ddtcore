package com.pickgliss.utils
{
   import flash.display.DisplayObject;
   import flash.utils.Dictionary;
   import flash.utils.getDefinitionByName;
   import flash.utils.getQualifiedClassName;
   
   public class DisplayPool
   {
      
      private static var _instance:DisplayPool;
       
      
      private var _objects:Dictionary;
      
      public function DisplayPool()
      {
         super();
         this._objects = new Dictionary();
      }
      
      public static function get Instance() : DisplayPool
      {
         if(_instance == null)
         {
            _instance = new DisplayPool();
         }
         return _instance;
      }
      
      public function clearAll() : void
      {
         var key:* = null;
         for(key in this._objects)
         {
            if(this._objects[key])
            {
               while(this._objects[key].length > 0)
               {
                  ObjectUtils.disposeObject(this._objects[key].shift());
               }
            }
            delete this._objects[key];
         }
      }
      
      public function creat(clazz:*) : DisplayObject
      {
         var classname:String = null;
         if(clazz is String)
         {
            classname = clazz;
         }
         else
         {
            classname = getQualifiedClassName(clazz);
         }
         if(this._objects[classname] == null)
         {
            this._objects[classname] = new Vector.<DisplayObject>();
         }
         var objects:Vector.<DisplayObject> = this._objects[classname];
         return this.getFreeObject(objects,classname);
      }
      
      private function getFreeObject(objects:Vector.<DisplayObject>, classname:String) : DisplayObject
      {
         var len:int = objects.length;
         for(var i:int = 0; i < objects.length; i++)
         {
            if(objects[i].parent == null)
            {
               return objects[i];
            }
         }
         var objectClass:Class = getDefinitionByName(classname) as Class;
         var object:* = new objectClass();
         objects.push(object);
         return object;
      }
   }
}
