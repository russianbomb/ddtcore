package com.pickgliss.utils
{
   import com.pickgliss.ui.core.Disposeable;
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.DisplayObject;
   import flash.display.DisplayObjectContainer;
   import flash.geom.Rectangle;
   import flash.utils.ByteArray;
   import flash.utils.Dictionary;
   import flash.utils.describeType;
   import flash.utils.getQualifiedClassName;
   
   public final class ObjectUtils
   {
      
      private static var _copyAbleTypes:Vector.<String>;
      
      private static var _descriptOjbXMLs:Dictionary;
       
      
      public function ObjectUtils()
      {
         super();
      }
      
      public static function cloneSimpleObject(obj:Object) : Object
      {
         var temp:ByteArray = new ByteArray();
         temp.writeObject(obj);
         temp.position = 0;
         return temp.readObject();
      }
      
      public static function copyPorpertiesByXML(object:Object, data:XML) : void
      {
         var item:XML = null;
         var propname:String = null;
         var value:String = null;
         var attr:XMLList = data.attributes();
         for each(item in attr)
         {
            propname = item.name().toString();
            if(object.hasOwnProperty(propname))
            {
               try
               {
                  value = item.toString();
                  if(value == "false")
                  {
                     object[propname] = false;
                  }
                  else
                  {
                     object[propname] = value;
                  }
               }
               catch(e:Error)
               {
                  trace("错误:",e.message);
                  continue;
               }
            }
         }
      }
      
      public static function copyProperties(dest:Object, orig:Object) : void
      {
         var properties:XMLList = null;
         var propertyInfo:XML = null;
         var accessorInfo:XML = null;
         var propertyType:String = null;
         var propertyName:String = null;
         var accessorType:String = null;
         var accessorName:String = null;
         if(_descriptOjbXMLs == null)
         {
            _descriptOjbXMLs = new Dictionary();
         }
         var copyAbleTypes:Vector.<String> = getCopyAbleType();
         var descriXML:XML = describeTypeSave(orig);
         properties = descriXML.variable;
         for each(propertyInfo in properties)
         {
            propertyType = propertyInfo.@type;
            if(copyAbleTypes.indexOf(propertyType) != -1)
            {
               propertyName = propertyInfo.@name;
               if(dest.hasOwnProperty(propertyName))
               {
                  dest[propertyName] = orig[propertyName];
               }
            }
         }
         properties = descriXML.accessor;
         for each(accessorInfo in properties)
         {
            accessorType = accessorInfo.@type;
            if(copyAbleTypes.indexOf(accessorType) != -1)
            {
               accessorName = accessorInfo.@name;
               try
               {
                  dest[accessorName] = orig[accessorName];
               }
               catch(err:Error)
               {
                  continue;
               }
            }
         }
      }
      
      public static function disposeAllChildren(container:DisplayObjectContainer) : void
      {
         var child:DisplayObject = null;
         if(container == null)
         {
            return;
         }
         while(container.numChildren > 0)
         {
            child = container.getChildAt(0);
            ObjectUtils.disposeObject(child);
         }
      }
      
      public static function removeChildAllChildren(container:DisplayObjectContainer) : void
      {
         while(container.numChildren > 0)
         {
            container.removeChildAt(0);
         }
      }
      
      public static function disposeObject(target:Object) : void
      {
         var targetDisplay:DisplayObject = null;
         var targetBitmap:Bitmap = null;
         var targetBitmapData:BitmapData = null;
         var targetDisplay1:DisplayObject = null;
         if(target == null)
         {
            return;
         }
         if(target is Disposeable)
         {
            if(target is DisplayObject)
            {
               targetDisplay = target as DisplayObject;
               if(targetDisplay.parent)
               {
                  targetDisplay.parent.removeChild(targetDisplay);
               }
            }
            Disposeable(target).dispose();
         }
         else if(target is Bitmap)
         {
            targetBitmap = Bitmap(target);
            if(targetBitmap.parent)
            {
               targetBitmap.parent.removeChild(targetBitmap);
            }
            targetBitmap.bitmapData.dispose();
         }
         else if(target is BitmapData)
         {
            targetBitmapData = BitmapData(target);
            targetBitmapData.dispose();
         }
         else if(target is DisplayObject)
         {
            targetDisplay1 = DisplayObject(target);
            if(targetDisplay1.parent)
            {
               targetDisplay1.parent.removeChild(targetDisplay1);
            }
         }
      }
      
      private static function getCopyAbleType() : Vector.<String>
      {
         if(_copyAbleTypes == null)
         {
            _copyAbleTypes = new Vector.<String>();
            _copyAbleTypes.push("int");
            _copyAbleTypes.push("uint");
            _copyAbleTypes.push("String");
            _copyAbleTypes.push("Boolean");
            _copyAbleTypes.push("Number");
         }
         return _copyAbleTypes;
      }
      
      public static function describeTypeSave(obj:Object) : XML
      {
         var result:XML = null;
         var classname:String = getQualifiedClassName(obj);
         if(_descriptOjbXMLs[classname] != null)
         {
            result = _descriptOjbXMLs[classname];
         }
         else
         {
            result = describeType(obj);
            _descriptOjbXMLs[classname] = result;
         }
         return result;
      }
      
      public static function encode(node:String, object:Object) : XML
      {
         var value:Object = null;
         var key:String = null;
         var v:XML = null;
         var temp:String = "<" + node + " ";
         var classInfo:XML = describeTypeSave(object);
         if(classInfo.@name.toString() == "Object")
         {
            for(key in object)
            {
               value = object[key];
               if(!(value is Function))
               {
                  temp = temp + encodingProperty(key,value);
               }
            }
         }
         else
         {
            for each(v in classInfo..*.(name() == "variable" || name() == "accessor"))
            {
               temp = temp + encodingProperty(v.@name.toString(),object[v.@name]);
            }
         }
         temp = temp + "/>";
         return new XML(temp);
      }
      
      private static function encodingProperty(name:String, value:Object) : String
      {
         if(value is Array)
         {
            return "";
         }
         return escapeString(name) + "=\"" + String(value) + "\" ";
      }
      
      private static function escapeString(str:String) : String
      {
         var ch:String = null;
         var hexCode:String = null;
         var zeroPad:String = null;
         var s:String = "";
         var len:Number = str.length;
         for(var i:int = 0; i < len; i++)
         {
            ch = str.charAt(i);
            switch(ch)
            {
               case "\"":
                  s = s + "\\\"";
                  break;
               case "/":
                  s = s + "\\/";
                  break;
               case "\\":
                  s = s + "\\\\";
                  break;
               case "\b":
                  s = s + "\\b";
                  break;
               case "\f":
                  s = s + "\\f";
                  break;
               case "\n":
                  s = s + "\\n";
                  break;
               case "\r":
                  s = s + "\\r";
                  break;
               case "\t":
                  s = s + "\\t";
                  break;
               default:
                  if(ch < " ")
                  {
                     hexCode = ch.charCodeAt(0).toString(16);
                     zeroPad = hexCode.length == 2?"00":"000";
                     s = s + ("\\u" + zeroPad + hexCode);
                  }
                  else
                  {
                     s = s + ch;
                  }
            }
         }
         return s;
      }
      
      public static function modifyVisibility(value:Boolean, ... args) : void
      {
         for(var i:int = 0; i < args.length; i++)
         {
            (args[i] as DisplayObject).visible = value;
         }
      }
      
      public static function copyPropertyByRectangle(source:DisplayObject, rt:Rectangle) : void
      {
         source.x = rt.x;
         source.y = rt.y;
         if(rt.width != 0)
         {
            source.width = rt.width;
         }
         if(rt.height != 0)
         {
            source.height = rt.height;
         }
      }
      
      public static function combineXML(result:XML, data:XML) : void
      {
         var item:XML = null;
         var propname:String = null;
         var value:String = null;
         if(data == null || result == null)
         {
            trace("警告！！！！  combineXML 出现问题  请马上解决");
            return;
         }
         var attr:XMLList = data.attributes();
         for each(item in attr)
         {
            propname = item.name().toString();
            value = item.toString();
            if(!result.hasOwnProperty("@" + propname))
            {
               result["@" + propname] = value;
            }
         }
      }
   }
}
