package com.pickgliss.utils
{
   import com.pickgliss.events.InteractiveEvent;
   import flash.display.InteractiveObject;
   import flash.events.MouseEvent;
   import flash.events.TimerEvent;
   import flash.utils.Timer;
   
   public final class DoubleClickManager
   {
      
      private static var _instance:DoubleClickManager;
       
      
      private const DoubleClickSpeed:uint = 350;
      
      private var _timer:Timer;
      
      private var _currentTarget:InteractiveObject;
      
      private var _ctrlKey:Boolean;
      
      public function DoubleClickManager()
      {
         super();
         this.init();
      }
      
      public static function get Instance() : DoubleClickManager
      {
         if(!_instance)
         {
            _instance = new DoubleClickManager();
         }
         return _instance;
      }
      
      public function enableDoubleClick(target:InteractiveObject) : void
      {
         target.addEventListener(MouseEvent.MOUSE_DOWN,this.__mouseDownHandler);
      }
      
      public function disableDoubleClick(target:InteractiveObject) : void
      {
         target.removeEventListener(MouseEvent.MOUSE_DOWN,this.__mouseDownHandler);
      }
      
      private function init() : void
      {
         this._timer = new Timer(this.DoubleClickSpeed,1);
         this._timer.addEventListener(TimerEvent.TIMER_COMPLETE,this.__timerCompleteHandler);
      }
      
      private function getEvent(type:String) : InteractiveEvent
      {
         var interactiveEvent:InteractiveEvent = new InteractiveEvent(type);
         interactiveEvent.ctrlKey = this._ctrlKey;
         return interactiveEvent;
      }
      
      private function __timerCompleteHandler(evt:TimerEvent) : void
      {
         this._currentTarget.dispatchEvent(this.getEvent(InteractiveEvent.CLICK));
      }
      
      private function __mouseDownHandler(evt:MouseEvent) : void
      {
         this._ctrlKey = evt.ctrlKey;
         if(this._timer.running)
         {
            this._timer.stop();
            if(this._currentTarget != evt.currentTarget)
            {
               return;
            }
            evt.stopImmediatePropagation();
            this._currentTarget.dispatchEvent(this.getEvent(InteractiveEvent.DOUBLE_CLICK));
         }
         else
         {
            this._timer.reset();
            this._timer.start();
            this._currentTarget = evt.currentTarget as InteractiveObject;
         }
      }
      
      public function clearTarget() : void
      {
         if(this._timer)
         {
            this._timer.stop();
         }
         this._currentTarget = null;
      }
   }
}
