package hall
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.PlayerManager;
   import flash.display.MovieClip;
   import flash.events.EventDispatcher;
   import flash.events.TimerEvent;
   import flash.utils.Timer;
   
   public class GameLoadingManager extends EventDispatcher
   {
      
      private static var _instance:GameLoadingManager;
       
      
      private var _loadingMc:MovieClip;
      
      private var _timer:Timer;
      
      public function GameLoadingManager()
      {
         super();
      }
      
      public static function get Instance() : GameLoadingManager
      {
         if(_instance == null)
         {
            _instance = new GameLoadingManager();
         }
         return _instance;
      }
      
      public function show() : void
      {
         this._loadingMc = ComponentFactory.Instance.creat("ddt.hall.GameLoaingMc");
         LayerManager.Instance.addToLayer(this._loadingMc,LayerManager.STAGE_TOP_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
         this._timer = new Timer(1000,10);
         this._timer.addEventListener(TimerEvent.TIMER_COMPLETE,this.__onHide);
         this._timer.start();
      }
      
      public function createRoomComplete() : void
      {
         if(this._timer)
         {
            this._timer.removeEventListener(TimerEvent.TIMER_COMPLETE,this.__onHide);
            this._timer.stop();
            this._timer = null;
         }
      }
      
      protected function __onHide(param1:TimerEvent) : void
      {
         this.hide();
      }
      
      public function hide() : void
      {
         ObjectUtils.disposeObject(this._loadingMc);
         this._loadingMc = null;
         if(this._timer)
         {
            this._timer.removeEventListener(TimerEvent.TIMER_COMPLETE,this.__onHide);
            this._timer.stop();
            this._timer = null;
         }
         PlayerManager.Instance.Self.LastServerId = -1;
      }
   }
}
