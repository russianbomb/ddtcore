package hall
{
   import com.greensock.TweenMax;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.Bitmap;
   import flash.display.DisplayObject;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import flash.geom.Rectangle;
   
   public class IconPanelContent extends Sprite implements Disposeable
   {
      
      public static const LEFT:String = "left";
      
      public static const RIGHT:String = "right";
       
      
      private var _openBtnSprite:Sprite;
      
      private var _openBtnBg:Bitmap;
      
      private var _closeBtnBg:Bitmap;
      
      private var _iconPanelBg:ScaleBitmapImage;
      
      private var _iconArray:Array;
      
      private var _iconSprite:Sprite;
      
      private var rowNum:int;
      
      private var columnWidth:Number;
      
      private var columnHeight:Number;
      
      private var sideSpace:Number;
      
      public var direction:String;
      
      private var iconRectangle:Rectangle;
      
      private var maskSprite:Sprite;
      
      private var tweenMax:TweenMax;
      
      private var isRollOver:Boolean = false;
      
      public function IconPanelContent(param1:String = "left", param2:int = 5, param3:Number = 90, param4:Number = 350, param5:Number = 5)
      {
         super();
         this.direction = param1;
         this.rowNum = param2;
         this.columnWidth = param3;
         this.columnHeight = param4;
         this.sideSpace = param5;
         this.init();
      }
      
      private function init() : void
      {
         this.iconRectangle = new Rectangle(0,0,this.columnWidth,this.columnHeight / this.rowNum);
         this.maskSprite = new Sprite();
         this.maskSprite.graphics.beginFill(0,0.5);
         this.maskSprite.graphics.drawRect(0,0,this.columnWidth,this.columnHeight);
         this.maskSprite.graphics.endFill();
         addChild(this.maskSprite);
         this._iconPanelBg = ComponentFactory.Instance.creat("asset.hall.iconPanelBg");
         this._iconPanelBg.height = this.columnHeight + this.sideSpace;
         this._iconPanelBg.width = this.columnWidth;
         this._iconPanelBg.alpha = 0;
         super.addChild(this._iconPanelBg);
         this._openBtnBg = ComponentFactory.Instance.creatBitmap("asset.hall.iconPanelScollOpenBtnBg");
         this._closeBtnBg = ComponentFactory.Instance.creatBitmap("asset.hall.iconPanelScollCloseBtnBg");
         this._openBtnSprite = new Sprite();
         this._openBtnSprite.addChild(this._openBtnBg);
         this._openBtnSprite.addChild(this._closeBtnBg);
         super.addChild(this._openBtnSprite);
         if(this.direction != LEFT)
         {
            this._openBtnBg.scaleX = -1;
            this._openBtnBg.x = this._openBtnBg.x + this._openBtnBg.width;
            this._closeBtnBg.scaleX = -1;
            this._closeBtnBg.x = this._closeBtnBg.x + this._closeBtnBg.width;
            this._iconPanelBg.scaleX = -1;
            this._iconPanelBg.x = this._iconPanelBg.width;
         }
         addEventListener(MouseEvent.ROLL_OVER,this.__openOverHandler);
         addEventListener(MouseEvent.ROLL_OUT,this.__openOverHandler);
         this._iconArray = new Array();
         this._iconSprite = new Sprite();
         this._iconSprite.y = this.sideSpace;
         super.addChild(this._iconSprite);
         this._iconSprite.mask = this.maskSprite;
         this.isButtonOpen(false);
         this.updateButtonPos();
      }
      
      private function isButtonOpen(param1:Boolean) : void
      {
         if(param1)
         {
            this._openBtnBg.visible = false;
            this._closeBtnBg.visible = true;
         }
         else
         {
            this._openBtnBg.visible = true;
            this._closeBtnBg.visible = false;
         }
      }
      
      private function __openOverHandler(param1:MouseEvent) : void
      {
         if(this.isResetBG())
         {
            this.updateButtonPos();
            return;
         }
         var _loc2_:int = 0;
         if(param1.type == MouseEvent.ROLL_OVER)
         {
            this.isRollOver = true;
            _loc2_ = 1;
         }
         else
         {
            this.isRollOver = false;
         }
         this.isButtonOpen(this.isRollOver);
         this.updateBgAndMask(this.isRollOver,true,_loc2_);
      }
      
      private function updateBgAndMask(param1:Boolean = true, param2:Boolean = false, param3:int = 1) : void
      {
         var _loc6_:Number = NaN;
         var _loc4_:int = (this.validLength(param1) - 1) / this.rowNum;
         var _loc5_:Number = _loc4_ * this.iconRectangle.width;
         _loc6_ = _loc5_ + this.iconRectangle.width;
         if(this.direction == LEFT)
         {
            if(_loc5_ > 0)
            {
               _loc5_ = -_loc5_;
            }
         }
         else if(_loc5_ > 0)
         {
            _loc5_ = _loc5_ + this.iconRectangle.width;
         }
         else
         {
            _loc5_ = this.iconRectangle.width;
         }
         if(param2)
         {
            this.tweenMax = TweenMax.to(this._iconPanelBg,0.5,{
               "alpha":param3,
               "x":_loc5_,
               "width":_loc6_,
               "onUpdate":this.onUpdate,
               "onComplete":this.onComplete
            });
         }
         else
         {
            this._iconPanelBg.x = _loc5_;
            this._iconPanelBg.width = _loc6_;
            this.onUpdate();
         }
      }
      
      private function onUpdate() : void
      {
         if(this.direction == LEFT)
         {
            this.maskSprite.x = this._iconPanelBg.x;
         }
         else
         {
            this.maskSprite.x = this._iconPanelBg.x - this._iconPanelBg.width - 5;
         }
         this.maskSprite.width = this._iconPanelBg.width;
         this.updateButtonPos();
      }
      
      private function onComplete() : void
      {
         this.killTweenMaxs();
      }
      
      private function updateButtonPos() : void
      {
         if(!this._openBtnSprite || !this.maskSprite)
         {
            return;
         }
         if(this.direction == LEFT)
         {
            this._openBtnSprite.x = this.maskSprite.x - this._openBtnSprite.width + 7;
         }
         else
         {
            this._openBtnSprite.x = this.maskSprite.x + this.maskSprite.width - 5;
         }
         this._openBtnSprite.y = (this.maskSprite.height - this._openBtnSprite.height) / 2;
         if(this._iconArray.length > this.rowNum)
         {
            this._openBtnSprite.visible = true;
         }
         else
         {
            this._openBtnSprite.visible = false;
         }
      }
      
      private function updateIconsPos() : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:Sprite = null;
         var _loc1_:int = 0;
         while(_loc1_ < this._iconArray.length)
         {
            _loc2_ = _loc1_ / this.rowNum;
            _loc3_ = _loc1_ % this.rowNum;
            _loc4_ = this._iconArray[_loc1_].icon;
            _loc4_.x = _loc2_ * this.iconRectangle.width;
            if(this.direction == LEFT)
            {
               if(_loc4_.x > 0)
               {
                  _loc4_.x = -_loc4_.x;
               }
            }
            _loc4_.y = _loc3_ * this.iconRectangle.height;
            _loc1_++;
         }
      }
      
      private function isResetBG() : Boolean
      {
         if(this._iconArray.length <= this.rowNum)
         {
            if(this.direction == LEFT)
            {
               this._iconPanelBg.x = 0;
            }
            else
            {
               this._iconPanelBg.x = this.iconRectangle.width;
            }
            this._iconPanelBg.width = this.iconRectangle.width;
            this._iconPanelBg.alpha = 0;
            this.maskSprite.x = 0;
            this.maskSprite.width = this._iconPanelBg.width;
            return true;
         }
         return false;
      }
      
      public function addIcon(param1:DisplayObject, param2:int) : DisplayObject
      {
         var _loc3_:Sprite = new Sprite();
         _loc3_.addChild(param1);
         var _loc4_:Object = {};
         _loc4_.orderId = param2;
         _loc4_.icon = _loc3_;
         this._iconSprite.addChild(_loc3_);
         this._iconArray.push(_loc4_);
         return param1;
      }
      
      public function iconSortOn(param1:String = "orderId") : void
      {
         this._iconArray.sortOn(param1,Array.NUMERIC);
      }
      
      public function arrange() : void
      {
         if(this.isResetBG())
         {
            this.updateButtonPos();
         }
         else
         {
            this.updateBgAndMask(this.isRollOver);
         }
         this.updateIconsPos();
      }
      
      private function validLength(param1:Boolean) : int
      {
         if(!param1)
         {
            return this.rowNum;
         }
         return this._iconArray.length;
      }
      
      private function killTweenMaxs() : void
      {
         if(!this.tweenMax)
         {
            return;
         }
         this.tweenMax.kill();
         this.tweenMax = null;
      }
      
      public function removeChildren() : void
      {
         var _loc2_:Sprite = null;
         while(this._iconSprite.numChildren > 0)
         {
            this._iconSprite.removeChildAt(0);
         }
         var _loc1_:int = 0;
         while(_loc1_ < this._iconArray.length)
         {
            _loc2_ = this._iconArray[_loc1_].icon;
            while(_loc2_.numChildren > 0)
            {
               _loc2_.removeChildAt(0);
            }
            this._iconArray[_loc1_] = null;
            _loc1_++;
         }
         this._iconArray = [];
      }
      
      public function dispose() : void
      {
         this.killTweenMaxs();
         removeEventListener(MouseEvent.ROLL_OVER,this.__openOverHandler);
         removeEventListener(MouseEvent.ROLL_OUT,this.__openOverHandler);
         ObjectUtils.disposeObject(this._openBtnBg);
         this._openBtnBg = null;
         ObjectUtils.disposeObject(this._closeBtnBg);
         this._closeBtnBg = null;
         ObjectUtils.disposeObject(this._iconPanelBg);
         this._iconPanelBg = null;
         ObjectUtils.disposeObject(this._openBtnSprite);
         this._openBtnSprite = null;
         this.removeChildren();
         this._iconArray = null;
         ObjectUtils.disposeObject(this._iconSprite);
         this._iconSprite = null;
         this.iconRectangle = null;
         ObjectUtils.disposeObject(this.maskSprite);
         this.maskSprite = null;
         if(this.parent)
         {
            this.parent.removeChild(this);
         }
      }
   }
}
