package hall
{
   import Dice.DiceManager;
   import GodSyah.SyahManager;
   import LimitAward.LimitAwardButton;
   import accumulativeLogin.AccumulativeManager;
   import battleGroud.BattleGroudManager;
   import beadSystem.controls.BeadLeadManager;
   import calendar.CalendarManager;
   import campbattle.CampBattleManager;
   import chickActivation.ChickActivationManager;
   import church.view.weddingRoomList.DivorcePromptFrame;
   import com.pickgliss.action.FunctionAction;
   import com.pickgliss.effect.IEffect;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.BaseLoader;
   import com.pickgliss.loader.LoadResourceManager;
   import com.pickgliss.loader.LoaderEvent;
   import com.pickgliss.loader.LoaderSavingManager;
   import com.pickgliss.loader.QueueLoader;
   import com.pickgliss.loader.RequestLoader;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.manager.CacheSysManager;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.ComponentSetting;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.ShowTipManager;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.ScrollPanel;
   import com.pickgliss.ui.controls.TextButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.image.MovieImage;
   import com.pickgliss.utils.ClassUtils;
   import com.pickgliss.utils.MD5;
   import com.pickgliss.utils.ObjectUtils;
   import consortionBattle.ConsortiaBattleManager;
   import dayActivity.DayActivityManager;
   import dayActivity.data.DayActiveData;
   import dayActivity.view.DdtImportantFrame;
   import ddt.DDT;
   import ddt.bagStore.BagStore;
   import ddt.constants.CacheConsts;
   import ddt.data.UIModuleTypes;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.data.player.SelfInfo;
   import ddt.data.quest.QuestInfo;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.events.DuowanInterfaceEvent;
   import ddt.loader.LoaderCreate;
   import ddt.loader.StartupResourceLoader;
   import ddt.manager.AcademyManager;
   import ddt.manager.BossBoxManager;
   import ddt.manager.CSMBoxManager;
   import ddt.manager.ChatManager;
   import ddt.manager.DuowanInterfaceManage;
   import ddt.manager.GameInSocketOut;
   import ddt.manager.KeyboardShortcutsManager;
   import ddt.manager.LanguageMgr;
   import ddt.manager.LeavePageManager;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PathManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SharedManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.manager.StateManager;
   import ddt.manager.TaskManager;
   import ddt.manager.TimeManager;
   import ddt.manager.VoteManager;
   import ddt.states.BaseStateView;
   import ddt.states.StateType;
   import ddt.utils.PositionUtils;
   import ddt.view.BackgoundView;
   import ddt.view.DailyButtunBar;
   import ddt.view.MainToolBar;
   import ddt.view.NovicePlatinumCard;
   import ddt.view.bossbox.SmallBoxButton;
   import ddt.view.chat.ChatData;
   import ddt.view.chat.ChatFormats;
   import ddt.view.chat.ChatInputView;
   import ddt.view.chat.ChatView;
   import ddtActivityIcon.ActivitStateEvent;
   import ddtActivityIcon.DdtActivityIconManager;
   import ddtBuried.BuriedManager;
   import dragonBoat.DragonBoatManager;
   import escort.EscortManager;
   import farm.FarmModelController;
   import fightFootballTime.manager.FightFootballTimeManager;
   import flash.display.Bitmap;
   import flash.display.MovieClip;
   import flash.display.Shape;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.events.TimerEvent;
   import flash.external.ExternalInterface;
   import flash.geom.Point;
   import flash.net.URLVariables;
   import flash.text.TextField;
   import flash.utils.Dictionary;
   import flash.utils.Timer;
   import flash.utils.getDefinitionByName;
   import flowerGiving.FlowerGivingManager;
   import game.view.stage.StageCurtain;
   import growthPackage.GrowthPackageManager;
   import guildMemberWeek.manager.GuildMemberWeekManager;
   import hallIcon.HallIconManager;
   import hallIcon.HallIconType;
   import hallIcon.view.HallLeftIconView;
   import hallIcon.view.HallRightIconView;
   import im.IMController;
   import kingBless.KingBlessManager;
   import kingDivision.KingDivisionManager;
   import labyrinth.LabyrinthManager;
   import league.manager.LeagueManager;
   import littleGame.LittleGameManager;
   import littleGame.events.LittleGameEvent;
   import oldPlayerRegress.RegressManager;
   import oldPlayerRegress.data.RegressData;
   import oldPlayerRegress.event.RegressEvent;
   import oldplayergetticket.GetTicketEvent;
   import oldplayergetticket.GetTicketManager;
   import petsBag.controller.PetBagController;
   import petsBag.data.PetFarmGuildeTaskType;
   import petsBag.event.UpdatePetFarmGuildeEvent;
   import quest.TaskMainFrame;
   import ringStation.event.RingStationEvent;
   import road7th.comm.PackageIn;
   import road7th.utils.MovieClipWrapper;
   import room.RoomManager;
   import room.transnational.TransnationalFightManager;
   import roomList.LookupEnumerate;
   import roulette.LeftGunRouletteManager;
   import roulette.RouletteFrameEvent;
   import serverlist.view.ServerDropList;
   import sevenDouble.SevenDoubleManager;
   import shop.manager.ShopSaleManager;
   import shop.view.ShopRechargeEquipAlert;
   import shop.view.ShopRechargeEquipServer;
   import socialContact.friendBirthday.FriendBirthdayManager;
   import store.StrengthDataManager;
   import superWinner.manager.SuperWinnerManager;
   import times.TimesManager;
   import trainer.controller.NewHandGuideManager;
   import trainer.controller.SystemOpenPromptManager;
   import trainer.controller.WeakGuildManager;
   import trainer.data.ArrowType;
   import trainer.data.Step;
   import trainer.view.NewHandContainer;
   import trainer.view.WelcomeView;
   import vip.VipController;
   import wantstrong.WantStrongManager;
   import wonderfulActivity.ActivityType;
   import wonderfulActivity.WonderfulActivityManager;
   import wonderfulActivity.event.WonderfulActivityEvent;
   import worldBossHelper.WorldBossHelperManager;
   import worldboss.WorldBossManager;
   
   public class HallStateView extends BaseStateView
   {
      
      public static const VIP_LEFT_DAY_TO_COMFIRM:int = 3;
      
      public static const VIP_LEFT_DAY_FIRST_PROMPT:int = 7;
      
      public static var SoundIILoaded:Boolean = false;
      
      public static var SoundILoaded:Boolean = false;
      
      private static var _firstLoadTimes:Boolean = true;
      
      private static var _isFirstCheck:uint = 1;
       
      
      private var _black:Shape;
      
      private var _hallMainView:MovieClip;
      
      private var _OpenAuction:MovieClip;
      
      private var _chatView:ChatView;
      
      private var _boxButton:SmallBoxButton;
      
      private var _btnList:Vector.<BaseButton>;
      
      private var _auctionBtn:BaseButton;
      
      private var _roomBtn:BaseButton;
      
      private var _churchBtn:BaseButton;
      
      private var _framBtn:BaseButton;
      
      private var _constrionBtn:BaseButton;
      
      private var _dungeonBtn:BaseButton;
      
      private var _storeBtn:BaseButton;
      
      private var _shopBtn:BaseButton;
      
      private var _tofflistBtn:BaseButton;
      
      private var _labyrinthBtn:BaseButton;
      
      private var _eventActives:Array;
      
      private var _isIMController:Boolean;
      
      private var isShow:Boolean = false;
      
      private var _renewal:BaseAlerFrame;
      
      private var _isAddFrameComplete:Boolean = false;
      
      private var _channalMc:MovieClip;
      
      private var list:ServerDropList;
      
      private var mc:MovieImage;
      
      private var txtArray:Array;
      
      private var _shine:Vector.<MovieClipWrapper>;
      
      private var _limitAwardButton:LimitAwardButton;
      
      private var _signBtnContainer:Sprite;
      
      private var _signShineEffect:IEffect;
      
      private var isInLuckStone:Boolean;
      
      private var _newChickenBoxBtn:MovieClip;
      
      private var _timer:Timer;
      
      private var _isShowWonderfulActTip1:Boolean;
      
      private var _isShowWonderfulActTip2:Boolean;
      
      private var _hallLeftIconView:HallLeftIconView;
      
      private var _hallRightIconView:HallRightIconView;
      
      private var _getTicketBtn:BaseButton;
      
      private var startDate:Date;
      
      private var endDate:Date;
      
      private var isActiv:Boolean;
      
      private var _count:uint;
      
      private var _timesCount:uint;
      
      private var _checkisDesk:Timer;
      
      private var _isDesk:Boolean = true;
      
      private var farmEnterButton:Sprite;
      
      private var _trainerWelcomeView:WelcomeView;
      
      private var _battleFrame:Frame;
      
      private var _battlePanel:ScrollPanel;
      
      private var _battleImg:Bitmap;
      
      private var _battleBtn:TextButton;
      
      private var _isFirst:Boolean;
      
      private var _lastCreatTime:int;
      
      public function HallStateView()
      {
         this.txtArray = ["txt_church_mc","txt_shop_mc","txt_dungeon_mc","txt_roomList_mc","txt_auction_mc","txt_store_mc","txt_tofflist_mc","txt_consortia_mc","txt_farm_mc","txt_Labyrinth_mc","txt_Regress_mc"];
         super();
         try
         {
            WeakGuildManager.Instance.timeStatistics(0,getDefinitionByName("DDT_Loading").time);
            if(PathManager.isStatistics)
            {
               WeakGuildManager.Instance.statistics(3,getDefinitionByName("DDT_Loading").time);
            }
         }
         catch(e:Error)
         {
         }
         this.farmEnterButtonTest();
         this.initEvent();
      }
      
      private function initEvent() : void
      {
         LabyrinthManager.Instance.addEventListener(LabyrinthManager.LABYRINTH_CHAT,this.__labyrinthChat);
      }
      
      protected function __labyrinthChat(param1:Event) : void
      {
         LayerManager.Instance.addToLayer(ChatManager.Instance.view,LayerManager.GAME_DYNAMIC_LAYER);
      }
      
      override public function getType() : String
      {
         return StateType.MAIN;
      }
      
      override public function getBackType() : String
      {
         return StateType.MAIN;
      }
      
      override public function enter(param1:BaseStateView, param2:Object = null) : void
      {
         var loadSecondGameSource:LoadSecondGameSource = null;
         var audioLoader:BaseLoader = null;
         var prev:BaseStateView = param1;
         var data:Object = param2;
         KeyboardShortcutsManager.Instance.cancelForbidden();
         super.enter(prev,data);
         if(data is Function)
         {
            data();
         }
         KeyboardShortcutsManager.Instance.setup();
         SocketManager.Instance.out.sendSceneLogin(LookupEnumerate.MAIN);
         BackgoundView.Instance.show();
         SystemOpenPromptManager.instance.showFrame();
         SocketManager.Instance.out.battleGroundUpdata(1);
         SocketManager.Instance.out.battleGroundUpdata(2);
         SocketManager.Instance.out.battleGroundPlayerUpdata();
         SocketManager.Instance.out.sendWonderfulActivity(0,-1);
         WonderfulActivityManager.Instance.addEventListener(WonderfulActivityEvent.CHECK_ACTIVITY_STATE,this.__checkShowWonderfulActivityTip);
         if(StartupResourceLoader.firstEnterHall)
         {
            this._black = new Shape();
            this._black.graphics.beginFill(0,1);
            this._black.graphics.drawRect(0,0,1000,600);
            this._black.graphics.endFill();
            addChild(this._black);
            MainToolBar.Instance.show();
            MainToolBar.Instance.btnOpen();
            if(!this._isAddFrameComplete)
            {
               TaskManager.instance.checkHighLight();
            }
            MainToolBar.Instance.enabled = true;
            MainToolBar.Instance.updateReturnBtn(MainToolBar.ENTER_HALL);
            MainToolBar.Instance.ExitBtnVisible = false;
            if(!SoundIILoaded)
            {
               audioLoader = LoaderCreate.Instance.createAudioIILoader();
               audioLoader.addEventListener(LoaderEvent.COMPLETE,this.__onAudioIILoadComplete);
               LoadResourceManager.Instance.startLoad(audioLoader);
            }
            this.loadUserGuide();
            StartupResourceLoader.Instance.addFirstGameNotStartupNeededResource();
            loadSecondGameSource = new LoadSecondGameSource();
            loadSecondGameSource.startLoad();
         }
         else
         {
            try
            {
               this.initHall();
               return;
            }
            catch(e:Error)
            {
               UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,__onHallLoadComplete);
               UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.DDT_COREI);
               return;
            }
         }
      }
      
      private function __onHallLoadComplete(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.DDT_COREI)
         {
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.__onHallLoadComplete);
            this.initHall();
         }
      }
      
      private function initHall() : void
      {
         var _loc1_:Object = null;
         var _loc2_:Object = null;
         var _loc3_:Object = null;
         var _loc4_:Object = null;
         var _loc5_:Object = null;
         var _loc6_:Object = null;
         var _loc7_:Object = null;
         var _loc8_:Object = null;
         var _loc9_:Object = null;
         var _loc10_:Object = null;
         var _loc11_:QueueLoader = null;
         var _loc12_:NovicePlatinumCard = null;
         SoundManager.instance.playMusic("062",true,false);
         if(DDT.AMIGOTYPE == true && TaskManager.instance.getQuestByID(751) != null)
         {
            SocketManager.Instance.out.sendQuestCheck(751,1,0);
         }
         if(!this._hallMainView)
         {
            this._hallMainView = ComponentFactory.Instance.creat("asset.hall.hallMainViewAsset1");
         }
         this._hallMainView.gotoAndStop(10);
         this._hallMainView["auctionButton"].gotoAndStop(1);
         this._hallMainView["churchButton"].gotoAndStop(1);
         this._hallMainView["consortiaButton"].gotoAndStop(1);
         this._hallMainView["dungeonButton"].gotoAndStop(1);
         this._hallMainView["tofflistButton"].gotoAndStop(1);
         this._hallMainView["roomListButton"].gotoAndStop(1);
         this._hallMainView["shopButton"].gotoAndStop(1);
         this._hallMainView["storeButton"].gotoAndStop(1);
         this._hallMainView["farmButton"].gotoAndStop(1);
         this._hallMainView["labyrinthButton"].gotoAndStop(1);
         addChild(this._hallMainView);
         SocketManager.Instance.out.requestWonderfulActInit(2);
         if(!this._auctionBtn)
         {
            this._auctionBtn = ComponentFactory.Instance.creatComponentByStylename("HallMain.auctionBtn");
            _loc1_ = new Object();
            _loc1_["title"] = LanguageMgr.GetTranslation("ddt.hall.build.auctiontitle");
            _loc1_["content"] = LanguageMgr.GetTranslation("ddt.HallStateView.auction");
            this._auctionBtn.tipData = _loc1_;
         }
         addChild(this._auctionBtn);
         if(!this._roomBtn)
         {
            this._roomBtn = ComponentFactory.Instance.creatComponentByStylename("HallMain.roomBtn");
            _loc2_ = new Object();
            _loc2_["title"] = LanguageMgr.GetTranslation("ddt.hall.build.roomtitle");
            _loc2_["content"] = LanguageMgr.GetTranslation("ddt.HallStateView.game");
            this._roomBtn.tipData = _loc2_;
         }
         addChild(this._roomBtn);
         if(!this._churchBtn)
         {
            this._churchBtn = ComponentFactory.Instance.creatComponentByStylename("HallMain.churchBtn");
            _loc3_ = new Object();
            _loc3_["title"] = LanguageMgr.GetTranslation("ddt.hall.build.churchtitle");
            _loc3_["content"] = LanguageMgr.GetTranslation("ddt.HallStateView.church");
            this._churchBtn.tipData = _loc3_;
         }
         addChild(this._churchBtn);
         if(!this._framBtn)
         {
            this._framBtn = ComponentFactory.Instance.creatComponentByStylename("HallMain.framBtn");
            _loc4_ = new Object();
            _loc4_["title"] = LanguageMgr.GetTranslation("ddt.hall.build.framtitle");
            _loc4_["content"] = LanguageMgr.GetTranslation("ddt.HallStateView.farm");
            this._framBtn.tipData = _loc4_;
         }
         addChild(this._framBtn);
         if(!this._constrionBtn)
         {
            this._constrionBtn = ComponentFactory.Instance.creatComponentByStylename("HallMain.constrionBtn");
            _loc5_ = new Object();
            _loc5_["title"] = LanguageMgr.GetTranslation("ddt.hall.build.constriontitle");
            _loc5_["content"] = LanguageMgr.GetTranslation("ddt.HallStateView.consortion");
            this._constrionBtn.tipData = _loc5_;
         }
         addChild(this._constrionBtn);
         if(!this._dungeonBtn)
         {
            this._dungeonBtn = ComponentFactory.Instance.creatComponentByStylename("HallMain.dungeonBtn");
            _loc6_ = new Object();
            _loc6_["title"] = LanguageMgr.GetTranslation("ddt.hall.build.dungeontitle");
            _loc6_["content"] = LanguageMgr.GetTranslation("ddt.HallStateView.Dungeon");
            this._dungeonBtn.tipData = _loc6_;
         }
         addChild(this._dungeonBtn);
         if(!this._storeBtn)
         {
            this._storeBtn = ComponentFactory.Instance.creatComponentByStylename("HallMain.storeBtn");
            _loc7_ = new Object();
            _loc7_["title"] = LanguageMgr.GetTranslation("ddt.hall.build.storetitle");
            _loc7_["content"] = LanguageMgr.GetTranslation("ddt.HallStateView.store");
            this._storeBtn.tipData = _loc7_;
         }
         addChild(this._storeBtn);
         this._storeBtn.height = 140;
         if(!this._shopBtn)
         {
            this._shopBtn = ComponentFactory.Instance.creatComponentByStylename("HallMain.shopBtn");
            _loc8_ = new Object();
            _loc8_["title"] = LanguageMgr.GetTranslation("ddt.hall.build.shoptitle");
            _loc8_["content"] = LanguageMgr.GetTranslation("ddt.HallStateView.Shop");
            this._shopBtn.tipData = _loc8_;
         }
         addChild(this._shopBtn);
         if(!this._tofflistBtn)
         {
            this._tofflistBtn = ComponentFactory.Instance.creatComponentByStylename("HallMain.tofflistbtn");
            _loc9_ = new Object();
            _loc9_["title"] = LanguageMgr.GetTranslation("ddt.hall.build.tofflisttitle");
            _loc9_["content"] = LanguageMgr.GetTranslation("ddt.HallStateView.toffist");
            this._tofflistBtn.tipData = _loc9_;
         }
         addChild(this._tofflistBtn);
         if(!this._labyrinthBtn)
         {
            this._labyrinthBtn = ComponentFactory.Instance.creatComponentByStylename("HallMain.Labyrinthbtn");
            _loc10_ = new Object();
            _loc10_["title"] = LanguageMgr.GetTranslation("ddt.hall.build.Labyrinthtitle");
            _loc10_["content"] = LanguageMgr.GetTranslation("ddt.HallStateView.Labyrinth");
            this._labyrinthBtn.tipData = _loc10_;
         }
         addChild(this._labyrinthBtn);
         this._btnList = new Vector.<BaseButton>();
         this._btnList.push(this._churchBtn);
         this._btnList.push(this._shopBtn);
         this._btnList.push(this._dungeonBtn);
         this._btnList.push(this._roomBtn);
         this._btnList.push(this._auctionBtn);
         this._btnList.push(this._storeBtn);
         this._btnList.push(this._tofflistBtn);
         this._btnList.push(this._constrionBtn);
         this._btnList.push(this._framBtn);
         this._btnList.push(this._labyrinthBtn);
         this._channalMc = ComponentFactory.Instance.creat("asset.hall.channalMc");
         PositionUtils.setPos(this._channalMc,"hall.channalMcPos");
         addChild(this._channalMc);
         MainToolBar.Instance.show();
         MainToolBar.Instance.btnOpen();
         if(!this._isAddFrameComplete)
         {
            TaskManager.instance.checkHighLight();
         }
         MainToolBar.Instance.enabled = true;
         MainToolBar.Instance.updateReturnBtn(MainToolBar.ENTER_HALL);
         MainToolBar.Instance.ExitBtnVisible = true;
         if(this.list == null)
         {
            this.list = ComponentFactory.Instance.creat("serverlist.hall.ServerDropList");
         }
         else
         {
            this.list.refresh();
         }
         addChild(this.list);
         DailyButtunBar.Insance.show();
         this.initBuild();
         this._hallMainView.svrname_txt.text = "";
         this.setOptionalModule(this._hallMainView["farm_mc"],8,PlayerManager.Instance.Self.Grade >= 25?Boolean(true):Boolean(false));
         StartupResourceLoader.Instance.finishLoadingProgress();
         if(NewHandGuideManager.Instance.progress < Step.POP_EXPLAIN_ONE && WeakGuildManager.Instance.switchUserGuide)
         {
            StartupResourceLoader.Instance.addNotStartupNeededResource();
         }
         else
         {
            StartupResourceLoader.Instance.addNotStartupNeededResource();
            if(!SoundILoaded)
            {
               _loc11_ = new QueueLoader();
               _loc11_.addLoader(LoaderCreate.Instance.createAudioILoader());
               _loc11_.addLoader(LoaderCreate.Instance.createAudioIILoader());
               _loc11_.addEventListener(Event.COMPLETE,this.__onAudioLoadComplete);
               _loc11_.start();
            }
         }
         this.loadUserGuide();
         CacheSysManager.unlock(CacheConsts.ALERT_IN_MARRY);
         CacheSysManager.unlock(CacheConsts.ALERT_IN_FIGHT);
         CacheSysManager.getInstance().singleRelease(CacheConsts.ALERT_IN_FIGHT);
         this.checkShowVipAlert_New();
         this.checkShowBossBox();
         KingBlessManager.instance.checkShowDueAlert();
         this.checkShowStoreFromShop();
         StrengthDataManager.instance.setup();
         GuildMemberWeekManager.instance.setup();
         if(!this._isIMController && PathManager.CommunityExist())
         {
            this._isIMController = true;
            IMController.Instance.createConsortiaLoader();
         }
         if(SharedManager.Instance.divorceBoolean)
         {
            DivorcePromptFrame.Instance.show();
         }
         if(PlayerManager.Instance.Self.baiduEnterCode == "true")
         {
            _loc12_ = ComponentFactory.Instance.creatComponentByStylename("core.NovicePlatinumCard");
            _loc12_.setup();
         }
         TaskManager.instance.addEventListener(TaskMainFrame.TASK_FRAME_HIDE,this.__taskFrameHide);
         this.loadWeakGuild();
         CacheSysManager.getInstance().release(CacheConsts.ALERT_IN_HALL);
         if(!this._isAddFrameComplete)
         {
            this.addFrame();
         }
         CacheSysManager.getInstance().singleRelease(CacheConsts.ALERT_IN_HALL);
         FriendBirthdayManager.Instance.findFriendBirthday();
         this._eventActives = CalendarManager.getInstance().eventActives;
         PetBagController.instance().addEventListener(UpdatePetFarmGuildeEvent.FINISH,this.__updatePetFarmGuilde);
         this.petFarmGuilde();
         CSMBoxManager.instance.showBox(this);
         this.updateWantStrong();
         HallIconManager.instance.checkDefaultIconShow();
         this._hallLeftIconView = new HallLeftIconView();
         PositionUtils.setPos(this._hallLeftIconView,"hallIcon.hallLeftIconViewPos");
         addChild(this._hallLeftIconView);
         this._hallRightIconView = new HallRightIconView();
         PositionUtils.setPos(this._hallRightIconView,"hallIcon.hallRightIconViewPos");
         LayerManager.Instance.addToLayer(this._hallRightIconView,LayerManager.GAME_UI_LAYER);
         HallIconManager.instance.checkIconCall();
         HallIconManager.instance.checkCacheRightIconShow();
         HallIconManager.instance.checkCacheRightIconTask();
         this.defaultRightWonderfulPlayIconShow();
         this.defaultRightActivityIconShow();
         if(KingDivisionManager.Instance.openFrame)
         {
            KingDivisionManager.Instance.onClickIcon();
         }
         this._timer.reset();
         this._timer.start();
         if(SyahManager.Instance.isOpen)
         {
            if(SyahManager.Instance.login == false)
            {
               SyahManager.Instance.login = true;
               ChatManager.Instance.sysChatYellow(LanguageMgr.GetTranslation("ddt.GodSyah.isCome"));
            }
         }
         DragonBoatManager.instance.createEntryBtn(this,this._hallMainView,this._btnList[7],this._channalMc);
         DragonBoatManager.instance.enterMainOpenFrame();
         this.initDdtActiviyIconState();
         if(DdtActivityIconManager.Instance.isOneOpen && DdtActivityIconManager.Instance.currObj)
         {
            this.changeIconState(DdtActivityIconManager.Instance.currObj,true);
         }
         DdtActivityIconManager.Instance.setup();
         BeadLeadManager.Instance.beadLeadOne();
         this.checkShowVote();
         ChatManager.Instance.state = ChatManager.CHAT_HALL_STATE;
         ChatManager.Instance.view.visible = true;
         ChatManager.Instance.chatDisabled = false;
         ChatManager.Instance.lock = false;
         addChild(ChatManager.Instance.view);
      }
      
      private function defaultRightWonderfulPlayIconShow() : void
      {
         LeagueManager.instance.addLeaIcon = this.addLeagueIcon;
         LeagueManager.instance.deleteLeaIcon = this.deleLeagueBtn;
         if(LeagueManager.instance.isOpen)
         {
            this.addLeagueIcon();
         }
         if(ConsortiaBattleManager.instance.isOpen)
         {
            ConsortiaBattleManager.instance.addEntryBtn(true);
         }
         if(CampBattleManager.instance.model.isOpen)
         {
            CampBattleManager.instance.addCampBtn();
         }
         if(PlayerManager.Instance.Self.Grade >= 22)
         {
            SocketManager.Instance.addEventListener(RingStationEvent.RINGSTATION_FIGHTFLAG,this.__ringstationOnFightFlag);
            SocketManager.Instance.out.sendRingStationFightFlag();
            HallIconManager.instance.updateSwitchHandler(HallIconType.RINGSTATION,true);
         }
         BattleGroudManager.Instance.initBattleIcon = this.addBattleIcon;
         BattleGroudManager.Instance.dispBattleIcon = this.delBattleIcon;
         if(BattleGroudManager.Instance.isShow)
         {
            this.addBattleIcon();
         }
         LittleGameManager.Instance.addEventListener(LittleGameEvent.ActivedChanged,this.__littlegameActived);
         this.__littlegameActived();
         if(TransnationalFightManager.Instance.hasActived())
         {
            this.ShowTransnationalIcon(true);
         }
         TransnationalFightManager.Instance.ShowIcon = this.ShowTransnationalIcon;
         if(TransnationalFightManager.Instance.isfromTransnational)
         {
            TransnationalFightManager.Instance.isfromTransnational = false;
         }
         if(SevenDoubleManager.instance.isStart && SevenDoubleManager.instance.isLoadIconComplete)
         {
            this.sevenDoubleIconResLoadComplete(null);
            SevenDoubleManager.instance.addEventListener(SevenDoubleManager.END,this.sevendoubleEndHandler);
         }
         else
         {
            SevenDoubleManager.instance.addEventListener(SevenDoubleManager.ICON_RES_LOAD_COMPLETE,this.sevenDoubleIconResLoadComplete);
         }
         if(EscortManager.instance.isStart && EscortManager.instance.isLoadIconComplete)
         {
            this.escortIconResLoadComplete(null);
            EscortManager.instance.addEventListener(EscortManager.END,this.escortEndHandler);
         }
         else
         {
            EscortManager.instance.addEventListener(EscortManager.ICON_RES_LOAD_COMPLETE,this.escortIconResLoadComplete);
         }
         this.checkShowWorldBossEntrance();
         this.checkShowWorldBossHelper();
         if(FightFootballTimeManager.instance.isopen)
         {
            this.showFightFootballTimeIcon(true);
         }
         BuriedManager.Instance.checkShowIcon();
         FightFootballTimeManager.instance.ShowIcon = this.showFightFootballTimeIcon;
      }
      
      private function __ringstationOnFightFlag(param1:RingStationEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readInt();
         var _loc4_:Date = _loc2_.readDate();
         if(StateManager.currentStateType == StateType.MAIN)
         {
            HallIconManager.instance.updateSwitchHandler(HallIconType.RINGSTATION,true);
         }
      }
      
      public function ShowTransnationalIcon(param1:Boolean) : void
      {
         if(param1)
         {
            HallIconManager.instance.updateSwitchHandler(HallIconType.TRANSNATIONAL,true);
         }
         else
         {
            HallIconManager.instance.updateSwitchHandler(HallIconType.TRANSNATIONAL,false);
         }
      }
      
      private function showFightFootballTimeIcon(param1:Boolean) : void
      {
         if(param1)
         {
            HallIconManager.instance.updateSwitchHandler(HallIconType.FIGHTFOOTBALLTIME,true);
         }
         else
         {
            HallIconManager.instance.updateSwitchHandler(HallIconType.FIGHTFOOTBALLTIME,false);
         }
      }
      
      private function addLeagueIcon(param1:Boolean = false, param2:String = null) : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.LEAGUE,true,param2);
      }
      
      private function deleLeagueBtn() : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.LEAGUE,false);
      }
      
      private function addBattleIcon(param1:Boolean = false, param2:String = "") : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.BATTLE,true,param2);
      }
      
      private function delBattleIcon() : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.BATTLE,false);
      }
      
      private function sevendoubleEndHandler(param1:Event) : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.SEVENDOUBLE,false);
      }
      
      private function sevenDoubleIconResLoadComplete(param1:Event) : void
      {
         SevenDoubleManager.instance.removeEventListener(SevenDoubleManager.ICON_RES_LOAD_COMPLETE,this.sevenDoubleIconResLoadComplete);
         if(SevenDoubleManager.instance.isStart && SevenDoubleManager.instance.isLoadIconComplete)
         {
            HallIconManager.instance.updateSwitchHandler(HallIconType.SEVENDOUBLE,true);
            SevenDoubleManager.instance.addEventListener(SevenDoubleManager.END,this.sevendoubleEndHandler);
         }
         else
         {
            SevenDoubleManager.instance.addEventListener(SevenDoubleManager.ICON_RES_LOAD_COMPLETE,this.sevenDoubleIconResLoadComplete);
         }
      }
      
      private function escortEndHandler(param1:Event) : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.ESCORT,false);
      }
      
      private function escortIconResLoadComplete(param1:Event) : void
      {
         EscortManager.instance.removeEventListener(EscortManager.ICON_RES_LOAD_COMPLETE,this.escortIconResLoadComplete);
         if(EscortManager.instance.isStart && EscortManager.instance.isLoadIconComplete)
         {
            HallIconManager.instance.updateSwitchHandler(HallIconType.ESCORT,true);
            EscortManager.instance.addEventListener(EscortManager.END,this.escortEndHandler);
         }
         else
         {
            EscortManager.instance.addEventListener(EscortManager.ICON_RES_LOAD_COMPLETE,this.escortIconResLoadComplete);
         }
      }
      
      private function __littlegameActived(param1:Event = null, param2:Boolean = false, param3:String = null) : void
      {
         if(LittleGameManager.Instance.hasActive())
         {
            HallIconManager.instance.updateSwitchHandler(HallIconType.LITTLEGAMENOTE,true,param3);
         }
         else
         {
            HallIconManager.instance.updateSwitchHandler(HallIconType.LITTLEGAMENOTE,false,param3);
         }
      }
      
      private function defaultRightWonderfulPlayIconRemove() : void
      {
         SocketManager.Instance.removeEventListener(RingStationEvent.RINGSTATION_FIGHTFLAG,this.__ringstationOnFightFlag);
         LittleGameManager.Instance.removeEventListener(LittleGameEvent.ActivedChanged,this.__littlegameActived);
         SevenDoubleManager.instance.removeEventListener(SevenDoubleManager.END,this.sevendoubleEndHandler);
         SevenDoubleManager.instance.removeEventListener(SevenDoubleManager.ICON_RES_LOAD_COMPLETE,this.sevenDoubleIconResLoadComplete);
         EscortManager.instance.removeEventListener(EscortManager.END,this.escortEndHandler);
         EscortManager.instance.removeEventListener(EscortManager.ICON_RES_LOAD_COMPLETE,this.escortIconResLoadComplete);
         BattleGroudManager.Instance.initBattleIcon = null;
         BattleGroudManager.Instance.dispBattleIcon = null;
      }
      
      private function defaultRightActivityIconShow() : void
      {
         GrowthPackageManager.instance.showEnterIcon();
         DiceManager.Instance.setup(this.showDiceBtn);
         if(DiceManager.Instance.isopen)
         {
            this.showDiceBtn(true);
         }
         this.addAccumulativeLoginAct();
         LeftGunRouletteManager.instance.addEventListener(RouletteFrameEvent.LEFTGUN_ENABLE,this.__leftGunShow);
         if(LeftGunRouletteManager.instance.IsOpen)
         {
            LeftGunRouletteManager.instance.showGunButton();
         }
         WonderfulActivityManager.Instance.setLimitActivities(CalendarManager.getInstance().eventActives);
         WonderfulActivityManager.Instance.addWAIcon = this.initLimitActivityIcon;
         WonderfulActivityManager.Instance.deleWAIcon = this.deleteLimitActivityIcon;
         if(PlayerManager.Instance.Self.Grade >= 3)
         {
            if(WonderfulActivityManager.Instance.actList.length > 0)
            {
               this.initLimitActivityIcon();
            }
         }
         SocketManager.Instance.out.requestForLuckStone();
         if(PlayerManager.Instance.Self.isOld && !RegressData.isOver)
         {
            this.addRegressBtn();
         }
         SocketManager.Instance.out.sendRegressTicketInfo();
         RegressManager.instance.addEventListener(RegressEvent.REGRESS_ADD_REGRESSBTN,this.__onAddRegressBtn);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.REGRESS_GET_TICKET,this.__addGetTicketBtn);
         this._timer = new Timer(10000);
         this._timer.addEventListener(TimerEvent.TIMER,this._checkActivityPer);
         FlowerGivingManager.instance.checkOpen();
         if(KingDivisionManager.Instance.isOpen)
         {
            KingDivisionManager.Instance.updateConsotionMessage();
            KingDivisionManager.Instance.kingDivisionIcon(KingDivisionManager.Instance.isOpen);
         }
         ShopSaleManager.Instance.checkOpenShopSale();
         ChickActivationManager.instance.checkShowIcon();
      }
      
      private function showDiceBtn(param1:Boolean = false) : void
      {
         if(param1)
         {
            HallIconManager.instance.updateSwitchHandler(HallIconType.DICE,true);
         }
         else
         {
            HallIconManager.instance.updateSwitchHandler(HallIconType.DICE,false);
         }
      }
      
      private function addAccumulativeLoginAct() : void
      {
         if(PlayerManager.Instance.Self.accumulativeLoginDays >= 7 && PlayerManager.Instance.Self.accumulativeAwardDays >= 7)
         {
            AccumulativeManager.instance.removeAct();
         }
         else
         {
            AccumulativeManager.instance.addAct();
         }
      }
      
      private function __getLuckStoneEnable(param1:CrazyTankSocketEvent) : void
      {
         var _loc4_:Boolean = false;
         var _loc2_:PackageIn = param1.pkg;
         this.startDate = _loc2_.readDate();
         this.endDate = _loc2_.readDate();
         this.isActiv = _loc2_.readBoolean();
         var _loc3_:Date = TimeManager.Instance.Now();
         if(_loc3_.getTime() >= this.startDate.getTime() && _loc3_.getTime() <= this.endDate.getTime())
         {
            _loc4_ = true;
         }
         WonderfulActivityManager.Instance.rouleEndTime = this.endDate;
         if(this.isActiv && _loc4_)
         {
            HallIconManager.instance.updateSwitchHandler(HallIconType.LUCKSTONE,true);
         }
         else
         {
            HallIconManager.instance.updateSwitchHandler(HallIconType.LUCKSTONE,false);
         }
      }
      
      private function __onAddRegressBtn(param1:RegressEvent) : void
      {
         SocketManager.Instance.removeEventListener(RegressEvent.REGRESS_ADD_REGRESSBTN,this.__onAddRegressBtn);
         if(!PlayerManager.Instance.Self.isOld || RegressData.isOver)
         {
            this.deleteRegressBtn();
         }
         else
         {
            this.addRegressBtn();
         }
         if(PlayerManager.Instance.Self.isOld && !RegressData.isOver && !PlayerManager.Instance.Self.isSameDay && RegressData.isFirstLogin)
         {
            RegressManager.instance.autoPopUp = true;
            RegressManager.instance.show();
         }
      }
      
      private function deleteRegressBtn() : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.OLDPLAYERREGRESS,false);
      }
      
      private function __addGetTicketBtn(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:GetTicketEvent = new GetTicketEvent(GetTicketEvent.GETTICKET_DATA);
         _loc3_.money = _loc2_.readInt();
         _loc3_.level = _loc2_.readInt();
         _loc3_.levelMoney = _loc2_.readInt();
         GetTicketManager.instance.dispatchEvent(_loc3_);
         this.addOldPlayerGetTicketBtn();
      }
      
      private function addOldPlayerGetTicketBtn() : void
      {
         this._getTicketBtn = ComponentFactory.Instance.creatComponentByStylename("hall.getTicketButton");
         this._getTicketBtn.addEventListener(MouseEvent.CLICK,this.__onGetTicketClick);
         addChild(this._getTicketBtn);
      }
      
      protected function __onGetTicketClick(param1:MouseEvent) : void
      {
         SoundManager.instance.playButtonSound();
         GetTicketManager.instance.show();
      }
      
      private function deleteGetTicketBtn() : void
      {
         if(this._getTicketBtn)
         {
            this._getTicketBtn.dispose();
            this._getTicketBtn = null;
         }
      }
      
      private function addRegressBtn() : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.OLDPLAYERREGRESS,true);
      }
      
      private function _checkActivityPer(param1:TimerEvent) : void
      {
         this._checkLimit();
         if(!this.isInLuckStone && this.checkLuckStone())
         {
            this.addButtonList();
         }
         else if(this.isInLuckStone && !this.checkLuckStone())
         {
            this.addButtonList();
         }
         ShopSaleManager.Instance.checkOpenShopSale();
      }
      
      private function __leftGunShow(param1:RouletteFrameEvent) : void
      {
         if(LeftGunRouletteManager.instance.IsOpen)
         {
            LeftGunRouletteManager.instance.showGunButton();
         }
         else
         {
            LeftGunRouletteManager.instance.hideGunButton();
         }
      }
      
      private function _checkLimit() : void
      {
         if(!this._limitAwardButton)
         {
            return;
         }
         if(!CalendarManager.getInstance().checkEventInfo())
         {
            this.addButtonList();
            return;
         }
      }
      
      private function initLimitActivityIcon() : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.LIMITACTIVITY,true);
      }
      
      private function deleteLimitActivityIcon() : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.LIMITACTIVITY,false);
      }
      
      private function defaultRightActivityIconRemove() : void
      {
         LeftGunRouletteManager.instance.removeEventListener(RouletteFrameEvent.LEFTGUN_ENABLE,this.__leftGunShow);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.REGRESS_GET_TICKET,this.__addGetTicketBtn);
         RegressManager.instance.removeEventListener(RegressEvent.REGRESS_ADD_REGRESSBTN,this.__onAddRegressBtn);
         this.deleteGetTicketBtn();
         WonderfulActivityManager.Instance.addWAIcon = null;
         WonderfulActivityManager.Instance.deleWAIcon = null;
      }
      
      private function updateWantStrong() : void
      {
         var _loc1_:Vector.<DayActiveData> = null;
         var _loc2_:Dictionary = null;
         var _loc3_:Dictionary = null;
         var _loc4_:Date = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:Boolean = false;
         var _loc8_:int = 0;
         var _loc9_:String = null;
         if(PlayerManager.Instance.Self.Grade >= 8 && PlayerManager.Instance.Self.Grade <= 80)
         {
            _loc1_ = DayActivityManager.Instance.acitiveDataList;
            _loc2_ = DayActivityManager.Instance.bossDataDic;
            _loc3_ = WantStrongManager.Instance.findBackDic;
            _loc4_ = TimeManager.Instance.serverDate;
            _loc7_ = false;
            _loc8_ = 0;
            while(_loc8_ < _loc1_.length)
            {
               _loc5_ = int(_loc1_[_loc8_].ActiveTime.substr(_loc1_[_loc8_].ActiveTime.length - 5,2));
               _loc6_ = int(_loc1_[_loc8_].ActiveTime.substr(_loc1_[_loc8_].ActiveTime.length - 2,2)) + 10;
               switch(_loc1_[_loc8_].ID)
               {
                  case 1:
                     if(this.compareDay(_loc4_.day,_loc1_[_loc8_].DayOfWeek) && PlayerManager.Instance.Self.Grade >= int(_loc1_[_loc8_].LevelLimit) && this.compareDate(_loc4_,_loc5_,_loc6_) && !this.isPassBoss(_loc2_,1) && !this.isFindBack(_loc3_,1))
                     {
                        WantStrongManager.Instance.findBackExist = true;
                        _loc7_ = true;
                        WantStrongManager.Instance.setFindBackData(1);
                     }
                     break;
                  case 2:
                     if(this.compareDay(_loc4_.day,_loc1_[_loc8_].DayOfWeek) && PlayerManager.Instance.Self.Grade >= int(_loc1_[_loc8_].LevelLimit) && this.compareDate(_loc4_,_loc5_,_loc6_) && !this.isPassBoss(_loc2_,2) && !this.isFindBack(_loc3_,2))
                     {
                        WantStrongManager.Instance.findBackExist = true;
                        _loc7_ = true;
                        WantStrongManager.Instance.setFindBackData(0);
                     }
                     break;
                  case 19:
                     _loc9_ = _loc1_[_loc8_].ActiveTime.split(" ")[0];
                     _loc5_ = int(_loc9_.substr(_loc9_.length - 5,2));
                     _loc6_ = int(_loc9_.substr(_loc9_.length - 2,2)) + 10;
                     if(this.compareDay(_loc4_.day,_loc1_[_loc8_].DayOfWeek) && PlayerManager.Instance.Self.Grade >= int(_loc1_[_loc8_].LevelLimit) && this.compareDate(_loc4_,_loc5_,_loc6_) && !this.isPassBoss(_loc2_,19) && !this.isFindBack(_loc3_,19))
                     {
                        WantStrongManager.Instance.findBackExist = true;
                        _loc7_ = true;
                        WantStrongManager.Instance.setFindBackData(2);
                     }
                     break;
                  case 4:
                     if(this.compareDay(_loc4_.day,_loc1_[_loc8_].DayOfWeek) && PlayerManager.Instance.Self.Grade >= int(_loc1_[_loc8_].LevelLimit) && this.compareDate(_loc4_,_loc5_,_loc6_) && !this.isPassBoss(_loc2_,4) && !this.isFindBack(_loc3_,4))
                     {
                        WantStrongManager.Instance.findBackExist = true;
                        _loc7_ = true;
                        WantStrongManager.Instance.setFindBackData(4);
                     }
                     break;
                  case 5:
                     if(this.compareDay(_loc4_.day,_loc1_[_loc8_].DayOfWeek) && PlayerManager.Instance.Self.Grade >= int(_loc1_[_loc8_].LevelLimit) && this.compareDate(_loc4_,_loc5_,_loc6_) && !this.isPassBoss(_loc2_,5) && !this.isFindBack(_loc3_,5))
                     {
                        WantStrongManager.Instance.findBackExist = true;
                        _loc7_ = true;
                        WantStrongManager.Instance.setFindBackData(3);
                     }
               }
               _loc8_++;
            }
            if(!_loc7_)
            {
               WantStrongManager.Instance.findBackExist = false;
            }
         }
         ChatManager.Instance.state = ChatManager.CHAT_HALL_STATE;
         ChatManager.Instance.view.visible = true;
         ChatManager.Instance.chatDisabled = false;
         addChild(ChatManager.Instance.view);
      }
      
      private function __checkShowWonderfulActivityTip(param1:WonderfulActivityEvent) : void
      {
         var _loc3_:Dictionary = null;
         var _loc4_:* = null;
         if(!this._isShowWonderfulActTip1 || !this._isShowWonderfulActTip2)
         {
            _loc3_ = WonderfulActivityManager.Instance.stateDic;
            for(_loc4_ in WonderfulActivityManager.Instance.stateDic)
            {
               if(_loc4_ == String(ActivityType.PAY_RETURN) && !this._isShowWonderfulActTip1)
               {
                  ChatManager.Instance.sysChatAmaranth(LanguageMgr.GetTranslation("wonderfulActivity.startTip1"));
                  this._isShowWonderfulActTip1 = true;
               }
               else if(_loc4_ == String(ActivityType.CONSUME_RETURN) && !this._isShowWonderfulActTip2)
               {
                  ChatManager.Instance.sysChatAmaranth(LanguageMgr.GetTranslation("wonderfulActivity.startTip2"));
                  this._isShowWonderfulActTip2 = true;
               }
            }
         }
         var _loc2_:int = this.checkHuiKui();
         this.createHuiKuiChat(_loc2_);
      }
      
      private function __superWinnerIsOpen(param1:Event) : void
      {
         var _loc2_:BaseAlerFrame = null;
         if(SuperWinnerManager.instance.isOpen && PlayerManager.Instance.Self.Grade >= 20)
         {
            _loc2_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("ddt.superWinner.openSuperWinner"),LanguageMgr.GetTranslation("ok"),"",false,true,true,LayerManager.BLCAK_BLOCKGOUND);
            _loc2_.addEventListener(FrameEvent.RESPONSE,this._responseEnterSuperWinner);
         }
      }
      
      private function _responseEnterSuperWinner(param1:FrameEvent) : void
      {
         var _loc2_:BaseAlerFrame = param1.currentTarget as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this._responseEnterSuperWinner);
         SoundManager.instance.playButtonSound();
         _loc2_.dispose();
      }
      
      private function createHuiKuiChat(param1:int) : void
      {
         if(param1 == 0)
         {
            return;
         }
         var _loc2_:ChatData = new ChatData();
         _loc2_.channel = ChatInputView.COMPLEX_NOTICE;
         _loc2_.childChannelArr = [ChatInputView.SYS_TIP,ChatInputView.GM_NOTICE];
         if(param1 == 1 || param1 == 2)
         {
            ChatManager.Instance.chat(this.createChatData(param1));
         }
         else if(param1 == 3)
         {
            ChatManager.Instance.chat(this.createChatData(1));
            ChatManager.Instance.chat(this.createChatData(2));
         }
      }
      
      private function createChatData(param1:int) : ChatData
      {
         var _loc2_:ChatData = new ChatData();
         _loc2_.channel = ChatInputView.COMPLEX_NOTICE;
         _loc2_.childChannelArr = [ChatInputView.SYS_TIP,ChatInputView.GM_NOTICE];
         if(param1 == 1)
         {
            _loc2_.type = ChatFormats.CLICK_ENTERHUIKUI_ACTIVITY;
            _loc2_.actId = WonderfulActivityManager.Instance.getActIdWithViewId(ActivityType.PAY_RETURN);
            _loc2_.msg = LanguageMgr.GetTranslation("wonderfulActivity.startTip3");
         }
         else if(param1 == 2)
         {
            _loc2_.type = ChatFormats.CLICK_ENTERHUIKUI_ACTIVITY;
            _loc2_.actId = WonderfulActivityManager.Instance.getActIdWithViewId(ActivityType.CONSUME_RETURN);
            _loc2_.msg = LanguageMgr.GetTranslation("wonderfulActivity.startTip4");
         }
         return _loc2_;
      }
      
      private function checkHuiKui() : int
      {
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc5_:* = null;
         var _loc4_:Dictionary = WonderfulActivityManager.Instance.stateDic;
         for(_loc5_ in WonderfulActivityManager.Instance.stateDic)
         {
            if(_loc5_ == String(ActivityType.PAY_RETURN) && WonderfulActivityManager.Instance.stateDic[_loc5_])
            {
               _loc1_ = 1;
            }
            else if(_loc5_ == String(ActivityType.CONSUME_RETURN) && WonderfulActivityManager.Instance.stateDic[_loc5_])
            {
               _loc2_ = 1;
            }
         }
         if(_loc1_ == 1 && _loc2_ == 1)
         {
            _loc3_ = 3;
         }
         else if(_loc1_ == 1)
         {
            _loc3_ = 1;
         }
         else if(_loc2_ == 1)
         {
            _loc3_ = 2;
         }
         else
         {
            _loc3_ = 0;
         }
         return _loc3_;
      }
      
      public function __CheckIsDesk(param1:TimerEvent) : void
      {
         this._timesCount++;
         if(ExternalInterface.available)
         {
            if(ExternalInterface.objectID)
            {
               this._count++;
            }
            if(this._count == 5)
            {
               this._isDesk = false;
            }
         }
         this.clearCheck();
      }
      
      private function clearCheck() : void
      {
         var _loc1_:BaseAlerFrame = null;
         if(this._timesCount == 5)
         {
            this._checkisDesk.stop();
            this._checkisDesk.removeEventListener(TimerEvent.TIMER,this.__CheckIsDesk);
            if(this._isDesk)
            {
               SocketManager.Instance.out.sendDeskTopLogin(1);
               if(PathManager.checkDeskKillSwitch)
               {
                  SocketManager.Instance.socket.close();
                  _loc1_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("alert"),LanguageMgr.GetTranslation("ddt.checkIsDeskAlert.msg"),LanguageMgr.GetTranslation("tank.room.RoomIIView2.affirm"));
                  _loc1_.addEventListener(FrameEvent.RESPONSE,this.__onCheckIsDeskAlertResponse);
               }
            }
         }
      }
      
      private function __onCheckIsDeskAlertResponse(param1:FrameEvent) : void
      {
         param1.currentTarget.removeEventListener(FrameEvent.RESPONSE,this.__onCheckIsDeskAlertResponse);
         ObjectUtils.disposeObject(param1.currentTarget);
         LeavePageManager.leaveToLoginPath();
      }
      
      private function initDdtActiviyIconState() : void
      {
         DdtActivityIconManager.Instance.addEventListener(DdtActivityIconManager.READY_START,this.readyHander);
         DdtActivityIconManager.Instance.addEventListener(DdtActivityIconManager.START,this.startHander);
      }
      
      private function removeDdtActiviyIconState() : void
      {
         DdtActivityIconManager.Instance.removeEventListener(DdtActivityIconManager.READY_START,this.readyHander);
         DdtActivityIconManager.Instance.removeEventListener(DdtActivityIconManager.START,this.startHander);
      }
      
      private function readyHander(param1:ActivitStateEvent) : void
      {
         this.changeIconState(param1.data,false);
      }
      
      private function startHander(param1:ActivitStateEvent) : void
      {
         this.changeIconState(param1.data,true);
      }
      
      private function changeIconState(param1:Array, param2:Boolean = false) : void
      {
         var _loc3_:int = param1[0];
         var _loc4_:int = param1[1];
         var _loc5_:String = param1[2];
         if(param2)
         {
            _loc5_ = "";
         }
         switch(_loc3_)
         {
            case 1:
            case 2:
            case 19:
               if(param2 && !WorldBossManager.Instance.isOpen)
               {
                  DdtActivityIconManager.Instance.isOneOpen = false;
                  return;
               }
               WorldBossManager.Instance.creatEnterIcon(param2,_loc4_,_loc5_);
               this.checkShowWorldBossHelper();
               break;
            case 4:
               this.addLeagueIcon(param2,_loc5_);
               break;
            case 5:
               this.addBattleIcon(param2,_loc5_);
               break;
            case 10:
               this.__littlegameActived(null,param2,_loc5_);
               break;
            case 18:
               CampBattleManager.instance.addCampBtn(param2,_loc5_);
               break;
            case 20:
               ConsortiaBattleManager.instance.addEntryBtn(param2,_loc5_);
         }
      }
      
      private function stopAllMc(param1:MovieClip) : void
      {
         var _loc3_:MovieClip = null;
         var _loc2_:int = 0;
         while(param1.numChildren - _loc2_)
         {
            if(param1.getChildAt(_loc2_) is MovieClip)
            {
               _loc3_ = param1.getChildAt(_loc2_) as MovieClip;
               _loc3_.stop();
               this.stopAllMc(_loc3_);
            }
            _loc2_++;
         }
      }
      
      private function playAllMc(param1:MovieClip) : void
      {
         var _loc3_:MovieClip = null;
         var _loc2_:int = 0;
         while(param1.numChildren - _loc2_)
         {
            if(param1.getChildAt(_loc2_) is MovieClip)
            {
               _loc3_ = param1.getChildAt(_loc2_) as MovieClip;
               _loc3_.play();
               this.playAllMc(_loc3_);
            }
            _loc2_++;
         }
      }
      
      private function isOnceFindBack(param1:Dictionary) : Boolean
      {
         var _loc2_:Array = null;
         for each(_loc2_ in param1)
         {
            if(_loc2_[0] || _loc2_[1])
            {
               return true;
            }
         }
         return false;
      }
      
      private function isFindBack(param1:Dictionary, param2:int) : Boolean
      {
         if(param1[param2])
         {
            if(param1[param2][0] && param1[param2][1])
            {
               return true;
            }
         }
         return false;
      }
      
      private function isPassBoss(param1:Dictionary, param2:int) : Boolean
      {
         if(!param1[param2] || param1[param2] == 0)
         {
            return false;
         }
         return true;
      }
      
      private function compareDay(param1:int, param2:String) : Boolean
      {
         var _loc3_:Array = param2.split(",");
         if(_loc3_.indexOf("" + param1) == -1)
         {
            return false;
         }
         return true;
      }
      
      private function compareDate(param1:Date, param2:int, param3:int) : Boolean
      {
         if(param1.hours < param2)
         {
            return false;
         }
         if(param1.hours == param2)
         {
            if(param1.minutes < param3)
            {
               return false;
            }
         }
         return true;
      }
      
      private function addButtonList() : void
      {
      }
      
      private function checkLuckStone() : Boolean
      {
         var _loc1_:Date = TimeManager.Instance.Now();
         if(!this.startDate)
         {
            return false;
         }
         if(_loc1_.getTime() > this.startDate.getTime() && _loc1_.getTime() < this.endDate.getTime() && this.isActiv)
         {
            this.isInLuckStone = true;
            return true;
         }
         this.isInLuckStone = false;
         return false;
      }
      
      private function __updatePetFarmGuilde(param1:UpdatePetFarmGuildeEvent) : void
      {
         PetBagController.instance().finishTask();
         var _loc2_:QuestInfo = param1.data as QuestInfo;
         if(_loc2_.QuestID == PetFarmGuildeTaskType.PET_TASK5)
         {
         }
         if(_loc2_.QuestID == PetFarmGuildeTaskType.PET_TASK4)
         {
         }
      }
      
      private function petFarmGuilde() : void
      {
         if(PetBagController.instance().haveTaskOrderByID(PetFarmGuildeTaskType.PET_TASK1))
         {
            PetBagController.instance().showPetFarmGuildArrow(ArrowType.IN_FARM,-150,"farmTrainer.infarmArrowPos","asset.farmTrainer.infarm","farmTrainer.infarmTipPos",this);
         }
         if(PetBagController.instance().haveTaskOrderByID(PetFarmGuildeTaskType.PET_TASK2))
         {
            PetBagController.instance().showPetFarmGuildArrow(ArrowType.OPEN_PET_BAG,-50,"farmTrainer.openPetArrowPos","asset.farmTrainer.clickHere","farmTrainer.openBagTipPos",this);
         }
         if(PetBagController.instance().haveTaskOrderByID(PetFarmGuildeTaskType.PET_TASK4))
         {
            PetBagController.instance().showPetFarmGuildArrow(ArrowType.OPEN_SEED_BAG,-150,"farmTrainer.openFarmArrowPos","asset.farmTrainer.seed","farmTrainer.grainopenFarmTipPosout",this);
         }
         if(PetBagController.instance().haveTaskOrderByID(PetFarmGuildeTaskType.PET_TASK5))
         {
            PetBagController.instance().showPetFarmGuildArrow(ArrowType.GRAIN_IN_FRAME,-150,"farmTrainer.openFarmArrowPos","asset.farmTrainer.grain1","farmTrainer.grainopenFarmTipPos",this);
         }
      }
      
      private function farmEnterButtonTest() : void
      {
         this.farmEnterButton = new Sprite();
         this.farmEnterButton.width = this.farmEnterButton.height = 200;
         var _loc1_:TextField = new TextField();
         _loc1_.text = "FarmEnterButton";
         this.farmEnterButton.addChild(_loc1_);
         this.farmEnterButton.mouseChildren = false;
         this.farmEnterButton.x = 200;
         this.farmEnterButton.y = 375;
         this.addChild(this.farmEnterButton);
         this.farmEnterButton.addEventListener(MouseEvent.CLICK,this.onFarmEnter);
      }
      
      private function onFarmEnter(param1:Event) : void
      {
      }
      
      private function __onAudioIILoadComplete(param1:LoaderEvent) : void
      {
         param1.loader.removeEventListener(LoaderEvent.COMPLETE,this.__onAudioIILoadComplete);
         SoundManager.instance.setupAudioResource(true);
         SoundIILoaded = true;
      }
      
      private function __onAudioLoadComplete(param1:Event) : void
      {
         param1.currentTarget.removeEventListener(Event.COMPLETE,this.__onAudioLoadComplete);
         SoundManager.instance.setupAudioResource(false);
         SoundILoaded = true;
      }
      
      private function setOptionalModule(param1:MovieClip, param2:int, param3:Boolean) : void
      {
         if(param1 == null)
         {
            return;
         }
         param1.visible = param3;
         var _loc4_:BaseButton = this._btnList[param2];
         this._btnList[param2].buttonMode = this._btnList[param2].mouseEnabled = this._btnList[param2].mouseChildren = this._btnList[param2].enable = param3;
         this._hallMainView[this.txtArray[param2]].visible = param3;
         if(!param3)
         {
            _loc4_.removeEventListener(MouseEvent.CLICK,this.__btnClick);
            _loc4_.removeEventListener(MouseEvent.MOUSE_MOVE,this.__btnOver);
            _loc4_.removeEventListener(MouseEvent.MOUSE_OUT,this.__btnOut);
         }
      }
      
      private function initBuild() : void
      {
         var _loc1_:int = PlayerManager.Instance.Self.Grade;
         this.setBuildState(3,true);
         this.setBuildState(7,_loc1_ >= 6?Boolean(true):Boolean(false));
         this.setBuildState(2,_loc1_ >= 7?Boolean(true):Boolean(false));
         this.setBuildState(5,_loc1_ >= 2?Boolean(true):Boolean(false));
         this.setBuildState(1,_loc1_ >= 2?Boolean(true):Boolean(false));
         this.setBuildState(0,_loc1_ >= 8?Boolean(true):Boolean(false));
         this.setBuildState(6,_loc1_ >= 10?Boolean(true):Boolean(false));
         this.setBuildState(4,_loc1_ >= 13?Boolean(true):Boolean(false));
         this.setBuildState(8,_loc1_ >= 25?Boolean(true):Boolean(false));
         this.setBuildStateII(9,_loc1_ >= 30?Boolean(true):Boolean(false));
      }
      
      private function setBuildStateII(param1:int, param2:Boolean) : void
      {
         var _loc3_:MovieClip = this._hallMainView[this.txtArray[param1]];
         var _loc4_:BaseButton = this._btnList[param1];
         _loc3_.buttonMode = _loc3_.mouseChildren = _loc3_.mouseEnabled = false;
         _loc4_.buttonMode = _loc4_.mouseChildren = _loc4_.mouseEnabled = _loc3_.visible = param2;
         if(param2)
         {
            _loc4_.addEventListener(MouseEvent.CLICK,this.__btnClick);
            _loc4_.addEventListener(MouseEvent.MOUSE_MOVE,this.__btnOver);
            _loc4_.addEventListener(MouseEvent.MOUSE_OUT,this.__btnOut);
         }
      }
      
      private function setBuildState(param1:int, param2:Boolean) : void
      {
         if(!WeakGuildManager.Instance.switchUserGuide || PlayerManager.Instance.Self.IsWeakGuildFinish(Step.OLD_PLAYER))
         {
            param2 = true;
         }
         var _loc3_:MovieClip = this._hallMainView[this.txtArray[param1]];
         var _loc4_:BaseButton = this._btnList[param1];
         if(_loc3_)
         {
            _loc3_.buttonMode = _loc3_.mouseChildren = _loc3_.mouseEnabled = false;
            _loc3_.visible = param2;
         }
         _loc4_.buttonMode = _loc4_.mouseChildren = _loc4_.mouseEnabled = param2;
         if(param2)
         {
            _loc4_.addEventListener(MouseEvent.CLICK,this.__btnClick);
            _loc4_.addEventListener(MouseEvent.MOUSE_MOVE,this.__btnOver);
            _loc4_.addEventListener(MouseEvent.MOUSE_OUT,this.__btnOut);
         }
      }
      
      private function addFrame() : void
      {
         if(this._isAddFrameComplete)
         {
            return;
         }
         if(TimeManager.Instance.TotalDaysToNow(PlayerManager.Instance.Self.LastDate as Date) >= 30 && PlayerManager.Instance.Self.isOldPlayerHasValidEquitAtLogin && !PlayerManager.Instance.Self.isOld)
         {
            CacheSysManager.getInstance().cacheFunction(CacheConsts.ALERT_IN_HALL,new FunctionAction(new ShopRechargeEquipServer().show));
         }
         else if(PlayerManager.Instance.Self.OvertimeListByBody.length > 0)
         {
            CacheSysManager.getInstance().cacheFunction(CacheConsts.ALERT_IN_HALL,new FunctionAction(new ShopRechargeEquipAlert().show));
         }
         else
         {
            InventoryItemInfo.startTimer();
         }
         if(AcademyManager.Instance.isRecommend())
         {
            CacheSysManager.getInstance().cacheFunction(CacheConsts.ALERT_IN_HALL,new FunctionAction(AcademyManager.Instance.recommend));
         }
         this._isAddFrameComplete = true;
      }
      
      private function loadWeakGuild() : void
      {
         if(!WeakGuildManager.Instance.switchUserGuide || PlayerManager.Instance.Self.IsWeakGuildFinish(Step.OLD_PLAYER))
         {
            return;
         }
         WeakGuildManager.Instance.checkFunction();
         if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.GAME_ROOM_OPEN))
         {
            if(!PlayerManager.Instance.Self.IsWeakGuildFinish(Step.GAME_ROOM_SHOW_OPEN))
            {
               this.showBuildOpen(Step.GAME_ROOM_SHOW_OPEN,"asset.trainer.openGameRoom");
            }
            else if(!PlayerManager.Instance.Self.IsWeakGuildFinish(Step.GAME_ROOM_CLICKED))
            {
               this.buildShine(Step.GAME_ROOM_CLICKED,"asset.trainer.RoomShineAsset","trainer.posBuildGameRoom");
            }
         }
         if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.CONSORTIA_OPEN))
         {
            if(!PlayerManager.Instance.Self.IsWeakGuildFinish(Step.CONSORTIA_SHOW))
            {
               if(!DragonBoatManager.instance.isStart)
               {
                  this.showBuildOpen(Step.CONSORTIA_SHOW,"asset.trainer.openConsortia");
               }
            }
            else if(!PlayerManager.Instance.Self.IsWeakGuildFinish(Step.CONSORTIA_CLICKED))
            {
               if(!DragonBoatManager.instance.isStart)
               {
                  this.buildShine(Step.CONSORTIA_CLICKED,"asset.trainer.shineConsortia","trainer.posBuildConsortia");
               }
            }
         }
         if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.DUNGEON_OPEN))
         {
            if(!PlayerManager.Instance.Self.IsWeakGuildFinish(Step.DUNGEON_SHOW))
            {
               this.showBuildOpen(Step.DUNGEON_SHOW,"asset.trainer.openDungeon");
            }
            else if(!PlayerManager.Instance.Self.IsWeakGuildFinish(Step.DUNGEON_CLICKED))
            {
               this.buildShine(Step.DUNGEON_CLICKED,"asset.trainer.shineDungeon","trainer.posBuildDungeon");
            }
         }
         if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.CHURCH_OPEN))
         {
            if(!PlayerManager.Instance.Self.IsWeakGuildFinish(Step.CHURCH_SHOW))
            {
               this.showBuildOpen(Step.CHURCH_SHOW,"asset.trainer.openChurch");
            }
            else if(!PlayerManager.Instance.Self.IsWeakGuildFinish(Step.CHURCH_CLICKED))
            {
               this.buildShine(Step.CHURCH_CLICKED,"asset.trainer.shineChurch","trainer.posBuildChurch");
            }
         }
         if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.TOFF_LIST_OPEN))
         {
            if(!PlayerManager.Instance.Self.IsWeakGuildFinish(Step.TOFF_LIST_SHOW))
            {
               this.showBuildOpen(Step.TOFF_LIST_SHOW,"asset.trainer.openToffList");
            }
            else if(!PlayerManager.Instance.Self.IsWeakGuildFinish(Step.TOFF_LIST_CLICKED))
            {
               this.buildShine(Step.TOFF_LIST_CLICKED,"asset.trainer.shineToffList","trainer.posBuildToffList");
            }
         }
         if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.AUCTION_OPEN))
         {
            if(!PlayerManager.Instance.Self.IsWeakGuildFinish(Step.AUCTION_SHOW))
            {
               this.showBuildOpen(Step.AUCTION_SHOW,"asset.trainer.openAuction");
            }
            else if(!PlayerManager.Instance.Self.IsWeakGuildFinish(Step.AUCTION_CLICKED))
            {
               this.buildShine(Step.AUCTION_CLICKED,"asset.trainer.shineAuction","trainer.posBuildAuction");
            }
         }
      }
      
      private function __taskFrameHide(param1:Event) : void
      {
         this.loadWeakGuild();
      }
      
      private function checkShowVote() : void
      {
         if(VoteManager.Instance.showVote)
         {
            VoteManager.Instance.addEventListener(VoteManager.LOAD_COMPLETED,this.__vote);
            if(VoteManager.Instance.loadOver)
            {
               VoteManager.Instance.removeEventListener(VoteManager.LOAD_COMPLETED,this.__vote);
               VoteManager.Instance.openVote();
            }
         }
      }
      
      private function checkDdtImportantFrame() : void
      {
         var _loc1_:DdtImportantFrame = null;
         if(SharedManager.Instance.isShowDdtImportantView)
         {
            _loc1_ = ComponentFactory.Instance.creatComponentByStylename("DdtImportantFrame");
            LayerManager.Instance.addToLayer(_loc1_,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
            SharedManager.Instance.isShowDdtImportantView = false;
            SharedManager.Instance.save();
         }
      }
      
      private function checkShowVipAlert() : void
      {
         var _loc2_:String = null;
         var _loc1_:SelfInfo = PlayerManager.Instance.Self;
         if(!_loc1_.isSameDay && !VipController.instance.isRechargePoped)
         {
            VipController.instance.isRechargePoped = true;
            if(_loc1_.IsVIP)
            {
               if(_loc1_.VIPLeftDays <= VIP_LEFT_DAY_TO_COMFIRM && _loc1_.VIPLeftDays >= 0 || _loc1_.VIPLeftDays == VIP_LEFT_DAY_FIRST_PROMPT)
               {
                  _loc2_ = "";
                  if(_loc1_.VIPLeftDays == 0)
                  {
                     if(_loc1_.VipLeftHours > 0)
                     {
                        _loc2_ = LanguageMgr.GetTranslation("ddt.vip.vipView.expiredToday",_loc1_.VipLeftHours);
                     }
                     else if(_loc1_.VipLeftHours == 0)
                     {
                        _loc2_ = LanguageMgr.GetTranslation("ddt.vip.vipView.expiredHour");
                     }
                     else
                     {
                        _loc2_ = LanguageMgr.GetTranslation("ddt.vip.vipView.expiredTrue");
                     }
                  }
                  else
                  {
                     _loc2_ = LanguageMgr.GetTranslation("ddt.vip.vipView.expired",_loc1_.VIPLeftDays);
                  }
                  this._renewal = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),_loc2_,LanguageMgr.GetTranslation("ddt.vip.vipView.RenewalNow"),"",false,false,false,LayerManager.ALPHA_BLOCKGOUND);
                  this._renewal.moveEnable = false;
                  this._renewal.addEventListener(FrameEvent.RESPONSE,this.__goRenewal);
               }
            }
            else if(_loc1_.VIPExp > 0)
            {
               if(_loc1_.LastDate.valueOf() < _loc1_.VIPExpireDay.valueOf() && _loc1_.VIPExpireDay.valueOf() <= _loc1_.systemDate.valueOf())
               {
                  this._renewal = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("ddt.vip.vipView.expiredTrue"),LanguageMgr.GetTranslation("ddt.vip.vipView.RenewalNow"),"",false,false,false,LayerManager.ALPHA_BLOCKGOUND);
                  this._renewal.moveEnable = false;
                  this._renewal.addEventListener(FrameEvent.RESPONSE,this.__goRenewal);
               }
            }
         }
      }
      
      private function checkShowVipAlert_New() : void
      {
         var _loc1_:SelfInfo = PlayerManager.Instance.Self;
         if(!_loc1_.isSameDay && !VipController.instance.isRechargePoped)
         {
            VipController.instance.isRechargePoped = true;
            if(_loc1_.IsVIP)
            {
               if(_loc1_.VIPLeftDays <= VIP_LEFT_DAY_TO_COMFIRM && _loc1_.VIPLeftDays >= 0 || _loc1_.VIPLeftDays == VIP_LEFT_DAY_FIRST_PROMPT)
               {
                  VipController.instance.showRechargeAlert();
               }
            }
            else if(_loc1_.VIPExp > 0)
            {
               VipController.instance.showRechargeAlert();
            }
         }
      }
      
      private function __goRenewal(param1:FrameEvent) : void
      {
         this._renewal.removeEventListener(FrameEvent.RESPONSE,this.__goRenewal);
         switch(param1.responseCode)
         {
            case FrameEvent.SUBMIT_CLICK:
            case FrameEvent.ENTER_CLICK:
               VipController.instance.show();
         }
         this._renewal.dispose();
         if(this._renewal.parent)
         {
            this._renewal.parent.removeChild(this._renewal);
         }
         this._renewal = null;
      }
      
      private function __vote(param1:Event) : void
      {
         VoteManager.Instance.removeEventListener(VoteManager.LOAD_COMPLETED,this.__vote);
         VoteManager.Instance.openVote();
      }
      
      private function checkShowBossBox() : void
      {
         if(BossBoxManager.instance.isShowBoxButton())
         {
            if(!this._boxButton)
            {
               this._boxButton = new SmallBoxButton(SmallBoxButton.HALL_POINT);
               BossBoxManager.instance.startDelayTime();
            }
            addChild(this._boxButton);
         }
      }
      
      private function checkShowLimiAward() : Boolean
      {
         if(CalendarManager.getInstance().checkEventInfo() && PlayerManager.Instance.Self.Grade >= 8)
         {
            this._limitAwardButton = new LimitAwardButton(LimitAwardButton.HALL_POINT);
            this._limitAwardButton.x = 5;
            this._limitAwardButton.y = 21;
            this._limitAwardButton.width = 75;
            this._limitAwardButton.height = 70;
            return true;
         }
         return false;
      }
      
      private function checkShowStoreFromShop() : void
      {
         if(BagStore.instance.isFromShop)
         {
            BagStore.instance.isFromShop = false;
            BagStore.instance.show();
         }
      }
      
      private function toDungeon() : void
      {
         if(!WeakGuildManager.Instance.checkOpen(Step.DUNGEON_OPEN,8))
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.functionLimitTip",8));
            return;
         }
         if(!PlayerManager.Instance.checkEnterDungeon)
         {
            return;
         }
         StateManager.setState(StateType.DUNGEON_LIST);
         ComponentSetting.SEND_USELOG_ID(4);
         if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.DUNGEON_OPEN) && !PlayerManager.Instance.Self.IsWeakGuildFinish(Step.DUNGEON_CLICKED))
         {
            SocketManager.Instance.out.syncWeakStep(Step.DUNGEON_CLICKED);
         }
      }
      
      private function toFarmSelf() : void
      {
         FarmModelController.instance.goFarm(PlayerManager.Instance.Self.ID,PlayerManager.Instance.Self.NickName);
      }
      
      private function __btnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("047");
         var _loc2_:int = this._btnList.indexOf(param1.currentTarget as BaseButton);
         switch(_loc2_)
         {
            case 4:
               if(!WeakGuildManager.Instance.checkOpen(Step.AUCTION_OPEN,14))
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.functionLimitTip",14));
                  return;
               }
               StateManager.setState(StateType.AUCTION);
               ComponentSetting.SEND_USELOG_ID(7);
               if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.AUCTION_OPEN) && !PlayerManager.Instance.Self.IsWeakGuildFinish(Step.AUCTION_CLICKED))
               {
                  SocketManager.Instance.out.syncWeakStep(Step.AUCTION_CLICKED);
               }
               break;
            case 8:
               this.toFarmSelf();
               break;
            case 0:
               if(!WeakGuildManager.Instance.checkOpen(Step.CHURCH_OPEN,10))
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.functionLimitTip",10));
                  return;
               }
               StateManager.setState(StateType.DDTCHURCH_ROOM_LIST);
               ComponentSetting.SEND_USELOG_ID(6);
               if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.CHURCH_OPEN) && !PlayerManager.Instance.Self.IsWeakGuildFinish(Step.CHURCH_CLICKED))
               {
                  SocketManager.Instance.out.syncWeakStep(Step.CHURCH_CLICKED);
               }
               break;
            case 7:
               if(!WeakGuildManager.Instance.checkOpen(Step.CONSORTIA_OPEN,7))
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.functionLimitTip",7));
                  return;
               }
               StateManager.setState(StateType.CONSORTIA);
               ComponentSetting.SEND_USELOG_ID(5);
               if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.CONSORTIA_OPEN) && !PlayerManager.Instance.Self.IsWeakGuildFinish(Step.CONSORTIA_CLICKED))
               {
                  SocketManager.Instance.out.syncWeakStep(Step.CONSORTIA_CLICKED);
               }
               break;
            case 2:
               this.toDungeon();
               break;
            case 3:
               if(!WeakGuildManager.Instance.checkOpen(Step.GAME_ROOM_OPEN,2))
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.functionLimitTip",2));
                  return;
               }
               StateManager.setState(StateType.ROOM_LIST);
               ComponentSetting.SEND_USELOG_ID(3);
               if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.GAME_ROOM_OPEN) && !PlayerManager.Instance.Self.IsWeakGuildFinish(Step.GAME_ROOM_CLICKED))
               {
                  SocketManager.Instance.out.syncWeakStep(Step.GAME_ROOM_CLICKED);
               }
               break;
            case 1:
               if(!WeakGuildManager.Instance.checkOpen(Step.SHOP_OPEN,3))
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.functionLimitTip",3));
                  return;
               }
               StateManager.setState(StateType.SHOP);
               ComponentSetting.SEND_USELOG_ID(1);
               break;
            case 5:
               if(WeakGuildManager.Instance.switchUserGuide && !PlayerManager.Instance.Self.IsWeakGuildFinish(Step.OLD_PLAYER))
               {
                  if(PlayerManager.Instance.Self.Grade < 3)
                  {
                     MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.functionLimitTip",3));
                     return;
                  }
               }
               BagStore.instance.show(BagStore.BAG_STORE);
               ComponentSetting.SEND_USELOG_ID(2);
               break;
            case 6:
               if(!WeakGuildManager.Instance.checkOpen(Step.TOFF_LIST_OPEN,12))
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.functionLimitTip",12));
                  return;
               }
               StateManager.setState(StateType.TOFFLIST);
               ComponentSetting.SEND_USELOG_ID(8);
               if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.TOFF_LIST_OPEN) && !PlayerManager.Instance.Self.IsWeakGuildFinish(Step.TOFF_LIST_CLICKED))
               {
                  SocketManager.Instance.out.syncWeakStep(Step.TOFF_LIST_CLICKED);
               }
               break;
            case 9:
               if(!WeakGuildManager.Instance.checkOpen(Step.TOFF_LIST_OPEN,30))
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.functionLimitTip",30));
                  return;
               }
               LabyrinthManager.Instance.show();
               break;
         }
      }
      
      private function __btnOver(param1:MouseEvent) : void
      {
         var _loc2_:int = this._btnList.indexOf(param1.currentTarget as BaseButton);
         switch(_loc2_)
         {
            case 4:
               this._hallMainView["auctionButton"].gotoAndStop(2);
               this._hallMainView.gotoAndStop(6);
               break;
            case 0:
               this._hallMainView["churchButton"].gotoAndStop(2);
               this._hallMainView.gotoAndStop(3);
               break;
            case 7:
               this._hallMainView["consortiaButton"].gotoAndStop(2);
               this._hallMainView.gotoAndStop(4);
               break;
            case 2:
               this._hallMainView["dungeonButton"].gotoAndStop(2);
               this._hallMainView.gotoAndStop(2);
               break;
            case 6:
               this._hallMainView["tofflistButton"].gotoAndStop(2);
               this._hallMainView.gotoAndStop(7);
               break;
            case 3:
               this._hallMainView["roomListButton"].gotoAndStop(2);
               this._hallMainView.gotoAndStop(8);
               break;
            case 1:
               this._hallMainView["shopButton"].gotoAndStop(2);
               this._hallMainView.gotoAndStop(1);
               break;
            case 5:
               this._hallMainView["storeButton"].gotoAndStop(2);
               this._hallMainView.gotoAndStop(5);
               break;
            case 8:
               this._hallMainView["farmButton"].gotoAndStop(2);
               this._hallMainView.gotoAndStop(9);
               break;
            case 9:
               this._hallMainView["labyrinthButton"].gotoAndStop(2);
         }
      }
      
      private function __btnOut(param1:MouseEvent) : void
      {
         if(this._hallMainView)
         {
            this._hallMainView.gotoAndStop(10);
         }
         this._hallMainView["auctionButton"].gotoAndStop(1);
         this._hallMainView["churchButton"].gotoAndStop(1);
         this._hallMainView["consortiaButton"].gotoAndStop(1);
         this._hallMainView["dungeonButton"].gotoAndStop(1);
         this._hallMainView["tofflistButton"].gotoAndStop(1);
         this._hallMainView["roomListButton"].gotoAndStop(1);
         this._hallMainView["shopButton"].gotoAndStop(1);
         this._hallMainView["storeButton"].gotoAndStop(1);
         this._hallMainView["farmButton"].gotoAndStop(1);
         this._hallMainView["labyrinthButton"].gotoAndStop(1);
      }
      
      private function loadUserGuide() : void
      {
         if(NewHandGuideManager.Instance.progress < Step.POP_EXPLAIN_ONE && WeakGuildManager.Instance.switchUserGuide)
         {
            if(PathManager.TRAINER_STANDALONE && !PlayerManager.Instance.Self.IsWeakGuildFinish(Step.GAIN_ADDONE))
            {
               SocketManager.Instance.out.syncStep(Step.POP_EXPLAIN_ONE,true);
               this.prePopWelcome();
               this.sendToLoginInterface();
               DuowanInterfaceManage.Instance.dispatchEvent(new DuowanInterfaceEvent(DuowanInterfaceEvent.ADD_ROLE));
            }
         }
         MainToolBar.Instance.tipTask();
      }
      
      private function sendToLoginInterface() : void
      {
         var _loc5_:RequestLoader = null;
         var _loc1_:URLVariables = new URLVariables();
         var _loc2_:String = PlayerManager.Instance.Self.ID.toString();
         _loc2_ = encodeURI(_loc2_);
         var _loc3_:String = "sdkxccjlqaoehtdwjkdycdrw";
         _loc1_["username"] = _loc2_;
         _loc1_["sign"] = MD5.hash(_loc2_ + _loc3_);
         var _loc4_:String = PathManager.callLoginInterface();
         if(_loc4_)
         {
            _loc5_ = LoadResourceManager.Instance.createLoader(_loc4_,BaseLoader.REQUEST_LOADER,_loc1_);
            LoadResourceManager.Instance.startLoad(_loc5_);
         }
      }
      
      private function prePopWelcome() : void
      {
         this._trainerWelcomeView = ComponentFactory.Instance.creat("trainer.welcome.mainFrame");
         this._trainerWelcomeView.addEventListener(FrameEvent.RESPONSE,this.__trainerResponse);
         this._trainerWelcomeView.show();
      }
      
      private function __trainerResponse(param1:FrameEvent) : void
      {
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            param1.currentTarget.removeEventListener(FrameEvent.RESPONSE,this.__trainerResponse);
            SoundManager.instance.play("008");
            if(!PathManager.TRAINER_STANDALONE && !PlayerManager.Instance.Self.IsWeakGuildFinish(Step.GAIN_ADDONE))
            {
               NewHandGuideManager.Instance.mapID = 111;
               SocketManager.Instance.out.createUserGuide();
            }
            this.finPopWelcome();
         }
      }
      
      private function exePopWelcome() : Boolean
      {
         return RoomManager.Instance.current != null;
      }
      
      private function finPopWelcome() : void
      {
         this._trainerWelcomeView.dispose();
         this._trainerWelcomeView = null;
      }
      
      private function showSPAAlert() : void
      {
         var _loc1_:Frame = ComponentFactory.Instance.creatComponentByStylename("hall.hotSpringAlertFrame");
         _loc1_.addEventListener(FrameEvent.RESPONSE,this.__onRespose);
         LayerManager.Instance.addToLayer(_loc1_,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
      }
      
      private function __onRespose(param1:FrameEvent) : void
      {
         var _loc2_:Frame = param1.currentTarget as Frame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.__onRespose);
         _loc2_.dispose();
      }
      
      private function __battleBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         this.battleFrameClose();
      }
      
      private function battleFrameClose() : void
      {
         this._battleFrame.removeEventListener(FrameEvent.RESPONSE,this.__frameEventHandler);
         this._battleBtn.removeEventListener(MouseEvent.CLICK,this.__battleBtnClick);
         this._battlePanel = null;
         this._battleImg = null;
         this._battleFrame.dispose();
      }
      
      private function __frameEventHandler(param1:FrameEvent) : void
      {
         switch(param1.responseCode)
         {
            case FrameEvent.ESC_CLICK:
            case FrameEvent.CLOSE_CLICK:
               SoundManager.instance.play("008");
               this.battleFrameClose();
         }
      }
      
      override public function leaving(param1:BaseStateView) : void
      {
         var _loc2_:int = 0;
         this.removeDdtActiviyIconState();
         VoteManager.Instance.removeEventListener(VoteManager.LOAD_COMPLETED,this.__vote);
         SuperWinnerManager.instance.removeEventListener(SuperWinnerManager.ROOM_IS_OPEN,this.__superWinnerIsOpen);
         TaskManager.instance.removeEventListener(TaskMainFrame.TASK_FRAME_HIDE,this.__taskFrameHide);
         MainToolBar.Instance.hide();
         MainToolBar.Instance.updateReturnBtn(MainToolBar.LEAVE_HALL);
         DailyButtunBar.Insance.hide();
         KingBlessManager.instance.clearConfirmFrame();
         DragonBoatManager.instance.disposeEntryBtn();
         NewHandContainer.Instance.clearArrowByID(ArrowType.HALL_BUILD);
         SystemOpenPromptManager.instance.dispose();
         if(this._timer && this._timer.running)
         {
            this._timer.stop();
         }
         if(this._shine)
         {
            while(this._shine.length > 0)
            {
               this._shine.shift().dispose();
            }
            this._shine = null;
         }
         if(this._black)
         {
            ObjectUtils.disposeObject(this._black);
            this._black = null;
         }
         if(this._hallMainView)
         {
            _loc2_ = 0;
            while(_loc2_ < this._btnList.length)
            {
               this._btnList[_loc2_].removeEventListener(MouseEvent.CLICK,this.__btnClick);
               this._btnList[_loc2_].removeEventListener(MouseEvent.MOUSE_MOVE,this.__btnOver);
               this._btnList[_loc2_].removeEventListener(MouseEvent.MOUSE_OUT,this.__btnOut);
               _loc2_++;
            }
            if(this._hallMainView.parent)
            {
               removeChild(this._hallMainView);
            }
            this._hallMainView = null;
         }
         if(this._channalMc)
         {
            this._channalMc.stop();
            ObjectUtils.disposeObject(this._channalMc);
            this._channalMc = null;
         }
         if(this.list)
         {
            this.list.dispose();
         }
         this.list = null;
         if(this._limitAwardButton)
         {
            ObjectUtils.disposeObject(this._limitAwardButton);
         }
         this._limitAwardButton = null;
         if(PathManager.solveWeeklyEnable())
         {
            TimesManager.Instance.hideButton();
         }
         if(param1.getType() != StateType.DUNGEON_LIST && param1.getType() != StateType.ROOM_LIST)
         {
            GameInSocketOut.sendExitScene();
         }
         if(this._hallLeftIconView)
         {
            ObjectUtils.disposeObject(this._hallLeftIconView);
            this._hallLeftIconView = null;
         }
         if(this._hallRightIconView)
         {
            ObjectUtils.disposeObject(this._hallRightIconView);
            this._hallRightIconView = null;
         }
         this.defaultRightWonderfulPlayIconRemove();
         this.defaultRightActivityIconRemove();
         super.leaving(param1);
      }
      
      override public function prepare() : void
      {
         super.prepare();
         this._isFirst = true;
      }
      
      override public function fadingComplete() : void
      {
         var _loc1_:SaveFileWidow = null;
         super.fadingComplete();
         if(this._isFirst)
         {
            this._isFirst = false;
            if(LoaderSavingManager.cacheAble == false && PlayerManager.Instance.Self.IsFirst > 1 && PlayerManager.Instance.Self.LastServerId == -1)
            {
               _loc1_ = ComponentFactory.Instance.creatComponentByStylename("hall.SaveFileWidow");
               _loc1_.show();
            }
            LeavePageManager.setFavorite(PlayerManager.Instance.Self.IsFirst <= 1);
         }
      }
      
      private function showBuildOpen(param1:int, param2:String) : void
      {
         var _loc3_:String = null;
         if(StateManager.currentStateType != StateType.MAIN)
         {
            return;
         }
         SoundManager.instance.play("159");
         var _loc4_:MovieClipWrapper = new MovieClipWrapper(ClassUtils.CreatInstance(param2),true,true);
         _loc4_.movie.mouseEnabled = _loc4_.movie.mouseChildren = false;
         addChild(_loc4_.movie);
         SocketManager.Instance.out.syncWeakStep(param1);
         switch(param1)
         {
            case Step.GAME_ROOM_SHOW_OPEN:
               _loc4_.addEventListener(Event.COMPLETE,this.__completeGameRoom);
               _loc3_ = "tank.hall.ChooseHallView.roomList";
               break;
            case Step.CONSORTIA_SHOW:
               _loc4_.addEventListener(Event.COMPLETE,this.__completeConsortia);
               _loc3_ = "tank.hall.ChooseHallView.consortia";
               break;
            case Step.DUNGEON_SHOW:
               _loc4_.addEventListener(Event.COMPLETE,this.__completeDungeon);
               _loc3_ = "tank.hall.ChooseHallView.dungeon";
               break;
            case Step.CHURCH_SHOW:
               _loc4_.addEventListener(Event.COMPLETE,this.__completeChurch);
               _loc3_ = "tank.hall.ChooseHallView.church";
               break;
            case Step.TOFF_LIST_SHOW:
               _loc4_.addEventListener(Event.COMPLETE,this.__completeToffList);
               _loc3_ = "tank.hall.ChooseHallView.tofflist";
               break;
            case Step.AUCTION_SHOW:
               _loc4_.addEventListener(Event.COMPLETE,this.__completeAuction);
               _loc3_ = "tank.hall.ChooseHallView.auction";
         }
         WeakGuildManager.Instance.openBuildTip(_loc3_);
      }
      
      private function __completeGameRoom(param1:Event) : void
      {
         MovieClipWrapper(param1.currentTarget).removeEventListener(Event.COMPLETE,this.__completeGameRoom);
         this.buildShine(Step.GAME_ROOM_CLICKED,"asset.trainer.RoomShineAsset","trainer.posBuildGameRoom");
      }
      
      private function __completeConsortia(param1:Event) : void
      {
         MovieClipWrapper(param1.currentTarget).removeEventListener(Event.COMPLETE,this.__completeConsortia);
         this.buildShine(Step.CONSORTIA_CLICKED,"asset.trainer.shineConsortia","trainer.posBuildConsortia");
      }
      
      private function __completeDungeon(param1:Event) : void
      {
         MovieClipWrapper(param1.currentTarget).removeEventListener(Event.COMPLETE,this.__completeDungeon);
         this.buildShine(Step.DUNGEON_CLICKED,"asset.trainer.shineDungeon","trainer.posBuildDungeon");
      }
      
      private function __completeChurch(param1:Event) : void
      {
         MovieClipWrapper(param1.currentTarget).removeEventListener(Event.COMPLETE,this.__completeChurch);
         this.buildShine(Step.CHURCH_CLICKED,"asset.trainer.shineChurch","trainer.posBuildChurch");
      }
      
      private function __completeToffList(param1:Event) : void
      {
         MovieClipWrapper(param1.currentTarget).removeEventListener(Event.COMPLETE,this.__completeToffList);
         this.buildShine(Step.TOFF_LIST_CLICKED,"asset.trainer.shineToffList","trainer.posBuildToffList");
      }
      
      private function __completeAuction(param1:Event) : void
      {
         MovieClipWrapper(param1.currentTarget).removeEventListener(Event.COMPLETE,this.__completeAuction);
         this.buildShine(Step.AUCTION_CLICKED,"asset.trainer.shineAuction","trainer.posBuildAuction");
      }
      
      private function __onClickServerName(param1:MouseEvent) : void
      {
      }
      
      private function buildShine(param1:int, param2:String, param3:String) : void
      {
         if(StateManager.currentStateType != StateType.MAIN)
         {
            return;
         }
         var _loc4_:Point = ComponentFactory.Instance.creatCustomObject(param3);
         var _loc5_:MovieClipWrapper = new MovieClipWrapper(ClassUtils.CreatInstance(param2),true,false);
         _loc5_.movie.mouseEnabled = _loc5_.movie.mouseChildren = false;
         _loc5_.repeat = true;
         _loc5_.movie.x = _loc4_.x;
         _loc5_.movie.y = _loc4_.y;
         addChild(_loc5_.movie);
         if(!this._shine)
         {
            this._shine = new Vector.<MovieClipWrapper>();
         }
         this._shine.push(_loc5_);
         NewHandContainer.Instance.clearArrowByID(-1);
         switch(param1)
         {
            case Step.GAME_ROOM_CLICKED:
               NewHandContainer.Instance.showArrow(ArrowType.HALL_BUILD,145,"trainer.gameRoomArrowPos","asset.trainer.txtClickHall","trainer.gameRoomTipPos",this);
               break;
            case Step.CIVIL_CLICKED:
               NewHandContainer.Instance.showArrow(ArrowType.HALL_BUILD,-150,"trainer.civilArrowPos","asset.trainer.txtClickCivil","trainer.civilTipPos",this);
               break;
            case Step.MASTER_ROOM_CLICKED:
               NewHandContainer.Instance.showArrow(ArrowType.HALL_BUILD,-150,"trainer.masterRoomArrowPos","asset.trainer.txtClickMasterRoom","trainer.masterRoomTipPos",this);
               break;
            case Step.CONSORTIA_CLICKED:
               NewHandContainer.Instance.showArrow(ArrowType.HALL_BUILD,45,"trainer.consortiaArrowPos","asset.trainer.txtClickConsortia","trainer.consortiaTipPos",this);
               break;
            case Step.DUNGEON_CLICKED:
               NewHandContainer.Instance.showArrow(ArrowType.HALL_BUILD,-45,"trainer.dungeonArrowPos","asset.trainer.txtClickDungeon","trainer.dungeonTipPos",this);
               break;
            case Step.CHURCH_CLICKED:
               NewHandContainer.Instance.showArrow(ArrowType.HALL_BUILD,150,"trainer.churchArrowPos","asset.trainer.txtClickChurch","trainer.churchTipPos",this);
               break;
            case Step.TOFF_LIST_CLICKED:
               NewHandContainer.Instance.showArrow(ArrowType.HALL_BUILD,150,"trainer.toffListArrowPos","asset.trainer.txtClickToffList","trainer.toffListTipPos",this);
               break;
            case Step.HOT_WELL_CLICKED:
               NewHandContainer.Instance.showArrow(ArrowType.HALL_BUILD,150,"trainer.hotWellArrowPos","asset.trainer.txtClickHotWell","trainer.hotWellTipPos",this);
               break;
            case Step.AUCTION_CLICKED:
               NewHandContainer.Instance.showArrow(ArrowType.HALL_BUILD,-150,"trainer.auctionArrowPos","asset.trainer.txtClickAuction","trainer.auctionTipPos",this);
               break;
            case Step.CAMPAIGN_LAB_CLICKED:
               NewHandContainer.Instance.showArrow(ArrowType.HALL_BUILD,150,"trainer.campaignLabArrowPos","asset.trainer.txtClickCampaignLab","trainer.campaignLabTipPos",this);
         }
      }
      
      override public function refresh() : void
      {
         var _loc1_:StageCurtain = new StageCurtain();
         _loc1_.play(25);
         LayerManager.Instance.clearnGameDynamic();
         ShowTipManager.Instance.removeAllTip();
         ChatManager.Instance.state = ChatManager.CHAT_HALL_STATE;
         ChatManager.Instance.view.visible = true;
         ChatManager.Instance.chatDisabled = false;
         addChild(ChatManager.Instance.view);
         super.refresh();
      }
      
      private function checkShowWorldBossEntrance() : void
      {
         WorldBossManager.Instance.addshowHallEntranceBtn();
         if(WorldBossManager.Instance.bossInfo && !WorldBossManager.Instance.bossInfo.fightOver)
         {
            (this._hallMainView["woldboss_sky"] as MovieClip).visible = true;
         }
         else
         {
            (this._hallMainView["woldboss_sky"] as MovieClip).visible = false;
         }
         WorldBossManager.Instance.showHallSkyEffort(this._hallMainView["woldboss_sky"] as MovieClip);
      }
      
      private function checkShowWorldBossHelper() : void
      {
         if(WorldBossManager.Instance.bossInfo && !WorldBossManager.Instance.bossInfo.fightOver && WorldBossHelperManager.Instance.helperOpen && !WorldBossHelperManager.Instance.isInWorldBossHelperFrame && !WorldBossHelperManager.Instance.isHelperInited)
         {
            if(WorldBossHelperManager.Instance.isHelperOnlyOnce)
            {
               if(WorldBossManager.Instance.worldBossNum > 1)
               {
                  return;
               }
               WorldBossHelperManager.Instance.setup();
            }
            else
            {
               WorldBossHelperManager.Instance.setup();
            }
         }
      }
   }
}
