package trainer.view
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.utils.ClassUtils;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.PlayerManager;
   import flash.display.DisplayObjectContainer;
   import flash.display.MovieClip;
   import flash.geom.Point;
   import flash.utils.Dictionary;
   import flash.utils.setTimeout;
   import trainer.controller.WeakGuildManager;
   import trainer.data.Step;
   
   public class NewHandContainer
   {
      
      private static var _instance:NewHandContainer;
       
      
      private var _arrows:Dictionary;
      
      private var _movies:Dictionary;
      
      public function NewHandContainer(param1:NewHandContainerEnforcer)
      {
         super();
         this._arrows = new Dictionary();
         this._movies = new Dictionary();
      }
      
      public static function get Instance() : NewHandContainer
      {
         if(!_instance)
         {
            _instance = new NewHandContainer(new NewHandContainerEnforcer());
         }
         return _instance;
      }
      
      public function showArrow(param1:int, param2:int, param3:String, param4:String = "", param5:String = "", param6:DisplayObjectContainer = null, param7:int = 0, param8:Boolean = false, param9:Point = null, param10:Point = null) : void
      {
         var _loc12_:Point = null;
         var _loc13_:MovieClip = null;
         var _loc14_:MovieClip = null;
         var _loc15_:Point = null;
         if(this.hasArrow(param1))
         {
            this.clearArrow(param1);
         }
         if(!param8)
         {
            if(!WeakGuildManager.Instance.switchUserGuide || PlayerManager.Instance.Self.IsWeakGuildFinish(Step.OLD_PLAYER))
            {
               return;
            }
         }
         var _loc11_:Object = {};
         _loc12_ = ComponentFactory.Instance.creatCustomObject(param3);
         _loc13_ = ClassUtils.CreatInstance("asset.trainer.TrainerArrowAsset");
         _loc13_.mouseChildren = false;
         _loc13_.mouseEnabled = false;
         _loc13_.rotation = param2;
         _loc13_.x = _loc12_.x;
         _loc13_.y = _loc12_.y;
         if(param9)
         {
            _loc13_.x = param9.x;
            _loc13_.y = param9.y;
         }
         if(param6)
         {
            param6.addChild(_loc13_);
         }
         else
         {
            LayerManager.Instance.addToLayer(_loc13_,LayerManager.GAME_UI_LAYER,false,LayerManager.NONE_BLOCKGOUND);
         }
         _loc11_["arrow"] = _loc13_;
         if(param4 != "")
         {
            _loc14_ = ClassUtils.CreatInstance(param4);
            _loc14_.mouseChildren = false;
            _loc14_.mouseEnabled = false;
            if(param5 != "")
            {
               _loc15_ = ComponentFactory.Instance.creatCustomObject(param5);
               _loc14_.x = _loc15_.x;
               _loc14_.y = _loc15_.y;
            }
            if(param10)
            {
               _loc14_.x = param10.x;
               _loc14_.y = param10.y;
            }
            if(param6)
            {
               param6.addChild(_loc14_);
            }
            else
            {
               LayerManager.Instance.addToLayer(_loc14_,LayerManager.GAME_UI_LAYER,false,LayerManager.NONE_BLOCKGOUND);
            }
            _loc11_["tip"] = _loc14_;
         }
         this._arrows[param1] = _loc11_;
         if(param7 > 0)
         {
            setTimeout(this.clearArrow,param7,param1);
         }
      }
      
      public function clearArrowByID(param1:int) : void
      {
         var _loc2_:* = null;
         if(param1 == -1)
         {
            for(_loc2_ in this._arrows)
            {
               this.clearArrow(int(_loc2_));
            }
         }
         else
         {
            this.clearArrow(param1);
         }
      }
      
      public function hasArrow(param1:int) : Boolean
      {
         return this._arrows[param1] != null;
      }
      
      public function showMovie(param1:String, param2:String = "") : void
      {
         var _loc3_:MovieClip = null;
         var _loc4_:Point = null;
         if(this._movies[param1])
         {
            throw new Error("Already has a arrow with this id!");
         }
         _loc3_ = ClassUtils.CreatInstance(param1);
         _loc3_.mouseEnabled = _loc3_.mouseChildren = false;
         if(param2 != "")
         {
            _loc4_ = ComponentFactory.Instance.creatCustomObject(param2);
            _loc3_.x = _loc4_.x;
            _loc3_.y = _loc4_.y;
         }
         LayerManager.Instance.addToLayer(_loc3_,LayerManager.GAME_DYNAMIC_LAYER,false,LayerManager.NONE_BLOCKGOUND);
         this._movies[param1] = _loc3_;
      }
      
      public function hideMovie(param1:String) : void
      {
         var _loc2_:* = null;
         if(param1 == "-1")
         {
            for(_loc2_ in this._movies)
            {
               this.clearMovie(_loc2_);
            }
         }
         else
         {
            this.clearMovie(param1);
         }
      }
      
      private function clearArrow(param1:int) : void
      {
         var _loc2_:Object = this._arrows[param1];
         if(_loc2_)
         {
            ObjectUtils.disposeObject(_loc2_["arrow"]);
            ObjectUtils.disposeObject(_loc2_["tip"]);
         }
         delete this._arrows[param1];
      }
      
      private function clearMovie(param1:String) : void
      {
         ObjectUtils.disposeObject(this._movies[param1]);
         delete this._movies[param1];
      }
      
      public function dispose() : void
      {
         _instance = null;
         this._arrows = null;
         this._movies = null;
      }
   }
}

class NewHandContainerEnforcer
{
    
   
   function NewHandContainerEnforcer()
   {
      super();
   }
}
