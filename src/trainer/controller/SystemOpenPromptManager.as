package trainer.controller
{
   import bagAndInfo.BagAndInfoManager;
   import battleGroud.BattleGroudManager;
   import calendar.CalendarManager;
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.ui.ComponentFactory;
   import consortion.ConsortionModelControl;
   import ddt.bagStore.BagStore;
   import ddt.data.UIModuleTypes;
   import ddt.manager.PathManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.StateManager;
   import ddt.states.StateType;
   import farm.FarmModelController;
   import flash.events.EventDispatcher;
   import mainbutton.MainButtnController;
   import pet.sprite.PetSpriteController;
   import trainer.data.Step;
   import trainer.view.SystemOpenPromptFrame;
   import treasure.controller.TreasureManager;
   import treasure.model.TreasureModel;
   
   public class SystemOpenPromptManager extends EventDispatcher
   {
      
      public static const TOTEM:int = 1;
      
      public static const GEMSTONE:int = 2;
      
      public static const GET_AWARD:int = 3;
      
      public static const SIGN:int = 4;
      
      public static const TREASURE:int = 7;
      
      public static const CONSORTIA_BOSS_OPEN:int = 5;
      
      public static const BATTLE_GROUND_OPEN:int = 6;
      
      public static const FARM_CROP_RIPE:int = 9;
      
      public static const SEVEN_DOUBLE_DUNGEON:int = 8;
      
      private static var _instance:SystemOpenPromptManager;
       
      
      private var _isLoadComplete:Boolean = false;
      
      private var _frameList:Object;
      
      private var _isJudge:Boolean = false;
      
      public function SystemOpenPromptManager()
      {
         super(null);
      }
      
      public static function get instance() : SystemOpenPromptManager
      {
         if(_instance == null)
         {
            _instance = new SystemOpenPromptManager();
         }
         return _instance;
      }
      
      public function loadModule() : void
      {
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadCompleteHandler);
         UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.SYSTEM_OPEN_PROMPT);
      }
      
      private function loadCompleteHandler(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.SYSTEM_OPEN_PROMPT)
         {
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadCompleteHandler);
            this._isLoadComplete = true;
            this.showFrame();
         }
      }
      
      public function showFrame() : void
      {
         var _loc1_:SystemOpenPromptFrame = null;
         this._frameList = {};
         if(PetSpriteController.Instance.checkFarmCropRipe())
         {
            if(!this._isLoadComplete)
            {
               this.loadModule();
               return;
            }
            _loc1_ = ComponentFactory.Instance.creatCustomObject("SystemOpenPromptFrame");
            _loc1_.show(FARM_CROP_RIPE,this.gotoSystem);
            this._frameList[FARM_CROP_RIPE] = _loc1_;
         }
         if(PlayerManager.Instance.Self.Grade >= 20 && !PlayerManager.Instance.Self.isNewOnceFinish(Step.TOTEM_OPEN))
         {
            if(!this._isLoadComplete)
            {
               this.loadModule();
               return;
            }
            _loc1_ = ComponentFactory.Instance.creatCustomObject("SystemOpenPromptFrame");
            _loc1_.show(TOTEM,this.gotoSystem);
            this._frameList[TOTEM] = _loc1_;
            SocketManager.Instance.out.syncWeakStep(Step.TOTEM_OPEN);
         }
         if(PlayerManager.Instance.Self.Grade >= 30 && !PlayerManager.Instance.Self.isNewOnceFinish(Step.GEMSTONE_OPEN) && PathManager.solveGemstoneSwitch)
         {
            if(!this._isLoadComplete)
            {
               this.loadModule();
               return;
            }
            _loc1_ = ComponentFactory.Instance.creatCustomObject("SystemOpenPromptFrame");
            _loc1_.show(GEMSTONE,this.gotoSystem);
            this._frameList[GEMSTONE] = _loc1_;
            SocketManager.Instance.out.syncWeakStep(Step.GEMSTONE_OPEN);
         }
         if(PathManager.treasureSwitch && !this._isJudge && PlayerManager.Instance.Self.Grade >= 25 && PlayerManager.Instance.Self.treasure + PlayerManager.Instance.Self.treasureAdd > 0 && !TreasureModel.instance.isEndTreasure)
         {
            if(!this._isLoadComplete)
            {
               this.loadModule();
               return;
            }
            _loc1_ = ComponentFactory.Instance.creatCustomObject("SystemOpenPromptFrame");
            _loc1_.show(TREASURE,this.gotoSystem);
            this._frameList[TREASURE] = _loc1_;
         }
         if(!this._isJudge && PlayerManager.Instance.Self.Grade >= 5 && (PlayerManager.Instance.Self.IsVIP && PlayerManager.Instance.Self.canTakeVipReward))
         {
            if(!this._isLoadComplete)
            {
               this.loadModule();
               return;
            }
            _loc1_ = ComponentFactory.Instance.creatCustomObject("SystemOpenPromptFrame");
            _loc1_.show(GET_AWARD,this.gotoSystem);
            this._frameList[GET_AWARD] = _loc1_;
         }
         if(!this._isJudge && PlayerManager.Instance.Self.Grade >= 5 && !PlayerManager.Instance.Self.Sign)
         {
            if(!this._isLoadComplete)
            {
               this.loadModule();
               return;
            }
            _loc1_ = ComponentFactory.Instance.creatCustomObject("SystemOpenPromptFrame");
            _loc1_.show(SIGN,this.gotoSystem);
            this._frameList[SIGN] = _loc1_;
         }
         if(ConsortionModelControl.Instance.isShowBossOpenTip)
         {
            if(!this._isLoadComplete)
            {
               this.loadModule();
               return;
            }
            _loc1_ = ComponentFactory.Instance.creatCustomObject("SystemOpenPromptFrame");
            _loc1_.show(CONSORTIA_BOSS_OPEN,this.gotoSystem);
            this._frameList[CONSORTIA_BOSS_OPEN] = _loc1_;
            ConsortionModelControl.Instance.isShowBossOpenTip = false;
         }
         this._isJudge = true;
      }
      
      public function gotoSystem(param1:int) : void
      {
         switch(param1)
         {
            case TOTEM:
               this.showTotem();
               break;
            case GEMSTONE:
               this.showGemstone();
               break;
            case GET_AWARD:
               this.showGetAward();
               break;
            case SIGN:
               this.showSign();
               break;
            case TREASURE:
               this.showTreasure();
               break;
            case CONSORTIA_BOSS_OPEN:
               this.showConsortiaBoss();
               break;
            case BATTLE_GROUND_OPEN:
               this.showBattleGround();
               break;
            case FARM_CROP_RIPE:
               this.showFarm();
               break;
            case SEVEN_DOUBLE_DUNGEON:
               this.goSevenDoubleDungeon();
         }
         this._frameList[param1] = null;
         delete this._frameList[param1];
      }
      
      private function goSevenDoubleDungeon() : void
      {
         StateManager.setState(StateType.DUNGEON_LIST);
      }
      
      private function showFarm() : void
      {
         FarmModelController.instance.goFarm(PlayerManager.Instance.Self.ID,PlayerManager.Instance.Self.NickName);
      }
      
      private function showBattleGround() : void
      {
         BattleGroudManager.Instance.initBattleView();
      }
      
      private function showConsortiaBoss() : void
      {
         StateManager.setState(StateType.CONSORTIA,ConsortionModelControl.Instance.openBossFrame);
      }
      
      private function showTreasure() : void
      {
         TreasureManager.instance.show();
      }
      
      private function showSign() : void
      {
         CalendarManager.getInstance().open(1);
      }
      
      private function showGetAward() : void
      {
         MainButtnController.instance.show(MainButtnController.DDT_AWARD);
      }
      
      private function showGemstone() : void
      {
         BagStore.instance.show(BagStore.FORGE_STORE,3);
      }
      
      private function showTotem() : void
      {
         BagAndInfoManager.Instance.showBagAndInfo(5);
      }
      
      public function dispose() : void
      {
         var _loc1_:SystemOpenPromptFrame = null;
         for each(_loc1_ in this._frameList)
         {
            _loc1_.dispose();
         }
         this._frameList = null;
      }
      
      public function get isLoadComplete() : Boolean
      {
         return this._isLoadComplete;
      }
   }
}
