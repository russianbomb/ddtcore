package room.transnational
{
   import com.pickgliss.action.BaseAction;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.alert.SimpleAlert;
   import ddt.constants.CacheConsts;
   import ddt.manager.LanguageMgr;
   
   public class SnapAction extends BaseAction
   {
       
      
      private var title:String;
      
      private var msg:String;
      
      private var submitLabel:String = "";
      
      private var cancelLabel:String = "";
      
      private var autoDispose:Boolean = false;
      
      private var enableHtml:Boolean = false;
      
      private var multiLine:Boolean = false;
      
      private var blockBackgound:int = 0;
      
      private var cacheFlag:String = null;
      
      private var frameStyle:String = "SimpleAlert";
      
      private var buttonGape:int = 30;
      
      private var autoButtonGape:Boolean = true;
      
      private var type:int = 0;
      
      private var __enterConsortiaConfirm:Function;
      
      public function SnapAction(param1:Function, param2:String, param3:String, param4:String = "", param5:String = "", param6:Boolean = false, param7:Boolean = false, param8:Boolean = false, param9:int = 0, param10:String = null, param11:String = "SimpleAlert", param12:int = 30, param13:Boolean = true, param14:int = 0)
      {
         this.__enterConsortiaConfirm = param1;
         this.title = param2;
         this.msg = param3;
         this.submitLabel = param4;
         this.cancelLabel = param5;
         this.autoDispose = param6;
         this.enableHtml = param7;
         this.multiLine = param8;
         this.blockBackgound = param9;
         this.cacheFlag = param10;
         this.frameStyle = param11;
         this.buttonGape = param12;
         this.autoButtonGape = param13;
         this.type = param14;
         super();
      }
      
      override public function act() : void
      {
         var _loc1_:SimpleAlert = null;
         _loc1_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("tank.manager.PlayerManager.request"),this.msg,LanguageMgr.GetTranslation("tank.manager.PlayerManager.sure"),LanguageMgr.GetTranslation("tank.manager.PlayerManager.refuse"),false,true,true,LayerManager.ALPHA_BLOCKGOUND,CacheConsts.ALERT_IN_FIGHT) as SimpleAlert;
         _loc1_.addEventListener(FrameEvent.RESPONSE,this.__enterConsortiaConfirm);
      }
   }
}
