package room.transnational
{
   public class TransnationalPackageType
   {
      
      public static const OPEN:int = 1;
      
      public static const OVER:int = 2;
      
      public static const UPDATE_PLAYER:int = 3;
      
      public static const UPDATE_VALUE_REP:int = 4;
      
      public static const UPDATE_PLAYER_DATA:int = 12;
      
      public static const UPDATE_VALUE:int = 7;
      
      public static const MOVE_EQUIPMENT:int = 11;
      
      public static const ENTER_ROOM:int = 5;
       
      
      public function TransnationalPackageType()
      {
         super();
      }
   }
}
