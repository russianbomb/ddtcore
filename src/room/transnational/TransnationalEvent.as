package room.transnational
{
   import flash.events.Event;
   
   public class TransnationalEvent extends Event
   {
      
      public static var SHOPENABLE:String = "shopenable";
       
      
      public var _shopenable:Boolean;
      
      public function TransnationalEvent(param1:String, param2:Boolean, param3:Boolean = false, param4:Boolean = false)
      {
         super(param1,param3,param4);
         this._shopenable = param2;
      }
   }
}
