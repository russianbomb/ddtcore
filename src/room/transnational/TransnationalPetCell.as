package room.transnational
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.ShowTipManager;
   import com.pickgliss.ui.image.Scale9CornerImage;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.display.BitmapLoaderProxy;
   import ddt.manager.PathManager;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.DisplayObject;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.geom.Rectangle;
   import pet.date.PetSkillTemplateInfo;
   import pet.date.PetTemplateInfo;
   import petsBag.controller.PetBagController;
   import petsBag.view.item.SkillItem;
   
   public class TransnationalPetCell extends SkillItem
   {
       
      
      private var _cellinfo:PetTemplateInfo;
      
      private var _shiner:Scale9CornerImage;
      
      private var _petIcon:DisplayObject;
      
      private var _skill:Array;
      
      private var cellBg:Bitmap;
      
      public function TransnationalPetCell(param1:PetSkillTemplateInfo = null)
      {
         super(param1,NaN);
         this.addEvent();
      }
      
      private function addEvent() : void
      {
         addEventListener(MouseEvent.MOUSE_OVER,this.__overhandler);
         addEventListener(MouseEvent.MOUSE_OUT,this.__outhandler);
      }
      
      private function __overhandler(param1:MouseEvent) : void
      {
         this.cellBg.visible = true;
      }
      
      private function __outhandler(param1:MouseEvent) : void
      {
         this.cellBg.visible = false;
      }
      
      override protected function initView() : void
      {
         tipDirctions = "5,2,7,1,6,4";
         tipGapV = tipGapH = 20;
         this.cellBg = ComponentFactory.Instance.creatBitmap("bagAndInfo.cell.bagCellOverShareBG");
         PositionUtils.setPos(this.cellBg,"Transnational.cellbackgroud.pos");
         this.cellBg.visible = false;
         addChild(this.cellBg);
         this.buttonMode = true;
      }
      
      public function set Cellinfo(param1:PetTemplateInfo) : void
      {
         if(this._petIcon)
         {
            ShowTipManager.Instance.removeTip(this);
            this._petIcon.removeEventListener(BitmapLoaderProxy.LOADING_FINISH,this.__fixPetIconPostion);
            this._petIcon.parent.removeChild(this._petIcon);
            this._petIcon = null;
         }
         this._cellinfo = param1;
         if(param1)
         {
            ShowTipManager.Instance.addTip(this);
            this._petIcon = new BitmapLoaderProxy(PathManager.solvePetIconUrl(PetBagController.instance().getPetPic(param1,TransnationalFightManager.TRANSNATIONAL_PETLEVEL)),new Rectangle(0,0,42,41),true);
            this._petIcon.addEventListener(BitmapLoaderProxy.LOADING_FINISH,this.__fixPetIconPostion);
            addChild(this._petIcon);
         }
      }
      
      public function get Cellinfo() : PetTemplateInfo
      {
         return this._cellinfo;
      }
      
      public function setSkill(param1:Array) : void
      {
         this._skill = param1;
      }
      
      private function __fixPetIconPostion(param1:Event) : void
      {
         if(this._petIcon)
         {
            this._petIcon.x = 64 - this._petIcon.width >> 1;
            this._petIcon.y = 59 - this._petIcon.height >> 1;
         }
      }
      
      public function set cellSize(param1:int) : void
      {
         var _loc2_:Number = NaN;
         PositionUtils.setPos(this._petIcon,"ddtshop.ItemCellStartPos");
         if(this._petIcon.height >= param1 && param1 >= this._petIcon.width || this._petIcon.height >= this._petIcon.width && this._petIcon.width >= param1 || param1 >= this._petIcon.height && this._petIcon.height >= this._petIcon.width)
         {
            _loc2_ = this._petIcon.height / param1;
         }
         else
         {
            _loc2_ = this._petIcon.width / param1;
         }
         this._petIcon.height = this._petIcon.height / _loc2_;
         this._petIcon.width = this._petIcon.width / _loc2_;
         this._petIcon.x = this._petIcon.x + (param1 - this._petIcon.width) / 2;
         this._petIcon.y = this._petIcon.y + (param1 - this._petIcon.height) / 2;
      }
      
      override public function get tipStyle() : String
      {
         return "room.transnational.TransnationalPetCellTips";
      }
      
      override public function get tipData() : Object
      {
         return this._skill;
      }
      
      override public function dispose() : void
      {
         super.dispose();
         if(this._petIcon.parent)
         {
            this._petIcon.parent.removeChild(this._petIcon);
            this._petIcon = null;
         }
         if(this.cellBg)
         {
            ObjectUtils.disposeObject(this.cellBg);
            this.cellBg = null;
         }
         if(this._skill)
         {
            ObjectUtils.disposeObject(this._skill);
            this._skill = null;
         }
         if(this._cellinfo)
         {
            ObjectUtils.disposeObject(this._cellinfo);
            this._cellinfo = null;
         }
      }
   }
}
