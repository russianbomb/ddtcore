package room.transnational
{
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.EquipType;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.GameInSocketOut;
   import ddt.manager.ItemManager;
   import ddt.manager.PetInfoManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import flash.events.EventDispatcher;
   import pet.date.PetSkill;
   import pet.date.PetTemplateInfo;
   import road7th.data.DictionaryData;
   import room.RoomManager;
   
   public class TransnationalFightManager extends EventDispatcher
   {
      
      private static var Tfight:TransnationalFightManager;
      
      public static var TRANSNATIONAL_WEAPON:int = 7;
      
      public static var TRANSNATIONAL_AUX:int = 17;
      
      public static var _weaponList:Vector.<ItemTemplateInfo>;
      
      public static var _auxList:Vector.<ItemTemplateInfo>;
      
      public static var _petList:Vector.<PetTemplateInfo>;
      
      public static var _petsSkill:DictionaryData;
      
      public static var TRANSNATIONAL_PETLEVEL:int = 30;
      
      public static var LEFTCELL_WEAPON:int = 0;
      
      public static var LEFTCELL_AUX:int = 1;
      
      public static var LEFTCELL_PET:int = 2;
      
      public static var TRANSNATIONAL_SECWEAPONLEVEL:int;
      
      public static var SCORESDAILETOTAL:int = 2000;
       
      
      private var TransnatinalRoomView:TransnationalFightRoomController;
      
      private var callback:Function;
      
      private var _isfromTransnational:Boolean = false;
      
      private var _actived:Boolean = false;
      
      private var _currentScores:int;
      
      private var _dailyScores:int;
      
      public function TransnationalFightManager()
      {
         super();
      }
      
      public static function get Instance() : TransnationalFightManager
      {
         if(Tfight == null)
         {
            Tfight = new TransnationalFightManager();
         }
         return Tfight;
      }
      
      public function set ShowIcon(param1:Function) : void
      {
         this.callback = param1;
      }
      
      public function Setup() : void
      {
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.TRANSNATIONALFIGHT_ACTIVED,this.__actived);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.TRANSNATIONALSCORE_UPDATA,this.__updataScores);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.TRANSNATIONALfIGHT_OVER,this.__overActivity);
      }
      
      private function __overActivity(param1:CrazyTankSocketEvent) : void
      {
         this._actived = false;
         this.callback(this._actived);
         this.hideTransnationalRoomView();
      }
      
      public function get isfromTransnational() : Boolean
      {
         return this._isfromTransnational;
      }
      
      public function set isfromTransnational(param1:Boolean) : void
      {
         this._isfromTransnational = param1;
      }
      
      private function __updataScores(param1:CrazyTankSocketEvent) : void
      {
         this.dailyScores = param1.pkg.readInt();
         this.currentScores = param1.pkg.readInt();
         if(this.TransnatinalRoomView)
         {
            this.TransnatinalRoomView.updataScores();
         }
      }
      
      private function __actived(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:Array = null;
         var _loc9_:int = 0;
         var _loc10_:InventoryItemInfo = null;
         var _loc11_:int = 0;
         var _loc12_:InventoryItemInfo = null;
         var _loc13_:int = 0;
         var _loc14_:int = 0;
         var _loc15_:PetTemplateInfo = null;
         var _loc16_:int = 0;
         var _loc17_:int = 0;
         var _loc18_:PetSkill = null;
         _weaponList = new Vector.<ItemTemplateInfo>();
         _auxList = new Vector.<ItemTemplateInfo>();
         _petList = new Vector.<PetTemplateInfo>();
         _petsSkill = new DictionaryData();
         this._actived = true;
         if(this.callback != null)
         {
            this.callback(this._actived);
         }
         TRANSNATIONAL_SECWEAPONLEVEL = param1.pkg.readInt();
         var _loc3_:int = param1.pkg.readInt();
         var _loc4_:int = 0;
         while(_loc4_ < _loc3_)
         {
            _loc9_ = param1.pkg.readInt();
            _loc10_ = new InventoryItemInfo();
            ObjectUtils.copyProperties(_loc10_,ItemManager.Instance.getTemplateById(_loc9_));
            _loc10_.StrengthenLevel = TransnationalFightManager.TRANSNATIONAL_SECWEAPONLEVEL;
            _loc10_.CanCompose = false;
            _loc10_.CanStrengthen = false;
            _loc10_.BindType = 4;
            _weaponList.push(_loc10_);
            _loc4_++;
         }
         var _loc5_:int = param1.pkg.readInt();
         var _loc6_:int = 0;
         while(_loc6_ < _loc5_)
         {
            _loc11_ = param1.pkg.readInt();
            _loc12_ = new InventoryItemInfo();
            ObjectUtils.copyProperties(_loc12_,ItemManager.Instance.getTemplateById(_loc11_));
            if(_loc11_ != EquipType.WishKingBlessing)
            {
               _loc12_.StrengthenLevel = TransnationalFightManager.TRANSNATIONAL_SECWEAPONLEVEL;
            }
            _loc12_.CanCompose = false;
            _loc12_.CanStrengthen = false;
            _loc12_.BindType = 4;
            _auxList.push(_loc12_);
            _loc6_++;
         }
         var _loc7_:int = param1.pkg.readInt();
         var _loc8_:int = 0;
         while(_loc8_ < _loc7_)
         {
            _loc13_ = param1.pkg.readInt();
            _loc14_ = param1.pkg.readInt();
            _loc15_ = PetInfoManager.getPetByTemplateID(_loc13_);
            _loc2_ = new Array();
            _petList.push(_loc15_);
            _loc16_ = 0;
            while(_loc16_ < _loc14_)
            {
               _loc17_ = param1.pkg.readInt();
               _loc18_ = new PetSkill(_loc17_);
               _loc2_.push(_loc18_);
               _loc16_++;
            }
            _petsSkill.add(_loc15_.TemplateID,_loc2_);
            _loc8_++;
         }
         TRANSNATIONAL_PETLEVEL = param1.pkg.readInt();
         SCORESDAILETOTAL = param1.pkg.readInt();
      }
      
      private function __equipment(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:ItemTemplateInfo = new ItemTemplateInfo();
      }
      
      public function hasActived() : Boolean
      {
         return this._actived;
      }
      
      public function get dailyScores() : int
      {
         return this._dailyScores;
      }
      
      public function set dailyScores(param1:int) : void
      {
         this._dailyScores = param1;
      }
      
      public function get currentScores() : int
      {
         return this._currentScores;
      }
      
      public function set currentScores(param1:int) : void
      {
         this._currentScores = param1;
      }
      
      public function enterTransnationalFight() : void
      {
         SoundManager.instance.play("008");
         GameInSocketOut.sendTransnationalBegin(RoomManager.TRANSNATIONAL_ROOM);
      }
      
      public function show(param1:String, param2:int, param3:int, param4:int, param5:int) : void
      {
         if(this.TransnatinalRoomView == null || this.TransnatinalRoomView.isdispose)
         {
            if(this.TransnatinalRoomView)
            {
               this.TransnatinalRoomView.dispose();
               this.TransnatinalRoomView = null;
            }
            this.TransnatinalRoomView = ComponentFactory.Instance.creat("room.transnational.TransnationalRoomView");
         }
         this.TransnatinalRoomView.playerStyle(param1,param2,param3,param4,param5);
         this.TransnatinalRoomView.updataScores();
         this.TransnatinalRoomView.show();
         this.TransnatinalRoomView.addEventListener(FrameEvent.RESPONSE,this.___onTransnationalRoomEvent);
      }
      
      public function hide() : void
      {
         if(this.TransnatinalRoomView.parent)
         {
            this.TransnatinalRoomView.cancel();
            this.TransnatinalRoomView.parent.removeChild(this.TransnatinalRoomView);
         }
      }
      
      private function ___onTransnationalRoomEvent(param1:FrameEvent) : void
      {
         if(param1.responseCode == FrameEvent.ESC_CLICK || param1.responseCode == FrameEvent.CANCEL_CLICK || param1.responseCode == FrameEvent.CLOSE_CLICK)
         {
            SoundManager.instance.playButtonSound();
            GameInSocketOut.revertPlayerInfo(TransnationalPackageType.UPDATE_PLAYER_DATA);
            this.hide();
         }
      }
      
      private function hideTransnationalRoomView() : void
      {
         if(this.TransnatinalRoomView)
         {
            this.TransnatinalRoomView.removeEventListener(FrameEvent.RESPONSE,this.___onTransnationalRoomEvent);
            this.TransnatinalRoomView.dispose();
            ObjectUtils.disposeObject(this.TransnatinalRoomView);
            this.TransnatinalRoomView = null;
         }
      }
   }
}
