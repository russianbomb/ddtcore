package room.transnational
{
   import bagAndInfo.cell.CellFactory;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.controls.TextButton;
   import com.pickgliss.ui.controls.container.HBox;
   import com.pickgliss.ui.image.Scale9CornerImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ClassUtils;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.manager.GameInSocketOut;
   import ddt.manager.LanguageMgr;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import flash.events.TimerEvent;
   import flash.utils.Timer;
   import shop.view.ShopItemCell;
   
   public class TransnationalRightView extends Sprite
   {
      
      private static var rightView:TransnationalRightView;
       
      
      private var _bg:Bitmap;
      
      private var _matchingTxt:Bitmap;
      
      private var _btnbg:Bitmap;
      
      private var _helpBtn:BaseButton;
      
      private var _startBtn:MovieClip;
      
      private var _cancelBtn:SimpleBitmapButton;
      
      private var _helpFrame:Frame;
      
      private var _helpBG:Scale9CornerImage;
      
      private var _okBtn:TextButton;
      
      private var _content:MovieClip;
      
      private var _timeTxt:FilterFrameText;
      
      private var _timer:Timer;
      
      private var _weaponsContainer:HBox;
      
      private var _auxsContainer:HBox;
      
      private var _petsContainer:HBox;
      
      private var _weaponsVec:Vector.<ShopItemCell>;
      
      private var _auxsVec:Vector.<ShopItemCell>;
      
      private var _petsVec:Vector.<TransnationalPetCell>;
      
      private var _smallmap:Bitmap;
      
      private var _smarker:Sprite;
      
      public function TransnationalRightView()
      {
         super();
         this.initView();
         this.initEvent();
      }
      
      public static function get Instance() : TransnationalRightView
      {
         if(rightView == null)
         {
            rightView = new TransnationalRightView();
         }
         return rightView;
      }
      
      private function initView() : void
      {
         this._bg = ComponentFactory.Instance.creatBitmap("asset.room.TransnationalrightView.BG");
         this._matchingTxt = ComponentFactory.Instance.creatBitmap("asset.room.Transnational.matchingTxt");
         this._matchingTxt.visible = false;
         this._btnbg = ComponentFactory.Instance.creatBitmap("asset.ddtroom.btnBg");
         PositionUtils.setPos(this._btnbg,"asset.ddtroom.transnationalstartbtnBg");
         this._smallmap = ComponentFactory.Instance.creatBitmap("asset.room.transnational.smalllmap");
         this._startBtn = ClassUtils.CreatInstance("asset.ddtroom.startMovie") as MovieClip;
         PositionUtils.setPos(this._startBtn,"asset.ddtroom.transnationalstartbtn");
         this._startBtn.buttonMode = true;
         this._cancelBtn = ComponentFactory.Instance.creatComponentByStylename("asset.room.Transnational.CancelButton");
         this._helpBtn = ComponentFactory.Instance.creatComponentByStylename("ddtTransnational.HelpButton");
         this._weaponsContainer = ComponentFactory.Instance.creatComponentByStylename("Transnationalequipment.weaponsContainer");
         this._auxsContainer = ComponentFactory.Instance.creatComponentByStylename("Transnationalequipment.auxsContainer");
         this._petsContainer = ComponentFactory.Instance.creatComponentByStylename("Transnationalequipment.petsContainer");
         this._weaponsVec = new Vector.<ShopItemCell>();
         this._auxsVec = new Vector.<ShopItemCell>();
         this._petsVec = new Vector.<TransnationalPetCell>();
         this._timeTxt = ComponentFactory.Instance.creatComponentByStylename("asset.room.Transnational.timeTxt");
         this._timeTxt.text = "00";
         this._timeTxt.visible = false;
         this._timer = new Timer(1000);
         this._timer.addEventListener(TimerEvent.TIMER,this.__timer);
         this._smarker = new Sprite();
         addChild(this._bg);
         addChild(this._smallmap);
         addChild(this._matchingTxt);
         addChild(this._btnbg);
         addChild(this._startBtn);
         addChild(this._helpBtn);
         addChild(this._cancelBtn);
         addChild(this._timeTxt);
         addChild(this._weaponsContainer);
         addChild(this._auxsContainer);
         addChild(this._petsContainer);
         this.updataCell();
      }
      
      private function updataCell() : void
      {
         var _loc1_:ShopItemCell = null;
         var _loc2_:ShopItemCell = null;
         var _loc3_:TransnationalPetCell = null;
         var _loc6_:Bitmap = null;
         var _loc7_:int = 0;
         var _loc4_:Number = 289;
         var _loc5_:Number = 123;
         _loc7_ = 0;
         while(_loc7_ < TransnationalFightManager._weaponList.length)
         {
            _loc1_ = this.creatItemCell();
            _loc1_.buttonMode = true;
            _loc1_.info = TransnationalFightManager._weaponList[_loc7_] as ItemTemplateInfo;
            _loc1_.cellSize = 42;
            _loc6_ = ComponentFactory.Instance.creatBitmap("bagAndInfo.cell.bagCellOverShareBG");
            PositionUtils.setPos(_loc6_,"Transnational.cellbackgroud.pos");
            _loc1_.overBg = _loc6_;
            this._weaponsVec.push(this._weaponsContainer.addChild(_loc1_));
            _loc7_++;
         }
         var _loc8_:int = 0;
         while(_loc8_ < TransnationalFightManager._auxList.length)
         {
            _loc2_ = this.creatItemCell();
            _loc2_.buttonMode = true;
            _loc2_.info = TransnationalFightManager._auxList[_loc8_] as ItemTemplateInfo;
            _loc6_ = ComponentFactory.Instance.creatBitmap("bagAndInfo.cell.bagCellOverShareBG");
            PositionUtils.setPos(_loc6_,"Transnational.cellbackgroud.pos");
            _loc2_.overBg = _loc6_;
            _loc2_.cellSize = 42;
            this._auxsVec.push(this._auxsContainer.addChild(_loc2_));
            _loc8_++;
         }
         var _loc9_:int = 0;
         while(_loc9_ < TransnationalFightManager._petList.length)
         {
            _loc3_ = new TransnationalPetCell();
            _loc3_.Cellinfo = TransnationalFightManager._petList[_loc9_];
            _loc3_.setSkill(TransnationalFightManager._petsSkill[_loc3_.Cellinfo.TemplateID]);
            _loc3_.cellSize = 42;
            this._petsVec.push(this._petsContainer.addChild(_loc3_));
            _loc9_++;
         }
      }
      
      private function __timer(param1:TimerEvent) : void
      {
         var _loc2_:uint = this._timer.currentCount % 60;
         this._timeTxt.text = _loc2_ > 9?_loc2_.toString():"0" + _loc2_;
      }
      
      private function initEvent() : void
      {
         var _loc1_:ShopItemCell = null;
         var _loc2_:ShopItemCell = null;
         var _loc3_:TransnationalPetCell = null;
         this._startBtn.addEventListener(MouseEvent.CLICK,this.__toStartMatch);
         this._helpBtn.addEventListener(MouseEvent.CLICK,this.__toShowHelp);
         this._cancelBtn.addEventListener(MouseEvent.CLICK,this.__cancelClick);
         for each(_loc1_ in this._weaponsVec)
         {
            _loc1_.addEventListener(MouseEvent.CLICK,this.__onClick);
         }
         for each(_loc2_ in this._auxsVec)
         {
            _loc2_.addEventListener(MouseEvent.CLICK,this.__onClick);
         }
         for each(_loc3_ in this._petsVec)
         {
            _loc3_.addEventListener(MouseEvent.CLICK,this.__onClick);
         }
      }
      
      private function __onClick(param1:MouseEvent) : void
      {
         var _loc2_:int = 0;
         var _loc3_:TransnationalPetCell = null;
         var _loc4_:int = 0;
         if(this._startBtn.visible)
         {
            SoundManager.instance.play("008");
            if(param1.currentTarget is ShopItemCell)
            {
               _loc2_ = (param1.currentTarget as ShopItemCell).info.TemplateID;
               GameInSocketOut.sendEquipmentTransnational(_loc2_);
            }
            else
            {
               _loc3_ = param1.currentTarget as TransnationalPetCell;
               _loc4_ = _loc3_.Cellinfo.TemplateID;
               GameInSocketOut.sendEquipmentTransnational(_loc4_);
            }
         }
      }
      
      private function removeEvent() : void
      {
         var _loc1_:ShopItemCell = null;
         var _loc2_:ShopItemCell = null;
         var _loc3_:TransnationalPetCell = null;
         this._startBtn.removeEventListener(MouseEvent.CLICK,this.__toStartMatch);
         this._helpBtn.removeEventListener(MouseEvent.CLICK,this.__toShowHelp);
         this._cancelBtn.removeEventListener(MouseEvent.CLICK,this.__cancelClick);
         this._timer.removeEventListener(TimerEvent.TIMER,this.__timer);
         for each(_loc1_ in this._weaponsVec)
         {
            _loc1_.removeEventListener(MouseEvent.CLICK,this.__onClick);
         }
         for each(_loc2_ in this._auxsVec)
         {
            _loc2_.removeEventListener(MouseEvent.CLICK,this.__onClick);
         }
         for each(_loc3_ in this._petsVec)
         {
            _loc3_.removeEventListener(MouseEvent.CLICK,this.__onClick);
         }
      }
      
      private function __toStartMatch(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         this._timeTxt.visible = this._matchingTxt.visible = this._cancelBtn.visible = this._cancelBtn.enable = true;
         this._smallmap.visible = this._startBtn.visible = this._startBtn.enabled = false;
         dispatchEvent(new TransnationalEvent(TransnationalEvent.SHOPENABLE,this._startBtn.visible));
         this._timer.start();
         this.startGame();
      }
      
      public function __cancelClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         this._timer.stop();
         this._timer.reset();
         this._timeTxt.text = "00";
         GameInSocketOut.sendCancelWait();
         this._timeTxt.visible = this._matchingTxt.visible = this._cancelBtn.visible = this._cancelBtn.enable = false;
         this._smallmap.visible = this._startBtn.visible = this._startBtn.enabled = true;
         dispatchEvent(new TransnationalEvent(TransnationalEvent.SHOPENABLE,this._startBtn.visible));
      }
      
      protected function startGame() : void
      {
         GameInSocketOut.sendSingleRoomBegin(TransnationalPackageType.ENTER_ROOM);
      }
      
      protected function creatItemCell() : ShopItemCell
      {
         var _loc1_:Sprite = new Sprite();
         _loc1_.graphics.beginFill(16777215,0);
         _loc1_.graphics.drawRect(8,8,42,42);
         _loc1_.graphics.endFill();
         return CellFactory.instance.createShopItemCell(_loc1_,null,true,true) as ShopItemCell;
      }
      
      private function __toShowHelp(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         if(this._helpFrame == null)
         {
            this._helpFrame = ComponentFactory.Instance.creatComponentByStylename("asset.Transnational.helpFrame");
            this._helpBG = ComponentFactory.Instance.creatComponentByStylename("asset.Transnational.help.BG");
            this._okBtn = ComponentFactory.Instance.creatComponentByStylename("asset.Transnational.helpFrame.OK");
            this._content = ComponentFactory.Instance.creat("asset.Transnational.helpConent");
            this._okBtn.text = LanguageMgr.GetTranslation("ok");
            this._helpFrame.titleText = LanguageMgr.GetTranslation("Transnational.help.Title");
            this._helpFrame.addToContent(this._okBtn);
            this._helpFrame.addToContent(this._helpBG);
            this._helpFrame.addToContent(this._content);
            this._okBtn.addEventListener(MouseEvent.CLICK,this.__closeHelpFrame);
            this._helpFrame.addEventListener(FrameEvent.RESPONSE,this.__helpFrameRespose);
         }
         LayerManager.Instance.addToLayer(this._helpFrame,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.ALPHA_BLOCKGOUND);
      }
      
      protected function __helpFrameRespose(param1:FrameEvent) : void
      {
         if(param1.responseCode == FrameEvent.CLOSE_CLICK || param1.responseCode == FrameEvent.ESC_CLICK)
         {
            SoundManager.instance.play("008");
            this._helpFrame.parent.removeChild(this._helpFrame);
         }
      }
      
      private function __closeHelpFrame(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         this._helpFrame.parent.removeChild(this._helpFrame);
      }
      
      public function dispose() : void
      {
         var _loc1_:ShopItemCell = null;
         var _loc2_:ShopItemCell = null;
         var _loc3_:TransnationalPetCell = null;
         this.removeEvent();
         ObjectUtils.disposeAllChildren(this);
         ObjectUtils.disposeObject(this._bg);
         this._bg = null;
         ObjectUtils.disposeObject(this._btnbg);
         this._btnbg = null;
         ObjectUtils.disposeObject(this._startBtn);
         this._startBtn = null;
         ObjectUtils.disposeObject(this._cancelBtn);
         this._cancelBtn = null;
         ObjectUtils.disposeObject(this._helpBtn);
         this._helpBtn = null;
         ObjectUtils.disposeObject(this._timeTxt);
         this._timeTxt = null;
         ObjectUtils.disposeObject(this._timeTxt);
         this._timer = null;
         ObjectUtils.disposeObject(this._smallmap);
         this._smallmap = null;
         if(this._helpFrame)
         {
            ObjectUtils.disposeObject(this._helpFrame);
            this._helpFrame = null;
         }
         for each(_loc1_ in this._weaponsVec)
         {
            _loc1_.dispose();
            ObjectUtils.disposeObject(_loc1_);
            _loc1_ = null;
         }
         for each(_loc2_ in this._auxsVec)
         {
            _loc2_.dispose();
            ObjectUtils.disposeObject(_loc1_);
            _loc2_ = null;
         }
         for each(_loc3_ in this._petsVec)
         {
            _loc3_.dispose();
            ObjectUtils.disposeObject(_loc3_);
            _loc3_ = null;
         }
         ObjectUtils.disposeAllChildren(this._weaponsContainer);
         this._weaponsContainer = null;
         ObjectUtils.disposeObject(this._weaponsVec);
         this._weaponsVec = null;
         ObjectUtils.disposeObject(this._auxsVec);
         this._auxsVec = null;
         ObjectUtils.disposeObject(this._petsVec);
         this._petsVec = null;
      }
   }
}
