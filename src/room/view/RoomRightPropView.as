package room.view
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.container.HBox;
   import com.pickgliss.ui.controls.container.SimpleTileList;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.image.MutipleImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ClassUtils;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.command.StripTip;
   import ddt.data.BagInfo;
   import ddt.data.PropInfo;
   import ddt.data.ShopType;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.data.goods.ShopItemInfo;
   import ddt.data.player.PlayerInfo;
   import ddt.events.BagEvent;
   import ddt.events.PlayerPropertyEvent;
   import ddt.events.RoomEvent;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerManager;
   import ddt.manager.ShopManager;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import room.RoomManager;
   import room.model.RoomInfo;
   import trainer.data.Step;
   
   public class RoomRightPropView extends Sprite implements Disposeable
   {
      
      public static const UPCELLS_NUMBER:int = 3;
       
      
      protected var _bg:MovieClip;
      
      protected var _secBg:MutipleImage;
      
      protected var _upCells:Array;
      
      protected var _upCellsContainer:HBox;
      
      protected var _downCellsContainer:SimpleTileList;
      
      protected var _roomIdTxt:FilterFrameText;
      
      protected var _chanelNameTxt:FilterFrameText;
      
      protected var _goldInfoTxt:FilterFrameText;
      
      protected var _roomNameTxt:FilterFrameText;
      
      protected var _upCellsStripTip:StripTip;
      
      protected var _downCellsStripTip:StripTip;
      
      public function RoomRightPropView()
      {
         super();
         this.initView();
         this.initEvents();
      }
      
      protected function initView() : void
      {
         var _loc4_:InventoryItemInfo = null;
         var _loc5_:RoomInfo = null;
         var _loc6_:RoomPropCell = null;
         var _loc7_:PropInfo = null;
         var _loc8_:RoomPropCell = null;
         this._bg = ClassUtils.CreatInstance("asset.background.room.left") as MovieClip;
         addChild(this._bg);
         this._secBg = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroom.roomRightPropView.secbg");
         addChild(this._secBg);
         this._upCellsContainer = ComponentFactory.Instance.creatCustomObject("asset.ddtroom.upCellsContainer");
         addChild(this._upCellsContainer);
         this._downCellsContainer = ComponentFactory.Instance.creatCustomObject("asset.ddtroom.downCellsContainer",[4]);
         addChild(this._downCellsContainer);
         this._upCells = [];
         var _loc1_:int = 0;
         while(_loc1_ < UPCELLS_NUMBER)
         {
            _loc6_ = new RoomPropCell(true,_loc1_);
            this._upCellsContainer.addChild(_loc6_);
            this._upCells.push(_loc6_);
            _loc1_++;
         }
         var _loc2_:Vector.<ShopItemInfo> = ShopManager.Instance.getGoodsByType(ShopType.ROOM_PROP);
         var _loc3_:int = 0;
         while(_loc3_ < this.getPropNum())
         {
            _loc7_ = new PropInfo(_loc2_[_loc3_].TemplateInfo);
            _loc8_ = new RoomPropCell(false,_loc1_);
            this._downCellsContainer.addChild(_loc8_);
            _loc8_.info = _loc7_;
            _loc3_++;
         }
         this._chanelNameTxt = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroom.ChanelNameText");
         addChild(this._chanelNameTxt);
         this._roomNameTxt = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroom.roomNameText");
         addChild(this._roomNameTxt);
         for each(_loc4_ in PlayerManager.Instance.Self.getBag(BagInfo.FIGHTBAG).items)
         {
            this._upCells[_loc4_.Place].info = new PropInfo(PlayerManager.Instance.Self.getBag(BagInfo.FIGHTBAG).items[_loc4_.Place]);
         }
         _loc5_ = RoomManager.Instance.current;
         if(_loc5_)
         {
            this._roomIdTxt = ComponentFactory.Instance.creatComponentByStylename("room.roomID.text");
            this._roomIdTxt.text = RoomManager.Instance.current.ID.toString();
            addChild(this._roomIdTxt);
            this._roomNameTxt.text = RoomManager.Instance.current.Name;
         }
         this._chanelNameTxt.text = ServerManager.Instance.current.Name;
         this.creatTipShapeTip();
      }
      
      protected function getPropNum() : int
      {
         if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.FROZE_PROP_OPEN))
         {
            return 8;
         }
         if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.HIDE_PROP_OPEN))
         {
            return 7;
         }
         if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.POWER_PROP_OPEN))
         {
            return 4;
         }
         if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.GUILD_PROP_OPEN))
         {
            return 3;
         }
         if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.HP_PROP_OPEN))
         {
            return 2;
         }
         return 0;
      }
      
      protected function initEvents() : void
      {
         PlayerManager.Instance.Self.addEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__updateGold);
         PlayerManager.Instance.Self.getBag(BagInfo.FIGHTBAG).addEventListener(BagEvent.UPDATE,this.__updateBag);
         if(RoomManager.Instance.current)
         {
            RoomManager.Instance.current.addEventListener(RoomEvent.ROOM_NAME_CHANGED,this._roomNameChanged);
         }
      }
      
      protected function removeEvents() : void
      {
         PlayerManager.Instance.Self.removeEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__updateGold);
         PlayerManager.Instance.Self.getBag(BagInfo.FIGHTBAG).removeEventListener(BagEvent.UPDATE,this.__updateBag);
         if(RoomManager.Instance.current)
         {
            RoomManager.Instance.current.removeEventListener(RoomEvent.ROOM_NAME_CHANGED,this._roomNameChanged);
         }
      }
      
      protected function _roomNameChanged(param1:RoomEvent) : void
      {
         this._roomNameTxt.text = RoomManager.Instance.current.Name;
      }
      
      protected function creatTipShapeTip() : void
      {
         this._upCellsStripTip = ComponentFactory.Instance.creatCustomObject("asset.ddtroom.upRightPropTip");
         this._downCellsStripTip = ComponentFactory.Instance.creatCustomObject("asset.ddtroom.downRightPropTip");
         this._upCellsStripTip.tipData = LanguageMgr.GetTranslation("room.roomRightPropView.uppropTip");
         this._downCellsStripTip.tipData = LanguageMgr.GetTranslation("room.roomRightPropView.downpropTip");
         addChild(this._upCellsStripTip);
         addChild(this._downCellsStripTip);
      }
      
      protected function __updateGold(param1:PlayerPropertyEvent) : void
      {
         if(param1.changedProperties[PlayerInfo.GOLD])
         {
         }
      }
      
      protected function __updateBag(param1:BagEvent) : void
      {
         var _loc2_:InventoryItemInfo = null;
         for each(_loc2_ in param1.changedSlots)
         {
            if(PlayerManager.Instance.Self.getBag(BagInfo.FIGHTBAG).items[_loc2_.Place] == null)
            {
               this._upCells[_loc2_.Place].info = null;
               break;
            }
            this._upCells[_loc2_.Place].info = new PropInfo(PlayerManager.Instance.Self.getBag(BagInfo.FIGHTBAG).items[_loc2_.Place]);
         }
      }
      
      protected function _cellClick(param1:MouseEvent) : void
      {
         var _loc3_:RoomPropCell = null;
         var _loc2_:int = 0;
         for each(_loc3_ in this._upCells)
         {
            if(_loc3_.info != null)
            {
               _loc2_++;
            }
         }
         if(_loc2_ > UPCELLS_NUMBER)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("room.roomRightPropView.bagFull"));
         }
      }
      
      public function dispose() : void
      {
         this.removeEvents();
         ObjectUtils.disposeObject(this._bg);
         this._bg = null;
         if(this._secBg)
         {
            ObjectUtils.disposeObject(this._secBg);
         }
         this._secBg = null;
         if(this._roomIdTxt)
         {
            this._roomIdTxt.dispose();
            this._roomIdTxt = null;
         }
         this._upCells = null;
         this._upCellsContainer.dispose();
         this._upCellsContainer = null;
         this._downCellsContainer.dispose();
         this._downCellsContainer = null;
         this._chanelNameTxt.dispose();
         this._chanelNameTxt = null;
         this._roomNameTxt.dispose();
         this._roomNameTxt = null;
         if(this._upCellsStripTip)
         {
            ObjectUtils.disposeObject(this._upCellsStripTip);
         }
         this._upCellsStripTip = null;
         if(this._downCellsStripTip)
         {
            ObjectUtils.disposeObject(this._downCellsStripTip);
         }
         this._downCellsStripTip = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
