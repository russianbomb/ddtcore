package room.view
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.utils.ClassUtils;
   import ddt.data.BagInfo;
   import ddt.data.PropInfo;
   import ddt.data.ShopType;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.data.goods.ShopItemInfo;
   import ddt.events.BagEvent;
   import ddt.events.PlayerPropertyEvent;
   import ddt.events.RoomEvent;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerManager;
   import ddt.manager.ShopManager;
   import flash.display.MovieClip;
   
   public class SingleRoomRightPropView extends RoomRightPropView
   {
       
      
      public function SingleRoomRightPropView()
      {
         super();
      }
      
      override protected function initView() : void
      {
         var _loc4_:InventoryItemInfo = null;
         var _loc5_:RoomPropCell = null;
         var _loc6_:PropInfo = null;
         var _loc7_:RoomPropCell = null;
         _bg = ClassUtils.CreatInstance("asset.background.room.left") as MovieClip;
         addChild(_bg);
         _secBg = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroom.roomRightPropView.secbg");
         addChild(_secBg);
         _upCellsContainer = ComponentFactory.Instance.creatCustomObject("asset.ddtroom.upCellsContainer");
         addChild(_upCellsContainer);
         _downCellsContainer = ComponentFactory.Instance.creatCustomObject("asset.ddtroom.downCellsContainer",[4]);
         addChild(_downCellsContainer);
         _upCells = [];
         var _loc1_:int = 0;
         while(_loc1_ < UPCELLS_NUMBER)
         {
            _loc5_ = new RoomPropCell(true,_loc1_);
            _upCellsContainer.addChild(_loc5_);
            _upCells.push(_loc5_);
            _loc1_++;
         }
         var _loc2_:Vector.<ShopItemInfo> = ShopManager.Instance.getGoodsByType(ShopType.ROOM_PROP);
         var _loc3_:int = 0;
         while(_loc3_ < getPropNum())
         {
            _loc6_ = new PropInfo(_loc2_[_loc3_].TemplateInfo);
            _loc7_ = new RoomPropCell(false,_loc1_);
            _downCellsContainer.addChild(_loc7_);
            _loc7_.info = _loc6_;
            _loc3_++;
         }
         _chanelNameTxt = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroom.ChanelNameText");
         addChild(_chanelNameTxt);
         _roomNameTxt = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroom.roomNameText");
         addChild(_roomNameTxt);
         for each(_loc4_ in PlayerManager.Instance.Self.getBag(BagInfo.FIGHTBAG).items)
         {
            _upCells[_loc4_.Place].info = new PropInfo(PlayerManager.Instance.Self.getBag(BagInfo.FIGHTBAG).items[_loc4_.Place]);
         }
         _chanelNameTxt.text = ServerManager.Instance.current.Name;
         creatTipShapeTip();
         _bg.parent.removeChild(_bg);
         _secBg.parent.removeChild(_secBg);
         _roomNameTxt.parent.removeChild(_roomNameTxt);
         _chanelNameTxt.parent.removeChild(_chanelNameTxt);
      }
      
      override protected function initEvents() : void
      {
         PlayerManager.Instance.Self.addEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,__updateGold);
         PlayerManager.Instance.Self.getBag(BagInfo.FIGHTBAG).addEventListener(BagEvent.UPDATE,__updateBag);
      }
      
      override protected function removeEvents() : void
      {
         PlayerManager.Instance.Self.removeEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,__updateGold);
         PlayerManager.Instance.Self.getBag(BagInfo.FIGHTBAG).removeEventListener(BagEvent.UPDATE,__updateBag);
      }
      
      override protected function _roomNameChanged(param1:RoomEvent) : void
      {
      }
   }
}
