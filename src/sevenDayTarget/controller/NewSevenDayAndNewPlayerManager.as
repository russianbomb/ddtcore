package sevenDayTarget.controller
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.ComponentSetting;
   import ddt.bagStore.BagStore;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.manager.StateManager;
   import ddt.manager.TaskManager;
   import ddt.states.StateType;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.events.MouseEvent;
   import hallIcon.HallIconManager;
   import hallIcon.HallIconType;
   import sevenDayTarget.model.NewTargetQuestionInfo;
   import sevenDayTarget.view.NewSevenDayAndNewPlayerMainView;
   
   public class NewSevenDayAndNewPlayerManager extends EventDispatcher
   {
      
      private static var _instance:NewSevenDayAndNewPlayerManager;
       
      
      private var _isShowIcon:Boolean;
      
      private var _sevenDayOpen:Boolean;
      
      private var _newPlayerOpen:Boolean;
      
      public var sevenDayMainViewPreOk:Boolean;
      
      public var newPlayerMainViewPreOk:Boolean;
      
      private var _newSevenDayAndNewPlayerMainView:NewSevenDayAndNewPlayerMainView;
      
      public function NewSevenDayAndNewPlayerManager()
      {
         super();
      }
      
      public static function get Instance() : NewSevenDayAndNewPlayerManager
      {
         if(_instance == null)
         {
            _instance = new NewSevenDayAndNewPlayerManager();
         }
         return _instance;
      }
      
      public function get isShowIcon() : Boolean
      {
         var _loc1_:Boolean = false;
         if(!this.sevenDayOpen && !this.newPlayerOpen)
         {
            this._isShowIcon = false;
         }
         else
         {
            this._isShowIcon = true;
         }
         return this._isShowIcon;
      }
      
      public function get sevenDayOpen() : Boolean
      {
         var _loc1_:Boolean = SevenDayTargetManager.Instance.isShowIcon;
         return _loc1_;
      }
      
      public function set sevenDayOpen(param1:Boolean) : void
      {
         this._sevenDayOpen = param1;
      }
      
      public function get newPlayerOpen() : Boolean
      {
         var _loc1_:Boolean = NewPlayerRewardManager.Instance.isShowIcon;
         return _loc1_;
      }
      
      public function set newPlayerOpen(param1:Boolean) : void
      {
         this._newPlayerOpen = param1;
      }
      
      public function setup() : void
      {
         SevenDayTargetManager.Instance.setup();
         NewPlayerRewardManager.Instance.setup();
         addEventListener("openUpdate",this._aciveOtherManager);
         addEventListener("openSevenDayMainView",this._openMainView);
         addEventListener("clickLink",this.__clickLink);
      }
      
      private function _aciveOtherManager(param1:Event) : void
      {
         if(this.isShowIcon)
         {
            this.addEnterIcon();
         }
         else
         {
            this.disposeEnterIcon();
         }
      }
      
      public function addEnterIcon() : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.SEVENDAYTARGET,true);
      }
      
      private function disposeEnterIcon() : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.SEVENDAYTARGET,false);
      }
      
      public function onClickSevenDayTargetIcon(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         if(this.sevenDayOpen)
         {
            SevenDayTargetManager.Instance.onClickSevenDayTargetIcon();
         }
         if(this.newPlayerOpen)
         {
            SocketManager.Instance.out.newPlayerReward_enter();
         }
      }
      
      private function _openMainView(param1:Event) : void
      {
         if(!this._newSevenDayAndNewPlayerMainView)
         {
            if(this.sevenDayOpen && this.newPlayerOpen)
            {
               if(this.sevenDayMainViewPreOk && this.newPlayerMainViewPreOk)
               {
                  this._newSevenDayAndNewPlayerMainView = ComponentFactory.Instance.creatComponentByStylename("newSevenDayTarget.newSevenDayTargetFrame");
                  this._newSevenDayAndNewPlayerMainView.show();
               }
            }
            else if(this.sevenDayOpen && !this.newPlayerOpen)
            {
               if(this.sevenDayMainViewPreOk)
               {
                  this._newSevenDayAndNewPlayerMainView = ComponentFactory.Instance.creatComponentByStylename("newSevenDayTarget.newSevenDayTargetFrame");
                  this._newSevenDayAndNewPlayerMainView.show();
               }
            }
            else if(!this.sevenDayOpen && this.newPlayerOpen)
            {
               if(this.newPlayerMainViewPreOk)
               {
                  this._newSevenDayAndNewPlayerMainView = ComponentFactory.Instance.creatComponentByStylename("newSevenDayTarget.newSevenDayTargetFrame");
                  this._newSevenDayAndNewPlayerMainView.show();
               }
            }
         }
      }
      
      public function hideMainView() : void
      {
         this._newSevenDayAndNewPlayerMainView.dispose();
         this._newSevenDayAndNewPlayerMainView = null;
      }
      
      private function __clickLink(param1:Event) : void
      {
         var _loc2_:NewTargetQuestionInfo = this._newSevenDayAndNewPlayerMainView.todayInfo();
         var _loc3_:int = _loc2_.linkId;
         switch(_loc3_)
         {
            case 1:
               this.hideMainView();
               TaskManager.instance.switchVisible();
               break;
            case 2:
               this.hideMainView();
               StateManager.setState(StateType.SHOP);
               ComponentSetting.SEND_USELOG_ID(1);
               break;
            case 3:
               this.hideMainView();
               BagStore.instance.show(BagStore.BAG_STORE);
               ComponentSetting.SEND_USELOG_ID(2);
               break;
            case 4:
               this.hideMainView();
               StateManager.setState(StateType.CONSORTIA);
               ComponentSetting.SEND_USELOG_ID(5);
               break;
            case 5:
               this.hideMainView();
               StateManager.setState(StateType.DUNGEON_LIST);
               ComponentSetting.SEND_USELOG_ID(4);
         }
      }
   }
}
