package sevenDayTarget.dataAnalyzer
{
   import com.pickgliss.loader.DataAnalyzer;
   import sevenDayTarget.model.NewTargetQuestionInfo;
   
   public class SevenDayTargetDataAnalyzer extends DataAnalyzer
   {
       
      
      public var dataList:Array;
      
      public function SevenDayTargetDataAnalyzer(param1:Function)
      {
         super(param1);
      }
      
      override public function analyze(param1:*) : void
      {
         var _loc3_:XMLList = null;
         var _loc4_:int = 0;
         var _loc5_:NewTargetQuestionInfo = null;
         var _loc6_:XMLList = null;
         var _loc7_:int = 0;
         var _loc2_:XML = new XML(param1);
         this.dataList = new Array();
         if(_loc2_.@value == "true")
         {
            _loc3_ = _loc2_..Item;
            _loc4_ = 0;
            while(_loc4_ < _loc3_.length())
            {
               _loc5_ = new NewTargetQuestionInfo();
               _loc5_.questId = _loc3_[_loc4_].@ID;
               _loc5_.Period = _loc3_[_loc4_].@Period;
               _loc6_ = _loc3_[_loc4_]..Item_Condiction;
               _loc7_ = 0;
               while(_loc7_ < _loc6_.length())
               {
                  _loc5_.linkId = _loc6_[_loc7_].@IndexType;
                  if(_loc6_[_loc7_].@CondictionID == "1")
                  {
                     _loc5_.condition1Title = _loc6_[_loc7_].@CondictionTitle;
                     _loc5_.condition1Para = _loc6_[_loc7_].@Para2;
                  }
                  else if(_loc6_[_loc7_].@CondictionID == "2")
                  {
                     _loc5_.condition2Title = _loc6_[_loc7_].@CondictionTitle;
                     _loc5_.condition2Para = _loc6_[_loc7_].@Para2;
                  }
                  else if(_loc6_[_loc7_].@CondictionID == "3")
                  {
                     _loc5_.condition3Title = _loc6_[_loc7_].@CondictionTitle;
                     _loc5_.condition3Para = _loc6_[_loc7_].@Para2;
                  }
                  _loc7_++;
               }
               this.dataList.push(_loc5_);
               _loc4_++;
            }
            onAnalyzeComplete();
         }
      }
   }
}
