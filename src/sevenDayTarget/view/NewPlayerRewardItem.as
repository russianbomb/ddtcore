package sevenDayTarget.view
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.container.SimpleTileList;
   import com.pickgliss.ui.text.FilterFrameText;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.manager.ItemManager;
   import ddt.manager.LanguageMgr;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import sevenDayTarget.model.NewPlayerRewardInfo;
   
   public class NewPlayerRewardItem extends Sprite
   {
       
      
      private var _bg:Bitmap;
      
      private var _titleText:FilterFrameText;
      
      private var _getRewardBnt:BaseButton;
      
      private var _rewardList:SimpleTileList;
      
      private var _info:NewPlayerRewardInfo;
      
      public function NewPlayerRewardItem()
      {
         super();
      }
      
      public function setInfo(param1:NewPlayerRewardInfo) : void
      {
         var _loc8_:NewPlayerRewardCell = null;
         var _loc9_:InventoryItemInfo = null;
         var _loc2_:Array = param1.rewardArr;
         var _loc3_:int = param1.bgType;
         var _loc4_:int = _loc2_.length;
         var _loc5_:Boolean = param1.finished;
         var _loc6_:Boolean = param1.getRewarded;
         this._info = param1;
         if(!_loc5_)
         {
            this._getRewardBnt = ComponentFactory.Instance.creatComponentByStylename("newSevenDayAndNewPlayer.newplayerRewardItemRewardBnt");
            this._getRewardBnt.enable = false;
         }
         else if(_loc5_ && !_loc6_)
         {
            this._getRewardBnt = ComponentFactory.Instance.creatComponentByStylename("newSevenDayAndNewPlayer.newplayerRewardItemRewardBnt");
            this._getRewardBnt.enable = true;
         }
         else if(_loc6_)
         {
            this._getRewardBnt = ComponentFactory.Instance.creatComponentByStylename("newSevenDayAndNewPlayer.newplayerRewardItemRewardBnt2");
            this._getRewardBnt.enable = false;
         }
         this._getRewardBnt.addEventListener(MouseEvent.CLICK,this.__getReward);
         if(_loc4_ <= 4 && _loc4_ > 0 && _loc3_ == NewPlayerRewardMainView.CHONGZHI)
         {
            this._bg = ComponentFactory.Instance.creat("newSevenDayAndNewPlayer.chongzhi");
            this._titleText = ComponentFactory.Instance.creatComponentByStylename("newSevenDayAndNewPlayer.titletext1");
            this._titleText.text = LanguageMgr.GetTranslation("newSevenDayAndNewPlayer.chongzhitext",param1.num);
         }
         else if(_loc4_ > 4 && _loc4_ <= 8 && _loc3_ == NewPlayerRewardMainView.CHONGZHI)
         {
            this._bg = ComponentFactory.Instance.creat("newSevenDayAndNewPlayer.chongzhibig");
            this._titleText = ComponentFactory.Instance.creatComponentByStylename("newSevenDayAndNewPlayer.titletext1");
            this._titleText.text = LanguageMgr.GetTranslation("newSevenDayAndNewPlayer.chongzhitext",param1.num);
         }
         else if(_loc4_ <= 4 && _loc4_ > 0 && _loc3_ == NewPlayerRewardMainView.XIAOFEI)
         {
            this._bg = ComponentFactory.Instance.creat("newSevenDayAndNewPlayer.xiaofei");
            this._titleText = ComponentFactory.Instance.creatComponentByStylename("newSevenDayAndNewPlayer.titletext2");
            this._titleText.text = LanguageMgr.GetTranslation("newSevenDayAndNewPlayer.xiaofeitext",param1.num);
         }
         else if(_loc4_ > 4 && _loc4_ <= 8 && _loc3_ == NewPlayerRewardMainView.XIAOFEI)
         {
            this._bg = ComponentFactory.Instance.creat("newSevenDayAndNewPlayer.xiaofeiibig");
            this._titleText = ComponentFactory.Instance.creatComponentByStylename("newSevenDayAndNewPlayer.titletext2");
            this._titleText.text = LanguageMgr.GetTranslation("newSevenDayAndNewPlayer.xiaofeitext",param1.num);
         }
         else
         {
            this._titleText = ComponentFactory.Instance.creatComponentByStylename("newSevenDayAndNewPlayer.titletext3");
            this._titleText.text = LanguageMgr.GetTranslation("newSevenDayAndNewPlayer.hunlitext");
         }
         PositionUtils.setPos(this._titleText,"newSevenDayAndNewPlayer.newplayerRewardItemTitlePos");
         if(_loc4_ <= 4 && _loc4_ > 0)
         {
            PositionUtils.setPos(this._getRewardBnt,"newSevenDayAndNewPlayer.newplayerRewardItemRewardBntPos");
         }
         else
         {
            PositionUtils.setPos(this._getRewardBnt,"newSevenDayAndNewPlayer.newplayerRewardItemRewardBntPos2");
         }
         this._rewardList = ComponentFactory.Instance.creat("newPlayerReward.simpleTileList.rewardList",[4]);
         if(this._bg)
         {
            addChild(this._bg);
         }
         addChild(this._titleText);
         addChild(this._getRewardBnt);
         addChild(this._rewardList);
         var _loc7_:int = 0;
         while(_loc7_ < _loc2_.length)
         {
            _loc8_ = new NewPlayerRewardCell();
            _loc9_ = _loc2_[_loc7_] as InventoryItemInfo;
            _loc8_.info = ItemManager.Instance.getTemplateById(_loc9_.ItemID);
            _loc8_.itemNum = _loc9_.Count + "";
            this._rewardList.addChild(_loc8_);
            _loc7_++;
         }
      }
      
      private function __getReward(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         if(this._getRewardBnt)
         {
            this._getRewardBnt.backStyle = "newSevenDayAndNewPlayer.getRewardBG1";
            this._getRewardBnt.enable = false;
         }
         var _loc2_:int = this._info.questId;
         SocketManager.Instance.out.newPlayerReward_getReward(_loc2_);
      }
      
      private function initView(param1:int) : void
      {
      }
   }
}
