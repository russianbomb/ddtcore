package quest
{
   import baglocked.BaglockedManager;
   import beadSystem.controls.BeadLeadManager;
   import beadSystem.data.BeadLeadEvent;
   import com.pickgliss.effect.AlphaShinerAnimation;
   import com.pickgliss.effect.EffectColorType;
   import com.pickgliss.effect.EffectManager;
   import com.pickgliss.effect.EffectTypes;
   import com.pickgliss.effect.IEffect;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.ScrollPanel;
   import com.pickgliss.ui.controls.TextButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.controls.container.VBox;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.command.QuickBuyFrame;
   import ddt.data.fightLib.FightLibInfo;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.data.goods.ShopItemInfo;
   import ddt.data.quest.QuestInfo;
   import ddt.data.quest.QuestItemReward;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.events.TaskEvent;
   import ddt.manager.AcademyManager;
   import ddt.manager.FightLibManager;
   import ddt.manager.GameInSocketOut;
   import ddt.manager.ItemManager;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PathManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerConfigManager;
   import ddt.manager.ShopManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.manager.StateManager;
   import ddt.manager.TaskManager;
   import ddt.states.StateType;
   import ddt.utils.PositionUtils;
   import ddt.view.MainToolBar;
   import fightLib.LessonType;
   import flash.display.MovieClip;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.events.TimerEvent;
   import flash.net.URLRequest;
   import flash.net.navigateToURL;
   import flash.utils.Timer;
   import flash.utils.getTimer;
   import flash.utils.setTimeout;
   import petsBag.controller.PetBagController;
   import petsBag.data.PetFarmGuildeTaskType;
   import petsBag.event.UpdatePetFarmGuildeEvent;
   import room.RoomManager;
   import roomList.pvpRoomList.RoomListBGView;
   import trainer.controller.NewHandGuideManager;
   import trainer.controller.WeakGuildManager;
   import trainer.data.ArrowType;
   import trainer.data.Step;
   import trainer.view.NewHandContainer;
   import tryonSystem.TryonSystemController;
   
   public class TaskMainFrame extends Frame
   {
      
      public static const NORMAL:int = 1;
      
      public static const GUIDE:int = 2;
      
      public static const REWARD_UDERLINE:int = 417;
      
      public static const TASK_FRAME_HIDE:String = "taskFrameHide";
      
      private static const SPINEL:int = 11555;
      
      private static const TYPE_NUMBER:int = 6;
       
      
      private const CATEVIEW_X:int = 0;
      
      private const CATEVIEW_Y:int = 0;
      
      private const CATEVIEW_H:int = 50;
      
      private var cateViewArr:Array;
      
      private var infoView:QuestInfoPanelView;
      
      private var _questBtn:BaseButton;
      
      private var _goDungeonBtnShine:IEffect;
      
      private var _downClientShine:IEffect;
      
      private var _questBtnShine:IEffect;
      
      private var _buySpinelBtn:TextButton;
      
      private var _opened:Boolean = false;
      
      private var _currentCateView:QuestCateView;
      
      public var currentNewCateView:QuestCateView;
      
      private var leftPanel:ScrollPanel;
      
      private var leftPanelContent:VBox;
      
      private var _trusteeshipView:TrusteeshipView;
      
      private var _leftBGStyleNormal:MovieClip;
      
      private var _rightBGStyleNormal:MovieClip;
      
      private var _rightBottomBg:ScaleBitmapImage;
      
      private var _goDungeonBtn:BaseButton;
      
      private var _downloadClientBtn:TextButton;
      
      private var _gotoAcademy:BaseButton;
      
      private var _gotoGameBtn:BaseButton;
      
      private var _gotoTrainBtn:BaseButton;
      
      private var _mcTaskTarget:MovieClip;
      
      private var _timer:Timer;
      
      private var _style:int;
      
      private var _gotoGameTime:Number;
      
      private var _gotoTrainTime:Number;
      
      private var _quick:QuickBuyFrame;
      
      public function TaskMainFrame()
      {
         super();
         this.initView();
         this.addEvent();
      }
      
      override public function get width() : Number
      {
         return _container.width;
      }
      
      override public function get height() : Number
      {
         return _container.height;
      }
      
      private function initView() : void
      {
         this.initStyleNormalBG();
         this.initStyleGuideBG();
         this.leftPanel = ComponentFactory.Instance.creatComponentByStylename("core.quest.questCateList");
         this.leftPanelContent = new VBox();
         this.leftPanelContent.spacing = 0;
         this.leftPanel.setView(this.leftPanelContent);
         addToContent(this.leftPanel);
         this.addQuestList();
         QuestInfoPanelView.panelHeight = "TaskMainFrame";
         this.infoView = new QuestInfoPanelView();
         PositionUtils.setPos(this.infoView,"quest.infoPanelPos");
         addToContent(this.infoView);
         this._questBtn = ComponentFactory.Instance.creat("quest.getAwardBtn");
         addToContent(this._questBtn);
         this._goDungeonBtn = ComponentFactory.Instance.creatComponentByStylename("core.quest.GoDungeonBtn");
         addToContent(this._goDungeonBtn);
         this._goDungeonBtn.visible = false;
         this._gotoGameBtn = ComponentFactory.Instance.creatComponentByStylename("core.quest.goGameBtn");
         addToContent(this._gotoGameBtn);
         this._gotoGameBtn.visible = false;
         this._gotoTrainBtn = ComponentFactory.Instance.creatComponentByStylename("core.quest.goGameBtn");
         addToContent(this._gotoTrainBtn);
         this._gotoTrainBtn.visible = false;
         this._gotoAcademy = ComponentFactory.Instance.creatComponentByStylename("core.quest.gotoAcademyBtn");
         addToContent(this._gotoAcademy);
         this._gotoAcademy.visible = false;
         this._downloadClientBtn = ComponentFactory.Instance.creat("core.quest.DownloadClientBtn");
         this._downloadClientBtn.text = LanguageMgr.GetTranslation("tank.manager.TaskManager.DownloadClient");
         addToContent(this._downloadClientBtn);
         this._downloadClientBtn.visible = false;
         this._buySpinelBtn = ComponentFactory.Instance.creatComponentByStylename("core.quest.buySpinelBtn");
         this._buySpinelBtn.text = LanguageMgr.GetTranslation("tank.manager.TaskManager.buySpinel");
         addToContent(this._buySpinelBtn);
         var _loc1_:Object = new Object();
         _loc1_[AlphaShinerAnimation.COLOR] = EffectColorType.GOLD;
         this._goDungeonBtnShine = EffectManager.Instance.creatEffect(EffectTypes.ALPHA_SHINER_ANIMATION,this._goDungeonBtn,_loc1_);
         this._goDungeonBtnShine.stop();
         this._downClientShine = EffectManager.Instance.creatEffect(EffectTypes.ALPHA_SHINER_ANIMATION,this._downloadClientBtn,_loc1_);
         this._downClientShine.play();
         this._questBtnShine = EffectManager.Instance.creatEffect(EffectTypes.ALPHA_SHINER_ANIMATION,this._questBtn,_loc1_);
         this._questBtnShine.stop();
         this._buySpinelBtn.visible = false;
         this._questBtn.enable = false;
         titleText = LanguageMgr.GetTranslation("tank.game.ToolStripView.task");
         if(PathManager.solveTrusteeshipEnable())
         {
            this._trusteeshipView = ComponentFactory.Instance.creatCustomObject("quest.trusteeshipView");
            addToContent(this._trusteeshipView);
         }
         this.showStyle(NORMAL);
      }
      
      private function initStyleNormalBG() : void
      {
         this._leftBGStyleNormal = ComponentFactory.Instance.creat("asset.core.quest.leftBGStyle1");
         PositionUtils.setPos(this._leftBGStyleNormal,"quest.leftBgpos");
         this._rightBGStyleNormal = ComponentFactory.Instance.creat("asset.background.mapBg");
         PositionUtils.setPos(this._rightBGStyleNormal,"quest.rightBgpos");
         this._rightBottomBg = ComponentFactory.Instance.creatComponentByStylename("quest.rightBottomBgImg");
         addToContent(this._leftBGStyleNormal);
         addToContent(this._rightBGStyleNormal);
         addToContent(this._rightBottomBg);
      }
      
      private function clearVBox() : void
      {
         while(this.leftPanelContent.numChildren)
         {
            this.leftPanelContent.removeChild(this.leftPanelContent.getChildAt(0));
         }
      }
      
      private function initStyleGuideBG() : void
      {
      }
      
      private function switchBG(param1:int) : void
      {
      }
      
      private function addQuestList() : void
      {
         var _loc1_:int = 0;
         var _loc2_:QuestCateView = null;
         if(this.cateViewArr)
         {
            return;
         }
         this.cateViewArr = new Array();
         _loc1_ = 0;
         while(_loc1_ < TYPE_NUMBER)
         {
            _loc2_ = new QuestCateView(_loc1_,this.leftPanel);
            _loc2_.collapse();
            _loc2_.x = this.CATEVIEW_X;
            _loc2_.y = this.CATEVIEW_Y + this.CATEVIEW_H * _loc1_;
            _loc2_.addEventListener(QuestCateView.TITLECLICKED,this.__onTitleClicked);
            _loc2_.addEventListener(Event.CHANGE,this.__onCateViewChange);
            _loc2_.addEventListener(QuestCateView.ENABLE_CHANGE,this.__onEnbleChange);
            this.cateViewArr.push(_loc2_);
            this.leftPanelContent.addChild(_loc2_);
            _loc1_++;
         }
         this.leftPanel.invalidateViewport();
         this.__onEnbleChange(null);
      }
      
      private function __onEnbleChange(param1:Event) : void
      {
         var _loc4_:QuestCateView = null;
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         while(_loc3_ < TYPE_NUMBER - 1)
         {
            _loc4_ = this.cateViewArr[_loc3_];
            if(_loc4_.visible)
            {
               _loc4_.y = this.CATEVIEW_Y + this.CATEVIEW_H * _loc2_;
               _loc2_++;
               this.leftPanelContent.addChild(_loc4_);
            }
            _loc3_++;
         }
         this.leftPanel.setView(this.leftPanelContent);
         this.leftPanel.invalidateViewport();
      }
      
      private function addEvent() : void
      {
         TaskManager.instance.addEventListener(TaskEvent.CHANGED,this.__onDataChanged);
         TaskManager.instance.addEventListener(TaskEvent.FINISH,this.__onTaskFinished);
         addEventListener(FrameEvent.RESPONSE,this.__frameEventHandler);
         this._questBtn.addEventListener(MouseEvent.CLICK,this.__onQuestBtnClicked);
         this._goDungeonBtn.addEventListener(MouseEvent.CLICK,this.__onGoDungeonClicked);
         this._gotoAcademy.addEventListener(MouseEvent.CLICK,this.__gotoAcademy);
         this._downloadClientBtn.addEventListener(MouseEvent.CLICK,this.__downloadClient);
         this._buySpinelBtn.addEventListener(MouseEvent.CLICK,this.__buySpinelClick);
         this._gotoGameBtn.addEventListener(MouseEvent.CLICK,this.__gotoGame);
         this._gotoTrainBtn.addEventListener(MouseEvent.CLICK,this.__gotoTrain);
         PetBagController.instance().addEventListener(UpdatePetFarmGuildeEvent.FINISH,this.__updatePetFarmGuilde);
      }
      
      protected function __onTaskFinished(param1:TaskEvent) : void
      {
         if(param1.data.id == 367)
         {
            PetBagController.instance().showPetFarmGuildArrow(ArrowType.OPEN_PET_BAG,-50,"farmTrainer.openPetArrowPos","asset.farmTrainer.clickHere","farmTrainer.openBagTipPos");
         }
         else if(param1.data.id == 369 && StateManager.currentStateType == StateType.MAIN)
         {
            PetBagController.instance().showPetFarmGuildArrow(ArrowType.GRAIN_IN_FRAME,-150,"farmTrainer.openFarmArrowPos","asset.farmTrainer.grain1","farmTrainer.grainopenFarmTipPos");
         }
         else if(param1.data.id == 368 && StateManager.currentStateType == StateType.MAIN)
         {
            PetBagController.instance().showPetFarmGuildArrow(ArrowType.PLANT_IN_FRAME,-150,"farmTrainer.openFarmArrowPos","asset.farmTrainer.seed","farmTrainer.grainopenFarmTipPosout");
         }
         else if(param1.data.id == TaskManager.BEADLEAD_TASKTYPE1)
         {
            BeadLeadManager.Instance.dispatchEvent(new BeadLeadEvent(BeadLeadEvent.GETTASKISCOMPLETE));
         }
      }
      
      private function removeEvent() : void
      {
         TaskManager.instance.removeEventListener(TaskEvent.CHANGED,this.__onDataChanged);
         TaskManager.instance.removeEventListener(TaskEvent.FINISH,this.__onTaskFinished);
         removeEventListener(FrameEvent.RESPONSE,this.__frameEventHandler);
         this._questBtn.removeEventListener(MouseEvent.CLICK,this.__onQuestBtnClicked);
         this._goDungeonBtn.removeEventListener(MouseEvent.CLICK,this.__onGoDungeonClicked);
         this._gotoAcademy.removeEventListener(MouseEvent.CLICK,this.__gotoAcademy);
         this._downloadClientBtn.removeEventListener(MouseEvent.CLICK,this.__downloadClient);
         this._buySpinelBtn.removeEventListener(MouseEvent.CLICK,this.__buySpinelClick);
         this._gotoGameBtn.removeEventListener(MouseEvent.CLICK,this.__gotoGame);
         this._gotoTrainBtn.removeEventListener(MouseEvent.CLICK,this.__gotoTrain);
         PetBagController.instance().removeEventListener(UpdatePetFarmGuildeEvent.FINISH,this.__updatePetFarmGuilde);
         RoomManager.Instance.removeEventListener(CrazyTankSocketEvent.GAME_ROOM_CREATE,this.__gameStart);
      }
      
      private function __updatePetFarmGuilde(param1:UpdatePetFarmGuildeEvent) : void
      {
         var _loc3_:int = 0;
         var _loc4_:QuestCateView = null;
         var _loc5_:QuestInfo = null;
         PetBagController.instance().finishTask();
         var _loc2_:QuestInfo = param1.data as QuestInfo;
         if(_loc2_ && _loc2_.QuestID == PetFarmGuildeTaskType.PET_TASK3)
         {
            _loc3_ = 0;
            while(_loc3_ < this.cateViewArr.length)
            {
               _loc4_ = this.cateViewArr[_loc3_] as QuestCateView;
               _loc4_.initData();
               for each(_loc5_ in _loc4_.data.list)
               {
                  if(_loc5_ == _loc2_)
                  {
                     _loc4_.active();
                     this.jumpToQuest(_loc2_);
                     if(!PetBagController.instance().haveTaskOrderByID(PetFarmGuildeTaskType.PET_TASK3))
                     {
                     }
                     return;
                  }
               }
               _loc3_++;
            }
         }
      }
      
      private function __onDataChanged(param1:TaskEvent) : void
      {
         var _loc2_:uint = 0;
         if(!this._currentCateView || this.currentNewCateView != null)
         {
            return;
         }
         if(this._currentCateView.active())
         {
            return;
         }
         _loc2_ = 0;
         while(!(this.cateViewArr[_loc2_] as QuestCateView).active())
         {
            _loc2_++;
            if(_loc2_ == 4)
            {
               return;
            }
         }
      }
      
      private function __frameEventHandler(param1:FrameEvent) : void
      {
         switch(param1.responseCode)
         {
            case FrameEvent.ESC_CLICK:
            case FrameEvent.CLOSE_CLICK:
               SoundManager.instance.play("008");
               TaskManager.instance.switchVisible();
         }
      }
      
      public function jumpToQuest(param1:QuestInfo) : void
      {
         if(param1.MapID > 0)
         {
            this.showOtherBtn(param1);
         }
         else
         {
            this._goDungeonBtn.visible = false;
            this._goDungeonBtnShine.stop();
            this._gotoAcademy.visible = false;
            this._downloadClientBtn.visible = false;
            this._questBtn.visible = true;
            this._gotoGameBtn.visible = false;
            this._gotoTrainBtn.visible = false;
            this._buySpinelBtn.visible = this.existRewardId(param1,SPINEL);
            this.showStyle(NORMAL);
            this.hideGuide();
         }
         this.infoView.info = param1;
         this.showQuestOverBtn(param1.isCompleted);
         if(PathManager.solveTrusteeshipEnable())
         {
            this.showTrusteeshipView(param1);
         }
      }
      
      private function showTrusteeshipView(param1:QuestInfo) : void
      {
         if(param1.TrusteeshipCost >= 0)
         {
            this._trusteeshipView.visible = true;
            this._trusteeshipView.refreshView(param1,this.showQuestOverBtn,this._questBtn);
         }
         else
         {
            this._trusteeshipView.visible = false;
            this._trusteeshipView.clearSome();
         }
      }
      
      private function showQuestOverBtn(param1:Boolean) : void
      {
         NewHandContainer.Instance.clearArrowByID(ArrowType.GET_REWARD);
         if(param1)
         {
            this._questBtn.enable = true;
            this._questBtn.visible = true;
            this._questBtnShine.play();
            this._goDungeonBtn.visible = false;
            this._gotoAcademy.visible = false;
            this._gotoGameBtn.visible = false;
            this._gotoTrainBtn.visible = false;
            if(WeakGuildManager.Instance.switchUserGuide && !PlayerManager.Instance.Self.IsWeakGuildFinish(Step.OLD_PLAYER) && TaskManager.instance.getQuestByID(318).isCompleted && !TaskManager.instance.getQuestByID(318).isAchieved)
            {
               NewHandContainer.Instance.showArrow(ArrowType.GET_REWARD,0,"trainer.getTaskRewardArrowPos","asset.trainer.txtGetReward","trainer.getTaskRewardTipPos",this);
            }
         }
         else
         {
            this._questBtn.enable = false;
            this._questBtnShine.stop();
         }
      }
      
      private function showOtherBtn(param1:QuestInfo) : void
      {
         if(param1.MapID > 0)
         {
            if(param1.MapID == 2)
            {
               this._gotoAcademy.visible = true;
               this._goDungeonBtn.visible = false;
               this._downloadClientBtn.visible = false;
               this._questBtn.visible = false;
               this._buySpinelBtn.visible = false;
               this._gotoGameBtn.visible = false;
               this._gotoTrainBtn.visible = false;
            }
            else if(param1.MapID == 3)
            {
               this._downloadClientBtn.visible = true;
               this._questBtn.visible = true;
               this._gotoAcademy.visible = false;
               this._goDungeonBtn.visible = false;
               this._buySpinelBtn.visible = false;
               this._gotoGameBtn.visible = false;
               this._gotoTrainBtn.visible = false;
            }
            else if(param1.MapID == 4)
            {
               this._downloadClientBtn.visible = false;
               this._gotoAcademy.visible = false;
               this._goDungeonBtn.visible = false;
               this._buySpinelBtn.visible = false;
               this._gotoGameBtn.visible = true;
               this._gotoTrainBtn.visible = false;
            }
            else if(param1.MapID > 9 && param1.MapID < 30)
            {
               this._downloadClientBtn.visible = false;
               this._gotoAcademy.visible = false;
               this._goDungeonBtn.visible = false;
               this._buySpinelBtn.visible = false;
               this._gotoGameBtn.visible = false;
               this._gotoTrainBtn.visible = true;
            }
            else
            {
               this.showStyle(GUIDE);
               this._goDungeonBtn.visible = true;
               this._goDungeonBtn.enable = !param1.isCompleted;
               if(this._goDungeonBtn.enable)
               {
                  this._goDungeonBtnShine.play();
               }
               else
               {
                  this._goDungeonBtnShine.stop();
               }
               this._gotoAcademy.visible = false;
               this._downloadClientBtn.visible = false;
               this._questBtn.visible = false;
               this._buySpinelBtn.visible = false;
               this._gotoGameBtn.visible = false;
               this._gotoTrainBtn.visible = false;
            }
         }
         else
         {
            this._gotoAcademy.visible = false;
            this._downloadClientBtn.visible = false;
            this._goDungeonBtn.visible = false;
            this._gotoGameBtn.visible = false;
            this._gotoTrainBtn.visible = false;
            this._buySpinelBtn.visible = this.existRewardId(param1,SPINEL);
         }
         if(WeakGuildManager.Instance.switchUserGuide && !PlayerManager.Instance.Self.IsWeakGuildFinish(Step.OLD_PLAYER) && PlayerManager.Instance.Self.Grade < 2 && this._goDungeonBtn.visible && this._goDungeonBtn.enable)
         {
            this.showGuide();
         }
      }
      
      private function existRewardId(param1:QuestInfo, param2:int) : Boolean
      {
         var _loc3_:QuestItemReward = null;
         for each(_loc3_ in param1.itemRewards)
         {
            if(_loc3_.itemID == param2)
            {
               return true;
            }
         }
         return false;
      }
      
      private function showGuide() : void
      {
         this.hideGuide();
         if(!this._mcTaskTarget)
         {
            this._mcTaskTarget = ComponentFactory.Instance.creatCustomObject("trainer.mcTaskTarget");
         }
         if(!this._timer)
         {
            this._timer = new Timer(4000,1);
         }
         addChild(this._mcTaskTarget);
         this._timer.addEventListener(TimerEvent.TIMER,this.__timer);
         this._timer.start();
      }
      
      private function __timer(param1:TimerEvent) : void
      {
         this.resetTimer();
         removeChild(this._mcTaskTarget);
         NewHandContainer.Instance.showArrow(ArrowType.CLICK_BEAT,0,"trainer.clickBeatArrowPos","asset.trainer.txtClickBeat","trainer.clickBeatTipPos",this);
      }
      
      private function resetTimer() : void
      {
         if(this._timer)
         {
            this._timer.removeEventListener(TimerEvent.TIMER,this.__timer);
            this._timer.stop();
            this._timer.reset();
         }
      }
      
      private function hideGuide() : void
      {
         if(!WeakGuildManager.Instance.switchUserGuide || PlayerManager.Instance.Self.IsWeakGuildFinish(Step.OLD_PLAYER))
         {
            return;
         }
         NewHandContainer.Instance.clearArrowByID(ArrowType.CLICK_BEAT);
         this.resetTimer();
         if(this._mcTaskTarget && this._mcTaskTarget.parent)
         {
            removeChild(this._mcTaskTarget);
         }
      }
      
      private function showStyle(param1:int) : void
      {
         if(this._style == param1)
         {
            return;
         }
         this._style = param1;
         var _loc2_:int = 0;
         while(_loc2_ < this.cateViewArr.length)
         {
            (this.cateViewArr[_loc2_] as QuestCateView).taskStyle = param1;
            _loc2_++;
         }
         this.switchBG(param1);
      }
      
      private function __onCateViewChange(param1:Event) : void
      {
         var _loc4_:QuestCateView = null;
         this.clearVBox();
         var _loc2_:int = 42;
         var _loc3_:int = 0;
         while(_loc3_ < this.cateViewArr.length)
         {
            _loc4_ = this.cateViewArr[_loc3_] as QuestCateView;
            if(_loc4_.visible)
            {
               _loc4_.y = _loc2_;
               _loc2_ = _loc2_ + (_loc4_.contentHeight + 7);
               this.leftPanelContent.addChild(_loc4_);
            }
            _loc3_++;
         }
         this.leftPanel.setView(this.leftPanelContent);
         this.leftPanel.invalidateViewport();
      }
      
      private function __onTitleClicked(param1:Event) : void
      {
         var _loc4_:QuestCateView = null;
         this.clearVBox();
         if(!parent || this.currentNewCateView != null)
         {
            return;
         }
         if(this._currentCateView != param1.target as QuestCateView)
         {
         }
         this._currentCateView = param1.target as QuestCateView;
         var _loc2_:int = this.CATEVIEW_Y;
         var _loc3_:int = 0;
         while(_loc3_ < this.cateViewArr.length)
         {
            _loc4_ = this.cateViewArr[_loc3_] as QuestCateView;
            if(_loc4_ != this._currentCateView)
            {
               _loc4_.collapse();
            }
            if(_loc4_.visible)
            {
               _loc4_.y = _loc2_;
               _loc2_ = _loc2_ + (_loc4_.contentHeight + 7);
               this.leftPanelContent.addChild(_loc4_);
            }
            _loc3_++;
         }
         this.leftPanel.setView(this.leftPanelContent);
         this.leftPanel.invalidateViewport();
      }
      
      public function switchVisible() : void
      {
         if(parent)
         {
            this.dispose();
         }
         else
         {
            this._show();
         }
      }
      
      private function _show() : void
      {
         if(this._opened == true)
         {
            this.dispose();
         }
         this._opened = true;
         MainToolBar.Instance.unReadTask = false;
         LayerManager.Instance.addToLayer(this,LayerManager.GAME_DYNAMIC_LAYER,false,LayerManager.BLCAK_BLOCKGOUND);
      }
      
      public function open() : void
      {
         if(!this._opened)
         {
            this._show();
            this.leftPanel.invalidateViewport();
         }
      }
      
      private function __onQuestBtnClicked(param1:MouseEvent) : void
      {
         var _loc3_:BaseAlerFrame = null;
         if(!this.infoView.info)
         {
            return;
         }
         SoundManager.instance.play("008");
         var _loc2_:QuestInfo = this.infoView.info;
         if(_loc2_.RewardBindMoney != 0 && _loc2_.RewardBindMoney + PlayerManager.Instance.Self.BandMoney > ServerConfigManager.instance.getBindBidLimit(PlayerManager.Instance.Self.Grade,PlayerManager.Instance.Self.VIPLevel))
         {
            _loc3_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("tips"),LanguageMgr.GetTranslation("ddt.BindBid.tip"),LanguageMgr.GetTranslation("shop.PresentFrame.OkBtnText"),LanguageMgr.GetTranslation("shop.PresentFrame.CancelBtnText"),false,false,true,1);
            _loc3_.addEventListener(FrameEvent.RESPONSE,this.__onResponse);
         }
         else
         {
            this.finishQuest(_loc2_);
         }
      }
      
      private function finishQuest(param1:QuestInfo) : void
      {
         var _loc2_:Array = null;
         var _loc3_:QuestItemReward = null;
         var _loc4_:InventoryItemInfo = null;
         if(param1 && !param1.isCompleted)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("tank.view.task.TaskCatalogContentView.dropTaskIII"));
            this._currentCateView.active();
            return;
         }
         if(TaskManager.itemAwardSelected == -1)
         {
            _loc2_ = [];
            for each(_loc3_ in param1.itemRewards)
            {
               _loc4_ = new InventoryItemInfo();
               _loc4_.TemplateID = _loc3_.itemID;
               ItemManager.fill(_loc4_);
               _loc4_.ValidDate = _loc3_.ValidateTime;
               _loc4_.TemplateID = _loc3_.itemID;
               _loc4_.IsJudge = true;
               _loc4_.IsBinds = _loc3_.isBind;
               _loc4_.AttackCompose = _loc3_.AttackCompose;
               _loc4_.DefendCompose = _loc3_.DefendCompose;
               _loc4_.AgilityCompose = _loc3_.AgilityCompose;
               _loc4_.LuckCompose = _loc3_.LuckCompose;
               _loc4_.StrengthenLevel = _loc3_.StrengthenLevel;
               _loc4_.Count = _loc3_.count[param1.QuestLevel - 1];
               if(!(0 != _loc4_.NeedSex && this.getSexByInt(PlayerManager.Instance.Self.Sex) != _loc4_.NeedSex))
               {
                  if(_loc3_.isOptional == 1)
                  {
                     _loc2_.push(_loc4_);
                  }
               }
            }
            TryonSystemController.Instance.show(_loc2_,this.chooseReward,this.cancelChoose);
            return;
         }
         if(this.infoView.info)
         {
            TaskManager.instance.sendQuestFinish(this.infoView.info.QuestID);
            if(TaskManager.instance.isAchieved(TaskManager.instance.getQuestByID(318)) && TaskManager.instance.isAchieved(TaskManager.instance.getQuestByID(319)))
            {
               SocketManager.Instance.out.syncWeakStep(Step.ACHIVED_THREE_QUEST);
            }
         }
      }
      
      private function __onResponse(param1:FrameEvent) : void
      {
         var _loc2_:QuestInfo = null;
         param1.currentTarget.removeEventListener(FrameEvent.RESPONSE,this.__onResponse);
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK)
         {
            _loc2_ = this.infoView.info;
            this.finishQuest(_loc2_);
         }
         ObjectUtils.disposeObject(param1.currentTarget);
      }
      
      private function getSexByInt(param1:Boolean) : int
      {
         if(param1)
         {
            return 1;
         }
         return 2;
      }
      
      private function chooseReward(param1:ItemTemplateInfo) : void
      {
         SocketManager.Instance.out.sendQuestFinish(this.infoView.info.QuestID,param1.TemplateID);
         TaskManager.itemAwardSelected = -1;
      }
      
      private function cancelChoose() : void
      {
         TaskManager.itemAwardSelected = -1;
      }
      
      private function __onGoDungeonClicked(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         this._goDungeonBtn.enable = false;
         if(PlayerManager.Instance.Self.Bag.getItemAt(6) == null)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("tank.room.RoomIIController.weapon"));
            this._goDungeonBtn.enable = true;
            return;
         }
         if(this.infoView.info.MapID > 0)
         {
            NewHandGuideManager.Instance.mapID = this.infoView.info.MapID;
            if(StateManager.currentStateType == StateType.MATCH_ROOM || StateManager.currentStateType == StateType.CHALLENGE_ROOM)
            {
               StateManager.setState(StateType.ROOM_LIST);
            }
            setTimeout(SocketManager.Instance.out.createUserGuide,500);
         }
      }
      
      private function __gotoGame(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         var _loc2_:Number = getTimer();
         if(_loc2_ < this._gotoGameTime + 1000)
         {
            return;
         }
         this._gotoGameTime = _loc2_;
         this._gotoGameBtn.enable = false;
         StateManager.setState(StateType.ROOM_LIST);
         setTimeout(GameInSocketOut.sendCreateRoom,1000,RoomListBGView.PREWORD[int(Math.random() * RoomListBGView.PREWORD.length)],0,3);
         if(PetBagController.instance().haveTaskOrderByID(PetFarmGuildeTaskType.PET_TASK3))
         {
            PetBagController.instance().clearCurrentPetFarmGuildeArrow(ArrowType.JOIN_GAME);
         }
      }
      
      private function __gotoTrain(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         var _loc2_:Number = getTimer();
         if(_loc2_ < this._gotoTrainTime + 1000)
         {
            return;
         }
         this._gotoTrainTime = _loc2_;
         this._gotoTrainBtn.enable = false;
         if(PlayerManager.Instance.Self.WeaponID <= 0)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("tank.room.RoomIIController.weapon"));
            return;
         }
         TaskManager.instance.guideId = this.infoView.info.MapID;
         if(StateManager.currentStateType == StateType.MATCH_ROOM || StateManager.currentStateType == StateType.DUNGEON_ROOM)
         {
            StateManager.setState(StateType.ROOM_LIST);
         }
         setTimeout(this.__createUserGude,500);
         RoomManager.Instance.addEventListener(CrazyTankSocketEvent.GAME_ROOM_CREATE,this.__gameStart);
      }
      
      private function __createUserGude() : void
      {
         SocketManager.Instance.out.createUserGuide(5);
      }
      
      private function __gameStart(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         RoomManager.Instance.removeEventListener(CrazyTankSocketEvent.GAME_ROOM_CREATE,this.__gameStart);
         switch(TaskManager.instance.guideId)
         {
            case 10:
               _loc2_ = FightLibInfo.EASY;
               _loc3_ = LessonType.Measure;
               break;
            case 11:
               _loc2_ = FightLibInfo.NORMAL;
               _loc3_ = LessonType.Measure;
               break;
            case 12:
               _loc2_ = FightLibInfo.DIFFICULT;
               _loc3_ = LessonType.Measure;
               break;
            case 13:
               _loc2_ = FightLibInfo.EASY;
               _loc3_ = LessonType.Twenty;
               break;
            case 14:
               _loc2_ = FightLibInfo.NORMAL;
               _loc3_ = LessonType.Twenty;
               break;
            case 15:
               _loc2_ = FightLibInfo.DIFFICULT;
               _loc3_ = LessonType.Twenty;
               break;
            case 16:
               _loc2_ = FightLibInfo.EASY;
               _loc3_ = LessonType.SixtyFive;
               break;
            case 17:
               _loc2_ = FightLibInfo.NORMAL;
               _loc3_ = LessonType.SixtyFive;
               break;
            case 18:
               _loc2_ = FightLibInfo.DIFFICULT;
               _loc3_ = LessonType.SixtyFive;
               break;
            case 19:
               _loc2_ = FightLibInfo.EASY;
               _loc3_ = LessonType.HighThrow;
               break;
            case 20:
               _loc2_ = FightLibInfo.NORMAL;
               _loc3_ = LessonType.HighThrow;
               break;
            case 21:
               _loc2_ = FightLibInfo.DIFFICULT;
               _loc3_ = LessonType.HighThrow;
               break;
            case 22:
               _loc2_ = FightLibInfo.EASY;
               _loc3_ = LessonType.HighGap;
               break;
            case 23:
               _loc2_ = FightLibInfo.NORMAL;
               _loc3_ = LessonType.HighGap;
               break;
            case 24:
               _loc2_ = FightLibInfo.DIFFICULT;
               _loc3_ = LessonType.HighGap;
         }
         _loc4_ = this.getSecondType(_loc3_,_loc2_);
         GameInSocketOut.sendGameRoomSetUp(_loc3_,5,false,"","",_loc4_,_loc2_,0,false,0);
         FightLibManager.Instance.currentInfoID = _loc3_;
         FightLibManager.Instance.currentInfo.difficulty = _loc2_;
         StateManager.setState(StateType.FIGHT_LIB);
      }
      
      private function getSecondType(param1:int, param2:int) : int
      {
         var _loc3_:int = 0;
         if(param1 == LessonType.Twenty || param1 == LessonType.SixtyFive || param1 == LessonType.HighThrow)
         {
            if(param2 == FightLibInfo.EASY)
            {
               _loc3_ = 6;
            }
            else if(param2 == FightLibInfo.NORMAL)
            {
               _loc3_ = 5;
            }
            else
            {
               _loc3_ = 3;
            }
         }
         else if(param1 == LessonType.HighGap)
         {
            if(param2 == FightLibInfo.EASY)
            {
               _loc3_ = 5;
            }
            else if(param2 == FightLibInfo.NORMAL)
            {
               _loc3_ = 4;
            }
            else
            {
               _loc3_ = 3;
            }
         }
         return _loc3_;
      }
      
      private function __gotoAcademy(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         AcademyManager.Instance.gotoAcademyState();
      }
      
      private function __downloadClient(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         navigateToURL(new URLRequest(PathManager.solveClientDownloadPath()));
      }
      
      private function __buySpinelClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         var _loc2_:ShopItemInfo = ShopManager.Instance.getMoneyShopItemByTemplateID(SPINEL);
         this._quick = ComponentFactory.Instance.creatComponentByStylename("ddtcore.QuickFrame");
         this._quick.setTitleText(LanguageMgr.GetTranslation("tank.view.store.matte.goldQuickBuy"));
         this._quick.itemID = SPINEL;
         this._quick.buyFrom = 7;
         this._quick.maxLimit = 3;
         LayerManager.Instance.addToLayer(this._quick,LayerManager.GAME_TOP_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
      }
      
      public function gotoQuest(param1:QuestInfo) : void
      {
         var _loc3_:QuestCateView = null;
         var _loc4_:QuestInfo = null;
         var _loc5_:TaskPannelStripView = null;
         var _loc2_:int = 0;
         while(_loc2_ < this.cateViewArr.length)
         {
            _loc3_ = this.cateViewArr[_loc2_] as QuestCateView;
            for each(_loc4_ in _loc3_.data.list)
            {
               if(_loc4_.QuestID == param1.QuestID)
               {
                  _loc3_.active();
                  for each(_loc5_ in _loc3_.itemArr)
                  {
                     if(_loc5_.info.id == param1.QuestID)
                     {
                        _loc5_.active();
                     }
                  }
                  return;
               }
            }
            _loc2_++;
         }
      }
      
      override protected function __onAddToStage(param1:Event) : void
      {
         var _loc5_:QuestCateView = null;
         var _loc6_:QuestCateView = null;
         super.__onAddToStage(param1);
         var _loc2_:int = -1;
         var _loc3_:int = 0;
         while(_loc3_ < this.cateViewArr.length)
         {
            _loc5_ = this.cateViewArr[_loc3_] as QuestCateView;
            _loc5_.initData();
            _loc3_++;
         }
         var _loc4_:int = 0;
         while(_loc4_ < this.cateViewArr.length)
         {
            _loc6_ = this.cateViewArr[_loc4_] as QuestCateView;
            if(_loc6_.questType != 4)
            {
               _loc6_.initData();
               if(_loc6_.data.haveCompleted)
               {
                  _loc6_.active();
                  this.leftPanel.invalidateViewport();
                  return;
               }
               if(_loc6_.length > 0 && _loc2_ < 0)
               {
                  _loc2_ = _loc4_;
                  _loc6_.active();
               }
            }
            _loc4_++;
         }
      }
      
      override public function dispose() : void
      {
         var _loc1_:QuestCateView = null;
         this.hideGuide();
         this.removeEvent();
         this._currentCateView = null;
         this.currentNewCateView = null;
         while(_loc1_ = this.cateViewArr.pop())
         {
            _loc1_.removeEventListener(QuestCateView.TITLECLICKED,this.__onTitleClicked);
            _loc1_.removeEventListener(QuestCateView.ENABLE_CHANGE,this.__onEnbleChange);
            _loc1_.removeEventListener(Event.CHANGE,this.__onCateViewChange);
            _loc1_.dispose();
            _loc1_ = null;
         }
         ObjectUtils.disposeObject(this.leftPanelContent);
         this.leftPanelContent = null;
         ObjectUtils.disposeObject(this.leftPanel);
         this.leftPanel = null;
         if(this._quick && this._quick.canDispose)
         {
            this._quick.dispose();
         }
         this._quick = null;
         if(this.infoView)
         {
            this.infoView.dispose();
         }
         this.infoView = null;
         if(this._questBtn)
         {
            ObjectUtils.disposeObject(this._questBtn);
         }
         this._questBtn = null;
         if(this._goDungeonBtnShine)
         {
            ObjectUtils.disposeObject(this._goDungeonBtnShine);
         }
         this._goDungeonBtnShine = null;
         if(this._downClientShine)
         {
            ObjectUtils.disposeObject(this._downClientShine);
         }
         this._downClientShine = null;
         if(this._questBtnShine)
         {
            ObjectUtils.disposeObject(this._questBtnShine);
         }
         this._questBtnShine = null;
         if(this._mcTaskTarget)
         {
            ObjectUtils.disposeObject(this._mcTaskTarget);
         }
         this._mcTaskTarget = null;
         if(this._rightBottomBg)
         {
            ObjectUtils.disposeObject(this._rightBottomBg);
         }
         this._rightBottomBg = null;
         if(this._leftBGStyleNormal)
         {
            ObjectUtils.disposeObject(this._leftBGStyleNormal);
         }
         this._leftBGStyleNormal = null;
         if(this._rightBGStyleNormal)
         {
            ObjectUtils.disposeObject(this._rightBGStyleNormal);
         }
         this._rightBGStyleNormal = null;
         if(this._goDungeonBtn)
         {
            ObjectUtils.disposeObject(this._goDungeonBtn);
         }
         this._goDungeonBtn = null;
         if(this._downloadClientBtn)
         {
            ObjectUtils.disposeObject(this._downloadClientBtn);
         }
         this._downloadClientBtn = null;
         if(this._gotoAcademy)
         {
            ObjectUtils.disposeObject(this._gotoAcademy);
         }
         this._gotoAcademy = null;
         if(this._gotoTrainBtn)
         {
            ObjectUtils.disposeObject(this._gotoTrainBtn);
         }
         this._gotoTrainBtn = null;
         if(this._gotoGameBtn)
         {
            ObjectUtils.disposeObject(this._gotoGameBtn);
            this._gotoGameBtn = null;
         }
         ObjectUtils.disposeObject(this._trusteeshipView);
         this._trusteeshipView = null;
         this._opened = false;
         this._timer = null;
         TaskManager.instance.selectedQuest = null;
         TaskManager.instance.isShow = false;
         TaskManager.instance.clearNewQuest();
         NewHandContainer.Instance.clearArrowByID(ArrowType.GET_REWARD);
         PetBagController.instance().clearCurrentPetFarmGuildeArrow(ArrowType.JOIN_GAME);
         MainToolBar.Instance.tipTask();
         super.dispose();
         if(parent)
         {
            parent.removeChild(this);
         }
         dispatchEvent(new Event(TaskMainFrame.TASK_FRAME_HIDE));
      }
   }
}
