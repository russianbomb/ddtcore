package quest
{
   public class TrusteeshipPackageType
   {
      
      public static const NAME:String = "TrusteeshipPackageType";
      
      public static const START:int = 1;
      
      public static const START_EVENT:String = NAME + "start_event";
      
      public static const CANCEL:int = 2;
      
      public static const CANCEL_EVENT:String = NAME + "cancel_event";
      
      public static const SPEED_UP:int = 3;
      
      public static const SPEED_UP_EVENT:String = NAME + "speed_up_event";
      
      public static const BUY_SPIRIT:int = 4;
      
      public static const BUY_SPIRIT_EVENT:String = NAME + "buy_spirit_event";
      
      public static const USE_SPIRIT_ITME:int = 5;
      
      public static const USE_SPIRIT_ITME_EVENT:String = NAME + "use_spirit_item_event";
      
      public static const DELETE:int = 6;
      
      public static const DELETE_EVENT:String = NAME + "delete_event";
      
      public static const INIT:int = 7;
      
      public static const INIT_EVENT:String = NAME + "init_event";
       
      
      public function TrusteeshipPackageType()
      {
         super();
      }
   }
}
