package quest
{
   import ddt.data.quest.QuestInfo;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.ServerConfigManager;
   import ddt.manager.SocketManager;
   import ddt.manager.TaskManager;
   import ddt.manager.TimeManager;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import road7th.comm.PackageIn;
   
   public class TrusteeshipManager extends EventDispatcher
   {
      
      public static const UPDATE_ALL_DATA:String = "update_all_data";
      
      public static const UPDATE_SPIRIT_VALUE:String = "update_spirit_value";
      
      private static var _instance:TrusteeshipManager;
       
      
      private var _spiritValue:int;
      
      private var _list:Vector.<TrusteeshipDataVo>;
      
      private var _maxCanStartCount:int = -1;
      
      private var _maxSpiritValue:int = -1;
      
      private var _buyOnceSpiritValue:int = -1;
      
      private var _buyOnceNeedMoney:int = -1;
      
      private var _speedUpOneMinNeedMoney:int = -1;
      
      public function TrusteeshipManager()
      {
         this._list = new Vector.<TrusteeshipDataVo>();
         super(null);
      }
      
      public static function get instance() : TrusteeshipManager
      {
         if(_instance == null)
         {
            _instance = new TrusteeshipManager();
         }
         return _instance;
      }
      
      public function get list() : Vector.<TrusteeshipDataVo>
      {
         return this._list;
      }
      
      public function get spiritValue() : int
      {
         return this._spiritValue;
      }
      
      public function get speedUpOneMinNeedMoney() : int
      {
         if(this._speedUpOneMinNeedMoney == -1)
         {
            this._speedUpOneMinNeedMoney = int(ServerConfigManager.instance.serverConfigInfo["QuestCollocationAdvanceMinuteCost"].Value);
         }
         return this._speedUpOneMinNeedMoney;
      }
      
      public function get buyOnceNeedMoney() : int
      {
         if(this._buyOnceNeedMoney == -1)
         {
            this._buyOnceNeedMoney = int(ServerConfigManager.instance.serverConfigInfo["QuestCollocationEnergyBuyCost"].Value);
         }
         return this._buyOnceNeedMoney;
      }
      
      public function get buyOnceSpiritValue() : int
      {
         if(this._buyOnceSpiritValue == -1)
         {
            this._buyOnceSpiritValue = int(ServerConfigManager.instance.serverConfigInfo["QuestCollocationEnergyBuyCount"].Value);
         }
         return this._buyOnceSpiritValue;
      }
      
      public function get maxSpiritValue() : int
      {
         if(this._maxSpiritValue == -1)
         {
            this._maxSpiritValue = int(ServerConfigManager.instance.serverConfigInfo["QuestCollocationEnergyMaxCount"].Value);
         }
         return this._maxSpiritValue;
      }
      
      public function isCanStart() : Boolean
      {
         if(this._maxCanStartCount == -1)
         {
            this._maxCanStartCount = int(ServerConfigManager.instance.serverConfigInfo["QuestCollocationCount"].Value);
         }
         if(this._list.length < this._maxCanStartCount)
         {
            return true;
         }
         return false;
      }
      
      public function setup() : void
      {
         SocketManager.Instance.addEventListener(TrusteeshipPackageType.START_EVENT,this.updateData);
         SocketManager.Instance.addEventListener(TrusteeshipPackageType.BUY_SPIRIT_EVENT,this.updateSpiritValue);
      }
      
      private function updateSpiritValue(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         this._spiritValue = _loc2_.readInt();
         dispatchEvent(new Event(UPDATE_SPIRIT_VALUE));
      }
      
      private function updateData(param1:CrazyTankSocketEvent) : void
      {
         var _loc5_:TrusteeshipDataVo = null;
         var _loc2_:PackageIn = param1.pkg;
         this._spiritValue = _loc2_.readInt();
         this._list = new Vector.<TrusteeshipDataVo>();
         var _loc3_:int = _loc2_.readInt();
         var _loc4_:int = 0;
         while(_loc4_ < _loc3_)
         {
            _loc5_ = new TrusteeshipDataVo();
            _loc5_.id = _loc2_.readInt();
            _loc5_.endTime = _loc2_.readDate();
            this._list.push(_loc5_);
            _loc4_++;
         }
         dispatchEvent(new Event(UPDATE_ALL_DATA));
         this.isHasTrusteeshipQuestUnaviable();
      }
      
      public function isHasTrusteeshipQuestUnaviable() : Boolean
      {
         var _loc2_:TrusteeshipDataVo = null;
         var _loc3_:QuestInfo = null;
         var _loc1_:Boolean = false;
         for each(_loc2_ in this._list)
         {
            _loc3_ = TaskManager.instance.getQuestByID(_loc2_.id);
            if(!_loc3_ || !TaskManager.instance.isAvailableQuest(_loc3_,true))
            {
               SocketManager.Instance.out.sendTrusteeshipCancel(_loc2_.id);
               _loc1_ = true;
            }
         }
         return _loc1_;
      }
      
      public function isTrusteeshipQuestEnd(param1:int) : Boolean
      {
         var _loc2_:TrusteeshipDataVo = this.getTrusteeshipInfo(param1);
         if(!_loc2_)
         {
            return false;
         }
         var _loc3_:Number = _loc2_.endTime.getTime();
         var _loc4_:Number = TimeManager.Instance.Now().getTime();
         if(int((_loc3_ - _loc4_) / 1000) > 0)
         {
            return false;
         }
         return true;
      }
      
      public function getTrusteeshipInfo(param1:int) : TrusteeshipDataVo
      {
         var _loc2_:TrusteeshipDataVo = null;
         var _loc3_:TrusteeshipDataVo = null;
         for each(_loc3_ in this._list)
         {
            if(_loc3_.id == param1)
            {
               _loc2_ = _loc3_;
               break;
            }
         }
         return _loc2_;
      }
   }
}
