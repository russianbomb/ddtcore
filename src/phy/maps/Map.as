package phy.maps
{
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.Bitmap;
   import flash.display.DisplayObject;
   import flash.display.DisplayObjectContainer;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   import flash.utils.Dictionary;
   import flash.utils.getTimer;
   import par.enginees.ParticleEnginee;
   import par.renderer.DisplayObjectRenderer;
   import phy.object.PhysicalObj;
   import phy.object.Physics;
   import phy.object.PhysicsLayer;
   
   public class Map extends Sprite
   {
      
      private static var FRAME_TIME_OVER_TAG:int;
      
      private static var FRAME_OVER_COUNT_TAG:int;
       
      
      public var wind:Number = 3;
      
      public var gravity:Number = 9.8;
      
      public var airResistance:Number = 2;
      
      private var _objects:Dictionary;
      
      private var _phyicals:Dictionary;
      
      private var _bound:Rectangle;
      
      private var _partical:ParticleEnginee;
      
      protected var _sky:DisplayObject;
      
      protected var _stone:Ground;
      
      protected var _middle:DisplayObject;
      
      protected var _ground:Ground;
      
      protected var _livingLayer:Sprite;
      
      protected var _phyLayer:Sprite;
      
      protected var _mapThing:Sprite;
      
      private var _lastCheckTime:int = 0;
      
      private var _frameTimeOverCount:int = 0;
      
      protected var _mapChanged:Boolean = false;
      
      private var _isLackOfFPS:Boolean = false;
      
      public function Map(sky:DisplayObject, ground:Ground = null, stone:Ground = null, middle:DisplayObject = null)
      {
         super();
         this._phyicals = new Dictionary();
         this._objects = new Dictionary();
         this._sky = sky;
         addChild(this._sky);
         graphics.beginFill(0,1);
         graphics.drawRect(0,0,this._sky.width * 1.5,this._sky.height * 1.5);
         graphics.endFill();
         this._stone = stone;
         this._middle = middle;
         if(this._middle)
         {
            addChild(this._middle);
         }
         if(this._stone)
         {
            addChild(this._stone);
         }
         this._ground = ground;
         if(this._ground)
         {
            addChild(this._ground);
         }
         this._livingLayer = new Sprite();
         addChild(this._livingLayer);
         this._phyLayer = new Sprite();
         addChild(this._phyLayer);
         this._mapThing = new Sprite();
         addChild(this._mapThing);
         var rd:DisplayObjectRenderer = new DisplayObjectRenderer();
         this._phyLayer.addChild(rd);
         this._partical = new ParticleEnginee(rd);
         if(this._ground)
         {
            this._bound = new Rectangle(0,0,this._ground.width,this._ground.height);
         }
         else
         {
            this._bound = new Rectangle(0,0,this._stone.width,this._stone.height);
         }
         addEventListener(Event.ENTER_FRAME,this.__enterFrame);
      }
      
      public function get bound() : Rectangle
      {
         return this._bound;
      }
      
      public function get sky() : DisplayObject
      {
         return this._sky;
      }
      
      public function get mapThingLayer() : DisplayObjectContainer
      {
         return this._mapThing;
      }
      
      public function get ground() : Ground
      {
         return this._ground;
      }
      
      public function get stone() : Ground
      {
         return this._stone;
      }
      
      public function get particleEnginee() : ParticleEnginee
      {
         return this._partical;
      }
      
      public function Dig(center:Point, surface:Bitmap, border:Bitmap = null) : void
      {
         this._mapChanged = true;
         if(this._ground)
         {
            this._ground.Dig(center,surface,border);
         }
         if(this._stone)
         {
            this._stone.Dig(center,surface,border);
         }
      }
      
      public function get mapChanged() : Boolean
      {
         return this._mapChanged;
      }
      
      public function resetMapChanged() : void
      {
         this._mapChanged = false;
      }
      
      public function IsEmpty(x:int, y:int) : Boolean
      {
         return (this._ground == null || this._ground.IsEmpty(x,y)) && (this._stone == null || this._stone.IsEmpty(x,y));
      }
      
      public function IsRectangleEmpty(rect:Rectangle) : Boolean
      {
         return (this._ground == null || this._ground.IsRectangeEmptyQuick(rect)) && (this._stone == null || this._stone.IsRectangeEmptyQuick(rect));
      }
      
      public function findYLineNotEmptyPointDown(x:int, y:int, h:int) : Point
      {
         x = x < 0?int(0):x >= this._bound.width?int(this._bound.width - 1):int(x);
         y = y < 0?int(0):int(y);
         h = y + h >= this._bound.height?int(this._bound.height - y - 1):int(h);
         for(var i:int = 0; i < h; i++)
         {
            if(!this.IsEmpty(x - 1,y) || !this.IsEmpty(x + 1,y))
            {
               return new Point(x,y);
            }
            y++;
         }
         return null;
      }
      
      public function findYLineNotEmptyPointUp(x:int, y:int, h:int) : Point
      {
         x = x < 0?int(0):x > this._bound.width?int(this._bound.width):int(x);
         y = y < 0?int(0):int(y);
         h = y + h > this._bound.height?int(this._bound.height - y):int(h);
         for(var i:int = 0; i < h; i++)
         {
            if(!this.IsEmpty(x - 1,y) || !this.IsEmpty(x + 1,y))
            {
               return new Point(x,y);
            }
            y--;
         }
         return null;
      }
      
      public function findNextWalkPoint(posX:int, posY:int, direction:int, stepX:int, stepY:int) : Point
      {
         if(direction != 1 && direction != -1)
         {
            return null;
         }
         var tx:int = posX + direction * stepX;
         if(tx < 0 || tx > this._bound.width)
         {
            return null;
         }
         var p:Point = this.findYLineNotEmptyPointDown(tx,posY - stepY - 1,this._bound.height);
         if(p)
         {
            if(Math.abs(p.y - posY) > stepY)
            {
               trace(" null point offset:" + Math.abs(p.y - posY));
               p = null;
            }
         }
         return p;
      }
      
      public function canMove(x:int, y:int) : Boolean
      {
         return this.IsEmpty(x,y) && !this.IsOutMap(x,y);
      }
      
      public function IsOutMap(x:int, y:int) : Boolean
      {
         if(x < this._bound.x || x > this._bound.width || y > this._bound.height)
         {
            return true;
         }
         return false;
      }
      
      public function addPhysical(phyobj:Physics) : void
      {
         if(phyobj is PhysicalObj)
         {
            this._phyicals[phyobj] = phyobj;
            if(phyobj.layer == PhysicsLayer.GhostBox)
            {
               this._phyLayer.addChild(phyobj);
            }
            else if(phyobj.layer == PhysicsLayer.SimpleObject)
            {
               this._livingLayer.addChild(phyobj);
            }
            else if(phyobj.layer == PhysicsLayer.GameLiving)
            {
               this._livingLayer.addChild(phyobj);
            }
            else if(phyobj.layer == PhysicsLayer.Tomb || phyobj.layer == PhysicsLayer.AppointBottom)
            {
               this._livingLayer.addChildAt(phyobj,0);
            }
            else
            {
               this._phyLayer.addChild(phyobj);
            }
         }
         else
         {
            this._objects[phyobj] = phyobj;
            addChild(phyobj);
         }
         phyobj.setMap(this);
      }
      
      public function addToPhyLayer(b:DisplayObject) : void
      {
         this._phyLayer.addChild(b);
      }
      
      public function get livngLayer() : DisplayObjectContainer
      {
         return this._livingLayer;
      }
      
      public function addMapThing(phy:Physics) : void
      {
         this._mapThing.addChild(phy);
         phy.setMap(this);
         if(phy is PhysicalObj)
         {
            this._phyicals[phy] = phy;
         }
         else
         {
            this._objects[phy] = phy;
         }
      }
      
      public function removeMapThing(phy:Physics) : void
      {
         this._mapThing.removeChild(phy);
         phy.setMap(null);
         if(phy is PhysicalObj)
         {
            delete this._phyicals[phy];
         }
         else
         {
            delete this._objects[phy];
         }
      }
      
      public function setTopPhysical($phy:Physics) : void
      {
         $phy.parent.setChildIndex($phy,$phy.parent.numChildren - 1);
      }
      
      public function hasSomethingMoving() : Boolean
      {
         var p:PhysicalObj = null;
         for each(p in this._phyicals)
         {
            if(p.isMoving())
            {
               return true;
            }
         }
         return false;
      }
      
      public function removePhysical(phy:Physics) : void
      {
         if(phy.parent)
         {
            phy.parent.removeChild(phy);
         }
         phy.setMap(null);
         if(phy is PhysicalObj)
         {
            if(this._phyicals == null || !this._phyicals[phy])
            {
               return;
            }
            delete this._phyicals[phy];
         }
         else
         {
            if(this._objects && !this._objects[phy])
            {
               return;
            }
            delete this._objects[phy];
         }
      }
      
      public function hidePhysical(except:PhysicalObj) : void
      {
         var p:PhysicalObj = null;
         for each(p in this._phyicals)
         {
            if(p != except)
            {
               p.visible = false;
            }
         }
      }
      
      public function showPhysical() : void
      {
         var p:PhysicalObj = null;
         for each(p in this._phyicals)
         {
            p.visible = true;
         }
      }
      
      public function getPhysicalObjects(rect:Rectangle, except:PhysicalObj) : Array
      {
         var phy:PhysicalObj = null;
         var t:Rectangle = null;
         var temp:Array = new Array();
         for each(phy in this._phyicals)
         {
            if(phy != except && phy.isLiving && phy.canCollided)
            {
               t = phy.getCollideRect();
               t.offset(phy.x,phy.y);
               if(t.intersects(rect))
               {
                  temp.push(phy);
               }
            }
         }
         return temp;
      }
      
      public function getPhysicalObjectByPoint(point:Point, radius:Number, except:PhysicalObj = null) : Array
      {
         var t:PhysicalObj = null;
         var temp:Array = new Array();
         for each(t in this._phyicals)
         {
            if(t != except && t.isLiving && Point.distance(point,t.pos) <= radius)
            {
               temp.push(t);
            }
         }
         return temp;
      }
      
      public function getBoxesByRect(rect:Rectangle) : Array
      {
         var obj:PhysicalObj = null;
         var t:Rectangle = null;
         var temp:Array = new Array();
         for each(obj in this._phyicals)
         {
            if(obj.isBox() && obj.isLiving)
            {
               t = obj.getTestRect();
               t.offset(obj.x,obj.y);
               if(t.intersects(rect))
               {
                  temp.push(obj);
               }
            }
         }
         return temp;
      }
      
      private function __enterFrame(event:Event) : void
      {
         this.update();
      }
      
      protected function update() : void
      {
         var p:Physics = null;
         var num:Number = numChildren;
         for each(p in this._phyicals)
         {
            p.update(0.04);
         }
         this._partical.update();
      }
      
      private function checkOverFrameRate() : void
      {
         if(this._isLackOfFPS)
         {
            return;
         }
         var currentTime:int = getTimer();
         if(this._lastCheckTime == 0)
         {
            this._lastCheckTime = currentTime - 40;
         }
         if(currentTime - this._lastCheckTime > FRAME_TIME_OVER_TAG)
         {
            trace(String(currentTime - this._lastCheckTime));
            this._frameTimeOverCount++;
         }
         else
         {
            if(this._frameTimeOverCount > 0)
            {
               trace("delay frame count:" + this._frameTimeOverCount);
            }
            this._frameTimeOverCount = 0;
         }
         this._lastCheckTime = currentTime;
         if(this._frameTimeOverCount > FRAME_OVER_COUNT_TAG)
         {
            this._isLackOfFPS = true;
         }
      }
      
      public function getCollidedPhysicalObjects(rect:Rectangle, except:PhysicalObj) : Array
      {
         var phy:PhysicalObj = null;
         var t:Rectangle = null;
         var temp:Array = new Array();
         for each(phy in this._phyicals)
         {
            if(phy != except && phy.canCollided)
            {
               t = phy.getCollideRect();
               t.offset(phy.x,phy.y);
               if(t.intersects(rect))
               {
                  temp.push(phy);
               }
            }
         }
         return temp;
      }
      
      public function get isLackOfFPS() : Boolean
      {
         return this._isLackOfFPS;
      }
      
      public function dispose() : void
      {
         var obj:DisplayObject = null;
         removeEventListener(Event.ENTER_FRAME,this.__enterFrame);
         var num:Number = numChildren;
         for(var i:int = num - 1; i >= 0; i--)
         {
            obj = getChildAt(i);
            if(obj is Physics)
            {
               Physics(obj).dispose();
            }
         }
         this._partical.dispose();
         if(this._ground)
         {
            this._ground.dispose();
         }
         this._ground = null;
         if(this._stone)
         {
            this._stone.dispose();
         }
         this._stone = null;
         if(this._middle)
         {
            if(this._middle.parent)
            {
               this._middle.parent.removeChild(this._middle);
            }
            this._middle = null;
         }
         removeChild(this._sky);
         this._sky = null;
         if(this._mapThing && this._mapThing.parent)
         {
            this._mapThing.parent.removeChild(this._mapThing);
         }
         ObjectUtils.disposeAllChildren(this);
         this._phyicals = null;
         this._objects = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
