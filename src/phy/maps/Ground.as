package phy.maps
{
   import flash.display.BitmapData;
   import flash.geom.Rectangle;
   
   public class Ground extends Tile
   {
       
      
      private var _bound:Rectangle;
      
      public function Ground(bitmapData:BitmapData, digable:Boolean)
      {
         super(bitmapData,digable);
         this._bound = new Rectangle(0,0,width,height);
      }
      
      public function IsEmpty(x:int, y:int) : Boolean
      {
         return GetAlpha(x,y) <= 150;
      }
      
      public function IsRectangleEmpty(rect:Rectangle) : Boolean
      {
         rect = this._bound.intersection(rect);
         if(rect.width == 0 || rect.height == 0)
         {
            return true;
         }
         if(!this.IsXLineEmpty(rect.x,rect.y,rect.width))
         {
            return false;
         }
         if(rect.height > 1)
         {
            if(!this.IsXLineEmpty(rect.x,rect.y + rect.height - 1,rect.width))
            {
               return false;
            }
            if(!this.IsYLineEmtpy(rect.x,rect.y + 1,rect.height - 1))
            {
               return false;
            }
            if(rect.width > 1 && !this.IsYLineEmtpy(rect.x + rect.width - 1,rect.y,rect.height - 1))
            {
               return false;
            }
         }
         return true;
      }
      
      public function IsRectangeEmptyQuick(rect:Rectangle) : Boolean
      {
         rect = this._bound.intersection(rect);
         if(this.IsEmpty(rect.right,rect.bottom) && this.IsEmpty(rect.left,rect.bottom) && this.IsEmpty(rect.right,rect.top) && this.IsEmpty(rect.left,rect.top))
         {
            return true;
         }
         return false;
      }
      
      public function IsXLineEmpty(x:int, y:int, w:int) : Boolean
      {
         x = x < 0?int(0):int(x);
         w = x + w > width?int(width - x):int(w);
         for(var i:int = 0; i < w; i++)
         {
            if(!this.IsEmpty(x + i,y))
            {
               return false;
            }
         }
         return true;
      }
      
      public function IsYLineEmtpy(x:int, y:int, h:int) : Boolean
      {
         y = y < 0?int(0):int(y);
         h = y + h > height?int(height - y):int(h);
         for(var i:int = 0; i < h; i++)
         {
            if(!this.IsEmpty(x,y + i))
            {
               return false;
            }
         }
         return true;
      }
   }
}
