package phy.math
{
   public class EulerVector
   {
       
      
      public var x0:Number;
      
      public var x1:Number;
      
      public var x2:Number;
      
      public function EulerVector(x0:Number, x1:Number, x2:Number)
      {
         super();
         this.x0 = x0;
         this.x1 = x1;
         this.x2 = x2;
      }
      
      public function clear() : void
      {
         this.x0 = 0;
         this.x1 = 0;
         this.x2 = 0;
      }
      
      public function clearMotion() : void
      {
         this.x1 = 0;
         this.x2 = 0;
      }
      
      public function ComputeOneEulerStep(m:Number, af:Number, f:Number, dt:Number) : void
      {
         this.x2 = (f - af * this.x1) / m;
         this.x1 = this.x1 + this.x2 * dt;
         this.x0 = this.x0 + this.x1 * dt;
      }
      
      public function toString() : String
      {
         return "x:" + this.x0 + ",v:" + this.x1 + ",a" + this.x2;
      }
   }
}
