package phy.object
{
   import flash.display.Shape;
   import flash.geom.Point;
   
   public class SmallObject extends Shape
   {
      
      protected static const MovieTime:Number = 0.6;
       
      
      protected var _elapsed:int = 0;
      
      protected var _color:int = 16711680;
      
      protected var _radius:int = 4;
      
      private var _pos:Point;
      
      protected var _isAttacking:Boolean = false;
      
      public var onProcess:Boolean = false;
      
      public function SmallObject()
      {
         this._pos = new Point();
         super();
         this.draw();
      }
      
      override public function set visible(value:Boolean) : void
      {
         super.visible = value;
      }
      
      public function get color() : int
      {
         return this._color;
      }
      
      public function set color(val:int) : void
      {
         this._color = val;
         this.draw();
      }
      
      public function get onMap() : Boolean
      {
         return parent != null;
      }
      
      protected function draw() : void
      {
      }
      
      public function onFrame(frameRate:int) : void
      {
      }
      
      public function dispose() : void
      {
         if(parent)
         {
            parent.removeChild(this);
         }
      }
      
      public function get pos() : Point
      {
         return this._pos;
      }
      
      public function set isAttacking(val:Boolean) : void
      {
         this._isAttacking = val;
      }
      
      public function get isAttacking() : Boolean
      {
         return this._isAttacking;
      }
   }
}
