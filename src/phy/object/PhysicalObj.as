package phy.object
{
   import flash.display.Sprite;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   import road7th.utils.MathUtils;
   
   public class PhysicalObj extends Physics
   {
       
      
      protected var _id:int;
      
      protected var _testRect:Rectangle;
      
      protected var _canCollided:Boolean;
      
      protected var _isLiving:Boolean;
      
      protected var _layerType:int;
      
      private var _drawPointContainer:Sprite;
      
      public function PhysicalObj(id:int, layerType:int = 1, mass:Number = 1, gravityFactor:Number = 1, windFactor:Number = 1, airResitFactor:Number = 1)
      {
         super(mass,gravityFactor,windFactor,airResitFactor);
         this._id = id;
         this._layerType = layerType;
         this._canCollided = false;
         this._testRect = new Rectangle(-5,-5,10,10);
         this._isLiving = true;
      }
      
      public function get Id() : int
      {
         return this._id;
      }
      
      public function get layerType() : int
      {
         return this._layerType;
      }
      
      public function setCollideRect(left:int, top:int, right:int, buttom:int) : void
      {
         this._testRect.top = top;
         this._testRect.left = left;
         this._testRect.right = right;
         this._testRect.bottom = buttom;
      }
      
      public function getCollideRect() : Rectangle
      {
         return this._testRect.clone();
      }
      
      public function get canCollided() : Boolean
      {
         return this._canCollided;
      }
      
      public function set canCollided(value:Boolean) : void
      {
         this._canCollided = value;
      }
      
      public function get smallView() : SmallObject
      {
         return null;
      }
      
      public function get isLiving() : Boolean
      {
         return this._isLiving;
      }
      
      override public function moveTo(p:Point) : void
      {
         var dx:Number = NaN;
         var dy:Number = NaN;
         var count:int = 0;
         var dt:Number = NaN;
         var cur:Point = null;
         var dest:Point = null;
         var t:int = 0;
         var rect:Rectangle = null;
         var list:Array = null;
         if(Point.distance(p,pos) >= 3)
         {
            dx = Math.abs(int(p.x) - int(x));
            dy = Math.abs(int(p.y) - int(y));
            count = dx > dy?int(dx):int(dy);
            dt = 1 / Number(count);
            cur = pos;
            for(t = Math.abs(count); t > 0; t = t - 3)
            {
               dest = Point.interpolate(cur,p,dt * t);
               rect = this.getCollideRect();
               rect.offset(dest.x,dest.y);
               list = _map.getPhysicalObjects(rect,this);
               if(list.length > 0)
               {
                  pos = dest;
                  this.collideObject(list);
               }
               else if(!_map.IsRectangleEmpty(rect))
               {
                  pos = dest;
                  this.collideGround();
               }
               else if(_map.IsOutMap(dest.x,dest.y))
               {
                  pos = dest;
                  this.flyOutMap();
               }
               if(!_isMoving)
               {
                  return;
               }
            }
            pos = p;
         }
      }
      
      public function calcObjectAngle(bounds:Number = 16) : Number
      {
         var pre_array:Array = null;
         var next_array:Array = null;
         var pre:Point = null;
         var next:Point = null;
         var bound:Number = NaN;
         var m:Number = NaN;
         var n:Number = NaN;
         var nn:Number = NaN;
         var i:int = 0;
         var j:int = 0;
         if(_map)
         {
            pre_array = new Array();
            next_array = new Array();
            pre = new Point();
            next = new Point();
            bound = bounds;
            for(m = 1; m <= bound; m = m + 2)
            {
               for(i = -10; i <= 10; i++)
               {
                  if(_map.IsEmpty(x + m,y - i))
                  {
                     if(i == -10)
                     {
                        break;
                     }
                     pre_array.push(new Point(x + m,y - i));
                     break;
                  }
               }
               for(j = -10; j <= 10; j++)
               {
                  if(_map.IsEmpty(x - m,y - j))
                  {
                     if(j == -10)
                     {
                        break;
                     }
                     next_array.push(new Point(x - m,y - j));
                     break;
                  }
               }
            }
            pre = new Point(x,y);
            next = new Point(x,y);
            for(n = 0; n < pre_array.length; n++)
            {
               pre = pre.add(pre_array[n]);
            }
            for(nn = 0; nn < next_array.length; nn++)
            {
               next = next.add(next_array[nn]);
            }
            pre.x = pre.x / (pre_array.length + 1);
            pre.y = pre.y / (pre_array.length + 1);
            next.x = next.x / (next_array.length + 1);
            next.y = next.y / (next_array.length + 1);
            return MathUtils.GetAngleTwoPoint(pre,next);
         }
         return 0;
      }
      
      public function calcObjectAngleDebug(bounds:Number = 16) : Number
      {
         var pre_array:Array = null;
         var next_array:Array = null;
         var pre:Point = null;
         var next:Point = null;
         var bound:Number = NaN;
         var m:Number = NaN;
         var n:Number = NaN;
         var nn:Number = NaN;
         var i:int = 0;
         var j:int = 0;
         if(_map)
         {
            pre_array = new Array();
            next_array = new Array();
            pre = new Point();
            next = new Point();
            bound = bounds;
            for(m = 1; m <= bound; m = m + 2)
            {
               for(i = -10; i <= 10; i++)
               {
                  if(_map.IsEmpty(x + m,y - i))
                  {
                     if(i == -10)
                     {
                        break;
                     }
                     pre_array.push(new Point(x + m,y - i));
                     break;
                  }
               }
               for(j = -10; j <= 10; j++)
               {
                  if(_map.IsEmpty(x - m,y - j))
                  {
                     if(j == -10)
                     {
                        break;
                     }
                     next_array.push(new Point(x - m,y - j));
                     break;
                  }
               }
            }
            pre = new Point(x,y);
            next = new Point(x,y);
            for(n = 0; n < pre_array.length; n++)
            {
               pre = pre.add(pre_array[n]);
            }
            this.drawPoint(pre_array,true);
            for(nn = 0; nn < next_array.length; nn++)
            {
               next = next.add(next_array[nn]);
            }
            this.drawPoint(next_array,false);
            pre.x = pre.x / (pre_array.length + 1);
            pre.y = pre.y / (pre_array.length + 1);
            next.x = next.x / (next_array.length + 1);
            next.y = next.y / (next_array.length + 1);
            return MathUtils.GetAngleTwoPoint(pre,next);
         }
         return 0;
      }
      
      private function drawPoint(data:Array, clear:Boolean) : void
      {
         if(this._drawPointContainer == null)
         {
            this._drawPointContainer = new Sprite();
         }
         if(clear)
         {
            this._drawPointContainer.graphics.clear();
         }
         for(var i:int = 0; i < data.length; i++)
         {
            this._drawPointContainer.graphics.beginFill(16711680);
            this._drawPointContainer.graphics.drawCircle(data[i].x,data[i].y,2);
            this._drawPointContainer.graphics.endFill();
         }
         _map.addChild(this._drawPointContainer);
      }
      
      protected function flyOutMap() : void
      {
         if(this._isLiving)
         {
            this.die();
         }
      }
      
      protected function collideObject(list:Array) : void
      {
         var obj:PhysicalObj = null;
         for each(obj in list)
         {
            obj.collidedByObject(this);
         }
      }
      
      protected function collideGround() : void
      {
         if(_isMoving)
         {
            stopMoving();
         }
      }
      
      public function collidedByObject(obj:PhysicalObj) : void
      {
      }
      
      public function setActionMapping(source:String, target:String) : void
      {
      }
      
      public function die() : void
      {
         this._isLiving = false;
         if(_isMoving)
         {
            stopMoving();
         }
      }
      
      public function getTestRect() : Rectangle
      {
         return this._testRect.clone();
      }
      
      public function isBox() : Boolean
      {
         return false;
      }
   }
}
