package phy.object
{
   import com.pickgliss.ui.core.Disposeable;
   import flash.display.Sprite;
   import flash.geom.Point;
   import phy.maps.Map;
   import phy.math.EulerVector;
   
   public class Physics extends Sprite implements Disposeable
   {
       
      
      protected var _mass:Number;
      
      protected var _gravityFactor:Number;
      
      protected var _windFactor:Number;
      
      protected var _airResitFactor:Number;
      
      protected var _vx:EulerVector;
      
      protected var _vy:EulerVector;
      
      protected var _ef:Point;
      
      protected var _isMoving:Boolean;
      
      protected var _map:Map;
      
      protected var _arf:Number = 0;
      
      protected var _gf:Number = 0;
      
      protected var _wf:Number = 0;
      
      public function Physics(mass:Number = 1, gravityFactor:Number = 1, windFactor:Number = 1, airResitFactor:Number = 1)
      {
         super();
         this._mass = mass;
         this._gravityFactor = gravityFactor;
         this._windFactor = windFactor;
         this._airResitFactor = airResitFactor;
         this._vx = new EulerVector(0,0,0);
         this._vy = new EulerVector(0,0,0);
         this._ef = new Point(0,0);
      }
      
      public function get layer() : int
      {
         return PhysicsLayer.Phy;
      }
      
      public function addExternForce(force:Point) : void
      {
         this._ef.x = this._ef.x + force.x;
         this._ef.y = this._ef.y + force.y;
         if(!this._isMoving && this._map)
         {
            this.startMoving();
         }
      }
      
      public function addSpeedXY(vector:Point) : void
      {
         this._vx.x1 = this._vx.x1 + vector.x;
         this._vy.x1 = this._vy.x1 + vector.y;
         if(!this._isMoving && this._map)
         {
            this.startMoving();
         }
      }
      
      public function setSpeedXY(vector:Point) : void
      {
         this._vx.x1 = vector.x;
         this._vy.x1 = vector.y;
         if(!this._isMoving && this._map)
         {
            this.startMoving();
         }
      }
      
      public function get Vx() : Number
      {
         return this._vx.x1;
      }
      
      public function get Vy() : Number
      {
         return this._vy.x2;
      }
      
      public function get motionAngle() : Number
      {
         return Math.atan2(this._vy.x1,this._vx.x1);
      }
      
      public function isMoving() : Boolean
      {
         return this._isMoving;
      }
      
      public function startMoving() : void
      {
         this._isMoving = true;
      }
      
      public function stopMoving() : void
      {
         this._vx.clearMotion();
         this._vy.clearMotion();
         this._isMoving = false;
      }
      
      public function setMap(map:Map) : void
      {
         this._map = map;
         if(this._map)
         {
            this._arf = this._map.airResistance * this._airResitFactor;
            this._gf = this._map.gravity * this._gravityFactor * this._mass;
            this._wf = this._map.wind * this._windFactor;
         }
      }
      
      protected function computeFallNextXY(dt:Number) : Point
      {
         this._vx.ComputeOneEulerStep(this._mass,this._arf,this._wf + this._ef.x,dt);
         this._vy.ComputeOneEulerStep(this._mass,this._arf,this._gf + this._ef.y,dt);
         return new Point(this._vx.x0,this._vy.x0);
      }
      
      public function get pos() : Point
      {
         return new Point(x,y);
      }
      
      public function set pos(value:Point) : void
      {
         this.x = value.x;
         this.y = value.y;
      }
      
      public function update(dt:Number) : void
      {
         if(this._isMoving && this._map)
         {
            this.updatePosition(dt);
         }
      }
      
      protected function updatePosition(dt:Number) : void
      {
         this.moveTo(this.computeFallNextXY(dt));
      }
      
      public function moveTo(p:Point) : void
      {
         if(p.x != x || p.y != y)
         {
            this.pos = p;
         }
      }
      
      override public function set x(value:Number) : void
      {
         super.x = value;
         this._vx.x0 = value;
      }
      
      override public function set y(value:Number) : void
      {
         super.y = value;
         this._vy.x0 = value;
      }
      
      public function dispose() : void
      {
         if(this._map)
         {
            this._map.removePhysical(this);
         }
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
