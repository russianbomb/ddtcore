package halloween
{
   import bagAndInfo.cell.BagCell;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.container.HBox;
   import com.pickgliss.ui.controls.container.VBox;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.manager.ItemManager;
   import ddt.manager.LanguageMgr;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   
   public class HalloweenPrizeView extends Sprite implements Disposeable
   {
       
      
      private var _bg:Bitmap;
      
      private var _rank:FilterFrameText;
      
      private var _prize:FilterFrameText;
      
      private var vbox:VBox;
      
      public function HalloweenPrizeView()
      {
         super();
         this.initView();
      }
      
      private function initView() : void
      {
         var _loc2_:HBox = null;
         var _loc3_:int = 0;
         var _loc4_:Bitmap = null;
         var _loc5_:ItemTemplateInfo = null;
         var _loc6_:InventoryItemInfo = null;
         var _loc7_:BagCell = null;
         this._bg = ComponentFactory.Instance.creat("asset.halloween.prize.bg");
         this._rank = ComponentFactory.Instance.creat("asset.halloween.titleName");
         this._prize = ComponentFactory.Instance.creat("asset.halloween.titleName");
         PositionUtils.setPos(this._rank,"asset.pos.title1");
         PositionUtils.setPos(this._prize,"asset.pos.title2");
         this._rank.text = LanguageMgr.GetTranslation("ddt.halloween.titleName1");
         this._prize.text = LanguageMgr.GetTranslation("ddt.halloween.titleName2");
         addChild(this._bg);
         addChild(this._rank);
         addChild(this._prize);
         this.vbox = ComponentFactory.Instance.creatComponentByStylename("asset.halloween.prize.vbox");
         this.vbox.autoSize = 2;
         var _loc1_:int = 0;
         while(_loc1_ < HalloweenManager.instance.prizeArr.length)
         {
            _loc2_ = new HBox();
            _loc2_.spacing = 5;
            _loc2_.autoSize = 2;
            _loc3_ = 0;
            while(_loc3_ < HalloweenManager.instance.prizeArr[_loc1_].length)
            {
               _loc4_ = ComponentFactory.Instance.creatBitmap("asset.halloween.prize.cell");
               _loc5_ = ItemManager.Instance.getTemplateById(HalloweenManager.instance.prizeArr[_loc1_][_loc3_].templateId) as ItemTemplateInfo;
               _loc6_ = new InventoryItemInfo();
               ObjectUtils.copyProperties(_loc6_,_loc5_);
               _loc6_.ValidDate = HalloweenManager.instance.prizeArr[_loc1_][_loc3_].validate;
               _loc6_.StrengthenLevel = HalloweenManager.instance.prizeArr[_loc1_][_loc3_].strenthLevel;
               _loc6_.AttackCompose = HalloweenManager.instance.prizeArr[_loc1_][_loc3_].attack;
               _loc6_.DefendCompose = HalloweenManager.instance.prizeArr[_loc1_][_loc3_].defend;
               _loc6_.LuckCompose = HalloweenManager.instance.prizeArr[_loc1_][_loc3_].luck;
               _loc6_.AgilityCompose = HalloweenManager.instance.prizeArr[_loc1_][_loc3_].agility;
               _loc6_.IsBinds = HalloweenManager.instance.prizeArr[_loc1_][_loc3_].isBind;
               _loc7_ = new BagCell(_loc3_,_loc6_,false,_loc4_);
               _loc7_.setContentSize(35,35);
               _loc7_.setCount(HalloweenManager.instance.prizeArr[_loc1_][_loc3_].count);
               _loc2_.addChild(_loc7_);
               _loc3_++;
            }
            this.vbox.addChild(_loc2_);
            _loc1_++;
         }
         this.vbox.x = 167 + (218 - this.vbox.width) / 2;
         addChild(this.vbox);
      }
      
      public function dispose() : void
      {
         if(this._bg)
         {
            ObjectUtils.disposeObject(this._bg);
         }
         this._bg = null;
         if(this._bg)
         {
            ObjectUtils.disposeObject(this._rank);
         }
         this._rank = null;
         if(this._bg)
         {
            ObjectUtils.disposeObject(this._prize);
         }
         this._prize = null;
      }
   }
}
