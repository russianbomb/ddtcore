package halloween.analyze
{
   import com.pickgliss.loader.DataAnalyzer;
   import halloween.info.HalloweenRankInfo;
   
   public class HalloweenRankDataAnalyzer extends DataAnalyzer
   {
       
      
      public var dataList:Vector.<HalloweenRankInfo>;
      
      public function HalloweenRankDataAnalyzer(param1:Function)
      {
         this.dataList = new Vector.<HalloweenRankInfo>();
         super(param1);
      }
      
      override public function analyze(param1:*) : void
      {
         var _loc3_:XMLList = null;
         var _loc4_:int = 0;
         var _loc5_:HalloweenRankInfo = null;
         var _loc2_:XML = new XML(param1);
         if(_loc2_.@value == "true")
         {
            _loc3_ = _loc2_.halloweenInfo;
            _loc4_ = 0;
            while(_loc4_ < _loc3_.length())
            {
               _loc5_ = new HalloweenRankInfo();
               _loc5_.name = _loc3_[_loc4_].@nickName;
               _loc5_.rank = _loc3_[_loc4_].@rank;
               _loc5_.num = _loc3_[_loc4_].@useNum;
               _loc5_.isvip = _loc3_[_loc4_].@isVIP != "0"?Boolean(true):Boolean(false);
               this.dataList.push(_loc5_);
               _loc4_++;
            }
            onAnalyzeComplete();
         }
      }
   }
}
