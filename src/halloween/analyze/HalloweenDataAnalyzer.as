package halloween.analyze
{
   import com.pickgliss.loader.DataAnalyzer;
   import halloween.info.HalloweenPrizeInfo;
   
   public class HalloweenDataAnalyzer extends DataAnalyzer
   {
       
      
      public var dataList:Array;
      
      public function HalloweenDataAnalyzer(param1:Function)
      {
         this.dataList = new Array();
         super(param1);
      }
      
      override public function analyze(param1:*) : void
      {
         var _loc3_:XMLList = null;
         var _loc4_:int = 0;
         var _loc5_:Array = null;
         var _loc6_:int = 0;
         var _loc7_:HalloweenPrizeInfo = null;
         var _loc2_:XML = new XML(param1);
         if(_loc2_.@value == "true")
         {
            _loc3_ = _loc2_..items;
            _loc4_ = 0;
            while(_loc4_ < _loc3_.length())
            {
               _loc5_ = new Array();
               _loc6_ = 0;
               while(_loc6_ < _loc3_[_loc4_].item.length())
               {
                  _loc7_ = new HalloweenPrizeInfo();
                  _loc7_.templateId = _loc3_[_loc4_].item[_loc6_].@templateId;
                  _loc7_.count = _loc3_[_loc4_].item[_loc6_].@count;
                  _loc7_.validate = _loc3_[_loc4_].item[_loc6_].@validate;
                  _loc7_.strenthLevel = _loc3_[_loc4_].item[_loc6_].@strenthLevel;
                  _loc7_.attack = _loc3_[_loc4_].item[_loc6_].@attack;
                  _loc7_.defend = _loc3_[_loc4_].item[_loc6_].@defend;
                  _loc7_.luck = _loc3_[_loc4_].item[_loc6_].@luck;
                  _loc7_.agility = _loc3_[_loc4_].item[_loc6_].@agility;
                  _loc7_.isBind = _loc3_[_loc4_].item[_loc6_].@isBind;
                  _loc5_.push(_loc7_);
                  _loc6_++;
               }
               this.dataList.push(_loc5_);
               _loc4_++;
            }
            onAnalyzeComplete();
         }
      }
   }
}
