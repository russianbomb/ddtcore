package shop.view
{
   import com.greensock.TimelineLite;
   import com.greensock.TweenLite;
   import com.greensock.TweenProxy;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.ISelectable;
   import com.pickgliss.ui.controls.TextButton;
   import com.pickgliss.ui.controls.container.VBox;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.image.Image;
   import com.pickgliss.ui.image.Scale9CornerImage;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.DisplayUtils;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.EquipType;
   import ddt.data.ShopType;
   import ddt.data.goods.ShopCarItemInfo;
   import ddt.data.goods.ShopItemInfo;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.ShopManager;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.DisplayObject;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.FocusEvent;
   import flash.events.KeyboardEvent;
   import flash.events.MouseEvent;
   import flash.geom.Point;
   import shop.ShopController;
   import shop.manager.ShopBuyManager;
   import shop.manager.ShopGiftsManager;
   
   public class ShopRankingView extends Sprite implements Disposeable
   {
       
      
      private var _controller:ShopController;
      
      private var _shopSearchBg:Image;
      
      private var _rankingTitleBg:Bitmap;
      
      private var _rankingTitle:Bitmap;
      
      private var _rankingBackBg:Scale9CornerImage;
      
      private var _rankingFrontBg:Scale9CornerImage;
      
      private var _rankingFrontTexture:ScaleBitmapImage;
      
      private var _rankingBg:Image;
      
      private var _shopSearchBtn:TextButton;
      
      private var _shopSearchText:FilterFrameText;
      
      private var _vBox:VBox;
      
      private var _rankingItems:Vector.<ShopRankingCellItem>;
      
      private var _rankingLightMc:MovieClip;
      
      private var _separator:Vector.<Bitmap>;
      
      private var _currentShopSearchText:String;
      
      private var _currentList:Vector.<ShopItemInfo>;
      
      private var _shopPresentClearingFrame:ShopPresentClearingFrame;
      
      public function ShopRankingView()
      {
         super();
      }
      
      public function setup(param1:ShopController) : void
      {
         this._controller = param1;
         this.initView();
         this.addEvent();
      }
      
      private function initView() : void
      {
         var _loc2_:Bitmap = null;
         this._shopSearchBg = ComponentFactory.Instance.creatComponentByStylename("ddtshop.ShopSearchBg");
         addChild(this._shopSearchBg);
         this._rankingBg = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RankingViewBg");
         addChild(this._rankingBg);
         this._rankingTitleBg = ComponentFactory.Instance.creatBitmap("asset.ddtshop.RankingTitleBg");
         addChild(this._rankingTitleBg);
         this._rankingTitle = ComponentFactory.Instance.creatBitmap("asset.ddtshop.RankingTitle");
         addChild(this._rankingTitle);
         this._shopSearchBtn = ComponentFactory.Instance.creatComponentByStylename("ddtshop.ShopSearchBtn");
         this._shopSearchBtn.text = LanguageMgr.GetTranslation("shop.ShopRankingView.SearchBtnText");
         addChild(this._shopSearchBtn);
         this._shopSearchText = ComponentFactory.Instance.creatComponentByStylename("ddtshop.ShopSearchText");
         this._shopSearchText.text = LanguageMgr.GetTranslation("shop.view.ShopRankingView.shopSearchText");
         addChild(this._shopSearchText);
         this._rankingItems = new Vector.<ShopRankingCellItem>();
         this._vBox = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RankingItemsContainer");
         addChild(this._vBox);
         this._rankingLightMc = ComponentFactory.Instance.creatCustomObject("ddtshop.RankingLightMc");
         this._separator = new Vector.<Bitmap>();
         var _loc1_:int = 0;
         while(_loc1_ < 5)
         {
            this._rankingItems[_loc1_] = ComponentFactory.Instance.creatCustomObject("ddtshop.ShopRankingCellItem");
            this._rankingItems[_loc1_].itemCellBtn.addEventListener(MouseEvent.CLICK,this.__itemClick);
            this._rankingItems[_loc1_].itemCellBtn.addEventListener(MouseEvent.MOUSE_OVER,this.__rankingItemsMouseOver);
            this._rankingItems[_loc1_].itemCellBtn.addEventListener(MouseEvent.MOUSE_OUT,this.__rankingItemsMouseOut);
            this._rankingItems[_loc1_].itemBg.addEventListener(MouseEvent.MOUSE_OVER,this.__rankingItemsMouseOver);
            this._rankingItems[_loc1_].itemBg.addEventListener(MouseEvent.MOUSE_OUT,this.__rankingItemsMouseOut);
            this._rankingItems[_loc1_].payPaneGivingBtn.addEventListener(MouseEvent.CLICK,this.__payPaneGivingBtnClick);
            this._rankingItems[_loc1_].payPaneBuyBtn.addEventListener(MouseEvent.CLICK,this.__payPaneBuyBtnClick);
            if(this._rankingItems[_loc1_].payPaneAskBtn)
            {
               this._rankingItems[_loc1_].payPaneAskBtn.addEventListener(MouseEvent.CLICK,this.payPaneAskHander);
            }
            this._vBox.addChild(this._rankingItems[_loc1_]);
            if(_loc1_ != 0)
            {
               _loc2_ = ComponentFactory.Instance.creatBitmap("asset.ddtshop.RankingSeparator");
               PositionUtils.setPos(_loc2_,ComponentFactory.Instance.creatCustomObject("ddtshop.RankingViewSeparator_" + _loc1_));
               addChild(_loc2_);
               this._separator.push(_loc2_);
            }
            _loc1_++;
         }
         this.loadList();
      }
      
      private function addEvent() : void
      {
         this._shopSearchText.addEventListener(FocusEvent.FOCUS_IN,this.__shopSearchTextFousIn);
         this._shopSearchText.addEventListener(FocusEvent.FOCUS_OUT,this.__shopSearchTextFousOut);
         this._shopSearchText.addEventListener(KeyboardEvent.KEY_DOWN,this.__shopSearchTextKeyDown);
         this._shopSearchBtn.addEventListener(MouseEvent.CLICK,this.__shopSearchBtnClick);
      }
      
      private function removeEvent() : void
      {
         this._shopSearchText.removeEventListener(FocusEvent.FOCUS_IN,this.__shopSearchTextFousIn);
         this._shopSearchText.removeEventListener(FocusEvent.FOCUS_OUT,this.__shopSearchTextFousOut);
         this._shopSearchText.removeEventListener(KeyboardEvent.KEY_DOWN,this.__shopSearchTextKeyDown);
         this._shopSearchBtn.removeEventListener(MouseEvent.CLICK,this.__shopSearchBtnClick);
         var _loc1_:int = 0;
         while(_loc1_ < 5)
         {
            this._rankingItems[_loc1_].itemCellBtn.removeEventListener(MouseEvent.CLICK,this.__itemClick);
            this._rankingItems[_loc1_].itemCellBtn.removeEventListener(MouseEvent.MOUSE_OVER,this.__rankingItemsMouseOver);
            this._rankingItems[_loc1_].itemCellBtn.removeEventListener(MouseEvent.MOUSE_OUT,this.__rankingItemsMouseOut);
            this._rankingItems[_loc1_].itemBg.removeEventListener(MouseEvent.MOUSE_OVER,this.__rankingItemsMouseOver);
            this._rankingItems[_loc1_].itemBg.removeEventListener(MouseEvent.MOUSE_OUT,this.__rankingItemsMouseOut);
            this._rankingItems[_loc1_].payPaneGivingBtn.removeEventListener(MouseEvent.CLICK,this.__payPaneGivingBtnClick);
            this._rankingItems[_loc1_].payPaneBuyBtn.removeEventListener(MouseEvent.CLICK,this.__payPaneBuyBtnClick);
            if(this._rankingItems[_loc1_].payPaneAskBtn)
            {
               this._rankingItems[_loc1_].payPaneAskBtn.removeEventListener(MouseEvent.CLICK,this.payPaneAskHander);
            }
            _loc1_++;
         }
      }
      
      private function payPaneAskHander(param1:MouseEvent) : void
      {
         param1.stopImmediatePropagation();
         var _loc2_:ShopRankingCellItem = param1.currentTarget.parent as ShopRankingCellItem;
         var _loc3_:ShopItemInfo = _loc2_.shopItemInfo;
         if(_loc3_ && _loc3_.LimitCount == 0)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("shop.ShopIIModel.countOver"));
            return;
         }
         if(_loc3_ != null)
         {
            SoundManager.instance.play("008");
            if(_loc3_.isDiscount == 2 && ShopManager.Instance.getDisCountShopItemByGoodsID(_loc3_.GoodsID) == null)
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.shop.discount.exit"));
               return;
            }
            ShopBuyManager.Instance.buy(_loc3_.GoodsID,_loc3_.isDiscount,3);
         }
      }
      
      private function __payPaneGivingBtnClick(param1:Event) : void
      {
         var _loc3_:ISelectable = null;
         param1.stopImmediatePropagation();
         var _loc2_:ShopRankingCellItem = param1.currentTarget.parent as ShopRankingCellItem;
         if(_loc2_.shopItemInfo && _loc2_.shopItemInfo.LimitCount == 0)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("shop.ShopIIModel.countOver"));
            return;
         }
         if(_loc2_.shopItemInfo != null)
         {
            SoundManager.instance.play("008");
            ShopGiftsManager.Instance.buy(_loc2_.shopItemInfo.GoodsID,false,ShopCheckOutView.PRESENT);
            for each(_loc3_ in this._rankingItems)
            {
               _loc3_.selected = false;
            }
            _loc2_.selected = true;
         }
      }
      
      private function __payPaneBuyBtnClick(param1:Event) : void
      {
         var _loc3_:ISelectable = null;
         param1.stopImmediatePropagation();
         var _loc2_:ShopRankingCellItem = param1.currentTarget.parent as ShopRankingCellItem;
         if(_loc2_.shopItemInfo && _loc2_.shopItemInfo.LimitCount == 0)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("shop.ShopIIModel.countOver"));
            return;
         }
         if(_loc2_.shopItemInfo != null)
         {
            SoundManager.instance.play("008");
            ShopBuyManager.Instance.buy(_loc2_.shopItemInfo.GoodsID);
            for each(_loc3_ in this._rankingItems)
            {
               _loc3_.selected = false;
            }
            _loc2_.selected = true;
         }
      }
      
      protected function __shopSearchTextKeyDown(param1:KeyboardEvent) : void
      {
         if(param1.keyCode == 13)
         {
            this.__shopSearchBtnClick();
         }
      }
      
      protected function __shopSearchTextFousIn(param1:FocusEvent) : void
      {
         if(this._shopSearchText.text == LanguageMgr.GetTranslation("shop.view.ShopRankingView.shopSearchText"))
         {
            this._shopSearchText.text = "";
         }
      }
      
      protected function __shopSearchTextFousOut(param1:FocusEvent) : void
      {
         if(this._shopSearchText.text.length == 0)
         {
            this._shopSearchText.text = LanguageMgr.GetTranslation("shop.view.ShopRankingView.shopSearchText");
         }
      }
      
      protected function __shopSearchBtnClick(param1:MouseEvent = null) : void
      {
         var _loc2_:Vector.<ShopItemInfo> = null;
         SoundManager.instance.play("008");
         if(this._shopSearchText.text == LanguageMgr.GetTranslation("shop.view.ShopRankingView.shopSearchText") || this._shopSearchText.text.length == 0)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("shop.view.ShopRankingView.PleaseEnterTheKeywords"));
            return;
         }
         if(this._currentShopSearchText != this._shopSearchText.text)
         {
            this._currentShopSearchText = this._shopSearchText.text;
            _loc2_ = ShopManager.Instance.getDesignatedAllShopItem();
            _loc2_ = ShopManager.Instance.fuzzySearch(_loc2_,this._currentShopSearchText);
            this._currentList = _loc2_;
         }
         else
         {
            _loc2_ = this._currentList;
         }
         if(_loc2_.length > 0)
         {
            this._controller.rightView.searchList(_loc2_);
         }
         else
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("shop.view.ShopRankingView.NoSearchResults"));
         }
      }
      
      public function loadList() : void
      {
         this.setList(ShopManager.Instance.getValidSortedGoodsByType(this.getType(),1,5));
      }
      
      private function getType() : int
      {
         var _loc1_:Array = [ShopType.M_POPULARITY_RANKING,ShopType.F_POPULARITY_RANKING];
         var _loc2_:int = this._controller.rightView.genderGroup.selectIndex;
         return int(_loc1_[_loc2_]);
      }
      
      public function setList(param1:Vector.<ShopItemInfo>) : void
      {
         this.clearitems();
         var _loc2_:int = 0;
         while(_loc2_ < 5)
         {
            if(_loc2_ < param1.length && param1[_loc2_])
            {
               this._rankingItems[_loc2_].shopItemInfo = param1[_loc2_];
            }
            _loc2_++;
         }
      }
      
      private function clearitems() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ < 5)
         {
            this._rankingItems[_loc1_].shopItemInfo = null;
            _loc1_++;
         }
      }
      
      private function __itemClick(param1:MouseEvent) : void
      {
         var _loc3_:ISelectable = null;
         var _loc4_:ISelectable = null;
         var _loc5_:Boolean = false;
         var _loc6_:Boolean = false;
         var _loc7_:int = 0;
         var _loc8_:ShopCarItemInfo = null;
         var _loc2_:ShopRankingCellItem = param1.currentTarget.parent as ShopRankingCellItem;
         if(!_loc2_.shopItemInfo)
         {
            return;
         }
         SoundManager.instance.play("008");
         if(_loc2_.shopItemInfo.LimitCount == 0)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("shop.ShopIIModel.countOver"));
            return;
         }
         if(this._controller.model.isOverCount(_loc2_.shopItemInfo))
         {
            for each(_loc3_ in this._rankingItems)
            {
               _loc3_.selected = _loc3_ == _loc2_;
            }
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("shop.ShopIIModel.GoodsNumberLimit"));
            return;
         }
         if(_loc2_.shopItemInfo && _loc2_.shopItemInfo.TemplateInfo)
         {
            for each(_loc4_ in this._rankingItems)
            {
               _loc4_.selected = _loc4_ == _loc2_;
            }
            if(EquipType.dressAble(_loc2_.shopItemInfo.TemplateInfo))
            {
               _loc7_ = _loc2_.shopItemInfo.TemplateInfo.NeedSex != 2?int(0):int(1);
               if(_loc2_.shopItemInfo.TemplateInfo.NeedSex != 0 && this._controller.rightView.genderGroup.selectIndex != _loc7_)
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("tank.view.changeColor.sexAlert"));
                  return;
               }
               this._controller.addTempEquip(_loc2_.shopItemInfo);
            }
            else
            {
               _loc8_ = new ShopCarItemInfo(_loc2_.shopItemInfo.GoodsID,_loc2_.shopItemInfo.TemplateID);
               ObjectUtils.copyProperties(_loc8_,_loc2_.shopItemInfo);
               _loc5_ = this._controller.addToCar(_loc8_);
            }
            _loc6_ = this._controller.leftView.getColorEditorVisble();
            if(_loc5_ && !_loc6_)
            {
               this.addCartEffects(_loc2_.itemCell);
            }
         }
      }
      
      private function addCartEffects(param1:DisplayObject) : void
      {
         var _loc4_:TweenProxy = null;
         var _loc5_:TimelineLite = null;
         var _loc6_:TweenLite = null;
         var _loc7_:TweenLite = null;
         if(!param1)
         {
            return;
         }
         var _loc2_:BitmapData = new BitmapData(param1.width,param1.height,true,0);
         _loc2_.draw(param1);
         var _loc3_:Bitmap = new Bitmap(_loc2_,"auto",true);
         parent.addChild(_loc3_);
         _loc4_ = TweenProxy.create(_loc3_);
         _loc4_.registrationX = _loc4_.width / 2;
         _loc4_.registrationY = _loc4_.height / 2;
         var _loc8_:Point = DisplayUtils.localizePoint(parent,param1);
         _loc4_.x = _loc8_.x + _loc4_.width / 2;
         _loc4_.y = _loc8_.y + _loc4_.height / 2;
         _loc5_ = new TimelineLite();
         _loc5_.vars.onComplete = this.twComplete;
         _loc5_.vars.onCompleteParams = [_loc5_,_loc4_,_loc3_];
         _loc6_ = new TweenLite(_loc4_,0.3,{
            "x":220,
            "y":430
         });
         _loc7_ = new TweenLite(_loc4_,0.3,{
            "scaleX":0.1,
            "scaleY":0.1
         });
         _loc5_.append(_loc6_);
         _loc5_.append(_loc7_,-0.2);
      }
      
      private function twComplete(param1:TimelineLite, param2:TweenProxy, param3:Bitmap) : void
      {
         if(param1)
         {
            param1.kill();
         }
         if(param2)
         {
            param2.destroy();
         }
         if(param3.parent)
         {
            param3.parent.removeChild(param3);
            param3.bitmapData.dispose();
         }
         param2 = null;
         param3 = null;
         param1 = null;
      }
      
      protected function __rankingItemsMouseOver(param1:MouseEvent) : void
      {
         var _loc2_:ShopRankingCellItem = param1.currentTarget.parent as ShopRankingCellItem;
         _loc2_.setItemLight(this._rankingLightMc);
         _loc2_.mouseOver();
      }
      
      protected function __rankingItemsMouseOut(param1:MouseEvent) : void
      {
         var _loc2_:ShopRankingCellItem = param1.currentTarget.parent as ShopRankingCellItem;
         _loc2_.mouseOut();
      }
      
      public function dispose() : void
      {
         var _loc1_:int = 0;
         this.removeEvent();
         if(this._separator)
         {
            _loc1_ = 0;
            while(_loc1_ < this._separator.length)
            {
               if(this._separator[_loc1_])
               {
                  ObjectUtils.disposeObject(this._separator[_loc1_]);
               }
               this._separator[_loc1_] = null;
               _loc1_++;
            }
         }
         this._separator = null;
         ObjectUtils.disposeObject(this._rankingTitleBg);
         this._rankingTitleBg = null;
         ObjectUtils.disposeObject(this._rankingTitle);
         this._rankingTitle = null;
         ObjectUtils.disposeObject(this._rankingBg);
         this._rankingBg = null;
         ObjectUtils.disposeObject(this._rankingLightMc);
         this._rankingLightMc = null;
         ObjectUtils.disposeObject(this._shopSearchBg);
         this._shopSearchBg = null;
         ObjectUtils.disposeObject(this._rankingBackBg);
         this._rankingBackBg = null;
         ObjectUtils.disposeObject(this._rankingFrontBg);
         this._rankingFrontBg = null;
         ObjectUtils.disposeObject(this._rankingBackBg);
         this._rankingBackBg = null;
         ObjectUtils.disposeObject(this._rankingFrontTexture);
         this._rankingFrontTexture = null;
         ObjectUtils.disposeObject(this._shopSearchBtn);
         this._shopSearchBtn = null;
         ObjectUtils.disposeObject(this._shopSearchText);
         this._shopSearchText = null;
         ObjectUtils.disposeObject(this._vBox);
         this._vBox = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
