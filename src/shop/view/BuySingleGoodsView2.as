package shop.view
{
   import baglocked.BaglockedManager;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.NumberSelecter;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.image.Image;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.ShopType;
   import ddt.data.goods.Price;
   import ddt.data.goods.ShopCarItemInfo;
   import ddt.data.goods.ShopItemInfo;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.LanguageMgr;
   import ddt.manager.LeavePageManager;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.ShopManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.utils.FilterWordManager;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import shop.manager.ShopSaleManager;
   
   public class BuySingleGoodsView2 extends Sprite implements Disposeable
   {
      
      public static const SHOP_CANNOT_FIND:String = "shopCannotfind";
       
      
      private var _frame:Frame;
      
      private var _shopCartItem:ShopCartItem;
      
      private var _commodityPricesText1:FilterFrameText;
      
      private var _commodityPricesText2:FilterFrameText;
      
      private var _commodityPricesText1Label:FilterFrameText;
      
      private var _commodityPricesText2Label:FilterFrameText;
      
      private var _needToPayTip:FilterFrameText;
      
      public var _purchaseConfirmationBtn:BaseButton;
      
      private var _numberSelecter:NumberSelecter;
      
      private var _goodsID:int;
      
      private var _isDisCount:Boolean = false;
      
      public var isSale:Boolean = false;
      
      private var _isBand:Boolean;
      
      private var _type:int;
      
      protected var _askBtn:SimpleBitmapButton;
      
      private var _shopPresentClearingFrame:ShopPresentClearingFrame;
      
      public function BuySingleGoodsView2(param1:int = 1)
      {
         super();
         this._type = param1;
         this.initView();
         this.addEvent();
      }
      
      public function get frame() : Frame
      {
         return this._frame;
      }
      
      private function initView() : void
      {
         this._frame = ComponentFactory.Instance.creatComponentByStylename("core.ddtshop.CheckOutViewFrame");
         this._frame.titleText = LanguageMgr.GetTranslation("store.view.shortcutBuy.buyBtn");
         var _loc1_:ScaleBitmapImage = ComponentFactory.Instance.creatComponentByStylename("core.ddtshop.CheckOutViewBg");
         this._frame.addToContent(_loc1_);
         this._purchaseConfirmationBtn = ComponentFactory.Instance.creatComponentByStylename("core.ddtshop.SingleGoodView.PurchaseBtn");
         this._frame.addToContent(this._purchaseConfirmationBtn);
         var _loc2_:Image = ComponentFactory.Instance.creatComponentByStylename("ddtshop.TotalMoneyPanel2");
         PositionUtils.setPos(_loc2_,"ddtshop.CheckOutViewBgPos");
         this._frame.addToContent(_loc2_);
         var _loc3_:Bitmap = ComponentFactory.Instance.creatBitmap("asset.ddtshop.PurchaseAmount");
         PositionUtils.setPos(_loc3_,"ddtshop.PurchaseAmountTextImgPos");
         this._frame.addToContent(_loc3_);
         this._numberSelecter = ComponentFactory.Instance.creatComponentByStylename("core.ddtshop.NumberSelecter");
         this._frame.addToContent(this._numberSelecter);
         this._needToPayTip = ComponentFactory.Instance.creatComponentByStylename("ddtshop.NeedToPayTip");
         this._needToPayTip.text = LanguageMgr.GetTranslation("shop.CheckOutView.NeedToPayTipText");
         PositionUtils.setPos(this._needToPayTip,"ddtshop.NeedToPayTipTextPos");
         this._frame.addToContent(this._needToPayTip);
         this._commodityPricesText1Label = ComponentFactory.Instance.creatComponentByStylename("ddtshop.CommodityPricesText1Label");
         this._commodityPricesText1Label.text = LanguageMgr.GetTranslation("shop.CheckOutView.CommodityPricesText1Label");
         this._commodityPricesText2Label = ComponentFactory.Instance.creatComponentByStylename("ddtshop.CommodityPricesText2Label");
         this._commodityPricesText2Label.text = LanguageMgr.GetTranslation("shop.CheckOutView.CommodityPricesText2Label");
         if(this._type == 3)
         {
            this._purchaseConfirmationBtn.visible = false;
            this._frame.titleText = LanguageMgr.GetTranslation("shop.ShopIIPresentView.ask");
            this._askBtn = ComponentFactory.Instance.creatComponentByStylename("core.ddtshop.askButton");
            this._askBtn.x = 120;
            this._askBtn.y = 280;
            this._askBtn.addEventListener(MouseEvent.CLICK,this.askBtnHander);
            this._frame.addToContent(this._askBtn);
         }
         else if(this._type != Price.DDT_MONEY)
         {
            this._isBand = false;
         }
         this._commodityPricesText1 = ComponentFactory.Instance.creatComponentByStylename("core.ddtshop.CommodityPricesText");
         this._commodityPricesText1.text = "0";
         PositionUtils.setPos(this._commodityPricesText1,"ddtshop.commodityPricesText1Pos");
         this._frame.addToContent(this._commodityPricesText1);
         this._commodityPricesText2 = ComponentFactory.Instance.creatComponentByStylename("core.ddtshop.CommodityPricesText");
         this._commodityPricesText2.text = "0";
         PositionUtils.setPos(this._commodityPricesText2,"ddtshop.commodityPricesText2Pos");
         this._frame.addToContent(this._commodityPricesText2);
         PositionUtils.setPos(this._commodityPricesText1Label,"ddtshop.commodityPricesText1LabelPos");
         PositionUtils.setPos(this._commodityPricesText2Label,"ddtshop.commodityPricesText2LabelPos");
         this._frame.addToContent(this._commodityPricesText1Label);
         this._frame.addToContent(this._commodityPricesText2Label);
         addChild(this._frame);
      }
      
      private function askBtnHander(param1:MouseEvent) : void
      {
         this.payPanl();
      }
      
      private function payPanl() : void
      {
         this._shopPresentClearingFrame = ComponentFactory.Instance.creatComponentByStylename("core.ddtshop.ShopPresentClearingFrame");
         this._shopPresentClearingFrame.show();
         this._shopPresentClearingFrame.setType(ShopPresentClearingFrame.FPAYTYPE_SHOP);
         this._shopPresentClearingFrame.presentBtn.addEventListener(MouseEvent.CLICK,this.presentBtnClick);
         this._shopPresentClearingFrame.addEventListener(FrameEvent.RESPONSE,this.shopPresentClearingFrameResponseHandler);
      }
      
      protected function shopPresentClearingFrameResponseHandler(param1:FrameEvent) : void
      {
         this._shopPresentClearingFrame.removeEventListener(FrameEvent.RESPONSE,this.shopPresentClearingFrameResponseHandler);
         if(this._shopPresentClearingFrame.presentBtn)
         {
            this._shopPresentClearingFrame.presentBtn.removeEventListener(MouseEvent.CLICK,this.presentBtnClick);
         }
         if(param1.responseCode == FrameEvent.CLOSE_CLICK || param1.responseCode == FrameEvent.ESC_CLICK || param1.responseCode == FrameEvent.CANCEL_CLICK)
         {
            this._shopPresentClearingFrame.dispose();
            this._shopPresentClearingFrame = null;
         }
      }
      
      protected function presentBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         var _loc2_:String = this._shopPresentClearingFrame.nameInput.text;
         if(_loc2_ == "")
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("shop.ShopIIPresentView.askPay"));
            return;
         }
         if(FilterWordManager.IsNullorEmpty(_loc2_))
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("shop.ShopIIPresentView.askSpace"));
            return;
         }
         this._shopPresentClearingFrame.presentBtn.removeEventListener(MouseEvent.CLICK,this.presentBtnClick);
         this._shopPresentClearingFrame.removeEventListener(FrameEvent.RESPONSE,this.shopPresentClearingFrameResponseHandler);
         this.sendAsk();
         this._shopPresentClearingFrame.dispose();
         this._shopPresentClearingFrame = null;
         this.dispose();
      }
      
      private function sendAsk() : void
      {
         var _loc7_:ShopCarItemInfo = null;
         var _loc1_:Array = [];
         var _loc2_:Array = [];
         var _loc3_:Array = [];
         var _loc4_:Array = [];
         var _loc5_:Array = [];
         var _loc6_:int = 0;
         while(_loc6_ < this._numberSelecter.currentValue)
         {
            _loc7_ = this._shopCartItem.shopItemInfo;
            _loc1_.push(_loc7_.GoodsID);
            _loc2_.push(_loc7_.currentBuyType);
            _loc3_.push(_loc7_.isDiscount);
            _loc4_.push(_loc7_.Color);
            _loc5_.push(_loc7_.skin);
            _loc6_++;
         }
         SocketManager.Instance.out.requestShopPay(_loc1_,_loc2_,_loc3_,_loc4_,_loc5_,this._shopPresentClearingFrame.Name,this._shopPresentClearingFrame.textArea.text);
      }
      
      public function set isDisCount(param1:Boolean) : void
      {
         this._isDisCount = param1;
      }
      
      public function set goodsID(param1:int) : void
      {
         var _loc2_:ShopItemInfo = null;
         var _loc3_:ShopCarItemInfo = null;
         if(this._shopCartItem)
         {
            this._shopCartItem.removeEventListener(ShopCartItem.CONDITION_CHANGE,this.__shopCartItemChange);
            this._shopCartItem.dispose();
         }
         this._goodsID = param1;
         if(this.isSale)
         {
            _loc2_ = ShopManager.Instance.getMoneySaleShopItemByTemplateID(this._goodsID);
         }
         else if(this._isDisCount)
         {
            _loc2_ = ShopManager.Instance.getDisCountShopItemByGoodsID(this._goodsID);
         }
         else
         {
            _loc2_ = ShopManager.Instance.getShopItemByGoodsID(this._goodsID);
         }
         if(!_loc2_)
         {
            _loc2_ = ShopManager.Instance.getGoodsByTemplateID(this._goodsID);
         }
         if(_loc2_)
         {
            _loc3_ = new ShopCarItemInfo(_loc2_.GoodsID,_loc2_.TemplateID);
            ObjectUtils.copyProperties(_loc3_,_loc2_);
            this._shopCartItem = new ShopCartItem();
            PositionUtils.setPos(this._shopCartItem,"ddtshop.shopCartItemPos");
            this._shopCartItem.closeBtn.visible = false;
            this._shopCartItem.setShopItemInfo(_loc3_);
            this._shopCartItem.setColor(_loc3_.Color);
            this._frame.addToContent(this._shopCartItem);
            this._shopCartItem.addEventListener(ShopCartItem.CONDITION_CHANGE,this.__shopCartItemChange);
            this.updateCommodityPrices();
         }
      }
      
      private function addEvent() : void
      {
         this._purchaseConfirmationBtn.addEventListener(MouseEvent.CLICK,this.__purchaseConfirmationBtnClick);
         this._numberSelecter.addEventListener(Event.CHANGE,this.__numberSelecterChange);
         this._frame.addEventListener(FrameEvent.RESPONSE,this.__framePesponse);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.BUY_GOODS,this.onBuyedGoods);
      }
      
      private function removeEvent() : void
      {
         if(this._askBtn)
         {
            this._askBtn.removeEventListener(MouseEvent.CLICK,this.askBtnHander);
         }
         if(this._purchaseConfirmationBtn)
         {
            this._purchaseConfirmationBtn.removeEventListener(MouseEvent.CLICK,this.__purchaseConfirmationBtnClick);
         }
         if(this._numberSelecter)
         {
            this._numberSelecter.removeEventListener(Event.CHANGE,this.__numberSelecterChange);
         }
         if(this._shopCartItem)
         {
            this._shopCartItem.removeEventListener(ShopCartItem.CONDITION_CHANGE,this.__shopCartItemChange);
         }
         if(this._frame)
         {
            this._frame.removeEventListener(FrameEvent.RESPONSE,this.__framePesponse);
         }
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.BUY_GOODS,this.onBuyedGoods);
      }
      
      private function updateCommodityPrices() : void
      {
         if(this._shopCartItem.shopItemInfo.getCurrentPrice().PriceType == Price.HARD_CURRENCY)
         {
            this._commodityPricesText1.text = (this._shopCartItem.shopItemInfo.getCurrentPrice().moneyValue * this._numberSelecter.currentValue).toString();
            this._commodityPricesText2.text = (this._shopCartItem.shopItemInfo.getCurrentPrice().hardCurrencyValue * this._numberSelecter.currentValue).toString();
            this._commodityPricesText2Label.text = Price.HARD_CURRENCY_TO_STRING;
         }
         else if(this._isBand)
         {
            this._commodityPricesText1.text = (this._shopCartItem.shopItemInfo.getCurrentPrice().bandDdtMoneyValue * this._numberSelecter.currentValue).toString();
            this._commodityPricesText2.text = (this._shopCartItem.shopItemInfo.getCurrentPrice().moneyValue * this._numberSelecter.currentValue).toString();
         }
         else
         {
            this._commodityPricesText1.text = (this._shopCartItem.shopItemInfo.getCurrentPrice().moneyValue * this._numberSelecter.currentValue).toString();
            this._commodityPricesText2.text = (this._shopCartItem.shopItemInfo.getCurrentPrice().bandDdtMoneyValue * this._numberSelecter.currentValue).toString();
         }
      }
      
      protected function __purchaseConfirmationBtnClick(param1:MouseEvent) : void
      {
         var _loc11_:ShopCarItemInfo = null;
         SoundManager.instance.play("008");
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         var _loc2_:int = this._shopCartItem.shopItemInfo.getCurrentPrice().moneyValue;
         if(this._isBand && PlayerManager.Instance.Self.BandMoney < _loc2_)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("store.view.transfer.StoreIITransferBG.lijinbuzu"));
            return;
         }
         if(!this._isBand && PlayerManager.Instance.Self.Money < _loc2_)
         {
            LeavePageManager.showFillFrame();
            return;
         }
         if(this._shopCartItem.shopItemInfo.ShopID == ShopType.SALE_SHOP && ShopSaleManager.Instance.goodsBuyMaxNum > 0)
         {
            if(this._numberSelecter.currentValue > ShopSaleManager.Instance.goodsBuyMaxNum)
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("asset.ddtshop.notBuyMaxNum",ShopSaleManager.Instance.goodsBuyMaxNum));
               return;
            }
         }
         this._purchaseConfirmationBtn.enable = false;
         var _loc3_:Array = new Array();
         var _loc4_:Array = new Array();
         var _loc5_:Array = new Array();
         var _loc6_:Array = new Array();
         var _loc7_:Array = new Array();
         var _loc8_:Array = [];
         var _loc9_:Array = [];
         var _loc10_:int = 0;
         while(_loc10_ < this._numberSelecter.currentValue)
         {
            _loc11_ = this._shopCartItem.shopItemInfo;
            _loc3_.push(_loc11_.GoodsID);
            _loc4_.push(_loc11_.currentBuyType);
            _loc5_.push("");
            _loc6_.push("");
            _loc7_.push("");
            _loc8_.push(_loc11_.isDiscount);
            _loc9_.push(this._isBand);
            _loc10_++;
         }
         SocketManager.Instance.out.sendBuyGoods(_loc3_,_loc4_,_loc5_,_loc7_,_loc6_,null,0,_loc8_,_loc9_);
      }
      
      protected function onBuyedGoods(param1:CrazyTankSocketEvent) : void
      {
         this._purchaseConfirmationBtn.enable = true;
         param1.pkg.position = SocketManager.PACKAGE_CONTENT_START_INDEX;
         var _loc2_:int = param1.pkg.readInt();
         if(_loc2_ != 0)
         {
         }
      }
      
      protected function __numberSelecterChange(param1:Event) : void
      {
         SoundManager.instance.play("008");
         this.updateCommodityPrices();
      }
      
      protected function __shopCartItemChange(param1:Event) : void
      {
         this.updateCommodityPrices();
      }
      
      protected function __framePesponse(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         switch(param1.responseCode)
         {
            case FrameEvent.CLOSE_CLICK:
            case FrameEvent.ESC_CLICK:
               this.dispose();
         }
      }
      
      public function dispose() : void
      {
         this.removeEvent();
         if(this._frame)
         {
            ObjectUtils.disposeObject(this._frame);
         }
         this._frame = null;
         ObjectUtils.disposeObject(this._askBtn);
         this._askBtn = null;
         if(this._shopCartItem)
         {
            ObjectUtils.disposeObject(this._shopCartItem);
         }
         this._shopCartItem = null;
         if(this._needToPayTip)
         {
            ObjectUtils.disposeObject(this._needToPayTip);
         }
         this._needToPayTip = null;
         if(this._commodityPricesText1)
         {
            ObjectUtils.disposeObject(this._commodityPricesText1);
         }
         this._commodityPricesText1 = null;
         if(this._commodityPricesText2)
         {
            ObjectUtils.disposeObject(this._commodityPricesText2);
         }
         this._commodityPricesText2 = null;
         if(this._purchaseConfirmationBtn)
         {
            ObjectUtils.disposeObject(this._purchaseConfirmationBtn);
         }
         this._purchaseConfirmationBtn = null;
         if(this._numberSelecter)
         {
            ObjectUtils.disposeObject(this._numberSelecter);
         }
         this._numberSelecter = null;
         if(this._commodityPricesText1Label)
         {
            ObjectUtils.disposeObject(this._commodityPricesText1Label);
         }
         this._commodityPricesText1Label = null;
         if(this._commodityPricesText2Label)
         {
            ObjectUtils.disposeObject(this._commodityPricesText2Label);
         }
         this._commodityPricesText2Label = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
