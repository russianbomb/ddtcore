package shop.view
{
   import baglocked.BaglockedManager;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.toplevel.StageReferance;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.ScrollPanel;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.controls.container.VBox;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.image.Image;
   import com.pickgliss.ui.image.Scale9CornerImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.ShopCarItemInfo;
   import ddt.data.goods.ShopItemInfo;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.LanguageMgr;
   import ddt.manager.LeavePageManager;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.ShopManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.utils.FilterWordManager;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import shop.ShopController;
   import shop.ShopEvent;
   import shop.ShopModel;
   import shop.manager.ShopBuyManager;
   
   public class ShopCheckOutView extends Sprite implements Disposeable
   {
      
      public static const COUNT:uint = 3;
      
      public static const DDT_MONEY:uint = 1;
      
      public static const BAND_MONEY:uint = 2;
      
      public static const LACK:uint = 1;
      
      public static const MONEY:uint = 0;
      
      public static const PLAYER:uint = 0;
      
      public static const PRESENT:int = 2;
      
      public static const PURCHASE:int = 1;
      
      public static const SAVE:int = 3;
      
      public static const ASKTYPE:int = 4;
       
      
      protected var _commodityNumberText:FilterFrameText;
      
      protected var _commodityNumberTip:FilterFrameText;
      
      protected var _commodityPricesText1:FilterFrameText;
      
      protected var _commodityPricesText2:FilterFrameText;
      
      private var _commodityPricesText1Bg:Scale9CornerImage;
      
      private var _commodityPricesText2Bg:Scale9CornerImage;
      
      private var _commodityPricesText3Bg:Scale9CornerImage;
      
      protected var _commodityPricesText1Label:FilterFrameText;
      
      protected var _commodityPricesText2Label:FilterFrameText;
      
      protected var _needToPayTip:FilterFrameText;
      
      protected var _purchaseConfirmationBtn:BaseButton;
      
      protected var _giftsBtn:BaseButton;
      
      protected var _askBtn:SimpleBitmapButton;
      
      protected var _saveImageBtn:BaseButton;
      
      private var _buyArray:Array;
      
      protected var _cartList:VBox;
      
      private var _castList2:Sprite;
      
      private var _cartItemList:Vector.<ShopCartItem>;
      
      private var _cartScroll:ScrollPanel;
      
      private var _controller:ShopController;
      
      protected var _frame:Frame;
      
      private var _giveArray:Array;
      
      protected var _innerBg1:Image;
      
      private var _innerBg:Image;
      
      protected var _model:ShopModel;
      
      protected var _tempList:Array;
      
      protected var _type:int;
      
      private var _isDisposed:Boolean;
      
      private var shopPresent:ShopPresentView;
      
      protected var _list:Array;
      
      private var _bandMoneyTotal:int;
      
      private var _MoneyTotal:int;
      
      private var _shopPresentClearingFrame:ShopPresentClearingFrame;
      
      private var _isAsk:Boolean;
      
      public function ShopCheckOutView()
      {
         this._buyArray = new Array();
         this._giveArray = new Array();
         super();
      }
      
      protected function drawFrame() : void
      {
         this._frame = ComponentFactory.Instance.creatComponentByStylename("ddtshop.CheckOutViewFrame");
         this._frame.titleText = LanguageMgr.GetTranslation("shop.Shop.car");
         addChild(this._frame);
      }
      
      protected function drawItemCountField() : void
      {
         this._innerBg1 = ComponentFactory.Instance.creatComponentByStylename("ddtshop.TotalMoneyPanel");
         this._frame.addToContent(this._innerBg1);
         this._commodityNumberTip = ComponentFactory.Instance.creatComponentByStylename("ddtshop.CommodityNumberTipText");
         this._commodityNumberTip.text = LanguageMgr.GetTranslation("shop.CheckOutView.CommodityNumberTip");
         this._frame.addToContent(this._commodityNumberTip);
         this._commodityNumberText = ComponentFactory.Instance.creatComponentByStylename("ddtshop.CommodityNumberText");
         this._frame.addToContent(this._commodityNumberText);
         this._needToPayTip = ComponentFactory.Instance.creatComponentByStylename("ddtshop.NeedToPayTip");
         this._needToPayTip.text = LanguageMgr.GetTranslation("shop.CheckOutView.NeedToPayTipText");
         this._frame.addToContent(this._needToPayTip);
         this._commodityPricesText1 = ComponentFactory.Instance.creatComponentByStylename("ddtshop.CommodityPricesText1");
         this._commodityPricesText2 = ComponentFactory.Instance.creatComponentByStylename("ddtshop.CommodityPricesText2");
         this._commodityPricesText1Label = ComponentFactory.Instance.creatComponentByStylename("ddtshop.CommodityPricesText1Label");
         this._commodityPricesText1Label.text = LanguageMgr.GetTranslation("shop.CheckOutView.CommodityPricesText1Label");
         this._commodityPricesText2Label = ComponentFactory.Instance.creatComponentByStylename("ddtshop.CommodityPricesText2Label");
         this._commodityPricesText2Label.text = LanguageMgr.GetTranslation("shop.CheckOutView.CommodityPricesText2Label");
         this._frame.addToContent(this._commodityPricesText1Label);
         this._frame.addToContent(this._commodityPricesText2Label);
         this._frame.addToContent(this._commodityPricesText1);
         this._frame.addToContent(this._commodityPricesText2);
      }
      
      protected function drawPayListField() : void
      {
         this._innerBg = ComponentFactory.Instance.creatComponentByStylename("ddtshop.CheckOutViewBg");
         this._frame.addToContent(this._innerBg);
      }
      
      protected function init() : void
      {
         this._cartList = new VBox();
         this._castList2 = new Sprite();
         this.drawFrame();
         this._purchaseConfirmationBtn = ComponentFactory.Instance.creatComponentByStylename("ddtshop.PurchaseBtn");
         this._purchaseConfirmationBtn.visible = false;
         this._askBtn = ComponentFactory.Instance.creatComponentByStylename("core.ddtshop.askButton");
         this._askBtn.visible = false;
         this._giftsBtn = ComponentFactory.Instance.creatComponentByStylename("ddtshop.GiftsBtn");
         this._giftsBtn.visible = false;
         this._saveImageBtn = ComponentFactory.Instance.creatComponentByStylename("ddtshop.SaveImageBtn");
         this._saveImageBtn.visible = false;
         this._cartScroll = ComponentFactory.Instance.creatComponentByStylename("ddtshop.CheckOutViewItemList");
         this._cartScroll.setView(this._castList2);
         this._cartScroll.vScrollProxy = ScrollPanel.ON;
         this._cartList.spacing = 5;
         this._cartList.strictSize = 80;
         this._cartList.isReverAdd = true;
         this.drawItemCountField();
         this.drawPayListField();
         this._frame.addToContent(this._cartScroll);
         this._frame.addToContent(this._askBtn);
         this._frame.addToContent(this._purchaseConfirmationBtn);
         this._frame.addToContent(this._giftsBtn);
         this._frame.addToContent(this._saveImageBtn);
         this.setList(this._tempList,0,true);
         this.updateTxt();
         if(this._type == SAVE)
         {
            this._saveImageBtn.visible = true;
         }
         else if(this._type == PURCHASE)
         {
            this._purchaseConfirmationBtn.visible = true;
         }
         else if(this._type == PRESENT)
         {
            this._giftsBtn.visible = true;
         }
         else if(this._type == ASKTYPE)
         {
            this._askBtn.visible = true;
         }
      }
      
      private function clearList() : void
      {
         var _loc1_:ShopCartItem = null;
         while(this._castList2.numChildren > 0)
         {
            _loc1_ = this._castList2.getChildAt(this._castList2.numChildren - 1) as ShopCartItem;
            this.removeItemEvent(_loc1_);
            this._castList2.removeChild(_loc1_);
            _loc1_.dispose();
            _loc1_ = null;
         }
      }
      
      protected function initEvent() : void
      {
         this._frame.addEventListener(FrameEvent.RESPONSE,this.__frameEventHandler);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.BUY_GOODS,this.onBuyedGoods);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GOODS_PRESENT,this.onPresent);
         this._purchaseConfirmationBtn.addEventListener(MouseEvent.CLICK,this.__purchaseConfirmationBtnClick);
         this._saveImageBtn.addEventListener(MouseEvent.CLICK,this.__purchaseConfirmationBtnClick);
         this._giftsBtn.addEventListener(MouseEvent.CLICK,this.__purchaseConfirmationBtnClick);
         this._askBtn.addEventListener(MouseEvent.CLICK,this.__purchaseConfirmationBtnClick);
      }
      
      protected function __purchaseConfirmationBtnClick(param1:MouseEvent = null) : void
      {
         SoundManager.instance.play("008");
         var _loc2_:int = int(this._commodityPricesText1.text);
         var _loc3_:int = int(this._commodityPricesText2.text);
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         if(this._type == ASKTYPE)
         {
            this.sendAsk();
         }
         else if(this._type == SAVE)
         {
            if(_loc3_ > PlayerManager.Instance.Self.BandMoney)
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("store.view.transfer.StoreIITransferBG.lijinbuzu"));
               return;
            }
            if(_loc2_ > PlayerManager.Instance.Self.Money)
            {
               LeavePageManager.showFillFrame();
               return;
            }
            this.saveFigureCheckOut();
         }
         else if(this._type == PURCHASE)
         {
            if(_loc3_ > PlayerManager.Instance.Self.BandMoney)
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("store.view.transfer.StoreIITransferBG.lijinbuzu"));
               return;
            }
            if(_loc2_ > PlayerManager.Instance.Self.Money)
            {
               LeavePageManager.showFillFrame();
               return;
            }
            this.shopCarCheckOut();
         }
         else if(this._type == PRESENT)
         {
            if(_loc2_ > PlayerManager.Instance.Self.Money)
            {
               LeavePageManager.showFillFrame();
               this.dispose();
               return;
            }
            this.presentCheckOut();
         }
      }
      
      private function sendAsk() : void
      {
         this._isAsk = true;
         if(this._shopPresentClearingFrame)
         {
            this._shopPresentClearingFrame.dispose();
            this._shopPresentClearingFrame = null;
         }
         var _loc1_:Array = this._model.allItems;
         if(_loc1_.length > 0)
         {
            this._shopPresentClearingFrame = ComponentFactory.Instance.creatComponentByStylename("core.ddtshop.ShopPresentClearingFrame");
            this._shopPresentClearingFrame.show();
            this._shopPresentClearingFrame.setType(ShopPresentClearingFrame.FPAYTYPE_SHOP);
            this._shopPresentClearingFrame.presentBtn.addEventListener(MouseEvent.CLICK,this.__presentBtnClick);
            this._shopPresentClearingFrame.addEventListener(FrameEvent.RESPONSE,this.__shopPresentClearingFrameResponseHandler);
            this.visible = false;
         }
         else
         {
            LeavePageManager.showFillFrame();
         }
      }
      
      private function seleBand(param1:int, param2:int, param3:Boolean) : void
      {
         this._model.isBandList[param1] = param3;
         this.updateTxt();
      }
      
      protected function addItemEvent(param1:ShopCartItem) : void
      {
         param1.addEventListener(ShopCartItem.DELETE_ITEM,this.__deleteItem);
         param1.addEventListener(ShopCartItem.ADD_LENGTH,this.addLength);
         param1.addEventListener(ShopCartItem.CONDITION_CHANGE,this.__conditionChange);
      }
      
      protected function addLength(param1:Event) : void
      {
         var _loc2_:ShopCartItem = param1.currentTarget as ShopCartItem;
         ShopBuyManager.crrItemId = _loc2_.id;
         this.setList(this._tempList,_loc2_.id);
      }
      
      protected function removeItemEvent(param1:ShopCartItem) : void
      {
         param1.removeEventListener(ShopCartItem.DELETE_ITEM,this.__deleteItem);
         param1.removeEventListener(ShopCartItem.CONDITION_CHANGE,this.__conditionChange);
      }
      
      public function setList(param1:Array, param2:int = 0, param3:Boolean = false) : void
      {
         var _loc6_:ShopCartItem = null;
         this.clearList();
         this._cartItemList = new Vector.<ShopCartItem>();
         this._list = param1;
         if(param3)
         {
            this._model.isBandList = [];
         }
         var _loc4_:int = param1.length;
         var _loc5_:int = 0;
         while(_loc5_ < _loc4_)
         {
            _loc6_ = this.createShopItem();
            _loc6_.id = _loc5_;
            if(this._type == PRESENT || this._type == ASKTYPE)
            {
               _loc6_.setShopItemInfo(param1[_loc5_]);
               _loc6_.type = PRESENT;
            }
            else if(ShopBuyManager.crrItemId != 0)
            {
               _loc6_.setShopItemInfo(param1[_loc5_],ShopBuyManager.crrItemId,this._model.isBandList[_loc5_]);
            }
            else
            {
               _loc6_.setShopItemInfo(param1[_loc5_],param2,this._model.isBandList[_loc5_]);
            }
            _loc6_.seleBand = this.seleBand;
            _loc6_.upDataBtnState = this.upDataBtnState;
            _loc6_.setColor(param1[_loc5_].Color);
            this._castList2.addChild(_loc6_);
            if(param3)
            {
               this._model.isBandList.push(_loc6_.isBand);
            }
            else
            {
               _loc6_.setDianquanType(this._model.isBandList[_loc5_]);
            }
            this._cartItemList.push(_loc6_);
            this.addItemEvent(_loc6_);
            _loc5_++;
         }
         this.updateList();
         this._cartScroll.invalidateViewport();
         this.updateTxt();
      }
      
      private function upDataBtnState() : void
      {
         this._model.dispatchEvent(new ShopEvent(ShopEvent.COST_UPDATE));
      }
      
      private function updateList() : void
      {
         var _loc1_:int = this._cartItemList.length;
         var _loc2_:int = 0;
         while(_loc2_ < _loc1_)
         {
            this._cartItemList[_loc2_].id = _loc2_;
            if(_loc2_ > 0)
            {
               this._cartItemList[_loc2_].y = this._cartItemList[_loc2_ - 1].y + this._cartItemList[_loc2_ - 1].height;
            }
            else
            {
               this._cartItemList[_loc2_].y = 0;
            }
            _loc2_++;
         }
      }
      
      protected function createShopItem() : ShopCartItem
      {
         return new ShopCartItem();
      }
      
      public function setup(param1:ShopController, param2:ShopModel, param3:Array, param4:int) : void
      {
         this._controller = param1;
         this._model = param2;
         this._tempList = param3;
         this._type = param4;
         this._isDisposed = false;
         this.visible = true;
         this.init();
         this.initEvent();
      }
      
      private function __conditionChange(param1:Event) : void
      {
         this.updateTxt();
      }
      
      private function upDataListInfo(param1:int) : void
      {
         var _loc2_:int = this._cartItemList.length;
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_)
         {
            if(param1 == this._cartItemList[_loc3_].id)
            {
               this._cartItemList.splice(_loc3_,1);
               this._tempList.splice(_loc3_,1);
               if(param1 == ShopBuyManager.crrItemId)
               {
                  ShopBuyManager.crrItemId = 0;
                  if(this._cartItemList.length > 0 && this._type != PRESENT)
                  {
                     this._cartItemList[0].addItem(this._model.isBandList[0]);
                  }
               }
               break;
            }
            _loc3_++;
         }
      }
      
      private function __deleteItem(param1:Event) : void
      {
         var _loc2_:ShopCartItem = param1.currentTarget as ShopCartItem;
         var _loc3_:ShopCarItemInfo = _loc2_.shopItemInfo;
         _loc2_.removeEventListener(ShopCartItem.DELETE_ITEM,this.__deleteItem);
         _loc2_.removeEventListener(ShopCartItem.CONDITION_CHANGE,this.__conditionChange);
         this._model.isBandList.splice(_loc2_.id,1);
         this._castList2.removeChild(_loc2_);
         if(ShopBuyManager.crrItemId > _loc2_.id)
         {
            ShopBuyManager.crrItemId--;
         }
         _loc2_.dispose();
         if(this._type == SAVE)
         {
            this._controller.removeTempEquip(_loc3_);
            this.updateTxt();
            this.updateList();
            this._cartScroll.invalidateViewport();
            if(this._model.currentTempList.length == 0)
            {
               this.dispose();
            }
         }
         if(this._type == PURCHASE || this._type == ASKTYPE)
         {
            this._controller.removeFromCar(_loc3_);
            this.upDataListInfo(_loc2_.id);
            this.updateTxt();
            this.updateList();
            this._cartScroll.invalidateViewport();
            if(this._model.allItems.length == 0)
            {
               this.dispose();
            }
         }
         if(this._type == PRESENT)
         {
            this._controller.removeFromCar(_loc3_);
            this.upDataListInfo(_loc2_.id);
            this.updateTxt();
            this.updateList();
            this._cartScroll.invalidateViewport();
            if(this._tempList.length == 0)
            {
               this.dispose();
            }
         }
      }
      
      private function __frameEventHandler(param1:FrameEvent) : void
      {
         switch(param1.responseCode)
         {
            case FrameEvent.CLOSE_CLICK:
            case FrameEvent.ESC_CLICK:
               SoundManager.instance.play("008");
               this.dispose();
         }
      }
      
      public function get extraButton() : BaseButton
      {
         return null;
      }
      
      protected function removeEvent() : void
      {
         this._frame.removeEventListener(FrameEvent.RESPONSE,this.__frameEventHandler);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.BUY_GOODS,this.onBuyedGoods);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.GOODS_PRESENT,this.onPresent);
         this._purchaseConfirmationBtn.removeEventListener(MouseEvent.CLICK,this.__purchaseConfirmationBtnClick);
         this._saveImageBtn.removeEventListener(MouseEvent.CLICK,this.__purchaseConfirmationBtnClick);
         this._giftsBtn.removeEventListener(MouseEvent.CLICK,this.__purchaseConfirmationBtnClick);
      }
      
      private function __dispatchFrameEvent(param1:MouseEvent) : void
      {
         this._frame.dispatchEvent(new FrameEvent(FrameEvent.SUBMIT_CLICK));
      }
      
      private function isMoneyGoods(param1:*, param2:int, param3:Array) : Boolean
      {
         if(param1 is ShopItemInfo)
         {
            return ShopItemInfo(param1).getItemPrice(1).IsMoneyType;
         }
         return false;
      }
      
      private function notPresentGoods() : Array
      {
         var _loc2_:ShopCarItemInfo = null;
         var _loc1_:Array = [];
         for each(_loc2_ in this._tempList)
         {
            if(this._giveArray.indexOf(_loc2_) == -1)
            {
               _loc1_.push(_loc2_);
            }
         }
         return _loc1_;
      }
      
      private function onBuyedGoods(param1:CrazyTankSocketEvent) : void
      {
         var _loc4_:ShopCarItemInfo = null;
         var _loc5_:int = 0;
         param1.pkg.position = SocketManager.PACKAGE_CONTENT_START_INDEX;
         var _loc2_:int = param1.pkg.readInt();
         var _loc3_:Boolean = false;
         if(_loc2_ != 0)
         {
            if(this._type == SAVE)
            {
               this._model.clearCurrentTempList(!!this._model.fittingSex?int(1):int(2));
            }
            else if(this._type == PURCHASE)
            {
               this._model.clearAllitems();
            }
         }
         else if(this._type == SAVE)
         {
            this._model.clearCurrentTempList(!!this._model.fittingSex?int(1):int(2));
            for each(_loc4_ in this._model.currentLeftList)
            {
               this._model.addTempEquip(_loc4_);
            }
            this.setList(this._model.currentTempList);
            if(this._model.currentTempList.length < 1)
            {
               _loc3_ = true;
            }
         }
         else if(this._type == PURCHASE)
         {
            _loc5_ = 0;
            while(_loc5_ < this._buyArray.length)
            {
               this._model.removeFromShoppingCar(this._buyArray[_loc5_] as ShopCarItemInfo);
               _loc5_++;
            }
            this.setList(this._model.allItems);
            if(this._model.allItems.length < 1)
            {
               _loc3_ = true;
            }
         }
         if(_loc2_ != 0)
         {
            this.dispose();
         }
         else if(_loc3_)
         {
            this.dispose();
         }
      }
      
      private function onPresent(param1:CrazyTankSocketEvent) : void
      {
         this._shopPresentClearingFrame.presentBtn.enable = true;
         this._shopPresentClearingFrame.dispose();
         this._shopPresentClearingFrame = null;
         this.visible = true;
         var _loc2_:Boolean = param1.pkg.readBoolean();
         var _loc3_:int = 0;
         while(_loc3_ < this._giveArray.length)
         {
            this._model.removeFromShoppingCar(this._giveArray[_loc3_] as ShopCarItemInfo);
            this._tempList.splice(this._tempList.indexOf(this._giveArray[_loc3_] as ShopCarItemInfo),1);
            _loc3_++;
         }
         if(this._tempList.length == 0)
         {
            this.dispose();
            return;
         }
         if(this._tempList.length > 0)
         {
            this.setList(this.notPresentGoods());
            return;
         }
      }
      
      private function presentCheckOut() : void
      {
         this._isAsk = false;
         if(this._shopPresentClearingFrame)
         {
            this._shopPresentClearingFrame.dispose();
            this._shopPresentClearingFrame = null;
         }
         this._giveArray = ShopManager.Instance.giveGift(this._model.allItems,this._model.Self);
         if(this._giveArray.length > 0)
         {
            this._shopPresentClearingFrame = ComponentFactory.Instance.creatComponentByStylename("core.ddtshop.ShopPresentClearingFrame");
            this._shopPresentClearingFrame.show();
            this._shopPresentClearingFrame.presentBtn.addEventListener(MouseEvent.CLICK,this.__presentBtnClick);
            this._shopPresentClearingFrame.addEventListener(FrameEvent.RESPONSE,this.__shopPresentClearingFrameResponseHandler);
            this.visible = false;
         }
         else
         {
            LeavePageManager.showFillFrame();
         }
      }
      
      private function __shopPresentClearingFrameResponseHandler(param1:FrameEvent) : void
      {
         this._shopPresentClearingFrame.removeEventListener(FrameEvent.RESPONSE,this.__shopPresentClearingFrameResponseHandler);
         if(param1.responseCode == FrameEvent.CLOSE_CLICK || param1.responseCode == FrameEvent.ESC_CLICK || param1.responseCode == FrameEvent.CANCEL_CLICK)
         {
            StageReferance.stage.focus = this._frame;
            this.visible = true;
         }
      }
      
      protected function __presentBtnClick(param1:MouseEvent) : void
      {
         var _loc2_:int = 0;
         var _loc3_:Array = null;
         var _loc4_:Array = null;
         var _loc5_:Array = null;
         var _loc6_:Array = null;
         var _loc7_:Array = null;
         var _loc8_:int = 0;
         SoundManager.instance.play("008");
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         if(this._shopPresentClearingFrame.nameInput.text == "")
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("shop.ShopIIPresentView.give"));
            return;
         }
         if(FilterWordManager.IsNullorEmpty(this._shopPresentClearingFrame.nameInput.text))
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("shop.ShopIIPresentView.space"));
            return;
         }
         if(this._isAsk)
         {
            this._shopPresentClearingFrame.presentBtn.removeEventListener(MouseEvent.CLICK,this.__presentBtnClick);
            this._shopPresentClearingFrame.removeEventListener(FrameEvent.RESPONSE,this.__shopPresentClearingFrameResponseHandler);
            _loc2_ = this._cartItemList.length;
            _loc3_ = [];
            _loc4_ = [];
            _loc5_ = [];
            _loc6_ = [];
            _loc7_ = [];
            _loc8_ = 0;
            while(_loc8_ < _loc2_)
            {
               _loc3_.push(this._cartItemList[_loc8_].shopItemInfo.GoodsID);
               _loc4_.push(this._cartItemList[_loc8_].shopItemInfo.currentBuyType);
               _loc5_.push(this._cartItemList[_loc8_].shopItemInfo.isDiscount);
               _loc6_.push(this._cartItemList[_loc8_].shopItemInfo.Color);
               _loc7_.push(this._cartItemList[_loc8_].shopItemInfo.skin);
               _loc8_++;
            }
            SocketManager.Instance.out.requestShopPay(_loc3_,_loc4_,_loc5_,_loc6_,_loc7_,this._shopPresentClearingFrame.Name);
         }
         else
         {
            this._shopPresentClearingFrame.presentBtn.enable = false;
            this._controller.presentItems(this._tempList,this._shopPresentClearingFrame.textArea.text,this._shopPresentClearingFrame.nameInput.text);
         }
         this._shopPresentClearingFrame.dispose();
         this._shopPresentClearingFrame = null;
         this.dispose();
      }
      
      private function saveFigureCheckOut() : void
      {
         this._buyArray = ShopManager.Instance.buyIt(this._model.currentTempList);
         this._controller.buyItems(this._model.currentTempList,true,this._model.currentModel.Skin,this._model.isBandList);
      }
      
      private function shopCarCheckOut() : void
      {
         this._buyArray = ShopManager.Instance.buyIt(this._model.allItems);
         this._controller.buyItems(this._model.allItems,false,"",this._model.isBandList);
      }
      
      protected function updateTxt() : void
      {
         var _loc1_:Array = this._type == SAVE?this._model.currentTempList:this._model.allItems;
         if(this._type == PRESENT)
         {
            _loc1_ = this._tempList;
         }
         var _loc2_:Array = this._model.calcPrices(_loc1_,this._model.isBandList);
         this._commodityNumberText.text = String(_loc1_.length);
         this._commodityPricesText1.text = String(_loc2_[MONEY + 1]);
         this._commodityPricesText2.text = String(_loc2_[BAND_MONEY + 1]);
      }
      
      public function dispose() : void
      {
         var _loc1_:ShopCartItem = null;
         if(this._shopPresentClearingFrame)
         {
            if(this._shopPresentClearingFrame.presentBtn)
            {
               this._shopPresentClearingFrame.presentBtn.removeEventListener(MouseEvent.CLICK,this.__presentBtnClick);
            }
            this._shopPresentClearingFrame.removeEventListener(FrameEvent.RESPONSE,this.__shopPresentClearingFrameResponseHandler);
            this._shopPresentClearingFrame.dispose();
            this._shopPresentClearingFrame = null;
         }
         this._model.isBandList = [];
         if(!this._isDisposed)
         {
            this.removeEvent();
            ObjectUtils.disposeAllChildren(this);
            while(this._cartList.numChildren > 0)
            {
               _loc1_ = this._cartList.getChildAt(this._cartList.numChildren - 1) as ShopCartItem;
               this.removeItemEvent(_loc1_);
               this._cartList.removeChild(_loc1_);
               _loc1_.dispose();
               _loc1_ = null;
            }
            ObjectUtils.disposeObject(this._needToPayTip);
            this._needToPayTip = null;
            this._buyArray = null;
            this._cartList = null;
            this._cartScroll = null;
            this._controller = null;
            this._giveArray = null;
            this._innerBg = null;
            this._frame = null;
            this.shopPresent = null;
            this._commodityNumberText = null;
            this._commodityPricesText1 = null;
            this._commodityNumberTip = null;
            this._commodityPricesText2 = null;
            this._commodityPricesText1Bg = null;
            this._commodityPricesText2Bg = null;
            this._commodityPricesText3Bg = null;
            this._commodityPricesText1Label = null;
            this._commodityPricesText2Label = null;
            this._purchaseConfirmationBtn = null;
            this._giftsBtn = null;
            this._saveImageBtn = null;
            this._innerBg1 = null;
            this._innerBg = null;
            this._model = null;
            if(parent)
            {
               parent.removeChild(this);
            }
            this._isDisposed = true;
         }
      }
   }
}
