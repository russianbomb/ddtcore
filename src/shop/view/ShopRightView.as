package shop.view
{
   import com.greensock.TimelineLite;
   import com.greensock.TweenLite;
   import com.greensock.TweenProxy;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.ISelectable;
   import com.pickgliss.ui.controls.SelectedButton;
   import com.pickgliss.ui.controls.SelectedButtonGroup;
   import com.pickgliss.ui.controls.SelectedCheckButton;
   import com.pickgliss.ui.controls.SelectedTextButton;
   import com.pickgliss.ui.controls.container.HBox;
   import com.pickgliss.ui.controls.container.VBox;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.image.Image;
   import com.pickgliss.ui.image.Scale9CornerImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.DisplayUtils;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.EquipType;
   import ddt.data.ShopType;
   import ddt.data.goods.ShopCarItemInfo;
   import ddt.data.goods.ShopItemInfo;
   import ddt.events.ItemEvent;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.ShopManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.DisplayObject;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.geom.Point;
   import shop.ShopController;
   import shop.ShopEvent;
   import trainer.data.ArrowType;
   import trainer.data.Step;
   import trainer.view.NewHandContainer;
   
   public class ShopRightView extends Sprite implements Disposeable
   {
      
      public static const TOP_RECOMMEND:uint = 0;
      
      public static const SHOP_ITEM_NUM:uint = 8;
      
      public static var CURRENT_GENDER:int = -1;
      
      public static var CURRENT_MONEY_TYPE:int = 1;
      
      public static var CURRENT_PAGE:int = 1;
      
      public static var TOP_TYPE:int = 0;
      
      public static var SUB_TYPE:int = 2;
      
      public static const SHOW_LIGHT:String = "SHOW_LIGHT";
      
      private static var isDiscountType:Boolean = false;
       
      
      private var _bg:Image;
      
      private var _bg1:Bitmap;
      
      private var _controller:ShopController;
      
      private var _currentPageTxt:FilterFrameText;
      
      private var _currentPageInput:Scale9CornerImage;
      
      private var _femaleBtn:SelectedButton;
      
      private var _genderContainer:VBox;
      
      private var _genderGroup:SelectedButtonGroup;
      
      private var _rightViewTitleBg:Bitmap;
      
      private var _goodItemContainerAll:Sprite;
      
      private var _goodItemContainerBg:Image;
      
      private var _goodItemContainerTwoLine:Image;
      
      private var _goodItemGroup:SelectedButtonGroup;
      
      private var _goodItems:Vector.<ShopGoodItem>;
      
      private var _maleBtn:SelectedButton;
      
      private var _firstPage:BaseButton;
      
      private var _prePageBtn:BaseButton;
      
      private var _nextPageBtn:BaseButton;
      
      private var _endPageBtn:BaseButton;
      
      private var _subBtns:Vector.<SelectedTextButton>;
      
      private var _subBtnsContainers:Vector.<HBox>;
      
      private var _subBtnsGroups:Vector.<SelectedButtonGroup>;
      
      private var _currentSubBtnContainerIndex:int;
      
      private var _topBtns:Vector.<SelectedTextButton>;
      
      private var _topBtnsContainer:HBox;
      
      private var _topBtnsGroup:SelectedButtonGroup;
      
      private var _shopSearchBox:Sprite;
      
      private var _shopSearchEndBtnBg:Bitmap;
      
      private var _shopSearchColseBtn:BaseButton;
      
      private var _rightItemLightMc:MovieClip;
      
      private var _shopMoneySelectedCkBtn:SelectedCheckButton;
      
      private var _shopDDTMoneySelectedCkBtn:SelectedCheckButton;
      
      private var _shopMoneyGroup:SelectedButtonGroup;
      
      private var _moneyBg:Bitmap;
      
      private var _moneySeperateLine:Image;
      
      private var _tempTopType:int = -1;
      
      private var _tempCurrentPage:int = -1;
      
      private var _tempSubBtnHBox:HBox;
      
      private var _isSearch:Boolean;
      
      private var _searchShopItemList:Vector.<ShopItemInfo>;
      
      private var _searchItemTotalPage:int;
      
      public function ShopRightView()
      {
         super();
      }
      
      public function get genderGroup() : SelectedButtonGroup
      {
         return this._genderGroup;
      }
      
      public function setup(param1:ShopController) : void
      {
         this._controller = param1;
         this.init();
      }
      
      private function init() : void
      {
         this._bg = ComponentFactory.Instance.creat("ddtshop.RightViewBg");
         addChild(this._bg);
         this._rightViewTitleBg = ComponentFactory.Instance.creatBitmap("asset.ddtshop.RightViewTitleBg");
         addChild(this._rightViewTitleBg);
         this.initBtns();
         this.initEvent();
         if(CURRENT_GENDER < 0)
         {
            this.setCurrentSex(!!PlayerManager.Instance.Self.Sex?int(1):int(2));
         }
      }
      
      private function initBtns() : void
      {
         var i:uint = 0;
         var topBtnTextTranslation:Array = null;
         var subBtnTextTranslation:Array = null;
         var dx:Number = NaN;
         var dy:Number = NaN;
         var k:uint = 0;
         i = 0;
         this._topBtns = new Vector.<SelectedTextButton>();
         this._topBtnsGroup = new SelectedButtonGroup();
         this._subBtns = new Vector.<SelectedTextButton>();
         this._subBtnsContainers = new Vector.<HBox>();
         this._subBtnsGroups = new Vector.<SelectedButtonGroup>();
         this._genderGroup = new SelectedButtonGroup();
         this._goodItems = new Vector.<ShopGoodItem>();
         this._goodItemGroup = new SelectedButtonGroup();
         this._firstPage = ComponentFactory.Instance.creat("ddtshop.BtnFirstPage");
         this._prePageBtn = ComponentFactory.Instance.creat("ddtshop.BtnPrePage");
         this._nextPageBtn = ComponentFactory.Instance.creat("ddtshop.BtnNextPage");
         this._endPageBtn = ComponentFactory.Instance.creat("ddtshop.BtnEndPage");
         this._currentPageTxt = ComponentFactory.Instance.creatComponentByStylename("ddtshop.CurrentPage");
         this._currentPageInput = ComponentFactory.Instance.creatComponentByStylename("ddtshop.CurrentPageInput");
         this._topBtnsContainer = ComponentFactory.Instance.creat("ddtshop.TopBtnContainer");
         var topBtnStyleName:Array = ["ddtshop.TopBtnRecommend","ddtshop.TopBtnEquipment","ddtshop.TopBtnBeautyup","ddtshop.TopBtnProp","ddtshop.TopBtnDisCount"];
         topBtnTextTranslation = ["shop.ShopRightView.TopBtn.recommend","shop.ShopRightView.TopBtn.equipment","shop.ShopRightView.TopBtn.beautyup","shop.ShopRightView.TopBtn.prop","shop.ShopRightView.TopBtn.discount"];
         topBtnStyleName.forEach(function(param1:*, param2:int, param3:Array):void
         {
            var _loc4_:SelectedTextButton = ComponentFactory.Instance.creat(param1 as String);
            _loc4_.text = LanguageMgr.GetTranslation(topBtnTextTranslation[param2]);
            _topBtns.push(_loc4_);
         });
         this._genderContainer = ComponentFactory.Instance.creat("ddtshop.GenderBtnContainer");
         this._maleBtn = ComponentFactory.Instance.creat("ddtshop.GenderBtnMale");
         this._femaleBtn = ComponentFactory.Instance.creat("ddtshop.GenderBtnFemale");
         this._rightItemLightMc = ComponentFactory.Instance.creatCustomObject("ddtshop.RightItemLightMc");
         this._goodItemContainerBg = ComponentFactory.Instance.creatComponentByStylename("ddtshop.GoodItemContainerBg");
         this._goodItemContainerTwoLine = ComponentFactory.Instance.creatComponentByStylename("ddtshop.TwoLine");
         this._goodItemContainerAll = ComponentFactory.Instance.creatCustomObject("ddtshop.GoodItemContainerAll");
         i = 0;
         while(i < SHOP_ITEM_NUM)
         {
            this._goodItems[i] = ComponentFactory.Instance.creatCustomObject("ddtshop.GoodItem");
            dx = this._goodItems[i].width;
            dy = this._goodItems[i].height;
            dx = dx * int(i % 2);
            dy = dy * int(i / 2);
            this._goodItems[i].x = dx;
            this._goodItems[i].y = dy + i / 2 * 2;
            this._goodItemContainerAll.addChild(this._goodItems[i]);
            this._goodItems[i].setItemLight(this._rightItemLightMc);
            this._goodItems[i].addEventListener(ItemEvent.ITEM_CLICK,this.__itemClick);
            this._goodItems[i].addEventListener(ItemEvent.ITEM_SELECT,this.__itemSelect);
            i++;
         }
         this._maleBtn.displacement = this._femaleBtn.displacement = false;
         this._genderContainer.addChild(this._femaleBtn);
         this._genderContainer.addChild(this._maleBtn);
         this._genderGroup.addSelectItem(this._maleBtn);
         this._genderGroup.addSelectItem(this._femaleBtn);
         i = 0;
         while(i < this._topBtns.length)
         {
            this._topBtns[i].addEventListener(MouseEvent.CLICK,this.__topBtnClick);
            this._topBtnsContainer.addChild(this._topBtns[i]);
            this._topBtnsGroup.addSelectItem(this._topBtns[i]);
            if(i == 0)
            {
               this._topBtnsGroup.selectIndex = i;
            }
            this._subBtnsGroups[i] = new SelectedButtonGroup();
            i++;
         }
         this._subBtnsContainers.push(ComponentFactory.Instance.creat("ddtshop.SubBtnContainerRecommend"));
         this._subBtnsContainers.push(ComponentFactory.Instance.creat("ddtshop.SubBtnContainerEquipment"));
         this._subBtnsContainers.push(ComponentFactory.Instance.creat("ddtshop.SubBtnContainerBeautyup"));
         this._subBtnsContainers.push(ComponentFactory.Instance.creat("ddtshop.SubBtnContainerProp"));
         this._subBtnsContainers.push(ComponentFactory.Instance.creat("ddtshop.SubBtnContainerExchange"));
         var subBtnStyleName:Array = ["ddtshop.SubBtnHotSaleIcon","ddtshop.SubBtnRecommend","ddtshop.SubBtnDiscount","ddtshop.SubBtnGiftMedalWeapon","ddtshop.SubBtnCloth","ddtshop.SubBtnHat","ddtshop.SubBtnGlasses","ddtshop.SubBtnRing","ddtshop.SubBtnHair","ddtshop.SubBtnEye","ddtshop.SubBtnFace","ddtshop.SubBtnSuit","ddtshop.SubBtnWing","ddtshop.SubBtnFunc","ddtshop.SubBtnSpecial","ddtshop.SubBtnGiftMedalAll"];
         subBtnTextTranslation = ["shop.ShopRightView.SubBtn.hotSale","shop.ShopRightView.SubBtn.recommend","shop.ShopRightView.SubBtn.discount","shop.ShopRightView.SubBtn.weapon","shop.ShopRightView.SubBtn.cloth","shop.ShopRightView.SubBtn.hat","shop.ShopRightView.SubBtn.glasses","shop.ShopRightView.SubBtn.ring","shop.ShopRightView.SubBtn.hair","shop.ShopRightView.SubBtn.eye","shop.ShopRightView.SubBtn.face","shop.ShopRightView.SubBtn.suit","shop.ShopRightView.SubBtn.wing","shop.ShopRightView.SubBtn.func","shop.ShopRightView.SubBtn.special","shop.ShopRightView.SubBtn.giftMedalAll"];
         subBtnStyleName.forEach(function(param1:*, param2:int, param3:Array):void
         {
            var _loc4_:SelectedTextButton = ComponentFactory.Instance.creat(param1 as String);
            _loc4_.text = LanguageMgr.GetTranslation(subBtnTextTranslation[param2]);
            _subBtns.push(_loc4_);
         });
         var controlArr:Array = [3,8,13,15];
         k = 0;
         i = 0;
         while(i < this._subBtns.length)
         {
            if(i == controlArr[k])
            {
               k++;
            }
            if(this._subBtnsContainers[k] == null)
            {
               k++;
            }
            this._subBtns[i].addEventListener(MouseEvent.CLICK,this.__subBtnClick);
            this._subBtnsContainers[k].addChild(this._subBtns[i]);
            this._subBtnsGroups[k].addSelectItem(this._subBtns[i]);
            if(i == 0)
            {
               this._subBtnsGroups[k].selectIndex = i;
            }
            this._subBtnsGroups[k].addEventListener(Event.CHANGE,this.subButtonSelectedChangeHandler);
            i++;
         }
         addChild(this._firstPage);
         addChild(this._prePageBtn);
         addChild(this._nextPageBtn);
         addChild(this._endPageBtn);
         addChild(this._currentPageInput);
         addChild(this._currentPageTxt);
         addChild(this._genderContainer);
         addChild(this._goodItemContainerBg);
         addChild(this._goodItemContainerTwoLine);
         addChild(this._goodItemContainerAll);
         addChild(this._topBtnsContainer);
         i = 0;
         while(i < this._subBtnsContainers.length)
         {
            if(this._subBtnsContainers[i])
            {
               addChild(this._subBtnsContainers[i]);
               this._subBtnsContainers[i].visible = false;
               if(i == 0)
               {
                  this._subBtnsContainers[i].visible = true;
               }
            }
            i++;
         }
         this._shopSearchBox = ComponentFactory.Instance.creatCustomObject("ddtshop.SearchBox");
         this._shopSearchEndBtnBg = ComponentFactory.Instance.creatBitmap("asset.ddtshop.SearchResultImage");
         this._shopSearchBox.addChild(this._shopSearchEndBtnBg);
         this._shopSearchColseBtn = ComponentFactory.Instance.creatComponentByStylename("ddtshop.ShopSearchColseBtn");
         this._shopSearchBox.addChild(this._shopSearchColseBtn);
         addChild(this._shopSearchBox);
         this._shopSearchBox.visible = false;
         this._moneyBg = ComponentFactory.Instance.creatBitmap("asset.shop.moneybg");
         addChild(this._moneyBg);
         this._moneySeperateLine = ComponentFactory.Instance.creatComponentByStylename("ddtshop.moneySeperateLine");
         addChild(this._moneySeperateLine);
         this._shopMoneySelectedCkBtn = ComponentFactory.Instance.creatComponentByStylename("ddtShop.moneySelectedCkBtn");
         this._shopDDTMoneySelectedCkBtn = ComponentFactory.Instance.creatComponentByStylename("ddtShop.ddtMoneySelectedCkBtn");
         this._shopMoneyGroup = new SelectedButtonGroup();
         this._shopMoneyGroup.addSelectItem(this._shopMoneySelectedCkBtn);
         this._shopMoneyGroup.addSelectItem(this._shopDDTMoneySelectedCkBtn);
         addChild(this._shopMoneySelectedCkBtn);
         addChild(this._shopDDTMoneySelectedCkBtn);
         this._shopMoneyGroup.selectIndex = CURRENT_MONEY_TYPE - 1;
         if(CURRENT_MONEY_TYPE == 2 && TOP_TYPE == 0)
         {
            SUB_TYPE = 0;
         }
         this._currentSubBtnContainerIndex = SUB_TYPE;
         this.updateBtn();
         this._topBtnsContainer.arrange();
      }
      
      private function updateBtn() : void
      {
         if(this._topBtns)
         {
            this._topBtns[4].visible = ShopManager.Instance.isHasDisCountGoods(CURRENT_MONEY_TYPE);
         }
         if(!this._topBtns[4].visible)
         {
            if(TOP_TYPE == 4)
            {
               TOP_TYPE = 0;
               SUB_TYPE = CURRENT_MONEY_TYPE == 1?int(2):int(0);
               isDiscountType = false;
               this._topBtnsGroup.selectIndex = TOP_TYPE;
               this.showSubBtns(TOP_TYPE);
            }
         }
      }
      
      private function subButtonSelectedChangeHandler(param1:Event) : void
      {
         this._subBtnsContainers[this._currentSubBtnContainerIndex].arrange();
      }
      
      private function initEvent() : void
      {
         this._topBtnsGroup.addEventListener(Event.CHANGE,this.__topBtnChangeHandler);
         this._maleBtn.addEventListener(MouseEvent.CLICK,this.__genderClick);
         this._femaleBtn.addEventListener(MouseEvent.CLICK,this.__genderClick);
         this._firstPage.addEventListener(MouseEvent.CLICK,this.__pageBtnClick);
         this._prePageBtn.addEventListener(MouseEvent.CLICK,this.__pageBtnClick);
         this._nextPageBtn.addEventListener(MouseEvent.CLICK,this.__pageBtnClick);
         this._endPageBtn.addEventListener(MouseEvent.CLICK,this.__pageBtnClick);
         this._shopSearchColseBtn.addEventListener(MouseEvent.CLICK,this.__shopSearchColseBtnClick);
         addEventListener(Event.ADDED_TO_STAGE,this.__userGuide);
         this._shopMoneyGroup.addEventListener(Event.CHANGE,this.__moneySelectBtnChangeHandler);
         ShopManager.Instance.addEventListener(ShopEvent.DISCOUNT_IS_CHANGE,this.__discountChange);
      }
      
      protected function __discountChange(param1:ShopEvent) : void
      {
         this.updateBtn();
         this.loadList();
      }
      
      protected function __moneySelectBtnChangeHandler(param1:Event) : void
      {
         var _loc2_:int = this._shopMoneyGroup.selectIndex + 1;
         if(CURRENT_MONEY_TYPE == _loc2_)
         {
            return;
         }
         if(!this._isSearch)
         {
            CURRENT_PAGE = 1;
         }
         CURRENT_MONEY_TYPE = _loc2_;
         if(!ShopManager.Instance.isHasDisCountGoods(CURRENT_MONEY_TYPE) && TOP_TYPE == 4)
         {
            this._topBtnsGroup.selectIndex = TOP_TYPE = 0;
            this.showSubBtns(TOP_TYPE);
            this._subBtnsGroups[TOP_TYPE].selectIndex = SUB_TYPE = 0;
            isDiscountType = false;
         }
         if(_loc2_ == 2 && TOP_TYPE == 0)
         {
            this._subBtnsGroups[TOP_TYPE].selectIndex = SUB_TYPE = 0;
         }
         this.updateBtn();
         this.loadList();
         SoundManager.instance.play("008");
      }
      
      protected function __topBtnChangeHandler(param1:Event) : void
      {
         this._topBtnsContainer.arrange();
      }
      
      private function __userGuide(param1:Event) : void
      {
         removeEventListener(Event.ADDED_TO_STAGE,this.__userGuide);
         if(!PlayerManager.Instance.Self.IsWeakGuildFinish(Step.GUIDE_SHOP) && PlayerManager.Instance.Self.Grade >= 9)
         {
            NewHandContainer.Instance.showArrow(ArrowType.SHOP_GIFT,180,"trainer.shopGiftArrowPos","asset.trainer.txtShopGift","trainer.shopGiftTipPos",null,5);
         }
      }
      
      private function reoveArrow() : void
      {
         NewHandContainer.Instance.clearArrowByID(ArrowType.SHOP_GIFT);
      }
      
      protected function __shopSearchColseBtnClick(param1:MouseEvent) : void
      {
         this._isSearch = false;
         this._shopSearchBox.visible = false;
         TOP_TYPE = this._tempTopType;
         this._tempTopType = -1;
         this._topBtnsGroup.selectIndex = TOP_TYPE;
         if(!this._tempSubBtnHBox)
         {
            this._tempSubBtnHBox = this._subBtnsContainers[0];
         }
         this._tempSubBtnHBox.visible = true;
         CURRENT_PAGE = this._tempCurrentPage;
         this._tempCurrentPage = -1;
         if(TOP_TYPE == 0 && CURRENT_MONEY_TYPE == 2)
         {
            this._subBtnsGroups[TOP_TYPE].selectIndex = SUB_TYPE = 0;
         }
         this.loadList();
         SoundManager.instance.play("008");
      }
      
      public function loadList() : void
      {
         if(this._isSearch)
         {
            return;
         }
         if(!isDiscountType)
         {
            this.setList(ShopManager.Instance.getValidSortedGoodsByType(this.getType(),CURRENT_PAGE));
         }
         else
         {
            this.setList(ShopManager.Instance.getDisCountGoods(CURRENT_MONEY_TYPE,CURRENT_PAGE));
         }
      }
      
      private function getType() : int
      {
         var _loc1_:Array = [];
         if(CURRENT_MONEY_TYPE == 1)
         {
            _loc1_ = CURRENT_GENDER == 1?ShopType.MALE_MONEY_TYPE:ShopType.FEMALE_MONEY_TYPE;
            this._subBtns[1].visible = true;
            this._subBtns[2].visible = true;
         }
         else if(CURRENT_MONEY_TYPE == 2)
         {
            _loc1_ = CURRENT_GENDER == 1?ShopType.MALE_DDTMONEY_TYPE:ShopType.FEMALE_DDTMONEY_TYPE;
            this._subBtns[1].visible = false;
            this._subBtns[2].visible = false;
         }
         var _loc2_:* = _loc1_[TOP_TYPE];
         if(_loc2_ is Array && SUB_TYPE > -1)
         {
            _loc2_ = _loc2_[SUB_TYPE];
         }
         return int(_loc2_);
      }
      
      public function setCurrentSex(param1:int) : void
      {
         CURRENT_GENDER = param1;
         this._genderGroup.selectIndex = CURRENT_GENDER - 1;
      }
      
      public function setList(param1:Vector.<ShopItemInfo>) : void
      {
         this.clearitems();
         var _loc2_:int = 0;
         while(_loc2_ < SHOP_ITEM_NUM)
         {
            this._goodItems[_loc2_].selected = false;
            if(!param1)
            {
               break;
            }
            if(_loc2_ < param1.length && param1[_loc2_])
            {
               this._goodItems[_loc2_].shopItemInfo = param1[_loc2_];
            }
            _loc2_++;
         }
         if(!isDiscountType)
         {
            this._currentPageTxt.text = CURRENT_PAGE + "/" + ShopManager.Instance.getResultPages(this.getType());
         }
         else
         {
            this._currentPageTxt.text = CURRENT_PAGE + "/" + ShopManager.Instance.getDisCountResultPages(CURRENT_MONEY_TYPE);
         }
      }
      
      public function searchList(param1:Vector.<ShopItemInfo>) : void
      {
         var _loc3_:HBox = null;
         if(this._searchShopItemList == param1 && this._isSearch)
         {
            return;
         }
         this._searchShopItemList = param1;
         if(!this._isSearch)
         {
            this._tempTopType = TOP_TYPE;
            this._tempCurrentPage = CURRENT_PAGE;
         }
         this._isSearch = true;
         TOP_TYPE = -1;
         this._topBtnsGroup.selectIndex = -1;
         this._topBtnsContainer.arrange();
         CURRENT_PAGE = 1;
         var _loc2_:int = 0;
         while(_loc2_ < this._subBtnsContainers.length)
         {
            _loc3_ = this._subBtnsContainers[_loc2_] as HBox;
            if(_loc3_)
            {
               _loc3_.visible = false;
            }
            _loc2_++;
         }
         this._shopSearchBox.visible = true;
         this.runSearch();
      }
      
      private function runSearch() : void
      {
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         this.clearitems();
         this._searchItemTotalPage = Math.ceil(this._searchShopItemList.length / 8);
         if(CURRENT_PAGE > 0 && CURRENT_PAGE <= this._searchItemTotalPage)
         {
            _loc1_ = 8 * (CURRENT_PAGE - 1);
            _loc2_ = Math.min(this._searchShopItemList.length - _loc1_,8);
            _loc3_ = 0;
            while(_loc3_ < _loc2_)
            {
               this._goodItems[_loc3_].selected = false;
               if(this._searchShopItemList[_loc3_ + _loc1_])
               {
                  this._goodItems[_loc3_].shopItemInfo = this._searchShopItemList[_loc3_ + _loc1_];
               }
               _loc3_++;
            }
         }
         this._currentPageTxt.text = CURRENT_PAGE + "/" + this._searchItemTotalPage;
      }
      
      private function __genderClick(param1:MouseEvent) : void
      {
         var _loc2_:int = param1.currentTarget as SelectedButton == this._maleBtn?int(1):int(2);
         if(CURRENT_GENDER == _loc2_)
         {
            return;
         }
         this.setCurrentSex(_loc2_);
         if(!this._isSearch)
         {
            CURRENT_PAGE = 1;
         }
         this._controller.setFittingModel(CURRENT_GENDER == 1);
         SoundManager.instance.play("008");
      }
      
      private function __itemSelect(param1:ItemEvent) : void
      {
         var _loc3_:ISelectable = null;
         param1.stopImmediatePropagation();
         var _loc2_:ShopGoodItem = param1.currentTarget as ShopGoodItem;
         for each(_loc3_ in this._goodItems)
         {
            _loc3_.selected = false;
         }
         _loc2_.selected = true;
      }
      
      private function __itemClick(param1:ItemEvent) : void
      {
         var _loc3_:ISelectable = null;
         var _loc4_:ISelectable = null;
         var _loc5_:Boolean = false;
         var _loc6_:Boolean = false;
         var _loc7_:int = 0;
         var _loc8_:ShopCarItemInfo = null;
         var _loc2_:ShopGoodItem = param1.currentTarget as ShopGoodItem;
         if(this._controller.model.isOverCount(_loc2_.shopItemInfo))
         {
            for each(_loc3_ in this._goodItems)
            {
               _loc3_.selected = _loc3_ == _loc2_;
            }
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("shop.ShopIIModel.GoodsNumberLimit"));
            return;
         }
         if(_loc2_.shopItemInfo && _loc2_.shopItemInfo.TemplateInfo)
         {
            for each(_loc4_ in this._goodItems)
            {
               _loc4_.selected = _loc4_ == _loc2_;
            }
            if(EquipType.dressAble(_loc2_.shopItemInfo.TemplateInfo))
            {
               _loc7_ = _loc2_.shopItemInfo.TemplateInfo.NeedSex != 2?int(0):int(1);
               if(_loc2_.shopItemInfo.TemplateInfo.NeedSex != 0 && this._genderGroup.selectIndex != _loc7_)
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("tank.view.changeColor.sexAlert"));
                  return;
               }
               this._controller.addTempEquip(_loc2_.shopItemInfo);
            }
            else
            {
               _loc8_ = new ShopCarItemInfo(_loc2_.shopItemInfo.GoodsID,_loc2_.shopItemInfo.TemplateID);
               ObjectUtils.copyProperties(_loc8_,_loc2_.shopItemInfo);
               _loc5_ = this._controller.addToCar(_loc8_);
            }
            this.itemClick(_loc2_);
            _loc6_ = this._controller.leftView.getColorEditorVisble();
            if(_loc5_ && !_loc6_)
            {
               this.addCartEffects(_loc2_.itemCell);
            }
         }
         dispatchEvent(new Event(SHOW_LIGHT));
      }
      
      private function addCartEffects(param1:DisplayObject) : void
      {
         var _loc4_:TweenProxy = null;
         var _loc5_:TimelineLite = null;
         var _loc6_:TweenLite = null;
         var _loc7_:TweenLite = null;
         if(!param1)
         {
            return;
         }
         var _loc2_:BitmapData = new BitmapData(param1.width,param1.height,true,0);
         _loc2_.draw(param1);
         var _loc3_:Bitmap = new Bitmap(_loc2_,"auto",true);
         parent.addChild(_loc3_);
         _loc4_ = TweenProxy.create(_loc3_);
         _loc4_.registrationX = _loc4_.width / 2;
         _loc4_.registrationY = _loc4_.height / 2;
         var _loc8_:Point = DisplayUtils.localizePoint(parent,param1);
         _loc4_.x = _loc8_.x + _loc4_.width / 2;
         _loc4_.y = _loc8_.y + _loc4_.height / 2;
         _loc5_ = new TimelineLite();
         _loc5_.vars.onComplete = this.twComplete;
         _loc5_.vars.onCompleteParams = [_loc5_,_loc4_,_loc3_];
         _loc6_ = new TweenLite(_loc4_,0.3,{
            "x":220,
            "y":430
         });
         _loc7_ = new TweenLite(_loc4_,0.3,{
            "scaleX":0.1,
            "scaleY":0.1
         });
         _loc5_.append(_loc6_);
         _loc5_.append(_loc7_,-0.2);
      }
      
      private function twComplete(param1:TimelineLite, param2:TweenProxy, param3:Bitmap) : void
      {
         if(param1)
         {
            param1.kill();
         }
         if(param2)
         {
            param2.destroy();
         }
         if(param3.parent)
         {
            param3.parent.removeChild(param3);
            param3.bitmapData.dispose();
         }
         param2 = null;
         param3 = null;
         param1 = null;
      }
      
      private function itemClick(param1:ShopGoodItem) : void
      {
         if(param1.shopItemInfo.TemplateInfo != null)
         {
            if(CURRENT_GENDER != param1.shopItemInfo.TemplateInfo.NeedSex && param1.shopItemInfo.TemplateInfo.NeedSex != 0)
            {
               this.setCurrentSex(param1.shopItemInfo.TemplateInfo.NeedSex);
               this._controller.setFittingModel(CURRENT_GENDER == 1);
            }
         }
      }
      
      private function __pageBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         if(!this._isSearch)
         {
            if(!isDiscountType)
            {
               if(ShopManager.Instance.getResultPages(this.getType()) == 0)
               {
                  return;
               }
            }
            else if(ShopManager.Instance.getDisCountResultPages(CURRENT_MONEY_TYPE) == 0)
            {
               return;
            }
            switch(param1.currentTarget)
            {
               case this._firstPage:
                  if(CURRENT_PAGE != 1)
                  {
                     CURRENT_PAGE = 1;
                  }
                  break;
               case this._prePageBtn:
                  if(CURRENT_PAGE == 1)
                  {
                     if(!isDiscountType)
                     {
                        CURRENT_PAGE = ShopManager.Instance.getResultPages(this.getType()) + 1;
                     }
                     else
                     {
                        CURRENT_PAGE = ShopManager.Instance.getDisCountResultPages(CURRENT_MONEY_TYPE) + 1;
                     }
                  }
                  CURRENT_PAGE--;
                  break;
               case this._nextPageBtn:
                  if(!isDiscountType)
                  {
                     if(CURRENT_PAGE == ShopManager.Instance.getResultPages(this.getType()))
                     {
                        CURRENT_PAGE = 0;
                     }
                  }
                  else if(CURRENT_PAGE == ShopManager.Instance.getDisCountResultPages(CURRENT_MONEY_TYPE))
                  {
                     CURRENT_PAGE = 0;
                  }
                  CURRENT_PAGE++;
                  break;
               case this._endPageBtn:
                  if(!isDiscountType)
                  {
                     if(CURRENT_PAGE != ShopManager.Instance.getResultPages(this.getType()))
                     {
                        CURRENT_PAGE = ShopManager.Instance.getResultPages(this.getType());
                     }
                  }
                  else if(CURRENT_PAGE != ShopManager.Instance.getDisCountResultPages(CURRENT_MONEY_TYPE))
                  {
                     CURRENT_PAGE = ShopManager.Instance.getDisCountResultPages(CURRENT_MONEY_TYPE);
                  }
            }
            this.loadList();
         }
         else
         {
            switch(param1.currentTarget)
            {
               case this._firstPage:
                  if(CURRENT_PAGE != 1)
                  {
                     CURRENT_PAGE = 1;
                  }
                  break;
               case this._prePageBtn:
                  if(CURRENT_PAGE == 1)
                  {
                     CURRENT_PAGE = this._searchItemTotalPage + 1;
                  }
                  CURRENT_PAGE--;
                  break;
               case this._nextPageBtn:
                  if(CURRENT_PAGE == this._searchItemTotalPage)
                  {
                     CURRENT_PAGE = 0;
                  }
                  CURRENT_PAGE++;
                  break;
               case this._endPageBtn:
                  if(CURRENT_PAGE != this._searchItemTotalPage)
                  {
                     CURRENT_PAGE = this._searchItemTotalPage;
                  }
            }
            this.runSearch();
         }
      }
      
      private function __subBtnClick(param1:MouseEvent) : void
      {
         this.reoveArrow();
         var _loc2_:int = this._subBtnsContainers[TOP_TYPE].getChildIndex(param1.currentTarget as SelectedButton);
         if(_loc2_ != SUB_TYPE)
         {
            SUB_TYPE = _loc2_;
            CURRENT_PAGE = 1;
            this.loadList();
            SoundManager.instance.play("008");
         }
      }
      
      private function __topBtnClick(param1:MouseEvent) : void
      {
         this._topBtnsContainer.arrange();
         var _loc2_:int = this._topBtns.indexOf(param1.currentTarget as SelectedButton);
         this._isSearch = false;
         this._shopSearchBox.visible = false;
         this._tempTopType = -1;
         this._tempCurrentPage = -1;
         if(_loc2_ != TOP_TYPE)
         {
            TOP_TYPE = _loc2_;
            SUB_TYPE = 0;
            CURRENT_PAGE = 1;
            this.showSubBtns(_loc2_);
            this._currentSubBtnContainerIndex = _loc2_;
            if(TOP_TYPE == 4)
            {
               isDiscountType = true;
               if(!PlayerManager.Instance.Self.IsWeakGuildFinish(Step.GUIDE_SHOP) && PlayerManager.Instance.Self.Grade >= 9)
               {
                  NewHandContainer.Instance.clearArrowByID(ArrowType.SHOP_GIFT);
                  SocketManager.Instance.out.syncWeakStep(Step.GUIDE_SHOP);
               }
            }
            else
            {
               isDiscountType = false;
               this.disposeUserGuide();
            }
            this.loadList();
            SoundManager.instance.play("008");
         }
      }
      
      private function disposeUserGuide() : void
      {
      }
      
      private function clearitems() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ < SHOP_ITEM_NUM)
         {
            this._goodItems[_loc1_].shopItemInfo = null;
            _loc1_++;
         }
      }
      
      private function removeEvent() : void
      {
         this._topBtnsGroup.removeEventListener(Event.CHANGE,this.__topBtnChangeHandler);
         this._maleBtn.removeEventListener(MouseEvent.CLICK,this.__genderClick);
         this._femaleBtn.removeEventListener(MouseEvent.CLICK,this.__genderClick);
         this._prePageBtn.removeEventListener(MouseEvent.CLICK,this.__pageBtnClick);
         this._nextPageBtn.removeEventListener(MouseEvent.CLICK,this.__pageBtnClick);
         var _loc1_:uint = 0;
         while(_loc1_ < SHOP_ITEM_NUM)
         {
            this._goodItems[_loc1_].removeEventListener(ItemEvent.ITEM_CLICK,this.__itemClick);
            this._goodItems[_loc1_].removeEventListener(ItemEvent.ITEM_SELECT,this.__itemSelect);
            _loc1_++;
         }
         _loc1_ = 0;
         while(_loc1_ < this._topBtns.length)
         {
            this._topBtns[_loc1_].removeEventListener(MouseEvent.CLICK,this.__topBtnClick);
            _loc1_++;
         }
         _loc1_ = 0;
         while(_loc1_ < this._subBtns.length)
         {
            this._subBtns[_loc1_].removeEventListener(MouseEvent.CLICK,this.__subBtnClick);
            _loc1_++;
         }
         removeEventListener(Event.ADDED_TO_STAGE,this.__userGuide);
         this._shopSearchColseBtn.removeEventListener(MouseEvent.CLICK,this.__shopSearchColseBtnClick);
         this._shopMoneyGroup.removeEventListener(Event.CHANGE,this.__moneySelectBtnChangeHandler);
         ShopManager.Instance.removeEventListener(ShopEvent.DISCOUNT_IS_CHANGE,this.__discountChange);
      }
      
      private function showSubBtns(param1:int) : void
      {
         var _loc3_:HBox = null;
         var _loc2_:int = 0;
         while(_loc2_ < this._subBtnsContainers.length)
         {
            _loc3_ = this._subBtnsContainers[_loc2_] as HBox;
            if(_loc3_)
            {
               _loc3_.visible = false;
            }
            _loc2_++;
         }
         if(this._subBtnsContainers[param1])
         {
            this._subBtnsContainers[param1].visible = true;
            this._tempSubBtnHBox = this._subBtnsContainers[param1];
            this._subBtnsGroups[param1].selectIndex = SUB_TYPE;
            this._subBtnsContainers[param1].arrange();
         }
      }
      
      public function gotoPage(param1:int = -1, param2:int = -1, param3:int = 1, param4:int = 1) : void
      {
         var _loc6_:HBox = null;
         if(param1 == 4 && !ShopManager.Instance.isHasDisCountGoods(CURRENT_MONEY_TYPE))
         {
            param1 = 0;
            param2 = 2;
         }
         if(param1 != -1)
         {
            TOP_TYPE = param1;
         }
         if(param2 != -1)
         {
            SUB_TYPE = param2;
         }
         CURRENT_PAGE = param3;
         CURRENT_GENDER = param4;
         this._topBtnsGroup.selectIndex = TOP_TYPE;
         this._subBtnsGroups[TOP_TYPE].selectIndex = SUB_TYPE;
         this._genderGroup.selectIndex = CURRENT_GENDER - 1;
         this.setCurrentSex(CURRENT_GENDER);
         this._currentPageTxt.text = CURRENT_PAGE + "/" + this._searchItemTotalPage;
         var _loc5_:int = 0;
         while(_loc5_ < this._subBtnsContainers.length)
         {
            _loc6_ = this._subBtnsContainers[_loc5_] as HBox;
            if(_loc6_)
            {
               _loc6_.visible = false;
            }
            _loc5_++;
         }
         if(this._subBtnsContainers[TOP_TYPE])
         {
            this._subBtnsContainers[TOP_TYPE].visible = true;
            this._subBtnsContainers[TOP_TYPE].arrange();
            this._tempSubBtnHBox = this._subBtnsContainers[TOP_TYPE];
         }
         this.loadList();
      }
      
      public function dispose() : void
      {
         if(this._tempCurrentPage > -1)
         {
            CURRENT_PAGE = this._tempCurrentPage;
         }
         if(this._tempTopType > -1)
         {
            TOP_TYPE = this._tempTopType;
         }
         this.removeEvent();
         this.disposeUserGuide();
         ObjectUtils.disposeAllChildren(this);
         if(this._currentPageTxt)
         {
            this._currentPageTxt.dispose();
         }
         this._currentPageTxt = null;
         var _loc1_:int = 0;
         while(_loc1_ < this._goodItems.length)
         {
            ObjectUtils.disposeObject(this._goodItems[_loc1_]);
            this._goodItems[_loc1_] = null;
            _loc1_++;
         }
         this._goodItems = null;
         _loc1_ = 0;
         while(_loc1_ < this._topBtns.length)
         {
            ObjectUtils.disposeObject(this._topBtns[_loc1_]);
            this._topBtns[_loc1_] = null;
            _loc1_++;
         }
         this._topBtns = null;
         _loc1_ = 0;
         while(_loc1_ < this._subBtns.length)
         {
            ObjectUtils.disposeObject(this._subBtns[_loc1_]);
            this._subBtns[_loc1_] = null;
            _loc1_++;
         }
         this._subBtns = null;
         _loc1_ = 0;
         while(_loc1_ < this._subBtnsGroups.length)
         {
            ObjectUtils.disposeObject(this._subBtnsGroups[_loc1_]);
            this._subBtnsGroups[_loc1_] = null;
            ObjectUtils.disposeObject(this._subBtnsContainers[_loc1_]);
            this._subBtnsContainers[_loc1_] = null;
            _loc1_++;
         }
         this._subBtnsContainers = null;
         this._subBtnsGroups = null;
         this._controller = null;
         this._goodItemGroup = null;
         this._nextPageBtn = null;
         this._prePageBtn = null;
         this._topBtnsGroup = null;
         this._tempSubBtnHBox = null;
         if(this._shopDDTMoneySelectedCkBtn)
         {
            ObjectUtils.disposeObject(this._shopDDTMoneySelectedCkBtn);
            this._shopDDTMoneySelectedCkBtn = null;
         }
         if(this._shopMoneySelectedCkBtn)
         {
            ObjectUtils.disposeObject(this._shopMoneySelectedCkBtn);
            this._shopMoneySelectedCkBtn = null;
         }
         if(this._moneyBg)
         {
            ObjectUtils.disposeObject(this._moneyBg);
            this._moneyBg = null;
         }
         this._shopMoneyGroup = null;
         ObjectUtils.disposeObject(this._moneySeperateLine);
         this._moneySeperateLine = null;
         ObjectUtils.disposeObject(this._bg);
         this._bg = null;
         ObjectUtils.disposeObject(this._bg1);
         this._bg1 = null;
         ObjectUtils.disposeObject(this._currentPageInput);
         this._currentPageInput = null;
         ObjectUtils.disposeObject(this._femaleBtn);
         this._femaleBtn = null;
         ObjectUtils.disposeObject(this._genderContainer);
         this._genderContainer = null;
         ObjectUtils.disposeObject(this._genderGroup);
         this._genderGroup = null;
         ObjectUtils.disposeObject(this._rightViewTitleBg);
         this._rightViewTitleBg = null;
         ObjectUtils.disposeObject(this._goodItemContainerAll);
         this._goodItemContainerAll = null;
         ObjectUtils.disposeObject(this._maleBtn);
         this._maleBtn = null;
         ObjectUtils.disposeObject(this._firstPage);
         this._firstPage = null;
         ObjectUtils.disposeObject(this._prePageBtn);
         this._prePageBtn = null;
         ObjectUtils.disposeObject(this._nextPageBtn);
         this._nextPageBtn = null;
         ObjectUtils.disposeObject(this._endPageBtn);
         this._endPageBtn = null;
         ObjectUtils.disposeObject(this._topBtnsContainer);
         this._topBtnsContainer = null;
         ObjectUtils.disposeObject(this._rightItemLightMc);
         this._rightItemLightMc = null;
         ObjectUtils.disposeObject(this._shopSearchEndBtnBg);
         this._shopSearchEndBtnBg = null;
         ObjectUtils.disposeObject(this._shopSearchColseBtn);
         this._shopSearchColseBtn = null;
         ObjectUtils.disposeObject(this._shopSearchBox);
         this._shopSearchBox = null;
         ObjectUtils.disposeObject(this._goodItemContainerBg);
         this._goodItemContainerBg = null;
         ObjectUtils.disposeObject(this._goodItemContainerTwoLine);
         this._goodItemContainerTwoLine = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
