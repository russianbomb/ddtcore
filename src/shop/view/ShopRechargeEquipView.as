package shop.view
{
   import baglocked.BaglockedManager;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.ScrollPanel;
   import com.pickgliss.ui.controls.TextButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.controls.container.VBox;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.image.Image;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.ui.vo.AlertInfo;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.data.goods.ItemPrice;
   import ddt.data.goods.ShopCarItemInfo;
   import ddt.events.PlayerPropertyEvent;
   import ddt.manager.LanguageMgr;
   import ddt.manager.LeavePageManager;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.ShopManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   
   public class ShopRechargeEquipView extends Sprite implements Disposeable
   {
       
      
      private var price:ItemPrice;
      
      private var _bg:Image;
      
      private var _frame:BaseAlerFrame;
      
      private var _chargeBtn:TextButton;
      
      private var _itemContainer:VBox;
      
      private var _scrollPanel:ScrollPanel;
      
      private var _equipList:Array;
      
      private var _costMoneyTxt:FilterFrameText;
      
      private var _costGiftTxt:FilterFrameText;
      
      private var _playerMoneyTxt:FilterFrameText;
      
      private var _playerGiftTxt:FilterFrameText;
      
      private var _currentCountTxt:FilterFrameText;
      
      private var _affirmContinuBt:BaseButton;
      
      private var _needToPayPanelBg:Image;
      
      private var _haveOwnPanelBg:Image;
      
      private var _needToPayText:FilterFrameText;
      
      private var _haveOwnText:FilterFrameText;
      
      private var _leftTicketText:FilterFrameText;
      
      private var _rightTicketText:FilterFrameText;
      
      private var _leftGiftText:FilterFrameText;
      
      private var _rightGiftText:FilterFrameText;
      
      private var _amountOfItemTipText:FilterFrameText;
      
      private var _isBandList:Vector.<ShopRechargeEquipViewItem>;
      
      private var _leftOrderText:FilterFrameText;
      
      private var _rightOrderText:FilterFrameText;
      
      private var _playerOrderTxt:FilterFrameText;
      
      private var _costOrderTxt:FilterFrameText;
      
      public function ShopRechargeEquipView()
      {
         super();
         this.init();
      }
      
      private function init() : void
      {
         this._frame = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeViewFrame");
         this._bg = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeViewFrameBg");
         this._needToPayPanelBg = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeView.NeedToPayPanelBg");
         this._haveOwnPanelBg = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeView.HaveOwnPanelBg");
         this._amountOfItemTipText = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeView.AmountOfItemTipText");
         this._amountOfItemTipText.text = LanguageMgr.GetTranslation("shop.RechargeView.AmountOfItemTipText");
         this._needToPayText = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeView.NeedToPayText");
         this._needToPayText.text = LanguageMgr.GetTranslation("shop.RechargeView.NeedToPayText");
         this._haveOwnText = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeView.HaveOwnText");
         this._haveOwnText.text = LanguageMgr.GetTranslation("shop.RechargeView.HaveOwnText");
         this._leftTicketText = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeView.LeftTicketText");
         this._leftTicketText.text = LanguageMgr.GetTranslation("shop.RechargeView.TicketText");
         this._leftGiftText = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeView.LeftGiftText");
         this._leftGiftText.text = LanguageMgr.GetTranslation("shop.RechargeView.GiftText");
         this._leftOrderText = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeView.LeftOrderText");
         this._leftOrderText.text = LanguageMgr.GetTranslation("newDdtMoney");
         this._leftOrderText.visible = false;
         this._rightTicketText = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeView.RightTicketText");
         this._rightTicketText.text = LanguageMgr.GetTranslation("shop.RechargeView.TicketText");
         this._rightGiftText = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeView.RightGiftText");
         this._rightGiftText.text = LanguageMgr.GetTranslation("shop.RechargeView.GiftText");
         this._rightOrderText = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeView.RightOrderText");
         this._rightOrderText.text = LanguageMgr.GetTranslation("newDdtMoney");
         this._rightOrderText.visible = false;
         this._chargeBtn = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeView.RechargeBtn");
         this._chargeBtn.text = LanguageMgr.GetTranslation("shop.RechargeView.RechargeBtnText");
         this._scrollPanel = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeViewItemList");
         this._itemContainer = ComponentFactory.Instance.creatComponentByStylename("ddtshop.CartItemContainer");
         this._costMoneyTxt = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeView.LeftTicketNumberText");
         this._costGiftTxt = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeView.LeftGiftNumberText");
         this._costOrderTxt = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeView.LeftOrderNumberText");
         this._playerMoneyTxt = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeView.RightTicketNumberText");
         this._playerGiftTxt = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeView.RightGiftNumberText");
         this._playerOrderTxt = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeView.RightOrderNumberText");
         this._currentCountTxt = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeViewCurrentCount");
         this._affirmContinuBt = ComponentFactory.Instance.creatComponentByStylename("ddtshop.RechargeView.RechargeConfirmationBtn");
         var _loc1_:AlertInfo = new AlertInfo(LanguageMgr.GetTranslation("tank.view.continuation.contiuationTitle"),LanguageMgr.GetTranslation("tank.view.common.AddPricePanel.xu"),LanguageMgr.GetTranslation("cancel"),false,false);
         this._frame.info = _loc1_;
         this._equipList = PlayerManager.Instance.Self.OvertimeListByBody;
         this._scrollPanel.vScrollProxy = ScrollPanel.ON;
         this._scrollPanel.setView(this._itemContainer);
         this._itemContainer.spacing = 5;
         this._itemContainer.strictSize = 100;
         this._scrollPanel.invalidateViewport();
         this._frame.moveEnable = false;
         this._frame.addToContent(this._bg);
         this._frame.addToContent(this._needToPayPanelBg);
         this._frame.addToContent(this._haveOwnPanelBg);
         this._frame.addToContent(this._amountOfItemTipText);
         this._frame.addToContent(this._needToPayText);
         this._frame.addToContent(this._haveOwnText);
         this._frame.addToContent(this._leftTicketText);
         this._frame.addToContent(this._leftGiftText);
         this._frame.addToContent(this._leftOrderText);
         this._frame.addToContent(this._rightTicketText);
         this._frame.addToContent(this._rightGiftText);
         this._frame.addToContent(this._rightOrderText);
         this._frame.addToContent(this._chargeBtn);
         this._frame.addToContent(this._scrollPanel);
         this._frame.addToContent(this._costMoneyTxt);
         this._frame.addToContent(this._costGiftTxt);
         this._frame.addToContent(this._costOrderTxt);
         this._frame.addToContent(this._playerMoneyTxt);
         this._frame.addToContent(this._playerGiftTxt);
         this._frame.addToContent(this._playerOrderTxt);
         this._frame.addToContent(this._currentCountTxt);
         this._frame.addToContent(this._affirmContinuBt);
         this.setList();
         this.__onPlayerPropertyChange();
         this._chargeBtn.addEventListener(MouseEvent.CLICK,this.__onChargeClick);
         this._frame.addEventListener(FrameEvent.RESPONSE,this.__frameEventHandler);
         this._affirmContinuBt.addEventListener(MouseEvent.CLICK,this._clickContinuBt);
         addChild(this._frame);
      }
      
      private function __frameEventHandler(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         switch(param1.responseCode)
         {
            case FrameEvent.SUBMIT_CLICK:
               if(PlayerManager.Instance.Self.bagLocked)
               {
                  BaglockedManager.Instance.show();
               }
               else
               {
                  this.payAll();
               }
               break;
            case FrameEvent.CANCEL_CLICK:
            case FrameEvent.CLOSE_CLICK:
            case FrameEvent.ESC_CLICK:
               InventoryItemInfo.startTimer();
               this.dispose();
         }
      }
      
      private function _clickContinuBt(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         var _loc2_:int = int(this._costMoneyTxt.text);
         var _loc3_:int = int(this._costGiftTxt.text);
         var _loc4_:int = int(this._costOrderTxt.text);
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
         }
         if(_loc2_ > PlayerManager.Instance.Self.Money)
         {
            LeavePageManager.showFillFrame();
            return;
         }
         if(_loc3_ > PlayerManager.Instance.Self.BandMoney)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("shop.view.lackCoin"));
            return;
         }
         this.payAll();
      }
      
      private function __onChargeClick(param1:Event) : void
      {
         LeavePageManager.leaveToFillPath();
      }
      
      private function payAll() : void
      {
         var _loc3_:Array = null;
         var _loc4_:Array = null;
         var _loc5_:Array = null;
         var _loc6_:ShopCarItemInfo = null;
         var _loc7_:ShopRechargeEquipViewItem = null;
         var _loc8_:int = 0;
         var _loc9_:uint = 0;
         var _loc10_:ShopRechargeEquipViewItem = null;
         var _loc11_:Boolean = false;
         var _loc1_:Array = this.shopInfoList;
         var _loc2_:Array = ShopManager.Instance.buyIt(this.shopInfoListWithOutDelete);
         if(this.shopInfoListWithOutDelete.length > 0)
         {
            _loc3_ = new Array();
            _loc4_ = new Array();
            _loc5_ = new Array();
            for each(_loc6_ in this.shopInfoListWithOutDelete)
            {
               _loc9_ = _loc1_.indexOf(_loc6_);
               _loc10_ = this._itemContainer.getChildAt(_loc9_) as ShopRechargeEquipViewItem;
               _loc4_.push(_loc10_.info);
               _loc5_.push(_loc10_);
            }
            for each(_loc7_ in _loc5_)
            {
               this._itemContainer.removeChild(_loc7_);
            }
            this._scrollPanel.invalidateViewport();
            _loc8_ = 0;
            while(_loc8_ < _loc4_.length)
            {
               _loc11_ = _loc4_[_loc8_].Place <= 30;
               _loc3_.push([_loc4_[_loc8_].BagType,_loc4_[_loc8_].Place,_loc2_[_loc8_].GoodsID,_loc2_[_loc8_].currentBuyType,_loc11_,this._isBandList[_loc8_].moneType]);
               _loc8_++;
            }
            this.updateTxt();
            SocketManager.Instance.out.sendGoodsContinue(_loc3_);
            if(this._itemContainer.numChildren <= 0)
            {
               this.dispose();
            }
            else if(this.shopInfoListWithOutDelete.length > 0)
            {
               this.showAlert();
            }
         }
         else if(this.shopInfoListWithOutDelete.length != 0)
         {
            this.showAlert();
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("tank.view.continuation.contiuationFailed"));
         }
      }
      
      private function setList() : void
      {
         var i:InventoryItemInfo = null;
         var item:ShopRechargeEquipViewItem = null;
         this._equipList.sort(function(param1:InventoryItemInfo, param2:InventoryItemInfo):Number
         {
            var _loc3_:Array = [7,5,1,17,8,9,14,6,13,15,3,4,2];
            var _loc4_:uint = _loc3_.indexOf(param1.CategoryID);
            var _loc5_:uint = _loc3_.indexOf(param2.CategoryID);
            if(_loc4_ < _loc5_)
            {
               return -1;
            }
            if(_loc4_ == _loc5_)
            {
               return 0;
            }
            return 1;
         });
         this._isBandList = new Vector.<ShopRechargeEquipViewItem>();
         var t_i:int = 0;
         for each(i in this._equipList)
         {
            if(ShopManager.Instance.canAddPrice(i.TemplateID))
            {
               item = new ShopRechargeEquipViewItem();
               item.itemInfo = i;
               item.clieckHander = this.setIsBandList;
               item.id = t_i;
               this._isBandList.push(item);
               item.setColor(i.Color);
               item.addEventListener(ShopCartItem.DELETE_ITEM,this.__onItemDelete);
               item.addEventListener(ShopCartItem.CONDITION_CHANGE,this.__onItemChange);
               this._itemContainer.addChild(item);
               t_i++;
            }
         }
         this._scrollPanel.invalidateViewport();
         this.updateTxt();
      }
      
      private function setIsBandList(param1:int, param2:int) : void
      {
         var _loc3_:int = this._isBandList.length;
         var _loc4_:int = 0;
         while(_loc4_ < _loc3_)
         {
            if(this._isBandList[_loc4_].id == param1)
            {
               this._isBandList[_loc4_].moneType = param2;
               return;
            }
            _loc4_++;
         }
      }
      
      private function __onItemDelete(param1:Event) : void
      {
      }
      
      private function __onItemChange(param1:Event) : void
      {
         this.updateTxt();
      }
      
      private function get shopInfoListWithOutDelete() : Array
      {
         var _loc4_:ShopRechargeEquipViewItem = null;
         var _loc1_:Array = new Array();
         this._isBandList = new Vector.<ShopRechargeEquipViewItem>();
         var _loc2_:int = this._itemContainer.numChildren - 1;
         var _loc3_:uint = 0;
         while(_loc3_ < this._itemContainer.numChildren)
         {
            _loc4_ = this._itemContainer.getChildAt(_loc2_ - _loc3_) as ShopRechargeEquipViewItem;
            if(_loc4_ && !_loc4_.isDelete)
            {
               _loc1_.push(_loc4_.shopItemInfo);
               this._isBandList.push(_loc4_);
            }
            _loc3_++;
         }
         return _loc1_;
      }
      
      private function get shopInfoList() : Array
      {
         var _loc3_:ShopRechargeEquipViewItem = null;
         var _loc1_:Array = new Array();
         var _loc2_:uint = 0;
         while(_loc2_ < this._itemContainer.numChildren)
         {
            _loc3_ = this._itemContainer.getChildAt(_loc2_) as ShopRechargeEquipViewItem;
            _loc1_.push(_loc3_.shopItemInfo);
            _loc2_++;
         }
         return _loc1_;
      }
      
      private function updateTxt() : void
      {
         var _loc4_:ShopCarItemInfo = null;
         var _loc1_:Array = this.shopInfoListWithOutDelete;
         var _loc2_:uint = _loc1_.length;
         this._currentCountTxt.text = String(_loc2_);
         if(_loc2_ == 0)
         {
            this._affirmContinuBt.enable = false;
         }
         else
         {
            this._affirmContinuBt.enable = true;
         }
         this._frame.submitButtonEnable = _loc2_ <= 0?Boolean(false):Boolean(true);
         this.price = new ItemPrice(null,null,null);
         var _loc3_:int = 0;
         for each(_loc4_ in _loc1_)
         {
            this.price.addItemPrice(_loc4_.getCurrentPrice(),false,this._isBandList[_loc3_].moneType);
            _loc3_++;
         }
         this._costMoneyTxt.text = String(this.price.moneyValue);
         this._costGiftTxt.text = String(this.price.bandDdtMoneyValue);
         this.updataTextColor();
         PlayerManager.Instance.Self.addEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__onPlayerPropertyChange,false,0,true);
      }
      
      private function __onPlayerPropertyChange(param1:Event = null) : void
      {
         this._playerMoneyTxt.text = String(PlayerManager.Instance.Self.Money);
         this._playerGiftTxt.text = String(PlayerManager.Instance.Self.BandMoney);
         this.updataTextColor();
      }
      
      private function updataTextColor() : void
      {
         if(this.price)
         {
            if(this.price.moneyValue > PlayerManager.Instance.Self.Money)
            {
               this._costMoneyTxt.setTextFormat(ComponentFactory.Instance.model.getSet("ddtshop.DigitWarningTF"));
            }
            else
            {
               this._costMoneyTxt.setTextFormat(ComponentFactory.Instance.model.getSet("ddtshop.RechargeView.NumberTextTF"));
            }
            if(this.price.bandDdtMoneyValue > PlayerManager.Instance.Self.BandMoney)
            {
               this._costGiftTxt.setTextFormat(ComponentFactory.Instance.model.getSet("ddtshop.DigitWarningTF"));
            }
            else
            {
               this._costGiftTxt.setTextFormat(ComponentFactory.Instance.model.getSet("ddtshop.RechargeView.NumberTextTF"));
            }
            this._costOrderTxt.setTextFormat(ComponentFactory.Instance.model.getSet("ddtshop.RechargeView.NumberTextTF"));
         }
      }
      
      private function showAlert() : void
      {
         var _loc1_:BaseAlerFrame = null;
         if(this.price.moneyValue > PlayerManager.Instance.Self.Money)
         {
            _loc1_ = LeavePageManager.showFillFrame();
         }
      }
      
      public function dispose() : void
      {
         InventoryItemInfo.startTimer();
         PlayerManager.Instance.Self.removeEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__onPlayerPropertyChange);
         this._frame.removeEventListener(FrameEvent.RESPONSE,this.__frameEventHandler);
         this._chargeBtn.removeEventListener(MouseEvent.CLICK,this.__onChargeClick);
         this._affirmContinuBt.removeEventListener(MouseEvent.CLICK,this._clickContinuBt);
         this._frame.dispose();
         this._frame = null;
         this.price = null;
         this._bg = null;
         ObjectUtils.disposeObject(this._amountOfItemTipText);
         this._amountOfItemTipText = null;
         ObjectUtils.disposeObject(this._needToPayPanelBg);
         this._needToPayPanelBg = null;
         ObjectUtils.disposeObject(this._haveOwnPanelBg);
         this._haveOwnPanelBg = null;
         ObjectUtils.disposeObject(this._needToPayText);
         this._needToPayText = null;
         ObjectUtils.disposeObject(this._haveOwnText);
         this._haveOwnText = null;
         ObjectUtils.disposeObject(this._leftTicketText);
         this._leftTicketText = null;
         ObjectUtils.disposeObject(this._leftGiftText);
         this._leftGiftText = null;
         ObjectUtils.disposeObject(this._rightTicketText);
         this._rightTicketText = null;
         ObjectUtils.disposeObject(this._rightGiftText);
         this._rightGiftText = null;
         this._chargeBtn = null;
         this._itemContainer = null;
         this._scrollPanel = null;
         this._equipList = null;
         this._costMoneyTxt = null;
         this._costGiftTxt = null;
         this._playerMoneyTxt = null;
         this._playerGiftTxt = null;
         this._currentCountTxt = null;
         this._affirmContinuBt = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
