package shop.view
{
   import bagAndInfo.cell.CellFactory;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.SelectedButtonGroup;
   import com.pickgliss.ui.controls.SelectedCheckButton;
   import com.pickgliss.ui.controls.container.VBox;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.image.Image;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.data.goods.Price;
   import ddt.data.goods.ShopCarItemInfo;
   import ddt.manager.LanguageMgr;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import flash.display.DisplayObject;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.text.TextFormat;
   import shop.manager.ShopBuyManager;
   
   public class ShopCartItem extends Sprite implements Disposeable
   {
      
      public static const DELETE_ITEM:String = "deleteitem";
      
      public static const CONDITION_CHANGE:String = "conditionchange";
      
      public static const ADD_LENGTH:String = "add_length";
       
      
      protected var _bg:DisplayObject;
      
      protected var _itemCellBg:DisplayObject;
      
      protected var _verticalLine:Image;
      
      protected var _cartItemGroup:SelectedButtonGroup;
      
      protected var _cartItemSelectVBox:VBox;
      
      protected var _closeBtn:BaseButton;
      
      public var id:int;
      
      public var type:int;
      
      public var upDataBtnState:Function;
      
      protected var _itemName:FilterFrameText;
      
      protected var _cell:ShopPlayerCell;
      
      protected var _shopItemInfo:ShopCarItemInfo;
      
      protected var _blueTF:TextFormat;
      
      protected var _yellowTF:TextFormat;
      
      public var seleBand:Function;
      
      private var _items:Vector.<SelectedCheckButton>;
      
      protected var _isBand:Boolean;
      
      public function ShopCartItem()
      {
         super();
         this.drawBackground();
         this.drawNameField();
         this.drawCellField();
         this._closeBtn = ComponentFactory.Instance.creatComponentByStylename("ddtshop.CartItemCloseBtn");
         this._cartItemSelectVBox = ComponentFactory.Instance.creatComponentByStylename("ddtshop.CartItemSelectVBox");
         this._cartItemGroup = new SelectedButtonGroup();
         addChild(this._closeBtn);
         addChild(this._cartItemSelectVBox);
         this.initListener();
      }
      
      public function get closeBtn() : BaseButton
      {
         return this._closeBtn;
      }
      
      protected function drawBackground(param1:Boolean = false) : void
      {
         this._bg = ComponentFactory.Instance.creatComponentByStylename("ddtshop.CartItemBg");
         this._itemCellBg = ComponentFactory.Instance.creat("ddtshop.CartItemCellBg");
         this._verticalLine = ComponentFactory.Instance.creatComponentByStylename("ddtshop.VerticalLine");
         addChild(this._bg);
         addChild(this._verticalLine);
         addChild(this._itemCellBg);
      }
      
      protected function drawNameField() : void
      {
         this._itemName = ComponentFactory.Instance.creatComponentByStylename("ddtshop.CartItemName");
         addChild(this._itemName);
      }
      
      protected function drawCellField() : void
      {
         this._cell = CellFactory.instance.createShopCartItemCell() as ShopPlayerCell;
         PositionUtils.setPos(this._cell,"ddtshop.CartItemCellPoint");
         addChild(this._cell);
      }
      
      public function get isBand() : Boolean
      {
         return this._isBand;
      }
      
      public function set isBand(param1:Boolean) : void
      {
         this._isBand = param1;
      }
      
      protected function initListener() : void
      {
         this._closeBtn.addEventListener(MouseEvent.CLICK,this.__closeClick);
         addEventListener(MouseEvent.CLICK,this.clickHander);
      }
      
      public function addItem(param1:Boolean = false) : void
      {
         if(this._shopItemInfo.getCurrentPrice().PriceType == Price.DDT_MONEY)
         {
            return;
         }
         if(this.type == ShopCheckOutView.PRESENT)
         {
            return;
         }
         this._isBand = param1;
         if(Price.ONLYDDT_MONEY)
         {
            this.addSelectedBandBtn();
         }
         else if(Price.ONLYMONEY)
         {
            this.addSelectedBtn();
         }
         else
         {
            this.addSelectedBtn();
            this.addSelectedBandBtn();
         }
      }
      
      private function addSelectedBtn() : void
      {
      }
      
      private function addSelectedBandBtn() : void
      {
      }
      
      public function setDianquanType(param1:Boolean = false) : void
      {
         var _loc3_:String = null;
         var _loc4_:int = 0;
         var _loc6_:int = 0;
         var _loc2_:int = this._items.length;
         var _loc5_:int = 0;
         while(_loc5_ < _loc2_)
         {
            _loc6_ = _loc5_ + 1;
            if(param1)
            {
               _loc3_ = this._shopItemInfo.getTimeToString(_loc6_) != LanguageMgr.GetTranslation("ddt.shop.buyTime1")?this._shopItemInfo.getTimeToString(_loc6_):LanguageMgr.GetTranslation("ddt.shop.buyTime2");
               this._items[_loc5_].text = this._shopItemInfo.getItemPrice(_loc6_).toString(true) + "/" + _loc3_;
               _loc4_ = this.rigthValue(_loc6_);
            }
            else
            {
               _loc3_ = this._shopItemInfo.getTimeToString(_loc6_) != LanguageMgr.GetTranslation("ddt.shop.buyTime1")?this._shopItemInfo.getTimeToString(_loc6_):LanguageMgr.GetTranslation("ddt.shop.buyTime2");
               this._items[_loc5_].text = this._shopItemInfo.getItemPrice(_loc6_).toString() + "/" + _loc3_;
               _loc4_ = this.rigthValue(_loc6_);
            }
            _loc5_++;
         }
         if(parent && this.seleBand != null)
         {
            this.seleBand(this.id,_loc4_,param1);
         }
      }
      
      private function rigthValue(param1:int) : int
      {
         if(param1 == this._shopItemInfo.currentBuyType)
         {
            return this._shopItemInfo.getItemPrice(param1).moneyValue;
         }
         return 0;
      }
      
      protected function clickHander(param1:Event) : void
      {
         if(this.type == ShopCheckOutView.PRESENT || this.type == ShopCheckOutView.ASKTYPE)
         {
            return;
         }
         if(this._shopItemInfo.getCurrentPrice().PriceType == Price.DDT_MONEY)
         {
            return;
         }
         if(param1.currentTarget.id == ShopBuyManager.crrItemId)
         {
            return;
         }
         if(param1.target is SelectedCheckButton)
         {
            return;
         }
         if(param1.target is FilterFrameText)
         {
            return;
         }
         dispatchEvent(new Event(ADD_LENGTH));
      }
      
      protected function removeEvent() : void
      {
         if(this._cartItemGroup)
         {
            this._cartItemGroup.removeEventListener(Event.CHANGE,this.__cartItemGroupChange);
            this._cartItemGroup = null;
         }
         if(this._closeBtn)
         {
            this._closeBtn.removeEventListener(MouseEvent.CLICK,this.__closeClick);
         }
         removeEventListener(MouseEvent.CLICK,this.clickHander);
      }
      
      protected function __closeClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         dispatchEvent(new Event(DELETE_ITEM));
      }
      
      protected function __cartItemGroupChange(param1:Event) : void
      {
         this._shopItemInfo.currentBuyType = this._cartItemGroup.selectIndex + 1;
         dispatchEvent(new Event(CONDITION_CHANGE));
      }
      
      public function setShopItemInfo(param1:ShopCarItemInfo, param2:int = -10, param3:Boolean = false) : void
      {
         if(this._shopItemInfo != param1)
         {
            this._cell.info = param1.TemplateInfo;
            this._shopItemInfo = param1;
            if(this.id == param2)
            {
               this.addItem(param3);
            }
            if(param1 == null)
            {
               this._itemName.text = "";
            }
            else
            {
               this._itemName.text = String(param1.TemplateInfo.Name);
               this.cartItemSelectVBoxInit();
               this.setDianquanType(param3);
            }
         }
      }
      
      protected function cartItemSelectVBoxInit() : void
      {
         var _loc2_:SelectedCheckButton = null;
         var _loc3_:String = null;
         var _loc4_:String = null;
         this.clearitem();
         this._cartItemGroup = new SelectedButtonGroup();
         this._cartItemGroup.addEventListener(Event.CHANGE,this.__cartItemGroupChange);
         this._items = new Vector.<SelectedCheckButton>();
         var _loc1_:int = 1;
         while(_loc1_ < 4)
         {
            if(this._shopItemInfo.getItemPrice(_loc1_).IsValid)
            {
               _loc2_ = ComponentFactory.Instance.creatComponentByStylename("ddtshop.CartItemSelectBtn");
               _loc3_ = this._shopItemInfo.getTimeToString(_loc1_) != LanguageMgr.GetTranslation("ddt.shop.buyTime1")?this._shopItemInfo.getTimeToString(_loc1_):LanguageMgr.GetTranslation("ddt.shop.buyTime2");
               _loc4_ = this._shopItemInfo.getItemPrice(_loc1_).toString();
               _loc2_.text = this._shopItemInfo.getItemPrice(_loc1_).toString() + "/" + _loc3_;
               this._cartItemSelectVBox.addChild(_loc2_);
               _loc2_.addEventListener(MouseEvent.CLICK,this.__soundPlay);
               this._items.push(_loc2_);
               this._cartItemGroup.addSelectItem(_loc2_);
            }
            _loc1_++;
         }
         this._cartItemGroup.selectIndex = this._shopItemInfo.currentBuyType - 1 < 1?int(0):int(this._shopItemInfo.currentBuyType - 1);
         if(this._cartItemSelectVBox.numChildren == 2)
         {
            this._cartItemSelectVBox.y = 18;
         }
         else if(this._cartItemSelectVBox.numChildren == 1)
         {
            this._cartItemSelectVBox.y = 33;
         }
      }
      
      private function clearitem() : void
      {
         var _loc1_:int = 0;
         if(this._cartItemGroup)
         {
            this._cartItemGroup.removeEventListener(Event.CHANGE,this.__cartItemGroupChange);
            this._cartItemGroup = null;
         }
         if(this._items)
         {
            _loc1_ = 0;
            while(_loc1_ < this._items.length)
            {
               if(this._items[_loc1_])
               {
                  ObjectUtils.disposeObject(this._items[_loc1_]);
                  this._items[_loc1_].removeEventListener(MouseEvent.CLICK,this.__soundPlay);
               }
               this._items[_loc1_] = null;
               _loc1_++;
            }
         }
         if(this._cartItemSelectVBox)
         {
            this._cartItemSelectVBox.disposeAllChildren();
         }
      }
      
      protected function __soundPlay(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
      }
      
      public function get shopItemInfo() : ShopCarItemInfo
      {
         return this._shopItemInfo;
      }
      
      public function get info() : ItemTemplateInfo
      {
         return this._cell.info;
      }
      
      public function get TemplateID() : int
      {
         if(this._cell.info == null)
         {
            return -1;
         }
         return this._cell.info.TemplateID;
      }
      
      public function setColor(param1:*) : void
      {
         this._cell.setColor(param1);
      }
      
      public function dispose() : void
      {
         this.removeEvent();
         this.clearitem();
         ObjectUtils.disposeAllChildren(this);
         this._cartItemSelectVBox = null;
         this._cartItemGroup = null;
         this._bg = null;
         this._itemCellBg = null;
         this._verticalLine = null;
         this._closeBtn = null;
         this._itemName = null;
         this._cell = null;
         this._shopItemInfo = null;
         this._blueTF = null;
         this._yellowTF = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
