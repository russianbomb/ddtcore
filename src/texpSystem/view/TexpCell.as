package texpSystem.view
{
   import bagAndInfo.cell.BagCell;
   import bagAndInfo.cell.DragEffect;
   import baglocked.BaglockedManager;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.events.InteractiveEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.utils.DoubleClickManager;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.command.ShineObject;
   import ddt.data.BagInfo;
   import ddt.data.EquipType;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.manager.DragManager;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   import texpSystem.data.TexpInfo;
   
   public class TexpCell extends BagCell
   {
       
      
      private var _texpInfo:TexpInfo;
      
      private var _shiner:ShineObject;
      
      private var _texpCountSelectFrame:TexpCountSelectFrame;
      
      public function TexpCell()
      {
         var _loc1_:Sprite = new Sprite();
         var _loc2_:Bitmap = ComponentFactory.Instance.creatBitmap("asset.texpSystem.itemBg1");
         _loc1_.addChild(_loc2_);
         super(0,null,true,_loc1_);
         this._shiner = new ShineObject(ComponentFactory.Instance.creat("asset.texpSystem.cellShine"));
         addChild(this._shiner);
         this._shiner.mouseChildren = this._shiner.mouseEnabled = false;
         var _loc3_:Rectangle = ComponentFactory.Instance.creatCustomObject("texpSystem.texpCell.content");
         setContentSize(_loc3_.width,_loc3_.height);
         PicPos = new Point(_loc3_.x,_loc3_.y);
      }
      
      override protected function initEvent() : void
      {
         super.initEvent();
         addEventListener(InteractiveEvent.CLICK,this.__clickHandler);
         DoubleClickManager.Instance.enableDoubleClick(this);
      }
      
      override protected function removeEvent() : void
      {
         super.removeEvent();
         removeEventListener(InteractiveEvent.CLICK,this.__clickHandler);
         DoubleClickManager.Instance.disableDoubleClick(this);
      }
      
      private function getNeedCount(param1:int) : int
      {
         var _loc2_:int = (this._texpInfo.upExp - this._texpInfo.currExp) / param1;
         if((this._texpInfo.upExp - this._texpInfo.currExp) % param1 > 0)
         {
            _loc2_ = int(_loc2_) + 1;
         }
         return _loc2_;
      }
      
      override public function dragDrop(param1:DragEffect) : void
      {
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         var _loc2_:InventoryItemInfo = param1.data as InventoryItemInfo;
         if(_loc2_ && param1.action != DragEffect.SPLIT)
         {
            if(_loc2_.CategoryID != EquipType.TEXP)
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("texpSystem.view.TexpCell.typeError"));
               return;
            }
            this._texpCountSelectFrame = new TexpCountSelectFrame();
            this._texpCountSelectFrame = ComponentFactory.Instance.creatComponentByStylename("texpSystem.TexpCountSelectFrame");
            this._texpCountSelectFrame.addEventListener(FrameEvent.RESPONSE,this.__ontexpCountResponse);
            this._texpCountSelectFrame.texpInfo = _loc2_;
            this._texpCountSelectFrame.show(this.getNeedCount(int(_loc2_.Property2)),_loc2_.Count);
            param1.action = DragEffect.NONE;
            DragManager.acceptDrag(this);
         }
      }
      
      private function __ontexpCountResponse(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         var _loc2_:InventoryItemInfo = this._texpCountSelectFrame.texpInfo;
         switch(param1.responseCode)
         {
            case FrameEvent.ESC_CLICK:
            case FrameEvent.CANCEL_CLICK:
            case FrameEvent.CLOSE_CLICK:
               this._texpCountSelectFrame.dispose();
               break;
            case FrameEvent.SUBMIT_CLICK:
            case FrameEvent.ENTER_CLICK:
               if(info)
               {
                  SocketManager.Instance.out.sendClearStoreBag();
               }
               SocketManager.Instance.out.sendMoveGoods(_loc2_.BagType,_loc2_.Place,BagInfo.STOREBAG,0,this._texpCountSelectFrame.count,true);
               this._texpCountSelectFrame.dispose();
         }
      }
      
      override protected function createChildren() : void
      {
         super.createChildren();
         if(_tbxCount)
         {
            ObjectUtils.disposeObject(_tbxCount);
         }
         _tbxCount = ComponentFactory.Instance.creat("texpSystem.txtCount");
         _tbxCount.mouseEnabled = false;
         addChild(_tbxCount);
      }
      
      override protected function onMouseOver(param1:MouseEvent) : void
      {
      }
      
      override protected function onMouseOut(param1:MouseEvent) : void
      {
      }
      
      public function startShine() : void
      {
         this._shiner.shine();
      }
      
      public function stopShine() : void
      {
         this._shiner.stopShine();
      }
      
      private function __clickHandler(param1:InteractiveEvent) : void
      {
         if(_info && !locked && stage && allowDrag)
         {
            SoundManager.instance.playButtonSound();
         }
         dragStart();
      }
      
      override public function dispose() : void
      {
         if(this._shiner)
         {
            ObjectUtils.disposeObject(this._shiner);
            this._shiner = null;
         }
         if(this._texpCountSelectFrame)
         {
            ObjectUtils.disposeObject(this._texpCountSelectFrame);
            this._texpCountSelectFrame = null;
         }
         super.dispose();
      }
      
      public function get texpInfo() : TexpInfo
      {
         return this._texpInfo;
      }
      
      public function set texpInfo(param1:TexpInfo) : void
      {
         this._texpInfo = param1;
      }
   }
}
