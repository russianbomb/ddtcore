package texpSystem.view
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.NumberSelecter;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.ui.vo.AlertInfo;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.manager.LanguageMgr;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import flash.events.Event;
   
   public class TexpCountSelectFrame extends BaseAlerFrame
   {
       
      
      private var _alertInfo:AlertInfo;
      
      private var _text:FilterFrameText;
      
      private var _numberSelecter:NumberSelecter;
      
      private var _needText:FilterFrameText;
      
      private var _texpInfo:InventoryItemInfo;
      
      public function TexpCountSelectFrame()
      {
         super();
         this.intView();
      }
      
      private function intView() : void
      {
         cancelButtonStyle = "core.simplebt";
         submitButtonStyle = "core.simplebt";
         this._alertInfo = new AlertInfo(LanguageMgr.GetTranslation("texpSystem.view.TexpCountSelect.frame"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"));
         this._alertInfo.moveEnable = false;
         info = this._alertInfo;
         this.escEnable = true;
         this._text = ComponentFactory.Instance.creatComponentByStylename("texpSystem.TexpCountSelectFrame.Text");
         this._text.text = LanguageMgr.GetTranslation("texpSystem.view.TexpCountSelect.text");
         this._numberSelecter = ComponentFactory.Instance.creatComponentByStylename("core.ddtshop.NumberSelecter");
         PositionUtils.setPos(this._numberSelecter,"texpSystem.expCountSelecterPos");
         this._numberSelecter.addEventListener(Event.CHANGE,this.__seleterChange);
         this._needText = ComponentFactory.Instance.creatComponentByStylename("texpSystem.TexpCountSelectFrame.NeedText");
         this._needText.visible = false;
         addToContent(this._text);
         addToContent(this._numberSelecter);
         addToContent(this._needText);
      }
      
      public function show(param1:int, param2:int, param3:int = 1) : void
      {
         this._numberSelecter.valueLimit = param3 + "," + param2;
         LayerManager.Instance.addToLayer(this,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
         if(param2 < param1)
         {
            this._numberSelecter.currentValue = param2;
         }
         this._needText.htmlText = LanguageMgr.GetTranslation("texpSystem.view.TexpCountSelect.CountText",param1);
         this._needText.visible = true;
         if(param1 > param2)
         {
            this._numberSelecter.currentValue = param2;
         }
         else
         {
            this._numberSelecter.currentValue = param1;
         }
      }
      
      private function __seleterChange(param1:Event) : void
      {
         SoundManager.instance.play("008");
      }
      
      public function get texpInfo() : InventoryItemInfo
      {
         return this._texpInfo;
      }
      
      public function set texpInfo(param1:InventoryItemInfo) : void
      {
         this._texpInfo = param1;
      }
      
      public function get count() : int
      {
         return this._numberSelecter.currentValue;
      }
      
      override public function dispose() : void
      {
         super.dispose();
         if(this._numberSelecter)
         {
            this._numberSelecter.removeEventListener(Event.CHANGE,this.__seleterChange);
         }
         this.removeView();
      }
      
      private function removeView() : void
      {
         ObjectUtils.disposeObject(this._numberSelecter);
         this._numberSelecter = null;
         ObjectUtils.disposeObject(this._text);
         this._text = null;
         ObjectUtils.disposeObject(this._needText);
         this._needText = null;
      }
   }
}
