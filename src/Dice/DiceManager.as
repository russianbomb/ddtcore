package Dice
{
   import Dice.Controller.DiceController;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.GameInSocketOut;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import flash.events.EventDispatcher;
   import flash.events.IEventDispatcher;
   
   public class DiceManager extends EventDispatcher
   {
      
      private static var _instance:DiceManager;
       
      
      private var _callBack:Function;
      
      private var _isopen:Boolean = false;
      
      public function DiceManager(param1:IEventDispatcher = null)
      {
         super(param1);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.DICE_ACTIVE_OPEN,this.__showEnterIcon);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.DICE_ACTIVE_CLOSE,this.__hideEnterIcon);
      }
      
      public static function get Instance() : DiceManager
      {
         if(_instance == null)
         {
            _instance = new DiceManager();
         }
         return _instance;
      }
      
      public function setup(param1:Function = null) : void
      {
         if(param1 != null)
         {
            this._callBack = param1;
            if(DiceController.Instance.canUseModel)
            {
               this._callBack(true);
            }
         }
      }
      
      public function get isopen() : Boolean
      {
         return this._isopen;
      }
      
      public function __showEnterIcon(param1:CrazyTankSocketEvent) : void
      {
         this._isopen = true;
         DiceController.Instance.install(param1.pkg);
         if(this._callBack != null)
         {
            this._callBack(true);
         }
      }
      
      public function EnterDice() : void
      {
         SoundManager.instance.play("008");
         GameInSocketOut.sendRequestEnterDiceSystem();
      }
      
      private function __hideEnterIcon(param1:CrazyTankSocketEvent) : void
      {
         this._isopen = false;
         if(this._callBack != null)
         {
            this._callBack(false);
            DiceController.Instance.unInstall();
         }
      }
   }
}
