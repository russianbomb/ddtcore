package Dice.View
{
   import Dice.Controller.DiceController;
   import Dice.Event.DiceEvent;
   import baglocked.BaglockedManager;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.SelectedCheckButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.image.ScaleFrameImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.GameInSocketOut;
   import ddt.manager.LanguageMgr;
   import ddt.manager.LeavePageManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SoundManager;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   
   public class DiceSystemPanel extends Sprite implements Disposeable
   {
       
      
      private var _controller:DiceController;
      
      private var _bg:Bitmap;
      
      private var _startBG:Bitmap;
      
      private var _startArrow:Bitmap;
      
      private var _freeCount:FilterFrameText;
      
      private var _startBtn:ScaleFrameImage;
      
      private var _cellItem:Array;
      
      private var _target:int = -1;
      
      private var _baseAlert:BaseAlerFrame;
      
      private var _selectCheckBtn:SelectedCheckButton;
      
      private var _poorManAlert:BaseAlerFrame;
      
      public function DiceSystemPanel()
      {
         super();
         this.preInitialize();
      }
      
      public function set Controller(param1:DiceController) : void
      {
         this._controller = param1;
         this.initialize();
         this.addEvent();
      }
      
      private function initialize() : void
      {
         addChild(this._startBG);
         addChild(this._freeCount);
         addChild(this._startBtn);
         this._startBtn.buttonMode = true;
         this._startBtn.setFrame(DiceController.Instance.diceType + 1);
         this._freeCount.text = LanguageMgr.GetTranslation("dice.freeCount",DiceController.Instance.freeCount);
         this._cellItem = this._controller.cellIDs;
         if(!this._controller.hasUsedFirstCell)
         {
            if(this._startArrow == null)
            {
               this._startArrow = ComponentFactory.Instance.creatBitmap("asset.dice.startArrow");
            }
            addChild(this._startArrow);
         }
         var _loc1_:int = 0;
         while(_loc1_ < this._cellItem.length && _loc1_ < this._controller.CELL_COUNT)
         {
            if(this._cellItem[_loc1_])
            {
               addChild(this._cellItem[_loc1_]);
            }
            _loc1_++;
         }
      }
      
      private function preInitialize() : void
      {
         this._startBG = ComponentFactory.Instance.creatBitmap("asset.dice.bg1");
         this._freeCount = ComponentFactory.Instance.creatComponentByStylename("asset.dice.freeCount");
         this._startBtn = ComponentFactory.Instance.creatComponentByStylename("asset.dice.startBtn");
      }
      
      private function addEvent() : void
      {
         this._startBtn.addEventListener(MouseEvent.CLICK,this.__onStartBtnClick);
         DiceController.Instance.addEventListener(DiceEvent.PLAYER_ISWALKING,this.__onPlayerState);
         DiceController.Instance.addEventListener(DiceEvent.GET_DICE_RESULT_DATA,this.__getDiceResultData);
         DiceController.Instance.addEventListener(DiceEvent.CHANGED_DICETYPE,this.__onDicetypeChanged);
         DiceController.Instance.addEventListener(DiceEvent.REFRESH_DATA,this.__onRefreshData);
         DiceController.Instance.addEventListener(DiceEvent.CHANGED_FREE_COUNT,this.__onFreeCountChanged);
      }
      
      private function removeEvent() : void
      {
         this._startBtn.addEventListener(MouseEvent.CLICK,this.__onStartBtnClick);
         DiceController.Instance.removeEventListener(DiceEvent.PLAYER_ISWALKING,this.__onPlayerState);
         DiceController.Instance.removeEventListener(DiceEvent.GET_DICE_RESULT_DATA,this.__getDiceResultData);
         DiceController.Instance.removeEventListener(DiceEvent.CHANGED_DICETYPE,this.__onDicetypeChanged);
         DiceController.Instance.removeEventListener(DiceEvent.REFRESH_DATA,this.__onRefreshData);
         DiceController.Instance.removeEventListener(DiceEvent.CHANGED_FREE_COUNT,this.__onFreeCountChanged);
      }
      
      private function __onPlayerState(param1:DiceEvent) : void
      {
         if(Boolean(param1.resultData.isWalking))
         {
            this._startBtn.visible = false;
            DiceController.Instance.setDestinationCell(-1);
         }
         else
         {
            this._startBtn.visible = true;
            if(this._target != -1)
            {
               DiceController.Instance.setDestinationCell(this._target - 1);
            }
         }
      }
      
      private function __getDiceResultData(param1:DiceEvent) : void
      {
         var _loc2_:Object = param1.resultData;
         if(_loc2_)
         {
            this._target = (int(_loc2_.position) + int(_loc2_.result)) % DiceController.Instance.CELL_COUNT;
            this._target = this._target + (this._target < int(_loc2_.position) && !DiceController.Instance.hasUsedFirstCell?1:0);
         }
         this._startBtn.visible = false;
      }
      
      private function __onStartBtnClick(param1:MouseEvent) : void
      {
         var _loc2_:int = DiceController.Instance.commonDicePrice;
         if(DiceController.Instance.diceType == 1)
         {
            _loc2_ = DiceController.Instance.doubleDicePrice;
         }
         else if(DiceController.Instance.diceType == 2)
         {
            _loc2_ = DiceController.Instance.bigDicePrice;
         }
         else if(DiceController.Instance.diceType == 3)
         {
            _loc2_ = DiceController.Instance.smallDicePrice;
         }
         SoundManager.instance.play("008");
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         if(PlayerManager.Instance.Self.Money < Number(_loc2_) && DiceController.Instance.freeCount <= 0)
         {
            if(this._poorManAlert)
            {
               ObjectUtils.disposeObject(this._poorManAlert);
            }
            this._poorManAlert = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("poorNote"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),false,false,false,LayerManager.BLCAK_BLOCKGOUND);
            this._poorManAlert.moveEnable = false;
            this._poorManAlert.addEventListener(FrameEvent.RESPONSE,this.__poorManResponse);
            return;
         }
         this.AlertFeedeductionWindow();
      }
      
      private function __poorManResponse(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         this._poorManAlert.removeEventListener(FrameEvent.RESPONSE,this.__poorManResponse);
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            LeavePageManager.leaveToFillPath();
         }
         ObjectUtils.disposeObject(this._poorManAlert);
         this._poorManAlert = null;
      }
      
      private function AlertFeedeductionWindow() : void
      {
         if(!DiceController.Instance.canPopupNextStartWindow && DiceController.Instance.freeCount <= 0)
         {
            this.openAlertFrame();
         }
         else
         {
            this._startBtn.visible = false;
            this.sendStartDataToServer();
         }
      }
      
      private function sendStartDataToServer() : void
      {
         GameInSocketOut.sendStartDiceInfo(DiceController.Instance.diceType,DiceController.Instance.CurrentPosition - 1);
      }
      
      private function openAlertFrame() : void
      {
         var _loc1_:String = null;
         var _loc2_:int = 0;
         var _loc3_:String = null;
         if(this._selectCheckBtn == null)
         {
            this._selectCheckBtn = ComponentFactory.Instance.creatComponentByStylename("asset.dice.refreshAlert.selectedCheck");
            this._selectCheckBtn.text = LanguageMgr.GetTranslation("dice.alert.nextPrompt");
            this._selectCheckBtn.selected = DiceController.Instance.canPopupNextStartWindow;
            this._selectCheckBtn.addEventListener(MouseEvent.CLICK,this.__onCheckBtnClick);
         }
         switch(DiceController.Instance.diceType)
         {
            case 1:
               _loc1_ = LanguageMgr.GetTranslation("dice.type.double");
               _loc2_ = DiceController.Instance.doubleDicePrice;
               break;
            case 2:
               _loc1_ = LanguageMgr.GetTranslation("dice.type.big");
               _loc2_ = DiceController.Instance.bigDicePrice;
               break;
            case 3:
               _loc1_ = LanguageMgr.GetTranslation("dice.type.small");
               _loc2_ = DiceController.Instance.smallDicePrice;
               break;
            default:
               _loc1_ = LanguageMgr.GetTranslation("dice.type.common");
               _loc2_ = DiceController.Instance.commonDicePrice;
         }
         _loc3_ = LanguageMgr.GetTranslation("dice.type.prompt",_loc1_,_loc2_) + "\n\n";
         if(this._baseAlert)
         {
            ObjectUtils.disposeObject(this._baseAlert);
         }
         this._baseAlert = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),_loc3_,LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),true,false,false,LayerManager.GAME_TOP_LAYER);
         this._baseAlert.addChild(this._selectCheckBtn);
         this._baseAlert.addEventListener(FrameEvent.RESPONSE,this.__onResponse);
      }
      
      private function __onCheckBtnClick(param1:MouseEvent) : void
      {
         var _loc2_:SelectedCheckButton = param1.currentTarget as SelectedCheckButton;
         DiceController.Instance.setPopupNextStartWindow(_loc2_.selected);
      }
      
      private function __onResponse(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         var _loc2_:BaseAlerFrame = param1.target as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.__onResponse);
         this._selectCheckBtn.removeEventListener(MouseEvent.CLICK,this.__onCheckBtnClick);
         ObjectUtils.disposeObject(this._selectCheckBtn);
         this._selectCheckBtn = null;
         _loc2_.dispose();
         if(param1.responseCode == FrameEvent.ENTER_CLICK || param1.responseCode == FrameEvent.SUBMIT_CLICK)
         {
            this._startBtn.visible = false;
            this.sendStartDataToServer();
         }
      }
      
      private function __onDicetypeChanged(param1:DiceEvent) : void
      {
         this._startBtn.setFrame(DiceController.Instance.diceType + 1);
      }
      
      private function __onFreeCountChanged(param1:DiceEvent) : void
      {
         this._freeCount.text = LanguageMgr.GetTranslation("dice.freeCount",DiceController.Instance.freeCount);
      }
      
      private function __onRefreshData(param1:DiceEvent) : void
      {
         this.initialize();
      }
      
      public function dispose() : void
      {
         this.removeEvent();
         ObjectUtils.disposeObject(this._startBtn);
         this._startBtn = null;
         ObjectUtils.disposeObject(this._startBG);
         this._startBG = null;
         ObjectUtils.disposeObject(this._bg);
         this._bg = null;
         ObjectUtils.disposeObject(this._startArrow);
         this._startArrow = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
