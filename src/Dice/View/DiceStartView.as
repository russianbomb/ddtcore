package Dice.View
{
   import Dice.Controller.DiceController;
   import Dice.Event.DiceEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   
   public class DiceStartView extends Sprite implements Disposeable
   {
       
      
      private var _type:int;
      
      private var _result:int;
      
      private var _leftArr:Array;
      
      private var _rightArr:Array;
      
      private var _rule:Array;
      
      private var _left:int;
      
      private var _right:int;
      
      public function DiceStartView()
      {
         this._leftArr = [];
         this._rightArr = [];
         this._rule = [];
         super();
         this.preInitialize();
      }
      
      public function play(param1:int, param2:int) : void
      {
         this._type = param1;
         this._result = param2;
         if(this._type == 1)
         {
            this._left = int(Math.random() * 6) + 1;
            if(this._left >= param2)
            {
               this._left = 1;
            }
            if(this._result - this._left >= 7)
            {
               this._right = this._result - 6;
               this._left = this._result - this._right;
            }
            else
            {
               this._right = this._result - this._left;
            }
            this._leftArr[this._left - 1].addEventListener(Event.ENTER_FRAME,this.__onEnterFrame);
            this._leftArr[this._left - 1].gotoAndPlay(2);
            this._rightArr[this._right - 1].gotoAndPlay(2);
            addChild(this._leftArr[this._left - 1]);
            addChild(this._rightArr[this._right - 1]);
         }
         else
         {
            this._left = this._result;
            this._leftArr[this._left - 1].addEventListener(Event.ENTER_FRAME,this.__onEnterFrame);
            this._leftArr[this._left - 1].gotoAndPlay(2);
            addChild(this._leftArr[this._left - 1]);
         }
      }
      
      private function __onEnterFrame(param1:Event) : void
      {
         var _loc2_:MovieClip = param1.currentTarget as MovieClip;
         if(_loc2_.currentLabel == "endLabel")
         {
            DiceController.Instance.dispatchEvent(new DiceEvent(DiceEvent.MOVIE_FINISH));
            _loc2_.removeEventListener(Event.ENTER_FRAME,this.__onEnterFrame);
            _loc2_.stop();
         }
      }
      
      public function removeAllMovie() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ < this._leftArr.length)
         {
            if(this._leftArr[_loc1_].parent)
            {
               removeChild(this._leftArr[_loc1_]);
            }
            if(this._rightArr[_loc1_].parent)
            {
               removeChild(this._rightArr[_loc1_]);
            }
            _loc1_++;
         }
      }
      
      private function preInitialize() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ < 6)
         {
            this._leftArr[_loc1_] = ComponentFactory.Instance.creat("asset.dice.movie" + (_loc1_ + 1));
            this._leftArr[_loc1_].x = 50;
            this._rightArr[_loc1_] = ComponentFactory.Instance.creat("asset.dice.movie" + (_loc1_ + 7));
            _loc1_++;
         }
      }
      
      public function dispose() : void
      {
         ObjectUtils.disposeObject(this._rightArr[0]);
         this._rightArr[0] = null;
         ObjectUtils.disposeObject(this._rightArr[1]);
         this._rightArr[1] = null;
         ObjectUtils.disposeObject(this._rightArr[2]);
         this._rightArr[2] = null;
         ObjectUtils.disposeObject(this._rightArr[3]);
         this._rightArr[3] = null;
         ObjectUtils.disposeObject(this._rightArr[4]);
         this._rightArr[4] = null;
         ObjectUtils.disposeObject(this._rightArr[5]);
         this._rightArr[5] = null;
         ObjectUtils.disposeObject(this._leftArr[0]);
         this._leftArr[0] = null;
         ObjectUtils.disposeObject(this._leftArr[1]);
         this._leftArr[1] = null;
         ObjectUtils.disposeObject(this._leftArr[2]);
         this._leftArr[2] = null;
         ObjectUtils.disposeObject(this._leftArr[3]);
         this._leftArr[3] = null;
         ObjectUtils.disposeObject(this._leftArr[4]);
         this._leftArr[4] = null;
         ObjectUtils.disposeObject(this._leftArr[5]);
         this._leftArr[5] = null;
      }
   }
}
