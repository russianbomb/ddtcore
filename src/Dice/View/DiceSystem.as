package Dice.View
{
   import Dice.Controller.DiceController;
   import com.pickgliss.manager.CacheSysManager;
   import ddt.constants.CacheConsts;
   import ddt.manager.KeyboardShortcutsManager;
   import ddt.states.BaseStateView;
   import ddt.states.StateType;
   
   public class DiceSystem extends BaseStateView
   {
       
      
      private var _diceView:DiceSystemView;
      
      public function DiceSystem()
      {
         super();
      }
      
      override public function enter(param1:BaseStateView, param2:Object = null) : void
      {
         CacheSysManager.lock(CacheConsts.ALERT_IN_DICESYSTEM);
         KeyboardShortcutsManager.Instance.forbiddenFull();
         this._diceView = new DiceSystemView(DiceController.Instance);
         addChild(this._diceView);
         super.enter(param1,param2);
      }
      
      override public function leaving(param1:BaseStateView) : void
      {
         CacheSysManager.unlock(CacheConsts.ALERT_IN_DICESYSTEM);
         CacheSysManager.getInstance().release(CacheConsts.ALERT_IN_DICESYSTEM);
         KeyboardShortcutsManager.Instance.cancelForbidden();
         super.leaving(param1);
      }
      
      override public function getType() : String
      {
         return StateType.DICE_SYSTEM;
      }
      
      override public function getBackType() : String
      {
         return StateType.MAIN;
      }
      
      override public function dispose() : void
      {
         if(this._diceView)
         {
            this._diceView.dispose();
            this._diceView = null;
         }
      }
   }
}
