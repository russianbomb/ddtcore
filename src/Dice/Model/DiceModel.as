package Dice.Model
{
   import Dice.Controller.DiceController;
   import Dice.Event.DiceEvent;
   import Dice.VO.DiceCell;
   import Dice.VO.DiceCellInfo;
   import com.pickgliss.ui.ComponentFactory;
   import flash.events.EventDispatcher;
   import flash.events.IEventDispatcher;
   
   public class DiceModel extends EventDispatcher
   {
       
      
      private var _CELL_COUNT:int = 20;
      
      private var _popupAlert:int = 12;
      
      private var _diceType:int = 0;
      
      private var _freeCount:int = 0;
      
      private var _refreshPrice:int;
      
      private var _commonDicePrice:int;
      
      private var _doubleDicePrice:int;
      
      private var _bigDicePrice:int;
      
      private var _smallDicePrice:int;
      
      private var _MAX_LEVEL:int = 5;
      
      private var _levelInfo:Array;
      
      private var _LuckIntegral_Level:int = 0;
      
      private var _LuckIntegral:int = 0;
      
      private var _currentPosition:int = 0;
      
      private var _cellItem:Array;
      
      private var _cellPosition:Array;
      
      private var _useFirstCell:Boolean = false;
      
      private var _isPlayDownMovie:Boolean = false;
      
      private var _rewardItems:Array;
      
      public function DiceModel(param1:IEventDispatcher = null)
      {
         this.initialize();
         super(param1);
      }
      
      public function get rewardItems() : Array
      {
         return this._rewardItems;
      }
      
      public function set rewardItems(param1:Array) : void
      {
         this._rewardItems = param1;
      }
      
      public function get popupAlert() : int
      {
         return this._popupAlert;
      }
      
      public function set popupAlert(param1:int) : void
      {
         this._popupAlert = param1;
      }
      
      public function get isPlayDownMovie() : Boolean
      {
         return this._isPlayDownMovie;
      }
      
      public function set isPlayDownMovie(param1:Boolean) : void
      {
         this._isPlayDownMovie = param1;
      }
      
      public function get diceType() : int
      {
         return this._diceType;
      }
      
      public function set diceType(param1:int) : void
      {
         this._diceType = param1;
      }
      
      public function get levelInfo() : Array
      {
         return this._levelInfo;
      }
      
      public function set levelInfo(param1:Array) : void
      {
         this._levelInfo = param1;
      }
      
      public function get MAX_LEVEL() : int
      {
         return this._MAX_LEVEL;
      }
      
      public function set MAX_LEVEL(param1:int) : void
      {
         this._MAX_LEVEL = param1;
         this._levelInfo = [];
      }
      
      public function get smallDicePrice() : int
      {
         return this._smallDicePrice;
      }
      
      public function set smallDicePrice(param1:int) : void
      {
         this._smallDicePrice = param1;
      }
      
      public function get bigDicePrice() : int
      {
         return this._bigDicePrice;
      }
      
      public function set bigDicePrice(param1:int) : void
      {
         this._bigDicePrice = param1;
      }
      
      public function get doubleDicePrice() : int
      {
         return this._doubleDicePrice;
      }
      
      public function set doubleDicePrice(param1:int) : void
      {
         this._doubleDicePrice = param1;
      }
      
      public function get commonDicePrice() : int
      {
         return this._commonDicePrice;
      }
      
      public function set commonDicePrice(param1:int) : void
      {
         this._commonDicePrice = param1;
      }
      
      public function get refreshPrice() : int
      {
         return this._refreshPrice;
      }
      
      public function set refreshPrice(param1:int) : void
      {
         this._refreshPrice = param1;
      }
      
      public function get freeCount() : int
      {
         return this._freeCount;
      }
      
      public function set freeCount(param1:int) : void
      {
         if(this._freeCount != param1)
         {
            if(param1 < 0)
            {
               param1 = 0;
            }
            this._freeCount = param1;
            DiceController.Instance.dispatchEvent(new DiceEvent(DiceEvent.CHANGED_FREE_COUNT));
         }
      }
      
      public function get LuckIntegralLevel() : int
      {
         return this._LuckIntegral_Level;
      }
      
      public function set LuckIntegralLevel(param1:int) : void
      {
         if(this._LuckIntegral_Level != param1 + 1)
         {
            this._LuckIntegral_Level = param1 + 1;
         }
      }
      
      public function get CELL_COUNT() : int
      {
         return this._CELL_COUNT;
      }
      
      public function get userFirstCell() : Boolean
      {
         return this._useFirstCell;
      }
      
      public function set cellCount(param1:int) : void
      {
         this._CELL_COUNT = param1;
         this._cellItem = [];
         this._cellPosition = [];
      }
      
      public function set userFirstCell(param1:Boolean) : void
      {
         this._useFirstCell = param1;
      }
      
      private function initialize() : void
      {
      }
      
      public function setCellInfo() : void
      {
         var _loc1_:DiceCellInfo = null;
         var _loc2_:int = 0;
         while(_loc2_ < this._CELL_COUNT)
         {
            if(this._cellPosition[_loc2_])
            {
               this._cellPosition[_loc2_].dispose();
               this._cellPosition[_loc2_] = null;
            }
            _loc1_ = ComponentFactory.Instance.creatCustomObject("asset.dice.cellInfo." + (_loc2_ + 1));
            this._cellPosition[_loc2_] = _loc1_;
            _loc2_++;
         }
      }
      
      public function get LuckIntegral() : int
      {
         return this._LuckIntegral;
      }
      
      public function set LuckIntegral(param1:int) : void
      {
         if(this._LuckIntegral != param1)
         {
            this._LuckIntegral = param1;
         }
         DiceController.Instance.dispatchEvent(new DiceEvent(DiceEvent.CHANGED_LUCKINTEGRAL));
      }
      
      public function get currentPosition() : int
      {
         return this._currentPosition;
      }
      
      public function set currentPosition(param1:int) : void
      {
         if(param1 < 0)
         {
            param1 = 0;
         }
         else if(param1 >= this._CELL_COUNT)
         {
            param1 = this._CELL_COUNT - 1;
         }
         if(param1 != DiceController.Instance.CurrentPosition)
         {
            this._currentPosition = param1;
            DiceController.Instance.dispatchEvent(new DiceEvent(DiceEvent.CHANGED_PLAYER_POSITION));
         }
      }
      
      public function get cellIDs() : Array
      {
         return this._cellItem;
      }
      
      public function get cellPosition() : Array
      {
         return this._cellPosition;
      }
      
      public function addCellItem(param1:DiceCell) : void
      {
         this._cellItem.push(param1);
      }
      
      public function removeCellItem(param1:int) : void
      {
         if(param1 < this._cellItem.length)
         {
            if(this._cellItem[param1])
            {
               this._cellItem[param1].dispose();
            }
            this._cellItem[param1] = null;
            this._cellItem.splice(param1,1);
         }
      }
      
      public function removeAllItem() : void
      {
         var _loc1_:int = 0;
         if(this._cellItem)
         {
            _loc1_ = this._cellItem.length;
            while(_loc1_ > 0)
            {
               this.removeCellItem(_loc1_ - 1);
               _loc1_--;
            }
         }
      }
      
      public function dispose() : void
      {
         this._cellItem = null;
         this._cellPosition = null;
      }
   }
}
