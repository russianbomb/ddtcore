package Dice.Controller
{
   import Dice.Event.DiceEvent;
   import Dice.Model.DiceModel;
   import Dice.VO.DiceAwardInfo;
   import Dice.VO.DiceCell;
   import com.pickgliss.ui.ComponentFactory;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.ItemManager;
   import ddt.manager.SocketManager;
   import ddt.manager.StateManager;
   import ddt.states.StateType;
   import flash.display.MovieClip;
   import flash.events.EventDispatcher;
   import flash.events.IEventDispatcher;
   import road7th.comm.PackageIn;
   
   [Event(name="dice_refresh_data",type="Dice.Event.DiceEvent")]
   public class DiceController extends EventDispatcher
   {
      
      private static var _instance:DiceController;
       
      
      private var _model:DiceModel;
      
      private var _isFirst:Boolean = true;
      
      public function DiceController(param1:IEventDispatcher = null)
      {
         super(param1);
      }
      
      public static function get Instance() : DiceController
      {
         if(_instance == null)
         {
            _instance = new DiceController();
         }
         return _instance;
      }
      
      public function set isFirst(param1:Boolean) : void
      {
         this._isFirst = param1;
      }
      
      public function get hasUsedFirstCell() : Boolean
      {
         return this._model.userFirstCell;
      }
      
      public function get CELL_COUNT() : int
      {
         return this._model.CELL_COUNT;
      }
      
      public function get cellIDs() : Array
      {
         return this._model.cellIDs;
      }
      
      public function get MAX_LEVEL() : int
      {
         return this._model.MAX_LEVEL;
      }
      
      public function get freeCount() : int
      {
         return this._model.freeCount;
      }
      
      public function set freeCount(param1:int) : void
      {
         this._model.freeCount = param1;
      }
      
      public function get diceType() : int
      {
         return this._model.diceType;
      }
      
      public function get commonDicePrice() : int
      {
         return this._model.commonDicePrice;
      }
      
      public function get doubleDicePrice() : int
      {
         return this._model.doubleDicePrice;
      }
      
      public function get bigDicePrice() : int
      {
         return this._model.bigDicePrice;
      }
      
      public function get smallDicePrice() : int
      {
         return this._model.smallDicePrice;
      }
      
      public function get refreshPrice() : int
      {
         return this._model.refreshPrice;
      }
      
      public function get canPopupNextRefreshWindow() : Boolean
      {
         return (this._model.popupAlert & 1) == 1;
      }
      
      public function get canPopupNextStartWindow() : Boolean
      {
         return (this._model.popupAlert & 2) == 2;
      }
      
      public function get rewardItems() : Array
      {
         return this._model.rewardItems;
      }
      
      public function set rewardItems(param1:Array) : void
      {
         this._model.rewardItems = param1;
      }
      
      public function setPopupNextRefreshWindow(param1:Boolean) : void
      {
         if(param1)
         {
            this._model.popupAlert = this._model.popupAlert | 1;
         }
         else
         {
            this._model.popupAlert = this._model.popupAlert & 14;
         }
      }
      
      public function setPopupNextStartWindow(param1:Boolean) : void
      {
         if(param1)
         {
            this._model.popupAlert = this._model.popupAlert | 2;
         }
         else
         {
            this._model.popupAlert = this._model.popupAlert & 13;
         }
      }
      
      public function cannotPopupNextStartWindow() : void
      {
         this._model.popupAlert = this._model.popupAlert | 2;
      }
      
      public function set isPlayDownMovie(param1:Boolean) : void
      {
         this._model.isPlayDownMovie = param1;
      }
      
      public function get isPlayDownMovie() : Boolean
      {
         return this._model.isPlayDownMovie;
      }
      
      public function setDestinationCell(param1:int) : void
      {
         var _loc2_:Array = this._model.cellIDs;
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_.length)
         {
            if(_loc2_[_loc3_])
            {
               if(_loc3_ != param1)
               {
                  _loc2_[_loc3_].isDestination = false;
               }
               else
               {
                  _loc2_[_loc3_].isDestination = true;
               }
            }
            _loc3_++;
         }
      }
      
      public function set diceType(param1:int) : void
      {
         this._model.diceType = param1;
      }
      
      public function get LuckIntegralLevel() : int
      {
         return this._model.LuckIntegralLevel;
      }
      
      public function get canUseModel() : Boolean
      {
         if(this._model != null)
         {
            return true;
         }
         return false;
      }
      
      public function set LuckIntegralLevel(param1:int) : void
      {
         this._model.LuckIntegralLevel = param1;
      }
      
      public function get LuckIntegral() : int
      {
         return this._model.LuckIntegral;
      }
      
      public function get cellPosition() : Array
      {
         return this._model.cellPosition;
      }
      
      public function get CurrentPosition() : int
      {
         return this._model.currentPosition;
      }
      
      public function set CurrentPosition(param1:int) : void
      {
         this._model.currentPosition = param1;
      }
      
      public function setCellInfo() : void
      {
         this._model.setCellInfo();
      }
      
      public function get AwardLevelInfo() : Array
      {
         return this._model.levelInfo;
      }
      
      public function install(param1:PackageIn) : void
      {
         this.initialize(param1);
         this.addEvent();
      }
      
      public function unInstall() : void
      {
         dispatchEvent(new DiceEvent(DiceEvent.ACTIVE_CLOSE));
         this.removeEvent();
         if(this._model)
         {
            this._model.dispose();
            this._model = null;
         }
      }
      
      private function initialize(param1:PackageIn) : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:String = null;
         var _loc6_:int = 0;
         this._model = new DiceModel();
         this._model.freeCount = param1.readInt();
         this._model.refreshPrice = param1.readInt();
         this._model.commonDicePrice = param1.readInt();
         this._model.doubleDicePrice = param1.readInt();
         this._model.bigDicePrice = param1.readInt();
         this._model.smallDicePrice = param1.readInt();
         this._model.MAX_LEVEL = param1.readInt();
         var _loc5_:int = 0;
         while(_loc5_ < this._model.MAX_LEVEL)
         {
            _loc2_ = param1.readInt();
            _loc3_ = param1.readInt();
            _loc4_ = "";
            _loc6_ = 0;
            while(_loc6_ < _loc3_)
            {
               _loc4_ = _loc4_ + ("," + param1.readInt() + "|" + param1.readInt());
               _loc6_++;
            }
            _loc4_ = _loc4_.substring(1);
            this._model.levelInfo[_loc5_] = new DiceAwardInfo(_loc5_ + 1,_loc2_,_loc4_);
            _loc5_++;
         }
      }
      
      private function addEvent() : void
      {
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.DICE_RECEIVE_DATA,this.__onReceiveData);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.DICE_RECEIVE_RESULT,this.__receiveResult);
      }
      
      private function removeEvent() : void
      {
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.DICE_RECEIVE_DATA,this.__onReceiveData);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.DICE_RECEIVE_RESULT,this.__receiveResult);
      }
      
      private function __onReceiveData(param1:CrazyTankSocketEvent) : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc2_:PackageIn = param1.pkg;
         this._model.userFirstCell = _loc2_.readBoolean();
         this._model.currentPosition = _loc2_.readInt() + 1;
         _loc3_ = _loc2_.readInt();
         _loc4_ = _loc2_.readInt();
         if(this._isFirst)
         {
            this._model.LuckIntegralLevel = _loc3_;
            this._isFirst = false;
         }
         if(DiceController.Instance.LuckIntegralLevel != _loc3_ + 1 || _loc3_ == -1 && _loc4_ < this._model.LuckIntegral)
         {
            this._model.LuckIntegralLevel = _loc3_;
            this._model.isPlayDownMovie = true;
            dispatchEvent(new DiceEvent(DiceEvent.CHANGED_LUCKINTEGRAL_LEVEL));
         }
         this._model.LuckIntegral = _loc4_;
         this._model.freeCount = _loc2_.readInt();
         this.ReceiveListByPkg(_loc2_);
      }
      
      private function ReceiveListByPkg(param1:PackageIn) : void
      {
         var _loc2_:DiceCell = null;
         var _loc3_:MovieClip = null;
         var _loc4_:MovieClip = null;
         var _loc6_:ItemTemplateInfo = null;
         var _loc7_:int = 0;
         var _loc5_:int = !!this._model.userFirstCell?int(0):int(1);
         this._model.removeAllItem();
         this._model.cellCount = _loc7_ = param1.readInt() + _loc5_;
         _loc7_ = _loc7_ - _loc5_;
         this._model.setCellInfo();
         var _loc8_:int = 0;
         while(_loc8_ < _loc7_)
         {
            _loc3_ = ComponentFactory.Instance.creat("asset.dice.bg" + (_loc5_ + _loc8_ + 1));
            _loc4_ = ComponentFactory.Instance.creat("asset.cell.mask" + (_loc5_ + _loc8_));
            _loc6_ = ItemManager.Instance.getTemplateById(param1.readInt());
            _loc2_ = new DiceCell(_loc3_,this._model.cellPosition[_loc8_ + _loc5_],_loc6_,_loc4_);
            _loc2_.position = param1.readInt();
            _loc2_.strengthLevel = param1.readInt();
            _loc2_.count = param1.readInt();
            _loc2_.validate = param1.readInt();
            _loc2_.isBind = param1.readBoolean();
            this._model.addCellItem(_loc2_);
            _loc8_++;
         }
         dispatchEvent(new DiceEvent(DiceEvent.REFRESH_DATA));
         if(StateManager.currentStateType != StateType.DICE_SYSTEM)
         {
            StateManager.setState(StateType.DICE_SYSTEM);
         }
      }
      
      private function __receiveResult(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:Object = new Object();
         _loc3_.position = _loc2_.readInt() + 1;
         _loc3_.result = _loc2_.readInt();
         _loc3_.luckIntegral = _loc2_.readInt();
         _loc3_.level = _loc2_.readInt();
         _loc3_.freeCount = _loc2_.readInt();
         _loc3_.rewardItem = _loc2_.readUTF();
         if(DiceController.Instance.CurrentPosition != _loc3_.position)
         {
            DiceController.Instance.CurrentPosition = _loc3_.position;
         }
         this._model.freeCount = _loc3_.freeCount;
         if(DiceController.Instance.LuckIntegralLevel != _loc3_.level + 1 || _loc3_.level == -1 && this._model.LuckIntegral > _loc3_.luckIntegral)
         {
            this._model.LuckIntegralLevel = _loc3_.level;
            this._model.isPlayDownMovie = true;
            dispatchEvent(new DiceEvent(DiceEvent.CHANGED_LUCKINTEGRAL_LEVEL));
         }
         this._model.LuckIntegral = _loc3_.luckIntegral;
         dispatchEvent(new DiceEvent(DiceEvent.GET_DICE_RESULT_DATA,_loc3_));
      }
   }
}
