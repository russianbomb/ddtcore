package Dice.VO
{
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.manager.ItemManager;
   import flash.display.Bitmap;
   
   public class DiceAwardInfo
   {
       
      
      private var _level:int;
      
      private var _integral:int;
      
      private var _templateInfo:Vector.<DiceAwardCell>;
      
      public function DiceAwardInfo(param1:int, param2:int, param3:String)
      {
         super();
         this._templateInfo = new Vector.<DiceAwardCell>();
         this._level = param1;
         this._integral = param2;
         this.setTemplateInfo = param3;
      }
      
      public function get level() : int
      {
         return this._level;
      }
      
      public function get integral() : int
      {
         return this._integral;
      }
      
      public function get templateInfo() : Vector.<DiceAwardCell>
      {
         return this._templateInfo;
      }
      
      private function set setTemplateInfo(param1:String) : void
      {
         var _loc3_:DiceAwardCell = null;
         var _loc4_:ItemTemplateInfo = null;
         var _loc5_:Bitmap = null;
         var _loc6_:String = null;
         if(param1 == null || param1 == "")
         {
            return;
         }
         var _loc2_:Array = param1.split(",");
         var _loc7_:int = _loc2_.length;
         while(_loc7_ > 0)
         {
            _loc6_ = _loc2_[_loc7_ - 1];
            if(_loc6_ != null && _loc6_ != "")
            {
               _loc4_ = ItemManager.Instance.getTemplateById(int(_loc6_.split("|")[0]));
               _loc3_ = new DiceAwardCell(_loc4_,int(_loc6_.split("|")[1]));
               this._templateInfo.push(_loc3_);
            }
            _loc7_--;
         }
      }
      
      public function dispose() : void
      {
         var _loc1_:int = this._templateInfo.length;
         while(_loc1_ > 0)
         {
            if(this._templateInfo[_loc1_ - 1])
            {
               this._templateInfo[_loc1_ - 1].dispose();
               this._templateInfo[_loc1_ - 1] = null;
            }
            _loc1_--;
         }
         this._templateInfo = null;
      }
   }
}
