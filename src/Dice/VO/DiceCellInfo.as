package Dice.VO
{
   import flash.geom.Point;
   
   public class DiceCellInfo
   {
       
      
      private var _position:Point;
      
      private var _vertices1:Point;
      
      private var _vertices2:Point;
      
      private var _vertices3:Point;
      
      private var _centerPosition:Point;
      
      public function DiceCellInfo()
      {
         super();
         this._position = new Point();
         this._centerPosition = new Point();
         this._vertices1 = new Point(0,0);
         this._vertices2 = new Point(0,0);
         this._vertices3 = new Point(0,0);
      }
      
      public function get CellCenterPosition() : Point
      {
         return this._centerPosition;
      }
      
      public function set centerPosition(param1:String) : void
      {
         var _loc2_:Array = param1.split(",");
         this._centerPosition.x = int(_loc2_[0]);
         this._centerPosition.y = int(_loc2_[1]);
      }
      
      public function get Position() : Point
      {
         return this._position;
      }
      
      public function set position(param1:String) : void
      {
         var _loc2_:Array = param1.split(",");
         this._position.x = int(_loc2_[0]);
         this._position.y = int(_loc2_[1]);
      }
      
      public function set verticesString(param1:String) : void
      {
         var _loc2_:Array = param1.split(",");
         this._vertices1.x = _loc2_[0] == null?Number(0):Number(Number(_loc2_[0]));
         this._vertices1.y = _loc2_[1] == null?Number(0):Number(Number(_loc2_[1]));
         this._vertices2.x = _loc2_[2] == null?Number(0):Number(Number(_loc2_[2]));
         this._vertices2.y = _loc2_[3] == null?Number(0):Number(Number(_loc2_[3]));
         this._vertices3.x = _loc2_[4] == null?Number(0):Number(Number(_loc2_[4]));
         this._vertices3.y = _loc2_[5] == null?Number(0):Number(Number(_loc2_[5]));
      }
      
      public function get vertices1() : Point
      {
         return this._vertices1;
      }
      
      public function get vertices2() : Point
      {
         return this._vertices2;
      }
      
      public function get vertices3() : Point
      {
         return this._vertices3;
      }
      
      public function dispose() : void
      {
         this._position = null;
         this._vertices1 = null;
         this._vertices2 = null;
         this._vertices3 = null;
      }
   }
}
