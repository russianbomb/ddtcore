package Dice.VO
{
   import Dice.Controller.DiceController;
   import Dice.Event.DiceEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.ui.text.GradientText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.player.SelfInfo;
   import ddt.events.SceneCharacterEvent;
   import ddt.manager.PlayerManager;
   import ddt.utils.PositionUtils;
   import ddt.view.common.VipLevelIcon;
   import ddt.view.sceneCharacter.SceneCharacterDirection;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.geom.Point;
   import vip.VipController;
   
   public class DicePlayer extends DicePlayerBase
   {
       
      
      private var _playerInfo:SelfInfo;
      
      private var _lblName:FilterFrameText;
      
      private var _vipName:GradientText;
      
      private var _vipIcon:VipLevelIcon;
      
      private var _moveSpeedX:Number = 5;
      
      private var _moveSpeedY:Number = 4;
      
      private var _isShowName:Boolean = true;
      
      private var _propertyContainer:Sprite;
      
      private var _light:MovieClip;
      
      private var _isWalking:Boolean;
      
      public function DicePlayer(param1:Function = null)
      {
         this._playerInfo = PlayerManager.Instance.Self;
         super(this._playerInfo,this.SynchronousPosition,param1);
         this.preInitialize();
         this.initialize();
         this.addEvent();
      }
      
      public function get isWalking() : Boolean
      {
         return this._isWalking;
      }
      
      public function set isWalking(param1:Boolean) : void
      {
         var _loc2_:Object = null;
         if(this._isWalking != param1)
         {
            this._isWalking = param1;
            _loc2_ = new Object();
            _loc2_.isWalking = this._isWalking;
            DiceController.Instance.dispatchEvent(new DiceEvent(DiceEvent.PLAYER_ISWALKING,_loc2_));
            if(this._isWalking)
            {
               this._light.visible = false;
            }
            else
            {
               this._light.visible = true;
            }
         }
      }
      
      private function preInitialize() : void
      {
         this._propertyContainer = new Sprite();
         this._lblName = ComponentFactory.Instance.creatComponentByStylename("asset.dice.lblName");
         this._light = ComponentFactory.Instance.creat("asset.dice.destinationLight");
         PositionUtils.setPos(this._light,"asset.dice.playerlight.pos");
      }
      
      private function addEvent() : void
      {
         addEventListener(SceneCharacterEvent.CHARACTER_DIRECTION_CHANGE,this.characterDirectionChange);
         addEventListener(Event.ENTER_FRAME,this.__update);
      }
      
      private function characterDirectionChange(param1:SceneCharacterEvent) : void
      {
         this.isWalking = Boolean(param1.data);
         if(this._isWalking)
         {
            if(sceneCharacterDirection == SceneCharacterDirection.LT || sceneCharacterDirection == SceneCharacterDirection.RT)
            {
               if(sceneCharacterStateType == "natural")
               {
                  character.scaleX = 1;
                  character.x = 0;
                  sceneCharacterActionType = "naturalWalkBack";
               }
            }
            else if(sceneCharacterDirection == SceneCharacterDirection.RB)
            {
               if(sceneCharacterStateType == "natural")
               {
                  character.scaleX = 1;
                  character.x = 0;
                  sceneCharacterActionType = "naturalWalkFront";
               }
            }
            else if(sceneCharacterDirection.type == "LB")
            {
               if(sceneCharacterStateType == "natural")
               {
                  if(sceneCharacterDirection.isMirror)
                  {
                     sceneCharacterActionType = "naturalWalkFront";
                     character.scaleX = -1;
                     character.x = _playerWidth;
                  }
                  else
                  {
                     character.scaleX = 1;
                     character.x = 0;
                     sceneCharacterActionType = "naturalWalkFront";
                  }
               }
            }
         }
         else if(sceneCharacterDirection == SceneCharacterDirection.LT || sceneCharacterDirection == SceneCharacterDirection.RT)
         {
            if(sceneCharacterStateType == "natural")
            {
               character.scaleX = 1;
               character.x = 0;
               sceneCharacterActionType = "naturalStandBack";
            }
         }
         else if(sceneCharacterDirection == SceneCharacterDirection.RB)
         {
            if(sceneCharacterStateType == "natural")
            {
               character.scaleX = 1;
               character.x = 0;
               sceneCharacterActionType = "naturalStandFront";
            }
         }
         else if(sceneCharacterDirection.type == "LB")
         {
            if(sceneCharacterStateType == "natural")
            {
               if(sceneCharacterDirection.isMirror)
               {
                  character.scaleX = -1;
                  character.x = _playerWidth;
                  sceneCharacterActionType = "naturalStandFront";
               }
               else
               {
                  character.scaleX = 1;
                  character.x = 0;
                  sceneCharacterActionType = "naturalStandFront";
               }
            }
         }
      }
      
      private function __update(param1:Event) : void
      {
         if(character)
         {
            update();
         }
      }
      
      private function removeEvent() : void
      {
         removeEventListener(Event.ENTER_FRAME,this.__update);
         removeEventListener(SceneCharacterEvent.CHARACTER_DIRECTION_CHANGE,this.characterDirectionChange);
      }
      
      private function initialize() : void
      {
         mouseEnabled = false;
         mouseChildren = false;
         addChildAt(this._propertyContainer,0);
         addChild(this._light);
         this._lblName.text = this._playerInfo && this._playerInfo.NickName?this._playerInfo.NickName:"";
         this._lblName.textColor = 6029065;
         if(this._playerInfo.IsVIP)
         {
            this._vipName = VipController.instance.getVipNameTxt(-1,this._playerInfo.typeVIP);
            this._vipName.x = this._lblName.x;
            this._vipName.y = this._lblName.y;
            this._vipName.text = this._lblName.text;
            this._vipIcon = ComponentFactory.Instance.creatCustomObject("asset.dic.VipIcon");
            this._vipIcon.setInfo(this._playerInfo);
            this._propertyContainer.addChild(this._vipName);
            this._propertyContainer.addChild(this._vipIcon);
            this._vipName.visible = this._vipIcon.visible = this._isShowName;
            this._lblName.dispose();
            this._lblName = null;
         }
         else
         {
            this._propertyContainer.addChild(this._lblName);
            this._lblName.visible = this._isShowName;
         }
      }
      
      private function SynchronousPosition(param1:Point) : void
      {
      }
      
      public function PlayerWalkByPosition(param1:int, param2:int) : void
      {
         var _loc4_:Point = null;
         var _loc3_:Array = [];
         if(param1 < 0 || param1 >= DiceController.Instance.CELL_COUNT)
         {
            return;
         }
         if(param2 == 0 && !DiceController.Instance.hasUsedFirstCell)
         {
            param2 = 1;
         }
         _loc3_.push(this.GetCoordinatesByPosition(param1));
         while(param1 != param2)
         {
            param1 = ++param1 % DiceController.Instance.CELL_COUNT;
            _loc3_.push(this.GetCoordinatesByPosition(param1));
         }
         playerWalk(_loc3_);
      }
      
      public function GetCoordinatesByPosition(param1:int) : Point
      {
         var _loc2_:DiceCell = null;
         if(param1 < 0)
         {
            param1 = 0;
         }
         else if(param1 >= DiceController.Instance.CELL_COUNT)
         {
            param1 = DiceController.Instance.CELL_COUNT - 1;
         }
         if(DiceController.Instance.cellPosition == null)
         {
            DiceController.Instance.setCellInfo();
         }
         return (DiceController.Instance.cellPosition[param1] as DiceCellInfo).CellCenterPosition;
      }
      
      public function set CurrentPosition(param1:int) : void
      {
         var _loc2_:Point = null;
         _loc2_ = this.GetCoordinatesByPosition(param1);
         x = _loc2_.x;
         y = _loc2_.y;
      }
      
      override public function dispose() : void
      {
         this.removeEvent();
         ObjectUtils.disposeObject(this._lblName);
         this._lblName = null;
         ObjectUtils.disposeObject(this._vipName);
         this._vipName = null;
         ObjectUtils.disposeObject(this._vipIcon);
         this._vipIcon = null;
         super.dispose();
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
