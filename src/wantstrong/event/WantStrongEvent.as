package wantstrong.event
{
   import flash.events.Event;
   
   public class WantStrongEvent extends Event
   {
      
      public static const ALREADYFINDBACK:String = "alreadyFindBack";
      
      public static const ALREADYUPDATETIME:String = "alreadyUpdateTime";
       
      
      public function WantStrongEvent(param1:String, param2:Boolean = false, param3:Boolean = false)
      {
         super(param1,param2,param3);
      }
   }
}
