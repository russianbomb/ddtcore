package wantstrong.view
{
   import com.pickgliss.ui.core.Disposeable;
   import ddt.manager.LanguageMgr;
   import ddt.manager.SoundManager;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import wantstrong.WantStrongManager;
   import wantstrong.data.WantStrongModel;
   
   public class WantStrongMenu extends Sprite implements Disposeable
   {
       
      
      private var _menuArr:Array;
      
      private var _titleArr:Array;
      
      private var _selectItem:WantStrongCell;
      
      private var _cellArr:Array;
      
      private var _model:WantStrongModel;
      
      public function WantStrongMenu(param1:WantStrongModel)
      {
         this._menuArr = [1,2,3,4,5];
         this._titleArr = [LanguageMgr.GetTranslation("ddt.wantStrong.view.itemText"),LanguageMgr.GetTranslation("ddt.wantStrong.view.levelUp"),LanguageMgr.GetTranslation("ddt.wantStrong.view.earnMoney"),LanguageMgr.GetTranslation("ddt.wantStrong.view.artifact"),LanguageMgr.GetTranslation("ddt.wantStrong.view.findBack")];
         this._cellArr = [];
         super();
         WantStrongManager.Instance.addEventListener("cellChange",this.cellChangeHandler);
         this._model = param1;
         this.createUI();
      }
      
      private function cellChangeHandler(param1:Event) : void
      {
         if(this._model.data[5].length == 0)
         {
            removeChildAt(4);
            WantStrongManager.Instance.findBackExist = false;
            this.setSelectItem(this._cellArr[this._model.activeId - 1]);
         }
      }
      
      private function createUI() : void
      {
         var _loc3_:WantStrongCell = null;
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         while(_loc2_ < this._menuArr.length)
         {
            if(this._model.data[this._menuArr[_loc2_]])
            {
               _loc3_ = new WantStrongCell(this._model.data[this._menuArr[_loc2_]],this._titleArr[_loc2_]);
               _loc3_.y = _loc1_ * 54;
               _loc3_.addEventListener(MouseEvent.CLICK,this._cellClickedHandle);
               addChild(_loc3_);
               this._cellArr.push(_loc3_);
               _loc1_++;
            }
            _loc2_++;
         }
         if(this._cellArr.length > 0)
         {
            this.setSelectItem(this._cellArr[this._model.activeId - 1]);
            WantStrongManager.Instance.setinitState(this._model.data[this._model.activeId]);
         }
      }
      
      protected function _cellClickedHandle(param1:MouseEvent) : void
      {
         var _loc2_:WantStrongCell = param1.currentTarget as WantStrongCell;
         this.setSelectItem(_loc2_);
         WantStrongManager.Instance.setCurrentInfo(_loc2_.info);
         SoundManager.instance.play("008");
      }
      
      private function setSelectItem(param1:WantStrongCell) : void
      {
         if(param1 != this._selectItem)
         {
            if(this._selectItem)
            {
               this._selectItem.selected = false;
            }
            this._selectItem = param1;
            this._selectItem.selected = true;
         }
      }
      
      public function dispose() : void
      {
         var _loc2_:WantStrongCell = null;
         WantStrongManager.Instance.removeEventListener("cellChange",this.cellChangeHandler);
         this._menuArr = null;
         this._titleArr = null;
         if(this._selectItem)
         {
            this._selectItem.dispose();
            this._selectItem = null;
         }
         var _loc1_:int = 0;
         while(_loc1_ < this._cellArr.length)
         {
            _loc2_ = this._cellArr[_loc1_];
            if(_loc2_)
            {
               _loc2_.removeEventListener(MouseEvent.CLICK,this._cellClickedHandle);
               _loc2_.dispose();
               _loc2_ = null;
            }
            _loc1_++;
         }
         this._cellArr = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
