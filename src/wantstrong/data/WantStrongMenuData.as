package wantstrong.data
{
   public class WantStrongMenuData
   {
       
      
      private var _id:int;
      
      private var _type:int;
      
      private var _freeBackBtnEnable:Boolean = true;
      
      private var _allBackBtnEnable:Boolean = true;
      
      private var _bossType:int;
      
      private var _title:String;
      
      private var _starNum:int;
      
      private var _description:String;
      
      private var _needLevel:int;
      
      private var _iconUrl:String;
      
      private var _awardType:int;
      
      private var _awardNum:int;
      
      private var _moneyNum:int;
      
      public function WantStrongMenuData()
      {
         super();
      }
      
      public function get iconUrl() : String
      {
         return this._iconUrl;
      }
      
      public function set iconUrl(param1:String) : void
      {
         this._iconUrl = param1;
      }
      
      public function get moneyNum() : int
      {
         return this._moneyNum;
      }
      
      public function set moneyNum(param1:int) : void
      {
         this._moneyNum = param1;
      }
      
      public function get awardType() : int
      {
         return this._awardType;
      }
      
      public function set awardType(param1:int) : void
      {
         this._awardType = param1;
      }
      
      public function get awardNum() : int
      {
         return this._awardNum;
      }
      
      public function set awardNum(param1:int) : void
      {
         this._awardNum = param1;
      }
      
      public function get allBackBtnEnable() : Boolean
      {
         return this._allBackBtnEnable;
      }
      
      public function set allBackBtnEnable(param1:Boolean) : void
      {
         this._allBackBtnEnable = param1;
      }
      
      public function get freeBackBtnEnable() : Boolean
      {
         return this._freeBackBtnEnable;
      }
      
      public function set freeBackBtnEnable(param1:Boolean) : void
      {
         this._freeBackBtnEnable = param1;
      }
      
      public function get bossType() : int
      {
         return this._bossType;
      }
      
      public function set bossType(param1:int) : void
      {
         this._bossType = param1;
      }
      
      public function get type() : int
      {
         return this._type;
      }
      
      public function set type(param1:int) : void
      {
         this._type = param1;
      }
      
      public function get needLevel() : int
      {
         return this._needLevel;
      }
      
      public function set needLevel(param1:int) : void
      {
         this._needLevel = param1;
      }
      
      public function get description() : String
      {
         return this._description;
      }
      
      public function set description(param1:String) : void
      {
         this._description = param1;
      }
      
      public function get starNum() : int
      {
         return this._starNum;
      }
      
      public function set starNum(param1:int) : void
      {
         this._starNum = param1;
      }
      
      public function get title() : String
      {
         return this._title;
      }
      
      public function set title(param1:String) : void
      {
         this._title = param1;
      }
      
      public function get id() : int
      {
         return this._id;
      }
      
      public function set id(param1:int) : void
      {
         this._id = param1;
      }
   }
}
