package wantstrong.data
{
   import com.pickgliss.ui.core.Disposeable;
   import ddt.manager.LanguageMgr;
   import ddt.manager.PathManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerConfigManager;
   import flash.events.EventDispatcher;
   import flash.events.IEventDispatcher;
   import flash.utils.Dictionary;
   import wantstrong.WantStrongManager;
   
   public class WantStrongModel extends EventDispatcher implements Disposeable
   {
       
      
      private var _activeId:int = 1;
      
      private var _data:Dictionary;
      
      private var _worldBossStartLevel:int = -1;
      
      private var _titleArr:Array;
      
      private var _findBackTitleArr:Array;
      
      private var _descriptionArr:Array;
      
      private var _findBackDescriptionArr:Array;
      
      private var _starArr:Array;
      
      private var _findBackStarArr:Array;
      
      private var _iconArr:Array;
      
      private var _findBackIconArr:Array;
      
      private var _needLevelArr:Array;
      
      private var _findBackNeedLevelArr:Array;
      
      private var _idArr:Array;
      
      private var _bossTypeDic:Dictionary;
      
      public function WantStrongModel(param1:IEventDispatcher = null)
      {
         this._titleArr = ["ddt.wantStrong.view.texp","ddt.wantStrong.view.totem","ddt.wantStrong.view.gemstone","ddt.wantStrong.view.card","ddt.wantStrong.view.pet","ddt.wantStrong.view.equip","ddt.wantStrong.view.jewelery","ddt.wantStrong.view.gem","ddt.wantStrong.view.bead","ddt.wantStrong.view.bury","ddt.wantStrong.view.gold","ddt.wantStrong.view.power","ddt.wantStrong.view.fam","ddt.wantStrong.view.task","ddt.wantStrong.view.fight","ddt.wantStrong.view.daywright","ddt.wantStrong.view.treasure","ddt.wantStrong.view.dungeon"];
         this._findBackTitleArr = ["ddt.wantStrong.view.boss1","ddt.wantStrong.view.boss2","ddt.wantStrong.view.captain","ddt.wantStrong.view.battle","ddt.wantStrong.view.competition"];
         this._descriptionArr = ["ddt.wantStrong.view.texpDescription","ddt.wantStrong.view.totemDescription","ddt.wantStrong.view.gemstoneDescription","ddt.wantStrong.view.cardDescription","ddt.wantStrong.view.petDescription","ddt.wantStrong.view.equipDescription","ddt.wantStrong.view.jeweleryDescription","ddt.wantStrong.view.gemDescription","ddt.wantStrong.view.beadDescription","ddt.wantStrong.view.buryDescription","ddt.wantStrong.view.goldDescription","ddt.wantStrong.view.powerDescription","ddt.wantStrong.view.famDescription","ddt.wantStrong.view.taskDescription","ddt.wantStrong.view.fightDescription","ddt.wantStrong.view.daywrightDescription","ddt.wantStrong.view.treasureDescription","ddt.wantStrong.view.dungeonDescription"];
         this._findBackDescriptionArr = ["ddt.wantStrong.view.boss1Description","ddt.wantStrong.view.boss2Description","ddt.wantStrong.view.captainDescription","ddt.wantStrong.view.battleDescription","ddt.wantStrong.view.competitionDescription"];
         this._starArr = [5,5,5,4,5,5,4,5,5,5,5,4,5,5,4,5,4,5];
         this._findBackStarArr = [5,5,5,5,5];
         this._iconArr = ["wantstrong.texp","wantstrong.totem","wantstrong.gemstone","wantstrong.card","wantstrong.pet","wantstrong.equip","wantstrong.jewelery","wantstrong.gem","wantstrong.bead","wantstrong.bury","wantstrong.gold","wantstrong.power","wantstrong.fam","wantstrong.task","wantstrong.fight","wantstrong.daywright","wantstrong.treasure","wantstrong.dungeon"];
         this._findBackIconArr = ["wantstrong.boss1","wantstrong.boss2","wantstrong.captain","wantstrong.battle","wantstrong.competition"];
         this._needLevelArr = [30,20,30,20,25,8,8,8,10,25,20,30,30,8,8,8,8,18];
         this._findBackNeedLevelArr = [0,0,0,0,0];
         this._idArr = [101,102,103,104,105,106,107,108,109,110,111,112,201,202,203,301,302,401,501,502,503,504,505];
         super(param1);
         this._data = new Dictionary();
         this._bossTypeDic = new Dictionary();
         this._bossTypeDic[501] = 6;
         this._bossTypeDic[502] = 18;
         this._bossTypeDic[503] = 19;
         this._bossTypeDic[504] = 5;
         this._bossTypeDic[505] = 4;
      }
      
      public function get bossTypeDic() : Dictionary
      {
         return this._bossTypeDic;
      }
      
      public function initFindBackData() : void
      {
         var _loc1_:int = 0;
         var _loc5_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc11_:int = 0;
         var _loc12_:int = 0;
         var _loc15_:WantStrongMenuData = null;
         var _loc17_:Array = null;
         var _loc2_:* = ServerConfigManager.instance.findInfoByName("WorldBossFindBack");
         var _loc3_:Array = String(_loc2_.Value).split("|");
         var _loc4_:int = 0;
         while(_loc4_ < _loc3_.length)
         {
            _loc17_ = _loc3_[_loc4_].split(",");
            if(this._worldBossStartLevel == -1)
            {
               this._worldBossStartLevel = int(_loc17_[0]);
            }
            if(PlayerManager.Instance.Self.Grade >= _loc17_[0] && PlayerManager.Instance.Self.Grade <= _loc17_[1])
            {
               _loc1_ = _loc17_[2];
               break;
            }
            _loc4_++;
         }
         var _loc6_:* = ServerConfigManager.instance.findInfoByName("WorldBossFindBackMoney");
         var _loc7_:Boolean = false;
         _loc5_ = _loc6_.Value * _loc1_ / 100;
         var _loc10_:* = ServerConfigManager.instance.findInfoByName("FairBattleFindBackMoney");
         _loc8_ = String(_loc10_.Value).split(",")[1];
         _loc9_ = String(_loc10_.Value).split(",")[0];
         var _loc13_:* = ServerConfigManager.instance.findInfoByName("DailyLeagueFindBackMoney");
         _loc11_ = String(_loc13_.Value).split(",")[1];
         _loc12_ = String(_loc13_.Value).split(",")[0];
         var _loc14_:Vector.<WantStrongMenuData> = new Vector.<WantStrongMenuData>();
         var _loc16_:int = 0;
         while(_loc16_ < WantStrongManager.Instance.findBackDataExist.length)
         {
            if(WantStrongManager.Instance.findBackDataExist[_loc16_])
            {
               _loc15_ = new WantStrongMenuData();
               _loc15_.title = LanguageMgr.GetTranslation(this._findBackTitleArr[_loc16_]);
               _loc15_.starNum = this._findBackStarArr[_loc16_];
               _loc15_.description = LanguageMgr.GetTranslation(this._findBackDescriptionArr[_loc16_]);
               _loc15_.needLevel = this._findBackNeedLevelArr[_loc16_];
               _loc15_.iconUrl = this._findBackIconArr[_loc16_];
               _loc15_.id = this._idArr[_loc16_ + 18];
               _loc15_.type = 5;
               _loc15_.bossType = this._bossTypeDic[_loc15_.id];
               _loc15_.freeBackBtnEnable = WantStrongManager.Instance.findBackDic[_loc15_.bossType] == null?Boolean(true):Boolean(!WantStrongManager.Instance.findBackDic[_loc15_.bossType][0]);
               _loc15_.allBackBtnEnable = WantStrongManager.Instance.findBackDic[_loc15_.bossType] == null?Boolean(true):Boolean(!WantStrongManager.Instance.findBackDic[_loc15_.bossType][1]);
               if(_loc16_ < this._findBackTitleArr.length - 2)
               {
                  _loc15_.awardType = 1;
                  _loc15_.awardNum = _loc1_;
                  _loc15_.moneyNum = _loc5_;
                  _loc7_ = true;
               }
               else if(_loc16_ == this._findBackTitleArr.length - 2)
               {
                  _loc15_.awardType = 2;
                  _loc15_.awardNum = _loc8_;
                  _loc15_.moneyNum = _loc9_;
               }
               else if(_loc16_ == this._findBackTitleArr.length - 1)
               {
                  _loc15_.awardType = 3;
                  _loc15_.awardNum = _loc11_;
                  _loc15_.moneyNum = _loc12_;
               }
               if(PlayerManager.Instance.Self.Grade < this._worldBossStartLevel && _loc7_)
               {
                  _loc7_ = false;
               }
               else
               {
                  _loc14_.push(_loc15_);
               }
            }
            _loc16_++;
         }
         this._data[5] = _loc14_;
      }
      
      public function initData() : void
      {
         var _loc1_:Vector.<WantStrongMenuData> = null;
         var _loc4_:WantStrongMenuData = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc2_:String = "";
         var _loc3_:int = 0;
         while(_loc3_ < 4)
         {
            _loc1_ = new Vector.<WantStrongMenuData>();
            if(_loc3_ == 0)
            {
               _loc5_ = 0;
               while(_loc5_ < 12)
               {
                  if(this._needLevelArr[_loc5_] <= PlayerManager.Instance.Self.Grade)
                  {
                     _loc4_ = new WantStrongMenuData();
                     _loc4_.title = LanguageMgr.GetTranslation(this._titleArr[_loc5_]);
                     _loc4_.starNum = this._starArr[_loc5_];
                     _loc2_ = LanguageMgr.GetTranslation(this._descriptionArr[_loc5_]);
                     if(this._descriptionArr[_loc5_] == "ddt.wantStrong.view.beadDescription")
                     {
                        _loc2_ = LanguageMgr.GetTranslation(this._descriptionArr[_loc5_],PathManager.russiaLanguage);
                     }
                     else if(this._descriptionArr[_loc5_] == "ddt.wantStrong.view.goldDescription")
                     {
                        if(PathManager.russiaEdition == "mail.ru")
                        {
                           _loc2_ = LanguageMgr.GetTranslation("ddt.wantStrong.view.goldDescription");
                        }
                        else
                        {
                           _loc2_ = LanguageMgr.GetTranslation("ddt.wantStrong.view.goldDescription1");
                        }
                     }
                     _loc4_.description = _loc2_;
                     _loc4_.needLevel = this._needLevelArr[_loc5_];
                     _loc4_.iconUrl = this._iconArr[_loc5_];
                     _loc4_.id = this._idArr[_loc5_];
                     _loc4_.type = 1;
                     _loc1_.push(_loc4_);
                  }
                  _loc5_++;
               }
               if(_loc1_.length > 0)
               {
                  this._data[1] = _loc1_;
               }
            }
            else if(_loc3_ == 1)
            {
               _loc6_ = 12;
               while(_loc6_ < 15)
               {
                  if(this._needLevelArr[_loc6_] <= PlayerManager.Instance.Self.Grade)
                  {
                     _loc4_ = new WantStrongMenuData();
                     _loc4_.title = LanguageMgr.GetTranslation(this._titleArr[_loc6_]);
                     _loc4_.starNum = this._starArr[_loc6_];
                     _loc2_ = LanguageMgr.GetTranslation(this._descriptionArr[_loc6_]);
                     if(this._descriptionArr[_loc6_] == "ddt.wantStrong.view.beadDescription")
                     {
                        _loc2_ = LanguageMgr.GetTranslation(this._descriptionArr[_loc5_],PathManager.russiaLanguage);
                     }
                     else if(this._descriptionArr[_loc6_] == "ddt.wantStrong.view.goldDescription")
                     {
                        if(PathManager.russiaEdition == "mail.ru")
                        {
                           _loc2_ = LanguageMgr.GetTranslation("ddt.wantStrong.view.goldDescription");
                        }
                        else
                        {
                           _loc2_ = LanguageMgr.GetTranslation("ddt.wantStrong.view.goldDescription1");
                        }
                     }
                     _loc4_.description = _loc2_;
                     _loc4_.needLevel = this._needLevelArr[_loc6_];
                     _loc4_.iconUrl = this._iconArr[_loc6_];
                     _loc4_.id = this._idArr[_loc6_];
                     _loc4_.type = 2;
                     _loc1_.push(_loc4_);
                  }
                  _loc6_++;
               }
               if(_loc1_.length > 0)
               {
                  this._data[2] = _loc1_;
               }
            }
            else if(_loc3_ == 2)
            {
               _loc7_ = 15;
               while(_loc7_ < 17)
               {
                  if(this._needLevelArr[_loc7_] <= PlayerManager.Instance.Self.Grade)
                  {
                     _loc4_ = new WantStrongMenuData();
                     _loc4_.title = LanguageMgr.GetTranslation(this._titleArr[_loc7_]);
                     _loc4_.starNum = this._starArr[_loc7_];
                     _loc2_ = LanguageMgr.GetTranslation(this._descriptionArr[_loc7_]);
                     if(this._descriptionArr[_loc7_] == "ddt.wantStrong.view.beadDescription")
                     {
                        _loc2_ = LanguageMgr.GetTranslation(this._descriptionArr[_loc5_],PathManager.russiaLanguage);
                     }
                     else if(this._descriptionArr[_loc7_] == "ddt.wantStrong.view.goldDescription")
                     {
                        if(PathManager.russiaEdition == "mail.ru")
                        {
                           _loc2_ = LanguageMgr.GetTranslation("ddt.wantStrong.view.goldDescription");
                        }
                        else
                        {
                           _loc2_ = LanguageMgr.GetTranslation("ddt.wantStrong.view.goldDescription1");
                        }
                     }
                     _loc4_.description = _loc2_;
                     _loc4_.needLevel = this._needLevelArr[_loc7_];
                     _loc4_.iconUrl = this._iconArr[_loc7_];
                     _loc4_.id = this._idArr[_loc7_];
                     _loc4_.type = 3;
                     _loc1_.push(_loc4_);
                  }
                  _loc7_++;
               }
               if(_loc1_.length > 0)
               {
                  this._data[3] = _loc1_;
               }
            }
            else if(_loc3_ == 3)
            {
               if(this._needLevelArr[17] <= PlayerManager.Instance.Self.Grade)
               {
                  _loc4_ = new WantStrongMenuData();
                  _loc4_.title = LanguageMgr.GetTranslation(this._titleArr[17]);
                  _loc4_.starNum = this._starArr[17];
                  _loc2_ = LanguageMgr.GetTranslation(this._descriptionArr[17]);
                  if(this._descriptionArr[17] == "ddt.wantStrong.view.beadDescription")
                  {
                     _loc2_ = LanguageMgr.GetTranslation(this._descriptionArr[_loc5_],PathManager.russiaLanguage);
                  }
                  else if(this._descriptionArr[17] == "ddt.wantStrong.view.goldDescription")
                  {
                     if(PathManager.russiaEdition == "mail.ru")
                     {
                        _loc2_ = LanguageMgr.GetTranslation("ddt.wantStrong.view.goldDescription");
                     }
                     else
                     {
                        _loc2_ = LanguageMgr.GetTranslation("ddt.wantStrong.view.goldDescription1");
                     }
                  }
                  _loc4_.description = _loc2_;
                  _loc4_.needLevel = this._needLevelArr[17];
                  _loc4_.iconUrl = this._iconArr[17];
                  _loc4_.id = this._idArr[17];
                  _loc4_.type = 4;
                  _loc1_.push(_loc4_);
                  if(_loc1_.length > 0)
                  {
                     this._data[4] = _loc1_;
                  }
               }
            }
            _loc3_++;
         }
      }
      
      public function get data() : Dictionary
      {
         return this._data;
      }
      
      public function get activeId() : int
      {
         return this._activeId;
      }
      
      public function set activeId(param1:int) : void
      {
         if(param1 == 5 && PlayerManager.Instance.Self.Grade < this._worldBossStartLevel)
         {
            param1 = 1;
         }
         this._activeId = param1;
      }
      
      public function dispose() : void
      {
      }
   }
}
