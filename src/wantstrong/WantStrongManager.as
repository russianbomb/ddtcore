package wantstrong
{
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.UIModuleTypes;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.LanguageMgr;
   import ddt.manager.PathManager;
   import ddt.manager.SocketManager;
   import ddt.view.UIModuleSmallLoading;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.events.IEventDispatcher;
   import flash.utils.Dictionary;
   import wantstrong.data.WantStrongMenuData;
   import wantstrong.data.WantStrongModel;
   import wantstrong.view.WantStrongFrame;
   
   public class WantStrongManager extends EventDispatcher
   {
      
      private static var _instance:WantStrongManager;
       
      
      private var _frame:Frame;
      
      private var _model:WantStrongModel;
      
      private var _initState;
      
      private var _findBackExist:Boolean;
      
      private var _findBackDataExist:Array;
      
      public var findBackDic:Dictionary;
      
      public var isPlayMovie:Boolean;
      
      public function WantStrongManager(param1:IEventDispatcher = null)
      {
         super(param1);
      }
      
      public static function get Instance() : WantStrongManager
      {
         if(_instance == null)
         {
            _instance = new WantStrongManager();
         }
         return _instance;
      }
      
      public function get findBackDataExist() : Array
      {
         if(this._findBackDataExist == null)
         {
            this._findBackDataExist = new Array();
            this._findBackDataExist.push(false);
            this._findBackDataExist.push(false);
            this._findBackDataExist.push(false);
            this._findBackDataExist.push(false);
            this._findBackDataExist.push(false);
         }
         return this._findBackDataExist;
      }
      
      public function get findBackExist() : Boolean
      {
         return this._findBackExist;
      }
      
      public function set findBackExist(param1:Boolean) : void
      {
         this.isPlayMovie = this._findBackExist = param1;
      }
      
      public function setup() : void
      {
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.Find_Back_Income,this.findBackHandler);
         UIModuleSmallLoading.Instance.progress = 0;
         UIModuleSmallLoading.Instance.show();
         UIModuleSmallLoading.Instance.addEventListener(Event.CLOSE,this._loadingCloseHandle);
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this._loaderCompleteHandle);
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this._loaderProgressHandle);
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_ERROR,this._loaderErrorHandle);
         UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.WANT_STRONG);
      }
      
      private function findBackHandler(param1:CrazyTankSocketEvent) : void
      {
         var _loc5_:Vector.<WantStrongMenuData> = null;
         var _loc2_:int = param1.pkg.readInt();
         var _loc3_:Array = new Array();
         _loc3_[0] = param1.pkg.readBoolean();
         _loc3_[1] = param1.pkg.readBoolean();
         if(this.isPlayMovie)
         {
            if(_loc3_[0] || _loc3_[1])
            {
               this.isPlayMovie = false;
               WantStrongManager.Instance.dispatchEvent(new Event("alreadyFindBack"));
            }
         }
         this.findBackDic[_loc2_] = _loc3_;
         var _loc4_:int = 0;
         while(_loc4_ < this._model.data[5].length)
         {
            if((this._model.data[5][_loc4_] as WantStrongMenuData).bossType == _loc2_)
            {
               (this._model.data[5][_loc4_] as WantStrongMenuData).freeBackBtnEnable = !_loc3_[0];
               (this._model.data[5][_loc4_] as WantStrongMenuData).allBackBtnEnable = !_loc3_[1];
               if(_loc3_[0] && _loc3_[1])
               {
                  _loc5_ = this._model.data[5];
                  _loc5_.splice(_loc4_,1);
                  if(_loc2_ == 1)
                  {
                     this.findBackDataExist[1] = false;
                  }
                  else if(_loc2_ == 2)
                  {
                     this.findBackDataExist[0] = false;
                  }
                  else if(_loc2_ == 19)
                  {
                     this.findBackDataExist[2] = false;
                  }
                  else if(_loc2_ == 4)
                  {
                     this.findBackDataExist[4] = false;
                  }
                  else if(_loc2_ == 5)
                  {
                     this.findBackDataExist[3] = false;
                  }
               }
            }
            _loc4_++;
         }
         this.updateFindBackView();
      }
      
      private function updateFindBackView() : void
      {
         if(this._model.data[5].length > 0)
         {
            this._model.activeId = 5;
         }
         else
         {
            this._model.activeId = 1;
         }
         dispatchEvent(new Event("cellChange"));
         this.setCurrentInfo(this._model.data[this._model.activeId],true);
      }
      
      private function setData() : void
      {
         if(this.findBackExist)
         {
            this._model.initFindBackData();
            this._model.activeId = 5;
         }
         this._model.initData();
         this._initState = this._model.data[this._model.activeId];
      }
      
      public function setFindBackData(param1:int) : void
      {
         this.findBackDataExist[param1] = true;
      }
      
      protected function _loaderErrorHandle(param1:UIModuleEvent) : void
      {
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this._loaderCompleteHandle);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this._loaderProgressHandle);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_ERROR,this._loaderErrorHandle);
         UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this._loadingCloseHandle);
         UIModuleSmallLoading.Instance.hide();
         this.close();
      }
      
      public function close() : void
      {
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this._loaderCompleteHandle);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this._loaderProgressHandle);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_ERROR,this._loaderErrorHandle);
         UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this._loadingCloseHandle);
         ObjectUtils.disposeObject(this._model);
         this._model = null;
         if(this._frame)
         {
            ObjectUtils.disposeObject(this._frame);
            this._frame = null;
         }
      }
      
      protected function _loaderProgressHandle(param1:UIModuleEvent) : void
      {
         UIModuleSmallLoading.Instance.progress = param1.loader.progress * 100;
      }
      
      public function setCurrentInfo(param1:* = null, param2:Boolean = false) : void
      {
         if(this._frame)
         {
            (this._frame as WantStrongFrame).setInfo(param1,param2);
         }
      }
      
      protected function _loaderCompleteHandle(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.WANT_STRONG)
         {
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this._loaderCompleteHandle);
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this._loaderProgressHandle);
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_ERROR,this._loaderErrorHandle);
            UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this._loadingCloseHandle);
            UIModuleSmallLoading.Instance.hide();
            if(!this._model)
            {
               this._model = new WantStrongModel();
            }
            this.setData();
            this._frame = ComponentFactory.Instance.creatCustomObject("wantStrong.WantStrongFrame",[this._model]);
            this._frame.titleText = LanguageMgr.GetTranslation("ddt.wantStrong.view.titleText",PathManager.russiaLanguage);
            LayerManager.Instance.addToLayer(this._frame,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
            this.setCurrentInfo(this._initState);
         }
      }
      
      protected function _loadingCloseHandle(param1:Event) : void
      {
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this._loaderCompleteHandle);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this._loaderProgressHandle);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_ERROR,this._loaderErrorHandle);
         UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this._loadingCloseHandle);
         UIModuleSmallLoading.Instance.hide();
      }
      
      public function setinitState(param1:*) : void
      {
         this._initState = param1;
      }
   }
}
