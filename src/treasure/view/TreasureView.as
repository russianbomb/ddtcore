package treasure.view
{
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.image.ScaleUpDownImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PathManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import farm.FarmModelController;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import treasure.controller.TreasureManager;
   import treasure.events.TreasureEvents;
   import treasure.model.TreasureModel;
   
   public class TreasureView extends Sprite implements Disposeable
   {
       
      
      private var _model:TreasureModel;
      
      private var _bg:Bitmap;
      
      private var _loginDaysTitle:Bitmap;
      
      private var _numBg:Bitmap;
      
      private var _digTimesTitle:Bitmap;
      
      private var _helpTimesTitle:Bitmap;
      
      private var _line1:Bitmap;
      
      private var _line2:Bitmap;
      
      private var _loginDaysTf:FilterFrameText;
      
      private var infoFrameBg:ScaleUpDownImage;
      
      private var beginBtn:SimpleBitmapButton;
      
      private var endBtn:SimpleBitmapButton;
      
      private var box:Sprite;
      
      private var fieldView:TreasureField;
      
      private var _treasureReturnBar:TreasureReturnBar;
      
      private var _helpFrame:TreasureHelpFrame;
      
      public function TreasureView()
      {
         super();
         this._model = TreasureModel.instance;
         this.init();
         this.addListener();
      }
      
      private function init() : void
      {
         this._bg = ComponentFactory.Instance.creatBitmap("asset.treasure.bg");
         this.infoFrameBg = ComponentFactory.Instance.creatComponentByStylename("asset.treasure.infoFrame.BG");
         this._loginDaysTitle = ComponentFactory.Instance.creatBitmap("asset.treasure.loginDays");
         this._numBg = ComponentFactory.Instance.creatBitmap("asset.treasure.numBg");
         this._digTimesTitle = ComponentFactory.Instance.creatBitmap("asset.treasure.digTimes");
         this._helpTimesTitle = ComponentFactory.Instance.creatBitmap("asset.treasure.helpTimes");
         this._line1 = ComponentFactory.Instance.creatBitmap("asset.treasure.line");
         this._line2 = ComponentFactory.Instance.creatBitmap("asset.treasure.line");
         this._loginDaysTf = ComponentFactory.Instance.creatComponentByStylename("asset.treasure.loginDaysTf");
         this.beginBtn = ComponentFactory.Instance.creatComponentByStylename("treasure.beginbtn");
         this.box = new Sprite();
         this._treasureReturnBar = ComponentFactory.Instance.creat("asset.treasure.returnMenu");
         this._helpFrame = ComponentFactory.Instance.creat("asset.treasure.helpFrame");
         PositionUtils.setPos(this._line1,"treasure.pos.line1");
         PositionUtils.setPos(this._line2,"treasure.pos.line2");
         addChild(this._bg);
         addChild(this.infoFrameBg);
         addChild(this._loginDaysTitle);
         addChild(this._numBg);
         addChild(this._digTimesTitle);
         addChild(this._helpTimesTitle);
         addChild(this._line1);
         addChild(this._line2);
         addChild(this._loginDaysTf);
         addChild(this.box);
         addChild(this.beginBtn);
         addChild(this._treasureReturnBar);
         this.fieldView = new TreasureField(this);
         addChild(this._helpFrame);
         if(TreasureModel.instance.isEndTreasure)
         {
            this.fieldView.setField(true);
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.treasure.over"));
         }
         else if(TreasureModel.instance.isBeginTreasure)
         {
            this.fieldView.setField(false);
         }
         else
         {
            this.fieldView.setField(true);
         }
         this.initData();
      }
      
      private function initData() : void
      {
         var _loc3_:Array = null;
         var _loc6_:Array = null;
         var _loc10_:Bitmap = null;
         var _loc11_:Bitmap = null;
         var _loc12_:Bitmap = null;
         if(TreasureModel.instance.isEndTreasure)
         {
            this.beginBtn.visible = false;
         }
         else if(TreasureModel.instance.isBeginTreasure)
         {
            this.beginBtn.visible = false;
         }
         else
         {
            this.beginBtn.visible = true;
         }
         this._loginDaysTf.text = String(TreasureModel.instance.logoinDays);
         var _loc1_:int = TreasureModel.instance.logoinDays > 2?int(3):int(TreasureModel.instance.logoinDays);
         if(this.box)
         {
            ObjectUtils.disposeAllChildren(this.box);
         }
         var _loc2_:int = 0;
         while(_loc2_ < TreasureModel.instance.friendHelpTimes)
         {
            _loc10_ = ComponentFactory.Instance.creatBitmap("asset.treasure.forfex");
            this.box.addChild(_loc10_);
            _loc10_.x = 60 + _loc2_ * (_loc10_.width + 10);
            _loc2_++;
         }
         _loc3_ = [];
         var _loc4_:int = 0;
         while(_loc4_ < _loc1_)
         {
            _loc11_ = ComponentFactory.Instance.creatBitmap("asset.treasure.star");
            this.box.addChild(_loc11_);
            _loc11_.x = 60 + _loc4_ * (_loc11_.width + 10);
            _loc11_.filters = ComponentFactory.Instance.creatFilters("grayFilter");
            _loc3_.push(_loc11_);
            _loc4_++;
         }
         var _loc5_:int = TreasureModel.instance.friendHelpTimes >= PathManager.treasureHelpTimes?int(1):int(0);
         _loc6_ = [];
         var _loc7_:int = 0;
         while(_loc7_ < _loc5_)
         {
            _loc12_ = ComponentFactory.Instance.creatBitmap("asset.treasure.sun");
            this.box.addChild(_loc12_);
            _loc12_.x = 60 + _loc1_ * (_loc12_.width + 10);
            _loc12_.filters = ComponentFactory.Instance.creatFilters("grayFilter");
            _loc6_.push(_loc12_);
            _loc7_++;
         }
         var _loc8_:int = 0;
         while(_loc8_ < PlayerManager.Instance.Self.treasure)
         {
            if(_loc3_[_loc8_])
            {
               _loc3_[_loc8_].filters = null;
            }
            _loc8_++;
         }
         var _loc9_:int = 0;
         while(_loc9_ < PlayerManager.Instance.Self.treasureAdd)
         {
            if(_loc6_[_loc9_])
            {
               _loc6_[_loc9_].filters = null;
            }
            _loc9_++;
         }
      }
      
      private function addListener() : void
      {
         this.beginBtn.addEventListener(MouseEvent.CLICK,this.__onbeginBtnClick);
         this._treasureReturnBar.addEventListener(TreasureEvents.RETURN_TREASURE,this.__returnHandler);
         TreasureManager.instance.addEventListener(TreasureEvents.BEREPAIR_FRIEND_FARM_SEND,this.__friendHelpFarmHandler);
         TreasureManager.instance.addEventListener(TreasureEvents.BEGIN_GAME,this.__beginGameHandler);
         TreasureManager.instance.addEventListener(TreasureEvents.DIG,this.__diHandler);
         TreasureManager.instance.addEventListener(TreasureEvents.END_GAME,this.__endGameHandler);
      }
      
      private function __endGameHandler(param1:TreasureEvents) : void
      {
         this.fieldView.endGameShow();
      }
      
      private function __diHandler(param1:TreasureEvents) : void
      {
         this.initData();
         this.fieldView.digField(param1.info.pos);
      }
      
      private function __beginGameHandler(param1:TreasureEvents) : void
      {
         this.beginBtn.visible = false;
         this.fieldView.playStartCartoon();
      }
      
      private function __returnHandler(param1:TreasureEvents) : void
      {
         SoundManager.instance.play("008");
         FarmModelController.instance.goFarm(PlayerManager.Instance.Self.ID,PlayerManager.Instance.Self.NickName);
      }
      
      private function __friendHelpFarmHandler(param1:TreasureEvents) : void
      {
         this.initData();
      }
      
      private function __onEndBtnClick(param1:MouseEvent) : void
      {
         var _loc2_:BaseAlerFrame = null;
         if(TreasureModel.instance.friendHelpTimes < PathManager.treasureHelpTimes)
         {
            _loc2_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("tips"),LanguageMgr.GetTranslation("ddt.treasure.giveUp"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),false,true,false,LayerManager.ALPHA_BLOCKGOUND);
            _loc2_.addEventListener(FrameEvent.RESPONSE,this.__onFeedResponse);
         }
         else
         {
            SocketManager.Instance.out.endTreasure();
         }
      }
      
      protected function __onFeedResponse(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         switch(param1.responseCode)
         {
            case FrameEvent.SUBMIT_CLICK:
            case FrameEvent.ENTER_CLICK:
               SocketManager.Instance.out.endTreasure();
         }
         param1.currentTarget.removeEventListener(FrameEvent.RESPONSE,this.__onFeedResponse);
         ObjectUtils.disposeObject(param1.currentTarget);
      }
      
      private function __onbeginBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         this.beginBtn.enable = false;
         SocketManager.Instance.out.startTreasure();
      }
      
      private function removeEvent() : void
      {
         this.beginBtn.removeEventListener(MouseEvent.CLICK,this.__onbeginBtnClick);
         this._treasureReturnBar.removeEventListener(TreasureEvents.RETURN_TREASURE,this.__returnHandler);
         TreasureManager.instance.removeEventListener(TreasureEvents.BEREPAIR_FRIEND_FARM_SEND,this.__friendHelpFarmHandler);
         TreasureManager.instance.removeEventListener(TreasureEvents.BEGIN_GAME,this.__beginGameHandler);
         TreasureManager.instance.removeEventListener(TreasureEvents.DIG,this.__diHandler);
         TreasureManager.instance.removeEventListener(TreasureEvents.END_GAME,this.__endGameHandler);
      }
      
      public function dispose() : void
      {
         this.removeEvent();
         if(this.box)
         {
            ObjectUtils.disposeAllChildren(this.box);
         }
         if(this.box)
         {
            ObjectUtils.disposeObject(this.box);
         }
         this.box = null;
         if(this._loginDaysTf)
         {
            ObjectUtils.disposeObject(this._loginDaysTf);
         }
         this._loginDaysTf = null;
         if(this._line1)
         {
            ObjectUtils.disposeObject(this._line1);
         }
         this._line1 = null;
         if(this._line2)
         {
            ObjectUtils.disposeObject(this._line2);
         }
         this._line2 = null;
         if(this._helpTimesTitle)
         {
            ObjectUtils.disposeObject(this._helpTimesTitle);
         }
         this._helpTimesTitle = null;
         if(this._digTimesTitle)
         {
            ObjectUtils.disposeObject(this._digTimesTitle);
         }
         this._digTimesTitle = null;
         if(this._numBg)
         {
            ObjectUtils.disposeObject(this._numBg);
         }
         this._numBg = null;
         if(this._loginDaysTitle)
         {
            ObjectUtils.disposeObject(this._loginDaysTitle);
         }
         this._loginDaysTitle = null;
         if(this.infoFrameBg)
         {
            ObjectUtils.disposeObject(this.infoFrameBg);
         }
         this.infoFrameBg = null;
         if(this._bg)
         {
            ObjectUtils.disposeObject(this._bg);
         }
         this._bg = null;
         this.fieldView.dispose();
         this.fieldView = null;
         this._helpFrame.dispose();
         this._helpFrame = null;
         this._treasureReturnBar.dispose();
         this._treasureReturnBar = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
