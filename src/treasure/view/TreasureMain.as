package treasure.view
{
   import com.pickgliss.manager.CacheSysManager;
   import ddt.constants.CacheConsts;
   import ddt.manager.ChatManager;
   import ddt.states.BaseStateView;
   import ddt.states.StateType;
   import ddt.view.MainToolBar;
   
   public class TreasureMain extends BaseStateView
   {
       
      
      private var _treasure:TreasureView;
      
      public function TreasureMain()
      {
         super();
      }
      
      override public function enter(param1:BaseStateView, param2:Object = null) : void
      {
         CacheSysManager.lock(CacheConsts.TREASURE);
         MainToolBar.Instance.hide();
         this._treasure = new TreasureView();
         addChild(this._treasure);
         ChatManager.Instance.state = ChatManager.CHAT_GAME_LOADING;
         addChild(ChatManager.Instance.view);
         super.enter(param1,param2);
      }
      
      override public function leaving(param1:BaseStateView) : void
      {
         CacheSysManager.unlock(CacheConsts.TREASURE);
         CacheSysManager.getInstance().release(CacheConsts.TREASURE);
         MainToolBar.Instance.show();
         super.leaving(param1);
         this.dispose();
      }
      
      override public function getType() : String
      {
         return StateType.TREASURE;
      }
      
      override public function getBackType() : String
      {
         return StateType.FARM;
      }
      
      override public function dispose() : void
      {
         if(this._treasure)
         {
            this._treasure.dispose();
            this._treasure = null;
         }
      }
   }
}
