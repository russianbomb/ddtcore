package treasure.controller
{
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.UIModuleTypes;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.ItemManager;
   import ddt.manager.PathManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.StateManager;
   import ddt.states.StateType;
   import ddt.view.UIModuleSmallLoading;
   import farm.FarmModelController;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import treasure.data.TreasureTempInfo;
   import treasure.events.TreasureEvents;
   import treasure.model.TreasureModel;
   
   public class TreasureManager extends EventDispatcher
   {
      
      private static var _instance:TreasureManager = null;
       
      
      private var _UILoadComplete:Boolean = false;
      
      public function TreasureManager()
      {
         super();
         this.initEvent();
      }
      
      public static function get instance() : TreasureManager
      {
         if(_instance == null)
         {
            _instance = new TreasureManager();
         }
         return _instance;
      }
      
      private function initEvent() : void
      {
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.ENTER_TREASURE,this.__onEnterTreasure);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.BEREPAIR_FRIEND_FARM_SEND,this.__farmBeRepairHandler);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.START_GAME_TREASURE,this.__stratGameHandler);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.END_TREASURE,this.__endGameHandler);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.DIG,this.__digHandler);
      }
      
      private function __digHandler(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:int = param1.pkg.readInt();
         var _loc3_:int = param1.pkg.readInt();
         var _loc4_:int = param1.pkg.readInt();
         PlayerManager.Instance.Self.treasure = param1.pkg.readInt();
         var _loc5_:int = param1.pkg.readInt();
         PlayerManager.Instance.Self.treasureAdd = _loc5_ > 0?int(_loc5_):int(0);
         var _loc6_:int = 0;
         while(_loc6_ < TreasureModel.instance.itemList.length)
         {
            if(TreasureModel.instance.itemList[_loc6_].TemplateID == _loc2_ && TreasureModel.instance.itemList[_loc6_].Count == _loc4_)
            {
               TreasureModel.instance.itemList[_loc6_].pos = _loc3_;
               this.sortList();
               dispatchEvent(new TreasureEvents(TreasureEvents.DIG,{"pos":_loc3_}));
               return;
            }
            _loc6_++;
         }
      }
      
      private function __endGameHandler(param1:CrazyTankSocketEvent) : void
      {
         TreasureModel.instance.isEndTreasure = param1.pkg.readBoolean();
         dispatchEvent(new TreasureEvents(TreasureEvents.END_GAME));
      }
      
      private function __stratGameHandler(param1:CrazyTankSocketEvent) : void
      {
         TreasureModel.instance.isBeginTreasure = param1.pkg.readBoolean();
         dispatchEvent(new TreasureEvents(TreasureEvents.BEGIN_GAME));
      }
      
      private function __farmBeRepairHandler(param1:CrazyTankSocketEvent) : void
      {
         TreasureModel.instance.friendHelpTimes = param1.pkg.readInt();
         if(TreasureModel.instance.friendHelpTimes == PathManager.treasureHelpTimes)
         {
            PlayerManager.Instance.Self.treasureAdd = 1;
         }
         dispatchEvent(new TreasureEvents(TreasureEvents.BEREPAIR_FRIEND_FARM_SEND));
      }
      
      private function __onEnterTreasure(param1:CrazyTankSocketEvent) : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:ItemTemplateInfo = null;
         var _loc7_:TreasureTempInfo = null;
         var _loc8_:ItemTemplateInfo = null;
         var _loc9_:TreasureTempInfo = null;
         TreasureModel.instance.logoinDays = param1.pkg.readInt();
         PlayerManager.Instance.Self.treasure = param1.pkg.readInt();
         var _loc2_:int = param1.pkg.readInt();
         PlayerManager.Instance.Self.treasureAdd = _loc2_ > 0?int(_loc2_):int(0);
         TreasureModel.instance.friendHelpTimes = param1.pkg.readInt();
         TreasureModel.instance.isEndTreasure = param1.pkg.readBoolean();
         TreasureModel.instance.isBeginTreasure = param1.pkg.readBoolean();
         TreasureModel.instance.itemList = new Vector.<TreasureTempInfo>();
         _loc3_ = param1.pkg.readInt();
         _loc4_ = 0;
         while(_loc4_ < _loc3_)
         {
            _loc6_ = new ItemTemplateInfo();
            _loc6_.TemplateID = param1.pkg.readInt();
            _loc6_ = ItemManager.Instance.getTemplateById(_loc6_.TemplateID);
            _loc7_ = new TreasureTempInfo();
            ObjectUtils.copyProperties(_loc7_,_loc6_);
            _loc7_.ValidDate = param1.pkg.readInt();
            _loc7_.Count = param1.pkg.readInt();
            _loc7_.pos = 0;
            _loc7_.IsBinds = true;
            _loc7_.BindType = 1;
            if(TreasureModel.instance.itemList.length == 0)
            {
               _loc5_ = 0;
            }
            else
            {
               _loc5_ = Math.floor((TreasureModel.instance.itemList.length + 1) * Math.random());
            }
            TreasureModel.instance.itemList.splice(_loc5_,0,_loc7_);
            _loc4_++;
         }
         _loc3_ = param1.pkg.readInt();
         _loc4_ = 0;
         while(_loc4_ < _loc3_)
         {
            _loc8_ = new ItemTemplateInfo();
            _loc8_.TemplateID = param1.pkg.readInt();
            _loc8_ = ItemManager.Instance.getTemplateById(_loc8_.TemplateID);
            _loc9_ = new TreasureTempInfo();
            ObjectUtils.copyProperties(_loc9_,_loc8_);
            _loc9_.pos = param1.pkg.readInt();
            _loc9_.ValidDate = param1.pkg.readInt();
            _loc9_.Count = param1.pkg.readInt();
            _loc9_.IsBinds = true;
            _loc9_.BindType = 1;
            TreasureModel.instance.itemList.push(_loc9_);
            _loc4_++;
         }
         this.sortList();
         if(this._UILoadComplete)
         {
            this.creatView();
         }
         else
         {
            this.loadUIModule();
         }
      }
      
      private function sortList() : void
      {
         var _loc6_:int = 0;
         var _loc7_:TreasureTempInfo = null;
         var _loc1_:Array = [];
         var _loc2_:int = 0;
         while(_loc2_ < TreasureModel.instance.itemList.length)
         {
            if(TreasureModel.instance.itemList[_loc2_].pos != 0)
            {
               _loc1_.push({"pos":TreasureModel.instance.itemList[_loc2_].pos});
            }
            _loc2_++;
         }
         var _loc3_:int = 0;
         while(_loc3_ < TreasureModel.instance.itemList.length - 1)
         {
            _loc6_ = _loc3_ + 1;
            while(_loc6_ < TreasureModel.instance.itemList.length)
            {
               if(TreasureModel.instance.itemList[_loc3_].pos > TreasureModel.instance.itemList[_loc6_].pos)
               {
                  _loc7_ = TreasureModel.instance.itemList[_loc3_];
                  TreasureModel.instance.itemList[_loc3_] = TreasureModel.instance.itemList[_loc6_];
                  TreasureModel.instance.itemList[_loc6_] = _loc7_;
               }
               _loc6_++;
            }
            _loc3_++;
         }
         var _loc4_:Vector.<TreasureTempInfo> = TreasureModel.instance.itemList.splice(TreasureModel.instance.itemList.length - _loc1_.length,_loc1_.length);
         var _loc5_:int = 0;
         while(_loc5_ < _loc4_.length)
         {
            TreasureModel.instance.itemList.splice(_loc4_[_loc5_].pos - 1,0,_loc4_[_loc5_]);
            _loc5_++;
         }
      }
      
      private function creatView() : void
      {
         if(StateManager.currentStateType != StateType.TREASURE)
         {
            StateManager.setState(StateType.TREASURE);
         }
         else
         {
            FarmModelController.instance.goFarm(PlayerManager.Instance.Self.ID,PlayerManager.Instance.Self.NickName);
         }
      }
      
      private function loadUIModule() : void
      {
         if(!this._UILoadComplete)
         {
            UIModuleSmallLoading.Instance.progress = 0;
            UIModuleSmallLoading.Instance.show();
            UIModuleSmallLoading.Instance.addEventListener(Event.CLOSE,this.__onClose);
            UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.__onUIModuleComplete);
            UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.__onProgress);
            UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.TREASURE);
         }
      }
      
      protected function __onClose(param1:Event) : void
      {
         UIModuleSmallLoading.Instance.hide();
         UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this.__onClose);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.__onUIModuleComplete);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.__onProgress);
      }
      
      protected function __onUIModuleComplete(param1:UIModuleEvent) : void
      {
         this._UILoadComplete = true;
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.__onUIModuleComplete);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.__onProgress);
         UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this.__onClose);
         UIModuleSmallLoading.Instance.hide();
         this.creatView();
      }
      
      protected function __onProgress(param1:UIModuleEvent) : void
      {
         UIModuleSmallLoading.Instance.progress = param1.loader.progress * 100;
      }
      
      public function show() : void
      {
         SocketManager.Instance.out.enterTreasure();
      }
   }
}
