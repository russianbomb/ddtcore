package GodSyah
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.manager.LanguageMgr;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   
   public class SyahItem extends Sprite
   {
       
      
      private var _itemBg:Bitmap;
      
      private var _cell:SyahCell;
      
      private var _mode:SyahMode;
      
      private var _info:InventoryItemInfo;
      
      private var _hp:FilterFrameText;
      
      private var _hpValue:FilterFrameText;
      
      private var _armor:FilterFrameText;
      
      private var _armorValue:FilterFrameText;
      
      private var _damage:FilterFrameText;
      
      private var _damageValue:FilterFrameText;
      
      private var _attack:FilterFrameText;
      
      private var _attackValue:FilterFrameText;
      
      private var _defense:FilterFrameText;
      
      private var _defenseValue:FilterFrameText;
      
      private var _agility:FilterFrameText;
      
      private var _agilityValue:FilterFrameText;
      
      private var _lucky:FilterFrameText;
      
      private var _luckyValue:FilterFrameText;
      
      private var _vec:Vector.<FilterFrameText>;
      
      public function SyahItem()
      {
         super();
         this._buildUI();
      }
      
      public function setSyahItemInfo(param1:InventoryItemInfo) : void
      {
         this._info = param1;
         this._mode = SyahManager.Instance.getSyahModeByInfo(param1);
         this._createInfo();
      }
      
      private function _buildUI() : void
      {
         this._itemBg = ComponentFactory.Instance.creatBitmap("wonderfulactivity.GodSyah.syahView.item.bg");
         this._hp = ComponentFactory.Instance.creatComponentByStylename("GodSyah.syahView.ability");
         this._armor = ComponentFactory.Instance.creatComponentByStylename("GodSyah.syahView.ability");
         this._damage = ComponentFactory.Instance.creatComponentByStylename("GodSyah.syahView.ability");
         this._attack = ComponentFactory.Instance.creatComponentByStylename("GodSyah.syahView.ability");
         this._defense = ComponentFactory.Instance.creatComponentByStylename("GodSyah.syahView.ability");
         this._agility = ComponentFactory.Instance.creatComponentByStylename("GodSyah.syahView.ability");
         this._lucky = ComponentFactory.Instance.creatComponentByStylename("GodSyah.syahView.ability");
         this._hpValue = ComponentFactory.Instance.creatComponentByStylename("GodSyah.syahView.abilityValue");
         this._armorValue = ComponentFactory.Instance.creatComponentByStylename("GodSyah.syahView.abilityValue");
         this._damageValue = ComponentFactory.Instance.creatComponentByStylename("GodSyah.syahView.abilityValue");
         this._attackValue = ComponentFactory.Instance.creatComponentByStylename("GodSyah.syahView.abilityValue");
         this._defenseValue = ComponentFactory.Instance.creatComponentByStylename("GodSyah.syahView.abilityValue");
         this._agilityValue = ComponentFactory.Instance.creatComponentByStylename("GodSyah.syahView.abilityValue");
         this._luckyValue = ComponentFactory.Instance.creatComponentByStylename("GodSyah.syahView.abilityValue");
         this._hp.text = LanguageMgr.GetTranslation("ddt.GodSyah.tip.hp");
         this._armor.text = LanguageMgr.GetTranslation("ddt.GodSyah.tip.armor");
         this._damage.text = LanguageMgr.GetTranslation("ddt.GodSyah.tip.damage");
         this._attack.text = LanguageMgr.GetTranslation("ddt.GodSyah.tip.attack");
         this._defense.text = LanguageMgr.GetTranslation("ddt.GodSyah.tip.defense");
         this._agility.text = LanguageMgr.GetTranslation("ddt.GodSyah.tip.agility");
         this._lucky.text = LanguageMgr.GetTranslation("ddt.GodSyah.tip.lucky");
         addChild(this._itemBg);
      }
      
      private function _createInfo() : void
      {
         this._hpValue.text = "+" + this._mode.hp;
         this._armorValue.text = "+" + this._mode.armor;
         this._damageValue.text = "+" + this._mode.damage;
         this._attackValue.text = "+" + this._mode.attack;
         this._defenseValue.text = "+" + this._mode.defense;
         this._agilityValue.text = "+" + this._mode.agility;
         this._luckyValue.text = "+" + this._mode.lucky;
         this._cell = ComponentFactory.Instance.creatCustomObject("godSyah.syahview.syahcell");
         this._cell.info = this._info;
         this._vec = new Vector.<FilterFrameText>();
         if(parseInt(this._attackValue.text) > 0)
         {
            this._vec.push(this._attack);
            this._vec.push(this._attackValue);
         }
         if(parseInt(this._defenseValue.text) > 0)
         {
            this._vec.push(this._defense);
            this._vec.push(this._defenseValue);
         }
         if(parseInt(this._agilityValue.text) > 0)
         {
            this._vec.push(this._agility);
            this._vec.push(this._agilityValue);
         }
         if(parseInt(this._luckyValue.text) > 0)
         {
            this._vec.push(this._lucky);
            this._vec.push(this._luckyValue);
         }
         if(parseInt(this._hpValue.text) > 0)
         {
            this._vec.push(this._hp);
            this._vec.push(this._hpValue);
         }
         if(parseInt(this._armorValue.text) > 0)
         {
            this._vec.push(this._armor);
            this._vec.push(this._armorValue);
         }
         if(parseInt(this._damageValue.text) > 0)
         {
            this._vec.push(this._damage);
            this._vec.push(this._damageValue);
         }
         addChild(this._cell);
         this._arrangeText();
      }
      
      private function _arrangeText() : void
      {
         var _loc4_:int = 0;
         var _loc1_:int = 155;
         var _loc2_:int = 273;
         var _loc3_:int = 230;
         _loc4_ = 343;
         switch(this._vec.length)
         {
            case 2:
               this._vec[0].x = _loc1_;
               this._vec[1].x = _loc3_;
               this._vec[0].y = this._vec[1].y = 43;
               break;
            case 4:
               this._vec[0].x = _loc1_;
               this._vec[1].x = _loc3_;
               this._vec[2].x = _loc2_;
               this._vec[3].x = _loc4_;
               this._vec[0].y = this._vec[1].y = this._vec[2].y = this._vec[3].y = 43;
               break;
            case 6:
               this._vec[0].x = this._vec[4].x = _loc1_;
               this._vec[1].x = this._vec[5].x = _loc3_;
               this._vec[2].x = _loc2_;
               this._vec[3].x = _loc4_;
               this._vec[0].y = this._vec[1].y = this._vec[2].y = this._vec[3].y = 21;
               this._vec[4].y = this._vec[5].y = 63;
               break;
            case 8:
               this._vec[0].x = this._vec[4].x = _loc1_;
               this._vec[1].x = this._vec[5].x = _loc3_;
               this._vec[2].x = this._vec[6].x = _loc2_;
               this._vec[3].x = this._vec[7].x = _loc4_;
               this._vec[0].y = this._vec[1].y = this._vec[2].y = this._vec[3].y = 21;
               this._vec[4].y = this._vec[5].y = this._vec[6].y = this._vec[7].y = 63;
               break;
            case 10:
               this._vec[0].x = this._vec[4].x = this._vec[8].x = _loc1_;
               this._vec[1].x = this._vec[5].x = this._vec[9].x = _loc3_;
               this._vec[2].x = this._vec[6].x = _loc2_;
               this._vec[3].x = this._vec[7].x = _loc4_;
               this._vec[0].y = this._vec[1].y = this._vec[2].y = this._vec[3].y = 21;
               this._vec[4].y = this._vec[5].y = this._vec[6].y = this._vec[7].y = 42;
               this._vec[8].y = this._vec[9].y = 63;
               break;
            case 12:
               this._vec[0].x = this._vec[4].x = this._vec[8].x = _loc1_;
               this._vec[1].x = this._vec[5].x = this._vec[9].x = _loc3_;
               this._vec[2].x = this._vec[6].x = this._vec[10].x = _loc2_;
               this._vec[3].x = this._vec[7].x = this._vec[11].x = _loc4_;
               this._vec[0].y = this._vec[1].y = this._vec[2].y = this._vec[3].y = 21;
               this._vec[4].y = this._vec[5].y = this._vec[6].y = this._vec[7].y = 42;
               this._vec[8].y = this._vec[9].y = this._vec[10].y = this._vec[11].y = 63;
               break;
            case 14:
               this._vec[0].x = this._vec[4].x = this._vec[8].x = this._vec[12].x = _loc1_;
               this._vec[1].x = this._vec[5].x = this._vec[9].x = this._vec[13].x = _loc3_;
               this._vec[2].x = this._vec[6].x = this._vec[10].x = _loc2_;
               this._vec[3].x = this._vec[7].x = this._vec[11].x = _loc4_;
               this._vec[0].y = this._vec[1].y = this._vec[2].y = this._vec[3].y = 12;
               this._vec[4].y = this._vec[5].y = this._vec[6].y = this._vec[7].y = 32;
               this._vec[8].y = this._vec[9].y = this._vec[10].y = this._vec[11].y = 52;
               this._vec[12].y = this._vec[13].y = 72;
         }
         var _loc5_:int = 0;
         while(_loc5_ < this._vec.length)
         {
            addChild(this._vec[_loc5_]);
            _loc5_++;
         }
      }
      
      public function dispose() : void
      {
         if(this._itemBg)
         {
            ObjectUtils.disposeObject(this._itemBg);
            this._itemBg = null;
         }
         if(this._hp)
         {
            ObjectUtils.disposeObject(this._hp);
            this._hp = null;
         }
         if(this._armor)
         {
            ObjectUtils.disposeObject(this._armor);
            this._armor = null;
         }
         if(this._damage)
         {
            ObjectUtils.disposeObject(this._damage);
            this._damage = null;
         }
         if(this._attack)
         {
            ObjectUtils.disposeObject(this._attack);
            this._attack = null;
         }
         if(this._defense)
         {
            ObjectUtils.disposeObject(this._defense);
            this._defense = null;
         }
         if(this._agility)
         {
            ObjectUtils.disposeObject(this._agility);
            this._agility = null;
         }
         if(this._lucky)
         {
            ObjectUtils.disposeObject(this._lucky);
            this._lucky = null;
         }
         if(this._hpValue)
         {
            ObjectUtils.disposeObject(this._hpValue);
            this._hpValue = null;
         }
         if(this._armorValue)
         {
            ObjectUtils.disposeObject(this._armorValue);
            this._armorValue = null;
         }
         if(this._damageValue)
         {
            ObjectUtils.disposeObject(this._damageValue);
            this._damageValue = null;
         }
         if(this._attackValue)
         {
            ObjectUtils.disposeObject(this._attackValue);
            this._attackValue = null;
         }
         if(this._defenseValue)
         {
            ObjectUtils.disposeObject(this._defenseValue);
            this._defenseValue = null;
         }
         if(this._agilityValue)
         {
            ObjectUtils.disposeObject(this._agilityValue);
            this._agilityValue = null;
         }
         if(this._luckyValue)
         {
            ObjectUtils.disposeObject(this._luckyValue);
            this._luckyValue = null;
         }
      }
   }
}
