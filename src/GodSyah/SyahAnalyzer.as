package GodSyah
{
   import com.pickgliss.loader.DataAnalyzer;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.manager.ItemManager;
   
   public class SyahAnalyzer extends DataAnalyzer
   {
       
      
      private var _details:Array;
      
      private var _modes:Vector.<SyahMode>;
      
      private var _infos:Vector.<InventoryItemInfo>;
      
      private var _nowTime:Date;
      
      private var _syahArr:Array;
      
      private var _detailsArr:Array;
      
      private var _modeArr:Array;
      
      public function SyahAnalyzer(param1:Function)
      {
         super(param1);
      }
      
      override public function analyze(param1:*) : void
      {
         var _loc3_:XMLList = null;
         var _loc4_:int = 0;
         var _loc5_:Array = null;
         var _loc6_:int = 0;
         var _loc7_:SyahMode = null;
         var _loc8_:InventoryItemInfo = null;
         var _loc2_:XML = new XML(param1);
         this._nowTime = this._getEndTime(_loc2_.@nowTime,_loc2_.@nowTime);
         if(_loc2_.@value == "true")
         {
            this._details = new Array();
            this._modes = new Vector.<SyahMode>();
            this._infos = new Vector.<InventoryItemInfo>();
            this._syahArr = new Array();
            this._detailsArr = new Array();
            this._modeArr = new Array();
            _loc3_ = _loc2_..Condition;
            _loc4_ = 0;
            while(_loc4_ < _loc2_.child("Active").length())
            {
               this._detailsArr[_loc4_] = new Vector.<InventoryItemInfo>();
               this._modeArr[_loc4_] = new Vector.<SyahMode>();
               _loc5_ = new Array();
               _loc5_[0] = _loc2_.child("Active")[_loc4_].@IsOpen;
               _loc5_[1] = this._createValid(_loc2_.child("Active")[_loc4_].@StartDate,_loc2_.child("Active")[_loc4_].@EndDate);
               _loc5_[2] = _loc2_.child("Active")[_loc4_].@ActiveInfo;
               _loc5_[3] = this._getEndTime(_loc2_.child("Active")[_loc4_].@StartDate,_loc2_.child("Active")[_loc4_].@StartTime);
               _loc5_[4] = this._getEndTime(_loc2_.child("Active")[_loc4_].@EndDate,_loc2_.child("Active")[_loc4_].@EndTime);
               _loc5_[5] = _loc2_.child("Active")[_loc4_].@SubID;
               this._syahArr[_loc4_] = _loc5_;
               _loc6_ = 0;
               while(_loc6_ < _loc3_.length())
               {
                  if(_loc2_.child("Active")[_loc4_].@SubID == _loc3_[_loc6_].@SubID)
                  {
                     _loc7_ = this._createModeValue(_loc3_[_loc6_].@Value);
                     _loc7_.syahID = _loc3_[_loc6_].@ConditionID;
                     _loc7_.level = !!_loc7_.isGold?int(_loc3_[_loc6_].@Type - 100):int(_loc3_[_loc6_].@Type);
                     _loc7_.valid = this._createValid(_loc2_.child("Active")[_loc4_].@StartDate,_loc2_.child("Active")[_loc4_].@EndDate);
                     this._modeArr[_loc4_].push(_loc7_);
                     _loc8_ = new InventoryItemInfo();
                     _loc8_.TemplateID = _loc7_.syahID;
                     _loc8_ = ItemManager.fill(_loc8_);
                     _loc8_.StrengthenLevel = _loc7_.level;
                     _loc8_.isGold = _loc7_.isGold;
                     this._detailsArr[_loc4_].push(_loc8_);
                  }
                  _loc6_++;
               }
               _loc4_++;
            }
            onAnalyzeComplete();
         }
         else
         {
            message = _loc2_.@message;
            onAnalyzeComplete();
         }
      }
      
      private function _createModeValue(param1:String) : SyahMode
      {
         var _loc2_:Array = param1.split("-");
         var _loc3_:SyahMode = new SyahMode();
         var _loc4_:int = 0;
         while(_loc4_ < _loc2_.length)
         {
            switch(_loc2_[_loc4_])
            {
               case "1":
                  _loc3_.attack = _loc2_[_loc4_ + 1];
                  break;
               case "2":
                  _loc3_.defense = _loc2_[_loc4_ + 1];
                  break;
               case "3":
                  _loc3_.agility = _loc2_[_loc4_ + 1];
                  break;
               case "4":
                  _loc3_.lucky = _loc2_[_loc4_ + 1];
                  break;
               case "5":
                  _loc3_.hp = _loc2_[_loc4_ + 1];
                  break;
               case "6":
                  _loc3_.damage = _loc2_[_loc4_ + 1];
                  break;
               case "7":
                  _loc3_.armor = _loc2_[_loc4_ + 1];
                  break;
               case "11":
                  _loc3_.isGold = _loc2_[_loc4_ + 1] == 1?Boolean(true):Boolean(false);
            }
            _loc4_ = _loc4_ + 2;
         }
         return _loc3_;
      }
      
      private function _createValid(param1:String, param2:String) : String
      {
         return param1.split(" ")[0].replace("-",".").replace("-",".") + "-" + param2.split(" ")[0].replace("-",".").replace("-",".");
      }
      
      private function _getEndTime(param1:String, param2:String) : Date
      {
         var _loc3_:Array = param1.split(" ")[0].split("-");
         var _loc4_:String = _loc3_[1] + "/" + _loc3_[2] + "/" + _loc3_[0] + " " + param2.split(" ")[1];
         var _loc5_:Date = new Date(_loc4_);
         return _loc5_;
      }
      
      public function get modes() : Array
      {
         return this._modeArr;
      }
      
      public function get details() : Array
      {
         return this._syahArr;
      }
      
      public function get infos() : Array
      {
         return this._detailsArr;
      }
      
      public function get nowTime() : Date
      {
         return this._nowTime;
      }
   }
}
