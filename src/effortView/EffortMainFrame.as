package effortView
{
   import bagAndInfo.info.PlayerInfoEffortHonorView;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.EffortManager;
   import effortView.leftView.EffortTaskView;
   import effortView.rightView.EffortPullComboBox;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.geom.Point;
   
   public class EffortMainFrame extends Sprite implements Disposeable
   {
       
      
      private var _effortPannelView:EffortPannelView;
      
      private var _controller:EffortController;
      
      private var _effortTaskView:EffortTaskView;
      
      private var _effortPullComboBox:EffortPullComboBox;
      
      private var _playerInfoEffortHonorView:PlayerInfoEffortHonorView;
      
      private var _comboBoxBg:Bitmap;
      
      public function EffortMainFrame()
      {
         super();
         this.initContent();
         this.initEvent();
      }
      
      private function initContent() : void
      {
         var _loc1_:Point = null;
         var _loc2_:Point = null;
         var _loc3_:Point = null;
         this._controller = new EffortController();
         this._controller.isSelf = EffortManager.Instance.isSelf;
         this._controller.currentViewType = 0;
         this._effortPannelView = new EffortPannelView(this._controller);
         addChild(this._effortPannelView);
         this._effortPullComboBox = new EffortPullComboBox(this._controller);
         addChild(this._effortPullComboBox);
         this._comboBoxBg = ComponentFactory.Instance.creatBitmap("asset.Effort.comboBoxBg");
         if(EffortManager.Instance.isSelf)
         {
            this._effortPullComboBox.visible = false;
            this._playerInfoEffortHonorView = new PlayerInfoEffortHonorView();
            _loc2_ = ComponentFactory.Instance.creatCustomObject("effortView.EffortMainFrame.EffortHonorViewPos");
            this._playerInfoEffortHonorView.x = _loc2_.x;
            this._playerInfoEffortHonorView.y = _loc2_.y;
            addChild(this._playerInfoEffortHonorView);
         }
         else
         {
            this._effortPullComboBox.visible = false;
            this._comboBoxBg.visible = false;
         }
         if(EffortManager.Instance.isSelf)
         {
            this._effortTaskView = new EffortTaskView();
            _loc3_ = ComponentFactory.Instance.creatCustomObject("effortView.EffortTaskView.EffortTaskViewPos");
            this._effortTaskView.x = _loc3_.x;
            this._effortTaskView.y = _loc3_.y;
            addChild(this._effortTaskView);
         }
         _loc1_ = ComponentFactory.Instance.creatCustomObject("effortView.EffortView.EffortViewPos");
         this.x = _loc1_.x;
         this.y = _loc1_.y;
      }
      
      private function initEvent() : void
      {
         this._controller.addEventListener(Event.CHANGE,this.__rightChange);
      }
      
      private function __rightChange(param1:Event) : void
      {
         if(this._controller.currentRightViewType == 0)
         {
            if(!EffortManager.Instance.isSelf)
            {
               this._comboBoxBg.visible = false;
            }
            if(this._effortPullComboBox)
            {
               this._effortPullComboBox.visible = false;
            }
            if(this._playerInfoEffortHonorView)
            {
               this._playerInfoEffortHonorView.visible = true;
            }
         }
         else
         {
            if(this._comboBoxBg)
            {
               this._comboBoxBg.visible = true;
            }
            if(this._effortPullComboBox)
            {
               this._effortPullComboBox.visible = true;
            }
            if(this._playerInfoEffortHonorView)
            {
               this._playerInfoEffortHonorView.visible = false;
            }
         }
      }
      
      public function dispose() : void
      {
         this._controller.removeEventListener(Event.CHANGE,this.__rightChange);
         if(this._comboBoxBg)
         {
            ObjectUtils.disposeObject(this._comboBoxBg);
            this._comboBoxBg = null;
         }
         if(this._effortPannelView && this._effortPannelView.parent)
         {
            this._effortPannelView.parent.removeChild(this._effortPannelView);
            this._effortPannelView.dispose();
            this._effortPannelView = null;
         }
         if(this._effortTaskView && this._effortTaskView.parent)
         {
            this._effortTaskView.parent.removeChild(this._effortTaskView);
            this._effortTaskView.dispose();
            this._effortTaskView = null;
         }
         if(this._effortPullComboBox && this._effortPullComboBox.parent)
         {
            this._effortPullComboBox.parent.removeChild(this._effortPullComboBox);
            this._effortPullComboBox.dispose();
            this._effortPullComboBox = null;
         }
         if(this._playerInfoEffortHonorView)
         {
            this._playerInfoEffortHonorView.dispose();
            this._playerInfoEffortHonorView = null;
         }
         if(this.parent)
         {
            this.parent.removeChild(this);
         }
      }
   }
}
