package gemstone.items
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.ui.tip.BaseTip;
   import ddt.manager.LanguageMgr;
   import ddt.view.SimpleItem;
   import flash.display.DisplayObject;
   import gemstone.info.GemstoneStaticInfo;
   
   public class GemstoneTip extends BaseTip
   {
       
      
      private var _bg:ScaleBitmapImage;
      
      private var _tempData:Object;
      
      private var _fiSoulName:FilterFrameText;
      
      private var _quality:SimpleItem;
      
      private var _type:SimpleItem;
      
      private var _attack:FilterFrameText;
      
      private var _defense:FilterFrameText;
      
      private var _agility:FilterFrameText;
      
      private var _luck:FilterFrameText;
      
      private var _grade1:FilterFrameText;
      
      private var _grade2:FilterFrameText;
      
      private var _grade3:FilterFrameText;
      
      private var _forever:FilterFrameText;
      
      private var _displayList:Vector.<DisplayObject>;
      
      public function GemstoneTip()
      {
         super();
      }
      
      override protected function addChildren() : void
      {
         super.addChildren();
      }
      
      override public function set tipData(param1:Object) : void
      {
         this._tempData = param1;
         if(!this._tempData)
         {
            return;
         }
         this._displayList = new Vector.<DisplayObject>();
         this.updateView();
      }
      
      private function clear() : void
      {
         var _loc1_:DisplayObject = null;
         while(numChildren > 0)
         {
            _loc1_ = getChildAt(0) as DisplayObject;
            if(_loc1_.parent)
            {
               _loc1_.parent.removeChild(_loc1_);
            }
         }
      }
      
      private function updateView() : void
      {
         var _loc3_:FilterFrameText = null;
         var _loc4_:GemstonTipItem = null;
         var _loc5_:Object = null;
         this.clear();
         this._bg = ComponentFactory.Instance.creat("core.GoodsTipBg");
         this._bg.width = 285;
         this._bg.height = 335;
         this.tipbackgound = this._bg;
         this._fiSoulName = ComponentFactory.Instance.creatComponentByStylename("core.GoodsTipItemNameTxt");
         this._displayList.push(this._fiSoulName);
         var _loc1_:Vector.<GemstoneStaticInfo> = this._tempData as Vector.<GemstoneStaticInfo>;
         var _loc2_:int = _loc1_.length;
         var _loc6_:int = 0;
         while(_loc6_ < _loc2_)
         {
            if(_loc1_[_loc6_].attack != 0)
            {
               _loc5_ = new Object();
               _loc5_.id = _loc1_[_loc6_].id;
               _loc5_.str = LanguageMgr.GetTranslation("ddt.gemstone.curInfo.redGemstoneAtc",_loc1_[_loc6_].level,String(_loc1_[_loc6_].attack));
               this._fiSoulName.text = LanguageMgr.GetTranslation("ddt.gemstone.curInfo.redGemstone");
               _loc4_ = new GemstonTipItem();
               _loc4_.setInfo(_loc5_);
               this._displayList.push(_loc4_);
            }
            else if(_loc1_[_loc6_].defence != 0)
            {
               this._fiSoulName.text = LanguageMgr.GetTranslation("ddt.gemstone.curInfo.bluGemstone");
               _loc5_ = new Object();
               _loc5_.id = _loc1_[_loc6_].id;
               _loc5_.str = LanguageMgr.GetTranslation("ddt.gemstone.curInfo.bluGemstoneDef",_loc1_[_loc6_].level,String(_loc1_[_loc6_].defence));
               _loc4_ = new GemstonTipItem();
               _loc4_.setInfo(_loc5_);
               this._displayList.push(_loc4_);
            }
            else if(_loc1_[_loc6_].agility != 0)
            {
               this._fiSoulName.text = LanguageMgr.GetTranslation("ddt.gemstone.curInfo.gesGemstone");
               _loc5_ = new Object();
               _loc5_.id = _loc1_[_loc6_].id;
               _loc5_.str = LanguageMgr.GetTranslation("ddt.gemstone.curInfo.gesGemstoneAgi",_loc1_[_loc6_].level,String(_loc1_[_loc6_].agility));
               _loc4_ = new GemstonTipItem();
               _loc4_.setInfo(_loc5_);
               this._displayList.push(_loc4_);
            }
            else if(_loc1_[_loc6_].luck != 0)
            {
               this._fiSoulName.text = LanguageMgr.GetTranslation("ddt.gemstone.curInfo.yelGemstone");
               _loc5_ = new Object();
               _loc5_.id = _loc1_[_loc6_].id;
               _loc5_.str = LanguageMgr.GetTranslation("ddt.gemstone.curInfo.yelGemstoneLuk",_loc1_[_loc6_].level,String(_loc1_[_loc6_].luck));
               _loc4_ = new GemstonTipItem();
               _loc4_.setInfo(_loc5_);
               this._displayList.push(_loc4_);
            }
            else if(_loc1_[_loc6_].blood != 0)
            {
               this._fiSoulName.text = LanguageMgr.GetTranslation("ddt.gemstone.curInfo.purpleGemstone");
               _loc5_ = new Object();
               _loc5_.id = _loc1_[_loc6_].id;
               _loc5_.str = LanguageMgr.GetTranslation("ddt.gemstone.curInfo.purpleGemstoneLuk",_loc1_[_loc6_].level,String(_loc1_[_loc6_].blood));
               _loc4_ = new GemstonTipItem();
               _loc4_.setInfo(_loc5_);
               this._displayList.push(_loc4_);
            }
            _loc6_++;
         }
         this.initPos();
      }
      
      override public function get tipData() : Object
      {
         return this._tempData;
      }
      
      override protected function init() : void
      {
      }
      
      private function initPos() : void
      {
         var _loc2_:int = 0;
         var _loc1_:int = this._displayList.length;
         _loc2_ = 0;
         while(_loc2_ < _loc1_)
         {
            this._displayList[_loc2_].y = _loc2_ * 30 + 5;
            this._displayList[_loc2_].x = 5;
            addChild(this._displayList[_loc2_] as DisplayObject);
            _loc2_++;
         }
      }
   }
}
