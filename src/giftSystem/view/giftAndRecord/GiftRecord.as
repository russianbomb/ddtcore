package giftSystem.view.giftAndRecord
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.ScrollPanel;
   import com.pickgliss.ui.controls.container.VBox;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.player.PlayerInfo;
   import ddt.manager.PlayerManager;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import giftSystem.GiftController;
   import giftSystem.GiftEvent;
   import giftSystem.data.RecordInfo;
   import giftSystem.element.RecordItem;
   
   public class GiftRecord extends Sprite implements Disposeable
   {
       
      
      private var _playerInfo:PlayerInfo;
      
      private var _canClick:Boolean = false;
      
      private var _noGift:Bitmap;
      
      private var _container:VBox;
      
      private var _panel:ScrollPanel;
      
      private var _itemArr:Vector.<RecordItem>;
      
      public function GiftRecord()
      {
         super();
         this.initView();
         this.initEvent();
      }
      
      private function initView() : void
      {
         this._itemArr = new Vector.<RecordItem>();
         this._container = ComponentFactory.Instance.creatComponentByStylename("RecordParent.container");
         this._panel = ComponentFactory.Instance.creatComponentByStylename("RecordParent.Panel");
         this._panel.setView(this._container);
         addChild(this._panel);
      }
      
      private function initEvent() : void
      {
         GiftController.Instance.addEventListener(GiftEvent.LOAD_RECORD_COMPLETE,this.__setRecordList);
      }
      
      private function removeEvent() : void
      {
         GiftController.Instance.removeEventListener(GiftEvent.LOAD_RECORD_COMPLETE,this.__setRecordList);
      }
      
      public function set playerInfo(param1:PlayerInfo) : void
      {
         if(this._playerInfo == param1)
         {
            return;
         }
         this._playerInfo = param1;
         if(this._playerInfo.ID == PlayerManager.Instance.Self.ID)
         {
            this._canClick = true;
         }
      }
      
      private function __setRecordList(param1:GiftEvent) : void
      {
         if(param1.str == GiftController.RECEIVEDPATH)
         {
            this.setList(GiftController.Instance.recordInfo);
         }
      }
      
      private function setList(param1:RecordInfo) : void
      {
         this.clear();
         this._itemArr = new Vector.<RecordItem>();
         this.setReceived(param1);
      }
      
      private function setReceived(param1:RecordInfo) : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:RecordItem = null;
         if(param1)
         {
            _loc2_ = param1.recordList.length;
            if(_loc2_ != 0)
            {
               _loc3_ = 0;
               while(_loc3_ < _loc2_)
               {
                  _loc4_ = new RecordItem();
                  _loc4_.setup(this._playerInfo);
                  if(param1.recordList[_loc3_].Receive == 1)
                  {
                     _loc4_.setItemInfoType(param1.recordList[_loc3_],RecordItem.RECEIVED);
                  }
                  if(param1.recordList[_loc3_].Receive == 0)
                  {
                     _loc4_.setItemInfoType(param1.recordList[_loc3_],RecordItem.SENDED);
                  }
                  this._container.addChild(_loc4_);
                  this._itemArr.push(_loc4_);
                  _loc3_++;
               }
            }
         }
         this._panel.invalidateViewport();
      }
      
      private function clear() : void
      {
         if(this._noGift)
         {
            ObjectUtils.disposeObject(this._noGift);
         }
         this._noGift = null;
         var _loc1_:int = 0;
         while(_loc1_ < this._itemArr.length)
         {
            if(this._itemArr[_loc1_])
            {
               this._itemArr[_loc1_].dispose();
            }
            this._itemArr[_loc1_] = null;
            _loc1_++;
         }
         this._itemArr = null;
      }
      
      public function dispose() : void
      {
         this.removeEvent();
         this.clear();
         if(this._container)
         {
            ObjectUtils.disposeObject(this._container);
         }
         this._container = null;
         if(this._panel)
         {
            ObjectUtils.disposeObject(this._panel);
         }
         this._panel = null;
         if(this.parent)
         {
            this.parent.removeChild(this);
         }
      }
   }
}
