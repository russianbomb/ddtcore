package sevenDouble
{
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.BaseLoader;
   import com.pickgliss.loader.LoadResourceManager;
   import com.pickgliss.loader.LoaderEvent;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import ddt.data.ServerConfigInfo;
   import ddt.data.UIModuleTypes;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.ChatManager;
   import ddt.manager.LanguageMgr;
   import ddt.manager.PathManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerConfigManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.manager.TimeManager;
   import ddt.view.UIModuleSmallLoading;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.events.IEventDispatcher;
   import flash.events.TimerEvent;
   import flash.utils.Timer;
   import road7th.comm.PackageIn;
   import road7th.data.DictionaryData;
   import sevenDouble.data.SevenDoubleCarInfo;
   import sevenDouble.data.SevenDoubleInfoVo;
   import sevenDouble.data.SevenDoublePlayerInfo;
   import sevenDouble.event.SevenDoubleEvent;
   import sevenDouble.view.SevenDoubleFrame;
   
   public class SevenDoubleManager extends EventDispatcher
   {
      
      public static const ICON_RES_LOAD_COMPLETE:String = "sevenDoubleIconResLoadComplete";
      
      public static const CAR_STATUS_CHANGE:String = "sevenDoubleCarStatusChange";
      
      public static const START_GAME:String = "sevenDoubleStartGame";
      
      public static const ENTER_GAME:String = "sevenDoubleEnterGame";
      
      public static const ALL_READY:String = "sevenDoubleAllReady";
      
      public static const MOVE:String = "sevenDoubleMove";
      
      public static const REFRESH_ITEM:String = "sevenDoubleAppearItem";
      
      public static const REFRESH_BUFF:String = "sevenDoubleRefreshBuff";
      
      public static const USE_SKILL:String = "sevenDoubleUseSkill";
      
      public static const RANK_LIST:String = "sevenDoubleRankList";
      
      public static const RANK_ARRIVE_LIST:String = "";
      
      public static const ARRIVE:String = "sevenDoubleArrive";
      
      public static const DESTROY:String = "sevenDoubleDestroy";
      
      public static const RE_ENTER_ALL_INFO:String = "sevenDoubleReEnterAllInfo";
      
      public static const CAN_ENTER:String = "sevenDoubleCanEnter";
      
      public static const FIGHT_STATE_CHANGE:String = "sevenDoubleFightStateChange";
      
      public static const LEAP_PROMPT_SHOW_HIDE:String = "sevenDoubleLeapPromptShowHide";
      
      public static const END:String = "sevenDoubleEnd";
      
      public static const REFRESH_ENTER_COUNT:String = "sevenDoubleRefreshEnterCount";
      
      public static const REFRESH_ITEM_FREE_COUNT:String = "sevenDoubleRefreshItemCount";
      
      public static const CANCEL_GAME:String = "sevenDoubleCancelGame";
      
      private static var _instance:SevenDoubleManager;
       
      
      public var dataInfo:SevenDoubleInfoVo;
      
      public var isShowDungeonTip:Boolean = true;
      
      private var _isStart:Boolean;
      
      private var _isLoadIconComplete:Boolean;
      
      private var _isInGame:Boolean;
      
      private var _carStatus:int;
      
      private var _freeCount:int;
      
      private var _usableCount:int;
      
      private var _rankAddInfo:Array;
      
      private var _playerList:Vector.<SevenDoublePlayerInfo>;
      
      private var _accelerateRate:int;
      
      private var _decelerateRate:int;
      
      private var _buyRecordStatus:Array;
      
      private var _startGameNeedMoney:int;
      
      private var _doubleTimeArray:Array;
      
      private var _timer:Timer;
      
      private var _itemFreeCountList:Array;
      
      private var _sprintAwardInfo:Array;
      
      private var _endTime:int = -1;
      
      private var _hasPrompted:DictionaryData;
      
      private var _isPromptDoubleTime:Boolean = false;
      
      private var _loadResCount:int;
      
      public function SevenDoubleManager(param1:IEventDispatcher = null)
      {
         this._doubleTimeArray = [];
         this._itemFreeCountList = [0,0,0];
         super(param1);
      }
      
      public static function get instance() : SevenDoubleManager
      {
         if(_instance == null)
         {
            _instance = new SevenDoubleManager();
         }
         return _instance;
      }
      
      public function get sprintAwardInfo() : Array
      {
         return this._sprintAwardInfo;
      }
      
      public function get itemFreeCountList() : Array
      {
         return this._itemFreeCountList;
      }
      
      public function get isInDoubleTime() : Boolean
      {
         var _loc1_:Date = TimeManager.Instance.Now();
         var _loc2_:Number = _loc1_.hours;
         var _loc3_:Number = _loc1_.minutes;
         var _loc4_:int = int(this._doubleTimeArray[0]);
         var _loc5_:int = int(this._doubleTimeArray[1]);
         var _loc6_:int = int(this._doubleTimeArray[2]);
         var _loc7_:int = int(this._doubleTimeArray[3]);
         var _loc8_:int = int(this._doubleTimeArray[4]);
         var _loc9_:int = int(this._doubleTimeArray[5]);
         var _loc10_:int = int(this._doubleTimeArray[6]);
         var _loc11_:int = int(this._doubleTimeArray[7]);
         if((_loc2_ > _loc4_ || _loc2_ == _loc4_ && _loc3_ >= _loc5_) && (_loc2_ < _loc6_ || _loc2_ == _loc6_ && _loc3_ < _loc7_) || (_loc2_ > _loc8_ || _loc2_ == _loc8_ && _loc3_ >= _loc9_) && (_loc2_ < _loc10_ || _loc2_ == _loc10_ && _loc3_ < _loc11_))
         {
            return true;
         }
         return false;
      }
      
      public function get startGameNeedMoney() : int
      {
         return this._startGameNeedMoney;
      }
      
      public function getBuyRecordStatus(param1:int) : Object
      {
         var _loc2_:int = 0;
         var _loc3_:Object = null;
         if(!this._buyRecordStatus)
         {
            this._buyRecordStatus = [];
            _loc2_ = 0;
            while(_loc2_ < 5)
            {
               _loc3_ = {};
               _loc3_.isNoPrompt = false;
               _loc3_.isBand = false;
               this._buyRecordStatus.push(_loc3_);
               _loc2_++;
            }
         }
         return this._buyRecordStatus[param1];
      }
      
      public function get rankAddInfo() : Array
      {
         return this._rankAddInfo;
      }
      
      public function get decelerateRate() : int
      {
         return this._decelerateRate;
      }
      
      public function get accelerateRate() : int
      {
         return this._accelerateRate;
      }
      
      public function get playerList() : Vector.<SevenDoublePlayerInfo>
      {
         return this._playerList;
      }
      
      public function get usableCount() : int
      {
         return this._usableCount;
      }
      
      public function get freeCount() : int
      {
         return this._freeCount;
      }
      
      public function get carStatus() : int
      {
         return this._carStatus;
      }
      
      public function get isInGame() : Boolean
      {
         return this._isInGame;
      }
      
      public function get isStart() : Boolean
      {
         return this._isStart;
      }
      
      public function get isLoadIconComplete() : Boolean
      {
         return this._isLoadIconComplete;
      }
      
      public function setup() : void
      {
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.SEVEN_DOUBLE,this.pkgHandler);
      }
      
      private function pkgHandler(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readByte();
         switch(_loc3_)
         {
            case SevenDoublePackageType.START_OR_END:
               this.openOrCloseHandler(_loc2_);
               break;
            case SevenDoublePackageType.CALL:
               this.changeCarStatus(_loc2_);
               break;
            case SevenDoublePackageType.START_GAME:
               this.startGameHandler(_loc2_);
               break;
            case SevenDoublePackageType.ENTER_GAME:
               this.enterGameHandler(_loc2_);
               break;
            case SevenDoublePackageType.ALL_READY:
               this.allReadyHandler(_loc2_);
               break;
            case SevenDoublePackageType.MOVE:
               this.moveHandler(_loc2_);
               break;
            case SevenDoublePackageType.REFRESH_ITEM:
               this.refreshItemHandler(_loc2_);
               break;
            case SevenDoublePackageType.REFRESH_BUFF:
               this.refreshBuffHandler(_loc2_);
               break;
            case SevenDoublePackageType.USE_SKILL:
               this.useSkillHandler(_loc2_);
               break;
            case SevenDoublePackageType.RANK_LIST:
               this.rankListHandler(_loc2_);
               break;
            case SevenDoublePackageType.ARRIVE:
               this.arriveHandler(_loc2_);
               break;
            case SevenDoublePackageType.DESTROY:
               this.destroyHandler(_loc2_);
               break;
            case SevenDoublePackageType.RE_ENTER_ALL_INFO:
               this.reEnterAllInfoHandler(_loc2_);
               break;
            case SevenDoublePackageType.IS_CAN_ENTER:
               this.canEnterHandler(_loc2_);
               break;
            case SevenDoublePackageType.REFRESH_FIGHT_STATE:
               this.refreshFightStateHandler(_loc2_);
               break;
            case SevenDoublePackageType.REFRESH_ENTER_COUNT:
               this.refreshEnterCountHandler(_loc2_);
               break;
            case SevenDoublePackageType.REFRESH_ITEM_FREE_COUNT:
               this.refreshItemFreeCountHandler(_loc2_);
               break;
            case SevenDoublePackageType.CANCEL_GAME:
               dispatchEvent(new Event(CANCEL_GAME));
         }
      }
      
      private function refreshItemFreeCountHandler(param1:PackageIn) : void
      {
         this._itemFreeCountList[0] = param1.readInt();
         this._itemFreeCountList[1] = param1.readInt();
         this._itemFreeCountList[2] = param1.readInt();
         dispatchEvent(new Event(REFRESH_ITEM_FREE_COUNT));
      }
      
      private function refreshEnterCountHandler(param1:PackageIn) : void
      {
         this._freeCount = param1.readInt();
         this._usableCount = param1.readInt();
         dispatchEvent(new Event(REFRESH_ENTER_COUNT));
      }
      
      private function refreshFightStateHandler(param1:PackageIn) : void
      {
         var _loc2_:int = param1.readInt();
         var _loc3_:int = param1.readInt();
         var _loc4_:int = param1.readInt();
         var _loc5_:int = param1.readInt();
         var _loc6_:SevenDoubleEvent = new SevenDoubleEvent(FIGHT_STATE_CHANGE);
         _loc6_.data = {
            "id":_loc2_,
            "zoneId":_loc3_,
            "fightState":_loc4_,
            "posX":_loc5_
         };
         dispatchEvent(_loc6_);
      }
      
      private function canEnterHandler(param1:PackageIn) : void
      {
         this._isInGame = param1.readBoolean();
         if(this._isInGame)
         {
            dispatchEvent(new SevenDoubleEvent(CAN_ENTER));
         }
         else
         {
            this.loadSevenDoubleModule();
         }
      }
      
      private function reEnterAllInfoHandler(param1:PackageIn) : void
      {
         var _loc7_:SevenDoublePlayerInfo = null;
         var _loc2_:Date = param1.readDate();
         this._playerList = new Vector.<SevenDoublePlayerInfo>();
         var _loc3_:int = param1.readInt();
         var _loc4_:int = 0;
         while(_loc4_ < _loc3_)
         {
            _loc7_ = new SevenDoublePlayerInfo();
            _loc7_.index = _loc4_;
            _loc7_.id = param1.readInt();
            _loc7_.zoneId = param1.readInt();
            _loc7_.name = param1.readUTF();
            _loc7_.carType = param1.readInt();
            _loc7_.posX = param1.readInt();
            _loc7_.fightState = param1.readInt();
            _loc7_.acceleEndTime = param1.readDate();
            _loc7_.deceleEndTime = param1.readDate();
            _loc7_.invisiEndTime = param1.readDate();
            if(_loc7_.zoneId == PlayerManager.Instance.Self.ZoneID && _loc7_.id == PlayerManager.Instance.Self.ID)
            {
               _loc7_.isSelf = true;
            }
            else
            {
               _loc7_.isSelf = false;
            }
            this._playerList.push(_loc7_);
            _loc4_++;
         }
         dispatchEvent(new Event(RE_ENTER_ALL_INFO));
         this.refreshItemHandler(param1);
         this.rankListHandler(param1);
         var _loc5_:Date = param1.readDate();
         var _loc6_:SevenDoubleEvent = new SevenDoubleEvent(ALL_READY);
         _loc6_.data = {
            "endTime":_loc2_,
            "isShowStartCountDown":false,
            "sprintEndTime":_loc5_
         };
         dispatchEvent(_loc6_);
      }
      
      private function destroyHandler(param1:PackageIn) : void
      {
         this._isInGame = false;
         this._carStatus = 0;
         dispatchEvent(new SevenDoubleEvent(DESTROY));
      }
      
      private function arriveHandler(param1:PackageIn) : void
      {
         var _loc2_:int = param1.readInt();
         var _loc3_:int = param1.readInt();
         if(_loc3_ == PlayerManager.Instance.Self.ZoneID && _loc2_ == PlayerManager.Instance.Self.ID)
         {
            this._isInGame = false;
            this._carStatus = 0;
         }
         var _loc4_:SevenDoubleEvent = new SevenDoubleEvent(ARRIVE);
         _loc4_.data = {
            "id":_loc2_,
            "zoneId":_loc3_
         };
         dispatchEvent(_loc4_);
      }
      
      private function rankListHandler(param1:PackageIn) : void
      {
         var _loc6_:int = 0;
         var _loc7_:String = null;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:Boolean = false;
         var _loc2_:int = param1.readInt();
         var _loc3_:Array = [];
         var _loc4_:int = 0;
         while(_loc4_ < _loc2_)
         {
            _loc6_ = param1.readInt();
            _loc7_ = param1.readUTF();
            _loc8_ = param1.readInt();
            _loc9_ = param1.readInt();
            _loc10_ = param1.readInt();
            _loc11_ = param1.readBoolean();
            _loc3_.push({
               "rank":_loc6_,
               "name":_loc7_,
               "carType":_loc8_,
               "id":_loc9_,
               "zoneId":_loc10_,
               "isSprint":_loc11_
            });
            _loc4_++;
         }
         _loc3_.sortOn("rank",Array.NUMERIC);
         var _loc5_:SevenDoubleEvent = new SevenDoubleEvent(RANK_ARRIVE_LIST);
         _loc5_.data = _loc3_;
         dispatchEvent(_loc5_);
      }
      
      private function useSkillHandler(param1:PackageIn) : void
      {
         var _loc2_:int = param1.readInt();
         var _loc3_:int = param1.readInt();
         var _loc4_:int = param1.readInt();
         var _loc5_:SevenDoubleEvent = new SevenDoubleEvent(USE_SKILL);
         _loc5_.data = {
            "id":_loc2_,
            "zoneId":_loc3_,
            "leapX":_loc4_,
            "sound":true
         };
         dispatchEvent(_loc5_);
      }
      
      private function refreshBuffHandler(param1:PackageIn) : void
      {
         var _loc2_:int = param1.readInt();
         var _loc3_:int = param1.readInt();
         var _loc4_:Date = param1.readDate();
         var _loc5_:Date = param1.readDate();
         var _loc6_:Date = param1.readDate();
         var _loc7_:SevenDoubleEvent = new SevenDoubleEvent(REFRESH_BUFF);
         _loc7_.data = {
            "id":_loc2_,
            "zoneId":_loc3_,
            "acceleEndTime":_loc4_,
            "deceleEndTime":_loc5_,
            "invisiEndTime":_loc6_
         };
         dispatchEvent(_loc7_);
      }
      
      private function refreshItemHandler(param1:PackageIn) : void
      {
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc2_:int = param1.readInt();
         var _loc3_:Array = [];
         var _loc4_:int = 0;
         while(_loc4_ < _loc2_)
         {
            _loc6_ = param1.readInt();
            _loc7_ = param1.readInt();
            _loc8_ = param1.readInt();
            _loc9_ = param1.readInt();
            _loc3_.push({
               "index":_loc6_,
               "type":_loc7_,
               "posX":_loc8_,
               "tag":_loc9_
            });
            _loc4_++;
         }
         var _loc5_:SevenDoubleEvent = new SevenDoubleEvent(REFRESH_ITEM);
         _loc5_.data = {"itemList":_loc3_};
         dispatchEvent(_loc5_);
      }
      
      private function moveHandler(param1:PackageIn) : void
      {
         var _loc2_:int = param1.readInt();
         var _loc3_:int = param1.readInt();
         var _loc4_:Number = param1.readInt();
         var _loc5_:SevenDoubleEvent = new SevenDoubleEvent(MOVE);
         _loc5_.data = {
            "zoneId":_loc3_,
            "id":_loc2_,
            "destX":_loc4_
         };
         dispatchEvent(_loc5_);
      }
      
      private function allReadyHandler(param1:PackageIn) : void
      {
         var _loc2_:Date = param1.readDate();
         var _loc3_:Date = param1.readDate();
         var _loc4_:SevenDoubleEvent = new SevenDoubleEvent(ALL_READY);
         _loc4_.data = {
            "endTime":_loc2_,
            "isShowStartCountDown":true,
            "sprintEndTime":_loc3_
         };
         dispatchEvent(_loc4_);
      }
      
      private function enterGameHandler(param1:PackageIn) : void
      {
         var _loc4_:SevenDoublePlayerInfo = null;
         this._playerList = new Vector.<SevenDoublePlayerInfo>();
         var _loc2_:int = param1.readInt();
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_)
         {
            _loc4_ = new SevenDoublePlayerInfo();
            _loc4_.index = _loc3_;
            _loc4_.zoneId = param1.readInt();
            _loc4_.id = param1.readInt();
            _loc4_.carType = param1.readInt();
            _loc4_.name = param1.readUTF();
            if(_loc4_.zoneId == PlayerManager.Instance.Self.ZoneID && _loc4_.id == PlayerManager.Instance.Self.ID)
            {
               _loc4_.isSelf = true;
            }
            else
            {
               _loc4_.isSelf = false;
            }
            this._playerList.push(_loc4_);
            _loc3_++;
         }
         this._isInGame = true;
         dispatchEvent(new Event(ENTER_GAME));
      }
      
      private function startGameHandler(param1:PackageIn) : void
      {
         dispatchEvent(new Event(START_GAME));
      }
      
      private function changeCarStatus(param1:PackageIn) : void
      {
         this._carStatus = param1.readInt();
         dispatchEvent(new Event(CAR_STATUS_CHANGE));
      }
      
      private function openOrCloseHandler(param1:PackageIn) : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:String = null;
         var _loc6_:Array = null;
         var _loc7_:Array = null;
         var _loc8_:int = 0;
         var _loc9_:ServerConfigInfo = null;
         var _loc10_:Array = null;
         var _loc11_:Array = null;
         var _loc12_:Array = null;
         var _loc13_:Date = null;
         var _loc14_:SevenDoubleCarInfo = null;
         var _loc15_:int = 0;
         var _loc16_:int = 0;
         var _loc17_:int = 0;
         var _loc18_:int = 0;
         var _loc19_:Array = null;
         var _loc20_:int = 0;
         param1.readInt();
         this._isStart = param1.readBoolean();
         if(this._isStart)
         {
            this.dataInfo = new SevenDoubleInfoVo();
            this._isInGame = param1.readBoolean();
            this._freeCount = param1.readInt();
            this._usableCount = param1.readInt();
            this._carStatus = param1.readInt();
            this.dataInfo.carInfo = {};
            _loc2_ = param1.readInt();
            _loc3_ = 0;
            while(_loc3_ < _loc2_)
            {
               _loc14_ = new SevenDoubleCarInfo();
               _loc14_.type = param1.readInt();
               _loc14_.needMoney = param1.readInt();
               _loc14_.speed = param1.readInt();
               _loc15_ = param1.readInt();
               _loc16_ = 0;
               while(_loc16_ < _loc15_)
               {
                  _loc17_ = param1.readInt();
                  _loc18_ = param1.readInt();
                  if(_loc17_ == -1300)
                  {
                     _loc14_.prestige = _loc18_;
                  }
                  else
                  {
                     _loc14_.itemId = _loc17_;
                     _loc14_.itemCount = _loc18_;
                  }
                  _loc16_++;
               }
               this.dataInfo.carInfo[_loc14_.type] = _loc14_;
               _loc3_++;
            }
            this.dataInfo.useSkillNeedMoney = [];
            this.dataInfo.useSkillNeedMoney.push(param1.readInt());
            this.dataInfo.useSkillNeedMoney.push(param1.readInt());
            this.dataInfo.useSkillNeedMoney.push(param1.readInt());
            this._rankAddInfo = [];
            _loc2_ = param1.readInt();
            _loc4_ = 0;
            while(_loc4_ < _loc2_)
            {
               this._rankAddInfo.push(param1.readInt());
               _loc4_++;
            }
            this._accelerateRate = param1.readInt();
            this._decelerateRate = param1.readInt();
            this._startGameNeedMoney = param1.readInt();
            _loc5_ = param1.readUTF();
            _loc6_ = [];
            _loc7_ = _loc5_.split("|");
            _loc8_ = 0;
            while(_loc8_ < _loc7_.length)
            {
               _loc19_ = _loc7_[_loc8_].split(",");
               _loc20_ = 0;
               while(_loc20_ < _loc19_.length)
               {
                  _loc6_ = _loc6_.concat(_loc19_[_loc20_].split(":"));
                  _loc20_++;
               }
               _loc8_++;
            }
            this._doubleTimeArray = _loc6_;
            this._sprintAwardInfo = param1.readUTF().split(",");
            this._timer = new Timer(1000);
            this._timer.addEventListener(TimerEvent.TIMER,this.timerHandler,false,0,true);
            this._timer.start();
            this.open();
            _loc9_ = ServerConfigManager.instance.findInfoByName("Activity77EndDate");
            _loc10_ = _loc9_.Value.split(" ");
            if((_loc10_[0] as String).indexOf("-") > 0)
            {
               _loc11_ = _loc10_[0].split("-");
            }
            else
            {
               _loc11_ = _loc10_[0].split("/");
            }
            _loc12_ = _loc10_[1].split(":");
            _loc13_ = new Date(_loc11_[0],int(_loc11_[1]) - 1,_loc11_[2],_loc12_[0],_loc12_[1],_loc12_[2]);
            this._endTime = _loc13_.getTime() / 1000;
            this._hasPrompted = new DictionaryData();
         }
         else
         {
            this._isInGame = false;
            if(this._timer)
            {
               this._timer.removeEventListener(TimerEvent.TIMER,this.timerHandler);
               this._timer.stop();
               this._timer = null;
            }
            dispatchEvent(new Event(END));
         }
      }
      
      private function timerHandler(param1:TimerEvent) : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         if(this.isInDoubleTime)
         {
            if(!this._isPromptDoubleTime)
            {
               ChatManager.Instance.sysChatAmaranth(LanguageMgr.GetTranslation("sevenDouble.doubleTime.startTipTxt"));
               this._isPromptDoubleTime = true;
            }
         }
         else if(this._isPromptDoubleTime)
         {
            ChatManager.Instance.sysChatAmaranth(LanguageMgr.GetTranslation("sevenDouble.doubleTime.endTipTxt"));
            this._isPromptDoubleTime = false;
         }
         if(this._endTime > 0)
         {
            _loc2_ = TimeManager.Instance.Now().getTime() / 1000;
            _loc3_ = this._endTime - _loc2_;
            if(_loc3_ > 0)
            {
               _loc4_ = _loc3_ % 3600;
               if(_loc4_ < 5)
               {
                  _loc5_ = _loc3_ / 3600;
                  if(_loc5_ <= 48 && _loc5_ > 0 && !this._hasPrompted.hasKey(_loc5_))
                  {
                     ChatManager.Instance.sysChatAmaranth(LanguageMgr.GetTranslation("escort.willEnd.promptTxt",_loc5_));
                     this._hasPrompted.add(_loc5_,1);
                  }
               }
            }
         }
      }
      
      public function enterMainViewHandler() : void
      {
      }
      
      public function leaveMainViewHandler() : void
      {
         this._playerList = null;
      }
      
      public function open() : void
      {
         this._isLoadIconComplete = true;
         dispatchEvent(new Event(ICON_RES_LOAD_COMPLETE));
      }
      
      public function loadSevenDoubleModule() : void
      {
         this._loadResCount = 0;
         UIModuleSmallLoading.Instance.progress = 0;
         UIModuleSmallLoading.Instance.show();
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadFrameCompleteHandler);
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUimoduleLoadProgress);
         UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.SEVEN_DOUBLE_FRAME);
         UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.SEVEN_DOUBLE_GAME);
         UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.DDTROOM);
      }
      
      private function onUimoduleLoadProgress(param1:UIModuleEvent) : void
      {
         var _loc2_:Number = NaN;
         if(param1.module == UIModuleTypes.SEVEN_DOUBLE_FRAME || param1.module == UIModuleTypes.SEVEN_DOUBLE_GAME || param1.module == UIModuleTypes.DDTROOM)
         {
            _loc2_ = param1.loader.progress;
            _loc2_ = _loc2_ > 0.99?Number(0.99):Number(_loc2_);
            UIModuleSmallLoading.Instance.progress = _loc2_ * 100;
         }
      }
      
      private function loadFrameCompleteHandler(param1:UIModuleEvent) : void
      {
         var _loc2_:SevenDoubleFrame = null;
         if(param1.module == UIModuleTypes.SEVEN_DOUBLE_FRAME)
         {
            this._loadResCount++;
         }
         if(param1.module == UIModuleTypes.SEVEN_DOUBLE_GAME)
         {
            this._loadResCount++;
         }
         if(param1.module == UIModuleTypes.DDTROOM)
         {
            this._loadResCount++;
         }
         if(this._loadResCount >= 3)
         {
            UIModuleSmallLoading.Instance.hide();
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadFrameCompleteHandler);
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUimoduleLoadProgress);
            _loc2_ = ComponentFactory.Instance.creatComponentByStylename("SevenDoubleFrame");
            LayerManager.Instance.addToLayer(_loc2_,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
         }
      }
      
      public function getPlayerResUrl(param1:Boolean, param2:int) : String
      {
         var _loc3_:String = null;
         if(param1)
         {
            _loc3_ = "self";
         }
         else
         {
            _loc3_ = "other";
         }
         return PathManager.SITE_MAIN + "image/sevendouble/" + "sevendouble" + _loc3_ + param2 + ".swf";
      }
      
      public function loadSound() : void
      {
         var _loc1_:BaseLoader = LoadResourceManager.Instance.createLoader(PathManager.SITE_MAIN + "image/sevendouble/sevenDoubleAudio.swf",BaseLoader.MODULE_LOADER);
         _loc1_.addEventListener(LoaderEvent.COMPLETE,this.loadSoundCompleteHandler);
         LoadResourceManager.Instance.startLoad(_loc1_);
      }
      
      private function loadSoundCompleteHandler(param1:LoaderEvent) : void
      {
         param1.loader.removeEventListener(LoaderEvent.COMPLETE,this.loadSoundCompleteHandler);
         SoundManager.instance.initSevenDoubleSound();
      }
   }
}
