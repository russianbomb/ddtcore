package sevenDouble.view
{
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.UIModuleTypes;
   import ddt.manager.LanguageMgr;
   import ddt.manager.SoundManager;
   import flash.display.DisplayObject;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import store.HelpFrame;
   
   public class SevenDoubleHelpBtn extends Sprite implements Disposeable
   {
       
      
      private var _btn:SimpleBitmapButton;
      
      public function SevenDoubleHelpBtn(param1:Boolean = true)
      {
         super();
         if(param1)
         {
            this._btn = ComponentFactory.Instance.creatComponentByStylename("sevenDouble.HelpButton");
         }
         else
         {
            this._btn = ComponentFactory.Instance.creatComponentByStylename("sevenDouble.HelpButtonShort");
         }
         addChild(this._btn);
         this._btn.addEventListener(MouseEvent.CLICK,this.clickHandler,false,0,true);
      }
      
      private function clickHandler(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         param1.stopImmediatePropagation();
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadIconCompleteHandler);
         UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.SEVEN_DOUBLE_ICON);
      }
      
      private function loadIconCompleteHandler(param1:UIModuleEvent) : void
      {
         var _loc2_:DisplayObject = null;
         var _loc3_:HelpFrame = null;
         if(param1.module == UIModuleTypes.SEVEN_DOUBLE_ICON)
         {
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadIconCompleteHandler);
            _loc2_ = ComponentFactory.Instance.creat("sevenDouble.HelpPrompt");
            _loc3_ = ComponentFactory.Instance.creat("sevenDouble.HelpFrame");
            _loc3_.setView(_loc2_);
            _loc3_.titleText = LanguageMgr.GetTranslation("store.view.HelpButtonText");
            LayerManager.Instance.addToLayer(_loc3_,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
         }
      }
      
      public function dispose() : void
      {
         if(this._btn)
         {
            this._btn.removeEventListener(MouseEvent.CLICK,this.clickHandler);
         }
         ObjectUtils.disposeObject(this._btn);
         this._btn = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
