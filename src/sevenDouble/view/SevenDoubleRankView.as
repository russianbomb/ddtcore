package sevenDouble.view
{
   import com.greensock.TweenLite;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.LanguageMgr;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import sevenDouble.SevenDoubleManager;
   import sevenDouble.event.SevenDoubleEvent;
   
   public class SevenDoubleRankView extends Sprite implements Disposeable
   {
       
      
      private var _bg:Bitmap;
      
      private var _moveInBtn:SimpleBitmapButton;
      
      private var _moveOutBtn:SimpleBitmapButton;
      
      private var _rankTxt:FilterFrameText;
      
      private var _nameTxt:FilterFrameText;
      
      private var _rateTxt:FilterFrameText;
      
      private var _sprintTxt:FilterFrameText;
      
      private var _rankCellList:Vector.<SevenDoubleRankCell>;
      
      public function SevenDoubleRankView()
      {
         super();
         this.x = 787;
         this.y = -3;
         this.initView();
         this.initEvent();
         this.setInOutVisible(true);
      }
      
      private function initView() : void
      {
         var _loc1_:int = 0;
         var _loc2_:SevenDoubleRankCell = null;
         this._bg = ComponentFactory.Instance.creatBitmap("asset.sevenDouble.rankViewBg");
         this._moveOutBtn = ComponentFactory.Instance.creatComponentByStylename("sevenDouble.rankViewMoveOutBtn");
         this._moveInBtn = ComponentFactory.Instance.creatComponentByStylename("sevenDouble.rankViewMoveInBtn");
         this._rankTxt = ComponentFactory.Instance.creatComponentByStylename("sevenDouble.rankView.titleTxt");
         this._rankTxt.text = LanguageMgr.GetTranslation("sevenDouble.rankView.rankTitleTxt");
         this._nameTxt = ComponentFactory.Instance.creatComponentByStylename("sevenDouble.rankView.titleTxt");
         this._nameTxt.text = LanguageMgr.GetTranslation("sevenDouble.rankView.nameTitleTxt");
         PositionUtils.setPos(this._nameTxt,"sevenDouble.rankView.nameTitleTxtPos");
         this._rateTxt = ComponentFactory.Instance.creatComponentByStylename("sevenDouble.rankView.titleTxt");
         this._rateTxt.text = LanguageMgr.GetTranslation("sevenDouble.rankView.rateTitleTxt");
         PositionUtils.setPos(this._rateTxt,"sevenDouble.rankView.rateTitleTxtPos");
         this._sprintTxt = ComponentFactory.Instance.creatComponentByStylename("sevenDouble.rankView.titleTxt");
         this._sprintTxt.text = LanguageMgr.GetTranslation("sevenDouble.rankView.sprintTitleTxt");
         PositionUtils.setPos(this._sprintTxt,"sevenDouble.rankView.sprintTitleTxtPos");
         addChild(this._bg);
         addChild(this._moveOutBtn);
         addChild(this._moveInBtn);
         addChild(this._rankTxt);
         addChild(this._nameTxt);
         addChild(this._rateTxt);
         addChild(this._sprintTxt);
         this._rankCellList = new Vector.<SevenDoubleRankCell>();
         _loc1_ = 0;
         while(_loc1_ < 5)
         {
            _loc2_ = new SevenDoubleRankCell(_loc1_);
            _loc2_.x = 17;
            _loc2_.y = 63 + _loc1_ * 28;
            addChild(_loc2_);
            this._rankCellList.push(_loc2_);
            _loc1_++;
         }
      }
      
      private function initEvent() : void
      {
         this._moveOutBtn.addEventListener(MouseEvent.CLICK,this.outHandler,false,0,true);
         this._moveInBtn.addEventListener(MouseEvent.CLICK,this.inHandler,false,0,true);
         SevenDoubleManager.instance.addEventListener(SevenDoubleManager.RANK_LIST,this.refreshRankList);
      }
      
      private function outHandler(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         this.setInOutVisible(false);
         TweenLite.to(this,0.5,{"x":1000});
      }
      
      private function setInOutVisible(param1:Boolean) : void
      {
         this._moveOutBtn.visible = param1;
         this._moveInBtn.visible = !param1;
      }
      
      private function inHandler(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         this.setInOutVisible(true);
         TweenLite.to(this,0.5,{"x":787});
      }
      
      private function refreshRankList(param1:SevenDoubleEvent) : void
      {
         var _loc2_:Array = param1.data as Array;
         var _loc3_:int = _loc2_.length;
         var _loc4_:int = 0;
         while(_loc4_ < _loc3_)
         {
            this._rankCellList[_loc4_].setName(_loc2_[_loc4_].name,_loc2_[_loc4_].carType);
            _loc4_++;
         }
      }
      
      private function removeEvent() : void
      {
         this._moveOutBtn.removeEventListener(MouseEvent.CLICK,this.outHandler);
         this._moveInBtn.removeEventListener(MouseEvent.CLICK,this.inHandler);
         SevenDoubleManager.instance.removeEventListener(SevenDoubleManager.RANK_LIST,this.refreshRankList);
      }
      
      public function dispose() : void
      {
         this.removeEvent();
         ObjectUtils.disposeAllChildren(this);
         this._bg = null;
         this._moveInBtn = null;
         this._moveOutBtn = null;
         this._rankTxt = null;
         this._nameTxt = null;
         this._rateTxt = null;
         this._sprintTxt = null;
         this._rankCellList = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
