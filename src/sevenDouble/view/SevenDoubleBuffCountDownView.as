package sevenDouble.view
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.LanguageMgr;
   import ddt.manager.TimeManager;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.TimerEvent;
   import flash.utils.Timer;
   
   public class SevenDoubleBuffCountDownView extends Sprite implements Disposeable
   {
      
      public static const END:String = "SevenDoubleBuffCountDownEnd";
       
      
      private var _bg:Bitmap;
      
      private var _countDownTxt:FilterFrameText;
      
      private var _titleTxt:FilterFrameText;
      
      private var _endTime:Date;
      
      private var _timer:Timer;
      
      private var _type:int;
      
      private const colorList:Array = [16711680,1813759,59158];
      
      public function SevenDoubleBuffCountDownView(param1:Date, param2:int, param3:int)
      {
         super();
         if(param2 == 3)
         {
            this.x = -51;
         }
         else
         {
            this.x = -203;
         }
         this.y = -138;
         this._endTime = param1;
         this._type = param2;
         this._bg = ComponentFactory.Instance.creatBitmap("asset.sevenDouble.buffCountDownBg");
         this._titleTxt = ComponentFactory.Instance.creatComponentByStylename("sevenDouble.game.buffTitleTxt");
         this._titleTxt.text = LanguageMgr.GetTranslation("sevenDouble.game.buffTitleTxt" + this._type);
         this._titleTxt.textColor = this.colorList[this._type - 1];
         this._countDownTxt = ComponentFactory.Instance.creatComponentByStylename("sevenDouble.game.buffCountDownTxt");
         addChild(this._bg);
         addChild(this._titleTxt);
         this._countDownTxt.x = this._titleTxt.x + this._titleTxt.width + 10;
         this._countDownTxt.text = "00:00";
         addChild(this._countDownTxt);
         this._bg.width = this._titleTxt.width + 14 + this._countDownTxt.width;
         this.refreshTxt(null);
         this._timer = new Timer(1000);
         this._timer.addEventListener(TimerEvent.TIMER,this.refreshTxt,false,0,true);
         this._timer.start();
      }
      
      public function get type() : int
      {
         return this._type;
      }
      
      public function set endTime(param1:Date) : void
      {
         this._endTime = param1;
         this.refreshTxt(null);
      }
      
      private function refreshTxt(param1:TimerEvent) : void
      {
         var _loc2_:Number = this._endTime.getTime();
         var _loc3_:Number = TimeManager.Instance.Now().getTime();
         var _loc4_:Number = _loc2_ - _loc3_;
         _loc4_ = _loc4_ < 0?Number(0):Number(_loc4_);
         var _loc5_:int = _loc4_ / TimeManager.Minute_TICKS;
         var _loc6_:int = _loc4_ % TimeManager.Minute_TICKS / 1000;
         var _loc7_:String = _loc5_ < 10?"0" + _loc5_:_loc5_.toString();
         var _loc8_:String = _loc6_ < 10?"0" + _loc6_:_loc6_.toString();
         this._countDownTxt.text = _loc7_ + ":" + _loc8_;
         this._countDownTxt.textColor = this.colorList[this._type - 1];
         if(_loc6_ <= 0)
         {
            dispatchEvent(new Event(END));
         }
      }
      
      public function dispose() : void
      {
         if(this._timer)
         {
            this._timer.removeEventListener(TimerEvent.TIMER,this.refreshTxt);
            this._timer.stop();
         }
         this._timer = null;
         this._endTime = null;
         ObjectUtils.disposeAllChildren(this);
         this._bg = null;
         this._titleTxt = null;
         this._countDownTxt = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
