package sevenDouble.view
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.LanguageMgr;
   import ddt.manager.TimeManager;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.TimerEvent;
   import flash.utils.Timer;
   
   public class SevenDoubleSprintCountDownView extends Sprite implements Disposeable
   {
       
      
      private var _bg:Bitmap;
      
      private var _txt:FilterFrameText;
      
      private var _recordTxt:String;
      
      private var _timer:Timer;
      
      private var _endTime:Date;
      
      public function SevenDoubleSprintCountDownView()
      {
         super();
         this.x = 340;
         this.y = 34;
         this.initView();
         this._timer = new Timer(1000);
         this._timer.addEventListener(TimerEvent.TIMER,this.timerHandler,false,0,true);
      }
      
      private function initView() : void
      {
         this._bg = ComponentFactory.Instance.creatBitmap("asset.sevenDouble.countDownBg");
         this._txt = ComponentFactory.Instance.creatComponentByStylename("sevenDouble.countDownView.txt");
         this._recordTxt = LanguageMgr.GetTranslation("sevenDouble.frame.sprintCountDownViewTxt");
         this._txt.text = this._recordTxt + "--:--";
         addChild(this._bg);
         addChild(this._txt);
      }
      
      public function setCountDown(param1:Date) : void
      {
         this._timer.start();
         this._endTime = param1;
         this.refreshView(this._endTime);
      }
      
      private function refreshView(param1:Date) : void
      {
         var _loc2_:Number = param1.getTime();
         var _loc3_:Number = TimeManager.Instance.Now().getTime();
         var _loc4_:Number = _loc2_ - _loc3_;
         _loc4_ = _loc4_ < 0?Number(0):Number(_loc4_);
         var _loc5_:int = _loc4_ / TimeManager.Minute_TICKS;
         var _loc6_:int = _loc4_ % TimeManager.Minute_TICKS / 1000;
         var _loc7_:String = _loc5_ < 10?"0" + _loc5_:_loc5_.toString();
         var _loc8_:String = _loc6_ < 10?"0" + _loc6_:_loc6_.toString();
         this._txt.text = this._recordTxt + _loc7_ + ":" + _loc8_;
      }
      
      private function timerHandler(param1:TimerEvent) : void
      {
         this.refreshView(this._endTime);
      }
      
      public function dispose() : void
      {
         if(this._timer)
         {
            this._timer.removeEventListener(TimerEvent.TIMER,this.timerHandler);
            this._timer.stop();
         }
         this._timer = null;
         ObjectUtils.disposeAllChildren(this);
         this._bg = null;
         this._txt = null;
         this._recordTxt = null;
         this._endTime = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
