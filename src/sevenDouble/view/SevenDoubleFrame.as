package sevenDouble.view
{
   import baglocked.BaglockedManager;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.LanguageMgr;
   import ddt.manager.LeavePageManager;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.manager.StateManager;
   import ddt.states.StateType;
   import flash.display.Bitmap;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import sevenDouble.SevenDoubleManager;
   
   public class SevenDoubleFrame extends Frame
   {
       
      
      private var _bg:ScaleBitmapImage;
      
      private var _bottomBg:ScaleBitmapImage;
      
      private var _cellList:Vector.<SevenDoubleFrameItemCell>;
      
      private var _btn:SimpleBitmapButton;
      
      private var _countTxt:FilterFrameText;
      
      private var _matchView:SevenDoubleMatchView;
      
      private var _helpBtn:SevenDoubleHelpBtn;
      
      private var _doubleTagIcon:Bitmap;
      
      public function SevenDoubleFrame()
      {
         super();
         this._cellList = new Vector.<SevenDoubleFrameItemCell>();
         this.initView();
         this.initEvent();
      }
      
      private function initView() : void
      {
         var _loc2_:SevenDoubleFrameItemCell = null;
         titleText = LanguageMgr.GetTranslation("sevenDouble.frame.titleTxt");
         this._bg = ComponentFactory.Instance.creatComponentByStylename("sevenDouble.frame.bg");
         addToContent(this._bg);
         this._bottomBg = ComponentFactory.Instance.creatComponentByStylename("sevenDouble.frame.bottomBg");
         addToContent(this._bottomBg);
         var _loc1_:int = 0;
         while(_loc1_ < 3)
         {
            _loc2_ = new SevenDoubleFrameItemCell(_loc1_,SevenDoubleManager.instance.dataInfo.carInfo[_loc1_]);
            _loc2_.x = 8 + (_loc2_.width + 4) * _loc1_;
            _loc2_.y = 10;
            addToContent(_loc2_);
            this._cellList.push(_loc2_);
            _loc1_++;
         }
         this._countTxt = ComponentFactory.Instance.creatComponentByStylename("sevenDouble.frame.countTxt");
         this.refreshEnterCountHandler(null);
         addToContent(this._countTxt);
         this._helpBtn = new SevenDoubleHelpBtn(false);
         addToContent(this._helpBtn);
         this._doubleTagIcon = ComponentFactory.Instance.creatBitmap("asset.sevenDouble.doubleScoreIcon");
         addToContent(this._doubleTagIcon);
         this.refreshDoubleTagIcon();
      }
      
      private function refreshDoubleTagIcon() : void
      {
         if(SevenDoubleManager.instance.isInDoubleTime)
         {
            this._doubleTagIcon.visible = true;
         }
         else
         {
            this._doubleTagIcon.visible = false;
         }
      }
      
      private function initEvent() : void
      {
         addEventListener(FrameEvent.RESPONSE,this.__responseHandler);
         this._btn.addEventListener(MouseEvent.CLICK,this.clickHandler,false,0,true);
         SevenDoubleManager.instance.addEventListener(SevenDoubleManager.START_GAME,this.startGameHandler);
         SevenDoubleManager.instance.addEventListener(SevenDoubleManager.ENTER_GAME,this.enterGameHandler);
         SevenDoubleManager.instance.addEventListener(SevenDoubleManager.END,this.endHandler);
         SevenDoubleManager.instance.addEventListener(SevenDoubleManager.REFRESH_ENTER_COUNT,this.refreshEnterCountHandler);
      }
      
      private function refreshEnterCountHandler(param1:Event) : void
      {
         var _loc3_:int = 0;
         if(this._btn)
         {
            this._btn.removeEventListener(MouseEvent.CLICK,this.clickHandler);
            ObjectUtils.disposeObject(this._btn);
         }
         var _loc2_:int = SevenDoubleManager.instance.freeCount;
         if(_loc2_ > 0)
         {
            this._btn = ComponentFactory.Instance.creatComponentByStylename("sevenDouble.frame.freeBtn");
            _loc3_ = _loc2_;
         }
         else
         {
            this._btn = ComponentFactory.Instance.creatComponentByStylename("sevenDouble.frame.startBtn");
            _loc3_ = SevenDoubleManager.instance.usableCount;
         }
         addToContent(this._btn);
         this._countTxt.text = "(" + _loc3_ + ")";
      }
      
      private function endHandler(param1:Event) : void
      {
         SocketManager.Instance.out.sendSevenDoubleCancelGame();
         this.dispose();
      }
      
      private function enterGameHandler(param1:Event) : void
      {
         this.dispose();
         StateManager.setState(StateType.SEVEN_DOUBLE_SCENE);
      }
      
      private function startGameHandler(param1:Event) : void
      {
         this._matchView = ComponentFactory.Instance.creatComponentByStylename("sevenDouble.frame.matchView");
         this._matchView.show();
         this._matchView.addEventListener(FrameEvent.RESPONSE,this.cancelMatchHandler,false,0,true);
      }
      
      private function cancelMatchHandler(param1:FrameEvent) : void
      {
         if(param1.responseCode == FrameEvent.ESC_CLICK || param1.responseCode == FrameEvent.CANCEL_CLICK || param1.responseCode == FrameEvent.CLOSE_CLICK)
         {
            SoundManager.instance.play("008");
            SocketManager.Instance.out.sendSevenDoubleCancelGame();
            this.disposeMatchView();
         }
      }
      
      private function disposeMatchView() : void
      {
         if(this._matchView)
         {
            this._matchView.removeEventListener(FrameEvent.RESPONSE,this.cancelMatchHandler);
            ObjectUtils.disposeObject(this._matchView);
            this._matchView = null;
         }
      }
      
      private function clickHandler(param1:MouseEvent) : void
      {
         var _loc2_:int = 0;
         var _loc3_:Object = null;
         var _loc4_:BaseAlerFrame = null;
         SoundManager.instance.play("008");
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         if(SevenDoubleManager.instance.freeCount > 0)
         {
            SocketManager.Instance.out.sendSevenDoubleStartGame(false);
         }
         else
         {
            if(SevenDoubleManager.instance.usableCount <= 0)
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("sevenDouble.noEnoughUsableCount"));
               return;
            }
            _loc2_ = SevenDoubleManager.instance.startGameNeedMoney;
            _loc3_ = SevenDoubleManager.instance.getBuyRecordStatus(1);
            if(_loc3_.isNoPrompt)
            {
               if(_loc3_.isBand && PlayerManager.Instance.Self.BandMoney < _loc2_)
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("bindMoneyPoorNote"));
                  _loc3_.isNoPrompt = false;
               }
               else if(!_loc3_.isBand && PlayerManager.Instance.Self.Money < _loc2_)
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("moneyPoorNote"));
                  _loc3_.isNoPrompt = false;
               }
               else
               {
                  SocketManager.Instance.out.sendSevenDoubleStartGame(_loc3_.isBand);
                  return;
               }
            }
            _loc4_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("sevenDouble.frame.startGameConfirmTxt",_loc2_),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),true,true,true,LayerManager.BLCAK_BLOCKGOUND,null,"SevenDoubleBuyConfirmView",30,true);
            _loc4_.moveEnable = false;
            _loc4_.addEventListener(FrameEvent.RESPONSE,this.startConfirm,false,0,true);
         }
      }
      
      private function startConfirm(param1:FrameEvent) : void
      {
         var _loc3_:int = 0;
         var _loc4_:BaseAlerFrame = null;
         var _loc5_:Object = null;
         SoundManager.instance.play("008");
         var _loc2_:BaseAlerFrame = param1.currentTarget as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.startConfirm);
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            _loc3_ = SevenDoubleManager.instance.startGameNeedMoney;
            if(_loc2_.isBand && PlayerManager.Instance.Self.BandMoney < _loc3_)
            {
               _loc4_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("sevenDouble.game.useSkillNoEnoughReConfirm"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),true,true,true,LayerManager.BLCAK_BLOCKGOUND);
               _loc4_.moveEnable = false;
               _loc4_.addEventListener(FrameEvent.RESPONSE,this.startGameReConfirm,false,0,true);
               return;
            }
            if(!_loc2_.isBand && PlayerManager.Instance.Self.Money < _loc3_)
            {
               LeavePageManager.showFillFrame();
               return;
            }
            if((_loc2_ as SevenDoubleBuyConfirmView).isNoPrompt)
            {
               _loc5_ = SevenDoubleManager.instance.getBuyRecordStatus(1);
               _loc5_.isNoPrompt = true;
               _loc5_.isBand = _loc2_.isBand;
            }
            SocketManager.Instance.out.sendSevenDoubleStartGame(_loc2_.isBand);
         }
      }
      
      private function startGameReConfirm(param1:FrameEvent) : void
      {
         var _loc3_:int = 0;
         SoundManager.instance.play("008");
         var _loc2_:BaseAlerFrame = param1.currentTarget as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.startGameReConfirm);
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            _loc3_ = SevenDoubleManager.instance.startGameNeedMoney;
            if(PlayerManager.Instance.Self.Money < _loc3_)
            {
               LeavePageManager.showFillFrame();
               return;
            }
            SocketManager.Instance.out.sendSevenDoubleStartGame(false);
         }
      }
      
      private function __responseHandler(param1:FrameEvent) : void
      {
         if(param1.responseCode == FrameEvent.CLOSE_CLICK || param1.responseCode == FrameEvent.ESC_CLICK)
         {
            SoundManager.instance.play("008");
            this.dispose();
         }
      }
      
      private function removeEvent() : void
      {
         removeEventListener(FrameEvent.RESPONSE,this.__responseHandler);
         if(this._btn)
         {
            this._btn.removeEventListener(MouseEvent.CLICK,this.clickHandler);
         }
         SevenDoubleManager.instance.removeEventListener(SevenDoubleManager.START_GAME,this.startGameHandler);
         SevenDoubleManager.instance.removeEventListener(SevenDoubleManager.ENTER_GAME,this.enterGameHandler);
         SevenDoubleManager.instance.removeEventListener(SevenDoubleManager.END,this.endHandler);
         SevenDoubleManager.instance.removeEventListener(SevenDoubleManager.REFRESH_ENTER_COUNT,this.refreshEnterCountHandler);
      }
      
      override public function dispose() : void
      {
         this.disposeMatchView();
         this.removeEvent();
         super.dispose();
         this._bg = null;
         this._bottomBg = null;
         this._cellList = null;
         this._btn = null;
         this._countTxt = null;
         this._helpBtn = null;
         this._doubleTagIcon = null;
      }
   }
}
