package sevenDouble.view
{
   import baglocked.BaglockedManager;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.LanguageMgr;
   import ddt.manager.LeavePageManager;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import flash.display.Bitmap;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.utils.setTimeout;
   import sevenDouble.SevenDoubleManager;
   import sevenDouble.event.SevenDoubleEvent;
   
   public class SevenDoubleThreeBtnView extends Sprite implements Disposeable
   {
       
      
      private var _bg:Bitmap;
      
      private var _leapBtn:SimpleBitmapButton;
      
      private var _invisibilityBtn:SimpleBitmapButton;
      
      private var _cleanBtn:SimpleBitmapButton;
      
      private var _recordClickTag:int;
      
      private var _freeTipList:Vector.<MovieClip>;
      
      public function SevenDoubleThreeBtnView()
      {
         super();
         this.x = 885;
         this.y = 258;
         this.initView();
         this.initEvent();
      }
      
      private function initView() : void
      {
         var _loc1_:int = 0;
         var _loc2_:MovieClip = null;
         this._bg = ComponentFactory.Instance.creatBitmap("asset.sevenDouble.threeBtnBg");
         this._leapBtn = ComponentFactory.Instance.creatComponentByStylename("sevenDouble.leapBtn");
         this._leapBtn.tipData = LanguageMgr.GetTranslation("sevenDouble.game.leapBtnTipTxt");
         this._invisibilityBtn = ComponentFactory.Instance.creatComponentByStylename("sevenDouble.invisibilityBtn");
         this._invisibilityBtn.tipData = LanguageMgr.GetTranslation("sevenDouble.game.invisibilityBtnTipTxt");
         this._cleanBtn = ComponentFactory.Instance.creatComponentByStylename("sevenDouble.cleanBtn");
         this._cleanBtn.tipData = LanguageMgr.GetTranslation("sevenDouble.game.cleanBtnTipTxt");
         addChild(this._bg);
         addChild(this._leapBtn);
         addChild(this._invisibilityBtn);
         addChild(this._cleanBtn);
         this._freeTipList = new Vector.<MovieClip>();
         _loc1_ = 0;
         while(_loc1_ < 3)
         {
            _loc2_ = ComponentFactory.Instance.creat("asset.sevenDouble.freeTipMc") as MovieClip;
            _loc2_.x = -36;
            _loc2_.y = -14 + 44 * _loc1_;
            _loc2_.mouseEnabled = false;
            _loc2_.mouseChildren = false;
            addChild(_loc2_);
            this._freeTipList.push(_loc2_);
            _loc1_++;
         }
         this.refreshFreeCount(null);
      }
      
      private function initEvent() : void
      {
         this._leapBtn.addEventListener(MouseEvent.CLICK,this.clickHandler,false,0,true);
         this._leapBtn.addEventListener(MouseEvent.MOUSE_OVER,this.overHandler,false,0,true);
         this._leapBtn.addEventListener(MouseEvent.MOUSE_OUT,this.outHandler,false,0,true);
         this._invisibilityBtn.addEventListener(MouseEvent.CLICK,this.clickHandler,false,0,true);
         this._cleanBtn.addEventListener(MouseEvent.CLICK,this.clickHandler,false,0,true);
         SevenDoubleManager.instance.addEventListener(SevenDoubleManager.REFRESH_ITEM_FREE_COUNT,this.refreshFreeCount);
      }
      
      private function refreshFreeCount(param1:Event) : void
      {
         var _loc2_:Array = SevenDoubleManager.instance.itemFreeCountList;
         var _loc3_:int = 0;
         while(_loc3_ < 3)
         {
            if(_loc2_[_loc3_] > 0)
            {
               this._freeTipList[_loc3_].tf.text = _loc2_[_loc3_].toString();
               this._freeTipList[_loc3_].visible = true;
            }
            else
            {
               this._freeTipList[_loc3_].visible = false;
            }
            _loc3_++;
         }
      }
      
      private function outHandler(param1:MouseEvent) : void
      {
         var _loc2_:SevenDoubleEvent = new SevenDoubleEvent(SevenDoubleManager.LEAP_PROMPT_SHOW_HIDE);
         _loc2_.data = {"isShow":false};
         SevenDoubleManager.instance.dispatchEvent(_loc2_);
      }
      
      private function overHandler(param1:MouseEvent) : void
      {
         var _loc2_:SevenDoubleEvent = new SevenDoubleEvent(SevenDoubleManager.LEAP_PROMPT_SHOW_HIDE);
         _loc2_.data = {"isShow":true};
         SevenDoubleManager.instance.dispatchEvent(_loc2_);
      }
      
      private function enableBtn(param1:SimpleBitmapButton) : void
      {
         param1.enable = true;
      }
      
      private function unEnableBtn(param1:int) : void
      {
         var _loc2_:SimpleBitmapButton = null;
         switch(param1)
         {
            case 0:
               _loc2_ = this._leapBtn;
               break;
            case 1:
               _loc2_ = this._invisibilityBtn;
               break;
            case 2:
               _loc2_ = this._cleanBtn;
         }
         if(_loc2_)
         {
            _loc2_.enable = false;
            setTimeout(this.enableBtn,5000,_loc2_);
         }
      }
      
      private function clickHandler(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         var _loc2_:SimpleBitmapButton = param1.target as SimpleBitmapButton;
         switch(_loc2_)
         {
            case this._leapBtn:
               this._recordClickTag = 0;
               break;
            case this._invisibilityBtn:
               this._recordClickTag = 1;
               break;
            case this._cleanBtn:
               this._recordClickTag = 2;
         }
         if(this._freeTipList[this._recordClickTag].visible)
         {
            SocketManager.Instance.out.sendSevenDoubleUseSkill(this._recordClickTag,false,true);
            this.unEnableBtn(this._recordClickTag);
            return;
         }
         var _loc3_:Object = SevenDoubleManager.instance.getBuyRecordStatus(this._recordClickTag + 2);
         var _loc4_:int = SevenDoubleManager.instance.dataInfo.useSkillNeedMoney[this._recordClickTag];
         if(_loc3_.isNoPrompt)
         {
            if(_loc3_.isBand && PlayerManager.Instance.Self.BandMoney < _loc4_)
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("bindMoneyPoorNote"));
               _loc3_.isNoPrompt = false;
            }
            else if(!_loc3_.isBand && PlayerManager.Instance.Self.Money < _loc4_)
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("moneyPoorNote"));
               _loc3_.isNoPrompt = false;
            }
            else
            {
               SocketManager.Instance.out.sendSevenDoubleUseSkill(this._recordClickTag,_loc3_.isBand,this._freeTipList[this._recordClickTag].visible);
               this.unEnableBtn(this._recordClickTag);
               return;
            }
         }
         var _loc5_:BaseAlerFrame = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("sevenDouble.frame.useSkillConfirmTxt" + this._recordClickTag,_loc4_),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),true,true,true,LayerManager.BLCAK_BLOCKGOUND,null,"SevenDoubleBuyConfirmView1",30,true,AlertManager.NOSELECTBTN);
         _loc5_.moveEnable = false;
         _loc5_.addEventListener(FrameEvent.RESPONSE,this.useSkillConfirm,false,0,true);
      }
      
      private function useSkillConfirm(param1:FrameEvent) : void
      {
         var _loc3_:int = 0;
         var _loc4_:BaseAlerFrame = null;
         var _loc5_:Object = null;
         SoundManager.instance.play("008");
         var _loc2_:BaseAlerFrame = param1.currentTarget as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.useSkillConfirm);
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            _loc3_ = SevenDoubleManager.instance.dataInfo.useSkillNeedMoney[this._recordClickTag];
            if(_loc2_.isBand && PlayerManager.Instance.Self.BandMoney < _loc3_)
            {
               _loc4_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("sevenDouble.game.useSkillNoEnoughReConfirm"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),true,true,true,LayerManager.BLCAK_BLOCKGOUND);
               _loc4_.moveEnable = false;
               _loc4_.addEventListener(FrameEvent.RESPONSE,this.useSkillReConfirm,false,0,true);
               return;
            }
            if(!_loc2_.isBand && PlayerManager.Instance.Self.Money < _loc3_)
            {
               LeavePageManager.showFillFrame();
               return;
            }
            if((_loc2_ as SevenDoubleBuyConfirmView).isNoPrompt)
            {
               _loc5_ = SevenDoubleManager.instance.getBuyRecordStatus(this._recordClickTag + 2);
               _loc5_.isNoPrompt = true;
               _loc5_.isBand = _loc2_.isBand;
            }
            SocketManager.Instance.out.sendSevenDoubleUseSkill(this._recordClickTag,_loc2_.isBand,this._freeTipList[this._recordClickTag].visible);
            this.unEnableBtn(this._recordClickTag);
         }
      }
      
      private function useSkillReConfirm(param1:FrameEvent) : void
      {
         var _loc3_:int = 0;
         SoundManager.instance.play("008");
         var _loc2_:BaseAlerFrame = param1.currentTarget as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.useSkillConfirm);
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            _loc3_ = SevenDoubleManager.instance.dataInfo.useSkillNeedMoney[this._recordClickTag];
            if(PlayerManager.Instance.Self.Money < _loc3_)
            {
               LeavePageManager.showFillFrame();
               return;
            }
            SocketManager.Instance.out.sendSevenDoubleUseSkill(this._recordClickTag,false,this._freeTipList[this._recordClickTag].visible);
            this.unEnableBtn(this._recordClickTag);
         }
      }
      
      private function removeEvent() : void
      {
         this._leapBtn.removeEventListener(MouseEvent.CLICK,this.clickHandler);
         this._leapBtn.removeEventListener(MouseEvent.MOUSE_OVER,this.overHandler);
         this._leapBtn.removeEventListener(MouseEvent.MOUSE_OUT,this.outHandler);
         this._invisibilityBtn.removeEventListener(MouseEvent.CLICK,this.clickHandler);
         this._cleanBtn.removeEventListener(MouseEvent.CLICK,this.clickHandler);
         SevenDoubleManager.instance.removeEventListener(SevenDoubleManager.REFRESH_ITEM_FREE_COUNT,this.refreshFreeCount);
      }
      
      public function dispose() : void
      {
         this.removeEvent();
         ObjectUtils.disposeAllChildren(this);
         this._bg = null;
         this._leapBtn = null;
         this._invisibilityBtn = null;
         this._cleanBtn = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
