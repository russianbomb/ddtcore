package sevenDouble.player
{
   import com.pickgliss.loader.BaseLoader;
   import com.pickgliss.loader.LoadResourceManager;
   import com.pickgliss.loader.LoaderEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.LanguageMgr;
   import ddt.manager.SocketManager;
   import ddt.manager.TimeManager;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.TimerEvent;
   import flash.utils.Timer;
   import road7th.data.DictionaryData;
   import sevenDouble.SevenDoubleManager;
   import sevenDouble.data.SevenDoubleCarInfo;
   import sevenDouble.data.SevenDoublePlayerInfo;
   import sevenDouble.event.SevenDoubleEvent;
   import sevenDouble.view.SevenDoubleBuffCountDownView;
   
   public class SevenDoubleGamePlayer extends Sprite implements Disposeable
   {
       
      
      private var _playerInfo:SevenDoublePlayerInfo;
      
      private var _playerMc:MovieClip;
      
      private var _destinationX:int;
      
      private var _carInfo:SevenDoubleCarInfo;
      
      private var _moveTimer:Timer;
      
      private var _nameTxt:FilterFrameText;
      
      private var _buffCountDownList:DictionaryData;
      
      private var _isDispose:Boolean = false;
      
      private var _fightMc:MovieClip;
      
      private var _leapArrow:Bitmap;
      
      public function SevenDoubleGamePlayer(param1:SevenDoublePlayerInfo)
      {
         var _loc2_:Bitmap = null;
         this._buffCountDownList = new DictionaryData();
         super();
         this._playerInfo = param1;
         this._carInfo = SevenDoubleManager.instance.dataInfo.carInfo[this._playerInfo.carType];
         this.x = 280 + this._playerInfo.posX;
         this.y = 150 + 65 * param1.index;
         this._playerMc = new MovieClip();
         _loc2_ = ComponentFactory.Instance.creatBitmap("game.player.defaultPlayerCharacter");
         _loc2_.x = -50;
         _loc2_.y = -100;
         this._playerMc.addChild(_loc2_);
         addChild(this._playerMc);
         this.loadRes();
         this._nameTxt = ComponentFactory.Instance.creatComponentByStylename("sevenDouble.game.playerNameTxt");
         this._nameTxt.text = LanguageMgr.GetTranslation("sevenDouble.game.playerNameTxt" + this._carInfo.type,this._playerInfo.name);
         if(this._carInfo.type == 1)
         {
            this._nameTxt.textColor = 710173;
         }
         else if(this._carInfo.type == 2)
         {
            this._nameTxt.textColor = 16711680;
         }
         addChild(this._nameTxt);
         this._fightMc = ComponentFactory.Instance.creat("asset.sevenDouble.playerFighting");
         this._fightMc.gotoAndStop(2);
         PositionUtils.setPos(this._fightMc,"sevenDouble.gamePlayer.fightMcPos");
         addChild(this._fightMc);
         this.refreshFightMc();
         this._leapArrow = ComponentFactory.Instance.creatBitmap("asset.sevenDouble.leapPromptArrow");
         addChild(this._leapArrow);
         this._leapArrow.visible = false;
         if(this._playerInfo.isSelf)
         {
            this._moveTimer = new Timer(1000);
            this._moveTimer.addEventListener(TimerEvent.TIMER,this.moveTimerHandler,false,0,true);
            SevenDoubleManager.instance.addEventListener(SevenDoubleManager.LEAP_PROMPT_SHOW_HIDE,this.showOrHideLeapArrow);
         }
      }
      
      public function get playerInfo() : SevenDoublePlayerInfo
      {
         return this._playerInfo;
      }
      
      public function set destinationX(param1:Number) : void
      {
         this._destinationX = param1 + 280;
         var _loc2_:Number = this._carInfo.speed;
         if(this._playerInfo.acceleEndTime.getTime() > TimeManager.Instance.Now().getTime())
         {
            _loc2_ = _loc2_ * SevenDoubleManager.instance.accelerateRate / 100;
         }
         if(this._playerInfo.deceleEndTime.getTime() > TimeManager.Instance.Now().getTime())
         {
            _loc2_ = _loc2_ * SevenDoubleManager.instance.decelerateRate / 100;
         }
         if(this._destinationX - x > _loc2_ * 30)
         {
            x = x + _loc2_ * 25;
         }
      }
      
      private function loadRes() : void
      {
         var _loc1_:BaseLoader = LoadResourceManager.Instance.createLoader(SevenDoubleManager.instance.getPlayerResUrl(this._playerInfo.isSelf,this._playerInfo.carType),BaseLoader.MODULE_LOADER);
         _loc1_.addEventListener(LoaderEvent.COMPLETE,this.onLoadComplete);
         LoadResourceManager.Instance.startLoad(_loc1_);
      }
      
      private function onLoadComplete(param1:LoaderEvent) : void
      {
         var _loc2_:String = null;
         param1.loader.removeEventListener(LoaderEvent.COMPLETE,this.onLoadComplete);
         if(this._isDispose)
         {
            return;
         }
         if(this._playerMc && this._playerMc.parent)
         {
            this._playerMc.parent.removeChild(this._playerMc);
         }
         if(this._playerInfo.isSelf)
         {
            _loc2_ = "self";
         }
         else
         {
            _loc2_ = "other";
         }
         this._playerMc = ComponentFactory.Instance.creat("asset.sevenDouble." + _loc2_ + this._playerInfo.carType);
         this._playerMc.gotoAndPlay("stand");
         addChildAt(this._playerMc,0);
         this.refreshBuffCountDown();
      }
      
      private function moveTimerHandler(param1:TimerEvent) : void
      {
         SocketManager.Instance.out.sendSevenDoubleMove();
      }
      
      private function showOrHideLeapArrow(param1:SevenDoubleEvent) : void
      {
         this._leapArrow.visible = param1.data.isShow;
      }
      
      public function refreshBuffCountDown() : void
      {
         var _loc1_:SevenDoubleBuffCountDownView = null;
         var _loc3_:SevenDoubleBuffCountDownView = null;
         var _loc4_:SevenDoubleBuffCountDownView = null;
         var _loc5_:SevenDoubleBuffCountDownView = null;
         var _loc6_:SevenDoubleBuffCountDownView = null;
         var _loc2_:Boolean = false;
         if(this._playerInfo.deceleEndTime.getTime() > TimeManager.Instance.Now().getTime())
         {
            this._playerMc.gotoAndPlay("moderate");
            _loc2_ = true;
            if(this._buffCountDownList.hasKey("2"))
            {
               (this._buffCountDownList["2"] as SevenDoubleBuffCountDownView).endTime = this._playerInfo.deceleEndTime;
            }
            else
            {
               _loc1_ = new SevenDoubleBuffCountDownView(this._playerInfo.deceleEndTime,2,this._buffCountDownList.length);
               _loc1_.addEventListener(SevenDoubleBuffCountDownView.END,this.buffCountDownEnd,false,0,true);
               addChild(_loc1_);
               this._buffCountDownList.add("2",_loc1_);
            }
         }
         else if(this._buffCountDownList.hasKey("2"))
         {
            _loc3_ = this._buffCountDownList["2"] as SevenDoubleBuffCountDownView;
            _loc3_.removeEventListener(SevenDoubleBuffCountDownView.END,this.buffCountDownEnd);
            ObjectUtils.disposeObject(_loc3_);
            this._buffCountDownList.remove(_loc3_.type);
         }
         if(this._playerInfo.acceleEndTime.getTime() > TimeManager.Instance.Now().getTime())
         {
            this._playerMc.gotoAndPlay("accelerate");
            _loc2_ = true;
            if(this._buffCountDownList.hasKey("1"))
            {
               (this._buffCountDownList["1"] as SevenDoubleBuffCountDownView).endTime = this._playerInfo.acceleEndTime;
            }
            else
            {
               _loc1_ = new SevenDoubleBuffCountDownView(this._playerInfo.acceleEndTime,1,this._buffCountDownList.length);
               _loc1_.addEventListener(SevenDoubleBuffCountDownView.END,this.buffCountDownEnd,false,0,true);
               addChild(_loc1_);
               this._buffCountDownList.add("1",_loc1_);
            }
         }
         else if(this._buffCountDownList.hasKey("1"))
         {
            _loc4_ = this._buffCountDownList["1"] as SevenDoubleBuffCountDownView;
            _loc4_.removeEventListener(SevenDoubleBuffCountDownView.END,this.buffCountDownEnd);
            ObjectUtils.disposeObject(_loc4_);
            this._buffCountDownList.remove(_loc4_.type);
         }
         if(this._playerInfo.invisiEndTime.getTime() - TimeManager.Instance.Now().getTime() > 0)
         {
            this._playerMc.gotoAndPlay("transparent");
            _loc2_ = true;
            if(this._buffCountDownList.hasKey("3"))
            {
               (this._buffCountDownList["3"] as SevenDoubleBuffCountDownView).endTime = this._playerInfo.invisiEndTime;
            }
            else
            {
               _loc1_ = new SevenDoubleBuffCountDownView(this._playerInfo.invisiEndTime,3,this._buffCountDownList.length);
               _loc1_.addEventListener(SevenDoubleBuffCountDownView.END,this.buffCountDownEnd,false,0,true);
               addChild(_loc1_);
               this._buffCountDownList.add("3",_loc1_);
            }
         }
         else if(this._buffCountDownList.hasKey("3"))
         {
            _loc5_ = this._buffCountDownList["3"] as SevenDoubleBuffCountDownView;
            _loc5_.removeEventListener(SevenDoubleBuffCountDownView.END,this.buffCountDownEnd);
            ObjectUtils.disposeObject(_loc5_);
            this._buffCountDownList.remove(_loc5_.type);
         }
         if(!_loc2_)
         {
            this._playerMc.gotoAndPlay("stand");
         }
         if(!this.playerInfo.isSelf)
         {
            for each(_loc6_ in this._buffCountDownList)
            {
               _loc6_.visible = false;
            }
         }
      }
      
      private function buffCountDownEnd(param1:Event) : void
      {
         var _loc2_:SevenDoubleBuffCountDownView = param1.target as SevenDoubleBuffCountDownView;
         _loc2_.removeEventListener(SevenDoubleBuffCountDownView.END,this.buffCountDownEnd);
         ObjectUtils.disposeObject(_loc2_);
         this._buffCountDownList.remove(_loc2_.type);
         this.refreshBuffCountDown();
      }
      
      public function updatePlayer() : void
      {
         var _loc1_:Number = this._carInfo.speed;
         if(this._playerInfo.acceleEndTime.getTime() > TimeManager.Instance.Now().getTime())
         {
            _loc1_ = _loc1_ * SevenDoubleManager.instance.accelerateRate / 100;
         }
         if(this._playerInfo.deceleEndTime.getTime() > TimeManager.Instance.Now().getTime())
         {
            _loc1_ = _loc1_ * SevenDoubleManager.instance.decelerateRate / 100;
         }
         if(x < this._destinationX)
         {
            x = x + _loc1_;
         }
      }
      
      public function refreshFightMc() : void
      {
         if(this._playerInfo.fightState == 1)
         {
            this._fightMc.gotoAndStop(1);
            if(this._moveTimer && this._moveTimer.running)
            {
               this._moveTimer.stop();
            }
         }
         else
         {
            this._fightMc.gotoAndStop(2);
            if(this._moveTimer && !this._moveTimer.running)
            {
               this._moveTimer.start();
            }
         }
      }
      
      public function startGame() : void
      {
         if(this._playerInfo.isSelf)
         {
            this._moveTimer.start();
            this.moveTimerHandler(null);
         }
      }
      
      public function endGame() : void
      {
         if(this._moveTimer && this._moveTimer.running)
         {
            this._moveTimer.stop();
         }
      }
      
      public function dispose() : void
      {
         SevenDoubleManager.instance.removeEventListener(SevenDoubleManager.LEAP_PROMPT_SHOW_HIDE,this.showOrHideLeapArrow);
         if(this._playerMc)
         {
            this._playerMc.gotoAndStop(this._playerMc.totalFrames);
         }
         if(this._fightMc)
         {
            this._fightMc.gotoAndStop(2);
         }
         if(this._moveTimer)
         {
            this._moveTimer.removeEventListener(TimerEvent.TIMER,this.moveTimerHandler);
            this._moveTimer.stop();
         }
         this._moveTimer = null;
         this._carInfo = null;
         this._playerInfo = null;
         ObjectUtils.disposeAllChildren(this);
         this._playerMc = null;
         this._nameTxt = null;
         this._buffCountDownList = null;
         this._fightMc = null;
         this._isDispose = true;
      }
   }
}
