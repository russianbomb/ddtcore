package worldBossHelper
{
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.ServerConfigInfo;
   import ddt.data.UIModuleTypes;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.InviteManager;
   import ddt.manager.LanguageMgr;
   import ddt.manager.ServerConfigManager;
   import ddt.manager.SocketManager;
   import ddt.view.UIModuleSmallLoading;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import worldBossHelper.data.WorldBossHelperTypeData;
   import worldBossHelper.event.WorldBossHelperEvent;
   import worldBossHelper.view.WorldBossHelperFrame;
   
   public class WorldBossHelperManager extends EventDispatcher
   {
      
      private static var _instance:WorldBossHelperManager;
       
      
      private var _frame:WorldBossHelperFrame;
      
      public var data:WorldBossHelperTypeData;
      
      public var receieveData:Boolean;
      
      public var helperOpen:Boolean;
      
      public var isInWorldBossHelperFrame:Boolean;
      
      public var isHelperOnlyOnce:Boolean;
      
      public var price:int;
      
      public var isHelperInited:Boolean;
      
      private var _isFightOver:Boolean;
      
      public var isFighting:Boolean;
      
      private var _honor:int;
      
      public var allHonor:int;
      
      private var _money:int;
      
      public var allMoney:int;
      
      public var num:int;
      
      public var WorldBossAssistantTimeInfo1:ServerConfigInfo;
      
      public var WorldBossAssistantTimeInfo2:ServerConfigInfo;
      
      public var WorldBossAssistantTimeInfo3:ServerConfigInfo;
      
      public function WorldBossHelperManager()
      {
         super();
      }
      
      public static function get Instance() : WorldBossHelperManager
      {
         if(_instance == null)
         {
            _instance = new WorldBossHelperManager();
         }
         return _instance;
      }
      
      public function setup() : void
      {
         this.initData();
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.WORLDBOSS_PLAYERFIGHTASSIATANT,this.__playerInfoHandler);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.WORLDBOSS_ASSISTANT,this.__assistantHandler);
         addEventListener(WorldBossHelperEvent.BOSS_OPEN,this.__bossOpenHandler);
         UIModuleSmallLoading.Instance.progress = 0;
         UIModuleSmallLoading.Instance.show();
         UIModuleSmallLoading.Instance.addEventListener(Event.CLOSE,this._loadingCloseHandler);
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this._loaderCompleteHandler);
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this._loaderProgressHandler);
         UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.WORLDBOSS_HELPER);
      }
      
      private function initData() : void
      {
         this.isHelperInited = true;
         InviteManager.Instance.enabled = false;
         this.num = 1;
         this.isFighting = false;
         this.WorldBossAssistantTimeInfo1 = ServerConfigManager.instance.findInfoByName("WorldBossAssistantNormalTime");
         this.WorldBossAssistantTimeInfo2 = ServerConfigManager.instance.findInfoByName("WorldBossAssistantReviveTime");
         this.WorldBossAssistantTimeInfo3 = ServerConfigManager.instance.findInfoByName("WorldBossAssistantFastTime");
      }
      
      protected function __bossOpenHandler(param1:WorldBossHelperEvent) : void
      {
         if(this._frame)
         {
            this._frame.startFight();
         }
      }
      
      protected function __playerInfoHandler(param1:CrazyTankSocketEvent) : void
      {
         this.isFighting = param1.pkg.readBoolean();
         var _loc2_:int = param1.pkg.readInt();
         var _loc3_:Array = new Array();
         var _loc4_:int = 0;
         while(_loc4_ < _loc2_)
         {
            _loc3_.push(param1.pkg.readInt());
            _loc4_++;
         }
         this._honor = param1.pkg.readInt();
         this.allHonor = this.allHonor + this._honor;
         this._money = param1.pkg.readInt();
         this.allMoney = this.allMoney + this._money;
         if(this._frame)
         {
            this._frame.addPlayerInfo(true,this.num,_loc3_,this._honor);
         }
         this.num++;
      }
      
      protected function __assistantHandler(param1:CrazyTankSocketEvent) : void
      {
         this.data.isOpen = param1.pkg.readBoolean();
         this.helperOpen = this.data.isOpen;
         this.data.buffNum = param1.pkg.readInt();
         this.data.type = param1.pkg.readInt();
         this.data.openType = param1.pkg.readInt();
         if(this.data.openType == 0)
         {
            this.data.openType = 1;
         }
         if(this._frame)
         {
            this._frame.updateView();
            return;
         }
         this._frame = ComponentFactory.Instance.creatComponentByStylename("worldBossHelperFrame");
         this._frame.titleText = LanguageMgr.GetTranslation("worldbosshelper.title");
         LayerManager.Instance.addToLayer(this._frame,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
      }
      
      protected function _loadingCloseHandler(param1:Event) : void
      {
         this.closeLoading();
         UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this._loadingCloseHandler);
      }
      
      private function closeLoading() : void
      {
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this._loaderCompleteHandler);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this._loaderProgressHandler);
         UIModuleSmallLoading.Instance.hide();
      }
      
      protected function _loaderCompleteHandler(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.WORLDBOSS_HELPER)
         {
            this.closeLoading();
            this.data = new WorldBossHelperTypeData();
            this.data.requestType = 1;
            SocketManager.Instance.out.openOrCloseWorldBossHelper(this.data);
         }
      }
      
      public function dispose() : void
      {
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.WORLDBOSS_ASSISTANT,this.__assistantHandler);
         this.data = null;
         if(this._frame)
         {
            this.allHonor = this.allMoney = 0;
            this.isHelperInited = false;
            ObjectUtils.disposeObject(this._frame);
            this._frame = null;
            this.isInWorldBossHelperFrame = false;
            SocketManager.Instance.out.quitWorldBossHelperView();
         }
         InviteManager.Instance.enabled = true;
      }
      
      protected function _loaderProgressHandler(param1:UIModuleEvent) : void
      {
         UIModuleSmallLoading.Instance.progress = param1.loader.progress * 100;
      }
   }
}
