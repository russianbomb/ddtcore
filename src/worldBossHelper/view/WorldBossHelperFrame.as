package worldBossHelper.view
{
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.LanguageMgr;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import labyrinth.LabyrinthManager;
   import store.HelpFrame;
   import worldBossHelper.WorldBossHelperManager;
   import worldBossHelper.data.WorldBossHelperTypeData;
   import worldBossHelper.event.WorldBossHelperEvent;
   
   public class WorldBossHelperFrame extends Frame
   {
       
      
      private var _leftView:WorldBossHelperLeftView;
      
      private var _rightView:WorldBossHelperRightView;
      
      private var _chatBtn:SimpleBitmapButton;
      
      private var _helpBtn:SimpleBitmapButton;
      
      private var _data:WorldBossHelperTypeData;
      
      private var _helperState:Boolean;
      
      public function WorldBossHelperFrame()
      {
         super();
         this.initView();
         this.initEvent();
      }
      
      public function addPlayerInfo(param1:Boolean, param2:int, param3:Array, param4:int) : void
      {
         this._leftView.addDescription(param1,param2,param3,param4);
      }
      
      public function updateView() : void
      {
         this._leftView.changeState();
         this._rightView.setState();
      }
      
      private function initView() : void
      {
         this._leftView = new WorldBossHelperLeftView();
         this._leftView.buyAddMoney = this.getBuyAddMoney;
         addToContent(this._leftView);
         this._rightView = new WorldBossHelperRightView();
         addToContent(this._rightView);
         this.updateView();
         this._chatBtn = ComponentFactory.Instance.creatComponentByStylename("worldbosshelper.chatButton");
         this._chatBtn.tipData = LanguageMgr.GetTranslation("tank.game.ToolStripView.chat");
         addToContent(this._chatBtn);
         this._helpBtn = ComponentFactory.Instance.creatComponentByStylename("worldbosshelper.helpBtn");
         addToContent(this._helpBtn);
         WorldBossHelperManager.Instance.isInWorldBossHelperFrame = true;
      }
      
      private function initEvent() : void
      {
         this._chatBtn.addEventListener(MouseEvent.CLICK,this.__chatClick);
         this._helpBtn.addEventListener(MouseEvent.CLICK,this.__helpClick);
         addEventListener(FrameEvent.RESPONSE,this.__responseHandler);
         this._leftView.addEventListener(WorldBossHelperEvent.CHANGE_HELPER_STATE,this.__changeStateHandler);
      }
      
      private function __helpClick(param1:MouseEvent) : void
      {
         SoundManager.instance.playButtonSound();
         var _loc2_:MovieClip = ComponentFactory.Instance.creatCustomObject("worldbosshelper.helpBg");
         var _loc3_:HelpFrame = ComponentFactory.Instance.creat("worldBossHelper.HelpFrame");
         _loc3_.setView(_loc2_);
         _loc3_.titleText = LanguageMgr.GetTranslation("tank.view.emailII.ReadingView.useHelp");
         _loc3_.addEventListener(FrameEvent.RESPONSE,this.__frameEvent);
         LayerManager.Instance.addToLayer(_loc3_,LayerManager.GAME_TOP_LAYER,true,LayerManager.BLCAK_BLOCKGOUND,true);
      }
      
      protected function __frameEvent(param1:FrameEvent) : void
      {
         SoundManager.instance.playButtonSound();
         var _loc2_:Disposeable = param1.target as Disposeable;
         _loc2_.dispose();
         _loc2_ = null;
      }
      
      private function __chatClick(param1:MouseEvent) : void
      {
         SoundManager.instance.playButtonSound();
         LabyrinthManager.Instance.chat();
      }
      
      public function startFight() : void
      {
         this._leftView.startFight();
      }
      
      protected function __changeStateHandler(param1:WorldBossHelperEvent) : void
      {
         this._data = this._rightView.typeData;
         this._data.requestType = 2;
         this._data.isOpen = param1.state;
         SocketManager.Instance.out.openOrCloseWorldBossHelper(this._data);
      }
      
      public function getBuyAddMoney() : int
      {
         var _loc1_:WorldBossHelperTypeData = this._rightView.typeData;
         if(_loc1_.type == 1)
         {
            return 10;
         }
         if(_loc1_.type == 2)
         {
            return 12;
         }
         return 0;
      }
      
      private function __responseHandler(param1:FrameEvent) : void
      {
         var _loc2_:BaseAlerFrame = null;
         if(param1.responseCode == FrameEvent.CLOSE_CLICK || param1.responseCode == FrameEvent.ESC_CLICK)
         {
            SoundManager.instance.play("008");
            if(!WorldBossHelperManager.Instance.isFighting)
            {
               WorldBossHelperManager.Instance.dispose();
            }
            else
            {
               _loc2_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("tips"),LanguageMgr.GetTranslation("worldboss.helper.closehelper"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),false,true,false,LayerManager.ALPHA_BLOCKGOUND);
               _loc2_.addEventListener(FrameEvent.RESPONSE,this.__alertCloseHelper);
            }
         }
      }
      
      private function __alertCloseHelper(param1:FrameEvent) : void
      {
         switch(param1.responseCode)
         {
            case FrameEvent.SUBMIT_CLICK:
            case FrameEvent.ENTER_CLICK:
               WorldBossHelperManager.Instance.helperOpen = false;
               WorldBossHelperManager.Instance.dispose();
         }
         param1.currentTarget.removeEventListener(FrameEvent.RESPONSE,this.__alertCloseHelper);
         ObjectUtils.disposeObject(param1.currentTarget);
      }
      
      private function removeEvent() : void
      {
         this._chatBtn.removeEventListener(MouseEvent.CLICK,this.__chatClick);
         removeEventListener(FrameEvent.RESPONSE,this.__responseHandler);
         this._leftView.removeEventListener(WorldBossHelperEvent.CHANGE_HELPER_STATE,this.__changeStateHandler);
      }
      
      override public function dispose() : void
      {
         this.removeEvent();
         super.dispose();
         this._leftView = null;
         this._rightView = null;
         this._chatBtn = null;
      }
   }
}
