package worldBossHelper.view
{
   import baglocked.BaglockedManager;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.ScrollPanel;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.controls.container.VBox;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.ServerConfigInfo;
   import ddt.manager.LanguageMgr;
   import ddt.manager.LeavePageManager;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerConfigManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.manager.TimeManager;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import flash.events.TimerEvent;
   import flash.utils.Timer;
   import worldBossHelper.WorldBossHelperManager;
   import worldBossHelper.event.WorldBossHelperEvent;
   import worldboss.WorldBossManager;
   
   public class WorldBossHelperLeftView extends Sprite implements Disposeable
   {
       
      
      private var _infoBg:Bitmap;
      
      private var _scrollPanel:ScrollPanel;
      
      private var _vBox:VBox;
      
      private var _state:Boolean;
      
      private var _openStateTxt:FilterFrameText;
      
      private var _closeStateTxt:FilterFrameText;
      
      private var _openBtn:SimpleBitmapButton;
      
      private var _closeBtn:SimpleBitmapButton;
      
      private var _date1:Date;
      
      private var _date2:Date;
      
      private var _timer:Timer;
      
      private var _remainTime:int;
      
      private var _count:int;
      
      private var _countArr:Array;
      
      private var _titleTxtArr:Array;
      
      private var _allHonorTxt:FilterFrameText;
      
      private var _allMoneyTxt:FilterFrameText;
      
      private var _startTimer:Timer;
      
      private var _buyAddMoneyFun:Function;
      
      public function WorldBossHelperLeftView()
      {
         super();
         this._countArr = ["1","2","3"];
         this._titleTxtArr = new Array();
         this.initView();
         this.initEvent();
      }
      
      private function initEvent() : void
      {
         this._openBtn.addEventListener(MouseEvent.CLICK,this.__btnHandler);
         this._closeBtn.addEventListener(MouseEvent.CLICK,this.__btnHandler);
      }
      
      protected function __btnHandler(param1:MouseEvent) : void
      {
         var _loc2_:BaseAlerFrame = null;
         SoundManager.instance.play("008");
         if(param1.target == this._openBtn)
         {
            if(PlayerManager.Instance.Self.bagLocked)
            {
               BaglockedManager.Instance.show();
               return;
            }
         }
         if(!this._date1)
         {
            this._date1 = TimeManager.Instance.Now();
         }
         else
         {
            this._date2 = TimeManager.Instance.Now();
            if(this._date2.time - this._date1.time < 10 * 1000)
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("worldboss.helper.click"));
               return;
            }
            this._date1 = this._date2;
         }
         if(param1.target == this._openBtn)
         {
            _loc2_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("tips"),LanguageMgr.GetTranslation("worldboss.helper.isOpen"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),false,true,false,LayerManager.ALPHA_BLOCKGOUND);
            _loc2_.addEventListener(FrameEvent.RESPONSE,this.__alertOpenHelper);
         }
         else
         {
            if(!this._openBtn.visible)
            {
               this.fightFailDescription();
            }
            this.dispatchHelperEvent();
         }
      }
      
      public function dispatchHelperEvent() : void
      {
         var _loc1_:WorldBossHelperEvent = new WorldBossHelperEvent(WorldBossHelperEvent.CHANGE_HELPER_STATE);
         _loc1_.state = this._openBtn.visible;
         dispatchEvent(_loc1_);
      }
      
      protected function __alertOpenHelper(param1:FrameEvent) : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:ServerConfigInfo = null;
         var _loc5_:BaseAlerFrame = null;
         var _loc6_:int = 0;
         switch(param1.responseCode)
         {
            case FrameEvent.SUBMIT_CLICK:
            case FrameEvent.ENTER_CLICK:
               if(WorldBossManager.Instance.bossInfo)
               {
                  _loc2_ = WorldBossHelperManager.Instance.data.buffNum - WorldBossManager.Instance.bossInfo.myPlayerVO.buffLevel;
                  if(_loc2_ <= 0)
                  {
                     _loc2_ = 0;
                  }
               }
               else
               {
                  _loc2_ = WorldBossHelperManager.Instance.data.buffNum;
               }
               _loc4_ = ServerConfigManager.instance.findInfoByName("WorldBossAssistantFightMoney");
               if(_loc4_ && _loc4_.Value)
               {
                  _loc3_ = int(_loc4_.Value);
               }
               else
               {
                  _loc3_ = 10;
               }
               if(PlayerManager.Instance.Self.BandMoney < _loc3_)
               {
                  _loc5_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("tank.room.RoomIIView2.notenoughmoney.title"),LanguageMgr.GetTranslation("bindMoneyPoorNote"),LanguageMgr.GetTranslation("ok"),"",false,true,true,LayerManager.ALPHA_BLOCKGOUND);
                  _loc5_.addEventListener(FrameEvent.RESPONSE,this._response2);
                  param1.currentTarget.removeEventListener(FrameEvent.RESPONSE,this.__alertOpenHelper);
                  ObjectUtils.disposeObject(param1.currentTarget);
                  return;
               }
               _loc6_ = this._buyAddMoneyFun != null?int(this._buyAddMoneyFun()):int(0);
               if(_loc6_ > 0 && PlayerManager.Instance.Self.Money < _loc6_ + _loc2_ * 30)
               {
                  _loc5_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("tank.room.RoomIIView2.notenoughmoney.title"),LanguageMgr.GetTranslation("tank.room.RoomIIView2.notenoughmoney.content"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),false,true,true,LayerManager.ALPHA_BLOCKGOUND);
                  _loc5_.addEventListener(FrameEvent.RESPONSE,this._response);
                  param1.currentTarget.removeEventListener(FrameEvent.RESPONSE,this.__alertOpenHelper);
                  ObjectUtils.disposeObject(param1.currentTarget);
                  return;
               }
               this.dispatchHelperEvent();
               break;
         }
         param1.currentTarget.removeEventListener(FrameEvent.RESPONSE,this.__alertOpenHelper);
         ObjectUtils.disposeObject(param1.currentTarget);
      }
      
      public function set buyAddMoney(param1:Function) : void
      {
         this._buyAddMoneyFun = param1;
      }
      
      private function _response(param1:FrameEvent) : void
      {
         (param1.currentTarget as BaseAlerFrame).removeEventListener(FrameEvent.RESPONSE,this._response);
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            LeavePageManager.leaveToFillPath();
         }
         param1.currentTarget.removeEventListener(FrameEvent.RESPONSE,this._response);
         ObjectUtils.disposeObject(param1.currentTarget);
      }
      
      private function _response2(param1:FrameEvent) : void
      {
         param1.currentTarget.removeEventListener(FrameEvent.RESPONSE,this._response2);
         ObjectUtils.disposeObject(param1.currentTarget);
      }
      
      private function fightFailDescription() : void
      {
         var _loc1_:FilterFrameText = null;
         this.stopAndDisposeTimer();
         if(this._vBox.numChildren > 0)
         {
            _loc1_ = this._vBox.getChildAt(this._vBox.numChildren - 1) as FilterFrameText;
            _loc1_.text = LanguageMgr.GetTranslation("worldboss.helper.fightFail",WorldBossHelperManager.Instance.num);
         }
      }
      
      private function stopAndDisposeTimer() : void
      {
         if(this._timer)
         {
            this._timer.stop();
            this._timer.removeEventListener(TimerEvent.TIMER,this.__timerHandler);
            this._timer = null;
         }
      }
      
      public function get state() : Boolean
      {
         return this._state;
      }
      
      public function set state(param1:Boolean) : void
      {
         this._state = param1;
      }
      
      private function initView() : void
      {
         this._infoBg = ComponentFactory.Instance.creat("worldBossHelper.info");
         addChild(this._infoBg);
         this._allHonorTxt = ComponentFactory.Instance.creatComponentByStylename("worldBossHelper.view.allHonorAndMoneyText");
         addChild(this._allHonorTxt);
         this._allHonorTxt.text = "0";
         PositionUtils.setPos(this._allHonorTxt,"worldBossHelper.view.allHonorPos");
         this._allMoneyTxt = ComponentFactory.Instance.creatComponentByStylename("worldBossHelper.view.allHonorAndMoneyText");
         addChild(this._allMoneyTxt);
         this._allMoneyTxt.text = "0";
         PositionUtils.setPos(this._allMoneyTxt,"worldBossHelper.view.allMoneyPos");
         this._vBox = ComponentFactory.Instance.creatComponentByStylename("worldBossHelper.Vbox");
         this._scrollPanel = ComponentFactory.Instance.creatComponentByStylename("worldBossHelper.scrollPanel");
         this._scrollPanel.setView(this._vBox);
         addChild(this._scrollPanel);
         this._openStateTxt = ComponentFactory.Instance.creatComponentByStylename("worldBossHelper.view.helperStateText");
         this._openStateTxt.text = LanguageMgr.GetTranslation("worldbosshelper.open");
         addChild(this._openStateTxt);
         this._closeStateTxt = ComponentFactory.Instance.creatComponentByStylename("worldBossHelper.view.helperStateText");
         this._closeStateTxt.text = LanguageMgr.GetTranslation("worldbosshelper.close");
         addChild(this._closeStateTxt);
         this._openBtn = ComponentFactory.Instance.creatComponentByStylename("worldBossHelper.view.openBtn");
         this._openBtn.tipData = LanguageMgr.GetTranslation("worldboss.helper.openTip");
         addChild(this._openBtn);
         this._closeBtn = ComponentFactory.Instance.creatComponentByStylename("worldBossHelper.view.closeBtn");
         addChild(this._closeBtn);
      }
      
      public function changeState() : void
      {
         this._state = WorldBossHelperManager.Instance.data.isOpen;
         if(this._state)
         {
            this._closeStateTxt.visible = this._openBtn.visible = false;
            this._openStateTxt.visible = this._closeBtn.visible = true;
            if(WorldBossManager.Instance.bossInfo && !WorldBossManager.Instance.bossInfo.fightOver)
            {
               this.startFight();
            }
         }
         else
         {
            WorldBossHelperManager.Instance.isFighting = false;
            this._closeStateTxt.visible = this._openBtn.visible = true;
            this._openStateTxt.visible = this._closeBtn.visible = false;
         }
      }
      
      private function checkTrueStart() : Boolean
      {
         if(!WorldBossHelperManager.Instance.data.isOpen)
         {
            return false;
         }
         if(WorldBossManager.Instance.bossInfo && !WorldBossManager.Instance.bossInfo.fightOver && WorldBossManager.Instance.beforeStartTime <= 0)
         {
            return true;
         }
         if(!this._startTimer)
         {
            this._startTimer = new Timer(1000);
            this._startTimer.addEventListener(TimerEvent.TIMER,this.startTimerHandler,false,0,true);
         }
         this._startTimer.start();
         return false;
      }
      
      private function startTimerHandler(param1:TimerEvent) : void
      {
         if(WorldBossManager.Instance.bossInfo && !WorldBossManager.Instance.bossInfo.fightOver && WorldBossManager.Instance.beforeStartTime <= 0)
         {
            this.startFight();
            this.disposeStartTimer();
         }
      }
      
      private function disposeStartTimer() : void
      {
         if(this._startTimer)
         {
            this._startTimer.removeEventListener(TimerEvent.TIMER,this.startTimerHandler);
            this._startTimer.stop();
            this._startTimer = null;
         }
      }
      
      public function startFight() : void
      {
         if(!this.checkTrueStart())
         {
            return;
         }
         WorldBossHelperManager.Instance.isFighting = true;
         this.addDescription(false,WorldBossHelperManager.Instance.num,null,0);
         if(WorldBossHelperManager.Instance.data.type == 0)
         {
            if(WorldBossHelperManager.Instance.WorldBossAssistantTimeInfo1)
            {
               this._remainTime = int(WorldBossHelperManager.Instance.WorldBossAssistantTimeInfo1.Value);
            }
            else
            {
               this._remainTime = 90;
            }
         }
         else if(WorldBossHelperManager.Instance.data.type == 1)
         {
            if(WorldBossHelperManager.Instance.WorldBossAssistantTimeInfo2)
            {
               this._remainTime = int(WorldBossHelperManager.Instance.WorldBossAssistantTimeInfo2.Value);
            }
            else
            {
               this._remainTime = 70;
            }
         }
         else if(WorldBossHelperManager.Instance.data.type == 2)
         {
            if(WorldBossHelperManager.Instance.WorldBossAssistantTimeInfo3)
            {
               this._remainTime = int(WorldBossHelperManager.Instance.WorldBossAssistantTimeInfo3.Value);
            }
            else
            {
               this._remainTime = 65;
            }
         }
         this._timer = new Timer(1000);
         this._timer.addEventListener(TimerEvent.TIMER,this.__timerHandler);
         this._timer.start();
      }
      
      protected function __timerHandler(param1:TimerEvent) : void
      {
         if(this._count != this._remainTime)
         {
            this._count++;
         }
         if(this._count == this._remainTime)
         {
            if(!WorldBossHelperManager.Instance.receieveData && WorldBossHelperManager.Instance.data)
            {
               WorldBossHelperManager.Instance.data.requestType = 3;
               SocketManager.Instance.out.openOrCloseWorldBossHelper(WorldBossHelperManager.Instance.data);
            }
            else
            {
               WorldBossHelperManager.Instance.receieveData = false;
               this._count = 0;
               this.addDescription(false,WorldBossHelperManager.Instance.num,null,0);
            }
         }
      }
      
      public function addDescription(param1:Boolean, param2:int, param3:Array, param4:int) : void
      {
         var _loc5_:FilterFrameText = null;
         var _loc6_:int = 0;
         var _loc7_:FilterFrameText = null;
         var _loc8_:FilterFrameText = null;
         var _loc9_:FilterFrameText = null;
         if(WorldBossHelperManager.Instance.isFighting)
         {
            if(param1)
            {
               this._allHonorTxt.text = "" + WorldBossHelperManager.Instance.allHonor;
               this._allMoneyTxt.text = "" + WorldBossHelperManager.Instance.allMoney;
               _loc5_ = ComponentFactory.Instance.creatComponentByStylename("worldBossHelper.view.infoText");
               if(param3.length > 0)
               {
                  _loc7_ = this._titleTxtArr[param2 - 1];
                  _loc7_.text = LanguageMgr.GetTranslation("worldboss.helper.fightTitleTxt",param2);
               }
               _loc6_ = 0;
               while(_loc6_ < param3.length)
               {
                  _loc8_ = ComponentFactory.Instance.creatComponentByStylename("worldBossHelper.view.infoText");
                  _loc8_.text = LanguageMgr.GetTranslation("worldboss.helper.fightHurt",this._countArr[_loc6_],param3[_loc6_]);
                  _loc8_.x = _loc7_.x + 40;
                  this._vBox.addChild(_loc8_);
                  _loc6_++;
               }
               _loc5_.text = LanguageMgr.GetTranslation("worldboss.helper.fightHonor",param4);
               _loc5_.x = _loc7_.x + 40;
               this._vBox.addChild(_loc5_);
               WorldBossHelperManager.Instance.receieveData = true;
            }
            else
            {
               _loc9_ = ComponentFactory.Instance.creatComponentByStylename("worldBossHelper.view.titleInfoText");
               _loc9_.text = LanguageMgr.GetTranslation("worldboss.helper.fighting",param2);
               this._vBox.addChild(_loc9_);
               this._titleTxtArr.push(_loc9_);
            }
         }
         else
         {
            this.fightFailDescription();
         }
         this._scrollPanel.invalidateViewport(true);
      }
      
      private function removeEvent() : void
      {
         this._openBtn.removeEventListener(MouseEvent.CLICK,this.__btnHandler);
         this._closeBtn.removeEventListener(MouseEvent.CLICK,this.__btnHandler);
      }
      
      public function dispose() : void
      {
         this.disposeStartTimer();
         this.stopAndDisposeTimer();
         this.removeEvent();
         this._infoBg.bitmapData.dispose();
         this._infoBg = null;
         ObjectUtils.disposeObject(this._scrollPanel);
         this._scrollPanel = null;
         ObjectUtils.disposeObject(this._allHonorTxt);
         this._allHonorTxt = null;
         ObjectUtils.disposeObject(this._allMoneyTxt);
         this._allMoneyTxt = null;
         ObjectUtils.disposeObject(this._vBox);
         this._vBox = null;
         ObjectUtils.disposeObject(this._openStateTxt);
         this._openStateTxt = null;
         ObjectUtils.disposeObject(this._closeStateTxt);
         this._closeStateTxt = null;
         ObjectUtils.disposeObject(this._openBtn);
         this._openBtn = null;
         ObjectUtils.disposeObject(this._closeBtn);
         this._closeBtn = null;
         var _loc1_:int = 0;
         while(_loc1_ < this._titleTxtArr.length)
         {
            if(this._titleTxtArr[_loc1_])
            {
               ObjectUtils.disposeObject(this._titleTxtArr[_loc1_]);
               this._titleTxtArr[_loc1_] = null;
            }
            _loc1_++;
         }
         this._titleTxtArr = null;
         this._date1 = null;
         this._date2 = null;
         this._countArr = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
