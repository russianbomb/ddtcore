package worldBossHelper.data
{
   public class WorldBossHelperTypeData
   {
       
      
      private var _requestType:int;
      
      private var _isOpen:Boolean;
      
      private var _buffNum:int;
      
      private var _type:int;
      
      private var _openType:int = 1;
      
      public function WorldBossHelperTypeData()
      {
         super();
      }
      
      public function get isOpen() : Boolean
      {
         return this._isOpen;
      }
      
      public function set isOpen(param1:Boolean) : void
      {
         this._isOpen = param1;
      }
      
      public function get requestType() : int
      {
         return this._requestType;
      }
      
      public function set requestType(param1:int) : void
      {
         this._requestType = param1;
      }
      
      public function get openType() : int
      {
         return this._openType;
      }
      
      public function set openType(param1:int) : void
      {
         this._openType = param1;
      }
      
      public function get type() : int
      {
         return this._type;
      }
      
      public function set type(param1:int) : void
      {
         this._type = param1;
      }
      
      public function get buffNum() : int
      {
         return this._buffNum;
      }
      
      public function set buffNum(param1:int) : void
      {
         this._buffNum = param1;
      }
   }
}
