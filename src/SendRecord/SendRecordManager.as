package SendRecord
{
   import ddt.manager.DesktopManager;
   import ddt.manager.PathManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.StatisticManager;
   import flash.external.ExternalInterface;
   import flash.net.URLLoader;
   import flash.net.URLRequest;
   import flash.net.URLRequestMethod;
   import flash.net.URLVariables;
   import flash.system.Capabilities;
   import flash.system.fscommand;
   import flash.utils.Dictionary;
   
   public class SendRecordManager
   {
      
      private static var _instance:SendRecordManager;
       
      
      private var _browserInfo:String = "";
      
      public function SendRecordManager()
      {
         super();
      }
      
      public static function get Instance() : SendRecordManager
      {
         if(_instance == null)
         {
            _instance = new SendRecordManager();
         }
         return _instance;
      }
      
      private function sendRecordUserVersion(param1:String = "") : void
      {
         var _loc5_:* = null;
         var _loc6_:URLRequest = null;
         var _loc7_:Array = null;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc2_:Dictionary = new Dictionary();
         if(param1 != "")
         {
            _loc7_ = param1.split("|");
            _loc8_ = _loc7_.length / 2;
            _loc9_ = 0;
            while(_loc9_ < _loc8_)
            {
               _loc2_[_loc7_[_loc9_ * 2]] = _loc7_[_loc9_ * 2 + 1];
               _loc9_++;
            }
         }
         var _loc3_:URLVariables = new URLVariables();
         var _loc4_:URLLoader = new URLLoader();
         _loc3_.Browser = this._browserInfo;
         _loc3_.SiteName = StatisticManager.siteName;
         _loc3_.UserName = PlayerManager.Instance.Account.Account;
         _loc3_.Flash = Capabilities.version.split(" ")[1];
         _loc3_.Sys = Capabilities.os;
         _loc3_.Is64Bit = Capabilities.supports64BitProcesses;
         _loc3_.Screen = Capabilities.screenResolutionX + "X" + Capabilities.screenResolutionY;
         for(_loc5_ in _loc2_)
         {
            _loc3_[_loc5_] = _loc2_[_loc5_];
         }
         _loc6_ = new URLRequest(PathManager.solveRequestPath("RecordSysInfo.ashx"));
         _loc6_.method = URLRequestMethod.POST;
         _loc6_.data = _loc3_;
         _loc4_.load(_loc6_);
      }
      
      private function browserInfo(param1:String) : void
      {
         this._browserInfo = param1;
         if(ExternalInterface.available && !DesktopManager.Instance.isDesktop)
         {
            this.sendRecordUserVersion();
         }
         else
         {
            ExternalInterface.addCallback("GetUserData",this.sendRecordUserVersion);
            fscommand("AddUserData");
         }
      }
      
      public function setUp() : void
      {
         var _loc1_:String = null;
         if(ExternalInterface.available)
         {
            _loc1_ = ExternalInterface.call("getBrowserInfo");
            this.browserInfo(_loc1_);
         }
      }
   }
}
