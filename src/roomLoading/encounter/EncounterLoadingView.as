package roomLoading.encounter
{
   import com.greensock.TweenMax;
   import com.pickgliss.loader.BaseLoader;
   import com.pickgliss.loader.LoadResourceManager;
   import com.pickgliss.loader.ModuleLoader;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.UICreatShortcut;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.BallInfo;
   import ddt.events.GameEvent;
   import ddt.loader.MapLoader;
   import ddt.loader.StartupResourceLoader;
   import ddt.loader.TrainerLoader;
   import ddt.manager.BallManager;
   import ddt.manager.GameInSocketOut;
   import ddt.manager.KeyboardShortcutsManager;
   import ddt.manager.LoadBombManager;
   import ddt.manager.MapManager;
   import ddt.manager.PathManager;
   import ddt.manager.PetSkillManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.StateManager;
   import ddt.manager.TimeManager;
   import ddt.states.StateType;
   import ddt.utils.PositionUtils;
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import flash.events.TimerEvent;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   import flash.utils.Timer;
   import game.GameManager;
   import game.model.GameInfo;
   import game.model.GameNeedPetSkillInfo;
   import game.model.Player;
   import im.IMController;
   import pet.date.PetInfo;
   import pet.date.PetSkillTemplateInfo;
   import room.RoomManager;
   import room.events.RoomPlayerEvent;
   import room.model.RoomInfo;
   import room.model.RoomPlayer;
   import roomLoading.view.RoomLoadingCharacterItem;
   import roomLoading.view.RoomLoadingView;
   import trainer.controller.LevelRewardManager;
   import trainer.controller.NewHandGuideManager;
   
   public class EncounterLoadingView extends RoomLoadingView
   {
       
      
      protected var _playerItems:Vector.<EncounterLoadingCharacterItem>;
      
      protected var _love:MovieClip;
      
      protected var _loveII:MovieClip;
      
      protected var _selfItem:EncounterLoadingCharacterItem;
      
      protected var _showArrowComplete:Boolean = false;
      
      protected var _pairingComplete:Boolean = false;
      
      protected var boyIdx:int = 1;
      
      protected var girlIdx:int = 1;
      
      public function EncounterLoadingView(param1:GameInfo)
      {
         super(param1);
      }
      
      override protected function init() : void
      {
         if(NewHandGuideManager.Instance.mapID == 111)
         {
            _startTime = new Date().getTime();
         }
         TimeManager.Instance.enterFightTime = new Date().getTime();
         LevelRewardManager.Instance.hide();
         this._playerItems = new Vector.<EncounterLoadingCharacterItem>();
         KeyboardShortcutsManager.Instance.forbiddenFull();
         _bg = UICreatShortcut.creatAndAdd("asset.EncounterLoadingView.Bg",this);
         _countDownTxt = ComponentFactory.Instance.creatCustomObject("roomLoading.CountDownItem");
         _battleField = ComponentFactory.Instance.creatCustomObject("roomLoading.BattleFieldItem",[_gameInfo.mapIndex]);
         _tipsItem = ComponentFactory.Instance.creatCustomObject("roomLoading.TipsItem");
         _viewerItem = ComponentFactory.Instance.creatCustomObject("roomLoading.ViewerItem");
         _chatViewBg = ComponentFactory.Instance.creatComponentByStylename("roomloading.ChatViewBg");
         this._love = UICreatShortcut.creatAndAdd("ddtroomLoading.EncounterLoadingView.love",this);
         this._loveII = UICreatShortcut.creatAndAdd("ddtroomLoading.EncounterLoadingView.loveII",this);
         this._loveII.visible = false;
         if(_gameInfo.gameMode == 7 || _gameInfo.gameMode == 8 || _gameInfo.gameMode == 10 || _gameInfo.gameMode == 17)
         {
            _dungeonMapItem = ComponentFactory.Instance.creatCustomObject("roomLoading.DungeonMapItem");
         }
         _selfFinish = false;
         addChild(_chatViewBg);
         addChild(_countDownTxt);
         addChild(_battleField);
         addChild(_tipsItem);
         this.initLoadingItems();
         if(_dungeonMapItem)
         {
            addChild(_dungeonMapItem);
         }
         var _loc1_:int = RoomManager.Instance.current.type == RoomInfo.DUNGEON_ROOM || RoomManager.Instance.current.type == RoomInfo.ACADEMY_DUNGEON_ROOM?int(94):int(64);
         _countDownTimer = new Timer(1000,_loc1_);
         _countDownTimer.addEventListener(TimerEvent.TIMER,__countDownTick);
         _countDownTimer.addEventListener(TimerEvent.TIMER_COMPLETE,__countDownComplete);
         _countDownTimer.start();
         StateManager.currentStateType = StateType.GAME_LOADING;
         GameManager.Instance.addEventListener(GameEvent.SELECT_COMPLETE,this.__pairingComplete);
      }
      
      protected function __pairingComplete(param1:GameEvent) : void
      {
         var _loc2_:Timer = null;
         if(!this._showArrowComplete)
         {
            this.showArrow();
            _loc2_ = new Timer(1000,2);
            _loc2_.start();
            _loc2_.addEventListener(TimerEvent.TIMER_COMPLETE,this.__showPairing);
            this._showArrowComplete = true;
         }
      }
      
      protected function __showPairing(param1:TimerEvent) : void
      {
         this.hideArrow();
         var _loc2_:EncounterLoadingCharacterItem = this.getItemByTeam(1,true);
         var _loc3_:EncounterLoadingCharacterItem = this.getItemByTeam(1,false);
         var _loc4_:EncounterLoadingCharacterItem = this.getItemByTeam(2,true);
         var _loc5_:EncounterLoadingCharacterItem = this.getItemByTeam(2,false);
         var _loc6_:Point = PositionUtils.creatPoint("asset.roomLoading.EncounterLoadingCharacterItemBoyPos_1");
         var _loc7_:Point = PositionUtils.creatPoint("asset.roomLoading.EncounterLoadingCharacterItemGirlPos_1");
         var _loc8_:Point = PositionUtils.creatPoint("asset.roomLoading.EncounterLoadingCharacterItemBoyToPos_1");
         var _loc9_:Point = PositionUtils.creatPoint("asset.roomLoading.EncounterLoadingCharacterItemBoyToPos_2");
         var _loc10_:Point = PositionUtils.creatPoint("ddtroomLoading.EncounterLoadingView.lovePos");
         var _loc11_:Point = PositionUtils.creatPoint("ddtroomLoading.EncounterLoadingView.lovePos1");
         if(_loc2_)
         {
            TweenMax.to(_loc2_,2,{
               "x":_loc6_.x,
               "y":_loc6_.y
            });
         }
         if(_loc3_)
         {
            TweenMax.to(_loc3_,2,{
               "x":_loc8_.x,
               "y":_loc8_.y
            });
         }
         if(_loc4_)
         {
            TweenMax.to(_loc4_,2,{
               "x":_loc9_.x,
               "y":_loc9_.y
            });
         }
         if(_loc5_)
         {
            TweenMax.to(_loc5_,2,{
               "x":_loc7_.x,
               "y":_loc7_.y,
               "onComplete":this.pairingComplete
            });
         }
         if(this._selfItem.info.team == 1)
         {
            TweenMax.to(this._love,2,{
               "x":_loc10_.x,
               "y":_loc10_.y
            });
            this._loveII.x = _loc10_.x;
            this._loveII.y = _loc10_.y;
         }
         else
         {
            TweenMax.to(this._love,2,{
               "x":_loc11_.x,
               "y":_loc11_.y
            });
            this._loveII.x = _loc11_.x;
            this._loveII.y = _loc11_.y;
         }
         this._loveII.visible = true;
      }
      
      protected function pairingComplete() : void
      {
         this._pairingComplete = true;
      }
      
      protected function hideArrow() : void
      {
         var _loc1_:EncounterLoadingCharacterItem = null;
         for each(_loc1_ in this._playerItems)
         {
            if(_loc1_)
            {
               _loc1_.arrowVisible = false;
               _loc1_.buttonMode = false;
            }
         }
      }
      
      protected function showArrow() : void
      {
         var _loc2_:Object = null;
         var _loc3_:EncounterLoadingCharacterItem = null;
         var _loc4_:EncounterLoadingCharacterItem = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc1_:Array = GameManager.Instance.selectList;
         for each(_loc2_ in _loc1_)
         {
            if(_loc2_["selectID"] != -1)
            {
               _loc3_ = this.getItem(_loc2_["id"],_loc2_["zoneID"]);
               _loc4_ = this.getItem(_loc2_["selectID"],_loc2_["selectZoneID"]);
               _loc5_ = this.getPosition(_loc2_["id"],_loc2_["zoneID"]);
               _loc6_ = this.getPosition(_loc2_["selectID"],_loc2_["selectZoneID"]);
            }
            else
            {
               _loc3_ = this.getItem(_loc2_["id"],_loc2_["zoneID"]);
               _loc4_ = this.getItem(this.getTeammateID(_loc3_),this.getTeammateZoneID(_loc3_));
               _loc5_ = this.getPosition(_loc2_["id"],_loc2_["zoneID"]);
               _loc6_ = this.getPosition(this.getTeammateID(_loc3_),this.getTeammateZoneID(_loc3_));
            }
            _loc3_.selectObject = this.getArrowType(_loc5_,_loc6_);
         }
      }
      
      protected function getTeammateID(param1:RoomLoadingCharacterItem) : int
      {
         var _loc2_:Array = GameManager.Instance.Current.roomPlayers;
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_.length)
         {
            if(_loc2_[_loc3_].team == param1.info.team && _loc2_[_loc3_].playerInfo.ID != param1.info.playerInfo.ID)
            {
               return _loc2_[_loc3_].playerInfo.ID;
            }
            _loc3_++;
         }
         return -1;
      }
      
      protected function getTeammateZoneID(param1:RoomLoadingCharacterItem) : int
      {
         var _loc2_:Array = GameManager.Instance.Current.roomPlayers;
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_.length)
         {
            if(_loc2_[_loc3_].team == param1.info.team && _loc2_[_loc3_].playerInfo.ID != param1.info.playerInfo.ID && _loc2_[_loc3_].playerInfo.ZoneID != param1.info.playerInfo.ZoneID)
            {
               return _loc2_[_loc3_].playerInfo.ZoneID;
            }
            _loc3_++;
         }
         return -1;
      }
      
      protected function getArrowType(param1:int, param2:int) : int
      {
         if(Math.abs(param1 - param2) == 2)
         {
            return 1;
         }
         if(param1 - param2 == 3)
         {
            return 2;
         }
         if(param1 - param2 == 1)
         {
            return 3;
         }
         if(param1 - param2 == -3)
         {
            return 3;
         }
         if(param1 - param2 == -1)
         {
            return 2;
         }
         return -1;
      }
      
      protected function getPosition(param1:int, param2:int) : int
      {
         var _loc3_:RoomLoadingCharacterItem = this.getItem(param1,param2);
         if(_loc3_ && _loc3_.info.playerInfo.Sex)
         {
            if(_loc3_.index == 1)
            {
               return 1;
            }
            return 2;
         }
         if(_loc3_ && !_loc3_.info.playerInfo.Sex)
         {
            if(_loc3_.index == 1)
            {
               return 3;
            }
            return 4;
         }
         return -1;
      }
      
      protected function getItemByTeam(param1:int, param2:Boolean) : EncounterLoadingCharacterItem
      {
         var _loc3_:int = 0;
         while(_loc3_ < this._playerItems.length)
         {
            if(this._playerItems[_loc3_].info.team == param1 && this._playerItems[_loc3_].info.playerInfo.Sex == param2)
            {
               return this._playerItems[_loc3_];
            }
            _loc3_++;
         }
         return null;
      }
      
      protected function getItem(param1:int, param2:int) : EncounterLoadingCharacterItem
      {
         var _loc3_:int = 0;
         while(_loc3_ < this._playerItems.length)
         {
            if(this._playerItems[_loc3_].info.playerInfo.ID == param1 && this._playerItems[_loc3_].info.playerInfo.ZoneID == param2)
            {
               return this._playerItems[_loc3_];
            }
            _loc3_++;
         }
         return null;
      }
      
      override protected function initLoadingItems() : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:RoomPlayer = null;
         var _loc7_:RoomPlayer = null;
         var _loc8_:int = 0;
         var _loc9_:RoomPlayer = null;
         var _loc10_:EncounterLoadingCharacterItem = null;
         var _loc11_:Player = null;
         var _loc12_:PetInfo = null;
         var _loc13_:int = 0;
         var _loc14_:PetSkillTemplateInfo = null;
         var _loc15_:BallInfo = null;
         var _loc16_:int = 0;
         var _loc17_:GameNeedPetSkillInfo = null;
         var _loc1_:int = _gameInfo.roomPlayers.length;
         var _loc4_:Array = _gameInfo.roomPlayers;
         LoadBombManager.Instance.loadFullRoomPlayersBomb(_loc4_);
         if(!StartupResourceLoader.firstEnterHall)
         {
            LoadBombManager.Instance.loadSpecialBomb();
         }
         for each(_loc6_ in _loc4_)
         {
            if(PlayerManager.Instance.Self.ID == _loc6_.playerInfo.ID)
            {
               _loc5_ = _loc6_.team;
            }
         }
         for each(_loc7_ in _loc4_)
         {
            if(!_loc7_.isViewer)
            {
               if(_loc7_.team == RoomPlayer.BLUE_TEAM)
               {
                  _loc2_++;
               }
               else
               {
                  _loc3_++;
               }
               if(!(RoomManager.Instance.current.type == RoomInfo.FREE_MODE && _loc7_.team != _loc5_))
               {
                  IMController.Instance.saveRecentContactsID(_loc7_.playerInfo.ID);
               }
            }
         }
         _loc8_ = 0;
         while(_loc8_ < _loc1_)
         {
            _loc9_ = _gameInfo.roomPlayers[_loc8_];
            _loc9_.addEventListener(RoomPlayerEvent.PROGRESS_CHANGE,__onLoadingFinished);
            if(_loc9_.isViewer)
            {
               if(contains(_tipsItem))
               {
                  removeChild(_tipsItem);
               }
               addChild(_viewerItem);
            }
            else
            {
               _loc10_ = new EncounterLoadingCharacterItem(_loc9_);
               this.initRoomItem(_loc10_);
               _loc11_ = _gameInfo.findLivingByPlayerID(_loc9_.playerInfo.ID,_loc9_.playerInfo.ZoneID);
               this.initCharacter(_loc11_,_loc10_);
               if(RoomManager.Instance.current.type == RoomInfo.TRANSNATIONALFIGHT_ROOM)
               {
                  _loc12_ = _loc11_.playerInfo.snapPet;
               }
               else
               {
                  _loc12_ = _loc11_.playerInfo.currentPet;
               }
               if(_loc12_)
               {
                  LoadResourceManager.Instance.creatAndStartLoad(PathManager.solvePetGameAssetUrl(_loc12_.GameAssetUrl),BaseLoader.MODULE_LOADER);
                  for each(_loc13_ in _loc12_.equipdSkills)
                  {
                     if(_loc13_ > 0)
                     {
                        _loc14_ = PetSkillManager.getSkillByID(_loc13_);
                        if(_loc14_.EffectPic)
                        {
                           LoadResourceManager.Instance.creatAndStartLoad(PathManager.solvePetSkillEffect(_loc14_.EffectPic),BaseLoader.MODULE_LOADER);
                        }
                        if(_loc14_.NewBallID != -1)
                        {
                           _loc15_ = BallManager.findBall(_loc14_.NewBallID);
                           _loc15_.loadBombAsset();
                           _loc15_.loadCraterBitmap();
                        }
                     }
                  }
               }
            }
            _loc8_++;
         }
         if(blueBig)
         {
            addChild(blueBig);
         }
         if(redBig)
         {
            addChild(redBig);
         }
         if(!StartupResourceLoader.firstEnterHall)
         {
            _loc16_ = 0;
            while(_loc16_ < _gameInfo.neededMovies.length)
            {
               _gameInfo.neededMovies[_loc16_].startLoad();
               _loc16_++;
            }
            for each(_loc17_ in _gameInfo.neededPetSkillResource)
            {
               _loc17_.startLoad();
            }
         }
         _gameInfo.loaderMap = new MapLoader(MapManager.getMapInfo(_gameInfo.mapIndex));
         _gameInfo.loaderMap.load();
         switch(NewHandGuideManager.Instance.mapID)
         {
            case 111:
               _trainerLoad = new TrainerLoader("1");
               break;
            case 112:
               _trainerLoad = new TrainerLoader("2");
               break;
            case 113:
               _trainerLoad = new TrainerLoader("3");
               break;
            case 114:
               _trainerLoad = new TrainerLoader("4");
               break;
            case 115:
               _trainerLoad = new TrainerLoader("5");
               break;
            case 116:
               _trainerLoad = new TrainerLoader("6");
         }
         if(_trainerLoad)
         {
            _trainerLoad.load();
         }
      }
      
      override protected function initRoomItem(param1:RoomLoadingCharacterItem) : void
      {
         var _loc2_:Point = null;
         if(param1.info.playerInfo.Sex)
         {
            if(this.boyIdx == 1)
            {
               PositionUtils.setPos(param1,"asset.roomLoading.EncounterLoadingCharacterItemBoyPos_" + this.boyIdx.toString());
               _loc2_ = ComponentFactory.Instance.creatCustomObject("asset.roomLoading.EncounterLoadingCharacterItemBoyToPos_" + this.boyIdx.toString());
               blueBig = param1;
               this.boyIdx++;
            }
            else
            {
               PositionUtils.setPos(param1,"asset.roomLoading.EncounterLoadingCharacterItemBoyPos_" + this.boyIdx.toString());
               _loc2_ = ComponentFactory.Instance.creatCustomObject("asset.roomLoading.EncounterLoadingCharacterItemBoyToPos_" + this.boyIdx.toString());
            }
         }
         else if(this.girlIdx == 1)
         {
            PositionUtils.setPos(param1,"asset.roomLoading.EncounterLoadingCharacterItemGirlPos_" + this.girlIdx.toString());
            _loc2_ = ComponentFactory.Instance.creatCustomObject("asset.roomLoading.EncounterLoadingCharacterItemGirlToPos_" + this.girlIdx.toString());
            redBig = param1;
            this.girlIdx++;
         }
         else
         {
            PositionUtils.setPos(param1,"asset.roomLoading.EncounterLoadingCharacterItemGirlPos_" + this.girlIdx.toString());
            _loc2_ = ComponentFactory.Instance.creatCustomObject("asset.roomLoading.EncounterLoadingCharacterItemGirlToPos_" + this.girlIdx.toString());
         }
         this._playerItems.push(param1);
         addChild(param1);
         if(!param1.info.playerInfo.isSelf && param1.info.playerInfo.Sex != PlayerManager.Instance.Self.Sex)
         {
            param1.buttonMode = true;
            param1.addEventListener(MouseEvent.CLICK,this.__onSelectObject);
         }
         if(param1.info.playerInfo.isSelf)
         {
            this._selfItem = param1 as EncounterLoadingCharacterItem;
         }
      }
      
      protected function __onSelectObject(param1:MouseEvent) : void
      {
         var _loc3_:EncounterLoadingCharacterItem = null;
         var _loc2_:EncounterLoadingCharacterItem = param1.currentTarget as EncounterLoadingCharacterItem;
         _loc2_.buttonMode = false;
         _loc2_.removeEventListener(MouseEvent.CLICK,this.__onSelectObject);
         _loc2_.bubbleVisible = false;
         GameInSocketOut.sendSelectObject(_loc2_.info.playerInfo.ID,_loc2_.info.playerInfo.ZoneID);
         for each(_loc3_ in this._playerItems)
         {
            if(_loc3_)
            {
               _loc3_.buttonMode = false;
               _loc3_.bubbleVisible = false;
            }
         }
      }
      
      override protected function initCharacter(param1:Player, param2:RoomLoadingCharacterItem) : void
      {
         var _loc3_:Rectangle = ComponentFactory.Instance.creatCustomObject("asset.roomloading.BigCharacterSize");
         var _loc4_:Rectangle = ComponentFactory.Instance.creatCustomObject("asset.roomloading.SuitCharacterSize");
         param1.movie = param2.info.movie;
         param1.character = param2.info.character;
         if(param2.info.playerInfo.Sex)
         {
            param1.character.showWithSize(false,-1,_loc3_.width,_loc3_.height);
            PositionUtils.setPos(param1.character,"roomLoading.encounter.characterPos");
            param2.index = blueCharacterIndex;
            blueCharacterIndex++;
         }
         else
         {
            param1.character.showWithSize(false,1,_loc3_.width,_loc3_.height);
            PositionUtils.setPos(param1.character,"roomLoading.encounter.characterPos1");
            param2.index = redCharacterIndex;
            redCharacterIndex++;
         }
         param1.movie.show(true,-1);
      }
      
      override protected function checkProgress() : Boolean
      {
         var _loc3_:RoomPlayer = null;
         var _loc4_:int = 0;
         var _loc5_:GameNeedPetSkillInfo = null;
         var _loc8_:Player = null;
         var _loc9_:PetInfo = null;
         var _loc10_:int = 0;
         var _loc11_:PetSkillTemplateInfo = null;
         var _loc12_:BallInfo = null;
         _unloadedmsg = "";
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         for each(_loc3_ in _gameInfo.roomPlayers)
         {
            if(!_loc3_.isViewer)
            {
               if(!this._pairingComplete)
               {
                  _loc1_++;
               }
               _loc8_ = _gameInfo.findLivingByPlayerID(_loc3_.playerInfo.ID,_loc3_.playerInfo.ZoneID);
               if(_loc8_.character.completed)
               {
                  _loc2_++;
               }
               else
               {
                  _unloadedmsg = _unloadedmsg + (_loc3_.playerInfo.NickName + "gameplayer.character.completed false\n");
                  _unloadedmsg = _unloadedmsg + _loc8_.character.getCharacterLoadLog();
               }
               _loc1_++;
               if(_loc8_.movie.completed)
               {
                  _loc2_++;
               }
               else
               {
                  _unloadedmsg = _unloadedmsg + (_loc3_.playerInfo.NickName + "gameplayer.movie.completed false\n");
               }
               _loc1_++;
               if(LoadBombManager.Instance.getLoadBombComplete(_loc3_.currentWeapInfo))
               {
                  _loc2_++;
               }
               else
               {
                  _unloadedmsg = _unloadedmsg + ("LoadBombManager.getLoadBombComplete(info.currentWeapInfo) false" + LoadBombManager.Instance.getUnloadedBombString(_loc3_.currentWeapInfo) + "\n");
               }
               _loc1_++;
               _loc9_ = _loc8_.playerInfo.currentPet;
               if(_loc9_)
               {
                  if(_loc9_.assetReady)
                  {
                     _loc2_++;
                  }
                  _loc1_++;
                  for each(_loc10_ in _loc9_.equipdSkills)
                  {
                     if(_loc10_ > 0)
                     {
                        _loc11_ = PetSkillManager.getSkillByID(_loc10_);
                        if(_loc11_.EffectPic)
                        {
                           if(ModuleLoader.hasDefinition(_loc11_.EffectClassLink))
                           {
                              _loc2_++;
                           }
                           else
                           {
                              _unloadedmsg = _unloadedmsg + ("ModuleLoader.hasDefinition(skill.EffectClassLink):" + _loc11_.EffectClassLink + " false\n");
                           }
                           _loc1_++;
                        }
                        if(_loc11_.NewBallID != -1)
                        {
                           _loc12_ = BallManager.findBall(_loc11_.NewBallID);
                           if(_loc12_.isComplete())
                           {
                              _loc2_++;
                           }
                           else
                           {
                              _unloadedmsg = _unloadedmsg + ("BallManager.findBall(skill.NewBallID):" + _loc11_.NewBallID + "false\n");
                           }
                           _loc1_++;
                        }
                     }
                  }
               }
            }
         }
         _loc4_ = 0;
         while(_loc4_ < _gameInfo.neededMovies.length)
         {
            if(_gameInfo.neededMovies[_loc4_].type == 2)
            {
               if(ModuleLoader.hasDefinition(_gameInfo.neededMovies[_loc4_].classPath))
               {
                  _loc2_++;
               }
               else
               {
                  _unloadedmsg = _unloadedmsg + ("ModuleLoader.hasDefinition(_gameInfo.neededMovies[i].classPath):" + _gameInfo.neededMovies[_loc4_].classPath + " false\n");
               }
               _loc1_++;
            }
            else if(_gameInfo.neededMovies[_loc4_].type == 1)
            {
               if(LoadBombManager.Instance.getLivingBombComplete(_gameInfo.neededMovies[_loc4_].bombId))
               {
                  _loc2_++;
               }
               else
               {
                  _unloadedmsg = _unloadedmsg + ("LoadBombManager.getLivingBombComplete(_gameInfo.neededMovies[i].bombId):" + _gameInfo.neededMovies[_loc4_].bombId + " false\n");
               }
               _loc1_++;
            }
            _loc4_++;
         }
         for each(_loc5_ in _gameInfo.neededPetSkillResource)
         {
            if(_loc5_.effect)
            {
               if(ModuleLoader.hasDefinition(_loc5_.effectClassLink))
               {
                  _loc2_++;
               }
               else
               {
                  _unloadedmsg = _unloadedmsg + ("ModuleLoader.hasDefinition(" + _loc5_.effectClassLink + ") false\n");
               }
               _loc1_++;
            }
         }
         if(_gameInfo.loaderMap.completed)
         {
            _loc2_++;
         }
         else
         {
            _unloadedmsg = _unloadedmsg + ("_gameInfo.loaderMap.completed false,pic: " + _gameInfo.loaderMap.info.Pic + "id:" + _gameInfo.loaderMap.info.ID + "\n");
         }
         _loc1_++;
         if(!StartupResourceLoader.firstEnterHall)
         {
            if(LoadBombManager.Instance.getLoadSpecialBombComplete())
            {
               _loc2_++;
            }
            else
            {
               _unloadedmsg = _unloadedmsg + ("LoadBombManager.getLoadSpecialBombComplete() false  " + LoadBombManager.Instance.getUnloadedSpecialBombString() + "\n");
            }
            _loc1_++;
         }
         if(_trainerLoad)
         {
            if(_trainerLoad.completed)
            {
               _loc2_++;
            }
            else
            {
               _unloadedmsg = _unloadedmsg + "_trainerLoad.completed false\n";
            }
            _loc1_++;
         }
         var _loc6_:Number = int(_loc2_ / _loc1_ * 100);
         var _loc7_:Boolean = _loc1_ == _loc2_;
         if(_loc7_ && (!this.checkAnimationIsFinished() || !checkIsEnoughDelayTime()))
         {
            _loc6_ = 99;
            _loc7_ = false;
         }
         GameInSocketOut.sendLoadingProgress(_loc6_);
         RoomManager.Instance.current.selfRoomPlayer.progress = _loc6_;
         return _loc7_;
      }
      
      override protected function leave() : void
      {
      }
      
      override protected function checkAnimationIsFinished() : Boolean
      {
         var _loc1_:EncounterLoadingCharacterItem = null;
         for each(_loc1_ in _characterItems)
         {
            if(!_loc1_.isAnimationFinished)
            {
               return false;
            }
         }
         if(_delayBeginTime <= 0)
         {
            _delayBeginTime = new Date().time;
         }
         return true;
      }
      
      override public function dispose() : void
      {
         var _loc1_:RoomPlayer = null;
         var _loc2_:int = 0;
         GameManager.Instance.removeEventListener(GameEvent.SELECT_COMPLETE,this.__pairingComplete);
         KeyboardShortcutsManager.Instance.cancelForbidden();
         _countDownTimer.removeEventListener(TimerEvent.TIMER,__countDownTick);
         _countDownTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,__countDownComplete);
         _countDownTimer.stop();
         _countDownTimer = null;
         ObjectUtils.disposeObject(_trainerLoad);
         ObjectUtils.disposeObject(_bg);
         ObjectUtils.disposeObject(_chatViewBg);
         ObjectUtils.disposeObject(_versus);
         ObjectUtils.disposeObject(_countDownTxt);
         ObjectUtils.disposeObject(_battleField);
         ObjectUtils.disposeObject(_tipsItem);
         ObjectUtils.disposeObject(_viewerItem);
         for each(_loc1_ in _gameInfo.roomPlayers)
         {
            _loc1_.removeEventListener(RoomPlayerEvent.PROGRESS_CHANGE,__onLoadingFinished);
         }
         TweenMax.killAll(false,false,false);
         _loc2_ = 0;
         while(_loc2_ < this._playerItems.length)
         {
            TweenMax.killTweensOf(this._playerItems[_loc2_]);
            this._playerItems[_loc2_].removeEventListener(MouseEvent.CLICK,this.__onSelectObject);
            this._playerItems[_loc2_].dispose();
            this._playerItems[_loc2_] = null;
            _loc2_++;
         }
         if(this._love)
         {
            TweenMax.killTweensOf(this._love);
            ObjectUtils.disposeObject(this._love);
            this._love = null;
         }
         ObjectUtils.disposeObject(_dungeonMapItem);
         _dungeonMapItem = null;
         _characterItems = null;
         _trainerLoad = null;
         _bg = null;
         _chatViewBg = null;
         _gameInfo = null;
         _versus = null;
         _countDownTxt = null;
         _battleField = null;
         _tipsItem = null;
         _countDownTimer = null;
         _viewerItem = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
