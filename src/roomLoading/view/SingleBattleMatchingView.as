package roomLoading.view
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.StateManager;
   import ddt.states.StateType;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.events.Event;
   import flash.events.TimerEvent;
   import game.GameManager;
   import game.model.GameInfo;
   
   public class SingleBattleMatchingView extends RoomLoadingView
   {
       
      
      private var _matchTxt:Bitmap;
      
      private var _playerArr:Array;
      
      public function SingleBattleMatchingView(param1:GameInfo)
      {
         var _loc4_:Bitmap = null;
         super(param1);
         GameManager.Instance.addEventListener(GameManager.START_LOAD,this.__onStartLoad);
         this._matchTxt = ComponentFactory.Instance.creatBitmap("asset.room.view.roomView.SingleBattleMatch.matchTxt");
         addChild(this._matchTxt);
         this._playerArr = new Array();
         var _loc2_:int = _gameInfo.roomPlayers.length;
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_)
         {
            _loc4_ = ComponentFactory.Instance.creatBitmap("game.player.defaultPlayerCharacter");
            PositionUtils.setPos(_loc4_,"asset.roomLoading.CharacterItemRedPos_1");
            addChild(_loc4_);
            _loc4_.x = _loc4_.x + 90 * _loc3_;
            _loc4_.y = _loc4_.y + 62;
            this._playerArr.push(_loc4_);
            _loc3_++;
         }
      }
      
      override protected function init() : void
      {
         super.init();
         StateManager.currentStateType = StateType.SINGLEBATTLE_MATCHING;
      }
      
      override protected function __countDownTick(param1:TimerEvent) : void
      {
         _countDownTxt.updateNum();
      }
      
      override protected function initRoomItem(param1:RoomLoadingCharacterItem) : void
      {
         super.initRoomItem(param1);
         param1.removePerecentageTxt();
      }
      
      protected function __onStartLoad(param1:Event) : void
      {
         if(GameManager.Instance.Current == null)
         {
            return;
         }
         StateManager.setState(StateType.ROOM_LOADING,GameManager.Instance.Current);
      }
      
      override public function dispose() : void
      {
         super.dispose();
         GameManager.Instance.removeEventListener(GameManager.START_LOAD,this.__onStartLoad);
         if(this._matchTxt)
         {
            ObjectUtils.disposeObject(this._matchTxt);
         }
         this._matchTxt = null;
         var _loc1_:int = 0;
         while(_loc1_ < this._playerArr.length)
         {
            ObjectUtils.disposeObject(this._playerArr[_loc1_]);
            this._playerArr[_loc1_] = null;
            _loc1_++;
         }
         this._playerArr = null;
      }
   }
}
