package roomLoading.view
{
   import com.pickgliss.loader.BaseLoader;
   import com.pickgliss.loader.DisplayLoader;
   import com.pickgliss.loader.LoadResourceManager;
   import com.pickgliss.loader.LoaderEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.PathManager;
   import ddt.utils.PositionUtils;
   import escort.EscortManager;
   import flash.display.Bitmap;
   import flash.display.DisplayObject;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   import game.GameManager;
   import room.RoomManager;
   import room.model.RoomInfo;
   
   public class RoomLoadingDungeonMapItem extends Sprite implements Disposeable
   {
       
      
      private var _displayMc:MovieClip;
      
      private var _itemFrame:DisplayObject;
      
      private var _item:Sprite;
      
      private var _mapLoader:DisplayLoader;
      
      public function RoomLoadingDungeonMapItem()
      {
         super();
         this.init();
      }
      
      private function init() : void
      {
         this._item = new Sprite();
         this._itemFrame = ComponentFactory.Instance.creat("asset.roomLoading.DungeonMapFrame");
         this._displayMc = ComponentFactory.Instance.creat("asset.roomloading.displayMC");
         this._mapLoader = LoadResourceManager.Instance.createLoader(this.solveMapPath(),BaseLoader.BITMAP_LOADER);
         this._mapLoader.addEventListener(LoaderEvent.COMPLETE,this.__onLoadComplete);
         LoadResourceManager.Instance.startLoad(this._mapLoader);
      }
      
      private function __onLoadComplete(param1:Event) : void
      {
         var _loc2_:Bitmap = null;
         if(this._mapLoader.isSuccess)
         {
            this._mapLoader.removeEventListener(LoaderEvent.COMPLETE,this.__onLoadComplete);
            _loc2_ = this._mapLoader.content as Bitmap;
            _loc2_.height = 365;
            this._item.addChild(_loc2_);
            this._item.addChild(this._itemFrame);
            PositionUtils.setPos(this._displayMc,"asset.roomLoading.DungeonMapLoaderPos");
            this._displayMc.scaleX = this._item.scaleX = -1;
            this._displayMc["character"].addChild(this._item);
            this._displayMc.gotoAndPlay("appear1");
            addChild(this._displayMc);
         }
      }
      
      private function solveMapPath() : String
      {
         var _loc1_:String = PathManager.SITE_MAIN + "image/map/";
         if(GameManager.Instance.Current.gameMode == 8)
         {
            _loc1_ = _loc1_ + "1133/show.jpg";
            return _loc1_;
         }
         if(RoomManager.Instance.current.type == RoomInfo.LANBYRINTH_ROOM)
         {
            _loc1_ = _loc1_ + "214/show1.png";
            return _loc1_;
         }
         if(RoomManager.Instance.current.type == RoomInfo.CONSORTIA_BOSS)
         {
            _loc1_ = _loc1_ + "215/show1.jpg";
            return _loc1_;
         }
         if(RoomManager.Instance.current.type == RoomInfo.CAMPBATTLE_BATTLE)
         {
            _loc1_ = _loc1_ + "216/show1.jpg";
            return _loc1_;
         }
         if(RoomManager.Instance.current.type == RoomInfo.SEVEN_DOUBLE)
         {
            if(EscortManager.instance.isStart)
            {
               _loc1_ = _loc1_ + "1350/show1.png";
            }
            else
            {
               _loc1_ = _loc1_ + "217/show1.png";
            }
            return _loc1_;
         }
         if(RoomManager.Instance.current.type == RoomInfo.CATCH_BEAST)
         {
            _loc1_ = _loc1_ + "1347/show1.jpg";
            return _loc1_;
         }
         var _loc2_:String = GameManager.Instance.Current.missionInfo.pic;
         var _loc3_:String = RoomManager.Instance.current.pic;
         if(RoomManager.Instance.current.isOpenBoss)
         {
            if(_loc3_ == null || _loc3_ == "")
            {
               _loc1_ = _loc1_ + (RoomManager.Instance.current.mapId + "/show1.jpg");
            }
            else
            {
               _loc1_ = _loc1_ + (RoomManager.Instance.current.mapId + "/" + _loc3_);
            }
         }
         else if(_loc2_ == null || RoomManager.Instance.current.type == RoomInfo.ACTIVITY_DUNGEON_ROOM || _loc2_ == "")
         {
            _loc1_ = _loc1_ + (RoomManager.Instance.current.mapId + "/show1.jpg");
         }
         else
         {
            _loc1_ = _loc1_ + (RoomManager.Instance.current.mapId + "/" + _loc2_);
         }
         return _loc1_;
      }
      
      public function disappear() : void
      {
         this._displayMc.gotoAndPlay("disappear1");
      }
      
      public function dispose() : void
      {
         this._mapLoader.removeEventListener(LoaderEvent.COMPLETE,this.__onLoadComplete);
         ObjectUtils.disposeAllChildren(this);
         this._mapLoader = null;
         this._displayMc = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
