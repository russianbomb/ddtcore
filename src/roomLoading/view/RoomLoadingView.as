package roomLoading.view
{
   import com.greensock.TweenMax;
   import com.pickgliss.loader.BaseLoader;
   import com.pickgliss.loader.LoadResourceManager;
   import com.pickgliss.loader.ModuleLoader;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.image.Image;
   import com.pickgliss.utils.ObjectUtils;
   import consortion.ConsortionModelControl;
   import ddt.data.BallInfo;
   import ddt.loader.MapLoader;
   import ddt.loader.StartupResourceLoader;
   import ddt.loader.TrainerLoader;
   import ddt.manager.BallManager;
   import ddt.manager.GameInSocketOut;
   import ddt.manager.KeyboardShortcutsManager;
   import ddt.manager.LoadBombManager;
   import ddt.manager.MapManager;
   import ddt.manager.PathManager;
   import ddt.manager.PetSkillManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.StateManager;
   import ddt.manager.TimeManager;
   import ddt.states.StateType;
   import ddt.utils.PositionUtils;
   import escort.EscortManager;
   import fightFootballTime.manager.FightFootballTimeManager;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.TimerEvent;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   import flash.utils.Timer;
   import game.GameManager;
   import game.model.GameInfo;
   import game.model.GameNeedPetSkillInfo;
   import game.model.Player;
   import im.IMController;
   import labyrinth.LabyrinthManager;
   import pet.date.PetInfo;
   import pet.date.PetSkillTemplateInfo;
   import road7th.data.DictionaryData;
   import room.RoomManager;
   import room.events.RoomPlayerEvent;
   import room.model.RoomInfo;
   import room.model.RoomPlayer;
   import room.transnational.RoomLoadingTransnationalFieldItem;
   import sevenDouble.SevenDoubleManager;
   import trainer.controller.LevelRewardManager;
   import trainer.controller.NewHandGuideManager;
   import trainer.controller.WeakGuildManager;
   import worldboss.WorldBossManager;
   
   public class RoomLoadingView extends Sprite implements Disposeable
   {
      
      protected static const DELAY_TIME:int = 1000;
       
      
      protected var _bg:Bitmap;
      
      protected var _gameInfo:GameInfo;
      
      protected var _versus:RoomLoadingVersusItem;
      
      protected var _countDownTxt:RoomLoadingCountDownNum;
      
      protected var _battleField:RoomLoadingBattleFieldItem;
      
      protected var _tipsItem:RoomLoadingTipsItem;
      
      protected var _viewerItem:RoomLoadingViewerItem;
      
      protected var _dungeonMapItem:RoomLoadingDungeonMapItem;
      
      protected var _characterItems:Vector.<RoomLoadingCharacterItem>;
      
      protected var _countDownTimer:Timer;
      
      protected var _selfFinish:Boolean;
      
      protected var _trainerLoad:TrainerLoader;
      
      protected var _startTime:Number;
      
      protected var _chatViewBg:Image;
      
      protected var blueIdx:int = 1;
      
      protected var redIdx:int = 1;
      
      protected var blueCharacterIndex:int = 1;
      
      protected var redCharacterIndex:int = 1;
      
      protected var blueBig:RoomLoadingCharacterItem;
      
      protected var redBig:RoomLoadingCharacterItem;
      
      protected var blueFlag:RoomLoadingTransnationalFieldItem;
      
      protected var redFlag:RoomLoadingTransnationalFieldItem;
      
      protected var _leaving:Boolean = false;
      
      protected var _amountOfFinishedPlayer:int = 0;
      
      protected var _hasLoadedFinished:DictionaryData;
      
      protected var _delayBeginTime:Number = 0;
      
      protected var _unloadedmsg:String = "";
      
      public function RoomLoadingView(param1:GameInfo)
      {
         this._hasLoadedFinished = new DictionaryData();
         super();
         this._gameInfo = param1;
         this.init();
      }
      
      protected function init() : void
      {
         if(NewHandGuideManager.Instance.mapID == 111)
         {
            this._startTime = new Date().getTime();
         }
         TimeManager.Instance.enterFightTime = new Date().getTime();
         LevelRewardManager.Instance.hide();
         this.blueFlag = new RoomLoadingTransnationalFieldItem();
         this.redFlag = new RoomLoadingTransnationalFieldItem();
         this._characterItems = new Vector.<RoomLoadingCharacterItem>();
         KeyboardShortcutsManager.Instance.forbiddenFull();
         this._bg = ComponentFactory.Instance.creatBitmap("asset.roomloading.vsBg");
         this._versus = ComponentFactory.Instance.creatCustomObject("roomLoading.VersusItem",[RoomManager.Instance.current.gameMode]);
         this._countDownTxt = ComponentFactory.Instance.creatCustomObject("roomLoading.CountDownItem");
         this._battleField = ComponentFactory.Instance.creatCustomObject("roomLoading.BattleFieldItem",[this._gameInfo.mapIndex]);
         this._tipsItem = ComponentFactory.Instance.creatCustomObject("roomLoading.TipsItem");
         this._viewerItem = ComponentFactory.Instance.creatCustomObject("roomLoading.ViewerItem");
         this._chatViewBg = ComponentFactory.Instance.creatComponentByStylename("roomloading.ChatViewBg");
         this._selfFinish = false;
         if(RoomManager.Instance.current.type != RoomInfo.FIGHTFOOTBALLTIME_ROOM)
         {
            addChild(this._bg);
            addChild(this._chatViewBg);
            addChild(this._versus);
            addChild(this._countDownTxt);
            addChild(this._battleField);
            addChild(this._tipsItem);
         }
         this.initLoadingItems();
         if(this._gameInfo.gameMode == 7 || this._gameInfo.gameMode == 8 || this._gameInfo.gameMode == 10 || this._gameInfo.gameMode == 17 || this._gameInfo.gameMode == 19 || this._gameInfo.gameMode == GameManager.CAMP_BATTLE_MODEL_PVE || this._gameInfo.gameMode == 25 || this._gameInfo.gameMode == 40)
         {
            if(!this.redBig)
            {
               this._dungeonMapItem = ComponentFactory.Instance.creatCustomObject("roomLoading.DungeonMapItem");
            }
         }
         if(this._dungeonMapItem)
         {
            addChild(this._dungeonMapItem);
         }
         var _loc1_:int = RoomManager.Instance.current.type == RoomInfo.DUNGEON_ROOM || RoomManager.Instance.current.type == RoomInfo.ACADEMY_DUNGEON_ROOM || RoomManager.Instance.current.type == RoomInfo.SINGLE_BATTLE || RoomManager.Instance.current.type == RoomInfo.CONSORTIA_MATCH_SCORE?int(94):int(64);
         this._countDownTimer = new Timer(1000,_loc1_);
         this._countDownTimer.addEventListener(TimerEvent.TIMER,this.__countDownTick);
         this._countDownTimer.addEventListener(TimerEvent.TIMER_COMPLETE,this.__countDownComplete);
         this._countDownTimer.start();
         StateManager.currentStateType = StateType.GAME_LOADING;
      }
      
      protected function initLoadingItems() : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:Array = null;
         var _loc5_:int = 0;
         var _loc6_:RoomPlayer = null;
         var _loc7_:RoomPlayer = null;
         var _loc8_:int = 0;
         var _loc9_:RoomPlayer = null;
         var _loc10_:RoomLoadingCharacterItem = null;
         var _loc11_:Player = null;
         var _loc12_:PetInfo = null;
         var _loc13_:int = 0;
         var _loc14_:PetSkillTemplateInfo = null;
         var _loc15_:BallInfo = null;
         var _loc16_:int = 0;
         var _loc17_:GameNeedPetSkillInfo = null;
         var _loc1_:int = this._gameInfo.roomPlayers.length;
         _loc4_ = this._gameInfo.roomPlayers;
         if(this._gameInfo.roomType == 20)
         {
         }
         LoadBombManager.Instance.loadFullRoomPlayersBomb(_loc4_);
         if(!StartupResourceLoader.firstEnterHall)
         {
            LoadBombManager.Instance.loadSpecialBomb();
         }
         for each(_loc6_ in _loc4_)
         {
            if(PlayerManager.Instance.Self.ID == _loc6_.playerInfo.ID)
            {
               _loc5_ = _loc6_.team;
            }
         }
         for each(_loc7_ in _loc4_)
         {
            if(!_loc7_.isViewer)
            {
               if(_loc7_.team == RoomPlayer.BLUE_TEAM)
               {
                  _loc2_++;
               }
               else
               {
                  _loc3_++;
               }
               if(!(RoomManager.Instance.current.type == RoomInfo.FREE_MODE && _loc7_.team != _loc5_))
               {
                  IMController.Instance.saveRecentContactsID(_loc7_.playerInfo.ID);
               }
            }
         }
         _loc8_ = 0;
         while(_loc8_ < _loc1_)
         {
            _loc9_ = this._gameInfo.roomPlayers[_loc8_];
            _loc9_.addEventListener(RoomPlayerEvent.PROGRESS_CHANGE,this.__onLoadingFinished);
            if(_loc9_.isViewer)
            {
               if(contains(this._tipsItem))
               {
                  removeChild(this._tipsItem);
               }
               addChild(this._viewerItem);
            }
            else
            {
               _loc10_ = new RoomLoadingCharacterItem(_loc9_);
               this.initRoomItem(_loc10_);
               _loc11_ = this._gameInfo.findLivingByPlayerID(_loc9_.playerInfo.ID,_loc9_.playerInfo.ZoneID);
               this.initCharacter(_loc11_,_loc10_);
               if(RoomManager.Instance.isTransnationalFight() && _loc11_.isSelf)
               {
                  _loc12_ = _loc11_.playerInfo.snapPet;
               }
               else
               {
                  _loc12_ = _loc11_.playerInfo.currentPet;
               }
               if(_loc12_)
               {
                  LoadResourceManager.Instance.creatAndStartLoad(PathManager.solvePetGameAssetUrl(_loc12_.GameAssetUrl),BaseLoader.MODULE_LOADER);
                  for each(_loc13_ in _loc12_.equipdSkills)
                  {
                     if(_loc13_ > 0)
                     {
                        _loc14_ = PetSkillManager.getSkillByID(_loc13_);
                        if(_loc14_.EffectPic)
                        {
                           LoadResourceManager.Instance.creatAndStartLoad(PathManager.solvePetSkillEffect(_loc14_.EffectPic),BaseLoader.MODULE_LOADER);
                        }
                        if(_loc14_.NewBallID != -1)
                        {
                           _loc15_ = BallManager.findBall(_loc14_.NewBallID);
                           _loc15_.loadBombAsset();
                           _loc15_.loadCraterBitmap();
                        }
                     }
                  }
               }
            }
            _loc8_++;
         }
         if(this.blueBig)
         {
            addChild(this.blueBig);
         }
         if(this.redBig)
         {
            addChild(this.redBig);
         }
         if(!StartupResourceLoader.firstEnterHall)
         {
            _loc16_ = 0;
            while(_loc16_ < this._gameInfo.neededMovies.length)
            {
               if(this._gameInfo.neededMovies[_loc16_].type == 1)
               {
                  this._gameInfo.neededMovies[_loc16_].startLoad();
               }
               _loc16_++;
            }
            for each(_loc17_ in this._gameInfo.neededPetSkillResource)
            {
               _loc17_.startLoad();
            }
         }
         this._gameInfo.loaderMap = new MapLoader(MapManager.getMapInfo(this._gameInfo.mapIndex));
         this._gameInfo.loaderMap.load();
         switch(NewHandGuideManager.Instance.mapID)
         {
            case 111:
               this._trainerLoad = new TrainerLoader("1");
               break;
            case 112:
               this._trainerLoad = new TrainerLoader("2");
               break;
            case 113:
               this._trainerLoad = new TrainerLoader("3");
               break;
            case 114:
               this._trainerLoad = new TrainerLoader("4");
               break;
            case 115:
               this._trainerLoad = new TrainerLoader("5");
               break;
            case 116:
               this._trainerLoad = new TrainerLoader("6");
         }
         if(this._trainerLoad)
         {
            this._trainerLoad.load();
         }
      }
      
      protected function __onLoadingFinished(param1:Event) : void
      {
         var _loc2_:RoomPlayer = param1.currentTarget as RoomPlayer;
         if(_loc2_.progress < 100 || this._hasLoadedFinished.hasKey(_loc2_))
         {
            return;
         }
         this._amountOfFinishedPlayer++;
         this._hasLoadedFinished.add(_loc2_,_loc2_);
         if(this._amountOfFinishedPlayer == this._gameInfo.roomPlayers.length)
         {
            this.leave();
         }
      }
      
      protected function initCharacter(param1:Player, param2:RoomLoadingCharacterItem) : void
      {
         var _loc3_:Rectangle = ComponentFactory.Instance.creatCustomObject("asset.roomloading.BigCharacterSize");
         var _loc4_:Rectangle = ComponentFactory.Instance.creatCustomObject("asset.roomloading.SuitCharacterSize");
         param1.movie = param2.info.movie;
         param1.character = param2.info.character;
         param1.character.showGun = false;
         param1.character.showWing = false;
         if(param2.info.team == RoomPlayer.BLUE_TEAM)
         {
            if(param1.isSelf || this.blueCharacterIndex == 1 && this._gameInfo.selfGamePlayer.team != RoomPlayer.BLUE_TEAM)
            {
               PositionUtils.setPos(param2.displayMc,"asset.roomloading.BigCharacterBluePos");
               param1.character.showWithSize(false,-1,_loc3_.width,_loc3_.height);
            }
            else
            {
               PositionUtils.setPos(param2.displayMc,"asset.roomloading.SmallCharacterBluePos");
               param1.character.show(false,-1);
            }
            param2.appear(this.blueCharacterIndex.toString());
            param2.index = this.blueCharacterIndex;
            this.blueCharacterIndex++;
         }
         else
         {
            if(param1.isSelf || this.redCharacterIndex == 1 && this._gameInfo.selfGamePlayer.team != RoomPlayer.RED_TEAM)
            {
               param1.character.showWithSize(false,-1,_loc3_.width,_loc3_.height);
               PositionUtils.setPos(param2.displayMc,"asset.roomloading.BigCharacterRedPos");
            }
            else
            {
               PositionUtils.setPos(param2.displayMc,"asset.roomloading.SmallCharacterRedPos");
               param1.character.show(false,-1);
            }
            param2.appear(this.redCharacterIndex.toString());
            param2.index = this.redCharacterIndex;
            this.redCharacterIndex++;
         }
         param1.movie.show(true,-1);
      }
      
      protected function initRoomItem(param1:RoomLoadingCharacterItem) : void
      {
         var _loc2_:Point = null;
         if(param1.info.team == RoomPlayer.BLUE_TEAM)
         {
            if(param1.info.isSelf || this.blueIdx == 1 && this._gameInfo.selfGamePlayer.team != RoomPlayer.BLUE_TEAM)
            {
               PositionUtils.setPos(param1,"asset.roomLoading.CharacterItemBluePos_1");
               _loc2_ = ComponentFactory.Instance.creatCustomObject("asset.roomLoading.CharacterItemBlueFromPos_1");
               this.blueBig = param1;
               if(RoomManager.Instance.isTransnationalFight())
               {
                  this.blueFlag.FlagID = param1.info.playerInfo.flagID;
                  PositionUtils.setPos(this.blueFlag,"asset.roomLoading.flagItemBluePos");
                  addChild(this.blueFlag);
               }
               param1.addWeapon(false,-1);
               if(this._gameInfo.selfGamePlayer.team != RoomPlayer.BLUE_TEAM)
               {
                  this.blueIdx++;
               }
            }
            else
            {
               if(this.blueIdx == 1)
               {
                  this.blueIdx++;
               }
               PositionUtils.setPos(param1,"asset.roomLoading.CharacterItemBluePos_" + this.blueIdx.toString());
               _loc2_ = ComponentFactory.Instance.creatCustomObject("asset.roomLoading.CharacterItemBlueFromPos_" + this.blueIdx.toString());
               this.blueIdx++;
               param1.addWeapon(true,-1);
            }
         }
         else if(param1.info.isSelf || this.redIdx == 1 && this._gameInfo.selfGamePlayer.team != RoomPlayer.RED_TEAM)
         {
            PositionUtils.setPos(param1,"asset.roomLoading.CharacterItemRedPos_1");
            _loc2_ = ComponentFactory.Instance.creatCustomObject("asset.roomLoading.CharacterItemRedFromPos_1");
            this.redBig = param1;
            if(RoomManager.Instance.isTransnationalFight())
            {
               this.redFlag.FlagID = param1.info.playerInfo.flagID;
               PositionUtils.setPos(this.redFlag,"asset.roomLoading.flagItemRedPos");
               addChild(this.redFlag);
            }
            param1.addWeapon(false,1);
            if(this._gameInfo.selfGamePlayer.team != RoomPlayer.RED_TEAM)
            {
               this.redIdx++;
            }
         }
         else
         {
            if(this.redIdx == 1)
            {
               this.redIdx++;
            }
            PositionUtils.setPos(param1,"asset.roomLoading.CharacterItemRedPos_" + this.redIdx.toString());
            _loc2_ = ComponentFactory.Instance.creatCustomObject("asset.roomLoading.CharacterItemRedFromPos_" + this.redIdx.toString());
            this.redIdx++;
            param1.addWeapon(true,1);
         }
         this._characterItems.push(param1);
         if(RoomManager.Instance.current.type != RoomInfo.FIGHTFOOTBALLTIME_ROOM)
         {
            addChild(param1);
         }
      }
      
      protected function leave() : void
      {
         if(!this._leaving)
         {
            this._characterItems.forEach(function(param1:RoomLoadingCharacterItem, param2:int, param3:Vector.<RoomLoadingCharacterItem>):void
            {
               param1.disappear(param1.index.toString());
            });
            if(this._dungeonMapItem)
            {
               this._dungeonMapItem.disappear();
            }
            this._leaving = true;
         }
      }
      
      protected function __countDownTick(param1:TimerEvent) : void
      {
         this._selfFinish = this.checkProgress();
         this._countDownTxt.updateNum();
         if(this._selfFinish)
         {
            dispatchEvent(new Event(Event.COMPLETE));
            if(NewHandGuideManager.Instance.mapID == 111)
            {
               WeakGuildManager.Instance.timeStatistics(1,this._startTime);
            }
         }
      }
      
      protected function __countDownComplete(param1:TimerEvent) : void
      {
         var _loc2_:int = RoomManager.Instance.current.type == RoomInfo.DUNGEON_ROOM || RoomManager.Instance.current.type == RoomInfo.ACADEMY_DUNGEON_ROOM?int(94):int(64);
         if(this._countDownTimer.currentCount == _loc2_ && RoomManager.Instance.current.type == RoomInfo.WORLD_BOSS_FIGHT)
         {
            WorldBossManager.IsSuccessStartGame = false;
            StateManager.setState(StateType.MAIN);
            return;
         }
         if(!this._selfFinish)
         {
            if(RoomManager.Instance.current.type == RoomInfo.MATCH_ROOM || RoomManager.Instance.current.type == RoomInfo.CHALLENGE_ROOM || RoomManager.Instance.current.type == RoomInfo.ENCOUNTER_ROOM || RoomManager.Instance.current.type == RoomInfo.SINGLE_BATTLE)
            {
               StateManager.setState(StateType.ROOM_LIST);
            }
            else if(RoomManager.Instance.current.type == RoomInfo.FRESHMAN_ROOM)
            {
               StateManager.setState(StateType.MAIN);
            }
            else if(RoomManager.Instance.current.type == RoomInfo.FIGHT_LIB_ROOM || GameManager.Instance.Current.roomType == FightFootballTimeManager.FIGHTFOOTBALLTIME_ROOM)
            {
               StateManager.setState(StateType.MAIN);
            }
            else if(RoomManager.Instance.current.type == RoomInfo.WORLD_BOSS_FIGHT)
            {
               WorldBossManager.IsSuccessStartGame = false;
               StateManager.setState(StateType.WORLDBOSS_ROOM);
            }
            else if(RoomManager.Instance.current.type == RoomInfo.LANBYRINTH_ROOM)
            {
               StateManager.setState(StateType.MAIN,LabyrinthManager.Instance.show);
            }
            else if(RoomManager.Instance.current.type == RoomInfo.CONSORTIA_BOSS)
            {
               StateManager.setState(StateType.CONSORTIA,ConsortionModelControl.Instance.openBossFrame);
            }
            else if(RoomManager.Instance.current.type == RoomInfo.CONSORTIA_BATTLE)
            {
               StateManager.setState(StateType.MAIN);
            }
            else if(RoomManager.Instance.current.type == RoomInfo.CAMPBATTLE_BATTLE)
            {
               GameInSocketOut.sendSingleRoomBegin(RoomManager.CAMP_BATTLE_ROOM);
            }
            else if(RoomManager.Instance.current.type == RoomInfo.TRANSNATIONALFIGHT_ROOM)
            {
               StateManager.setState(StateType.MAIN);
            }
            else if(RoomManager.Instance.current.type == RoomInfo.SEVEN_DOUBLE)
            {
               if(SevenDoubleManager.instance.isStart)
               {
                  StateManager.setState(StateType.SEVEN_DOUBLE_SCENE);
               }
               else if(EscortManager.instance.isStart)
               {
                  StateManager.setState(StateType.ESCORT);
               }
               else
               {
                  StateManager.setState(StateType.MAIN);
               }
            }
            else if(RoomManager.Instance.current.type == RoomInfo.CONSORTIA_MATCH_SCORE || RoomManager.Instance.current.type == RoomInfo.CONSORTIA_MATCH_RANK || RoomManager.Instance.current.type == RoomInfo.CONSORTIA_MATCH_SCORE_WHOLE || RoomManager.Instance.current.type == RoomInfo.CONSORTIA_MATCH_RANK_WHOLE)
            {
               StateManager.setState(StateType.MAIN);
            }
            else
            {
               StateManager.setState(StateType.DUNGEON_LIST);
            }
            SocketManager.Instance.out.sendErrorMsg(this._unloadedmsg);
         }
      }
      
      protected function checkAnimationIsFinished() : Boolean
      {
         var _loc1_:RoomLoadingCharacterItem = null;
         for each(_loc1_ in this._characterItems)
         {
            if(!_loc1_.isAnimationFinished)
            {
               return false;
            }
         }
         if(this._delayBeginTime <= 0)
         {
            this._delayBeginTime = new Date().time;
         }
         return true;
      }
      
      protected function checkProgress() : Boolean
      {
         var _loc3_:RoomPlayer = null;
         var _loc4_:GameNeedPetSkillInfo = null;
         var _loc5_:int = 0;
         var _loc8_:GameInfo = null;
         var _loc9_:Player = null;
         var _loc10_:PetInfo = null;
         var _loc11_:int = 0;
         var _loc12_:PetSkillTemplateInfo = null;
         var _loc13_:BallInfo = null;
         this._unloadedmsg = "";
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         for each(_loc3_ in this._gameInfo.roomPlayers)
         {
            if(!_loc3_.isViewer)
            {
               _loc8_ = GameManager.Instance.Current;
               _loc9_ = this._gameInfo.findLivingByPlayerID(_loc3_.playerInfo.ID,_loc3_.playerInfo.ZoneID);
               if(LoadBombManager.Instance.getLoadBombComplete(_loc3_.currentWeapInfo))
               {
                  _loc2_++;
               }
               else
               {
                  this._unloadedmsg = this._unloadedmsg + ("LoadBombManager.getLoadBombComplete(info.currentWeapInfo) false" + LoadBombManager.Instance.getUnloadedBombString(_loc3_.currentWeapInfo) + "\n");
               }
               _loc1_++;
               if(RoomManager.Instance.isTransnationalFight() && _loc9_.isSelf)
               {
                  _loc10_ = _loc9_.playerInfo.snapPet;
               }
               else
               {
                  _loc10_ = _loc9_.playerInfo.currentPet;
               }
               if(_loc10_)
               {
                  if(_loc10_.assetReady)
                  {
                     _loc2_++;
                  }
                  _loc1_++;
                  for each(_loc11_ in _loc10_.equipdSkills)
                  {
                     if(_loc11_ > 0)
                     {
                        _loc12_ = PetSkillManager.getSkillByID(_loc11_);
                        if(_loc12_.EffectPic)
                        {
                           if(ModuleLoader.hasDefinition(_loc12_.EffectClassLink))
                           {
                              _loc2_++;
                           }
                           else
                           {
                              this._unloadedmsg = this._unloadedmsg + ("ModuleLoader.hasDefinition(skill.EffectClassLink):" + _loc12_.EffectClassLink + " false\n");
                           }
                           _loc1_++;
                        }
                        if(_loc12_.NewBallID != -1)
                        {
                           _loc13_ = BallManager.findBall(_loc12_.NewBallID);
                           if(_loc13_.isComplete())
                           {
                              _loc2_++;
                           }
                           else
                           {
                              this._unloadedmsg = this._unloadedmsg + ("BallManager.findBall(skill.NewBallID):" + _loc12_.NewBallID + "false\n");
                           }
                           _loc1_++;
                        }
                     }
                  }
               }
            }
         }
         for each(_loc4_ in this._gameInfo.neededPetSkillResource)
         {
            if(_loc4_.effect)
            {
               if(ModuleLoader.hasDefinition(_loc4_.effectClassLink))
               {
                  _loc2_++;
               }
               else
               {
                  this._unloadedmsg = this._unloadedmsg + ("ModuleLoader.hasDefinition(" + _loc4_.effectClassLink + ") false\n");
               }
               _loc1_++;
            }
         }
         _loc5_ = 0;
         while(_loc5_ < this._gameInfo.neededMovies.length)
         {
            if(this._gameInfo.neededMovies[_loc5_].type == 1)
            {
               if(LoadBombManager.Instance.getLivingBombComplete(this._gameInfo.neededMovies[_loc5_].bombId))
               {
                  _loc2_++;
               }
               else
               {
                  this._unloadedmsg = this._unloadedmsg + ("LoadBombManager.getLivingBombComplete(_gameInfo.neededMovies[i].bombId):" + this._gameInfo.neededMovies[_loc5_].bombId + " false\n");
               }
               _loc1_++;
            }
            _loc5_++;
         }
         if(this._gameInfo.loaderMap.completed)
         {
            _loc2_++;
         }
         else
         {
            this._unloadedmsg = this._unloadedmsg + ("_gameInfo.loaderMap.completed false,pic: " + this._gameInfo.loaderMap.info.Pic + "id:" + this._gameInfo.loaderMap.info.ID + "\n");
         }
         _loc1_++;
         if(!StartupResourceLoader.firstEnterHall)
         {
            if(LoadBombManager.Instance.getLoadSpecialBombComplete())
            {
               _loc2_++;
            }
            else
            {
               this._unloadedmsg = this._unloadedmsg + ("LoadBombManager.getLoadSpecialBombComplete() false  " + LoadBombManager.Instance.getUnloadedSpecialBombString() + "\n");
            }
            _loc1_++;
         }
         if(this._trainerLoad)
         {
            if(this._trainerLoad.completed)
            {
               _loc2_++;
            }
            else
            {
               this._unloadedmsg = this._unloadedmsg + "_trainerLoad.completed false\n";
            }
            _loc1_++;
         }
         var _loc6_:Number = int(_loc2_ / _loc1_ * 100);
         var _loc7_:Boolean = _loc1_ == _loc2_;
         if(_loc7_ && (!this.checkAnimationIsFinished() || !this.checkIsEnoughDelayTime()))
         {
            _loc6_ = 99;
            _loc7_ = false;
         }
         GameInSocketOut.sendLoadingProgress(_loc6_);
         RoomManager.Instance.current.selfRoomPlayer.progress = _loc6_;
         return _loc7_;
      }
      
      protected function checkIsEnoughDelayTime() : Boolean
      {
         var _loc1_:Number = new Date().time;
         return _loc1_ - this._delayBeginTime >= DELAY_TIME;
      }
      
      public function dispose() : void
      {
         var _loc1_:RoomPlayer = null;
         var _loc2_:int = 0;
         KeyboardShortcutsManager.Instance.cancelForbidden();
         this._countDownTimer.removeEventListener(TimerEvent.TIMER,this.__countDownTick);
         this._countDownTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,this.__countDownComplete);
         this._countDownTimer.stop();
         this._countDownTimer = null;
         ObjectUtils.disposeObject(this._trainerLoad);
         ObjectUtils.disposeObject(this._bg);
         ObjectUtils.disposeObject(this._chatViewBg);
         ObjectUtils.disposeObject(this._versus);
         ObjectUtils.disposeObject(this._countDownTxt);
         ObjectUtils.disposeObject(this._battleField);
         ObjectUtils.disposeObject(this._tipsItem);
         ObjectUtils.disposeObject(this._viewerItem);
         for each(_loc1_ in this._gameInfo.roomPlayers)
         {
            _loc1_.removeEventListener(RoomPlayerEvent.PROGRESS_CHANGE,this.__onLoadingFinished);
         }
         _loc2_ = 0;
         while(_loc2_ < this._characterItems.length)
         {
            TweenMax.killTweensOf(this._characterItems[_loc2_]);
            this._characterItems[_loc2_].dispose();
            this._characterItems[_loc2_] = null;
            _loc2_++;
         }
         if(this._dungeonMapItem)
         {
            ObjectUtils.disposeObject(this._dungeonMapItem);
            this._dungeonMapItem = null;
         }
         this._characterItems = null;
         this._trainerLoad = null;
         this._bg = null;
         this._chatViewBg = null;
         this._gameInfo = null;
         this._versus = null;
         this._countDownTxt = null;
         this._battleField = null;
         this._tipsItem = null;
         this._countDownTimer = null;
         this._viewerItem = null;
         if(this.blueFlag)
         {
            this.blueFlag.dispose();
            this.blueFlag = null;
         }
         if(this.redFlag)
         {
            this.redFlag.dispose();
            this.redFlag = null;
         }
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
