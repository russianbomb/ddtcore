package roomLoading.view
{
   import com.pickgliss.loader.BaseLoader;
   import com.pickgliss.loader.LoadResourceManager;
   import ddt.data.BallInfo;
   import ddt.loader.MapLoader;
   import ddt.loader.StartupResourceLoader;
   import ddt.loader.TrainerLoader;
   import ddt.manager.BallManager;
   import ddt.manager.LoadBombManager;
   import ddt.manager.MapManager;
   import ddt.manager.PathManager;
   import ddt.manager.PetSkillManager;
   import ddt.manager.PlayerManager;
   import game.model.GameInfo;
   import game.model.GameNeedPetSkillInfo;
   import game.model.Player;
   import im.IMController;
   import pet.date.PetInfo;
   import pet.date.PetSkillTemplateInfo;
   import room.RoomManager;
   import room.events.RoomPlayerEvent;
   import room.model.RoomInfo;
   import room.model.RoomPlayer;
   import trainer.controller.NewHandGuideManager;
   
   public class CampBattleRoomLoadingView extends RoomLoadingView
   {
       
      
      public function CampBattleRoomLoadingView(param1:GameInfo)
      {
         super(param1);
      }
      
      override protected function initLoadingItems() : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:Array = null;
         var _loc5_:int = 0;
         var _loc6_:RoomPlayer = null;
         var _loc7_:RoomPlayer = null;
         var _loc8_:int = 0;
         var _loc9_:RoomPlayer = null;
         var _loc10_:RoomLoadingCharacterItem = null;
         var _loc11_:Player = null;
         var _loc12_:PetInfo = null;
         var _loc13_:int = 0;
         var _loc14_:PetSkillTemplateInfo = null;
         var _loc15_:BallInfo = null;
         var _loc16_:int = 0;
         var _loc17_:GameNeedPetSkillInfo = null;
         var _loc1_:int = _gameInfo.roomPlayers.length;
         _loc4_ = _gameInfo.roomPlayers;
         LoadBombManager.Instance.loadFullRoomPlayersBomb(_loc4_);
         if(!StartupResourceLoader.firstEnterHall)
         {
            LoadBombManager.Instance.loadSpecialBomb();
         }
         for each(_loc6_ in _loc4_)
         {
            if(PlayerManager.Instance.Self.ID == _loc6_.playerInfo.ID)
            {
               _loc5_ = _loc6_.team;
            }
         }
         for each(_loc7_ in _loc4_)
         {
            if(!_loc7_.isViewer)
            {
               if(_loc7_.team == RoomPlayer.BLUE_TEAM)
               {
                  _loc2_++;
               }
               else
               {
                  _loc3_++;
               }
               if(!(RoomManager.Instance.current.type == RoomInfo.FREE_MODE && _loc7_.team != _loc5_))
               {
                  IMController.Instance.saveRecentContactsID(_loc7_.playerInfo.ID);
               }
            }
         }
         _loc8_ = 0;
         while(_loc8_ < _loc1_)
         {
            _loc9_ = _gameInfo.roomPlayers[_loc8_];
            _loc9_.addEventListener(RoomPlayerEvent.PROGRESS_CHANGE,__onLoadingFinished);
            if(_loc9_.isViewer)
            {
               if(contains(_tipsItem))
               {
                  removeChild(_tipsItem);
               }
               addChild(_viewerItem);
            }
            else
            {
               _loc10_ = new RoomLoadingCharacterItem(_loc9_);
               initRoomItem(_loc10_);
               _loc11_ = _gameInfo.findLivingByPlayerID(_loc9_.playerInfo.ID,_loc9_.playerInfo.ZoneID);
               initCharacter(_loc11_,_loc10_);
               _loc12_ = _loc11_.playerInfo.currentPet;
               if(_loc12_)
               {
                  LoadResourceManager.Instance.creatAndStartLoad(PathManager.solvePetGameAssetUrl(_loc12_.GameAssetUrl),BaseLoader.MODULE_LOADER);
                  for each(_loc13_ in _loc12_.equipdSkills)
                  {
                     if(_loc13_ > 0)
                     {
                        _loc14_ = PetSkillManager.getSkillByID(_loc13_);
                        if(_loc14_.EffectPic)
                        {
                           LoadResourceManager.Instance.creatAndStartLoad(PathManager.solvePetSkillEffect(_loc14_.EffectPic),BaseLoader.MODULE_LOADER);
                        }
                        if(_loc14_.NewBallID != -1)
                        {
                           _loc15_ = BallManager.findBall(_loc14_.NewBallID);
                           _loc15_.loadBombAsset();
                           _loc15_.loadCraterBitmap();
                        }
                     }
                  }
               }
            }
            _loc8_++;
         }
         if(blueBig)
         {
            addChild(blueBig);
         }
         if(redBig)
         {
            addChild(redBig);
         }
         if(!StartupResourceLoader.firstEnterHall)
         {
            _loc16_ = 0;
            while(_loc16_ < _gameInfo.neededMovies.length)
            {
               if(_gameInfo.neededMovies[_loc16_].type == 1)
               {
                  _gameInfo.neededMovies[_loc16_].startLoad();
               }
               _loc16_++;
            }
            for each(_loc17_ in _gameInfo.neededPetSkillResource)
            {
               _loc17_.startLoad();
            }
         }
         _gameInfo.loaderMap = new MapLoader(MapManager.getMapInfo(_gameInfo.mapIndex));
         _gameInfo.loaderMap.load();
         switch(NewHandGuideManager.Instance.mapID)
         {
            case 111:
               _trainerLoad = new TrainerLoader("1");
               break;
            case 112:
               _trainerLoad = new TrainerLoader("2");
               break;
            case 113:
               _trainerLoad = new TrainerLoader("3");
               break;
            case 114:
               _trainerLoad = new TrainerLoader("4");
               break;
            case 115:
               _trainerLoad = new TrainerLoader("5");
               break;
            case 116:
               _trainerLoad = new TrainerLoader("6");
         }
         if(_trainerLoad)
         {
            _trainerLoad.load();
         }
      }
   }
}
