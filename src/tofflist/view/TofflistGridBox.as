package tofflist.view
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.image.MutipleImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.LanguageMgr;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.geom.Point;
   import flash.utils.Dictionary;
   import tofflist.data.TofflistLayoutInfo;
   
   public class TofflistGridBox extends Sprite implements Disposeable
   {
      
      private static const RANK:String = LanguageMgr.GetTranslation("repute");
      
      private static const NAME:String = LanguageMgr.GetTranslation("civil.rightview.listname");
      
      private static const BATTLE:String = LanguageMgr.GetTranslation("tank.menu.FightPoweTxt");
      
      private static const LEVEL:String = LanguageMgr.GetTranslation("tank.menu.LevelTxt1");
      
      private static const EXP:String = LanguageMgr.GetTranslation("exp");
      
      private static const CHARM_LEVEL:String = LanguageMgr.GetTranslation("tofflist.charmLevel");
      
      private static const CHARM_VALUE:String = LanguageMgr.GetTranslation("ddt.giftSystem.GiftGoodItem.charmNum");
      
      private static const SCORE:String = LanguageMgr.GetTranslation("tofflist.battleScore");
      
      private static const ACHIVE_POINT:String = LanguageMgr.GetTranslation("tofflist.achivepoint");
      
      private static const ASSET:String = LanguageMgr.GetTranslation("consortia.Money1");
      
      private static const TOTAL_ASSET:String = LanguageMgr.GetTranslation("tofflist.totalasset");
      
      private static const SERVER:String = LanguageMgr.GetTranslation("tofflist.server");
       
      
      private var _bg:MutipleImage;
      
      private var _titleBg:MutipleImage;
      
      private var _layoutInfoArr:Dictionary;
      
      private var _title:Sprite;
      
      private var _orderList:TofflistOrderList;
      
      private var _id:String;
      
      public function TofflistGridBox()
      {
         super();
         this._layoutInfoArr = new Dictionary();
         this.initData();
         this._bg = ComponentFactory.Instance.creatComponentByStylename("tofflist.right.listBg");
         addChild(this._bg);
         this._title = new Sprite();
         this._title.y = 2;
         addChild(this._title);
         this._orderList = new TofflistOrderList();
         PositionUtils.setPos(this._orderList,"tofflist.orderlistPos");
         addChild(this._orderList);
      }
      
      private function initData() : void
      {
         var _loc1_:TofflistLayoutInfo = ComponentFactory.Instance.creatCustomObject("person_local_battle");
         _loc1_.TitleTextString = [RANK,NAME,BATTLE];
         this._layoutInfoArr[TofflistThirdClassMenu.PERSON_LOCAL_BATTLE] = _loc1_;
         var _loc2_:TofflistLayoutInfo = ComponentFactory.Instance.creatCustomObject("person_local_level");
         _loc2_.TitleTextString = [RANK,NAME,LEVEL,EXP];
         this._layoutInfoArr[TofflistThirdClassMenu.PERSON_LOCAL_LEVEL] = _loc2_;
         var _loc3_:TofflistLayoutInfo = ComponentFactory.Instance.creatCustomObject("person_local_achive");
         _loc3_.TitleTextString = [RANK,NAME,ACHIVE_POINT];
         this._layoutInfoArr[TofflistThirdClassMenu.PERSON_LOCAL_ACHIVE] = _loc3_;
         var _loc4_:TofflistLayoutInfo = ComponentFactory.Instance.creatCustomObject("person_local_charm");
         _loc4_.TitleTextString = [RANK,NAME,CHARM_LEVEL,CHARM_VALUE];
         this._layoutInfoArr[TofflistThirdClassMenu.PERSON_LOCAL_CHARM] = _loc4_;
         var _loc5_:TofflistLayoutInfo = ComponentFactory.Instance.creatCustomObject("person_local_match");
         _loc5_.TitleTextString = [RANK,NAME,SCORE];
         this._layoutInfoArr[TofflistThirdClassMenu.PERSON_LOCAL_MATCH] = _loc5_;
         var _loc6_:TofflistLayoutInfo = ComponentFactory.Instance.creatCustomObject("person_cross_battle");
         _loc6_.TitleTextString = [RANK,NAME,SERVER,BATTLE];
         this._layoutInfoArr[TofflistThirdClassMenu.PERSON_CROSS_BATTLE] = _loc6_;
         var _loc7_:TofflistLayoutInfo = ComponentFactory.Instance.creatCustomObject("person_cross_level");
         _loc7_.TitleTextString = [RANK,NAME,LEVEL,SERVER,EXP];
         this._layoutInfoArr[TofflistThirdClassMenu.PERSON_CROSS_LEVEL] = _loc7_;
         var _loc8_:TofflistLayoutInfo = ComponentFactory.Instance.creatCustomObject("person_cross_achive");
         _loc8_.TitleTextString = [RANK,NAME,SERVER,ACHIVE_POINT];
         this._layoutInfoArr[TofflistThirdClassMenu.PERSON_CROSS_ACHIVE] = _loc8_;
         var _loc9_:TofflistLayoutInfo = ComponentFactory.Instance.creatCustomObject("person_cross_charm");
         _loc9_.TitleTextString = [RANK,NAME,CHARM_LEVEL,SERVER,CHARM_VALUE];
         this._layoutInfoArr[TofflistThirdClassMenu.PERSON_CROSS_CHARM] = _loc9_;
         var _loc10_:TofflistLayoutInfo = ComponentFactory.Instance.creatCustomObject("consortia_local_battle");
         _loc10_.TitleTextString = [RANK,NAME,BATTLE];
         this._layoutInfoArr[TofflistThirdClassMenu.CONSORTIA_LOCAL_BATTLE] = _loc10_;
         var _loc11_:TofflistLayoutInfo = ComponentFactory.Instance.creatCustomObject("consortia_local_level");
         _loc11_.TitleTextString = [RANK,NAME,LEVEL,ASSET];
         this._layoutInfoArr[TofflistThirdClassMenu.CONSORTIA_LOCAL_LEVEL] = _loc11_;
         var _loc12_:TofflistLayoutInfo = ComponentFactory.Instance.creatCustomObject("consortia_local_asset");
         _loc12_.TitleTextString = [RANK,NAME,TOTAL_ASSET];
         this._layoutInfoArr[TofflistThirdClassMenu.CONSORTIA_LOCAL_ASSET] = _loc12_;
         var _loc13_:TofflistLayoutInfo = ComponentFactory.Instance.creatCustomObject("consortia_local_charm");
         _loc13_.TitleTextString = [RANK,NAME,CHARM_VALUE];
         this._layoutInfoArr[TofflistThirdClassMenu.CONSORTIA_LOCAL_CHARM] = _loc13_;
         var _loc14_:TofflistLayoutInfo = ComponentFactory.Instance.creatCustomObject("consortia_cross_battle");
         _loc14_.TitleTextString = [RANK,NAME,SERVER,BATTLE];
         this._layoutInfoArr[TofflistThirdClassMenu.CONSORTIA_CROSS_BATTLE] = _loc14_;
         var _loc15_:TofflistLayoutInfo = ComponentFactory.Instance.creatCustomObject("consortia_cross_level");
         _loc15_.TitleTextString = [RANK,NAME,LEVEL,SERVER,ASSET];
         this._layoutInfoArr[TofflistThirdClassMenu.CONSORTIA_CROSS_LEVEL] = _loc15_;
         var _loc16_:TofflistLayoutInfo = ComponentFactory.Instance.creatCustomObject("consortia_cross_asset");
         _loc16_.TitleTextString = [RANK,NAME,SERVER,TOTAL_ASSET];
         this._layoutInfoArr[TofflistThirdClassMenu.CONSORTIA_CROSS_ASSET] = _loc16_;
         var _loc17_:TofflistLayoutInfo = ComponentFactory.Instance.creatCustomObject("consortia_cross_charm");
         _loc17_.TitleTextString = [RANK,NAME,SERVER,CHARM_VALUE];
         this._layoutInfoArr[TofflistThirdClassMenu.CONSORTIA_CROSS_CHARM] = _loc17_;
      }
      
      public function get orderList() : TofflistOrderList
      {
         return this._orderList;
      }
      
      public function updateList(param1:Array, param2:int = 1) : void
      {
         var _loc3_:TofflistLayoutInfo = null;
         this._orderList.items(param1,param2);
         if(this._id)
         {
            _loc3_ = this._layoutInfoArr[this._id];
            this._orderList.showHline(_loc3_.TitleHLinePoint);
         }
      }
      
      public function dispose() : void
      {
      }
      
      public function updateStyleXY(param1:String) : void
      {
         var _loc3_:Point = null;
         var _loc4_:int = 0;
         var _loc5_:Bitmap = null;
         var _loc6_:FilterFrameText = null;
         this._id = param1;
         ObjectUtils.disposeAllChildren(this._title);
         var _loc2_:TofflistLayoutInfo = this._layoutInfoArr[param1];
         for each(_loc3_ in _loc2_.TitleHLinePoint)
         {
            _loc5_ = ComponentFactory.Instance.creatBitmap("asset.corel.formLineBig");
            PositionUtils.setPos(_loc5_,_loc3_);
            this._title.addChild(_loc5_);
         }
         _loc4_ = 0;
         while(_loc4_ < _loc2_.TitleTextPoint.length)
         {
            _loc6_ = ComponentFactory.Instance.creatComponentByStylename("toffilist.listTitleText");
            PositionUtils.setPos(_loc6_,_loc2_.TitleTextPoint[_loc4_]);
            _loc6_.text = _loc2_.TitleTextString[_loc4_];
            this._title.addChild(_loc6_);
            _loc4_++;
         }
      }
   }
}
