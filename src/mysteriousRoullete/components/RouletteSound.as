package mysteriousRoullete.components
{
   import ddt.manager.SoundManager;
   
   public class RouletteSound
   {
       
      
      private var _oneArray:Array;
      
      private var _number:int = 0;
      
      public function RouletteSound()
      {
         this._oneArray = ["130","131","133","132","135","134","129","128","127","132","135","134","129","128","127"];
         super();
      }
      
      public function playOneStep() : void
      {
         var _loc1_:String = this._oneArray[this._number];
         SoundManager.instance.play(_loc1_);
         this._number = this._number >= 4?int(0):int(this._number + 1);
      }
   }
}
