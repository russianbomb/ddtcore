package mysteriousRoullete
{
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.UIModuleTypes;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.view.TransactionsFrame;
   import ddt.view.UIModuleSmallLoading;
   import flash.display.Sprite;
   import flash.events.EventDispatcher;
   import flash.events.MouseEvent;
   import hallIcon.HallIconManager;
   import hallIcon.HallIconType;
   import mysteriousRoullete.view.MysteriousActivityView;
   import mysteriousRoullete.view.MysteriousShopView;
   import road7th.comm.PackageIn;
   import wonderfulActivity.WonderfulActivityManager;
   
   public class MysteriousManager extends EventDispatcher
   {
      
      private static var _instance:MysteriousManager;
       
      
      private var _transactionsFrame:TransactionsFrame;
      
      public var lotteryTimes:int;
      
      public var freeGetTimes:int;
      
      public var discountBuyTimes:int;
      
      public var startTime:Date;
      
      public var endTime:Date;
      
      public var mysteriousView:MysteriousActivityView;
      
      private var _func:Function;
      
      private var _funcParams:Array;
      
      public var viewType:int;
      
      public var selectIndex:int = -1;
      
      public var isMysteriousClose:Boolean = true;
      
      private var _mask:Sprite;
      
      public function MysteriousManager()
      {
         super();
      }
      
      public static function get instance() : MysteriousManager
      {
         if(!_instance)
         {
            _instance = new MysteriousManager();
         }
         return _instance;
      }
      
      public function setup() : void
      {
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GET_MYSTERIOUS_DATA,this.__getMysteriousData);
      }
      
      private function __getMysteriousData(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         this.lotteryTimes = _loc2_.readInt();
         this.freeGetTimes = _loc2_.readInt();
         this.discountBuyTimes = _loc2_.readInt();
         this.startTime = _loc2_.readDate();
         this.endTime = _loc2_.readDate();
         if(this.lotteryTimes <= 0)
         {
            if(this.mysteriousView && this.mysteriousView.view && this.mysteriousView.type == MysteriousActivityView.TYPE_SHOP)
            {
               (this.mysteriousView.view as MysteriousShopView).setTimes();
               if(this.freeGetTimes == 0 && this.discountBuyTimes == 0)
               {
                  this.isMysteriousClose = true;
               }
               return;
            }
            if(this.freeGetTimes == 0 && this.discountBuyTimes == 0)
            {
               if(WonderfulActivityManager.Instance.frame)
               {
                  this.isMysteriousClose = true;
               }
               else
               {
                  HallIconManager.instance.updateSwitchHandler(HallIconType.MYSTERIOUROULETTE,false);
               }
               return;
            }
            this.viewType = MysteriousActivityView.TYPE_SHOP;
         }
         else
         {
            this.viewType = MysteriousActivityView.TYPE_ROULETTE;
         }
         this.showIcon();
      }
      
      public function showIcon() : void
      {
         this.isMysteriousClose = false;
         HallIconManager.instance.updateSwitchHandler(HallIconType.MYSTERIOUROULETTE,true);
      }
      
      public function showFrame() : void
      {
         var _loc1_:MysteriousActivityView = null;
         SoundManager.instance.play("008");
         _loc1_ = new MysteriousActivityView();
         _loc1_.init();
         _loc1_.x = -227;
         HallIconManager.instance.showCommonFrame(_loc1_,"wonderfulActivityManager.btnTxt14");
      }
      
      public function loadMysteriousRouletteModule(param1:Function = null, param2:Array = null) : void
      {
         this._func = param1;
         this._funcParams = param2;
         UIModuleSmallLoading.Instance.progress = 0;
         UIModuleSmallLoading.Instance.show();
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadCompleteHandler);
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUimoduleLoadProgress);
         UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.MYSTERIOUS_ROULETTE);
      }
      
      private function loadCompleteHandler(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.MYSTERIOUS_ROULETTE)
         {
            UIModuleSmallLoading.Instance.hide();
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadCompleteHandler);
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUimoduleLoadProgress);
            if(null != this._func)
            {
               this._func.apply(null,this._funcParams);
            }
            this._func = null;
            this._funcParams = null;
         }
      }
      
      private function onUimoduleLoadProgress(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.MYSTERIOUS_ROULETTE)
         {
            UIModuleSmallLoading.Instance.progress = param1.loader.progress * 100;
         }
      }
      
      public function setView(param1:MysteriousActivityView) : void
      {
         this.mysteriousView = param1;
      }
      
      public function addMask() : void
      {
         if(this._mask == null)
         {
            this._mask = new Sprite();
            this._mask.graphics.beginFill(0,0);
            this._mask.graphics.drawRect(0,0,2000,1200);
            this._mask.graphics.endFill();
            this._mask.addEventListener(MouseEvent.CLICK,this.onMaskClick);
         }
         LayerManager.Instance.addToLayer(this._mask,LayerManager.GAME_TOP_LAYER,false,LayerManager.ALPHA_BLOCKGOUND);
      }
      
      public function removeMask() : void
      {
         if(this._mask != null)
         {
            this._mask.parent.removeChild(this._mask);
            this._mask.removeEventListener(MouseEvent.CLICK,this.onMaskClick);
            this._mask = null;
         }
      }
      
      private function onMaskClick(param1:MouseEvent) : void
      {
         MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("mysteriousRoulette.running"));
      }
      
      public function dispose() : void
      {
         this.mysteriousView = null;
         if(this._transactionsFrame)
         {
            ObjectUtils.disposeObject(this._transactionsFrame);
         }
         this._transactionsFrame = null;
      }
   }
}
