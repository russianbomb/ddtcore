package mysteriousRoullete.view
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.ScrollPanel;
   import com.pickgliss.ui.controls.container.SimpleTileList;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.ShopItemInfo;
   import ddt.manager.ShopManager;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import mysteriousRoullete.MysteriousManager;
   
   public class MysteriousShopView extends Sprite implements Disposeable
   {
      
      private static const NUMBER:int = 2;
      
      private static const LENGTH:int = 8;
       
      
      private var _shopTitle:Bitmap;
      
      private var _freeBG:Bitmap;
      
      private var _discountBG:Bitmap;
      
      private var _freeCount:FilterFrameText;
      
      private var _discountCount:FilterFrameText;
      
      private var _freePanel:ScrollPanel;
      
      private var _discontPanel:ScrollPanel;
      
      private var _freeItemList:SimpleTileList;
      
      private var _freeItemArr:Array;
      
      private var _discountItemList:SimpleTileList;
      
      private var _discountItemArr:Array;
      
      public function MysteriousShopView()
      {
         super();
         this._freeItemArr = [];
         this._discountItemArr = [];
         this.initView();
         this.initData();
      }
      
      public function initView() : void
      {
         this._shopTitle = ComponentFactory.Instance.creat("mysteriousRoulette.shopTitle");
         addChild(this._shopTitle);
         this._freeBG = ComponentFactory.Instance.creat("mysteriousRoulette.freeBG");
         addChild(this._freeBG);
         this._discountBG = ComponentFactory.Instance.creat("mysteriousRoulette.discountBG");
         addChild(this._discountBG);
         this._freeCount = ComponentFactory.Instance.creat("mysteriousRoulette.freeCount");
         this._freeCount.text = "0";
         addChild(this._freeCount);
         this._discountCount = ComponentFactory.Instance.creat("mysteriousRoulette.discountCount");
         this._discountCount.text = "0";
         addChild(this._discountCount);
         this._freePanel = ComponentFactory.Instance.creat("mysteriousRoulette.freePanel");
         addChild(this._freePanel);
         this._freeItemList = ComponentFactory.Instance.creatCustomObject("mysteriousRoulette.freeItemList",[NUMBER]);
         this._freePanel.setView(this._freeItemList);
         this._discontPanel = ComponentFactory.Instance.creat("mysteriousRoulette.discountPanel");
         addChild(this._discontPanel);
         this._discountItemList = ComponentFactory.Instance.creatCustomObject("mysteriousRoulette.discountItemList",[NUMBER]);
         this._discontPanel.setView(this._discountItemList);
      }
      
      private function initData() : void
      {
         var _loc5_:MysteriousShopItem = null;
         var _loc6_:MysteriousShopItem = null;
         var _loc1_:Vector.<ShopItemInfo> = ShopManager.Instance.getGoodsByType(96);
         var _loc2_:int = 0;
         while(_loc2_ <= _loc1_.length - 1)
         {
            _loc5_ = new MysteriousShopItem(MysteriousShopItem.TYPE_FREE);
            _loc5_.shopItemInfo = _loc1_[_loc2_];
            this._freeItemList.addChild(_loc5_);
            this._freeItemArr.push(_loc5_);
            _loc2_++;
         }
         var _loc3_:Vector.<ShopItemInfo> = ShopManager.Instance.getGoodsByType(97);
         var _loc4_:int = 0;
         while(_loc4_ <= _loc3_.length - 1)
         {
            _loc6_ = new MysteriousShopItem(MysteriousShopItem.TYPE_DISCOUNT);
            _loc6_.shopItemInfo = _loc3_[_loc4_];
            this._discountItemList.addChild(_loc6_);
            this._discountItemArr.push(_loc6_);
            _loc4_++;
         }
         this.setTimes();
      }
      
      public function setTimes() : void
      {
         var _loc1_:int = MysteriousManager.instance.freeGetTimes;
         var _loc2_:int = MysteriousManager.instance.discountBuyTimes;
         this._freeCount.text = _loc1_.toString();
         this._discountCount.text = _loc2_.toString();
         var _loc3_:Boolean = _loc1_ == 0?Boolean(true):Boolean(false);
         var _loc4_:Boolean = _loc2_ == 0?Boolean(true):Boolean(false);
         var _loc5_:int = 0;
         while(_loc5_ <= this._freeItemArr.length - 1)
         {
            (this._freeItemArr[_loc5_] as MysteriousShopItem).turnGray(_loc3_);
            _loc5_++;
         }
         var _loc6_:int = 0;
         while(_loc6_ <= this._discountItemArr.length - 1)
         {
            (this._discountItemArr[_loc6_] as MysteriousShopItem).turnGray(_loc4_);
            _loc6_++;
         }
      }
      
      public function dispose() : void
      {
         if(this._shopTitle)
         {
            ObjectUtils.disposeObject(this._shopTitle);
         }
         this._shopTitle = null;
         if(this._freeBG)
         {
            ObjectUtils.disposeObject(this._freeBG);
         }
         this._freeBG = null;
         if(this._discountBG)
         {
            ObjectUtils.disposeObject(this._discountBG);
         }
         this._discountBG = null;
         if(this._freeCount)
         {
            ObjectUtils.disposeObject(this._freeCount);
         }
         this._freeCount = null;
         if(this._discountCount)
         {
            ObjectUtils.disposeObject(this._discountCount);
         }
         this._discountCount = null;
         if(this._freePanel)
         {
            ObjectUtils.disposeObject(this._freePanel);
         }
         this._freePanel = null;
         if(this._discontPanel)
         {
            ObjectUtils.disposeObject(this._discontPanel);
         }
         this._discontPanel = null;
         if(this._freeItemList)
         {
            ObjectUtils.disposeObject(this._freeItemList);
         }
         this._freeItemList = null;
         if(this._discountItemList)
         {
            ObjectUtils.disposeObject(this._discountItemList);
         }
         this._discountItemList = null;
      }
   }
}
