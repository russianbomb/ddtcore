package mysteriousRoullete.view
{
   import baglocked.BaglockedManager;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.ShopItemInfo;
   import ddt.manager.LanguageMgr;
   import ddt.manager.LeavePageManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import flash.events.MouseEvent;
   import shop.view.ShopGoodItem;
   
   public class MysteriousShopItem extends ShopGoodItem
   {
      
      public static const TYPE_FREE:int = 0;
      
      public static const TYPE_DISCOUNT:int = 1;
       
      
      private var _itemCount:FilterFrameText;
      
      private var _getBtn:SimpleBitmapButton;
      
      private var _buyBtn:SimpleBitmapButton;
      
      private var type:int = 1;
      
      private var _alertFrame:BaseAlerFrame;
      
      private var price:int;
      
      public function MysteriousShopItem(param1:int)
      {
         this.type = param1;
         super();
      }
      
      override protected function initContent() : void
      {
         super.initContent();
         removeChild(_payPaneaskBtn);
         removeChild(_payPaneBuyBtn);
         removeChild(_payPaneGivingBtn);
         _shopItemCellTypeBg.parent.removeChild(_shopItemCellTypeBg);
         this._itemCount = ComponentFactory.Instance.creatComponentByStylename("mysteriousRoulette.itemCount");
         this._itemCount.text = "100";
         addChild(this._itemCount);
         switch(this.type)
         {
            case TYPE_FREE:
               this._getBtn = ComponentFactory.Instance.creatComponentByStylename("mysteriousRoulette.getBtn");
               this._getBtn.addEventListener(MouseEvent.CLICK,this.__getBtnClick);
               addChild(this._getBtn);
               removeChild(_payType);
               break;
            case TYPE_DISCOUNT:
               this._buyBtn = ComponentFactory.Instance.creatComponentByStylename("mysteriousRoulette.buyBtn");
               this._buyBtn.addEventListener(MouseEvent.CLICK,this.__buyBtnClick);
               addChild(this._buyBtn);
         }
      }
      
      override public function set shopItemInfo(param1:ShopItemInfo) : void
      {
         super.shopItemInfo = param1;
         if(_shopItemInfo.BuyType == 1)
         {
            this._itemCount.text = _shopItemInfo.AUnit.toString();
         }
         else
         {
            this._itemCount.text = "";
         }
      }
      
      private function __getBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         this._alertFrame = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("mysteriousRoulette.ensureGet"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),false,false,true,LayerManager.ALPHA_BLOCKGOUND);
         this._alertFrame.addEventListener(FrameEvent.RESPONSE,this.__alertResponseHandler);
      }
      
      private function __buyBtnClick(param1:MouseEvent) : void
      {
         this.price = shopItemInfo.getItemPrice(1).moneyValue;
         SoundManager.instance.play("008");
         var _loc2_:BaseAlerFrame = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("tips"),LanguageMgr.GetTranslation("mysteriousRoulette.ensureBuy",this.price),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),false,true,false,LayerManager.ALPHA_BLOCKGOUND,null,"SimpleAlert",60,false);
         _loc2_.addEventListener(FrameEvent.RESPONSE,this.__alertBuyGoods);
      }
      
      protected function __alertBuyGoods(param1:FrameEvent) : void
      {
         var _loc3_:BaseAlerFrame = null;
         var _loc2_:BaseAlerFrame = param1.currentTarget as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.__alertBuyGoods);
         switch(param1.responseCode)
         {
            case FrameEvent.SUBMIT_CLICK:
            case FrameEvent.ENTER_CLICK:
               if(PlayerManager.Instance.Self.bagLocked)
               {
                  BaglockedManager.Instance.show();
                  _loc2_.dispose();
                  return;
               }
               if(_loc2_.isBand)
               {
                  if(PlayerManager.Instance.Self.BandMoney < this.price)
                  {
                     _loc2_.dispose();
                     _loc3_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("buried.alertInfo.noBindMoney"),"",LanguageMgr.GetTranslation("cancel"),true,false,false,2);
                     _loc3_.addEventListener(FrameEvent.RESPONSE,this.onResponseHander);
                     return;
                  }
               }
               else if(PlayerManager.Instance.Self.Money < this.price)
               {
                  _loc2_.dispose();
                  _loc3_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("tank.room.RoomIIView2.notenoughmoney.title"),LanguageMgr.GetTranslation("tank.room.RoomIIView2.notenoughmoney.content"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),false,true,true,LayerManager.ALPHA_BLOCKGOUND);
                  _loc3_.addEventListener(FrameEvent.RESPONSE,this._response);
                  return;
               }
               this.buy(_loc2_.isBand);
               break;
         }
         _loc2_.dispose();
      }
      
      private function onResponseHander(param1:FrameEvent) : void
      {
         var _loc2_:BaseAlerFrame = null;
         (param1.currentTarget as BaseAlerFrame).removeEventListener(FrameEvent.RESPONSE,this.onResponseHander);
         if(param1.responseCode == FrameEvent.ENTER_CLICK || param1.responseCode == FrameEvent.SUBMIT_CLICK)
         {
            if(PlayerManager.Instance.Self.Money < this.price)
            {
               _loc2_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("tank.room.RoomIIView2.notenoughmoney.title"),LanguageMgr.GetTranslation("tank.room.RoomIIView2.notenoughmoney.content"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),false,true,true,LayerManager.ALPHA_BLOCKGOUND);
               _loc2_.addEventListener(FrameEvent.RESPONSE,this._response);
               return;
            }
            this.buy(false);
         }
         param1.currentTarget.dispose();
      }
      
      private function _response(param1:FrameEvent) : void
      {
         (param1.currentTarget as BaseAlerFrame).removeEventListener(FrameEvent.RESPONSE,this._response);
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            LeavePageManager.leaveToFillPath();
         }
         ObjectUtils.disposeObject(param1.currentTarget);
      }
      
      private function __alertResponseHandler(param1:FrameEvent) : void
      {
         this._alertFrame.removeEventListener(FrameEvent.RESPONSE,this.__alertResponseHandler);
         this._alertFrame.dispose();
         this._alertFrame = null;
         switch(param1.responseCode)
         {
            case FrameEvent.ENTER_CLICK:
            case FrameEvent.SUBMIT_CLICK:
               if(PlayerManager.Instance.Self.bagLocked)
               {
                  BaglockedManager.Instance.show();
                  return;
               }
               this.buy();
               break;
         }
      }
      
      private function buy(param1:Boolean = false) : void
      {
         var _loc2_:Array = new Array();
         var _loc3_:Array = new Array();
         var _loc4_:Array = new Array();
         var _loc5_:Array = new Array();
         var _loc6_:Array = new Array();
         var _loc7_:Array = [];
         var _loc8_:Array = [];
         _loc2_.push(shopItemInfo.GoodsID);
         _loc3_.push(1);
         _loc4_.push("");
         _loc5_.push("");
         _loc6_.push("");
         _loc7_.push(1);
         _loc8_.push(param1);
         SocketManager.Instance.out.sendBuyGoods(_loc2_,_loc3_,_loc4_,_loc6_,_loc5_,null,0,_loc7_,_loc8_);
      }
      
      public function turnGray(param1:Boolean = false) : void
      {
         if(this._buyBtn)
         {
            this._buyBtn.enable = !param1;
         }
         if(this._getBtn)
         {
            this._getBtn.enable = !param1;
         }
      }
      
      override protected function removeEvent() : void
      {
         if(this._getBtn)
         {
            this._getBtn.removeEventListener(MouseEvent.CLICK,this.__getBtnClick);
         }
         if(this._buyBtn)
         {
            this._buyBtn.removeEventListener(MouseEvent.CLICK,this.__buyBtnClick);
         }
      }
      
      override public function dispose() : void
      {
         this.removeEvent();
         ObjectUtils.disposeObject(this._itemCount);
         this._itemCount = null;
         ObjectUtils.disposeObject(this._getBtn);
         this._getBtn = null;
         ObjectUtils.disposeObject(this._buyBtn);
         this._buyBtn = null;
         super.dispose();
      }
   }
}
