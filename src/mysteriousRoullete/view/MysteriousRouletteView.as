package mysteriousRoullete.view
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import mysteriousRoullete.MysteriousManager;
   import mysteriousRoullete.components.RouletteRun;
   import mysteriousRoullete.event.MysteriousEvent;
   import road7th.comm.PackageIn;
   import wonderfulActivity.WonderfulActivityManager;
   
   public class MysteriousRouletteView extends Sprite implements Disposeable
   {
       
      
      private var _rouletteTitle:Bitmap;
      
      private var _description:Bitmap;
      
      private var _timeBG:Bitmap;
      
      private var _roullete:Bitmap;
      
      private var _gear:Bitmap;
      
      private var _startBtn:BaseButton;
      
      private var _shopBtn:BaseButton;
      
      private var _dateTxt:FilterFrameText;
      
      private var _rouletteRun:RouletteRun;
      
      private var _lightUnstart:MovieClip;
      
      private var _lightStart:MovieClip;
      
      private var _endBtnLight:MovieClip;
      
      private var _startBtnLight:MovieClip;
      
      private var selectedIndex:int;
      
      public function MysteriousRouletteView()
      {
         super();
         this.initView();
         this.initEvent();
         this.initData();
      }
      
      public function initView() : void
      {
         this._rouletteTitle = ComponentFactory.Instance.creat("mysteriousRoulette.rouletteTitle");
         addChild(this._rouletteTitle);
         this._description = ComponentFactory.Instance.creat("mysteriousRoulette.desctiption");
         addChild(this._description);
         this._roullete = ComponentFactory.Instance.creat("mysteriousRoulette.roullete");
         addChild(this._roullete);
         this._timeBG = ComponentFactory.Instance.creat("mysteriousRoulette.timeBG");
         addChild(this._timeBG);
         this._dateTxt = ComponentFactory.Instance.creat("mysteriousRoulette.dateTxt");
         this._dateTxt.text = "2013-09-26";
         addChild(this._dateTxt);
         this._rouletteRun = new RouletteRun();
         addChild(this._rouletteRun);
         this._gear = ComponentFactory.Instance.creat("mysteriousRoulette.gear");
         addChild(this._gear);
         this._startBtn = ComponentFactory.Instance.creat("mysteriousRoulette.startBtn");
         addChild(this._startBtn);
         this._shopBtn = ComponentFactory.Instance.creat("mysteriousRoulette.shopBtn");
         addChild(this._shopBtn);
         this._shopBtn.visible = false;
         this._lightUnstart = ComponentFactory.Instance.creat("mysteriousRoulette.mc.6LightUnstart");
         PositionUtils.setPos(this._lightUnstart,"mysteriousRoulette.mc.6LightUnstartPos");
         addChild(this._lightUnstart);
         this._lightStart = ComponentFactory.Instance.creat("mysteriousRoulette.mc.6Lightstart");
         PositionUtils.setPos(this._lightStart,"mysteriousRoulette.mc.6LightstartPos");
         addChild(this._lightStart);
         this._lightStart.visible = false;
         this._endBtnLight = ComponentFactory.Instance.creat("mysteriousRoulette.mc.endBtnLight");
         PositionUtils.setPos(this._endBtnLight,"mysteriousRoulette.mc.endBtnLightPos");
         addChild(this._endBtnLight);
         this._endBtnLight.visible = false;
         this._startBtnLight = ComponentFactory.Instance.creat("mysteriousRoulette.mc.startBtnLight");
         PositionUtils.setPos(this._startBtnLight,"mysteriousRoulette.mc.startBtnLightPos");
         addChild(this._startBtnLight);
         this._startBtnLight.visible = false;
         var _loc1_:int = MysteriousManager.instance.selectIndex;
         if(_loc1_ > 0)
         {
            this._rouletteRun.select(_loc1_);
            this._startBtn.mouseEnabled = false;
         }
      }
      
      private function initEvent() : void
      {
         this._startBtn.addEventListener(MouseEvent.MOUSE_DOWN,this.onStartBtnDown);
         this._shopBtn.addEventListener(MouseEvent.CLICK,this.onShopBtnClick);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.DAWN_LOTTERY,this.__dawnLottery);
      }
      
      private function initData() : void
      {
         var _loc1_:Date = MysteriousManager.instance.startTime;
         var _loc2_:Date = MysteriousManager.instance.endTime;
         this._dateTxt.text = _loc1_.getFullYear() + "." + (_loc1_.getMonth() + 1) + "." + _loc1_.getDate() + "-" + _loc2_.getFullYear() + "." + (_loc2_.getMonth() + 1) + "." + _loc2_.getDate();
      }
      
      private function onStartBtnDown(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         this._startBtn.mouseEnabled = false;
         this._startBtnLight.visible = true;
         this._startBtnLight.gotoAndStop(1);
         this._startBtnLight.play();
         this._startBtnLight.addEventListener(Event.ENTER_FRAME,this.onEnterFrame);
         MysteriousManager.instance.addMask();
         WonderfulActivityManager.Instance.frameCanClose = false;
         SocketManager.Instance.out.sendRouletteRun();
      }
      
      protected function onRouletteRunComplete(param1:Event) : void
      {
         this._rouletteRun.removeEventListener(Event.COMPLETE,this.onRouletteRunComplete);
         this._endBtnLight.visible = true;
         this._endBtnLight.gotoAndStop(1);
         this._endBtnLight.play();
         this._endBtnLight.addEventListener(Event.ENTER_FRAME,this.onEnterFrame);
         this._lightStart.visible = false;
         this._lightUnstart.visible = true;
         SoundManager.instance.playMusic("140");
         SoundManager.instance.play("126");
         MysteriousManager.instance.removeMask();
         WonderfulActivityManager.Instance.frameCanClose = true;
         if(this.selectedIndex != 4)
         {
            this._startBtn.visible = false;
            this._shopBtn.visible = true;
         }
         MysteriousManager.instance.selectIndex = this.selectedIndex;
      }
      
      private function onShopBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         dispatchEvent(new MysteriousEvent(MysteriousEvent.CHANG_VIEW,MysteriousActivityView.TYPE_SHOP));
      }
      
      private function __dawnLottery(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         this.selectedIndex = _loc2_.readInt();
         this._lightUnstart.visible = false;
         this._lightStart.visible = true;
         this._rouletteRun.run(this.selectedIndex);
         this._rouletteRun.addEventListener(Event.COMPLETE,this.onRouletteRunComplete);
      }
      
      protected function onEnterFrame(param1:Event) : void
      {
         if((param1.target as MovieClip).currentFrame == (param1.target as MovieClip).totalFrames)
         {
            (param1.target as MovieClip).stop();
            (param1.target as MovieClip).visible = false;
            (param1.target as MovieClip).gotoAndStop(1);
            (param1.target as MovieClip).removeEventListener(Event.ENTER_FRAME,this.onEnterFrame);
         }
      }
      
      private function removeEvent() : void
      {
         this._startBtn.removeEventListener(MouseEvent.MOUSE_DOWN,this.onStartBtnDown);
         this._shopBtn.removeEventListener(MouseEvent.CLICK,this.onShopBtnClick);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.DAWN_LOTTERY,this.__dawnLottery);
      }
      
      public function dispose() : void
      {
         this.removeEvent();
         if(this._rouletteTitle)
         {
            ObjectUtils.disposeObject(this._rouletteTitle);
         }
         this._rouletteTitle = null;
         if(this._description)
         {
            ObjectUtils.disposeObject(this._description);
         }
         this._description = null;
         if(this._timeBG)
         {
            ObjectUtils.disposeObject(this._timeBG);
         }
         this._timeBG = null;
         if(this._dateTxt)
         {
            ObjectUtils.disposeObject(this._dateTxt);
         }
         this._dateTxt = null;
         if(this._roullete)
         {
            ObjectUtils.disposeObject(this._roullete);
         }
         this._roullete = null;
         if(this._gear)
         {
            ObjectUtils.disposeObject(this._gear);
         }
         this._gear = null;
         if(this._startBtn)
         {
            ObjectUtils.disposeObject(this._startBtn);
         }
         this._startBtn = null;
         if(this._shopBtn)
         {
            ObjectUtils.disposeObject(this._shopBtn);
         }
         this._shopBtn = null;
         if(this._lightStart)
         {
            ObjectUtils.disposeObject(this._lightStart);
         }
         this._lightStart = null;
         if(this._lightUnstart)
         {
            ObjectUtils.disposeObject(this._lightUnstart);
         }
         this._lightUnstart = null;
         if(this._startBtnLight)
         {
            ObjectUtils.disposeObject(this._startBtnLight);
         }
         this._startBtnLight = null;
         if(this._endBtnLight)
         {
            ObjectUtils.disposeObject(this._endBtnLight);
         }
         this._endBtnLight = null;
         if(this._rouletteRun)
         {
            ObjectUtils.disposeObject(this._rouletteRun);
         }
         this._rouletteRun = null;
      }
   }
}
