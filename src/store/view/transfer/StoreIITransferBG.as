package store.view.transfer
{
   import bagAndInfo.cell.BagCell;
   import com.pickgliss.effect.EffectColorType;
   import com.pickgliss.effect.EffectManager;
   import com.pickgliss.effect.EffectTypes;
   import com.pickgliss.effect.IEffect;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.image.Image;
   import com.pickgliss.ui.image.MovieImage;
   import com.pickgliss.ui.image.MutipleImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.command.QuickBuyFrame;
   import ddt.data.BagInfo;
   import ddt.data.EquipType;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PathManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerConfigManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.external.ExternalInterface;
   import flash.geom.Point;
   import flash.net.URLRequest;
   import flash.net.navigateToURL;
   import flash.utils.Dictionary;
   import store.HelpFrame;
   import store.HelpPrompt;
   import store.IStoreViewBG;
   import store.view.ConsortiaRateManager;
   import store.view.StoneCellFrame;
   
   public class StoreIITransferBG extends Sprite implements IStoreViewBG
   {
       
      
      private var _bg:MutipleImage;
      
      private var _area:TransferDragInArea;
      
      private var _items:Vector.<TransferItemCell>;
      
      private var _transferBtnAsset:BaseButton;
      
      private var _transferHelpAsset:BaseButton;
      
      private var transShine:MovieImage;
      
      private var transArr:MutipleImage;
      
      private var _pointArray:Vector.<Point>;
      
      private var gold_txt:FilterFrameText;
      
      private var _goldIcon:Image;
      
      private var _transferBefore:Boolean = false;
      
      private var _transferAfter:Boolean = false;
      
      private var _equipmentCell1:StoneCellFrame;
      
      private var _equipmentCell2:StoneCellFrame;
      
      private var _transferArrow:Bitmap;
      
      private var _transferTitleSmall:Bitmap;
      
      private var _transferTitleLarge:Bitmap;
      
      private var _neededGoldTipText:FilterFrameText;
      
      private var _transferBtnAsset_shineEffect:IEffect;
      
      public function StoreIITransferBG()
      {
         super();
         this.init();
         this.initEvent();
      }
      
      private function init() : void
      {
         var _loc1_:int = 0;
         var _loc2_:TransferItemCell = null;
         this._transferTitleSmall = ComponentFactory.Instance.creatBitmap("asset.ddtstore.TransferTitleSmall");
         this._transferTitleLarge = ComponentFactory.Instance.creatBitmap("asset.ddtstore.TransferTitleLarge");
         addChild(this._transferTitleSmall);
         addChild(this._transferTitleLarge);
         this._equipmentCell1 = ComponentFactory.Instance.creatCustomObject("ddtstore.StoreIITransferBG.EquipmentCell1");
         this._equipmentCell2 = ComponentFactory.Instance.creatCustomObject("ddtstore.StoreIITransferBG.EquipmentCell2");
         this._equipmentCell1.label = this._equipmentCell2.label = LanguageMgr.GetTranslation("store.Strength.StrengthenEquipmentCellText");
         addChild(this._equipmentCell1);
         addChild(this._equipmentCell2);
         this._transferArrow = ComponentFactory.Instance.creatBitmap("asset.ddtstore.TransferArrow");
         addChild(this._transferArrow);
         this._transferBtnAsset = ComponentFactory.Instance.creatComponentByStylename("ddtstore.StoreIITransferBG.TransferBtn");
         addChild(this._transferBtnAsset);
         this._transferBtnAsset_shineEffect = EffectManager.Instance.creatEffect(EffectTypes.Linear_SHINER_ANIMATION,this._transferBtnAsset,{"color":EffectColorType.GOLD});
         this._transferHelpAsset = ComponentFactory.Instance.creatComponentByStylename("ddtstore.HelpButton");
         addChild(this._transferHelpAsset);
         this.transArr = ComponentFactory.Instance.creatComponentByStylename("ddtstore.ArrowHeadTransferTip");
         addChild(this.transArr);
         this._neededGoldTipText = ComponentFactory.Instance.creatComponentByStylename("ddtstore.StoreIITransferBG.NeededGoldTipText");
         this._neededGoldTipText.text = LanguageMgr.GetTranslation("store.Transfer.NeededGoldTipText");
         addChild(this._neededGoldTipText);
         this.gold_txt = ComponentFactory.Instance.creatComponentByStylename("ddtstore.StoreIITarnsferBG.NeedMoneyText");
         addChild(this.gold_txt);
         this._goldIcon = ComponentFactory.Instance.creatComponentByStylename("ddtstore.StoreIITransferBG.GoldIcon");
         addChild(this._goldIcon);
         this.getCellsPoint();
         this._items = new Vector.<TransferItemCell>();
         _loc1_ = 0;
         while(_loc1_ < 2)
         {
            _loc2_ = new TransferItemCell(_loc1_);
            _loc2_.addEventListener(Event.CHANGE,this.__itemInfoChange);
            _loc2_.x = this._pointArray[_loc1_].x;
            _loc2_.y = this._pointArray[_loc1_].y;
            addChild(_loc2_);
            this._items.push(_loc2_);
            _loc1_++;
         }
         this._area = new TransferDragInArea(this._items);
         addChildAt(this._area,0);
         this.hideArr();
         this.hide();
      }
      
      private function getCellsPoint() : void
      {
         var _loc2_:Point = null;
         this._pointArray = new Vector.<Point>();
         var _loc1_:int = 0;
         while(_loc1_ < 2)
         {
            _loc2_ = ComponentFactory.Instance.creatCustomObject("ddtstore.StoreIITransferBG.Transferpoint" + _loc1_);
            this._pointArray.push(_loc2_);
            _loc1_++;
         }
      }
      
      private function initEvent() : void
      {
         this._transferBtnAsset.addEventListener(MouseEvent.CLICK,this.__transferHandler);
         this._transferHelpAsset.addEventListener(MouseEvent.CLICK,this.__openHelpHandler);
      }
      
      private function removeEvent() : void
      {
         this._transferBtnAsset.removeEventListener(MouseEvent.CLICK,this.__transferHandler);
         this._transferHelpAsset.removeEventListener(MouseEvent.CLICK,this.__openHelpHandler);
      }
      
      public function startShine(param1:int) : void
      {
         this._items[param1].startShine();
      }
      
      public function stopShine() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ < 2)
         {
            this._items[_loc1_].stopShine();
            _loc1_++;
         }
      }
      
      private function showArr() : void
      {
         this.transArr.visible = true;
         this._transferBtnAsset_shineEffect.play();
      }
      
      private function hideArr() : void
      {
         this.transArr.visible = false;
         this._transferBtnAsset_shineEffect.stop();
      }
      
      public function get area() : Vector.<TransferItemCell>
      {
         return this._items;
      }
      
      public function dragDrop(param1:BagCell) : void
      {
         var _loc3_:TransferItemCell = null;
         if(param1 == null)
         {
            return;
         }
         var _loc2_:InventoryItemInfo = param1.info as InventoryItemInfo;
         for each(_loc3_ in this._items)
         {
            if(_loc3_.info == null)
            {
               if(this._items[0].info && this._items[0].info.CategoryID != _loc2_.CategoryID || this._items[1].info && this._items[1].info.CategoryID != _loc2_.CategoryID)
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("store.view.fusion.TransferItemCell.put"));
                  return;
               }
               SocketManager.Instance.out.sendMoveGoods(_loc2_.BagType,_loc2_.Place,BagInfo.STOREBAG,_loc3_.index,1);
               return;
            }
         }
      }
      
      private function __transferHandler(param1:MouseEvent) : void
      {
         var _loc4_:String = null;
         var _loc5_:BaseAlerFrame = null;
         var _loc6_:BaseAlerFrame = null;
         SoundManager.instance.play("008");
         param1.stopImmediatePropagation();
         var _loc2_:TransferItemCell = this._items[0] as TransferItemCell;
         var _loc3_:TransferItemCell = this._items[1] as TransferItemCell;
         if(this._showDontClickTip())
         {
            return;
         }
         if(_loc2_.info && _loc3_.info)
         {
            if(PlayerManager.Instance.Self.Gold < Number(this.gold_txt.text))
            {
               _loc6_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("tank.room.RoomIIView2.notenoughmoney.title"),LanguageMgr.GetTranslation("tank.view.GoldInadequate"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),false,false,false,LayerManager.ALPHA_BLOCKGOUND);
               _loc6_.moveEnable = false;
               _loc6_.addEventListener(FrameEvent.RESPONSE,this._responseV);
               return;
            }
            this.hideArr();
            this._transferBefore = this._transferAfter = false;
            _loc4_ = "";
            if(EquipType.isArm(_loc2_.info) || EquipType.isCloth(_loc2_.info) || EquipType.isHead(_loc2_.info) || EquipType.isArm(_loc3_.info) || EquipType.isCloth(_loc3_.info) || EquipType.isHead(_loc3_.info))
            {
               _loc4_ = LanguageMgr.GetTranslation("store.view.transfer.StoreIITransferBG.sure2");
            }
            else
            {
               _loc4_ = LanguageMgr.GetTranslation("store.view.transfer.StoreIITransferBG.sure");
            }
            _loc5_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),_loc4_,LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),false,true,true,LayerManager.ALPHA_BLOCKGOUND);
            _loc5_.addEventListener(FrameEvent.RESPONSE,this._responseII);
         }
         else
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("store.view.transfer.EmptyItem"));
         }
      }
      
      private function _responseV(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         (param1.currentTarget as BaseAlerFrame).removeEventListener(FrameEvent.RESPONSE,this._responseV);
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            this.okFastPurchaseGold();
         }
         ObjectUtils.disposeObject(param1.currentTarget);
      }
      
      private function okFastPurchaseGold() : void
      {
         var _loc1_:QuickBuyFrame = ComponentFactory.Instance.creatComponentByStylename("ddtcore.QuickFrame");
         _loc1_.setTitleText(LanguageMgr.GetTranslation("tank.view.store.matte.goldQuickBuy"));
         _loc1_.itemID = EquipType.GOLD_BOX;
         LayerManager.Instance.addToLayer(_loc1_,LayerManager.GAME_TOP_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
      }
      
      private function _response(param1:FrameEvent) : void
      {
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK)
         {
            this.depositAction();
         }
         ObjectUtils.disposeObject(param1.target);
      }
      
      private function _responseII(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         var _loc2_:BaseAlerFrame = BaseAlerFrame(param1.currentTarget);
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this._responseII);
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            this._transferBefore = this._transferAfter = true;
            this.sendSocket();
         }
         else
         {
            this.cannel();
         }
         ObjectUtils.disposeObject(param1.target);
      }
      
      private function cannel() : void
      {
         this.showArr();
      }
      
      private function depositAction() : void
      {
         if(ExternalInterface.available)
         {
            ExternalInterface.call("setFlashCall");
         }
         navigateToURL(new URLRequest(PathManager.solveFillPage()),"_blank");
      }
      
      private function isComposeStrengthen(param1:BagCell) : Boolean
      {
         if(param1.itemInfo.StrengthenLevel > 0)
         {
            return true;
         }
         if(param1.itemInfo.AttackCompose > 0)
         {
            return true;
         }
         if(param1.itemInfo.DefendCompose > 0)
         {
            return true;
         }
         if(param1.itemInfo.LuckCompose > 0)
         {
            return true;
         }
         if(param1.itemInfo.AgilityCompose > 0)
         {
            return true;
         }
         return false;
      }
      
      private function sendSocket() : void
      {
         SocketManager.Instance.out.sendItemTransfer(this._transferBefore,this._transferAfter);
      }
      
      private function __itemInfoChange(param1:Event) : void
      {
         this.gold_txt.text = "0";
         var _loc2_:TransferItemCell = this._items[0];
         var _loc3_:TransferItemCell = this._items[1];
         if(_loc2_.info)
         {
            _loc2_.categoryId = -1;
            if(_loc3_.info)
            {
               _loc2_.categoryId = _loc2_.info.CategoryID;
            }
            _loc3_.categoryId = _loc2_.info.CategoryID;
         }
         else
         {
            _loc3_.categoryId = -1;
            if(_loc3_.info)
            {
               _loc2_.categoryId = _loc3_.info.CategoryID;
            }
            else
            {
               _loc2_.categoryId = -1;
            }
         }
         if(_loc2_.info)
         {
            _loc2_.Refinery = _loc3_.Refinery = _loc2_.info.RefineryLevel;
         }
         else if(_loc3_.info)
         {
            _loc2_.Refinery = _loc3_.Refinery = _loc3_.info.RefineryLevel;
         }
         else
         {
            _loc2_.Refinery = _loc3_.Refinery = -1;
         }
         if(_loc2_.info && _loc3_.info)
         {
            if(_loc2_.info.CategoryID != _loc3_.info.CategoryID)
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("store.view.transfer.StoreIITransferBG.put"));
               return;
            }
            if(this.isComposeStrengthen(_loc2_) || this.isOpenHole(_loc2_) || this.isHasBead(_loc2_) || this.isComposeStrengthen(_loc3_) || this.isOpenHole(_loc3_) || this.isHasBead(_loc3_) || _loc2_.itemInfo.isHasLatentEnergy || _loc3_.itemInfo.isHasLatentEnergy)
            {
               this.showArr();
               this.goldMoney();
            }
            else
            {
               this.hideArr();
            }
         }
         else
         {
            this.hideArr();
         }
         if(_loc2_.info && !_loc3_.info)
         {
            ConsortiaRateManager.instance.sendTransferShowLightEvent(_loc2_.info,true);
         }
         else if(_loc3_.info && !_loc2_.info)
         {
            ConsortiaRateManager.instance.sendTransferShowLightEvent(_loc3_.info,true);
         }
         else
         {
            ConsortiaRateManager.instance.sendTransferShowLightEvent(null,false);
         }
      }
      
      private function _showDontClickTip() : Boolean
      {
         var _loc1_:TransferItemCell = this._items[0];
         var _loc2_:TransferItemCell = this._items[1];
         if(_loc1_.info == null && _loc2_.info == null)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("store.view.transfer.NoItem"));
            return true;
         }
         if(_loc1_.info == null || _loc2_.info == null)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("store.view.transfer.EmptyItem"));
            return true;
         }
         if(!this.isComposeStrengthen(_loc1_) && !this.isOpenHole(_loc1_) && !this.isHasBead(_loc1_) && !this.isComposeStrengthen(_loc2_) && !this.isOpenHole(_loc2_) && !this.isHasBead(_loc2_) && !_loc1_.itemInfo.isHasLatentEnergy && !_loc2_.itemInfo.isHasLatentEnergy)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("store.showTips.transfer.dontTransferII"));
            return true;
         }
         return false;
      }
      
      private function isHasBead(param1:BagCell) : Boolean
      {
         var _loc2_:InventoryItemInfo = param1.itemInfo;
         return _loc2_.Hole1 + _loc2_.Hole2 + _loc2_.Hole3 + _loc2_.Hole4 + _loc2_.Hole5 + _loc2_.Hole6 > 0;
      }
      
      private function isOpenHole(param1:BagCell) : Boolean
      {
         if(param1.itemInfo.Hole5Level > 0 || param1.itemInfo.Hole6Level > 0 || param1.itemInfo.Hole5Exp > 0 || param1.itemInfo.Hole6Exp > 0)
         {
            return true;
         }
         return false;
      }
      
      private function goldMoney() : void
      {
         this.gold_txt.text = ServerConfigManager.instance.TransferStrengthenEx;
      }
      
      public function show() : void
      {
         this.initEvent();
         this.visible = true;
         this.updateData();
      }
      
      public function refreshData(param1:Dictionary) : void
      {
         var _loc2_:* = null;
         var _loc3_:int = 0;
         for(_loc2_ in param1)
         {
            _loc3_ = int(_loc2_);
            if(_loc3_ < this._items.length)
            {
               this._items[_loc3_].info = PlayerManager.Instance.Self.StoreBag.items[_loc3_];
            }
         }
      }
      
      public function updateData() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ < 2)
         {
            this._items[_loc1_].info = PlayerManager.Instance.Self.StoreBag.items[_loc1_];
            _loc1_++;
         }
      }
      
      public function hide() : void
      {
         this.removeEvent();
         this.visible = false;
         this.hideArr();
      }
      
      private function __openHelpHandler(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         param1.stopImmediatePropagation();
         var _loc2_:HelpPrompt = ComponentFactory.Instance.creat("ddtstore.TransferHelpPrompt");
         var _loc3_:HelpFrame = ComponentFactory.Instance.creat("ddtstore.HelpFrame");
         _loc3_.setView(_loc2_);
         _loc3_.titleText = LanguageMgr.GetTranslation("store.view.transfer.StoreIITransferBG.move");
         LayerManager.Instance.addToLayer(_loc3_,LayerManager.STAGE_DYANMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
      }
      
      public function clearTransferItemCell() : void
      {
         var _loc1_:TransferItemCell = this._items[0];
         var _loc2_:TransferItemCell = this._items[1];
         if(_loc1_.info != null)
         {
            SocketManager.Instance.out.sendMoveGoods(BagInfo.STOREBAG,_loc1_.index,_loc1_.itemBagType,-1);
         }
         if(_loc2_.info != null)
         {
            SocketManager.Instance.out.sendMoveGoods(BagInfo.STOREBAG,_loc2_.index,_loc2_.itemBagType,-1);
         }
      }
      
      public function dispose() : void
      {
         this.removeEvent();
         var _loc1_:int = 0;
         while(_loc1_ < this._items.length)
         {
            this._items[_loc1_].removeEventListener(Event.CHANGE,this.__itemInfoChange);
            this._items[_loc1_].dispose();
            _loc1_++;
         }
         this._items = null;
         EffectManager.Instance.removeEffect(this._transferBtnAsset_shineEffect);
         this._pointArray = null;
         if(this._bg)
         {
            ObjectUtils.disposeObject(this._bg);
         }
         this._bg = null;
         if(this._neededGoldTipText)
         {
            ObjectUtils.disposeObject(this._neededGoldTipText);
         }
         this._neededGoldTipText = null;
         if(this._goldIcon)
         {
            ObjectUtils.disposeObject(this._goldIcon);
         }
         this._goldIcon = null;
         if(this._transferTitleLarge)
         {
            ObjectUtils.disposeObject(this._transferTitleLarge);
         }
         this._transferTitleLarge = null;
         if(this._transferTitleSmall)
         {
            ObjectUtils.disposeObject(this._transferTitleSmall);
         }
         this._transferTitleSmall = null;
         if(this._transferArrow)
         {
            ObjectUtils.disposeObject(this._transferArrow);
         }
         this._transferArrow = null;
         if(this._equipmentCell1)
         {
            ObjectUtils.disposeObject(this._equipmentCell1);
         }
         this._equipmentCell1 = null;
         if(this._equipmentCell2)
         {
            ObjectUtils.disposeObject(this._equipmentCell2);
         }
         this._equipmentCell2 = null;
         if(this._area)
         {
            ObjectUtils.disposeObject(this._area);
         }
         this._area = null;
         if(this._transferBtnAsset)
         {
            ObjectUtils.disposeObject(this._transferBtnAsset);
         }
         this._transferBtnAsset = null;
         if(this._transferHelpAsset)
         {
            ObjectUtils.disposeObject(this._transferHelpAsset);
         }
         this._transferHelpAsset = null;
         if(this.transShine)
         {
            ObjectUtils.disposeObject(this.transShine);
         }
         this.transShine = null;
         if(this.transArr)
         {
            ObjectUtils.disposeObject(this.transArr);
         }
         this.transArr = null;
         if(this.gold_txt)
         {
            ObjectUtils.disposeObject(this.gold_txt);
         }
         this.gold_txt = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
