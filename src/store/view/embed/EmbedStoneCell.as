package store.view.embed
{
   import bagAndInfo.cell.BaseCell;
   import bagAndInfo.cell.DragEffect;
   import baglocked.BaglockedManager;
   import beadSystem.beadSystemManager;
   import beadSystem.controls.BeadLeadManager;
   import beadSystem.model.BeadModel;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.events.InteractiveEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.image.Image;
   import com.pickgliss.ui.image.ScaleFrameImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.DoubleClickManager;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.command.ShineObject;
   import ddt.data.EquipType;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.data.player.PlayerInfo;
   import ddt.events.CellEvent;
   import ddt.events.PlayerPropertyEvent;
   import ddt.manager.BeadTemplateManager;
   import ddt.manager.DragManager;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerConfigManager;
   import ddt.manager.SharedManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.view.tips.GoodTipInfo;
   import ddt.view.tips.MultipleLineTip;
   import ddt.view.tips.OneLineTip;
   import flash.display.Bitmap;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.geom.Point;
   import road7th.utils.MovieClipWrapper;
   import store.events.EmbedBackoutEvent;
   import trainer.data.ArrowType;
   import trainer.view.NewHandContainer;
   
   public class EmbedStoneCell extends BaseCell
   {
      
      public static const Close:int = -1;
      
      public static const Empty:int = 0;
      
      public static const Full:int = 1;
      
      public static const ATTACK:int = 1;
      
      public static const DEFENSE:int = 2;
      
      public static const ATTRIBUTE:int = 3;
      
      public static const BEAD_STATE_CHANGED:String = "beadEmbed";
       
      
      private var _id:int;
      
      private var _state:int = -1;
      
      private var _stoneType:int;
      
      private var _shiner:ShineObject;
      
      private var _tipPanel:Sprite;
      
      private var _tipDerial:Boolean;
      
      private var tipSprite:Sprite;
      
      private var _tipOne:MultipleLineTip;
      
      private var _tipTwo:OneLineTip;
      
      private var _openGrid:ScaleFrameImage;
      
      private var _openBG:Bitmap;
      
      private var _closeStrip:Bitmap;
      
      private var _SelectedImage:Image;
      
      private var _holeLv:int = -1;
      
      private var _holeExp:int = -1;
      
      private var _dragBeadPic:MovieClip;
      
      private var _nameTxt:FilterFrameText;
      
      private var _lockIcon:Bitmap;
      
      private var _selected:Boolean = false;
      
      private var _isOpend:Boolean = false;
      
      private var _itemInfo:InventoryItemInfo;
      
      private var _beadInfo:InventoryItemInfo;
      
      private var _Player:PlayerInfo;
      
      public function EmbedStoneCell(param1:int, param2:int)
      {
         var _loc4_:Bitmap = null;
         var _loc3_:Sprite = new Sprite();
         if(param1 == 31)
         {
            _loc4_ = ComponentFactory.Instance.creatBitmap("beadSystem.upgradeBG");
         }
         else
         {
            _loc4_ = ComponentFactory.Instance.creatBitmap("asset.ddtstore.EmbedCellBG");
         }
         _loc3_.addChild(_loc4_);
         super(_loc3_,_info);
         this._id = param1;
         this._stoneType = param2;
         this._shiner = new ShineObject(ComponentFactory.Instance.creat("asset.ddtstore.EmbedStoneCellShine"));
         this._shiner.mouseChildren = this._shiner.mouseEnabled = this._shiner.visible = false;
         addChild(this._shiner);
         if(this._id != 31)
         {
            this._openBG = ComponentFactory.Instance.creatBitmap("beadSystem.embedBG");
            addChildAt(this._openBG,0);
         }
         if(this._id <= 12)
         {
            this._closeStrip = ComponentFactory.Instance.creatBitmap("beadSystem.unOpenedBG");
         }
         else
         {
            this._closeStrip = ComponentFactory.Instance.creatBitmap("beadSystem.disopened.bg");
         }
         addChild(this._closeStrip);
         if(this._id < 6)
         {
            this.open();
         }
         this._tipOne = ComponentFactory.Instance.creatCustomObject("ddtstore.StoreEmbedBG.MultipleLineTip");
         this._tipTwo = ComponentFactory.Instance.creatCustomObject("ddtstore.StoreEmbedBG.EmbedStoneCellCloseTip");
         this._tipTwo.tipData = LanguageMgr.GetTranslation("tank.store.embedCell.closeOpenHole");
         if(this._id > 12 && this._id != 31)
         {
            this._SelectedImage = ComponentFactory.Instance.creatComponentByStylename("beadSystem.openHoleSelectedImage");
            this._SelectedImage.visible = false;
            addChild(this._SelectedImage);
         }
         this.initEvents();
         if(this._id <= 12 && this._id >= 4)
         {
            this.setTipTwoData(this._id);
         }
      }
      
      private function setTipTwoData(param1:int) : void
      {
         switch(param1)
         {
            case 6:
               this._tipTwo.tipData = LanguageMgr.GetTranslation("ddt.beadSystem.tipLvOpenHole",15);
               PlayerManager.Instance.Self.addEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__changeHandler);
               if(PlayerManager.Instance.Self.Grade >= 15)
               {
                  this.open();
               }
               if(this.otherPlayer)
               {
                  if(this.otherPlayer.Grade < 15)
                  {
                     this.close();
                  }
                  else
                  {
                     this.open();
                  }
               }
               break;
            case 7:
               this._tipTwo.tipData = LanguageMgr.GetTranslation("ddt.beadSystem.tipLvOpenHole",18);
               PlayerManager.Instance.Self.addEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__changeHandler);
               if(PlayerManager.Instance.Self.Grade >= 18)
               {
                  this.open();
               }
               if(this.otherPlayer)
               {
                  if(this.otherPlayer.Grade < 18)
                  {
                     this.close();
                  }
                  else
                  {
                     this.open();
                  }
               }
               break;
            case 8:
               this._tipTwo.tipData = LanguageMgr.GetTranslation("ddt.beadSystem.tipLvOpenHole",21);
               PlayerManager.Instance.Self.addEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__changeHandler);
               if(PlayerManager.Instance.Self.Grade >= 21)
               {
                  this.open();
               }
               if(this.otherPlayer)
               {
                  if(this.otherPlayer.Grade < 21)
                  {
                     this.close();
                  }
                  else
                  {
                     this.open();
                  }
               }
               break;
            case 9:
               this._tipTwo.tipData = LanguageMgr.GetTranslation("ddt.beadSystem.tipLvOpenHole",24);
               PlayerManager.Instance.Self.addEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__changeHandler);
               if(PlayerManager.Instance.Self.Grade >= 24)
               {
                  this.open();
               }
               if(this.otherPlayer)
               {
                  if(this.otherPlayer.Grade < 24)
                  {
                     this.close();
                  }
                  else
                  {
                     this.open();
                  }
               }
               break;
            case 10:
               this._tipTwo.tipData = LanguageMgr.GetTranslation("ddt.beadSystem.tipLvOpenHole",27);
               PlayerManager.Instance.Self.addEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__changeHandler);
               if(PlayerManager.Instance.Self.Grade >= 27)
               {
                  this.open();
               }
               if(this.otherPlayer)
               {
                  if(this.otherPlayer.Grade < 27)
                  {
                     this.close();
                  }
                  else
                  {
                     this.open();
                  }
               }
               break;
            case 11:
               this._tipTwo.tipData = LanguageMgr.GetTranslation("ddt.beadSystem.tipLvOpenHole",30);
               PlayerManager.Instance.Self.addEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__changeHandler);
               if(PlayerManager.Instance.Self.Grade >= 30)
               {
                  this.open();
               }
               if(this.otherPlayer)
               {
                  if(this.otherPlayer.Grade < 30)
                  {
                     this.close();
                  }
                  else
                  {
                     this.open();
                  }
               }
               break;
            case 12:
               this._tipTwo.tipData = LanguageMgr.GetTranslation("ddt.beadSystem.tipLvOpenHole",33);
               PlayerManager.Instance.Self.addEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__changeHandler);
               if(PlayerManager.Instance.Self.Grade >= 33)
               {
                  this.open();
               }
               if(this.otherPlayer)
               {
                  if(this.otherPlayer.Grade < 33)
                  {
                     this.close();
                  }
                  else
                  {
                     this.open();
                  }
               }
               break;
            default:
               this._tipTwo.tipData = "";
         }
      }
      
      public function LockBead() : Boolean
      {
         if(!this.itemInfo)
         {
            return false;
         }
         if(!this.itemInfo.IsUsed)
         {
            if(this._lockIcon)
            {
               this._lockIcon.visible = true;
               setChildIndex(this._lockIcon,numChildren - 1);
            }
            else
            {
               this._lockIcon = ComponentFactory.Instance.creatBitmap("asset.beadSystem.beadInset.lockIcon1");
               this._lockIcon.scaleX = this._lockIcon.scaleY = 0.7;
               this.addChild(this._lockIcon);
               setChildIndex(this._lockIcon,numChildren - 1);
            }
            SocketManager.Instance.out.sendBeadLock(this.ID);
         }
         else
         {
            if(this._lockIcon)
            {
               this._lockIcon.visible = false;
            }
            SocketManager.Instance.out.sendBeadLock(this.ID);
         }
         return true;
      }
      
      private function __changeHandler(param1:PlayerPropertyEvent) : void
      {
         PlayerManager.Instance.Self.removeEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__changeHandler);
         this.setTipTwoData(this.ID);
      }
      
      public function set selected(param1:Boolean) : void
      {
         this._selected = param1;
         if(this._SelectedImage)
         {
            this._SelectedImage.visible = param1;
         }
      }
      
      public function get selected() : Boolean
      {
         return this._selected;
      }
      
      public function holeLvUp() : void
      {
         var _loc1_:MovieClip = null;
         _loc1_ = ComponentFactory.Instance.creatCustomObject("ddtstore.StoreEmbedBG.EmbedHoleExpUp");
         _loc1_.scaleX = _loc1_.scaleY = 0.4;
         _loc1_.y = 42;
         var _loc2_:MovieClipWrapper = new MovieClipWrapper(_loc1_,true,true);
         addChild(_loc2_.movie);
         BeadModel.isHoleOpendComplete = false;
      }
      
      public function get ID() : int
      {
         return this._id;
      }
      
      override public function get allowDrag() : Boolean
      {
         return true;
      }
      
      private function initEvents() : void
      {
         addEventListener(InteractiveEvent.CLICK,this.__clickHandler);
         addEventListener(InteractiveEvent.DOUBLE_CLICK,this.__doubleClick);
         DoubleClickManager.Instance.enableDoubleClick(this);
      }
      
      override protected function onMouseOver(param1:MouseEvent) : void
      {
         if(!this._Player)
         {
            if(this._state != Close && _info == null)
            {
               this._tipOne.x = localToGlobal(new Point(width,height)).x + 8;
               this._tipOne.y = localToGlobal(new Point(width,height)).y + 8;
               LayerManager.Instance.addToLayer(this._tipOne,LayerManager.GAME_TOP_LAYER);
               return;
            }
            if(!this.isOpend && _info == null)
            {
               this._tipTwo.x = localToGlobal(new Point(width,height)).x + 8;
               this._tipTwo.y = localToGlobal(new Point(width,height)).y + 8;
               LayerManager.Instance.addToLayer(this._tipTwo,LayerManager.GAME_TOP_LAYER);
            }
         }
      }
      
      private function __clickHandler(param1:InteractiveEvent) : void
      {
         dispatchEvent(new CellEvent(CellEvent.ITEM_CLICK,this));
      }
      
      private function __doubleClick(param1:InteractiveEvent) : void
      {
         SoundManager.instance.playButtonSound();
         if(_info)
         {
            dispatchEvent(new CellEvent(CellEvent.DOUBLE_CLICK,this));
         }
      }
      
      public function showTip(param1:MouseEvent) : void
      {
      }
      
      public function closeTip(param1:MouseEvent) : void
      {
         super.onMouseOut(param1);
      }
      
      override protected function onMouseOut(param1:MouseEvent) : void
      {
         if(this._state == Full && !this._tipDerial && _info != null)
         {
            dispatchEvent(new EmbedBackoutEvent(EmbedBackoutEvent.EMBED_BACKOUT_DOWNITEM_OUT,this._id,info.TemplateID));
         }
         if(this._state != Close && _info == null)
         {
            if(this._tipOne.parent)
            {
               this._tipOne.parent.removeChild(this._tipOne);
            }
            return;
         }
         if(!this.isOpend && _info == null)
         {
            super.onMouseOut(param1);
            if(this._tipTwo.parent)
            {
               this._tipTwo.parent.removeChild(this._tipTwo);
            }
         }
      }
      
      public function open() : void
      {
         if(this._stoneType == -1)
         {
            return;
         }
         this._state = Empty;
         if(this._closeStrip)
         {
            this._closeStrip.visible = false;
         }
         this._isOpend = true;
      }
      
      public function get isOpend() : Boolean
      {
         return this._isOpend;
      }
      
      public function set HoleExp(param1:int) : void
      {
         this._holeExp = param1;
      }
      
      public function get HoleExp() : int
      {
         return this._holeExp;
      }
      
      public function set HoleLv(param1:int) : void
      {
         this._holeLv = param1;
         if(this._holeLv >= 0 && this._stoneType == ATTRIBUTE && this._id > 4)
         {
            switch(this._holeLv)
            {
               case 1:
                  this._tipOne.tipData = LanguageMgr.GetTranslation("tank.store.embedCell.Hole",this._holeLv,4);
                  break;
               case 2:
                  this._tipOne.tipData = LanguageMgr.GetTranslation("tank.store.embedCell.Hole",this._holeLv,8);
                  break;
               case 3:
                  this._tipOne.tipData = LanguageMgr.GetTranslation("tank.store.embedCell.Hole",this._holeLv,12);
                  break;
               case 4:
                  this._tipOne.tipData = LanguageMgr.GetTranslation("tank.store.embedCell.Hole",this._holeLv,16);
                  break;
               case 5:
                  this._tipOne.tipData = LanguageMgr.GetTranslation("tank.store.embedCell.Hole",this._holeLv,19);
            }
         }
         if(this._holeLv > 0)
         {
            this.open();
         }
      }
      
      public function get HoleLv() : int
      {
         return this._holeLv;
      }
      
      public function set StoneType(param1:int) : void
      {
         var _loc2_:String = null;
         this._stoneType = param1;
         if(this.ID != 31)
         {
            switch(this._stoneType)
            {
               case ATTACK:
                  _loc2_ = LanguageMgr.GetTranslation("tank.store.embedCell.attack");
                  break;
               case DEFENSE:
                  _loc2_ = LanguageMgr.GetTranslation("tank.store.embedCell.defense");
                  break;
               case ATTRIBUTE:
                  _loc2_ = LanguageMgr.GetTranslation("tank.store.embedCell.attribute");
                  break;
               default:
                  _loc2_ = null;
            }
            if(_loc2_)
            {
               this._tipOne.tipData = _loc2_;
            }
         }
         else
         {
            this._tipOne.tipData = LanguageMgr.GetTranslation("ddt.beadSystem.tipFeedHole");
         }
      }
      
      public function get StoneType() : int
      {
         return this._stoneType;
      }
      
      override public function dragStart() : void
      {
         if(_info && !locked && stage && _allowDrag)
         {
            if(DragManager.startDrag(this,_info,createDragImg(),stage.mouseX,stage.mouseY,DragEffect.MOVE))
            {
               locked = true;
               this.dragHidePicTxt();
               this._openBG.visible = true;
            }
         }
      }
      
      private function dragHidePicTxt() : void
      {
         this._nameTxt.visible = false;
         if(this._lockIcon)
         {
            this._lockIcon.visible = false;
         }
      }
      
      private function dragShowPicTxt() : void
      {
         this._nameTxt.visible = true;
         if(this.itemInfo.IsUsed && this._lockIcon)
         {
            this._lockIcon.visible = true;
         }
      }
      
      public function set itemInfo(param1:InventoryItemInfo) : void
      {
         this._itemInfo = param1;
      }
      
      public function get itemInfo() : InventoryItemInfo
      {
         return this._itemInfo;
      }
      
      override public function set info(param1:ItemTemplateInfo) : void
      {
         var _loc2_:String = null;
         if(_info)
         {
            _tipData = null;
            locked = false;
            if(this._nameTxt)
            {
               this._nameTxt.htmlText = "";
               this._nameTxt.visible = false;
            }
         }
         super.info = param1;
         _info = param1;
         if(_info)
         {
            if(!this._nameTxt)
            {
               this._nameTxt = ComponentFactory.Instance.creatComponentByStylename("beadSystem.beadCellEquip.name");
               this._nameTxt.mouseEnabled = false;
               addChild(this._nameTxt);
            }
            this.setChildIndex(this._nameTxt,this.numChildren - 1);
            _loc2_ = beadSystemManager.Instance.getBeadNameTextFormatStyle(this.itemInfo.Hole1);
            this._nameTxt.textFormatStyle = _loc2_;
            this._nameTxt.visible = true;
            tipStyle = "core.GoodsTip";
            _tipData = new GoodTipInfo();
            GoodTipInfo(_tipData).itemInfo = _info;
            if(this.itemInfo.Hole2 > 0)
            {
               GoodTipInfo(_tipData).exp = this.itemInfo.Hole2;
               GoodTipInfo(_tipData).upExp = ServerConfigManager.instance.getBeadUpgradeExp()[this.itemInfo.Hole1 + 1];
               GoodTipInfo(_tipData).beadName = info.Name + "-" + BeadTemplateManager.Instance.GetBeadInfobyID(param1.TemplateID).Name + "ур." + this.itemInfo.Hole1;
               this._nameTxt.htmlText = BeadTemplateManager.Instance.GetBeadInfobyID(param1.TemplateID).Name + "-" + this.itemInfo.Hole1;
            }
            else
            {
               GoodTipInfo(_tipData).exp = ServerConfigManager.instance.getBeadUpgradeExp()[BeadTemplateManager.Instance.GetBeadInfobyID(this.itemInfo.TemplateID).BaseLevel];
               GoodTipInfo(_tipData).upExp = ServerConfigManager.instance.getBeadUpgradeExp()[BeadTemplateManager.Instance.GetBeadInfobyID(this.itemInfo.TemplateID).BaseLevel + 1];
               this._nameTxt.htmlText = BeadTemplateManager.Instance.GetBeadInfobyID(param1.TemplateID).Name + "-" + BeadTemplateManager.Instance.GetBeadInfobyID(this.itemInfo.TemplateID).BaseLevel;
               GoodTipInfo(_tipData).beadName = info.Name + "-" + BeadTemplateManager.Instance.GetBeadInfobyID(param1.TemplateID).Name + "ур." + BeadTemplateManager.Instance.GetBeadInfobyID(this.itemInfo.TemplateID).BaseLevel;
            }
            if(this.itemInfo.IsUsed)
            {
               if(this._lockIcon)
               {
                  this._lockIcon.visible = true;
                  setChildIndex(this._lockIcon,numChildren - 1);
               }
               else
               {
                  this._lockIcon = ComponentFactory.Instance.creatBitmap("asset.beadSystem.beadInset.lockIcon1");
                  this._lockIcon.scaleX = this._lockIcon.scaleY = 0.7;
                  this.addChild(this._lockIcon);
               }
            }
            else if(this._lockIcon)
            {
               this._lockIcon.visible = false;
            }
         }
         else
         {
            if(this._lockIcon)
            {
               this._lockIcon.visible = false;
            }
            this.disposeDragBeadPic();
            if(this._nameTxt)
            {
               this._nameTxt.visible = false;
            }
         }
         if(this._openBG)
         {
            if(param1)
            {
               this._openBG.visible = false;
            }
            else
            {
               this._openBG.visible = true;
            }
         }
      }
      
      public function close() : void
      {
         this._state = Close;
         if(this._closeStrip)
         {
            this._closeStrip.visible = true;
         }
         this.info = null;
         tipData = null;
      }
      
      override protected function onMouseClick(param1:MouseEvent) : void
      {
      }
      
      private function disposeDragBeadPic() : void
      {
         if(this._dragBeadPic)
         {
            this._dragBeadPic.gotoAndStop(this._dragBeadPic.totalFrames);
            ObjectUtils.disposeObject(this._dragBeadPic);
            this._dragBeadPic = null;
         }
      }
      
      override public function dragDrop(param1:DragEffect) : void
      {
         var _loc2_:InventoryItemInfo = null;
         var _loc3_:BaseAlerFrame = null;
         if(this.isOpend)
         {
            if(PlayerManager.Instance.Self.bagLocked)
            {
               DragManager.acceptDrag(this);
               BaglockedManager.Instance.show();
               return;
            }
            _loc2_ = param1.data as InventoryItemInfo;
            if(_loc2_ && param1.action != DragEffect.SPLIT)
            {
               param1.action = DragEffect.NONE;
               if(this.ID != 31 && !this.isRightType(_loc2_))
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("tank.view.store.matte.notType"));
               }
               else if(!beadSystemManager.Instance.judgeLevel(_loc2_.Hole1,this._holeLv) && 13 <= this.ID && this.ID <= 18)
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.beadSystem.tipDonotAdaptLevel"));
               }
               else
               {
                  this._beadInfo = _loc2_;
                  if(this.itemInfo && int(this.itemInfo.Hole1) == 19 && !(param1.source is EmbedStoneCell) && param1.source is EmbedUpLevelCell)
                  {
                     MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.beadSystem.mostHightLevel"));
                     return;
                  }
                  if(this.itemInfo && int(this.itemInfo.Hole1) == 1 && !(param1.source is EmbedStoneCell) && param1.source is EmbedUpLevelCell)
                  {
                     MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.beadSystem.lvOneCanntUpgrade"));
                  }
                  if(!this._beadInfo.IsBinds)
                  {
                     _loc3_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("tips"),LanguageMgr.GetTranslation("ddt.beadSystem.useBindBead"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),false,true,false,LayerManager.ALPHA_BLOCKGOUND);
                     _loc3_.addEventListener(FrameEvent.RESPONSE,this.__onBindRespones);
                  }
                  else
                  {
                     this.itemInfo = _loc2_;
                     this.info = _loc2_;
                     SocketManager.Instance.out.sendBeadEquip(_loc2_.Place,this._id);
                  }
                  dispatchEvent(new Event(BEAD_STATE_CHANGED));
                  this._tipDerial = false;
               }
            }
         }
         else
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.beadSystem.tipNoAccessEquipBead"));
         }
         DragManager.acceptDrag(this);
         if(NewHandContainer.Instance.hasArrow(ArrowType.LEAD_BEAD_EQUIPBEAD))
         {
            NewHandContainer.Instance.clearArrowByID(ArrowType.LEAD_BEAD_EQUIPBEAD);
            BeadLeadManager.Instance.leadCandleStick(LayerManager.Instance.getLayerByType(LayerManager.GAME_TOP_LAYER));
            SharedManager.Instance.beadLeadTaskStep = 6;
            SharedManager.Instance.save();
         }
      }
      
      protected function __onBindRespones(param1:FrameEvent) : void
      {
         switch(param1.responseCode)
         {
            case FrameEvent.CANCEL_CLICK:
            case FrameEvent.CLOSE_CLICK:
            case FrameEvent.ESC_CLICK:
               break;
            case FrameEvent.SUBMIT_CLICK:
            case FrameEvent.ENTER_CLICK:
               this.itemInfo = this._beadInfo;
               this.info = this._beadInfo;
               if(this.StoneType == int(this._beadInfo.Property2))
               {
                  SocketManager.Instance.out.sendBeadEquip(this._beadInfo.Place,this._id);
               }
               else
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("tank.view.store.matte.notType"));
               }
         }
         param1.currentTarget.removeEventListener(FrameEvent.RESPONSE,this.__onBindRespones);
         ObjectUtils.disposeObject(param1.currentTarget);
      }
      
      override public function dragStop(param1:DragEffect) : void
      {
         SoundManager.instance.play("008");
         if(param1.action == DragEffect.MOVE && !param1.target)
         {
            param1.action = DragEffect.NONE;
         }
         this.disposeDragBeadPic();
         this.dragShowPicTxt();
         if(this.ID != 31)
         {
            if(this._openBG)
            {
               this._openBG.visible = false;
            }
         }
         locked = false;
         super.dragStop(param1);
      }
      
      private function isRightType(param1:InventoryItemInfo) : Boolean
      {
         return param1.Property2 == this._stoneType.toString();
      }
      
      public function get tipDerial() : Boolean
      {
         return this._tipDerial;
      }
      
      public function set tipDerial(param1:Boolean) : void
      {
         this._tipDerial = param1;
      }
      
      public function startShine() : void
      {
         this._shiner.scaleX = this._shiner.scaleY = 0.8;
         this._shiner.x = this._shiner.y = 3;
         this._shiner.visible = true;
         this._shiner.shine();
      }
      
      public function stopShine() : void
      {
         this._shiner.stopShine();
         this._shiner.visible = false;
      }
      
      public function hasDrill() : Boolean
      {
         return EquipType.isDrill(_info as InventoryItemInfo);
      }
      
      public function get otherPlayer() : PlayerInfo
      {
         return this._Player;
      }
      
      public function set otherPlayer(param1:PlayerInfo) : void
      {
         this._Player = param1;
         if(this._Player)
         {
            if(this._id <= 12 && this._id >= 4)
            {
               this.setTipTwoData(this._id);
            }
         }
      }
      
      override protected function updateSize(param1:Sprite) : void
      {
         if(param1)
         {
            param1.scaleX = 0.8;
            param1.scaleY = 0.8;
            if(_picPos != null)
            {
               param1.x = _picPos.x;
            }
            else
            {
               param1.x = param1.x - param1.width / 2 + _contentWidth / 2;
            }
            if(_picPos != null)
            {
               param1.y = _picPos.y;
            }
            else
            {
               param1.y = param1.y - param1.height / 2 + _contentHeight / 2;
            }
         }
      }
      
      override public function dispose() : void
      {
         removeEventListener(InteractiveEvent.DOUBLE_CLICK,this.__doubleClick);
         removeEventListener(InteractiveEvent.CLICK,this.__clickHandler);
         PlayerManager.Instance.Self.removeEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__changeHandler);
         DoubleClickManager.Instance.disableDoubleClick(this);
         if(this._tipOne)
         {
            ObjectUtils.disposeObject(this._tipOne);
         }
         this._tipOne = null;
         if(this._tipTwo)
         {
            ObjectUtils.disposeObject(this._tipTwo);
         }
         this._tipTwo = null;
         if(this._shiner)
         {
            ObjectUtils.disposeObject(this._shiner);
         }
         this._shiner = null;
         if(this._openGrid)
         {
            ObjectUtils.disposeObject(this._openGrid);
         }
         this._openGrid = null;
         if(this._closeStrip)
         {
            ObjectUtils.disposeObject(this._closeStrip);
         }
         this._closeStrip = null;
         ObjectUtils.disposeObject(this._SelectedImage);
         this._SelectedImage = null;
         super.dispose();
      }
      
      public function get state() : int
      {
         return this._state;
      }
   }
}
