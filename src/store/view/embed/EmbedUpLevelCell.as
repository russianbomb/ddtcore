package store.view.embed
{
   import bagAndInfo.cell.BaseCell;
   import bagAndInfo.cell.DragEffect;
   import baglocked.BaglockedManager;
   import beadSystem.beadSystemManager;
   import beadSystem.controls.BeadLeadManager;
   import beadSystem.data.BeadEvent;
   import beadSystem.model.BeadModel;
   import beadSystem.tips.BeadUpgradeTip;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ClassUtils;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.command.ShineObject;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.events.CellEvent;
   import ddt.manager.BeadTemplateManager;
   import ddt.manager.DragManager;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerConfigManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.view.tips.GoodTipInfo;
   import flash.display.Bitmap;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   
   public class EmbedUpLevelCell extends BaseCell
   {
       
      
      private var _shiner:ShineObject;
      
      private var _beadPic:MovieClip;
      
      private var _dragBeadPic:MovieClip;
      
      private var _invenItemInfo:InventoryItemInfo;
      
      private var _nameTxt:FilterFrameText;
      
      private var _lockIcon:Bitmap;
      
      private var _upTip:BeadUpgradeTip;
      
      private var _previewArray:Bitmap;
      
      private var _upgradeMC:MovieClip;
      
      private var _itemInfo:InventoryItemInfo;
      
      public function EmbedUpLevelCell()
      {
         var _loc1_:Bitmap = ComponentFactory.Instance.creatBitmap("beadSystem.upgradeBG");
         super(_loc1_);
         this._shiner = new ShineObject(ComponentFactory.Instance.creat("asset.ddtstore.EmbedStoneCellShine"));
         this._shiner.mouseChildren = this._shiner.mouseEnabled = this._shiner.visible = false;
         addChild(this._shiner);
         PlayerManager.Instance.Self.embedUpLevelCell = this;
         beadSystemManager.Instance.addEventListener(BeadEvent.PLAYUPGRADEMC,this.__startPlay);
      }
      
      private function __startPlay(param1:Event) : void
      {
         if(!this._upgradeMC)
         {
            this._upgradeMC = ClassUtils.CreatInstance("beadSystem.upgrade.MC");
            this._upgradeMC.x = 451;
            this._upgradeMC.y = 125;
            this._upgradeMC.addEventListener("upgradeBeadComplete",this.__onUpgradeComplete);
            LayerManager.Instance.addToLayer(this._upgradeMC,LayerManager.STAGE_TOP_LAYER,false,LayerManager.BLCAK_BLOCKGOUND,true);
            this._upgradeMC.gotoAndPlay(1);
         }
      }
      
      private function __onUpgradeComplete(param1:Event) : void
      {
         this._upgradeMC.removeEventListener("upgradeBeadComplete",this.__onUpgradeComplete);
         this._upgradeMC.stop();
         ObjectUtils.disposeObject(this._upgradeMC);
         this._upgradeMC = null;
      }
      
      override public function set info(param1:ItemTemplateInfo) : void
      {
         super.info = param1;
         if(_info)
         {
            _tipData = null;
            locked = false;
            this.disposeBeadPic();
         }
         _info = param1;
         if(_info)
         {
            if(!this._nameTxt)
            {
               this._nameTxt = ComponentFactory.Instance.creatComponentByStylename("beadSystem.beadCellEquip.name");
               this._nameTxt.mouseEnabled = false;
               addChild(this._nameTxt);
            }
            tipStyle = "beadSystem.tips.BeadUpgradeTip";
            _tipData = new GoodTipInfo();
            GoodTipInfo(_tipData).itemInfo = _info;
            if(this.itemInfo.Hole2 > 0)
            {
               GoodTipInfo(_tipData).exp = this.itemInfo.Hole2;
               GoodTipInfo(_tipData).upExp = ServerConfigManager.instance.getBeadUpgradeExp()[this.itemInfo.Hole1 + 1];
               GoodTipInfo(_tipData).beadName = info.Name + "-" + BeadTemplateManager.Instance.GetBeadInfobyID(param1.TemplateID).Name + "yp." + this.itemInfo.Hole1;
            }
            else
            {
               GoodTipInfo(_tipData).exp = ServerConfigManager.instance.getBeadUpgradeExp()[BeadTemplateManager.Instance.GetBeadInfobyID(this.itemInfo.TemplateID).BaseLevel];
               GoodTipInfo(_tipData).upExp = ServerConfigManager.instance.getBeadUpgradeExp()[BeadTemplateManager.Instance.GetBeadInfobyID(this.itemInfo.TemplateID).BaseLevel + 1];
               GoodTipInfo(_tipData).beadName = info.Name + "-" + BeadTemplateManager.Instance.GetBeadInfobyID(param1.TemplateID).Name + "yp." + BeadTemplateManager.Instance.GetBeadInfobyID(this.itemInfo.TemplateID).BaseLevel;
            }
            if(this.invenItemInfo.IsUsed)
            {
               if(this._lockIcon)
               {
                  this._lockIcon.visible = true;
                  setChildIndex(this._lockIcon,numChildren - 1);
               }
               else
               {
                  this._lockIcon = ComponentFactory.Instance.creatBitmap("asset.beadSystem.beadInset.lockIcon1");
                  this._lockIcon.x = 5;
                  this._lockIcon.y = 5;
                  this.addChild(this._lockIcon);
                  setChildIndex(this._lockIcon,numChildren - 1);
               }
            }
            else if(this._lockIcon)
            {
               this._lockIcon.visible = false;
            }
            BeadModel.isBeadCellIsBind = this.itemInfo.IsBinds;
         }
         else
         {
            if(this._lockIcon)
            {
               this._lockIcon.visible = false;
            }
            this.disposeDragBeadPic();
         }
         BeadModel.beadCanUpgrade = param1 == null?Boolean(false):Boolean(true);
         dispatchEvent(new BeadEvent(BeadEvent.BEADCELLCHANGED,-1,param1));
      }
      
      public function set itemInfo(param1:InventoryItemInfo) : void
      {
         BeadModel.upgradeCellInfo = param1;
         this._itemInfo = param1;
      }
      
      public function get itemInfo() : InventoryItemInfo
      {
         return this._itemInfo;
      }
      
      private function createBeadPic(param1:ItemTemplateInfo) : void
      {
         if(this._invenItemInfo.Hole1 > 0)
         {
            this._beadPic = ClassUtils.CreatInstance("beadSystem.beadMC" + beadSystemManager.Instance.getBeadMcIndex(this.itemInfo.Hole1));
            this._beadPic.scaleX = 1;
            this._beadPic.scaleY = 1;
            this._beadPic.x = 2;
            this._beadPic.y = 2;
            addChild(this._beadPic);
         }
         else
         {
            this._beadPic = ComponentFactory.Instance.creat("beadSystem.bead" + beadSystemManager.Instance.getBeadMcIndex(BeadTemplateManager.Instance.GetBeadInfobyID(this.info.TemplateID).BaseLevel));
            this._beadPic.scaleX = 1;
            this._beadPic.scaleY = 1;
            this._beadPic.x = 2;
            this._beadPic.y = 2;
            addChild(this._beadPic);
         }
      }
      
      private function disposeBeadPic() : void
      {
         if(this._beadPic)
         {
            ObjectUtils.disposeObject(this._beadPic);
            this._beadPic = null;
         }
      }
      
      private function disposeDragBeadPic() : void
      {
         if(this._dragBeadPic)
         {
            this._dragBeadPic.gotoAndStop(this._dragBeadPic.totalFrames);
            ObjectUtils.disposeObject(this._dragBeadPic);
            this._dragBeadPic = null;
         }
      }
      
      override public function dragDrop(param1:DragEffect) : void
      {
         var _loc2_:InventoryItemInfo = null;
         if(PlayerManager.Instance.Self.bagLocked)
         {
            DragManager.acceptDrag(this);
            BaglockedManager.Instance.show();
            return;
         }
         _loc2_ = param1.data as InventoryItemInfo;
         if(_loc2_.Hole1 == 19)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.beadSystem.mostHightLevel"));
            return;
         }
         if(_loc2_.Hole1 == 1)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.beadSystem.lvOneCanntUpgrade"));
         }
         if(_loc2_ && param1.action != DragEffect.SPLIT)
         {
            this._invenItemInfo = _loc2_;
            this._itemInfo = _loc2_;
            this.info = _loc2_;
            SocketManager.Instance.out.sendBeadEquip(_loc2_.Place,31);
         }
         DragManager.acceptDrag(this);
         BeadLeadManager.Instance.beadLeadTwo();
      }
      
      override public function dragStart() : void
      {
         SoundManager.instance.play("008");
         if(_info && !locked && stage && _allowDrag)
         {
            if(DragManager.startDrag(this,_info,createDragImg(),stage.mouseX,stage.mouseY,DragEffect.MOVE))
            {
               this.dragHidePicTxt();
            }
         }
      }
      
      override protected function onMouseOut(param1:MouseEvent) : void
      {
         super.onMouseOut(param1);
      }
      
      override protected function onMouseOver(param1:MouseEvent) : void
      {
         if(this._upTip)
         {
            this._upTip.showTip(info);
         }
         super.onMouseOver(param1);
      }
      
      override public function dragStop(param1:DragEffect) : void
      {
         SoundManager.instance.play("008");
         if(param1.action == DragEffect.MOVE && !param1.target)
         {
            param1.action = DragEffect.NONE;
         }
         this.disposeDragBeadPic();
         this.dragShowPicTxt();
         super.dragStop(param1);
      }
      
      override protected function onMouseClick(param1:MouseEvent) : void
      {
         dispatchEvent(new CellEvent(CellEvent.ITEM_CLICK,this));
      }
      
      override protected function updateSize(param1:Sprite) : void
      {
         if(param1)
         {
            param1.scaleX = 0.8;
            param1.scaleY = 0.8;
            if(_picPos != null)
            {
               param1.x = _picPos.x;
            }
            else
            {
               param1.x = param1.x - param1.width / 2 + _contentWidth / 2;
            }
            if(_picPos != null)
            {
               param1.y = _picPos.y;
            }
            else
            {
               param1.y = param1.y - param1.height / 2 + _contentHeight / 2;
            }
         }
      }
      
      public function get invenItemInfo() : InventoryItemInfo
      {
         return this._invenItemInfo;
      }
      
      public function set invenItemInfo(param1:InventoryItemInfo) : void
      {
         this._invenItemInfo = param1;
      }
      
      private function dragHidePicTxt() : void
      {
         this._nameTxt.visible = false;
         if(this._lockIcon)
         {
            this._lockIcon.visible = false;
         }
      }
      
      private function dragShowPicTxt() : void
      {
         this._nameTxt.visible = true;
         if(this.invenItemInfo.IsUsed && this._lockIcon)
         {
            this._lockIcon.visible = true;
         }
      }
      
      override public function dispose() : void
      {
         if(this.info)
         {
            SocketManager.Instance.out.sendBeadEquip(31,-1);
         }
         if(this._beadPic)
         {
            this.disposeBeadPic();
         }
         if(this._dragBeadPic)
         {
            this.disposeDragBeadPic();
         }
         if(this._shiner)
         {
            ObjectUtils.disposeObject(this._shiner);
         }
         this._shiner = null;
         if(this._upgradeMC)
         {
            ObjectUtils.disposeObject(this._upgradeMC);
         }
         this._upgradeMC = null;
         beadSystemManager.Instance.removeEventListener(BeadEvent.PLAYUPGRADEMC,this.__startPlay);
         super.dispose();
      }
   }
}
