package store.newFusion.data
{
   import com.pickgliss.ui.controls.cell.INotSameHeightListCellData;
   import ddt.data.BagInfo;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.manager.ItemManager;
   import ddt.manager.PlayerManager;
   
   public class FusionNewVo implements INotSameHeightListCellData
   {
      
      public static const EQUIP_TYPE:int = 1;
      
      public static const DRILL_TYPE:int = 2;
      
      public static const COMBINE_TYPE:int = 3;
      
      public static const OTHER_TYPE:int = 4;
       
      
      private var _equipBag:BagInfo;
      
      private var _propBag:BagInfo;
      
      public var FusionID:int;
      
      public var Reward:int;
      
      public var Item1:int;
      
      public var Count1:int;
      
      public var Item2:int;
      
      public var Count2:int;
      
      public var Item3:int;
      
      public var Count3:int;
      
      public var Item4:int;
      
      public var Count4:int;
      
      public var Formula:int;
      
      private var _FusionRate:int;
      
      public var FusionType:int;
      
      public function FusionNewVo()
      {
         this._equipBag = PlayerManager.Instance.Self.getBag(BagInfo.EQUIPBAG);
         this._propBag = PlayerManager.Instance.Self.getBag(BagInfo.PROPBAG);
         super();
      }
      
      public function get fusionItemInfo() : ItemTemplateInfo
      {
         return ItemManager.Instance.getTemplateById(this.Reward);
      }
      
      public function get isNeedItem1() : Boolean
      {
         return this.Item1 != -1 && this.Count1 > 0;
      }
      
      public function get item1Count() : int
      {
         var _loc1_:int = this._equipBag.getBagItemCountByTemplateId(this.Item1);
         var _loc2_:int = this._propBag.getItemCountByTemplateId(this.Item1);
         return _loc1_ + _loc2_;
      }
      
      public function get isNeedItem2() : Boolean
      {
         return this.Item2 != -1 && this.Count2 > 0;
      }
      
      public function get item2Count() : int
      {
         var _loc1_:int = this._equipBag.getBagItemCountByTemplateId(this.Item2);
         var _loc2_:int = this._propBag.getItemCountByTemplateId(this.Item2);
         return _loc1_ + _loc2_;
      }
      
      public function get isNeedItem3() : Boolean
      {
         return this.Item3 != -1 && this.Count3 > 0;
      }
      
      public function get item3Count() : int
      {
         var _loc1_:int = this._equipBag.getBagItemCountByTemplateId(this.Item3);
         var _loc2_:int = this._propBag.getItemCountByTemplateId(this.Item3);
         return _loc1_ + _loc2_;
      }
      
      public function get isNeedItem4() : Boolean
      {
         return this.Item4 != -1 && this.Count4 > 0;
      }
      
      public function get item4Count() : int
      {
         var _loc1_:int = this._equipBag.getBagItemCountByTemplateId(this.Item4);
         var _loc2_:int = this._propBag.getItemCountByTemplateId(this.Item4);
         return _loc1_ + _loc2_;
      }
      
      public function get canFusionCount() : int
      {
         var _loc1_:int = int.MAX_VALUE;
         if(this.isNeedItem1)
         {
            _loc1_ = this.item1Count / this.Count1;
         }
         var _loc2_:int = int.MAX_VALUE;
         if(this.isNeedItem2)
         {
            _loc2_ = this.item2Count / this.Count2;
         }
         var _loc3_:int = int.MAX_VALUE;
         if(this.isNeedItem3)
         {
            _loc3_ = this.item3Count / this.Count3;
         }
         var _loc4_:int = int.MAX_VALUE;
         if(this.isNeedItem4)
         {
            _loc4_ = this.item4Count / this.Count4;
         }
         var _loc5_:int = Math.min(_loc1_,_loc2_,_loc3_,_loc4_);
         return _loc5_ == int.MAX_VALUE?int(0):int(_loc5_);
      }
      
      public function get FusionRate() : int
      {
         return this._FusionRate;
      }
      
      public function set FusionRate(param1:int) : void
      {
         this._FusionRate = param1 / 1000 > 1?int(int(param1 / 1000)):int(1);
      }
      
      public function getItemInfoByIndex(param1:int) : ItemTemplateInfo
      {
         if(!this["isNeedItem" + param1])
         {
            return null;
         }
         return ItemManager.Instance.getTemplateById(this["Item" + param1]);
      }
      
      public function getItemNeedCount(param1:int) : int
      {
         return this["Count" + param1];
      }
      
      public function getItemHadCount(param1:int) : int
      {
         return this["item" + param1 + "Count"];
      }
      
      public function getCellHeight() : Number
      {
         return 26;
      }
      
      public function isNeedPopBindTipWindow(param1:int) : Boolean
      {
         var _loc2_:int = this.getCanFusionCountByBindType(1);
         var _loc3_:int = this.getCanFusionCountByBindType(2);
         if(param1 > _loc2_ + _loc3_)
         {
            return true;
         }
         return false;
      }
      
      public function getCanFusionCountByBindType(param1:int) : int
      {
         var _loc2_:int = int.MAX_VALUE;
         if(this.isNeedItem1)
         {
            _loc2_ = this.getItemCountByIndexBindType(1,param1) / this.Count1;
         }
         var _loc3_:int = int.MAX_VALUE;
         if(this.isNeedItem2)
         {
            _loc3_ = this.getItemCountByIndexBindType(2,param1) / this.Count2;
         }
         var _loc4_:int = int.MAX_VALUE;
         if(this.isNeedItem3)
         {
            _loc4_ = this.getItemCountByIndexBindType(3,param1) / this.Count3;
         }
         var _loc5_:int = int.MAX_VALUE;
         if(this.isNeedItem4)
         {
            _loc5_ = this.getItemCountByIndexBindType(4,param1) / this.Count4;
         }
         var _loc6_:int = Math.min(_loc2_,_loc3_,_loc4_,_loc5_);
         return _loc6_ == int.MAX_VALUE?int(0):int(_loc6_);
      }
      
      private function getItemCountByIndexBindType(param1:int, param2:int) : int
      {
         var _loc3_:int = this._equipBag.getBagItemCountByTemplateIdBindType(this["Item" + param1],param2);
         var _loc4_:int = this._propBag.getItemCountByTemplateIdBindType(this["Item" + param1],param2);
         return _loc3_ + _loc4_;
      }
   }
}
