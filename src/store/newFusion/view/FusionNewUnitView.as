package store.newFusion.view
{
   import com.pickgliss.events.ListItemEvent;
   import com.pickgliss.geom.IntPoint;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.ListPanel;
   import com.pickgliss.ui.controls.SelectedButton;
   import com.pickgliss.ui.controls.list.IListModel;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.events.BagEvent;
   import ddt.manager.PlayerManager;
   import ddt.manager.SoundManager;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import store.newFusion.FusionNewManager;
   import store.newFusion.data.FusionNewVo;
   
   public class FusionNewUnitView extends Sprite implements Disposeable
   {
      
      public static const SELECTED_CHANGE:String = "fusionNewUnitView_selected_change";
       
      
      private var _index:int;
      
      private var _rightView:FusionNewRightView;
      
      private var _selectedBtn:SelectedButton;
      
      private var _bg:Bitmap;
      
      private var _list:ListPanel;
      
      private var _isFilter:Boolean = false;
      
      private var _dataList:Array;
      
      private var _selectedValue:FusionNewVo;
      
      public function FusionNewUnitView(param1:int, param2:FusionNewRightView)
      {
         super();
         this._index = param1;
         this._rightView = param2;
         this.initView();
         this.initEvent();
         this.initData();
         this.initStatus();
      }
      
      private function initStatus() : void
      {
         if(this._index == 1)
         {
            this.extendSelecteTheFirst();
         }
      }
      
      private function extendSelecteTheFirst() : void
      {
         this.extendHandler();
         this.autoSelect();
      }
      
      private function autoSelect() : void
      {
         var _loc3_:int = 0;
         var _loc4_:IntPoint = null;
         var _loc1_:IListModel = this._list.list.model;
         var _loc2_:int = _loc1_.getSize();
         if(_loc2_ > 0)
         {
            if(!this._selectedValue)
            {
               this._selectedValue = _loc1_.getElementAt(0) as FusionNewVo;
            }
            _loc3_ = _loc1_.indexOf(this._selectedValue);
            _loc4_ = new IntPoint(0,_loc1_.getCellPosFromIndex(_loc3_));
            this._list.list.viewPosition = _loc4_;
            this._list.list.currentSelectedIndex = _loc3_;
         }
         else
         {
            this._selectedValue = null;
         }
         this._rightView.refreshView(this._selectedValue);
      }
      
      public function set isFilter(param1:Boolean) : void
      {
         this._isFilter = param1;
         this.refreshList();
      }
      
      private function initData() : void
      {
         this._dataList = this.getDataList();
         this._list.vectorListModel.clear();
         this._list.vectorListModel.appendAll(this._dataList);
         this._list.list.updateListView();
      }
      
      private function refreshList() : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc1_:Array = [];
         if(this._isFilter)
         {
            _loc2_ = this._dataList.length;
            _loc3_ = 0;
            while(_loc3_ < _loc2_)
            {
               if((this._dataList[_loc3_] as FusionNewVo).canFusionCount > 0)
               {
                  _loc1_.push(this._dataList[_loc3_]);
               }
               _loc3_++;
            }
         }
         else
         {
            _loc1_ = this._dataList;
         }
         this._list.vectorListModel.clear();
         this._list.vectorListModel.appendAll(_loc1_);
         this._list.list.updateListView();
         if(this._selectedValue && _loc1_.indexOf(this._selectedValue) == -1)
         {
            this._selectedValue = null;
         }
         if(this._list.visible)
         {
            this.autoSelect();
         }
      }
      
      private function initView() : void
      {
         this._selectedBtn = this.getSelectedBtn();
         this._bg = ComponentFactory.Instance.creatBitmap("asset.newFusion.selectedUnitBg");
         this._bg.visible = false;
         this._list = ComponentFactory.Instance.creatComponentByStylename("store.newFusion.unitCellList");
         this._list.visible = false;
         addChild(this._selectedBtn);
         addChild(this._bg);
         addChild(this._list);
      }
      
      private function initEvent() : void
      {
         this._selectedBtn.addEventListener(MouseEvent.CLICK,this.clickHandler,false,0,true);
         this._list.list.addEventListener(ListItemEvent.LIST_ITEM_CLICK,this.__itemClick);
         PlayerManager.Instance.Self.Bag.addEventListener(BagEvent.UPDATE,this.updateBag);
         PlayerManager.Instance.Self.PropBag.addEventListener(BagEvent.UPDATE,this.updateBag);
      }
      
      private function updateBag(param1:BagEvent) : void
      {
         this.refreshList();
      }
      
      private function __itemClick(param1:ListItemEvent) : void
      {
         SoundManager.instance.play("008");
         this._selectedValue = param1.cellValue as FusionNewVo;
         if(this._rightView)
         {
            this._rightView.refreshView(this._selectedValue);
         }
      }
      
      private function clickHandler(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         this.extendSelecteTheFirst();
         dispatchEvent(new Event(SELECTED_CHANGE));
      }
      
      private function extendHandler() : void
      {
         this._bg.visible = true;
         this._list.visible = true;
         this._selectedBtn.enable = false;
      }
      
      public function unextendHandler() : void
      {
         this._selectedBtn.selected = false;
         this._selectedBtn.enable = true;
         this._bg.visible = false;
         this._list.visible = false;
         this._selectedValue = null;
      }
      
      private function getDataList() : Array
      {
         var _loc1_:Array = null;
         switch(this._index)
         {
            case 1:
               _loc1_ = FusionNewManager.instance.getDataListByType(FusionNewVo.EQUIP_TYPE);
               break;
            case 2:
               _loc1_ = FusionNewManager.instance.getDataListByType(FusionNewVo.DRILL_TYPE);
               break;
            case 3:
               _loc1_ = FusionNewManager.instance.getDataListByType(FusionNewVo.COMBINE_TYPE);
               break;
            case 4:
               _loc1_ = FusionNewManager.instance.getDataListByType(FusionNewVo.OTHER_TYPE);
               break;
            default:
               _loc1_ = [];
         }
         return Boolean(_loc1_)?_loc1_:[];
      }
      
      private function getSelectedBtn() : SelectedButton
      {
         var _loc1_:SelectedButton = null;
         switch(this._index)
         {
            case 1:
               _loc1_ = ComponentFactory.Instance.creatComponentByStylename("store.newFusion.equipBtn");
               break;
            case 2:
               _loc1_ = ComponentFactory.Instance.creatComponentByStylename("store.newFusion.drillBtn");
               break;
            case 3:
               _loc1_ = ComponentFactory.Instance.creatComponentByStylename("store.newFusion.combineBtn");
               break;
            case 4:
               _loc1_ = ComponentFactory.Instance.creatComponentByStylename("store.newFusion.otherBtn");
         }
         return _loc1_;
      }
      
      override public function get height() : Number
      {
         if(this._selectedBtn && this._bg)
         {
            if(this._bg.visible)
            {
               return this._bg.y + this._bg.height;
            }
            return this._selectedBtn.height;
         }
         return super.height;
      }
      
      private function removeEvent() : void
      {
         if(this._selectedBtn)
         {
            this._selectedBtn.removeEventListener(MouseEvent.CLICK,this.clickHandler);
         }
         if(this._list)
         {
            this._list.list.removeEventListener(ListItemEvent.LIST_ITEM_CLICK,this.__itemClick);
         }
         PlayerManager.Instance.Self.Bag.removeEventListener(BagEvent.UPDATE,this.updateBag);
         PlayerManager.Instance.Self.PropBag.removeEventListener(BagEvent.UPDATE,this.updateBag);
      }
      
      public function dispose() : void
      {
         this.removeEvent();
         ObjectUtils.disposeAllChildren(this);
         this._selectedBtn = null;
         this._bg = null;
         this._list = null;
         this._rightView = null;
         this._selectedValue = null;
         this._dataList = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
