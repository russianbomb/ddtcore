package store.newFusion.view
{
   import bagAndInfo.cell.BagCell;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.SelectedCheckButton;
   import com.pickgliss.ui.controls.container.VBox;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.LanguageMgr;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.DisplayObject;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.utils.Dictionary;
   import store.HelpFrame;
   import store.IStoreViewBG;
   
   public class FusionNewMainView extends Sprite implements IStoreViewBG
   {
       
      
      private var _leftBg:Bitmap;
      
      private var _vbox:VBox;
      
      private var _unitList:Vector.<FusionNewUnitView>;
      
      private var _rightView:FusionNewRightView;
      
      private var _canFusionSCB:SelectedCheckButton;
      
      private var _helpBtn:BaseButton;
      
      public function FusionNewMainView()
      {
         super();
         this.initView();
         this.initEvent();
      }
      
      private function initView() : void
      {
         var _loc2_:FusionNewUnitView = null;
         this._leftBg = ComponentFactory.Instance.creatBitmap("asset.newFusion.leftBg");
         this._rightView = new FusionNewRightView();
         PositionUtils.setPos(this._rightView,"store.newFusion.mainView.rightViewPos");
         this._canFusionSCB = ComponentFactory.Instance.creatComponentByStylename("store.newFusion.canFusionSCB");
         this._vbox = new VBox();
         PositionUtils.setPos(this._vbox,"store.newFusion.mainView.vboxPos");
         this._vbox.spacing = 2;
         this._unitList = new Vector.<FusionNewUnitView>();
         var _loc1_:int = 1;
         while(_loc1_ <= 4)
         {
            _loc2_ = new FusionNewUnitView(_loc1_,this._rightView);
            _loc2_.addEventListener(FusionNewUnitView.SELECTED_CHANGE,this.refreshView,false,0,true);
            this._vbox.addChild(_loc2_);
            this._unitList.push(_loc2_);
            _loc1_++;
         }
         this._helpBtn = ComponentFactory.Instance.creatComponentByStylename("store.newFusion.HelpButton");
         addChild(this._leftBg);
         addChild(this._vbox);
         addChild(this._canFusionSCB);
         addChild(this._helpBtn);
         addChild(this._rightView);
      }
      
      private function initEvent() : void
      {
         this._canFusionSCB.addEventListener(MouseEvent.CLICK,this.canFusionChangeHandler,false,0,true);
         this._helpBtn.addEventListener(MouseEvent.CLICK,this.helpBtnClickHandler,false,0,true);
      }
      
      private function helpBtnClickHandler(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         param1.stopImmediatePropagation();
         var _loc2_:DisplayObject = ComponentFactory.Instance.creat("store.newFusion.HelpPrompt");
         var _loc3_:HelpFrame = ComponentFactory.Instance.creat("store.newFusion.HelpFrame");
         _loc3_.setView(_loc2_);
         _loc3_.titleText = LanguageMgr.GetTranslation("store.view.HelpButtonText");
         LayerManager.Instance.addToLayer(_loc3_,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
      }
      
      private function canFusionChangeHandler(param1:MouseEvent) : void
      {
         var _loc2_:FusionNewUnitView = null;
         SoundManager.instance.play("008");
         for each(_loc2_ in this._unitList)
         {
            _loc2_.isFilter = this._canFusionSCB.selected;
         }
      }
      
      private function refreshView(param1:Event) : void
      {
         var _loc3_:FusionNewUnitView = null;
         var _loc2_:FusionNewUnitView = param1.target as FusionNewUnitView;
         for each(_loc3_ in this._unitList)
         {
            if(_loc3_ != _loc2_)
            {
               _loc3_.unextendHandler();
            }
         }
         this._vbox.arrange();
      }
      
      public function dragDrop(param1:BagCell) : void
      {
      }
      
      public function refreshData(param1:Dictionary) : void
      {
      }
      
      public function updateData() : void
      {
      }
      
      public function hide() : void
      {
         this.visible = false;
      }
      
      public function show() : void
      {
         this.visible = true;
      }
      
      private function removeEvent() : void
      {
         if(this._canFusionSCB)
         {
            this._canFusionSCB.removeEventListener(MouseEvent.CLICK,this.canFusionChangeHandler);
         }
         if(this._helpBtn)
         {
            this._helpBtn.removeEventListener(MouseEvent.CLICK,this.helpBtnClickHandler);
         }
      }
      
      public function dispose() : void
      {
         ObjectUtils.disposeAllChildren(this);
         this._leftBg = null;
         this._vbox = null;
         this._canFusionSCB = null;
         this._helpBtn = null;
         this._rightView = null;
         this._unitList = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
