package ddt.utils
{
   public class DatetimeHelper
   {
      
      public static const millisecondsPerMinute:int = 1000 * 60;
      
      public static const millisecondsPerHour:int = 1000 * 60 * 60;
      
      public static const millisecondsPerDay:int = 1000 * 60 * 60 * 24;
       
      
      public function DatetimeHelper()
      {
         super();
      }
   }
}
