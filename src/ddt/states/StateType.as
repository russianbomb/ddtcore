package ddt.states
{
   public class StateType
   {
      
      public static const DEAFULT:String = "default";
      
      public static const LOGIN:String = "login";
      
      public static const CREATE_PLAYER:String = "create_player";
      
      public static const MAIN:String = "main";
      
      public static const ROOM_LOADING:String = "roomLoading";
      
      public static const ENCOUNTER_LOADING:String = "encounterLoading";
      
      public static const ROOM_LIST:String = "roomlist";
      
      public static const MATCH_ROOM:String = "matchRoom";
      
      public static const CHALLENGE_ROOM:String = "challengeRoom";
      
      public static const DUNGEON_ROOM:String = "dungeonRoom";
      
      public static const MISSION_ROOM:String = "missionResult";
      
      public static const FIGHTING:String = "fighting";
      
      public static const SHOP:String = "shop";
      
      public static const STORE_ARM:String = "store_arm";
      
      public static const TOFFLIST:String = "tofflist";
      
      public static const TASK:String = "task";
      
      public static const AUCTION:String = "auction";
      
      public static const CONSORTIA:String = "consortia";
      
      public static const CONSORTIASTORE:String = "consortia_store";
      
      public static const DDTCHURCH_ROOM_LIST:String = "ddtchurchroomlist";
      
      public static const CHURCH_ROOM:String = "churchRoom";
      
      public static const CIVIL:String = "civil";
      
      public static const GAME_LOADING:String = "gameLoading";
      
      public static const DUNGEON_LIST:String = "dungeon";
      
      public static const TRAINER1:String = "trainer1";
      
      public static const TRAINER2:String = "trainer2";
      
      public static const LODING_TRAINER:String = "loadingTrainer";
      
      public static const FRESHMAN_ROOM1:String = "freshmanRoom1";
      
      public static const FRESHMAN_ROOM2:String = "freshmanRoom2";
      
      public static const FIGHT_LIB:String = "fightLib";
      
      public static const FIGHT_LIB_GAMEVIEW:String = "fightLabGameView";
      
      public static const HOT_SPRING_ROOM_LIST:String = "hotSpringRoomList";
      
      public static const HOT_SPRING_ROOM:String = "hotSpringRoom";
      
      public static const ACADEMY_REGISTRATION:String = "academyRegistration";
      
      public static const LITTLEHALL:String = "littleHall";
      
      public static const LITTLEGAME:String = "littleGame";
      
      public static const CAMP_STATE1:String = "camp_state1";
      
      public static const FARM:String = "farm";
      
      public static const ENTERTAINMENT:String = "entertainment";
      
      public static const WORLDBOSS_ROOM:String = "worldboss";
      
      public static const WORLDBOSS_AWARD:String = "worldbossAward";
      
      public static const WORLDBOSS_FIGHT_ROOM:String = "worldbossRoom";
      
      public static const CONSORTIA_BATTLE_SCENE:String = "consortiaBattleScene";
      
      public static const LOTTERY_HALL:String = "lotteryHall";
      
      public static const LOTTERY_CARD:String = "lotteryCard";
      
      public static const DICE_SYSTEM:String = "diceSystem";
      
      public static const TREASURE:String = "treasure";
      
      public static const FIGHTFOOTBALLTIME:String = "fightFootballTime";
      
      public static const PYRAMID:String = "pyramid";
      
      public static const CHRISTMAS:String = "christmas";
      
      public static const CHRISTMAS_ROOM:String = "christmasroom";
      
      public static const CAMP_BATTLE_SCENE:String = "campBattleScene";
      
      public static const CAMP_BATTLE_LOADING:String = "campBattleSceneloaing";
      
      public static const SEVEN_DOUBLE_SCENE:String = "sevenDoubleScene";
      
      public static const SUPER_WINNER:String = "superWinner";
      
      public static const SINGLEBATTLE_MATCHING:String = "singleBattleMatching";
      
      public static const LIGHTROAD_WINDOW:String = "lightRoadWindow";
      
      public static const KING_DIVISION:String = "kingdivision";
      
      public static const BOGU_ADVENTURE:String = "boguadventure";
      
      public static const ESCORT:String = "escort";
       
      
      public function StateType()
      {
         super();
      }
   }
}
