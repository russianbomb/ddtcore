package ddt.constants
{
   public class CacheConsts
   {
      
      public static const ALERT_IN_FIGHT:String = "alertInFight";
      
      public static const ALERT_IN_MOVIE:String = "alertInMovie";
      
      public static const ALERT_IN_HALL:String = "alertInHall";
      
      public static const ALERT_IN_HOTSPRING:String = "alertInHotSpring";
      
      public static const ALERT_IN_MARRY:String = "alertInMarry";
      
      public static const WORLDBOSS_IN_ROOM:String = "alertInBossRoom";
      
      public static const CONSORTIA_BATTLE_IN_ROOM:String = "consortiaBattleInRoom";
      
      public static const ALERT_IN_DICESYSTEM:String = "alertInDiceSystem";
      
      public static const TREASURE:String = "treasure";
      
      public static const TRANSNATIONAL:String = "transnational";
      
      public static const ALERT_IN_PYRAMID:String = "alertInPyramid";
      
      public static const SEVEN_DOUBLE_IN_ROOM:String = "sevenDoubleInRoom";
      
      public static const CHRISTMAS_IN_ROOM:String = "alertInChristmasRoom";
      
      public static const ALERT_IN_BOGU_ADVENTURE:String = "AlaertInBoguAdventure";
       
      
      public function CacheConsts()
      {
         super();
      }
   }
}
