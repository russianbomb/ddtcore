package ddt.view.vote
{
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.ScrollPanel;
   import com.pickgliss.ui.controls.SelectedButtonGroup;
   import com.pickgliss.ui.controls.TextButton;
   import com.pickgliss.ui.controls.container.SimpleTileList;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.vote.VoteQuestionInfo;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.SoundManager;
   import ddt.manager.VoteManager;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.FocusEvent;
   import flash.events.MouseEvent;
   
   public class VoteView extends Frame
   {
      
      public static var OK_CLICK:String = "Ok_click";
      
      public static var VOTEVIEW_CLOSE:String = "voteView_close";
      
      private static var CELL_1_GOODS:int = 11904;
      
      private static var CELL_2_GOODS:int = 11032;
      
      private static var TWOLINEHEIGHT:int = 31;
      
      private static var questionBGHeight_2line:int = 188;
      
      private static var questionBGY_2line:int = 143;
      
      private static var questionContentY_2line:int = 158;
      
      private static const NUMBER:int = 2;
       
      
      private var _voteInfo:VoteQuestionInfo;
      
      private var _choseAnswerID:int;
      
      private var _itemArr:Vector.<VoteSelectItem>;
      
      private var _answerGroup:SelectedButtonGroup;
      
      private var _okBtn:TextButton;
      
      private var _answerBG:ScaleBitmapImage;
      
      private var _questionContent:FilterFrameText;
      
      private var _voteProgress:FilterFrameText;
      
      private var _selectSp:Sprite;
      
      private var _bg:ScaleBitmapImage;
      
      private var _itemList:SimpleTileList;
      
      private var _inputTxt:FilterFrameText;
      
      private var panel:ScrollPanel;
      
      public function VoteView()
      {
         super();
         this.addEvent();
      }
      
      public function get choseAnswerID() : int
      {
         return this._choseAnswerID;
      }
      
      override protected function init() : void
      {
         super.init();
         this._itemArr = new Vector.<VoteSelectItem>();
         escEnable = true;
         titleText = LanguageMgr.GetTranslation("ddt.view.vote.title");
         this._bg = ComponentFactory.Instance.creatComponentByStylename("vote.VoteView.bg");
         this._answerBG = ComponentFactory.Instance.creatComponentByStylename("vote.answerBG");
         this._questionContent = ComponentFactory.Instance.creat("vote.questionContent");
         this._voteProgress = ComponentFactory.Instance.creat("vote.progress");
         this._okBtn = ComponentFactory.Instance.creatComponentByStylename("vote.okBtn");
         this._selectSp = new Sprite();
         this._itemList = ComponentFactory.Instance.creatCustomObject("vote.itemList",[NUMBER]);
         this.panel = ComponentFactory.Instance.creatComponentByStylename("vote.vote.VoteanswerPanel");
         this.panel.setView(this._itemList);
         addToContent(this._bg);
         addToContent(this._okBtn);
         this._selectSp.x = -4;
         this._selectSp.y = 58;
         this._selectSp.addChild(this._answerBG);
         this._selectSp.addChild(this._questionContent);
         this._selectSp.addChild(this._voteProgress);
         this._selectSp.addChild(this.panel);
         addToContent(this._selectSp);
         this._okBtn.text = LanguageMgr.GetTranslation("ok");
      }
      
      public function set info(param1:VoteQuestionInfo) : void
      {
         this._voteInfo = param1;
         if(this._voteInfo.questionType == 1)
         {
            this._itemList.hSpace = 60;
         }
         else if(this._voteInfo.questionType == 2)
         {
            this._itemList.hSpace = 10;
         }
         this.update();
      }
      
      private function update() : void
      {
         var _loc2_:int = 0;
         var _loc3_:Boolean = false;
         var _loc4_:VoteSelectItem = null;
         this.clear();
         this._voteProgress.text = "进度" + VoteManager.Instance.count + "/" + VoteManager.Instance.questionLength;
         if(this._voteInfo.questionType == 1)
         {
            if(this._voteInfo.multiple)
            {
               this._answerGroup = new SelectedButtonGroup(true,this._voteInfo.answerLength);
            }
            else
            {
               this._answerGroup = new SelectedButtonGroup();
            }
            this._answerGroup.addEventListener(Event.CHANGE,this.__changeHandler);
         }
         this._itemArr = new Vector.<VoteSelectItem>();
         var _loc1_:int = 0;
         this._questionContent.text = "    " + this._voteInfo.question;
         if(this._voteInfo.questionType != 3)
         {
            _loc2_ = 0;
            while(_loc2_ < this._voteInfo.answer.length)
            {
               _loc1_++;
               if(this._voteInfo.questionType == 1 && this._voteInfo.otherSelect && _loc2_ == this._voteInfo.answer.length - 1)
               {
                  _loc3_ = true;
               }
               _loc4_ = new VoteSelectItem(this._voteInfo.questionType,this._voteInfo.answer[_loc2_],_loc3_);
               _loc4_.text = _loc1_ + ". " + this._voteInfo.answer[_loc2_].answer;
               this._itemList.addChild(_loc4_);
               this._itemArr.push(_loc4_);
               if(this._voteInfo.questionType == 1)
               {
                  this._answerGroup.addSelectItem(_loc4_.item);
               }
               _loc4_.initEvent();
               _loc2_++;
            }
         }
         else
         {
            this._inputTxt = ComponentFactory.Instance.creatComponentByStylename("vote.inputTxt");
            this._inputTxt.text = LanguageMgr.GetTranslation("ddt.view.vote.defaultTxt");
            this._inputTxt.maxChars = 500;
            this._selectSp.addChild(this._inputTxt);
            this._inputTxt.setFocus();
            this._inputTxt.addEventListener(FocusEvent.FOCUS_IN,this.__searchInputFocusIn);
            this._inputTxt.addEventListener(FocusEvent.FOCUS_OUT,this.__searchInputFocusOut);
         }
         this.panel.invalidateViewport();
      }
      
      private function __playSound(param1:Event) : void
      {
         SoundManager.instance.play("008");
      }
      
      private function clear() : void
      {
         this._questionContent.text = "";
         this._voteProgress.text = "";
         if(this._answerGroup)
         {
            this._answerGroup.removeEventListener(Event.CHANGE,this.__changeHandler);
            this._answerGroup.dispose();
            this._answerGroup = null;
         }
         ObjectUtils.disposeObject(this._inputTxt);
         this._inputTxt = null;
         var _loc1_:int = 0;
         while(_loc1_ < this._itemArr.length)
         {
            this._itemArr[_loc1_].dispose();
            this._itemArr[_loc1_] = null;
            _loc1_++;
         }
         this._itemArr = null;
         if(this._itemList)
         {
            this._itemList.removeAllChild();
         }
      }
      
      private function addEvent() : void
      {
         addEventListener(FrameEvent.RESPONSE,this.__responseHandler);
         this._okBtn.addEventListener(MouseEvent.CLICK,this.__clickHandler);
      }
      
      protected function __searchInputFocusIn(param1:FocusEvent) : void
      {
         if(this._inputTxt.text == LanguageMgr.GetTranslation("ddt.view.vote.defaultTxt"))
         {
            this._inputTxt.text = "";
         }
      }
      
      protected function __searchInputFocusOut(param1:FocusEvent) : void
      {
         if(this._inputTxt.text.length == 0)
         {
            this._inputTxt.text = LanguageMgr.GetTranslation("ddt.view.vote.defaultTxt");
         }
      }
      
      protected function __changeHandler(param1:Event) : void
      {
         if(this._itemArr[this._itemArr.length - 1].item.selected)
         {
            this._itemArr[this._itemArr.length - 1].inputEnable = true;
         }
         else
         {
            this._itemArr[this._itemArr.length - 1].inputEnable = false;
         }
      }
      
      private function __clickHandler(param1:MouseEvent) : void
      {
         var _loc3_:VoteSelectItem = null;
         var _loc4_:int = 0;
         var _loc5_:VoteSelectItem = null;
         SoundManager.instance.play("008");
         var _loc2_:Boolean = false;
         if(this._voteInfo.questionType == 1)
         {
            for each(_loc3_ in this._itemArr)
            {
               if(_loc3_.otherSelect && _loc3_.selected)
               {
                  if(_loc3_.content == "")
                  {
                     MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.view.vote.choseOne3"));
                     return;
                  }
                  _loc2_ = true;
                  break;
               }
            }
            if(!_loc2_)
            {
               _loc4_ = 0;
               while(_loc4_ < this._itemArr.length)
               {
                  if(this._itemArr[_loc4_].selected)
                  {
                     _loc2_ = true;
                     break;
                  }
                  _loc4_++;
               }
            }
         }
         else if(this._voteInfo.questionType == 2)
         {
            _loc2_ = true;
            for each(_loc5_ in this._itemArr)
            {
               if(_loc5_.score == 0)
               {
                  _loc2_ = false;
                  break;
               }
            }
         }
         else if(this._inputTxt && this._inputTxt.text != LanguageMgr.GetTranslation("ddt.view.vote.defaultTxt") && this._inputTxt.text.length > 0)
         {
            _loc2_ = true;
         }
         if(!_loc2_)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.view.vote.choseOne" + this._voteInfo.questionType));
            return;
         }
         dispatchEvent(new Event(OK_CLICK));
      }
      
      public function get selectAnswer() : String
      {
         var _loc2_:int = 0;
         var _loc3_:VoteSelectItem = null;
         var _loc1_:String = this._voteInfo.questionType + "|";
         if(this._voteInfo.questionType != 3)
         {
            _loc2_ = 0;
            while(_loc2_ < this._itemArr.length)
            {
               _loc3_ = this._itemArr[_loc2_];
               if(_loc3_.selected)
               {
                  switch(this._voteInfo.questionType)
                  {
                     case 1:
                        if(_loc3_.otherSelect)
                        {
                           _loc1_ = _loc1_ + _loc3_.answerId + "," + _loc3_.content + "|";
                        }
                        else
                        {
                           _loc1_ = _loc1_ + _loc3_.answerId + "|";
                        }
                        break;
                     case 2:
                        _loc1_ = _loc1_ + _loc3_.answerId + "," + _loc3_.score + "|";
                  }
               }
               _loc2_++;
            }
         }
         else
         {
            _loc1_ = _loc1_ + this._voteInfo.questionID + "," + this._inputTxt.text + "|";
         }
         return _loc1_;
      }
      
      private function removeEvent() : void
      {
         removeEventListener(FrameEvent.RESPONSE,this.__responseHandler);
         this._okBtn.removeEventListener(MouseEvent.CLICK,this.__clickHandler);
         if(this._inputTxt)
         {
            this._inputTxt.removeEventListener(FocusEvent.FOCUS_IN,this.__searchInputFocusIn);
            this._inputTxt.removeEventListener(FocusEvent.FOCUS_OUT,this.__searchInputFocusOut);
         }
      }
      
      private function __responseHandler(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         switch(param1.responseCode)
         {
            case FrameEvent.ESC_CLICK:
            case FrameEvent.CLOSE_CLICK:
               this.dispose();
               dispatchEvent(new Event(VOTEVIEW_CLOSE));
         }
      }
      
      override public function dispose() : void
      {
         this.removeEvent();
         this.clear();
         super.dispose();
         if(this._bg)
         {
            ObjectUtils.disposeObject(this._bg);
         }
         this._bg = null;
         if(this._answerBG)
         {
            ObjectUtils.disposeObject(this._answerBG);
         }
         this._answerBG = null;
         if(this._answerGroup)
         {
            ObjectUtils.disposeObject(this._answerGroup);
         }
         this._answerGroup = null;
         if(this._okBtn)
         {
            ObjectUtils.disposeObject(this._okBtn);
         }
         this._okBtn = null;
         if(this._questionContent)
         {
            ObjectUtils.disposeObject(this._questionContent);
         }
         this._questionContent = null;
         if(this._voteProgress)
         {
            ObjectUtils.disposeObject(this._voteProgress);
         }
         this._voteProgress = null;
         ObjectUtils.disposeObject(this._selectSp);
         this._selectSp = null;
         ObjectUtils.disposeObject(this._itemList);
         this._itemList = null;
         ObjectUtils.disposeObject(this.panel);
         this.panel = null;
         ObjectUtils.disposeObject(this._inputTxt);
         this._inputTxt = null;
         this._voteInfo = null;
         if(this.parent)
         {
            this.parent.removeChild(this);
         }
      }
   }
}
