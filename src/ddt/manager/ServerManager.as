package ddt.manager
{
   import baglocked.BagLockedController;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.loader.BaseLoader;
   import com.pickgliss.loader.QueueLoader;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import ddt.DDT;
   import ddt.data.ServerInfo;
   import ddt.data.analyze.ServerListAnalyzer;
   import ddt.data.player.SelfInfo;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.events.DuowanInterfaceEvent;
   import ddt.loader.LoaderCreate;
   import ddt.loader.StartupResourceLoader;
   import ddt.states.StateType;
   import ddt.view.MainToolBar;
import ddt.view.chat.ChatBugleView;

import email.manager.MailManager;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.utils.Timer;
   import flash.utils.setTimeout;
   import oldPlayerRegress.data.RegressData;
   import pet.sprite.PetSpriteController;
   import road7th.comm.PackageIn;
   import trainer.controller.SystemOpenPromptManager;
   
   [Event(name="change",type="flash.events.Event")]
   public class ServerManager extends EventDispatcher
   {
      
      public static const CHANGE_SERVER:String = "changeServer";
      
      public static var AUTO_UNLOCK:Boolean = false;
      
      private static const CONNENT_TIME_OUT:int = 30000;
      
      private static var _instance:ServerManager;
       
      
      private var _list:Vector.<ServerInfo>;
      
      private var _current:ServerInfo;
      
      private var _zoneName:String;
      
      private var _agentid:int;
      
      public var refreshFlag:Boolean = false;
      
      private var _connentTimer:Timer;
      
      private var _loaderQueue:QueueLoader;
      
      private var _requestCompleted:int;
      
      public function ServerManager()
      {
         super();
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.LOGIN,this.__onLoginComplete);
      }
      
      public static function get Instance() : ServerManager
      {
         if(_instance == null)
         {
            _instance = new ServerManager();
         }
         return _instance;
      }
      
      public function get zoneName() : String
      {
         return this._zoneName;
      }
      
      public function set zoneName(param1:String) : void
      {
         this._zoneName = param1;
         dispatchEvent(new Event(Event.CHANGE));
      }
      
      public function get AgentID() : int
      {
         return this._agentid;
      }
      
      public function set AgentID(param1:int) : void
      {
         this._agentid = param1;
      }
      
      public function set current(param1:ServerInfo) : void
      {
         this._current = param1;
      }
      
      public function get current() : ServerInfo
      {
         return this._current;
      }
      
      public function get list() : Vector.<ServerInfo>
      {
         return this._list;
      }
      
      public function set list(param1:Vector.<ServerInfo>) : void
      {
         this._list = param1;
         dispatchEvent(new Event(Event.CHANGE));
      }
      
      public function setup(param1:ServerListAnalyzer) : void
      {
         this._list = param1.list;
         this._agentid = param1.agentId;
         this._zoneName = param1.zoneName;
      }
      
      public function canAutoLogin() : Boolean
      {
         this.searchAvailableServer();
         return this._current != null;
      }
      
      public function connentCurrentServer() : void
      {
         SocketManager.Instance.isLogin = false;
         SocketManager.Instance.connect(this._current.IP,this._current.Port);
      }
      
      private function searchAvailableServer() : void
      {
         var _loc2_:int = 0;
         var _loc1_:SelfInfo = PlayerManager.Instance.Self;
         if(DDT.SERVER_ID != -1)
         {
            this._current = this.getServerInfoByID(DDT.SERVER_ID);
            return;
         }
         if(_loc1_.LastServerId != -1)
         {
            this._current = this.getServerInfoByID(_loc1_.LastServerId);
            return;
         }
         if(PathManager.solveRandomChannel())
         {
            _loc2_ = Math.round(Math.random() * (this._list.length - 1));
            this._current = this._list[_loc2_];
            while(this._current.State == 1)
            {
               _loc2_ = Math.round(Math.random() * (this._list.length - 1));
               this._current = this._list[_loc2_];
            }
         }
         else
         {
            this._current = this.searchServerByState(ServerInfo.UNIMPEDED);
            if(this._current == null)
            {
               this._current = this.searchServerByState(ServerInfo.HALF);
            }
         }
         if(this._current == null)
         {
            this._current = this._list[0];
         }
      }
      
      public function getServerInfoByID(param1:int) : ServerInfo
      {
         var _loc2_:int = 0;
         while(_loc2_ < this._list.length)
         {
            if(this._list[_loc2_].ID == param1)
            {
               return this._list[_loc2_];
            }
            _loc2_++;
         }
         return null;
      }
      
      private function searchServerByState(param1:int) : ServerInfo
      {
         var _loc2_:int = 0;
         while(_loc2_ < this._list.length)
         {
            if(this._list[_loc2_].State == param1)
            {
               return this._list[_loc2_];
            }
            _loc2_++;
         }
         return null;
      }
      
      public function connentServer(param1:ServerInfo) : Boolean
      {
         var _loc2_:BaseAlerFrame = null;
         if(param1 == null)
         {
            _loc2_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("tank.serverlist.ServerListPosView.choose"));
            this.alertControl(_loc2_);
            return false;
         }
         if(param1.MustLevel < PlayerManager.Instance.Self.Grade)
         {
            _loc2_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("tank.serverlist.ServerListPosView.your"));
            this.alertControl(_loc2_);
            return false;
         }
         if(param1.LowestLevel > PlayerManager.Instance.Self.Grade)
         {
            _loc2_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("tank.serverlist.ServerListPosView.low"));
            this.alertControl(_loc2_);
            return false;
         }
         if(SocketManager.Instance.socket.connected && SocketManager.Instance.socket.isSame(param1.IP,param1.Port) && SocketManager.Instance.isLogin)
         {
            StateManager.setState(StateType.MAIN);
            return false;
         }
         if(param1.State == ServerInfo.ALL_FULL)
         {
            _loc2_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("tank.serverlist.ServerListPosView.full"));
            this.alertControl(_loc2_);
            return false;
         }
         if(param1.State == ServerInfo.MAINTAIN)
         {
            _loc2_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("tank.serverlist.ServerListPosView.maintenance"));
            this.alertControl(_loc2_);
            return false;
         }
         this._current = param1;
         this.connentCurrentServer();
         dispatchEvent(new Event(CHANGE_SERVER));
         return true;
      }
      
      private function alertControl(param1:BaseAlerFrame) : void
      {
         param1.addEventListener(FrameEvent.RESPONSE,this.__alertResponse);
      }
      
      private function __alertResponse(param1:FrameEvent) : void
      {
         var _loc2_:BaseAlerFrame = param1.currentTarget as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.__alertResponse);
         _loc2_.dispose();
      }
      
      private function __onLoginComplete(param1:CrazyTankSocketEvent) : void
      {
         var _loc4_:String = null;
         var _loc5_:Array = null;
         var _loc6_:Boolean = false;
         var _loc7_:Boolean = false;
         var _loc8_:BaseAlerFrame = null;
         var packageIn:PackageIn = param1.pkg;
         var selfInfo:SelfInfo = PlayerManager.Instance.Self;
         if(packageIn.readByte() == 0)
         {
            selfInfo.beginChanges();
            SocketManager.Instance.isLogin = true;
            selfInfo.ZoneID = packageIn.readInt();
            selfInfo.Attack = packageIn.readInt();
            selfInfo.Defence = packageIn.readInt();
            selfInfo.Agility = packageIn.readInt();
            selfInfo.Luck = packageIn.readInt();
            selfInfo.GP = packageIn.readInt();
            selfInfo.Repute = packageIn.readInt();
            selfInfo.Gold = packageIn.readInt();
            selfInfo.Money = packageIn.readInt();
            selfInfo.BandMoney = packageIn.readInt();
            packageIn.readInt();
            selfInfo.Score = packageIn.readInt();
            selfInfo.Hide = packageIn.readInt();
            selfInfo.FightPower = packageIn.readInt();
            selfInfo.apprenticeshipState = packageIn.readInt();
            selfInfo.masterID = packageIn.readInt();
            selfInfo.setMasterOrApprentices(packageIn.readUTF());
            selfInfo.graduatesCount = packageIn.readInt();
            selfInfo.honourOfMaster = packageIn.readUTF();
            selfInfo.freezesDate = packageIn.readDate();
            selfInfo.typeVIP = packageIn.readByte();
            selfInfo.VIPLevel = packageIn.readInt();
            selfInfo.VIPExp = packageIn.readInt();
            selfInfo.VIPExpireDay = packageIn.readDate();
            selfInfo.LastDate = packageIn.readDate();
            selfInfo.VIPNextLevelDaysNeeded = packageIn.readInt();
            selfInfo.systemDate = packageIn.readDate();
            selfInfo.canTakeVipReward = packageIn.readBoolean();
            selfInfo.OptionOnOff = packageIn.readInt();
            selfInfo.AchievementPoint = packageIn.readInt();
            selfInfo.honor = packageIn.readUTF();
            TimeManager.Instance.totalGameTime = packageIn.readInt();
            selfInfo.Sex = packageIn.readBoolean();
            _loc4_ = packageIn.readUTF();
            _loc5_ = _loc4_.split("&");
            selfInfo.Style = _loc5_[0];
            selfInfo.Colors = _loc5_[1];
            selfInfo.Skin = packageIn.readUTF();
            selfInfo.ConsortiaID = packageIn.readInt();
            selfInfo.ConsortiaName = packageIn.readUTF();
            selfInfo.badgeID = packageIn.readInt();
            selfInfo.DutyLevel = packageIn.readInt();
            selfInfo.DutyName = packageIn.readUTF();
            selfInfo.Right = packageIn.readInt();
            selfInfo.consortiaInfo.ChairmanName = packageIn.readUTF();
            selfInfo.consortiaInfo.Honor = packageIn.readInt();
            selfInfo.consortiaInfo.Riches = packageIn.readInt();
            _loc6_ = packageIn.readBoolean();
            _loc7_ = selfInfo.bagPwdState && !selfInfo.bagLocked;
            selfInfo.bagPwdState = _loc6_;
            if(_loc7_)
            {
               setTimeout(this.releaseLock,1000);
            }
            selfInfo.bagLocked = _loc6_;
            selfInfo.questionOne = packageIn.readUTF();
            selfInfo.questionTwo = packageIn.readUTF();
            selfInfo.leftTimes = packageIn.readInt();
            selfInfo.LoginName = packageIn.readUTF();
            selfInfo.Nimbus = packageIn.readInt();
            TaskManager.instance.requestCanAcceptTask();
            selfInfo.PvePermission = packageIn.readUTF();
            selfInfo.fightLibMission = packageIn.readUTF();
            selfInfo.userGuildProgress = packageIn.readInt();
            selfInfo.LastSpaDate = packageIn.readDate();
            selfInfo.shopFinallyGottenTime = packageIn.readDate();
            selfInfo.UseOffer = packageIn.readInt();
            selfInfo.matchInfo.dailyScore = packageIn.readInt();
            selfInfo.matchInfo.dailyWinCount = packageIn.readInt();
            selfInfo.matchInfo.dailyGameCount = packageIn.readInt();
            selfInfo.DailyLeagueFirst = packageIn.readBoolean();
            selfInfo.DailyLeagueLastScore = packageIn.readInt();
            selfInfo.matchInfo.weeklyScore = packageIn.readInt();
            selfInfo.matchInfo.weeklyGameCount = packageIn.readInt();
            selfInfo.matchInfo.weeklyRanking = packageIn.readInt();
            selfInfo.spdTexpExp = packageIn.readInt();
            selfInfo.attTexpExp = packageIn.readInt();
            selfInfo.defTexpExp = packageIn.readInt();
            selfInfo.hpTexpExp = packageIn.readInt();
            selfInfo.lukTexpExp = packageIn.readInt();
            selfInfo.texpTaskCount = packageIn.readInt();
            selfInfo.texpCount = packageIn.readInt();
            selfInfo.texpTaskDate = packageIn.readDate();
            selfInfo.isOldPlayerHasValidEquitAtLogin = packageIn.readBoolean();
            selfInfo.badLuckNumber = packageIn.readInt();
            selfInfo.luckyNum = packageIn.readInt();
            selfInfo.lastLuckyNumDate = packageIn.readDate();
            selfInfo.lastLuckNum = packageIn.readInt();
            selfInfo.isOld = packageIn.readBoolean();
            selfInfo.CardSoul = packageIn.readInt();
            selfInfo.GetSoulCount = packageIn.readInt();
            selfInfo.uesedFinishTime = packageIn.readInt();
            selfInfo.totemId = packageIn.readInt();
            selfInfo.necklaceExp = packageIn.readInt();
            selfInfo.accumulativeLoginDays = packageIn.readInt();
            selfInfo.accumulativeAwardDays = packageIn.readInt();
            //Disable
//            BossBoxManager.instance.receiebox = packageIn.readInt();
//            BossBoxManager.instance.receieGrade = packageIn.readInt();
//            BossBoxManager.instance.needGetBoxTime = packageIn.readInt();
//            BossBoxManager.instance.currentGrade = PlayerManager.Instance.Self.Grade;
//            BossBoxManager.instance.startGradeChangeEvent();
            packageIn.readInt();
            packageIn.readInt();
            packageIn.readInt();
            selfInfo.isFirstDivorce = packageIn.readInt();
            selfInfo.PveEpicPermission = packageIn.readUTF();
            selfInfo.AccessLevel = packageIn.readInt();
            selfInfo.commitChanges();
            ChatManager.Instance.setup();
            ChatBugleView.instance.setup();
            MapManager.buildMap();
            PlayerManager.Instance.Self.loadRelatedPlayersInfo();
            MainToolBar.Instance.signEffectEnable = true;
            StateManager.setState(StateType.MAIN);
            ExternalInterfaceManager.sendTo360Agent(4);
            if(!StartupResourceLoader.firstEnterHall)
            {
               StartupResourceLoader.Instance.startLoadRelatedInfo();
               SocketManager.Instance.out.sendPlayerGift(selfInfo.ID);
            }
            if(DesktopManager.Instance.isDesktop)
            {
               setTimeout(TaskManager.instance.onDesktopApp,500);
            }
            PetSpriteController.Instance.setup();
            DuowanInterfaceManage.Instance.dispatchEvent(new DuowanInterfaceEvent(DuowanInterfaceEvent.ONLINE));
            if(selfInfo.isOld)
            {
               SocketManager.Instance.addEventListener(CrazyTankSocketEvent.REGRESS_RECVPACKS,this.__onRegressRecvPacks);
               SocketManager.Instance.out.sendRegressRecvPacks();
            }
            if(this.refreshFlag)
            {
               SystemOpenPromptManager.instance.showFrame();
               this.refreshFlag = false;
            }
            if(MailManager.Instance.linkChurchRoomId != -1)
            {
               SocketManager.Instance.out.sendEnterRoom(MailManager.Instance.linkChurchRoomId,"");
            }
            SocketManager.Instance.out.requestWonderfulActInit(3);
            this.addLoaderAgain();
         }
         else
         {
            _loc8_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("alert"),LanguageMgr.GetTranslation("ServerLinkError"));
            this.alertControl(_loc8_);
         }
      }
      
      private function addLoaderAgain() : void
      {
         this._loaderQueue = new QueueLoader();
         this._loaderQueue.addEventListener(Event.CHANGE,this.__onSetupSourceLoadChange);
         this._loaderQueue.addEventListener(Event.COMPLETE,this.__onSetupSourceLoadComplete);
         this.addLoader(LoaderCreate.Instance.createCallPropDataLoader());
         this._loaderQueue.start();
      }
      
      private function addLoader(param1:BaseLoader) : void
      {
         this._loaderQueue.addLoader(param1);
      }
      
      private function __onSetupSourceLoadChange(param1:Event) : void
      {
         this._requestCompleted = (param1.currentTarget as QueueLoader).completeCount;
      }
      
      private function __onSetupSourceLoadComplete(param1:Event) : void
      {
         var _loc2_:QueueLoader = param1.currentTarget as QueueLoader;
         _loc2_.removeEventListener(Event.COMPLETE,this.__onSetupSourceLoadComplete);
         _loc2_.removeEventListener(Event.CHANGE,this.__onSetupSourceLoadChange);
         _loc2_.dispose();
         _loc2_ = null;
      }
      
      protected function __onRegressRecvPacks(param1:CrazyTankSocketEvent) : void
      {
         RegressData.recvPacksInfo(param1.pkg);
      }
      
      private function releaseLock() : void
      {
         AUTO_UNLOCK = true;
         SocketManager.Instance.out.sendBagLocked(BagLockedController.PWD,2);
      }
   }
}
