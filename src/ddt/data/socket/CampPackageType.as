package ddt.data.socket
{
   public class CampPackageType
   {
      
      public static const ADD_ROLE_LIST:int = 1;
      
      public static const ADD_ROLE_LIST_EVENT:String = "ADD_ROLE_LIST_EVENT";
      
      public static const ROLE_MOVE:int = 2;
      
      public static const ROLE_MOVE_EVENT:String = "ROLE_MOVE_EVENT";
      
      public static const REMOVE_ROLE:int = 3;
      
      public static const REMOVE_ROLE_EVENT:String = "REMOVE_ROLE_EVENT";
      
      public static const ADD_MONSTER_LIST:int = 4;
      
      public static const ADD_MONSTER_LIST_EVENT:String = "ADD_MONSTER_LIST_EVENT";
      
      public static const ENTER_MONSTER_FIGHT:int = 5;
      
      public static const ENTER_MONSTER_FIGHT_EVENT:String = "ENTER_MONSTER_FIGHT_EVENT";
      
      public static const INIT_SECEN:int = 6;
      
      public static const INIT_SECEN_EVENT:String = "INIT_SECEN_EVENT";
      
      public static const MONSTER_STATE_CHANGE:int = 7;
      
      public static const MONSTER_STATE_CHANGE_EVENT:String = "MONSTER_STATE_CHANGE_EVENT";
      
      public static const PLAYER_STATE_CHANGE:int = 8;
      
      public static const PLAYER_STATE_CHANGE_EVENT:String = "PLAYER_STATE_CHANGE_EVENT";
      
      public static const MAP_CHANGE:int = 9;
      
      public static const MAP_CHANGE_EVENT:String = "MAP_CHANGE_EVENT";
      
      public static const PVP_TO_FIGHT:int = 16;
      
      public static const PVP_TO_FIGHT_EVENT:String = "PVP_TO_FIGHT_EVENT";
      
      public static const CAPTURE_MAP:int = 17;
      
      public static const CAPTURE_MAP_EVENT:String = "CAPTURE_MAP_EVENT";
      
      public static const RESURRECT:int = 18;
      
      public static const RESURRECT_EVENT:String = "RESURRECT_EVENT";
      
      public static const WIN_COUNT_PTP:int = 19;
      
      public static const WIN_COUNT_PTP_EVENT:String = "WIN_COUNT_PTP_EVENT";
      
      public static const CAMP_SOCER_RANK:int = 20;
      
      public static const CAMP_SOCER_RANK_EVENT:String = "CAMP_SOCER_RANK_EVENT";
      
      public static const PER_SCORE_RANK:int = 21;
      
      public static const PER_SCORE_RANK_EVENT:String = "PER_SCORE_RANK_EVENT";
      
      public static const UPDATE_SCORE:int = 22;
      
      public static const UPDATE_SCORE_EVENT:String = "UPDATE_SCORE_EVENT";
      
      public static const ACTION_ISOPEN:int = 10;
      
      public static const ACTION_ISOPEN_EVENT:String = "ACTION_ISOPEN_EVENT";
      
      public static const DOOR_STATUS:int = 23;
      
      public static const DOOR_STATUS_EVENT:String = "DOOR_STATUS_EVENT";
      
      public static const OUT_CAMPBATTLE:int = 25;
      
      public static const OUT_CAMPBATTLE_EVENT:String = "OUT_CAMPBATTLE_EVENT";
       
      
      public function CampPackageType()
      {
         super();
      }
   }
}
