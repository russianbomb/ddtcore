package ddt.data.socket
{
   public class ActivityPackageType
   {
      
      public static const EVERYDAYACTIVEPOINT_DATA:int = 1;
      
      public static const EVERYDAYACTIVEPOINT_CHANGE:int = 2;
      
      public static const USEMONEYPOINT_COMPLETE:int = 3;
      
      public static const GETACTIVEPOINT_REWARD:int = 4;
      
      public static const REUSEMONEYPOINT_COMPLETE:int = 5;
      
      public static const REGETACTIVEPOINT_REWARD:int = 6;
      
      public static const ADD_ACTIVITY_DATA_CHANGE:int = 7;
      
      public static const GET_EXPBLESSED_DATA:int = 8;
       
      
      public function ActivityPackageType()
      {
         super();
      }
   }
}
