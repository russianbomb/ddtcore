package ddt.data.socket
{
   public class RingStationPackageType
   {
      
      public static const RINGSTATION_VIEWINFO:int = 1;
      
      public static const RINGSTATION_BUYCOUNTORTIME:int = 2;
      
      public static const RINGSTATION_ARMORY:int = 3;
      
      public static const RINGSTATION_NEWBATTLEFIELD:int = 4;
      
      public static const RINGSTATION_CHALLENGE:int = 5;
      
      public static const RINGSTATION_FIGHTFLAG:int = 6;
      
      public static const RINGSTATION_SENDSIGNMSG:int = 7;
      
      public static const LANDERSAWARD_RECEIVE:int = 48;
      
      public static const LANDERSAWARD_GET:int = 49;
       
      
      public function RingStationPackageType()
      {
         super();
      }
   }
}
