package ddt.data.analyze
{
   import com.pickgliss.loader.DataAnalyzer;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.box.GradeBoxInfo;
   import ddt.data.box.TimeBoxInfo;
   import flash.utils.Dictionary;
   import road7th.data.DictionaryData;
   
   public class UserBoxInfoAnalyzer extends DataAnalyzer
   {
       
      
      private var _xml:XML;
      
      private var _goodsList:XMLList;
      
      public var timeBoxList:DictionaryData;
      
      public var gradeBoxList:DictionaryData;
      
      public var boxTemplateID:Dictionary;
      
      public var timeBoxListHigh:DictionaryData;
      
      public function UserBoxInfoAnalyzer(param1:Function)
      {
         super(param1);
      }
      
      public static function getXML() : XML
      {
         var _loc1_:XML = <Result value="true" message="Success!">
  <Item ID="1" Type="0" Level="20" Condition="15" TemplateID="1120090"/>
  <Item ID="2" Type="0" Level="20" Condition="40" TemplateID="1120091"/>
  <Item ID="3" Type="0" Level="20" Condition="60" TemplateID="1120092"/>
  <Item ID="4" Type="0" Level="20" Condition="75" TemplateID="1120093"/>
  <Item ID="5" Type="0" Level="100" Condition="15" TemplateID="1120095"/>
  <Item ID="6" Type="0" Level="100" Condition="40" TemplateID="1120096"/>
  <Item ID="7" Type="0" Level="100" Condition="60" TemplateID="1120097"/>
  <Item ID="8" Type="0" Level="100" Condition="75" TemplateID="1120098"/>
  <Item ID="9" Type="1" Level="100" Condition="1" TemplateID="1120071"/>
  <Item ID="10" Type="1" Level="100" Condition="1" TemplateID="1120072"/>
  <Item ID="11" Type="1" Level="100" Condition="1" TemplateID="1120073"/>
  <Item ID="12" Type="1" Level="100" Condition="1" TemplateID="1120074"/>
  <Item ID="13" Type="1" Level="100" Condition="1" TemplateID="1120075"/>
  <Item ID="14" Type="1" Level="100" Condition="1" TemplateID="1120076"/>
  <Item ID="15" Type="1" Level="100" Condition="1" TemplateID="1120077"/>
  <Item ID="16" Type="1" Level="100" Condition="1" TemplateID="1120078"/>
  <Item ID="17" Type="1" Level="100" Condition="0" TemplateID="1120081"/>
  <Item ID="18" Type="1" Level="100" Condition="0" TemplateID="1120082"/>
  <Item ID="19" Type="1" Level="100" Condition="0" TemplateID="1120083"/>
  <Item ID="20" Type="1" Level="100" Condition="0" TemplateID="1120084"/>
  <Item ID="21" Type="1" Level="100" Condition="0" TemplateID="1120085"/>
  <Item ID="22" Type="1" Level="100" Condition="0" TemplateID="1120086"/>
  <Item ID="23" Type="1" Level="100" Condition="0" TemplateID="1120087"/>
  <Item ID="24" Type="1" Level="100" Condition="0" TemplateID="1120088"/>
  <Item ID="25" Type="2" Level="1" Condition="0" TemplateID="112112"/>
  <Item ID="26" Type="2" Level="2" Condition="0" TemplateID="112113"/>
  <Item ID="27" Type="2" Level="3" Condition="0" TemplateID="112114"/>
  <Item ID="28" Type="2" Level="4" Condition="0" TemplateID="112115"/>
  <Item ID="29" Type="2" Level="5" Condition="0" TemplateID="112116"/>
  <Item ID="30" Type="2" Level="6" Condition="0" TemplateID="112117"/>
  <Item ID="31" Type="2" Level="7" Condition="0" TemplateID="112118"/>
  <Item ID="32" Type="2" Level="8" Condition="0" TemplateID="112119"/>
  <Item ID="33" Type="2" Level="9" Condition="0" TemplateID="112120"/>
  <Item ID="34" Type="2" Level="10" Condition="0" TemplateID="112204"/>
  <Item ID="35" Type="2" Level="11" Condition="0" TemplateID="112205"/>
  <Item ID="36" Type="2" Level="12" Condition="0" TemplateID="112206"/>
	</Result>;
         return _loc1_;
      }
      
      override public function analyze(param1:*) : void
      {
         this._xml = new XML(param1);
         if(this._xml.@value == "true")
         {
            this.timeBoxList = new DictionaryData();
            this.timeBoxListHigh = new DictionaryData();
            this.gradeBoxList = new DictionaryData();
            this.boxTemplateID = new Dictionary();
            this._goodsList = this._xml..Item;
            this.parseShop();
         }
         else
         {
            message = this._xml.@message;
            onAnalyzeError();
            onAnalyzeComplete();
         }
      }
      
      private function parseShop() : void
      {
         var _loc2_:int = 0;
         var _loc3_:TimeBoxInfo = null;
         var _loc4_:GradeBoxInfo = null;
         var _loc5_:TimeBoxInfo = null;
         var _loc1_:int = 0;
         while(_loc1_ < this._goodsList.length())
         {
            _loc2_ = this._goodsList[_loc1_].@Type;
            switch(_loc2_)
            {
               case 0:
                  _loc3_ = new TimeBoxInfo();
                  ObjectUtils.copyPorpertiesByXML(_loc3_,this._goodsList[_loc1_]);
                  this.boxTemplateID[_loc3_.TemplateID] = _loc3_.TemplateID;
                  if(_loc3_.Level > 20)
                  {
                     this.timeBoxListHigh.add(_loc3_.ID,_loc3_);
                  }
                  else
                  {
                     this.timeBoxList.add(_loc3_.ID,_loc3_);
                  }
                  break;
               case 1:
                  _loc4_ = new GradeBoxInfo();
                  ObjectUtils.copyPorpertiesByXML(_loc4_,this._goodsList[_loc1_]);
                  this.boxTemplateID[_loc4_.TemplateID] = _loc4_.TemplateID;
                  this.gradeBoxList.add(_loc4_.ID,_loc4_);
                  break;
               case 2:
                  _loc5_ = new TimeBoxInfo();
                  ObjectUtils.copyPorpertiesByXML(_loc5_,this._goodsList[_loc1_]);
                  this.boxTemplateID[_loc5_.TemplateID] = _loc5_.TemplateID;
            }
            _loc1_++;
         }
         onAnalyzeComplete();
      }
   }
}
