package ddt.data
{
   public class ShopType
   {
      
      public static const MONEY_M_HOTSALE:int = 1;
      
      public static const MONEY_F_HOTSALE:int = 2;
      
      public static const MONEY_M_RECOMMEND:int = 3;
      
      public static const MONEY_F_RECOMMEND:int = 4;
      
      public static const MONEY_M_CONCESSIONS:int = 5;
      
      public static const MONEY_F_CONCESSIONS:int = 6;
      
      public static const MONEY_M_WEAPON:int = 7;
      
      public static const MONEY_F_WEAPON:int = 8;
      
      public static const MONEY_M_CLOTH:int = 9;
      
      public static const MONEY_F_CLOTH:int = 10;
      
      public static const MONEY_M_HAT:int = 11;
      
      public static const MONEY_F_HAT:int = 12;
      
      public static const MONEY_M_GLASS:int = 13;
      
      public static const MONEY_F_GLASS:int = 14;
      
      public static const MONEY_M_SHIPIN:int = 15;
      
      public static const MONEY_F_SHIPIN:int = 16;
      
      public static const MONEY_M_HAIR:int = 17;
      
      public static const MONEY_F_HAIR:int = 18;
      
      public static const MONEY_M_EYES:int = 19;
      
      public static const MONEY_F_EYES:int = 20;
      
      public static const MONEY_M_LIANSHI:int = 21;
      
      public static const MONEY_F_LIANSHI:int = 22;
      
      public static const MONEY_M_SUIT:int = 23;
      
      public static const MONEY_F_SUIT:int = 24;
      
      public static const MONEY_M_WING:int = 25;
      
      public static const MONEY_F_WING:int = 26;
      
      public static const MONEY_M_FUNCTIONPROP:int = 27;
      
      public static const MONEY_F_FUNCTIONPROP:int = 28;
      
      public static const MONEY_M_PAOPAO:int = 29;
      
      public static const MONEY_F_PAOPAO:int = 30;
      
      public static const DDTMONEY_M_HOTSALE:int = 41;
      
      public static const DDTMONEY_F_HOTSALE:int = 42;
      
      public static const DDTMONEY_M_RECOMMEND:int = 43;
      
      public static const DDTMONEY_F_RECOMMEND:int = 44;
      
      public static const DDTMONEY_M_CONCESSIONS:int = 45;
      
      public static const DDTMONEY_F_CONCESSIONS:int = 46;
      
      public static const DDTMONEY_M_WEAPON:int = 47;
      
      public static const DDTMONEY_F_WEAPON:int = 48;
      
      public static const DDTMONEY_M_CLOTH:int = 49;
      
      public static const DDTMONEY_F_CLOTH:int = 50;
      
      public static const DDTMONEY_M_HAT:int = 51;
      
      public static const DDTMONEY_F_HAT:int = 52;
      
      public static const DDTMONEY_M_GLASS:int = 53;
      
      public static const DDTMONEY_F_GLASS:int = 54;
      
      public static const DDTMONEY_M_SHIPIN:int = 55;
      
      public static const DDTMONEY_F_SHIPIN:int = 56;
      
      public static const DDTMONEY_M_HAIR:int = 57;
      
      public static const DDTMONEY_F_HAIR:int = 58;
      
      public static const DDTMONEY_M_EYES:int = 59;
      
      public static const DDTMONEY_F_EYES:int = 60;
      
      public static const DDTMONEY_M_LIANSHI:int = 61;
      
      public static const DDTMONEY_F_LIANSHI:int = 62;
      
      public static const DDTMONEY_M_SUIT:int = 63;
      
      public static const DDTMONEY_F_SUIT:int = 64;
      
      public static const DDTMONEY_M_WING:int = 65;
      
      public static const DDTMONEY_F_WING:int = 66;
      
      public static const DDTMONEY_M_FUNCTIONPROP:int = 67;
      
      public static const DDTMONEY_F_FUNCTIONPROP:int = 68;
      
      public static const DDTMONEY_M_PAOPAO:int = 69;
      
      public static const DDTMONEY_F_PAOPAO:int = 70;
      
      public static const MONEY_HOT_GOODS:int = 33;
      
      public static const MONEY_FLOWER:int = 34;
      
      public static const MONEY_DESSERT:int = 35;
      
      public static const MONEY_TOYS:int = 36;
      
      public static const MONEY_RARE:int = 37;
      
      public static const MONEY_FESTIVAL:int = 38;
      
      public static const MONEY_WEDDING:int = 39;
      
      public static const DDTMONEY_HOT_GOODS:int = 73;
      
      public static const DDTMONEY_FLOWER:int = 74;
      
      public static const DDTMONEY_DESSERT:int = 75;
      
      public static const DDTMONEY_TOYS:int = 76;
      
      public static const DDTMONEY_RARE:int = 77;
      
      public static const DDTMONEY_FESTIVAL:int = 78;
      
      public static const DDTMONEY_WEDDING:int = 79;
      
      public static const M_POPULARITY_RANKING:int = 85;
      
      public static const F_POPULARITY_RANKING:int = 86;
      
      public static const GUILD_SHOP_1:int = 80;
      
      public static const GUILD_SHOP_2:int = 81;
      
      public static const GUILD_SHOP_3:int = 82;
      
      public static const GUILD_SHOP_4:int = 83;
      
      public static const GUILD_SHOP_5:int = 84;
      
      public static const ROOM_PROP:int = 90;
      
      public static const LITTLEGAME_AWARD_TYPE:int = 87;
      
      public static const FARM_SEED_TYPE:int = 88;
      
      public static const FARM_MANURE_TYPE:int = 89;
      
      public static const FARM_PETEGG_TYPE:int = 92;
      
      public static const WORLDBOSS_AWARD_TYPE:int = 91;
      
      public static const TRANSNATIONAL_AWARD_TYPE:int = 200;
      
      public static const LEAGUE_SHOP_TYPE:int = 93;
      
      public static const DRAGON_BOAT_TYPE:int = 95;
      
      public static const REGRESS_SHOP:int = 199;
      
      public static const SALE_SHOP:int = 110;
      
      public static const SHOP_PAST_PRICE:int = 280;
      
      public static const KING_STATUE_SHOP:int = 100;
      
      public static const MALE_MONEY_TYPE:Array = [[MONEY_M_HOTSALE,MONEY_M_RECOMMEND,MONEY_M_CONCESSIONS],[MONEY_M_WEAPON,MONEY_M_CLOTH,MONEY_M_HAT,MONEY_M_GLASS,MONEY_M_SHIPIN],[MONEY_M_HAIR,MONEY_M_EYES,MONEY_M_LIANSHI,MONEY_M_SUIT,MONEY_M_WING],[MONEY_M_FUNCTIONPROP,MONEY_M_PAOPAO]];
      
      public static const FEMALE_MONEY_TYPE:Array = [[MONEY_F_HOTSALE,MONEY_F_RECOMMEND,MONEY_F_CONCESSIONS],[MONEY_F_WEAPON,MONEY_F_CLOTH,MONEY_F_HAT,MONEY_F_GLASS,MONEY_F_SHIPIN],[MONEY_F_HAIR,MONEY_F_EYES,MONEY_F_LIANSHI,MONEY_F_SUIT,MONEY_F_WING],[MONEY_F_FUNCTIONPROP,MONEY_F_PAOPAO]];
      
      public static const MALE_DDTMONEY_TYPE:Array = [[DDTMONEY_M_HOTSALE,DDTMONEY_M_RECOMMEND,DDTMONEY_M_CONCESSIONS],[DDTMONEY_M_WEAPON,DDTMONEY_M_CLOTH,DDTMONEY_M_HAT,DDTMONEY_M_GLASS,DDTMONEY_M_SHIPIN],[DDTMONEY_M_HAIR,DDTMONEY_M_EYES,DDTMONEY_M_LIANSHI,DDTMONEY_M_SUIT,DDTMONEY_M_WING],[DDTMONEY_M_FUNCTIONPROP,DDTMONEY_M_PAOPAO]];
      
      public static const FEMALE_DDTMONEY_TYPE:Array = [[DDTMONEY_F_HOTSALE,DDTMONEY_F_RECOMMEND,DDTMONEY_F_CONCESSIONS],[DDTMONEY_F_WEAPON,DDTMONEY_F_CLOTH,DDTMONEY_F_HAT,DDTMONEY_F_GLASS,DDTMONEY_F_SHIPIN],[DDTMONEY_F_HAIR,DDTMONEY_F_EYES,DDTMONEY_F_LIANSHI,DDTMONEY_F_SUIT,DDTMONEY_F_WING],[DDTMONEY_F_FUNCTIONPROP,DDTMONEY_F_PAOPAO]];
      
      public static const GIFT_MONEY_TYPE:Array = [MONEY_HOT_GOODS,MONEY_FLOWER,MONEY_DESSERT,MONEY_TOYS,MONEY_RARE,MONEY_FESTIVAL,MONEY_WEDDING];
      
      public static const GIFT_DDTMONEY_TYPE:Array = [DDTMONEY_HOT_GOODS,DDTMONEY_FLOWER,DDTMONEY_DESSERT,DDTMONEY_TOYS,DDTMONEY_RARE,DDTMONEY_FESTIVAL,DDTMONEY_WEDDING];
      
      public static const CAN_SHOW_IN_SHOP:Array = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,85,86];
       
      
      public function ShopType()
      {
         super();
      }
   }
}
