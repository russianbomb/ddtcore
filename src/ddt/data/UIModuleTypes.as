package ddt.data
{
   public class UIModuleTypes
   {
      
      public static const DDT_COREI:String = "ddtcorei";
      
      public static const DDT_COREII:String = "ddtcoreii";
      
      public static const FIRST_CORE:String = "firstcore";
      
      public static const ROAD_COMPONENT:String = "roadComponent";
      
      public static const CORE_ICON_AND_TIP:String = "coreIconAndTip";
      
      public static const CHAT:String = "chat";
      
      public static const CHATII:String = "chatii";
      
      public static const CHAT_BALL:String = "chatBall";
      
      public static const GAME:String = "game";
      
      public static const DEFAULT_LIVING:String = "defaultLiving";
      
      public static const GAMEII:String = "gameII";
      
      public static const GAMEIII:String = "gameiii";
      
      public static const LEVEL_ICON:String = "LevelIcon";
      
      public static const GAMEOVER:String = "gameOver";
      
      public static const HALL:String = "hall";
      
      public static const DDT_HALL:String = "ddthall";
      
      public static const DDT_HALLICON:String = "ddthallIcon";
      
      public static const TOFFLIST:String = "toffilist";
      
      public static const NEWBIE:String = "newbie";
      
      public static const SHOP:String = "shop";
      
      public static const DDTSHOP:String = "ddtshop";
      
      public static const DDTSTORE:String = "ddtstore";
      
      public static const DDT_BAGLOCKER:String = "ddtbagLocked";
      
      public static const IM:String = "ddtim";
      
      public static const TOOLBAR:String = "toolbar";
      
      public static const GORETURN:String = "goreturn";
      
      public static const GOTOPAGE:String = "gotoPage";
      
      public static const EMAIL:String = "ddtemail";
      
      public static const BAG:String = "bagAndInfo";
      
      public static const BAGANDINFO:String = "ddtbagandinfo";
      
      public static const SUPPLY:String = "supply";
      
      public static const SETTING:String = "setting";
      
      public static const DDT_SETTING:String = "ddtsetting";
      
      public static const DAILY:String = "daily";
      
      public static const ACTIVE_EVENTS:String = "activeEvents";
      
      public static const DDT_FEEDBACK:String = "ddtfeedback";
      
      public static const DDTAUCTION:String = "ddtauction";
      
      public static const DDT_TIMEBOX:String = "ddtawardSystem";
      
      public static const QUEST:String = "quest";
      
      public static const CONSORTIAII:String = "consortionII";
      
      public static const DUNGEON:String = "dungeon";
      
      public static const DDTCHURCH_ROOM_LIST:String = "ddtchurchroomlist";
      
      public static const CHURCH_ROOM:String = "churchRoom";
      
      public static const STORE_ARM:String = "storeArm";
      
      public static const FIGHT_LIB:String = "newfightlib";
      
      public static const DDT_HOT_SPRING_ROOM_LIST:String = "ddthotSpringRoomList";
      
      public static const DDT_EFFORT:String = "ddtEffort";
      
      public static const PLAYER_TIP:String = "playertip";
      
      public static const INVITE:String = "invite";
      
      public static const CHANGECOLOR:String = "changeColor";
      
      public static const HOT_SPRING_ROOM:String = "hotSpringRoom";
      
      public static const ENTHRALL:String = "enthrall";
      
      public static const TRAINER:String = "Trainer";
      
      public static const TRAINERFIRSTGAME:String = "trainerfirstgame";
      
      public static const DDT_TRAINER:String = "ddttrainer";
      
      public static const TRAINER_UI:String = "trainerUI";
      
      public static const LOGIN:String = "login";
      
      public static const CHALLENGE_ROOM:String = "challengeRoom";
      
      public static const VIP_VIEW:String = "ddtvipView";
      
      public static const TIMES:String = "times";
      
      public static const MANUAL:String = "manual";
      
      public static const DDT_CALENDAR:String = "ddtcalendar";
      
      public static const ACADEMY_COMMON:String = "academyCommon";
      
      public static const DDT_GIFT_SYSTEM:String = "ddtgiftSystem";
      
      public static const DDT_CARD_SYSTEM:String = "ddtcardSystem";
      
      public static const DDT_TEXP_SYSTEM:String = "ddttexpSystem";
      
      public static const JOURNAL:String = "journal";
      
      public static const LITTLEGAME:String = "littleGame";
      
      public static const DDT_LITTLEGAME:String = "ddtlittleGame";
      
      public static const ELITEGAME:String = "eliteGame";
      
      public static const CADDY:String = "caddy";
      
      public static const ROULETTE:String = "roulette";
      
      public static const PETS_BAG:String = "petsBag";
      
      public static const FARM_PET_TRAINER_UI:String = "farmPetTrainerUI";
      
      public static const EXPRESSION:String = "expression";
      
      public static const DDTROOM:String = "ddtroom";
      
      public static const DDTROOMLOADING:String = "ddtroomloading";
      
      public static const DDTCORESCALEBITMAP:String = "ddtcorescalebitmap";
      
      public static const DDTCONSORTIA:String = "ddtconsortion";
      
      public static const DDTROOMLIST:String = "ddtroomlist";
      
      public static const DDTACADEMY:String = "ddtacademy";
      
      public static const DDTINVITE:String = "ddtinvite";
      
      public static const DDTELITEGAME:String = "ddtelitegame";
      
      public static const LOTTERY:String = "lottery";
      
      public static const GEMSTONE_SYS:String = "gemstone";
      
      public static const FRISTRECHARGE_SYS:String = "firstRecharge";
      
      public static const DAY_ACTIVITY:String = "dayactivity";
      
      public static const DDT_BURIED:String = "ddtburied";
      
      public static const DDTCIVIL:String = "ddtcivil";
      
      public static const FARM:String = "farm";
      
      public static const DDTMAINBTN:String = "ddtmainbtn";
      
      public static const WORLDBOSS_MAP:String = "worldboss";
      
      public static const DDTBEAD:String = "ddtbead";
      
      public static const TOTEM:String = "totem";
      
      public static const LABYRINTH:String = "labyrinth";
      
      public static const SYSTEM_OPEN_PROMPT:String = "systemopenprompt";
      
      public static const NEW_CHICKEN_BOX:String = "newchickenbox";
      
      public static const TREASURE:String = "treasure";
      
      public static const GODSROADS:String = "godsroads";
      
      public static const KING_BLESS:String = "kingbless";
      
      public static const TRUSTEESHIP:String = "trusteeship";
      
      public static const KINGBLESS:String = "kingbless";
      
      public static const LEAGUE:String = "league";
      
      public static const LATENT_ENERGY:String = "latentenergy";
      
      public static const BATTLEGROUD:String = "battleground";
      
      public static const WONDERFULACTIVI:String = "wonderfulactivity";
      
      public static const CONSORTIA_BATTLE:String = "consortiabattle";
      
      public static const CAMP_BATTLE_SCENE:String = "campbattle";
      
      public static const DRAGON_BOAT:String = "dragonboat";
      
      public static const FIGHTFOOTBALLTIME:String = "fightFootballTime";
      
      public static const PYRAMID:String = "pyramid";
      
      public static const TIMES_UPDATE:String = "timesupdate";
      
      public static const REGRESS_VIEW:String = "ddtRegressView";
      
      public static const GETTICKET_VIEW:String = "ddtGetTicketView";
      
      public static const WANT_STRONG:String = "wantstrong";
      
      public static const GROUP_PURCHASE:String = "grouppurchase";
      
      public static const WORLDBOSS_HELPER:String = "worldBossHelper";
      
      public static const SEVEN_DOUBLE_ICON:String = "sevendoubleicon";
      
      public static const SEVEN_DOUBLE_FRAME:String = "sevendoubleframe";
      
      public static const SEVEN_DOUBLE_GAME:String = "sevendoublegame";
      
      public static const TREASURE_HUNTING:String = "treasureHunting";
      
      public static const MYSTERIOUS_ROULETTE:String = "mysteriousRoulette";
      
      public static const CHRISTMAS:String = "christmas";
      
      public static const SUPER_WINNER:String = "superwinner";
      
      public static const LUCKSTAR:String = "luckstar";
      
      public static const YY_VIP:String = "yyVip";
      
      public static const FORGE_MAIN:String = "forgemain";
      
      public static const RING_STATION:String = "ringstation";
      
      public static const GUILDMEMBERWEEK:String = "guildMemberWeek";
      
      public static const CATCH_BEAST:String = "catchbeast";
      
      public static const LANTERN_RIDDLES:String = "lanternriddles";
      
      public static const AVATAR_COLLECTION:String = "avatarcollection";
      
      public static const PLAYER_DRESS:String = "playerDress";
      
      public static const FLOWER_GIVING:String = "flowerGiving";
      
      public static const INTEGRAL_SHOP:String = "integralshop";
      
      public static const ESCORT_ICON:String = "escorticon";
      
      public static const ESCORT_FRAME:String = "escortframe";
      
      public static const ESCORT_GAME:String = "escortgame";
      
      public static const MAGIC_STONE:String = "magicStone";
      
      public static const ENTERTAINMENT_MODE:String = "entertainmentMode";
      
      public static const LIGHTROAD:String = "lightRoad";
      
      public static const SEVENDAYTARGET:String = "sevenDayTarget";
      
      public static const KINGDIVISION:String = "kingdivision";
      
      public static const CHICKACTIVATION:String = "chickActivation";
      
      public static const DDPLAY:String = "ddplay";
      
      public static const BOGU_ADVENTURE:String = "boguadventure";
      
      public static const GROWTH_PACKAGE:String = "growthPackage";
      
      public static const WITCH_BLESSING:String = "witchblessing";
      
      public static const HALLOWEEN:String = "halloween";
       
      
      public function UIModuleTypes()
      {
         super();
      }
   }
}
