package roomList.pvpRoomList
{
   import LimitAward.LimitAwardButton;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.events.ListItemEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.ComboBox;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.controls.container.SimpleTileList;
   import com.pickgliss.ui.controls.list.VectorListModel;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.image.MutipleImage;
   import com.pickgliss.ui.image.ScaleLeftRightImage;
   import com.pickgliss.utils.ClassUtils;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.BossBoxManager;
   import ddt.manager.GameInSocketOut;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import ddt.view.bossbox.SmallBoxButton;
   import flash.display.Bitmap;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.utils.getTimer;
   import road7th.data.DictionaryEvent;
   import room.RoomManager;
   import room.model.RoomInfo;
   import roomList.LookupEnumerate;
   import serverlist.view.RoomListServerDropList;
   import superWinner.manager.SuperWinnerManager;
   
   public class RoomListBGView extends Sprite implements Disposeable
   {
      
      public static var PREWORD:Array = [LanguageMgr.GetTranslation("tank.roomlist.RoomListIICreatePveRoomView.tank"),LanguageMgr.GetTranslation("tank.roomlist.RoomListIICreatePveRoomView.go"),LanguageMgr.GetTranslation("tank.roomlist.RoomListIICreatePveRoomView.fire")];
      
      public static const FULL_MODE:int = 0;
      
      public static const ATHLETICS_MODE:int = 1;
      
      public static const CHALLENGE_MODE:int = 2;
       
      
      private var _roomListBG:MovieClip;
      
      private var _titleBg:Bitmap;
      
      private var _itemlistBg:MutipleImage;
      
      private var _itemlistBg2:ScaleLeftRightImage;
      
      private var _title:Bitmap;
      
      private var _listTitle:Bitmap;
      
      private var _nextBtn:SimpleBitmapButton;
      
      private var _preBtn:SimpleBitmapButton;
      
      private var _createBtn:SimpleBitmapButton;
      
      private var _rivalshipBtn:SimpleBitmapButton;
      
      private var _lookUpBtn:SimpleBitmapButton;
      
      private var _encounterBtn:SimpleBitmapButton;
      
      private var _btnBG:Bitmap;
      
      private var _itemList:SimpleTileList;
      
      private var _itemArray:Array;
      
      private var _model:RoomListModel;
      
      private var _controller:RoomListController;
      
      private var _boxButton:SmallBoxButton;
      
      private var _limitAwardButton:LimitAwardButton;
      
      private var _serverlist:RoomListServerDropList;
      
      private var _tempDataList:Array;
      
      private var _modeMenu:ComboBox;
      
      private var _currentMode:int;
      
      private var _isPermissionEnter:Boolean;
      
      private var _modeArray:Array;
      
      private var _selectItemPos:int;
      
      private var _selectItemID:int;
      
      private var _lastCreatTime:int = 0;
      
      public function RoomListBGView(param1:RoomListController, param2:RoomListModel)
      {
         this._modeArray = ["ddt.roomList.roomListBG.full","ddt.roomList.roomListBG.Athletics","ddt.roomList.roomListBG.challenge"];
         this._model = param2;
         this._controller = param1;
         super();
         this.init();
         this.initEvent();
      }
      
      private function init() : void
      {
         this._roomListBG = ClassUtils.CreatInstance("asset.background.roomlist.right") as MovieClip;
         PositionUtils.setPos(this._roomListBG,"asset.ddtRoomlist.pvp.listBgpos");
         this._itemlistBg = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroomList.pvp.itemlistBg");
         this._itemlistBg2 = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroomList.pvp.itemlistBg2");
         this._titleBg = ComponentFactory.Instance.creatBitmap("asset.ddtcivil.titleBg");
         PositionUtils.setPos(this._titleBg,"asset.ddtRoomlist.pvp.titlebgpos");
         this._title = ComponentFactory.Instance.creatBitmap("asset.ddtroomlist.right.title");
         this._listTitle = ComponentFactory.Instance.creatBitmap("asset.ddtroomlist.right.listtitle");
         this._modeMenu = ComponentFactory.Instance.creatComponentByStylename("asset.ddtRoomlist.pvp.modeMenu");
         this._modeMenu.textField.text = LanguageMgr.GetTranslation(this._modeArray[FULL_MODE]);
         this._currentMode = FULL_MODE;
         this._itemList = ComponentFactory.Instance.creat("asset.ddtRoomList.pvp.itemContainer",[2]);
         addChild(this._roomListBG);
         addChild(this._itemlistBg);
         addChild(this._itemlistBg2);
         addChild(this._titleBg);
         addChild(this._title);
         addChild(this._listTitle);
         addChild(this._modeMenu);
         addChild(this._itemList);
         this.updateList();
         this.initButton();
         if(BossBoxManager.instance.isShowBoxButton())
         {
            this._boxButton = new SmallBoxButton(SmallBoxButton.PVR_ROOMLIST_POINT);
            addChild(this._boxButton);
         }
         this._isPermissionEnter = true;
      }
      
      private function initButton() : void
      {
         this._nextBtn = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroomList.pvp.nextBtn");
         this._preBtn = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroomList.pvp.preBtn");
         this._createBtn = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroomlist.pvpBtn.startBtn");
         this._createBtn.tipData = LanguageMgr.GetTranslation("tank.roomlist.RoomListIIRoomBtnPanel.createRoom");
         this._rivalshipBtn = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroomlist.pvpBtn.formAteamBtn");
         this._rivalshipBtn.tipData = LanguageMgr.GetTranslation("tank.roomlist.joinBattleQuickly");
         this._lookUpBtn = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroomlist.pvpBtn.lookUpBtn");
         this._lookUpBtn.tipData = LanguageMgr.GetTranslation("tank.roomlist.RoomListIIRoomBtnPanel.findRoom");
         this._encounterBtn = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroomlist.pvpBtn.encounterBtn");
         this._encounterBtn.tipData = LanguageMgr.GetTranslation("tank.roomlist.RoomListIIRoomBtnPanel.fastMatchTip");
         this._btnBG = ComponentFactory.Instance.creatBitmap("asset.ddtroomlist.pvpBtn.btnBG");
         this._serverlist = ComponentFactory.Instance.creat("asset.ddtRoomlist.pvp.serverlist");
         this.addTipPanel();
         addChild(this._btnBG);
         addChild(this._nextBtn);
         addChild(this._preBtn);
         addChild(this._createBtn);
         addChild(this._rivalshipBtn);
         addChild(this._lookUpBtn);
         addChild(this._encounterBtn);
         addChild(this._serverlist);
         this._itemArray = [];
      }
      
      private function initEvent() : void
      {
         this._encounterBtn.addEventListener(MouseEvent.CLICK,this.__encounterBtnClick);
         this._createBtn.addEventListener(MouseEvent.CLICK,this.__createBtnClick);
         this._rivalshipBtn.addEventListener(MouseEvent.CLICK,this._rivalshipClick);
         this._lookUpBtn.addEventListener(MouseEvent.CLICK,this.__lookupClick);
         this._nextBtn.addEventListener(MouseEvent.CLICK,this.__updateClick);
         this._preBtn.addEventListener(MouseEvent.CLICK,this.__updateClick);
         this._model.addEventListener(RoomListModel.ROOM_ITEM_UPDATE,this.__updateItem);
         this._model.getRoomList().addEventListener(DictionaryEvent.CLEAR,this.__clearRoom);
         this._modeMenu.listPanel.list.addEventListener(ListItemEvent.LIST_ITEM_CLICK,this.__onListClick);
         SuperWinnerManager.instance.addEventListener(SuperWinnerManager.ROOM_IS_OPEN,this.__superWinnerIsOpen);
      }
      
      private function removeEvent() : void
      {
         this._encounterBtn.removeEventListener(MouseEvent.CLICK,this.__encounterBtnClick);
         this._createBtn.removeEventListener(MouseEvent.CLICK,this.__createBtnClick);
         this._rivalshipBtn.removeEventListener(MouseEvent.CLICK,this._rivalshipClick);
         this._lookUpBtn.removeEventListener(MouseEvent.CLICK,this.__lookupClick);
         this._nextBtn.removeEventListener(MouseEvent.CLICK,this.__updateClick);
         this._preBtn.removeEventListener(MouseEvent.CLICK,this.__updateClick);
         this._model.removeEventListener(RoomListModel.ROOM_ITEM_UPDATE,this.__updateItem);
         if(this._model.getRoomList())
         {
            this._model.getRoomList().removeEventListener(DictionaryEvent.CLEAR,this.__clearRoom);
         }
         SuperWinnerManager.instance.removeEventListener(SuperWinnerManager.ROOM_IS_OPEN,this.__superWinnerIsOpen);
      }
      
      private function __superWinnerIsOpen(param1:Event) : void
      {
         var _loc2_:BaseAlerFrame = null;
         if(SuperWinnerManager.instance.isOpen)
         {
            if(PlayerManager.Instance.Self.Grade >= 20)
            {
               _loc2_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("ddt.superWinner.openSuperWinner"),LanguageMgr.GetTranslation("ok"),"",false,true,true,LayerManager.BLCAK_BLOCKGOUND);
               _loc2_.addEventListener(FrameEvent.RESPONSE,this._responseEnterSuperWinner);
            }
         }
      }
      
      private function _responseEnterSuperWinner(param1:FrameEvent) : void
      {
         var _loc2_:BaseAlerFrame = param1.currentTarget as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this._responseEnterSuperWinner);
         SoundManager.instance.playButtonSound();
         _loc2_.dispose();
      }
      
      private function __updateItem(param1:Event) : void
      {
         this.upadteItemPos();
         this._isPermissionEnter = true;
      }
      
      private function __onListClick(param1:ListItemEvent) : void
      {
         SoundManager.instance.play("008");
         this._currentMode = this.getCurrentMode(param1.cellValue);
         this.addTipPanel();
      }
      
      private function getCurrentMode(param1:String) : int
      {
         var _loc2_:int = 0;
         while(_loc2_ < this._modeArray.length)
         {
            if(LanguageMgr.GetTranslation(this._modeArray[_loc2_]) == param1)
            {
               return _loc2_;
            }
            _loc2_++;
         }
         return -1;
      }
      
      private function addTipPanel() : void
      {
         var _loc1_:VectorListModel = this._modeMenu.listPanel.vectorListModel;
         _loc1_.clear();
         switch(this._currentMode)
         {
            case FULL_MODE:
               _loc1_.append(LanguageMgr.GetTranslation(this._modeArray[ATHLETICS_MODE]));
               _loc1_.append(LanguageMgr.GetTranslation(this._modeArray[CHALLENGE_MODE]));
               SocketManager.Instance.out.sendUpdateRoomList(LookupEnumerate.ROOM_LIST,LookupEnumerate.ROOMLIST_DEFAULT);
               break;
            case ATHLETICS_MODE:
               _loc1_.append(LanguageMgr.GetTranslation(this._modeArray[FULL_MODE]));
               _loc1_.append(LanguageMgr.GetTranslation(this._modeArray[CHALLENGE_MODE]));
               SocketManager.Instance.out.sendUpdateRoomList(LookupEnumerate.ROOM_LIST,LookupEnumerate.ROOMLIST_ATHLETICTICS);
               break;
            case CHALLENGE_MODE:
               _loc1_.append(LanguageMgr.GetTranslation(this._modeArray[FULL_MODE]));
               _loc1_.append(LanguageMgr.GetTranslation(this._modeArray[ATHLETICS_MODE]));
               SocketManager.Instance.out.sendUpdateRoomList(LookupEnumerate.ROOM_LIST,LookupEnumerate.ROOMLIST_DEFY);
         }
      }
      
      private function __clearRoom(param1:DictionaryEvent) : void
      {
         this.cleanItem();
         this._isPermissionEnter = true;
      }
      
      private function updateList() : void
      {
         var _loc2_:RoomInfo = null;
         var _loc3_:RoomListItemView = null;
         var _loc1_:int = 0;
         while(_loc1_ < this._model.getRoomList().length)
         {
            _loc2_ = this._model.getRoomList().list[_loc1_];
            _loc3_ = new RoomListItemView(_loc2_);
            _loc3_.addEventListener(MouseEvent.CLICK,this.__itemClick);
            this._itemList.addChild(_loc3_);
            this._itemArray.push(_loc3_);
            _loc1_++;
         }
      }
      
      private function cleanItem() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ < this._itemArray.length)
         {
            (this._itemArray[_loc1_] as RoomListItemView).removeEventListener(MouseEvent.CLICK,this.__itemClick);
            (this._itemArray[_loc1_] as RoomListItemView).dispose();
            _loc1_++;
         }
         this._itemList.disposeAllChildren();
         this._itemArray = [];
      }
      
      private function __itemClick(param1:MouseEvent) : void
      {
         if(!this._isPermissionEnter)
         {
            return;
         }
         this.gotoIntoRoom((param1.currentTarget as RoomListItemView).info);
         this.getSelectItemPos((param1.currentTarget as RoomListItemView).id);
      }
      
      private function getSelectItemPos(param1:int) : int
      {
         if(!this._itemList)
         {
            return 0;
         }
         var _loc2_:int = 0;
         while(_loc2_ < this._itemArray.length)
         {
            if(!(this._itemArray[_loc2_] as RoomListItemView))
            {
               return 0;
            }
            if((this._itemArray[_loc2_] as RoomListItemView).id == param1)
            {
               this._selectItemPos = _loc2_;
               this._selectItemID = (this._itemArray[_loc2_] as RoomListItemView).id;
               return _loc2_;
            }
            _loc2_++;
         }
         return 0;
      }
      
      public function get currentDataList() : Array
      {
         if(this._model.roomShowMode == 1)
         {
            return this._model.getRoomList().filter("isPlaying",false).concat(this._model.getRoomList().filter("isPlaying",true));
         }
         return this._model.getRoomList().list;
      }
      
      private function getInfosPos(param1:int) : int
      {
         this._tempDataList = this.currentDataList;
         if(!this._tempDataList)
         {
            return 0;
         }
         var _loc2_:int = 0;
         while(_loc2_ < this._tempDataList.length)
         {
            if((this._tempDataList[_loc2_] as RoomInfo).ID == param1)
            {
               return _loc2_;
            }
            _loc2_++;
         }
         return 0;
      }
      
      private function upadteItemPos() : void
      {
         var _loc1_:RoomInfo = null;
         var _loc2_:int = 0;
         var _loc3_:RoomInfo = null;
         var _loc4_:RoomListItemView = null;
         this._tempDataList = this.currentDataList;
         if(this._tempDataList)
         {
            _loc1_ = this._tempDataList[this._selectItemPos];
            _loc2_ = this.getInfosPos(this._selectItemID);
            this._tempDataList[this._selectItemPos] = this._tempDataList[_loc2_];
            this._tempDataList[_loc2_] = _loc1_;
            this._tempDataList = this.sortRooInfo(this._tempDataList);
            this.cleanItem();
            for each(_loc3_ in this._tempDataList)
            {
               if(!_loc3_)
               {
                  return;
               }
               _loc4_ = new RoomListItemView(_loc3_);
               _loc4_.addEventListener(MouseEvent.CLICK,this.__itemClick,false,0,true);
               this._itemList.addChild(_loc4_);
               this._itemArray.push(_loc4_);
            }
         }
      }
      
      private function sortRooInfo(param1:Array) : Array
      {
         var _loc3_:int = 0;
         var _loc4_:RoomInfo = null;
         var _loc2_:Array = new Array();
         switch(this._currentMode)
         {
            case ATHLETICS_MODE:
               _loc3_ = 0;
               break;
            case CHALLENGE_MODE:
               _loc3_ = 1;
         }
         for each(_loc4_ in param1)
         {
            if(_loc4_)
            {
               if(_loc4_.type == _loc3_ && !_loc4_.isPlaying)
               {
                  _loc2_.unshift(_loc4_);
               }
               else
               {
                  _loc2_.push(_loc4_);
               }
            }
         }
         return _loc2_;
      }
      
      private function gotoTip(param1:int) : Boolean
      {
         if(param1 == 0)
         {
            if(PlayerManager.Instance.Self.Grade < 6)
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("tank.roomlist.notGotoIntoRoom",6,LanguageMgr.GetTranslation("tank.view.chat.ChannelListSelectView.ream")));
               return true;
            }
         }
         else if(param1 == 1)
         {
            if(PlayerManager.Instance.Self.Grade < 12)
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("tank.roomlist.notGotoIntoRoom",12,LanguageMgr.GetTranslation("tank.roomlist.challenge")));
               return true;
            }
         }
         return false;
      }
      
      public function gotoIntoRoom(param1:RoomInfo) : void
      {
         SoundManager.instance.play("008");
         if(this.gotoTip(param1.type))
         {
            return;
         }
         SocketManager.Instance.out.sendGameLogin(1,-1,param1.ID,"");
         this._isPermissionEnter = false;
      }
      
      private function __lookupClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         this._controller.showFindRoom();
      }
      
      private function _rivalshipClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         if(!this._isPermissionEnter)
         {
            return;
         }
         if(this.gotoTip(0))
         {
            return;
         }
         SocketManager.Instance.out.sendGameLogin(1,0);
         this._isPermissionEnter = false;
      }
      
      private function __updateClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         this.sendUpdate();
      }
      
      private function __placeCountClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         this.sendUpdate();
      }
      
      private function __hardLevelClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         this.sendUpdate();
      }
      
      private function __roomModeClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         this.sendUpdate();
      }
      
      private function __roomNameClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         this.sendUpdate();
      }
      
      private function __idBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         this.sendUpdate();
      }
      
      private function sendUpdate() : void
      {
         switch(this._currentMode)
         {
            case FULL_MODE:
               SocketManager.Instance.out.sendUpdateRoomList(LookupEnumerate.ROOM_LIST,LookupEnumerate.ROOMLIST_DEFAULT);
               break;
            case ATHLETICS_MODE:
               SocketManager.Instance.out.sendUpdateRoomList(LookupEnumerate.ROOM_LIST,LookupEnumerate.ROOMLIST_ATHLETICTICS);
               break;
            case CHALLENGE_MODE:
               SocketManager.Instance.out.sendUpdateRoomList(LookupEnumerate.ROOM_LIST,LookupEnumerate.ROOMLIST_DEFY);
         }
      }
      
      private function __createBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         if(getTimer() - this._lastCreatTime > 2000)
         {
            this._lastCreatTime = getTimer();
            GameInSocketOut.sendCreateRoom(PREWORD[int(Math.random() * PREWORD.length)],0,3);
         }
      }
      
      protected function __encounterBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         if(PlayerManager.Instance.Self.Bag.getItemAt(6) == null)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("tank.room.RoomIIController.weapon"));
            return;
         }
         if(getTimer() - this._lastCreatTime > 1000)
         {
            this._lastCreatTime = getTimer();
            RoomManager.Instance.addBattleSingleRoom();
         }
      }
      
      public function dispose() : void
      {
         this.removeEvent();
         this.cleanItem();
         if(this._itemlistBg2)
         {
            ObjectUtils.disposeObject(this._itemlistBg2);
         }
         addChild(this._itemlistBg2);
         if(this._itemlistBg)
         {
            ObjectUtils.disposeObject(this._itemlistBg);
         }
         addChild(this._itemlistBg);
         if(this._roomListBG)
         {
            ObjectUtils.disposeObject(this._roomListBG);
         }
         this._roomListBG = null;
         if(this._title)
         {
            ObjectUtils.disposeObject(this._title);
         }
         this._title = null;
         if(this._listTitle)
         {
            ObjectUtils.disposeObject(this._listTitle);
         }
         this._listTitle = null;
         ObjectUtils.disposeObject(this._titleBg);
         this._titleBg = null;
         this._nextBtn.dispose();
         this._nextBtn = null;
         this._preBtn.dispose();
         this._preBtn = null;
         this._createBtn.dispose();
         this._createBtn = null;
         this._rivalshipBtn.dispose();
         this._rivalshipBtn = null;
         this._lookUpBtn.dispose();
         this._lookUpBtn = null;
         if(this._itemList)
         {
            this._itemList.disposeAllChildren();
         }
         ObjectUtils.disposeObject(this._itemList);
         this._itemList = null;
         this._itemArray = null;
         if(this._boxButton && this._boxButton.parent)
         {
            this._boxButton.parent.removeChild(this._boxButton);
            this._boxButton.dispose();
            this._boxButton = null;
         }
         if(this._modeMenu)
         {
            ObjectUtils.disposeObject(this._modeMenu);
            this._modeMenu = null;
         }
         if(this._serverlist)
         {
            this._serverlist.dispose();
            this._serverlist = null;
         }
         if(this.parent)
         {
            this.parent.removeChild(this);
         }
         if(this._limitAwardButton)
         {
            ObjectUtils.disposeObject(this._limitAwardButton);
         }
         this._limitAwardButton = null;
      }
   }
}
