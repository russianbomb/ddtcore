package roomList.pvpRoomList
{
   import com.pickgliss.events.ListItemEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.ListPanel;
   import com.pickgliss.ui.controls.container.VBox;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.ui.text.GradientText;
   import com.pickgliss.utils.ClassUtils;
   import com.pickgliss.utils.ObjectUtils;
   import consortion.view.selfConsortia.Badge;
   import ddt.data.player.PlayerInfo;
   import ddt.data.player.SelfInfo;
   import ddt.manager.LanguageMgr;
   import ddt.manager.PlayerManager;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import ddt.view.buff.BuffControl;
   import ddt.view.character.CharactoryFactory;
   import ddt.view.character.ICharacter;
   import ddt.view.common.KingBlessIcon;
   import ddt.view.common.LevelIcon;
   import ddt.view.common.MarriedIcon;
   import ddt.view.common.VipLevelIcon;
   import flash.display.Bitmap;
   import flash.display.DisplayObject;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.geom.Point;
   import kingBless.KingBlessManager;
   import road7th.data.DictionaryData;
   import road7th.data.DictionaryEvent;
   import vip.VipController;
   
   public class RoomListPlayerListView extends Sprite implements Disposeable
   {
       
      
      private var _selfInfo:SelfInfo;
      
      protected var _playerListBG:MovieClip;
      
      protected var _title:Bitmap;
      
      protected var _characterBg:MovieClip;
      
      protected var _propbg:MovieClip;
      
      protected var _listbg:MovieClip;
      
      protected var _listbg2:DisplayObject;
      
      private var _player:ICharacter;
      
      private var _levelIcon:LevelIcon;
      
      private var _vipIcon:VipLevelIcon;
      
      private var _kingBlessIcon:KingBlessIcon;
      
      private var _badge:Badge;
      
      private var _iconContainer:VBox;
      
      private var _nickNameText:FilterFrameText;
      
      private var _consortiaNameText:FilterFrameText;
      
      private var _repute:FilterFrameText;
      
      private var _reputeText:FilterFrameText;
      
      private var _geste:FilterFrameText;
      
      private var _gesteText:FilterFrameText;
      
      protected var _level:FilterFrameText;
      
      protected var _sex:FilterFrameText;
      
      private var _playerList:ListPanel;
      
      private var _data:DictionaryData;
      
      private var _currentItem:RoomListPlayerItem;
      
      private var _marriedIcon:MarriedIcon;
      
      protected var _buffbgVec:Vector.<Bitmap>;
      
      private var _buff:BuffControl;
      
      private var _vipName:GradientText;
      
      public function RoomListPlayerListView(param1:DictionaryData)
      {
         this._data = param1;
         super();
         this._selfInfo = PlayerManager.Instance.Self;
         this.initbg();
         this.initView();
         this.initEvent();
      }
      
      public function set type(param1:int) : void
      {
      }
      
      protected function initbg() : void
      {
         var _loc1_:Point = null;
         var _loc2_:int = 0;
         this._playerListBG = ClassUtils.CreatInstance("asset.background.roomlist.left") as MovieClip;
         PositionUtils.setPos(this._playerListBG,"asset.ddtRoomlist.pvp.leftbgpos");
         addChild(this._playerListBG);
         this._title = ComponentFactory.Instance.creatBitmap("asset.ddtroomlist.left.title");
         addChild(this._title);
         this._characterBg = ClassUtils.CreatInstance("asset.ddtroomlist.characterbg") as MovieClip;
         PositionUtils.setPos(this._characterBg,"asset.ddtRoomlist.pvp.left.characterbgpos");
         addChild(this._characterBg);
         this._propbg = ClassUtils.CreatInstance("asset.ddtroomlist.proprbg") as MovieClip;
         PositionUtils.setPos(this._propbg,"asset.ddtRoomlist.pvp.left.propbgpos");
         addChild(this._propbg);
         this._listbg2 = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroomList.pvp.left.playerlistbg");
         addChild(this._listbg2);
         this._listbg = ClassUtils.CreatInstance("asset.ddtroomlist.listTitle.bg") as MovieClip;
         PositionUtils.setPos(this._listbg,"asset.ddtRoomlist.pvp.left.listbgpos");
         addChild(this._listbg);
         this._buffbgVec = new Vector.<Bitmap>(6);
         _loc1_ = ComponentFactory.Instance.creatCustomObject("asset.ddtRoomlist.pvp.buffbgpos");
         _loc2_ = 0;
         while(_loc2_ < 6)
         {
            this._buffbgVec[_loc2_] = ComponentFactory.Instance.creatBitmap("asset.core.buff.buffTiledBg");
            this._buffbgVec[_loc2_].x = _loc1_.x + (this._buffbgVec[_loc2_].width - 1) * _loc2_;
            this._buffbgVec[_loc2_].y = _loc1_.y;
            addChild(this._buffbgVec[_loc2_]);
            _loc2_++;
         }
      }
      
      protected function initView() : void
      {
         this._level = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroomList.pvp.left.levelText");
         this._level.text = LanguageMgr.GetTranslation("ddt.cardSystem.cardsTipPanel.level");
         addChild(this._level);
         this._sex = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroomList.pvp.left.sexText");
         this._sex.text = LanguageMgr.GetTranslation("ddt.roomlist.right.sex");
         addChild(this._sex);
         this._iconContainer = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroomList.pvp.left.iconContainer");
         addChild(this._iconContainer);
         this._consortiaNameText = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroomList.pvp.left.consortiaNameText");
         addChild(this._consortiaNameText);
         this._repute = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroomList.pvp.left.repute");
         this._repute.text = LanguageMgr.GetTranslation("repute");
         this._repute.mouseEnabled = false;
         addChild(this._repute);
         this._reputeText = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroomList.pvp.left.reputeTxt");
         addChild(this._reputeText);
         this._geste = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroomList.pvp.left.geste");
         this._geste.text = LanguageMgr.GetTranslation("offer");
         addChild(this._geste);
         this._gesteText = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroomList.pvp.left.gesteText");
         addChild(this._gesteText);
         this._nickNameText = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroomList.pvp.left.nickNameText");
         this._nickNameText.text = this._selfInfo.NickName;
         addChild(this._nickNameText);
         if(this._selfInfo.IsVIP)
         {
            this._vipName = VipController.instance.getVipNameTxt(104,this._selfInfo.typeVIP);
            this._vipName.textSize = 16;
            this._vipName.x = this._nickNameText.x;
            this._vipName.y = this._nickNameText.y + 1;
            this._vipName.text = this._selfInfo.NickName;
            addChild(this._vipName);
         }
         PositionUtils.adaptNameStyle(this._selfInfo,this._nickNameText,this._vipName);
         this._reputeText.text = String(this._selfInfo.Repute);
         this._gesteText.text = String(this._selfInfo.Offer);
         if(this._selfInfo.ConsortiaName == "")
         {
            this._consortiaNameText.text = "";
         }
         else
         {
            this._consortiaNameText.text = String("<" + this._selfInfo.ConsortiaName + ">");
         }
         if(this._consortiaNameText.text.substr(this._consortiaNameText.text.length - 1) == ".")
         {
            this._consortiaNameText.text = this._consortiaNameText.text + ">";
         }
         this._player = CharactoryFactory.createCharacter(this._selfInfo,"room");
         this._player.showGun = true;
         this._player.show();
         this._player.setShowLight(true);
         PositionUtils.setPos(this._player,"asset.ddtroomList.pvp.left.playerPos");
         addChild(this._player as DisplayObject);
         this._levelIcon = ComponentFactory.Instance.creatCustomObject("asset.ddtroomList.pvp.left.levelIcon");
         this._levelIcon.setInfo(this._selfInfo.Grade,this._selfInfo.Repute,this._selfInfo.WinCount,this._selfInfo.TotalCount,this._selfInfo.FightPower,this._selfInfo.Offer,true,false);
         this._levelIcon.allowClick();
         addChild(this._levelIcon);
         if(this._selfInfo.IsVIP)
         {
            this._levelIcon.x = this._levelIcon.x + 2;
         }
         this._vipIcon = ComponentFactory.Instance.creatCustomObject("asset.ddtroomList.pvp.left.VipIcon");
         this._vipIcon.setInfo(this._selfInfo);
         this._iconContainer.addChild(this._vipIcon);
         if(!this._selfInfo.IsVIP)
         {
            this._vipIcon.filters = ComponentFactory.Instance.creatFilters("grayFilter");
         }
         this._kingBlessIcon = ComponentFactory.Instance.creatCustomObject("asset.ddtroomList.pvp.left.KingBlessIcon");
         this._selfInfo.isOpenKingBless = KingBlessManager.instance.getRemainTimeTxt().isOpen;
         this._kingBlessIcon.setInfo(this._selfInfo.isOpenKingBless,true);
         this._iconContainer.addChild(this._kingBlessIcon);
         if(this._selfInfo.SpouseID > 0 && !this._marriedIcon)
         {
            this._marriedIcon = ComponentFactory.Instance.creatCustomObject("asset.ddtroomList.pvp.left.MarriedIcon");
            this._marriedIcon.tipData = {
               "nickName":this._selfInfo.SpouseName,
               "gender":this._selfInfo.Sex
            };
            this._iconContainer.addChild(this._marriedIcon);
         }
         if(this._selfInfo.ConsortiaID > 0 && this._selfInfo.badgeID)
         {
            this._badge = new Badge();
            this._badge.badgeID = this._selfInfo.badgeID;
            this._badge.showTip = true;
            this._badge.tipData = this._selfInfo.ConsortiaName;
            this._iconContainer.addChild(this._badge);
         }
         this._playerList = ComponentFactory.Instance.creatComponentByStylename("asset.ddtroomList.pvp.left.playerlistII");
         addChild(this._playerList);
         this._playerList.list.updateListView();
         this._playerList.list.addEventListener(ListItemEvent.LIST_ITEM_CLICK,this.__itemClick);
         this._buff = ComponentFactory.Instance.creatCustomObject("asset.ddtroomList.pvp.left.buff");
         addChild(this._buff);
      }
      
      private function initEvent() : void
      {
         this._data.addEventListener(DictionaryEvent.ADD,this.__addPlayer);
         this._data.addEventListener(DictionaryEvent.REMOVE,this.__removePlayer);
         this._data.addEventListener(DictionaryEvent.UPDATE,this.__updatePlayer);
      }
      
      private function __updatePlayer(param1:DictionaryEvent) : void
      {
         var _loc2_:PlayerInfo = param1.data as PlayerInfo;
         this._playerList.vectorListModel.remove(_loc2_);
         this._playerList.vectorListModel.insertElementAt(_loc2_,this.getInsertIndex(_loc2_));
         this._playerList.list.updateListView();
         this.upSelfItem();
      }
      
      private function __addPlayer(param1:DictionaryEvent) : void
      {
         var _loc2_:PlayerInfo = param1.data as PlayerInfo;
         this._playerList.vectorListModel.insertElementAt(_loc2_,this.getInsertIndex(_loc2_));
         this.upSelfItem();
      }
      
      private function __removePlayer(param1:DictionaryEvent) : void
      {
         var _loc2_:PlayerInfo = param1.data as PlayerInfo;
         this._playerList.vectorListModel.remove(_loc2_);
         this.upSelfItem();
      }
      
      private function upSelfItem() : void
      {
         var _loc1_:PlayerInfo = this._data[PlayerManager.Instance.Self.ID];
         var _loc2_:int = this._playerList.vectorListModel.indexOf(_loc1_);
         if(_loc2_ == -1 || _loc2_ == 0)
         {
            return;
         }
         this._playerList.vectorListModel.removeAt(_loc2_);
         this._playerList.vectorListModel.append(_loc1_,0);
      }
      
      private function __itemClick(param1:ListItemEvent) : void
      {
         SoundManager.instance.play("008");
         if(!this._currentItem)
         {
            this._currentItem = param1.cell as RoomListPlayerItem;
            this._currentItem.setListCellStatus(this._playerList.list,true,param1.index);
         }
         if(this._currentItem != param1.cell as RoomListPlayerItem)
         {
            this._currentItem.setListCellStatus(this._playerList.list,false,param1.index);
            this._currentItem = param1.cell as RoomListPlayerItem;
            this._currentItem.setListCellStatus(this._playerList.list,true,param1.index);
         }
      }
      
      private function getInsertIndex(param1:PlayerInfo) : int
      {
         var _loc4_:PlayerInfo = null;
         var _loc2_:int = 0;
         var _loc3_:Array = this._playerList.vectorListModel.elements;
         if(_loc3_.length == 0)
         {
            return 0;
         }
         var _loc5_:int = _loc3_.length - 1;
         while(_loc5_ >= 0)
         {
            _loc4_ = _loc3_[_loc5_] as PlayerInfo;
            if(!(param1.IsVIP && !_loc4_.IsVIP))
            {
               if(!param1.IsVIP && _loc4_.IsVIP)
               {
                  return _loc5_ + 1;
               }
               if(param1.IsVIP == _loc4_.IsVIP)
               {
                  if(param1.Grade > _loc4_.Grade)
                  {
                     _loc2_ = _loc5_ - 1;
                  }
                  else
                  {
                     return _loc5_ + 1;
                  }
               }
            }
            _loc5_--;
         }
         return _loc2_ < 0?int(0):int(_loc2_);
      }
      
      public function dispose() : void
      {
         var _loc1_:int = 0;
         this._data.removeEventListener(DictionaryEvent.ADD,this.__addPlayer);
         this._data.removeEventListener(DictionaryEvent.REMOVE,this.__removePlayer);
         this._data.removeEventListener(DictionaryEvent.UPDATE,this.__updatePlayer);
         this._playerList.list.removeEventListener(ListItemEvent.LIST_ITEM_CLICK,this.__itemClick);
         ObjectUtils.disposeObject(this._playerListBG);
         this._playerListBG = null;
         this._player.dispose();
         this._player = null;
         this._levelIcon.dispose();
         this._levelIcon = null;
         if(this._listbg2)
         {
            ObjectUtils.disposeObject(this._listbg2);
         }
         this._listbg2 = null;
         if(this._vipIcon.filters)
         {
            this._vipIcon.filters = null;
         }
         ObjectUtils.disposeObject(this._vipIcon);
         this._vipIcon = null;
         ObjectUtils.disposeObject(this._kingBlessIcon);
         this._kingBlessIcon = null;
         if(this._nickNameText)
         {
            this._nickNameText.dispose();
         }
         this._nickNameText = null;
         if(this._repute)
         {
            ObjectUtils.disposeObject(this._repute);
         }
         this._repute = null;
         this._consortiaNameText.dispose();
         this._consortiaNameText = null;
         this._reputeText.dispose();
         this._reputeText = null;
         if(this._title)
         {
            ObjectUtils.disposeObject(this._title);
         }
         this._title = null;
         if(this._characterBg)
         {
            ObjectUtils.disposeObject(this._characterBg);
         }
         this._characterBg = null;
         if(this._propbg)
         {
            ObjectUtils.disposeObject(this._propbg);
         }
         this._propbg = null;
         if(this._listbg)
         {
            ObjectUtils.disposeObject(this._listbg);
         }
         this._listbg = null;
         if(this._level)
         {
            ObjectUtils.disposeObject(this._level);
         }
         this._level = null;
         if(this._geste)
         {
            ObjectUtils.disposeObject(this._geste);
         }
         this._geste = null;
         if(this._sex)
         {
            ObjectUtils.disposeObject(this._sex);
         }
         this._sex = null;
         if(this._buffbgVec)
         {
            _loc1_ = 0;
            while(_loc1_ < this._buffbgVec.length)
            {
               ObjectUtils.disposeObject(this._buffbgVec[_loc1_]);
               this._buffbgVec[_loc1_] = null;
               _loc1_++;
            }
            this._buffbgVec = null;
         }
         this._gesteText.dispose();
         this._gesteText = null;
         this._playerList.vectorListModel.clear();
         this._playerList.dispose();
         this._playerList = null;
         if(this._marriedIcon)
         {
            ObjectUtils.disposeObject(this._marriedIcon);
            this._marriedIcon = null;
         }
         ObjectUtils.disposeObject(this._badge);
         this._badge = null;
         this._data = null;
         if(this._currentItem)
         {
            this._currentItem.dispose();
         }
         this._currentItem = null;
         ObjectUtils.disposeObject(this._iconContainer);
         this._iconContainer = null;
         this._buff.dispose();
         this._buff = null;
         if(this._vipName)
         {
            ObjectUtils.disposeObject(this._vipName);
         }
         this._vipName = null;
         if(this.parent)
         {
            this.parent.removeChild(this);
         }
      }
   }
}
