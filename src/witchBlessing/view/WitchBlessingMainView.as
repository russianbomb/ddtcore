package witchBlessing.view
{
   import bagAndInfo.cell.BagCell;
   import baglocked.BaglockedManager;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.SelectedButton;
   import com.pickgliss.ui.controls.SelectedButtonGroup;
   import com.pickgliss.ui.controls.SelectedCheckButton;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.manager.ItemManager;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerConfigManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import witchBlessing.WitchBlessingManager;
   import witchBlessing.data.WitchBlessingModel;
   
   public class WitchBlessingMainView extends Frame implements Disposeable
   {
       
      
      private var _leftView:Sprite;
      
      private var _rightView:Sprite;
      
      private var _bottom:ScaleBitmapImage;
      
      private var _btnGroup:SelectedButtonGroup;
      
      private var lv1Btn:SelectedButton;
      
      private var lv2Btn:SelectedButton;
      
      private var lv3Btn:SelectedButton;
      
      private var lv1View:WitchBlessingRightView;
      
      private var lv2View:WitchBlessingRightView;
      
      private var lv3View:WitchBlessingRightView;
      
      private var titleMc:MovieClip;
      
      private var _awardsTxt:FilterFrameText;
      
      private var _takeAwayTxt:FilterFrameText;
      
      private var _timeOpenTxt:FilterFrameText;
      
      private var _doubleTimeTxt:FilterFrameText;
      
      private var _progressTxtImage:Bitmap;
      
      private var _progressBg:Bitmap;
      
      private var _progressCover:Bitmap;
      
      private var _progressTxt:FilterFrameText;
      
      private var _blessLvTxt:FilterFrameText;
      
      private var blessBtn:BaseButton;
      
      private var talkBoxMc:MovieClip;
      
      private var blessHarderBtn:BaseButton;
      
      private var _witchBlessingFrame:WitchBlessingFrame;
      
      private var itemInfo:ItemTemplateInfo;
      
      private var cell:BagCell;
      
      private var maxNumInBag:int;
      
      private var maxMoneyCount:int;
      
      private const ITEMID:int = 201443;
      
      private const ONCE_BLESS_EXP:int = ServerConfigManager.instance.getWitchBlessGP[0];
      
      private const ONCE_HARDER_BLESS_EXP:int = ServerConfigManager.instance.getWitchBlessGP[2];
      
      private const ONCE_BLESS_MONEY:int = ServerConfigManager.instance.getWitchBlessMoney;
      
      private var expArr:Array;
      
      private var awardsNums:Array;
      
      private var doubleMoney:Array;
      
      public var nowLv:int = 0;
      
      private var needExp:int;
      
      private var allHarderBlessMax:int;
      
      private var allBlessMax:int;
      
      private var nextLvBlessMax:int;
      
      private var nextLvHarderBlessMax:int;
      
      private var _allIn:SelectedCheckButton;
      
      public function WitchBlessingMainView()
      {
         this.expArr = [0,500,1200,2200];
         this.awardsNums = [3,3,3];
         this.doubleMoney = [0,0,0];
         var _loc1_:WitchBlessingModel = WitchBlessingManager.Instance.model;
         this.expArr = _loc1_.ExpArr;
         this.awardsNums = _loc1_.AwardsNums;
         this.doubleMoney = _loc1_.DoubleMoney;
         super();
         this.initView();
         this.initEvent();
      }
      
      private function initView() : void
      {
         var _loc1_:Bitmap = null;
         var _loc3_:Bitmap = null;
         var _loc4_:Bitmap = null;
         var _loc5_:Bitmap = null;
         this.itemInfo = ItemManager.Instance.getTemplateById(this.ITEMID) as ItemTemplateInfo;
         this.maxNumInBag = PlayerManager.Instance.Self.PropBag.getItemCountByTemplateId(this.itemInfo.TemplateID);
         this.maxMoneyCount = int(PlayerManager.Instance.Self.Money / this.ONCE_BLESS_MONEY);
         this.titleText = LanguageMgr.GetTranslation("witchBlessing.view.title");
         this._leftView = new Sprite();
         this._rightView = new Sprite();
         this.lv1View = new WitchBlessingRightView(1,this.awardsNums[0],this.doubleMoney[0]);
         this.lv2View = new WitchBlessingRightView(2,this.awardsNums[1],this.doubleMoney[1]);
         this.lv3View = new WitchBlessingRightView(3,this.awardsNums[2],this.doubleMoney[2]);
         this.lv2View.visible = false;
         this.lv3View.visible = false;
         this.titleMc = ComponentFactory.Instance.creat("witchBlessing.titleMc");
         this.titleMc.gotoAndStop(1);
         PositionUtils.setPos(this.titleMc,"witchBlessing.titleMcPos");
         PositionUtils.setPos(this._leftView,"witchBlessing.leftViewPos");
         PositionUtils.setPos(this._rightView,"witchBlessing.rightViewPos");
         this._bottom = ComponentFactory.Instance.creatComponentByStylename("witchBlessing.frameBottom");
         _loc1_ = ComponentFactory.Instance.creat("asset.witchBlessing.leftBg");
         var _loc2_:Bitmap = ComponentFactory.Instance.creat("asset.witchBlessing.rightBg");
         _loc3_ = ComponentFactory.Instance.creat("asset.witchBlessing.rightViewBigBg");
         _loc4_ = ComponentFactory.Instance.creat("asset.witchBlessing.purpleBg");
         _loc5_ = ComponentFactory.Instance.creat("asset.witchBlessing.nextTimeBg");
         this.lv1Btn = ComponentFactory.Instance.creatComponentByStylename("blessingLv1.btn");
         this.lv2Btn = ComponentFactory.Instance.creatComponentByStylename("blessingLv2.btn");
         this.lv3Btn = ComponentFactory.Instance.creatComponentByStylename("blessingLv3.btn");
         this._takeAwayTxt = ComponentFactory.Instance.creatComponentByStylename("witchBlessing.takeAwayTxt");
         this._takeAwayTxt.text = LanguageMgr.GetTranslation("witchBlessing.view.takeAwayTxt",ServerConfigManager.instance.getWitchBlessGP[1]);
         this._timeOpenTxt = ComponentFactory.Instance.creatComponentByStylename("witchBlessing.timeOpenTxt");
         this._timeOpenTxt.text = LanguageMgr.GetTranslation("witchBlessing.view.timeOpenTxt",WitchBlessingManager.Instance.model.activityTime);
         this._doubleTimeTxt = ComponentFactory.Instance.creatComponentByStylename("witchBlessing.timeOpenTxt");
         this._doubleTimeTxt.text = LanguageMgr.GetTranslation("witchBlessing.view.doubleBlessTimeTxt",ServerConfigManager.instance.getWitchBlessDoubleGpTime);
         PositionUtils.setPos(this._doubleTimeTxt,"witchBlessing.doubleTimePos");
         this._btnGroup = new SelectedButtonGroup();
         this._btnGroup.addSelectItem(this.lv1Btn);
         this._btnGroup.addSelectItem(this.lv2Btn);
         this._btnGroup.addSelectItem(this.lv3Btn);
         this._btnGroup.selectIndex = 0;
         this._progressTxtImage = ComponentFactory.Instance.creatBitmap("asset.witchBlessing.progressTxt");
         this._progressBg = ComponentFactory.Instance.creatBitmap("asset.witchBlessing.progressBg");
         this._progressCover = ComponentFactory.Instance.creatBitmap("asset.witchBlessing.progress");
         this._progressTxt = ComponentFactory.Instance.creatComponentByStylename("witchBlessing.progressTxt");
         this._progressTxt.text = "0/0";
         this._blessLvTxt = ComponentFactory.Instance.creatComponentByStylename("witchBlessing.blessingLvTxt");
         this.blessBtn = ComponentFactory.Instance.creat("witchBlessing.blessBtn");
         this.blessHarderBtn = ComponentFactory.Instance.creat("witchBlessing.blessHarderBtn");
         this._allIn = ComponentFactory.Instance.creatComponentByStylename("witchBlessing.allIn");
         this._allIn.text = LanguageMgr.GetTranslation("ddt.pets.risingStar.tisingStarTxt");
         this._allIn.selected = true;
         this.cell = new BagCell(0,this.itemInfo,false,null,false);
         this.cell.setBgVisible(false);
         this.cell.setContentSize(62,62);
         this.cell.setCount(this.maxNumInBag);
         PositionUtils.setPos(this.cell,"witchBlessing.cellPos");
         this.talkBoxMc = ComponentFactory.Instance.creat("asset.witchBlessing.talkBoxMc");
         var _loc6_:FilterFrameText = ComponentFactory.Instance.creatComponentByStylename("witchBlessing.talkTxt");
         this.talkBoxMc.addChild(_loc6_);
         _loc6_.text = LanguageMgr.GetTranslation("witchBlessing.view.doubleBlessing");
         this.talkBoxMc.visible = false;
         PositionUtils.setPos(this.talkBoxMc,"witchBlessing.talkBoxMcPos");
         this._leftView.addChild(_loc1_);
         this._leftView.addChild(_loc4_);
         this._leftView.addChild(this._takeAwayTxt);
         this._leftView.addChild(this._timeOpenTxt);
         this._leftView.addChild(this._doubleTimeTxt);
         this._leftView.addChild(this._progressTxtImage);
         this._leftView.addChild(this._progressBg);
         this._leftView.addChild(this._progressCover);
         this._leftView.addChild(this._progressTxt);
         this._leftView.addChild(this._blessLvTxt);
         this._leftView.addChild(this._allIn);
         this._leftView.addChild(this.blessBtn);
         this._leftView.addChild(this.blessHarderBtn);
         this._leftView.addChild(this.cell);
         this._leftView.addChild(this.talkBoxMc);
         this._rightView.addChild(this.lv1Btn);
         this._rightView.addChild(this.lv2Btn);
         this._rightView.addChild(this.lv3Btn);
         this._rightView.addChild(_loc3_);
         this._rightView.addChild(_loc5_);
         this._rightView.addChild(this.lv1View);
         this._rightView.addChild(this.lv2View);
         this._rightView.addChild(this.lv3View);
         this._rightView.addChild(this.titleMc);
         addToContent(this._bottom);
         addToContent(_loc2_);
         addToContent(this._leftView);
         addToContent(this._rightView);
      }
      
      public function flushData() : void
      {
         this.talkBoxMc.visible = WitchBlessingManager.Instance.model.isDouble;
         var _loc1_:int = WitchBlessingManager.Instance.model.totalExp;
         var _loc2_:Array = [this.lv1View,this.lv2View,this.lv3View];
         var _loc3_:int = 3;
         while(_loc3_ > 0)
         {
            if(_loc1_ == this.expArr[3])
            {
               this.nowLv = 3;
               this.needExp = 0;
               break;
            }
            if(_loc1_ < this.expArr[_loc3_] && _loc1_ >= this.expArr[_loc3_ - 1])
            {
               this.nowLv = _loc3_ - 1;
               this.needExp = this.expArr[_loc3_] - _loc1_;
               break;
            }
            if(_loc1_ == this.expArr[0])
            {
               this.nowLv = 0;
               this.needExp = this.expArr[1];
               break;
            }
            _loc3_--;
         }
         if(WitchBlessingManager.Instance.model.isDouble)
         {
            this.allHarderBlessMax = Math.ceil((this.expArr[3] - _loc1_) / (this.ONCE_HARDER_BLESS_EXP * 2));
            this.allBlessMax = Math.ceil((this.expArr[3] - _loc1_) / (this.ONCE_BLESS_EXP * 2));
            if(this.nowLv != 3)
            {
               this.nextLvHarderBlessMax = Math.ceil((this.expArr[this.nowLv + 1] - _loc1_) / (this.ONCE_HARDER_BLESS_EXP * 2));
               this.nextLvBlessMax = Math.ceil((this.expArr[this.nowLv + 1] - _loc1_) / (this.ONCE_BLESS_EXP * 2));
            }
         }
         else
         {
            this.allHarderBlessMax = Math.ceil((this.expArr[3] - _loc1_) / this.ONCE_HARDER_BLESS_EXP);
            this.allBlessMax = Math.ceil((this.expArr[3] - _loc1_) / this.ONCE_BLESS_EXP);
            if(this.nowLv != 3)
            {
               this.nextLvHarderBlessMax = Math.ceil((this.expArr[this.nowLv + 1] - _loc1_) / this.ONCE_HARDER_BLESS_EXP);
               this.nextLvBlessMax = Math.ceil((this.expArr[this.nowLv + 1] - _loc1_) / this.ONCE_BLESS_EXP);
            }
         }
         this.flushEXP(_loc1_);
         this.maxNumInBag = PlayerManager.Instance.Self.PropBag.getItemCountByTemplateId(this.itemInfo.TemplateID);
         this.maxMoneyCount = int(PlayerManager.Instance.Self.Money / this.ONCE_BLESS_MONEY);
         this.cell.setCount(this.maxNumInBag);
         this._blessLvTxt.text = this.nowLv + "";
         this.lv1View.flushGetAwardsTimes(WitchBlessingManager.Instance.model.lv1GetAwardsTimes);
         this.lv2View.flushGetAwardsTimes(WitchBlessingManager.Instance.model.lv2GetAwardsTimes);
         this.lv3View.flushGetAwardsTimes(WitchBlessingManager.Instance.model.lv3GetAwardsTimes);
         this.lv1View.flushCDTime(WitchBlessingManager.Instance.model.lv1CD);
         this.lv2View.flushCDTime(WitchBlessingManager.Instance.model.lv2CD);
         this.lv3View.flushCDTime(WitchBlessingManager.Instance.model.lv3CD);
      }
      
      private function flushEXP(param1:int) : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         if(this.nowLv == 3)
         {
            this._progressTxt.text = "0/0";
            this._progressCover.scaleX = 1;
         }
         else
         {
            _loc2_ = param1 - this.expArr[this.nowLv];
            _loc3_ = _loc2_ + this.needExp;
            this._progressTxt.text = _loc2_ + "/" + _loc3_;
            this._progressCover.scaleX = _loc2_ / _loc3_;
         }
      }
      
      private function initEvent() : void
      {
         addEventListener(FrameEvent.RESPONSE,this.__frameEventHandler);
         this._btnGroup.addEventListener(Event.CHANGE,this.__changeHandler);
         this.lv1Btn.addEventListener(MouseEvent.CLICK,this.__soundPlay);
         this.lv2Btn.addEventListener(MouseEvent.CLICK,this.__soundPlay);
         this.lv3Btn.addEventListener(MouseEvent.CLICK,this.__soundPlay);
         this.blessHarderBtn.addEventListener(MouseEvent.CLICK,this.__blessHarderFunc);
         this.blessBtn.addEventListener(MouseEvent.CLICK,this.__blessFunc);
      }
      
      private function removeEvent() : void
      {
         removeEventListener(FrameEvent.RESPONSE,this.__frameEventHandler);
         if(this._btnGroup)
         {
            this._btnGroup.removeEventListener(Event.CHANGE,this.__changeHandler);
         }
         if(this.lv1Btn)
         {
            this.lv1Btn.removeEventListener(MouseEvent.CLICK,this.__soundPlay);
         }
         if(this.lv2Btn)
         {
            this.lv2Btn.removeEventListener(MouseEvent.CLICK,this.__soundPlay);
         }
         if(this.lv3Btn)
         {
            this.lv3Btn.removeEventListener(MouseEvent.CLICK,this.__soundPlay);
         }
         if(this.blessHarderBtn)
         {
            this.blessHarderBtn.removeEventListener(MouseEvent.CLICK,this.__blessHarderFunc);
         }
         if(this.blessBtn)
         {
            this.blessBtn.removeEventListener(MouseEvent.CLICK,this.__blessFunc);
         }
      }
      
      private function __changeHandler(param1:Event) : void
      {
         var _loc2_:int = this._btnGroup.selectIndex + 1;
         this.hideRightAllView();
         this.titleMc.gotoAndStop(_loc2_);
         (this["lv" + _loc2_ + "View"] as WitchBlessingRightView).visible = true;
      }
      
      private function __blessFunc(param1:MouseEvent) : void
      {
         var _loc2_:int = 0;
         if(this.nowLv == 3)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("witchBlessing.view.nowLvIsThree"));
            return;
         }
         if(this.maxNumInBag > 0)
         {
            _loc2_ = 1;
            if(this._allIn.selected)
            {
               _loc2_ = this.nextLvBlessMax;
            }
            SocketManager.Instance.out.sendWitchBless(_loc2_);
         }
         else
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("witchBlessing.view.youCannotBlessing"));
         }
      }
      
      private function __blessHarderFunc(param1:MouseEvent) : void
      {
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         if(this.nowLv == 3)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("witchBlessing.view.nowLvIsThree"));
            return;
         }
         if(this.maxMoneyCount > 0)
         {
            this._witchBlessingFrame = new WitchBlessingFrame();
            this._witchBlessingFrame = ComponentFactory.Instance.creatComponentByStylename("witchBlessing.WitchBlessingFrame");
            this._witchBlessingFrame.addEventListener(FrameEvent.RESPONSE,this.__framResponse);
            this._witchBlessingFrame.show(this.getNeedCount(),this.needExp,this.maxMoneyCount,this.nextLvHarderBlessMax);
         }
         else
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("witchBlessing.view.doubleBlessingNoMoney"));
         }
      }
      
      private function __framResponse(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         switch(param1.responseCode)
         {
            case FrameEvent.ESC_CLICK:
            case FrameEvent.CANCEL_CLICK:
            case FrameEvent.CLOSE_CLICK:
               this._witchBlessingFrame.dispose();
               break;
            case FrameEvent.SUBMIT_CLICK:
            case FrameEvent.ENTER_CLICK:
               SocketManager.Instance.out.sendWitchBless(this._witchBlessingFrame.count,true);
               this._witchBlessingFrame.dispose();
         }
      }
      
      private function getNeedCount() : int
      {
         var _loc1_:int = this.needExp / this.ONCE_BLESS_EXP;
         return _loc1_;
      }
      
      private function hideRightAllView() : void
      {
         this.lv1View.visible = false;
         this.lv2View.visible = false;
         this.lv3View.visible = false;
      }
      
      public function show() : void
      {
         LayerManager.Instance.addToLayer(this,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.ALPHA_BLOCKGOUND);
      }
      
      private function __frameEventHandler(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         switch(param1.responseCode)
         {
            case FrameEvent.ESC_CLICK:
            case FrameEvent.CLOSE_CLICK:
               this.dispose();
         }
      }
      
      private function __soundPlay(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
      }
      
      override public function dispose() : void
      {
         SocketManager.Instance.out.witchBlessing_enter(1);
         super.dispose();
         this.removeEvent();
         this.lv1View.dispose();
         this.lv2View.dispose();
         this.lv3View.dispose();
         ObjectUtils.disposeAllChildren(this);
         WitchBlessingManager.Instance.hide();
         if(this._leftView)
         {
            this._leftView = null;
         }
         if(this._rightView)
         {
            this._rightView = null;
         }
         if(this._bottom)
         {
            this._bottom = null;
         }
         if(this._btnGroup)
         {
            this._btnGroup = null;
         }
         if(this.lv1Btn)
         {
            this.lv1Btn = null;
         }
         if(this.lv2Btn)
         {
            this.lv2Btn = null;
         }
         if(this.lv3Btn)
         {
            this.lv3Btn = null;
         }
         if(this.lv1View)
         {
            this.lv1View = null;
         }
         if(this.lv2View)
         {
            this.lv2View = null;
         }
         if(this.lv3View)
         {
            this.lv3View = null;
         }
         if(this.titleMc)
         {
            this.titleMc = null;
         }
         if(this._awardsTxt)
         {
            this._awardsTxt = null;
         }
         if(this._takeAwayTxt)
         {
            this._takeAwayTxt = null;
         }
         if(this._timeOpenTxt)
         {
            this._timeOpenTxt = null;
         }
         if(this._doubleTimeTxt)
         {
            this._doubleTimeTxt = null;
         }
         if(this._progressTxtImage)
         {
            this._progressTxtImage = null;
         }
         if(this._progressBg)
         {
            this._progressBg = null;
         }
         if(this._progressCover)
         {
            this._progressCover = null;
         }
         if(this._progressTxt)
         {
            this._progressTxt = null;
         }
         if(this._blessLvTxt)
         {
            this._blessLvTxt = null;
         }
         if(this.blessBtn)
         {
            this.blessBtn = null;
         }
         if(this.talkBoxMc)
         {
            this.talkBoxMc = null;
         }
         if(this.blessHarderBtn)
         {
            this.blessHarderBtn = null;
         }
         if(this._witchBlessingFrame)
         {
            this._witchBlessingFrame = null;
         }
         if(this.itemInfo)
         {
            this.itemInfo = null;
         }
         if(this.cell)
         {
            this.cell = null;
         }
      }
   }
}
