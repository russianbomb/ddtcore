package consortion.data
{
   public class ConsortiaBossDataVo
   {
       
      
      public var rank:int;
      
      public var name:String;
      
      public var damage:int;
      
      public var contribution:int;
      
      public var honor:int;
      
      public function ConsortiaBossDataVo()
      {
         super();
      }
   }
}
