package consortion.view.selfConsortia
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.image.MutipleImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.LanguageMgr;
   import flash.display.Sprite;
   
   public class ConsortionShopItemBtn extends Sprite implements Disposeable
   {
       
      
      private var _bg:MutipleImage;
      
      private var _day:FilterFrameText;
      
      private var _pay:FilterFrameText;
      
      public function ConsortionShopItemBtn()
      {
         super();
         this.initView();
      }
      
      private function initView() : void
      {
         buttonMode = true;
         this._bg = ComponentFactory.Instance.creatComponentByStylename("consortion.shopItem.btnBG");
         this._day = ComponentFactory.Instance.creatComponentByStylename("consortion.shopItemBtn.day");
         this._pay = ComponentFactory.Instance.creatComponentByStylename("consortion.shopItemBtn.Pay");
         addChild(this._bg);
         addChild(this._day);
         addChild(this._pay);
      }
      
      public function setValue(param1:String, param2:String) : void
      {
         this.filteringValue(param1);
         this._pay.text = param2;
      }
      
      private function filteringValue(param1:String) : void
      {
         var _loc2_:Array = null;
         var _loc3_:String = null;
         if(param1.indexOf("Слава") != -1)
         {
            _loc2_ = param1.split("Слава");
            _loc3_ = LanguageMgr.GetTranslation("ConsortionShopItemBtn.name") + "  " + _loc2_[0];
            this._day.text = _loc3_;
         }
         else
         {
            this._day.text = param1;
         }
      }
      
      public function dispose() : void
      {
         ObjectUtils.disposeAllChildren(this);
         this._bg = null;
         this._day = null;
         this._pay = null;
         if(this.parent)
         {
            this.parent.removeChild(this);
         }
      }
   }
}
