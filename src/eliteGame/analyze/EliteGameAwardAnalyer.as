package eliteGame.analyze
{
   import com.pickgliss.loader.DataAnalyzer;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.manager.ItemManager;
   
   public class EliteGameAwardAnalyer extends DataAnalyzer
   {
       
      
      public var award30_39:Object;
      
      public var award40_60:Object;
      
      public function EliteGameAwardAnalyer(param1:Function)
      {
         super(param1);
      }
      
      override public function analyze(param1:*) : void
      {
         var _loc3_:XMLList = null;
         var _loc4_:int = 0;
         var _loc5_:XMLList = null;
         var _loc6_:int = 0;
         var _loc7_:Vector.<InventoryItemInfo> = null;
         var _loc8_:XMLList = null;
         var _loc9_:String = null;
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         var _loc12_:InventoryItemInfo = null;
         var _loc13_:ItemTemplateInfo = null;
         this.award30_39 = new Object();
         this.award40_60 = new Object();
         var _loc2_:XML = new XML(param1);
         if(_loc2_.@value == "true")
         {
            _loc3_ = _loc2_.PlayerGrade;
            _loc4_ = 0;
            while(_loc4_ < _loc3_.length())
            {
               _loc5_ = _loc3_[_loc4_].prizeType;
               _loc6_ = 0;
               while(_loc6_ < _loc5_.length())
               {
                  _loc7_ = new Vector.<InventoryItemInfo>();
                  _loc8_ = _loc5_[_loc6_].Item;
                  _loc9_ = _loc5_[_loc6_].@value;
                  _loc10_ = 0;
                  while(_loc10_ < _loc8_.length())
                  {
                     _loc11_ = parseInt(_loc8_[_loc10_].@ItemTemplateID);
                     _loc12_ = new InventoryItemInfo();
                     _loc13_ = ItemManager.Instance.getTemplateById(_loc11_);
                     ObjectUtils.copyProperties(_loc12_,_loc13_);
                     ObjectUtils.copyPorpertiesByXML(_loc12_,_loc8_[_loc10_]);
                     _loc12_.BindType = 4;
                     _loc7_.push(_loc12_);
                     _loc10_++;
                  }
                  if(_loc3_[_loc4_].@value == "1")
                  {
                     this.award30_39[_loc9_] = _loc7_;
                  }
                  else
                  {
                     this.award40_60[_loc9_] = _loc7_;
                  }
                  _loc6_++;
               }
               _loc4_++;
            }
            onAnalyzeComplete();
         }
         else
         {
            message = _loc2_.@message;
            onAnalyzeError();
            onAnalyzeComplete();
         }
      }
   }
}
