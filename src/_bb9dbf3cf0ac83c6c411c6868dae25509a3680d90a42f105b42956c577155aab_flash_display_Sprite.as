package
{
   import flash.display.Sprite;
   import flash.system.Security;
   
   [ExcludeClass]
   public class _bb9dbf3cf0ac83c6c411c6868dae25509a3680d90a42f105b42956c577155aab_flash_display_Sprite extends Sprite
   {
       
      
      public function _bb9dbf3cf0ac83c6c411c6868dae25509a3680d90a42f105b42956c577155aab_flash_display_Sprite()
      {
         super();
      }
      
      public function allowDomainInRSL(... rest) : void
      {
         Security.allowDomain(rest);
      }
      
      public function allowInsecureDomainInRSL(... rest) : void
      {
         Security.allowInsecureDomain(rest);
      }
   }
}
