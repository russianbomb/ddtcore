package playerDress.data
{
   import flash.events.Event;
   import road7th.comm.PackageIn;
   
   public class PlayerDressEvent extends Event
   {
      
      public static const GET_DRESS_MODEL:String = "getDressModel";
      
      public static const CURRENT_MODEL:String = "currentModel";
      
      public static const LOCK_DRESSBAG:String = "lockDressBag";
       
      
      private var _pkg:PackageIn;
      
      public function PlayerDressEvent(param1:String, param2:PackageIn = null)
      {
         super(param1,bubbles,cancelable);
         this._pkg = param2;
      }
      
      public function get pkg() : PackageIn
      {
         return this._pkg;
      }
   }
}
