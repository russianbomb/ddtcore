package playerDress.views
{
   import bagAndInfo.cell.BagCell;
   import bagAndInfo.cell.CellFactory;
   import baglocked.BaglockedManager;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.events.ListItemEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.ComponentSetting;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.ComboBox;
   import com.pickgliss.ui.controls.SelectedCheckButton;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.controls.list.VectorListModel;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.image.MovieImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.BagInfo;
   import ddt.data.EquipType;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.data.player.PlayerInfo;
   import ddt.events.BagEvent;
   import ddt.events.CellEvent;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.manager.TimeManager;
   import ddt.utils.PositionUtils;
   import ddt.view.character.CharactoryFactory;
   import ddt.view.character.RoomCharacter;
   import ddt.view.tips.OneLineTip;
   import flash.display.Bitmap;
   import flash.display.DisplayObject;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.geom.Point;
   import flash.utils.Dictionary;
   import magicStone.MagicStoneManager;
   import playerDress.PlayerDressManager;
   import playerDress.components.DressModel;
   import playerDress.components.DressUtils;
   import playerDress.data.DressVo;
   import road7th.data.DictionaryData;
   import road7th.utils.DateUtils;
   import store.HelpFrame;
   import trainer.data.ArrowType;
   import trainer.data.Step;
   import trainer.view.NewHandContainer;
   
   public class PlayerDressView extends Sprite implements Disposeable
   {
      
      public static const CELLS_LENGTH:uint = 8;
      
      public static const NEED_GOLD:int = 5000;
       
      
      private var _BG:Bitmap;
      
      private var _character:RoomCharacter;
      
      private var _hidePanel:MovieImage;
      
      private var _saveBtn:SimpleBitmapButton;
      
      private var _selectedBox:ComboBox;
      
      private var _okBtn:SimpleBitmapButton;
      
      private var _okBtnSprite:Sprite;
      
      private var _okBtnTip:OneLineTip;
      
      private var _descTxt:FilterFrameText;
      
      private var _hideHatBtn:SelectedCheckButton;
      
      private var _hideGlassBtn:SelectedCheckButton;
      
      private var _hideSuitBtn:SelectedCheckButton;
      
      private var _hideWingBtn:SelectedCheckButton;
      
      private var _dressCells:Vector.<BagCell>;
      
      private var _helpBtn:BaseButton;
      
      private var _currentModel:DressModel;
      
      private var _currentIndex:int;
      
      private var _comboBoxArr:Array;
      
      private var _self:PlayerInfo;
      
      private var _bodyThings:DictionaryData;
      
      private var saveBtnClick:Boolean = false;
      
      public function PlayerDressView()
      {
         this._comboBoxArr = [];
         super();
         this.initData();
         this.initView();
         this.initEvent();
      }
      
      private function initData() : void
      {
         PlayerDressManager.instance.dressView = this;
         this._currentIndex = PlayerDressManager.instance.currentIndex;
         this._currentModel = new DressModel();
         this._dressCells = new Vector.<BagCell>();
         this._comboBoxArr.push(LanguageMgr.GetTranslation("playerDress.model1"));
         this._comboBoxArr.push(LanguageMgr.GetTranslation("playerDress.model2"));
         this._comboBoxArr.push(LanguageMgr.GetTranslation("playerDress.model3"));
      }
      
      private function initView() : void
      {
         var _loc2_:BagCell = null;
         this._BG = ComponentFactory.Instance.creat("playerDress.BG");
         addChild(this._BG);
         this._hidePanel = ComponentFactory.Instance.creatComponentByStylename("bagAndInfo.view.ddtbg");
         PositionUtils.setPos(this._hidePanel,"playerDress.hidePanelPos");
         addChild(this._hidePanel);
         this._saveBtn = ComponentFactory.Instance.creatComponentByStylename("playerDress.saveBtn");
         addChild(this._saveBtn);
         this._saveBtn.enable = false;
         this._okBtn = ComponentFactory.Instance.creatComponentByStylename("playerDress.okBtn");
         addChild(this._okBtn);
         this.enableOKBtn(false);
         this._okBtn.tipData = LanguageMgr.GetTranslation("playerDress.okBtnTip");
         ComponentSetting.COMBOX_LIST_LAYER = LayerManager.Instance.getLayerByType(LayerManager.GAME_TOP_LAYER);
         this._selectedBox = ComponentFactory.Instance.creatComponentByStylename("playerDress.modelCombo");
         this._selectedBox.selctedPropName = "text";
         if(this._currentIndex == -1)
         {
            this._currentIndex = 0;
         }
         this._selectedBox.textField.text = this._comboBoxArr[this._currentIndex];
         addChild(this._selectedBox);
         this._descTxt = ComponentFactory.Instance.creatComponentByStylename("playerDress.decTxt");
         this._descTxt.text = LanguageMgr.GetTranslation("playerDress.descTxt");
         addChild(this._descTxt);
         this._hideHatBtn = ComponentFactory.Instance.creatComponentByStylename("playerDress.hideHatCheckBox");
         addChild(this._hideHatBtn);
         this._hideGlassBtn = ComponentFactory.Instance.creatComponentByStylename("playerDress.hideGlassCheckBox");
         addChild(this._hideGlassBtn);
         this._hideSuitBtn = ComponentFactory.Instance.creatComponentByStylename("playerDress.hideSuitCheckBox");
         addChild(this._hideSuitBtn);
         this._hideWingBtn = ComponentFactory.Instance.creatComponentByStylename("playerDress.hideWingCheckBox");
         addChild(this._hideWingBtn);
         this._helpBtn = ComponentFactory.Instance.creatComponentByStylename("playerDress.helpBtn");
         addChild(this._helpBtn);
         var _loc1_:int = 0;
         while(_loc1_ <= CELLS_LENGTH - 1)
         {
            _loc2_ = CellFactory.instance.createPlayerDressItemCell() as BagCell;
            _loc2_.doubleClickEnabled = true;
            _loc2_.addEventListener(CellEvent.DOUBLE_CLICK,this.__cellDoubleClick);
            PositionUtils.setPos(_loc2_,"playerDress.cellPos" + _loc1_);
            this._dressCells.push(_loc2_);
            addChild(_loc2_);
            _loc1_++;
         }
         this.updateComboBox(this._comboBoxArr[this._currentIndex]);
         this.updateModel();
      }
      
      public function updateModel() : void
      {
         var _loc3_:InventoryItemInfo = null;
         var _loc4_:InventoryItemInfo = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         this._self = PlayerManager.Instance.Self;
         if(this._self.Sex)
         {
            this._currentModel.model.updateStyle(this._self.Sex,this._self.Hide,DressModel.DEFAULT_MAN_STYLE,",,,,,,","");
         }
         else
         {
            this._currentModel.model.updateStyle(this._self.Sex,this._self.Hide,DressModel.DEFAULT_WOMAN_STYLE,",,,,,,","");
         }
         this._bodyThings = new DictionaryData();
         var _loc1_:Array = PlayerDressManager.instance.modelArr[this._currentIndex];
         var _loc2_:Boolean = false;
         if(_loc1_)
         {
            _loc5_ = 0;
            while(_loc5_ <= _loc1_.length - 1)
            {
               _loc6_ = (_loc1_[_loc5_] as DressVo).templateId;
               _loc7_ = (_loc1_[_loc5_] as DressVo).itemId;
               _loc4_ = new InventoryItemInfo();
               _loc3_ = this._self.Bag.getItemByItemId(_loc7_);
               if(!_loc3_)
               {
                  _loc3_ = this._self.Bag.getItemByTemplateId(_loc6_);
                  _loc2_ = true;
               }
               if(_loc3_)
               {
                  _loc4_.setIsUsed(_loc3_.IsUsed);
                  ObjectUtils.copyProperties(_loc4_,_loc3_);
                  _loc8_ = DressUtils.findItemPlace(_loc4_);
                  this._bodyThings.add(_loc8_,_loc4_);
                  if(_loc4_.CategoryID == EquipType.FACE)
                  {
                     this._currentModel.model.Skin = _loc4_.Skin;
                  }
                  this._currentModel.model.setPartStyle(_loc4_.CategoryID,_loc4_.NeedSex,_loc4_.TemplateID,_loc4_.Color);
               }
               _loc5_++;
            }
         }
         this._currentModel.model.Bag.items = this._bodyThings;
         if(_loc2_)
         {
            this.saveModel();
         }
         this.setBtnsStatus();
         this.updateCharacter();
         this.updateHideCheckBox();
      }
      
      public function putOnDress(param1:InventoryItemInfo) : void
      {
         var _loc2_:InventoryItemInfo = new InventoryItemInfo();
         _loc2_.setIsUsed(param1.IsUsed);
         ObjectUtils.copyProperties(_loc2_,param1);
         var _loc3_:int = DressUtils.findItemPlace(_loc2_);
         this._currentModel.model.Bag.items.add(_loc3_,_loc2_);
         if(_loc2_.CategoryID == EquipType.FACE)
         {
            this._currentModel.model.Skin = _loc2_.Skin;
         }
         this._currentModel.model.setPartStyle(_loc2_.CategoryID,_loc2_.NeedSex,_loc2_.TemplateID,_loc2_.Color);
         this.updateCharacter();
         this.setBtnsStatus();
      }
      
      private function initEvent() : void
      {
         this._self.Bag.addEventListener(BagEvent.UPDATE,this.__updateGoods);
         this._hideHatBtn.addEventListener(Event.SELECT,this.__hideHatChange);
         this._hideGlassBtn.addEventListener(Event.SELECT,this.__hideGlassChange);
         this._hideSuitBtn.addEventListener(Event.SELECT,this.__hideSuitesChange);
         this._hideWingBtn.addEventListener(Event.SELECT,this.__hideWingClickHandler);
         this._selectedBox.listPanel.list.addEventListener(ListItemEvent.LIST_ITEM_CLICK,this.__onListClick);
         this._saveBtn.addEventListener(MouseEvent.CLICK,this.__saveBtnClick);
         this._okBtn.addEventListener(MouseEvent.CLICK,this.__okBtnClick);
         this._helpBtn.addEventListener(MouseEvent.CLICK,this.__helpBtnClick);
      }
      
      protected function __updateGoods(param1:BagEvent) : void
      {
         var _loc3_:* = null;
         var _loc4_:InventoryItemInfo = null;
         var _loc5_:* = null;
         var _loc6_:InventoryItemInfo = null;
         var _loc7_:InventoryItemInfo = null;
         var _loc8_:int = 0;
         var _loc2_:Dictionary = param1.changedSlots;
         for(_loc3_ in this._currentModel.model.Bag.items)
         {
            _loc4_ = this._currentModel.model.Bag.items[_loc3_];
            if(_loc4_)
            {
               for(_loc5_ in _loc2_)
               {
                  _loc6_ = _loc2_[_loc5_];
                  if(this._self.Bag.items[_loc5_])
                  {
                     if(_loc4_.ItemID != 0 && _loc4_.ItemID == _loc6_.ItemID || _loc4_.ItemID == 0 && _loc4_.TemplateID == _loc6_.TemplateID)
                     {
                        _loc7_ = new InventoryItemInfo();
                        _loc7_.setIsUsed(_loc6_.IsUsed);
                        ObjectUtils.copyProperties(_loc7_,_loc6_);
                        this._currentModel.model.Bag.items[_loc3_] = _loc7_;
                        _loc8_ = DressUtils.getBagItems(int(_loc3_),true);
                        if(this._dressCells[_loc8_])
                        {
                           this._dressCells[_loc8_].info = _loc7_;
                        }
                     }
                  }
               }
            }
         }
         this.setBtnsStatus();
      }
      
      protected function __cellDoubleClick(param1:CellEvent) : void
      {
         var _loc3_:int = 0;
         var _loc4_:InventoryItemInfo = null;
         var _loc5_:int = 0;
         var _loc2_:int = 0;
         while(_loc2_ <= CELLS_LENGTH - 1)
         {
            if(this._dressCells[_loc2_] == param1.target)
            {
               _loc3_ = DressUtils.getBagItems(_loc2_);
               _loc4_ = this._currentModel.model.Bag.items[_loc3_];
               _loc5_ = !!this._self.Sex?int(1):int(2);
               this._currentModel.model.setPartStyle(_loc4_.CategoryID,_loc5_);
               if(_loc4_.CategoryID == EquipType.FACE)
               {
                  this._currentModel.model.Skin = "";
               }
               this._currentModel.model.Bag.items[_loc3_] = null;
               this._dressCells[_loc2_].info = null;
            }
            _loc2_++;
         }
         this.setBtnsStatus();
      }
      
      protected function __okBtnClick(param1:MouseEvent) : void
      {
         var _loc3_:BagCell = null;
         var _loc4_:BaseAlerFrame = null;
         SoundManager.instance.play("008");
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         this.enableOKBtn(false);
         this.saveBtnClick = false;
         var _loc2_:int = 0;
         while(_loc2_ <= CELLS_LENGTH - 1)
         {
            _loc3_ = this._dressCells[_loc2_] as BagCell;
            if(_loc3_.info && (_loc3_.info.BindType == 1 || _loc3_.info.BindType == 2 || _loc3_.info.BindType == 3) && _loc3_.itemInfo.IsBinds == false)
            {
               _loc4_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("tank.view.bagII.BagIIView.BindsInfo"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),true,true,true,LayerManager.ALPHA_BLOCKGOUND);
               _loc4_.addEventListener(FrameEvent.RESPONSE,this.__onResponse);
               return;
            }
            _loc2_++;
         }
         this.save();
         this.exchangeProperty();
      }
      
      private function exchangeProperty() : void
      {
         var _loc4_:int = 0;
         var _loc5_:InventoryItemInfo = null;
         var _loc6_:InventoryItemInfo = null;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         PlayerDressManager.instance.lockDressBag();
         var _loc1_:BagInfo = PlayerManager.Instance.Self.Bag;
         var _loc2_:Boolean = false;
         var _loc3_:int = 0;
         while(_loc3_ <= CELLS_LENGTH - 1)
         {
            _loc4_ = DressUtils.getBagItems(_loc3_);
            _loc5_ = this._currentModel.model.Bag.items[_loc4_];
            _loc6_ = this._self.Bag.items[_loc4_];
            if(_loc5_ && this.checkOverDue(_loc5_))
            {
               _loc5_ = null;
            }
            if(_loc5_ && _loc6_)
            {
               _loc7_ = _loc5_.Place;
               _loc8_ = _loc6_.Place;
               if(_loc7_ != _loc8_)
               {
                  if(DressUtils.hasNoAddition(_loc6_))
                  {
                     SocketManager.Instance.out.sendMoveGoods(BagInfo.EQUIPBAG,_loc7_,BagInfo.EQUIPBAG,_loc8_,1,true);
                  }
                  else if(PlayerManager.Instance.Self.Gold < NEED_GOLD)
                  {
                     _loc2_ = true;
                     SocketManager.Instance.out.sendMoveGoods(BagInfo.EQUIPBAG,_loc7_,BagInfo.EQUIPBAG,_loc8_,1,true);
                  }
                  else
                  {
                     SocketManager.Instance.out.sendItemTransfer(true,true,BagInfo.EQUIPBAG,_loc7_,BagInfo.EQUIPBAG,_loc8_);
                     SocketManager.Instance.out.sendMoveGoods(BagInfo.EQUIPBAG,_loc7_,BagInfo.EQUIPBAG,_loc8_,1,true);
                  }
               }
            }
            else if(!_loc5_ && _loc6_)
            {
               SocketManager.Instance.out.sendMoveGoods(BagInfo.EQUIPBAG,_loc6_.Place,BagInfo.EQUIPBAG,-1,1,true);
            }
            else if(_loc5_ && !_loc6_)
            {
               SocketManager.Instance.out.sendMoveGoods(BagInfo.EQUIPBAG,_loc5_.Place,BagInfo.EQUIPBAG,_loc4_,1,true);
            }
            _loc3_++;
         }
         SocketManager.Instance.out.setCurrentModel(this._currentIndex);
         SocketManager.Instance.out.lockDressBag();
         if(_loc2_)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("playerDress.goldNotEnough"));
         }
         if(NewHandContainer.Instance.hasArrow(ArrowType.DRESS_PUT_ON))
         {
            SocketManager.Instance.out.syncWeakStep(Step.DRESS_OPEN);
            NewHandContainer.Instance.clearArrowByID(ArrowType.DRESS_PUT_ON);
         }
         MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("playerDress.changeDressSuccess"));
      }
      
      protected function __saveBtnClick(param1:MouseEvent) : void
      {
         var _loc3_:BagCell = null;
         var _loc4_:BaseAlerFrame = null;
         SoundManager.instance.play("008");
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         this.saveBtnClick = true;
         var _loc2_:int = 0;
         while(_loc2_ <= CELLS_LENGTH - 1)
         {
            _loc3_ = this._dressCells[_loc2_] as BagCell;
            if(_loc3_.info && (_loc3_.info.BindType == 1 || _loc3_.info.BindType == 2 || _loc3_.info.BindType == 3) && _loc3_.itemInfo.IsBinds == false)
            {
               _loc4_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("tank.view.bagII.BagIIView.BindsInfo"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),true,true,true,LayerManager.ALPHA_BLOCKGOUND);
               _loc4_.addEventListener(FrameEvent.RESPONSE,this.__onResponse);
               return;
            }
            _loc2_++;
         }
         this.save();
      }
      
      private function __onResponse(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         var _loc2_:BaseAlerFrame = param1.target as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.__onResponse);
         _loc2_.dispose();
         switch(param1.responseCode)
         {
            case FrameEvent.ENTER_CLICK:
            case FrameEvent.SUBMIT_CLICK:
               this.save();
               if(!this.saveBtnClick)
               {
                  this.exchangeProperty();
               }
               break;
            case FrameEvent.CANCEL_CLICK:
            case FrameEvent.CLOSE_CLICK:
            case FrameEvent.ESC_CLICK:
               this.enableOKBtn(true);
         }
      }
      
      private function save() : void
      {
         this.saveModel();
         this.setBtnsStatus();
         if(this.saveBtnClick)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("playerDress.saveModelSuccess"));
         }
      }
      
      private function saveModel() : void
      {
         var _loc3_:int = 0;
         var _loc4_:InventoryItemInfo = null;
         var _loc1_:Array = [];
         var _loc2_:int = 0;
         while(_loc2_ <= DressModel.DRESS_LEN - 1)
         {
            _loc3_ = DressUtils.getBagItems(_loc2_);
            _loc4_ = this._currentModel.model.Bag.items[_loc3_];
            if(_loc4_)
            {
               _loc1_.push(_loc4_.Place);
            }
            _loc2_++;
         }
         SocketManager.Instance.out.saveDressModel(this._currentIndex,_loc1_);
      }
      
      protected function __onListClick(param1:ListItemEvent) : void
      {
         SoundManager.instance.play("008");
         if(this._currentIndex == this._comboBoxArr.indexOf(param1.cellValue))
         {
            return;
         }
         this._currentIndex = this._comboBoxArr.indexOf(param1.cellValue);
         PlayerDressManager.instance.currentIndex = this._currentIndex;
         this.updateModel();
         this.updateComboBox(param1.cellValue);
      }
      
      private function updateComboBox(param1:*) : void
      {
         var _loc2_:VectorListModel = this._selectedBox.listPanel.vectorListModel;
         _loc2_.clear();
         _loc2_.appendAll(this._comboBoxArr);
         _loc2_.remove(param1);
      }
      
      private function updateHideCheckBox() : void
      {
         this._hideHatBtn.selected = this._currentModel.model.getHatHide();
         this._hideGlassBtn.selected = this._currentModel.model.getGlassHide();
         this._hideSuitBtn.selected = this._currentModel.model.getSuitesHide();
         this._hideWingBtn.selected = this._currentModel.model.wingHide;
      }
      
      private function updateCharacter() : void
      {
         var _loc2_:int = 0;
         if(this._currentModel.model)
         {
            if(!this._character)
            {
               this._character = CharactoryFactory.createCharacter(this._currentModel.model,"room") as RoomCharacter;
               PositionUtils.setPos(this._character,"playerDress.characterPos");
               this._character.showGun = false;
               this._character.show(false,-1);
               addChild(this._character);
            }
         }
         var _loc1_:int = 0;
         while(_loc1_ < this._dressCells.length)
         {
            _loc2_ = DressUtils.getBagItems(_loc1_);
            if(this._currentModel.model.Bag.items[_loc2_])
            {
               this._dressCells[_loc1_].info = this._currentModel.model.Bag.items[_loc2_];
            }
            else
            {
               this._dressCells[_loc1_].info = null;
            }
            _loc1_++;
         }
         MagicStoneManager.instance.updataCharacter(this._currentModel.model);
      }
      
      public function setBtnsStatus() : void
      {
         if(this._saveBtn)
         {
            this._saveBtn.enable = this.canSaveBtnClick();
         }
         var _loc1_:Boolean = this.canOKBtnClick();
         if(this._okBtn)
         {
            this.enableOKBtn(_loc1_);
         }
      }
      
      private function canSaveBtnClick() : Boolean
      {
         var _loc4_:int = 0;
         var _loc5_:InventoryItemInfo = null;
         var _loc6_:Boolean = false;
         var _loc7_:int = 0;
         var _loc8_:DressVo = null;
         var _loc1_:Array = PlayerDressManager.instance.modelArr[this._currentIndex];
         if(!_loc1_)
         {
            if(this._currentModel.model.Bag.items.length == 0)
            {
               return false;
            }
            return true;
         }
         var _loc2_:Array = _loc1_.concat();
         var _loc3_:int = 0;
         while(true)
         {
            if(_loc3_ > CELLS_LENGTH - 1)
            {
               if(_loc2_.length == 0)
               {
                  return false;
               }
               return true;
            }
            _loc4_ = DressUtils.getBagItems(_loc3_);
            _loc5_ = this._currentModel.model.Bag.items[_loc4_];
            if(_loc5_)
            {
               _loc6_ = true;
               _loc7_ = 0;
               while(_loc7_ <= _loc2_.length - 1)
               {
                  _loc8_ = _loc2_[_loc7_];
                  if(_loc5_.ItemID == _loc8_.itemId)
                  {
                     _loc2_.splice(_loc7_,1);
                     _loc6_ = false;
                     break;
                  }
                  _loc7_++;
               }
               if(_loc6_)
               {
                  break;
               }
            }
            _loc3_++;
         }
         return true;
      }
      
      private function canOKBtnClick() : Boolean
      {
         var _loc2_:int = 0;
         var _loc3_:InventoryItemInfo = null;
         var _loc4_:InventoryItemInfo = null;
         var _loc1_:int = 0;
         while(_loc1_ <= CELLS_LENGTH - 1)
         {
            _loc2_ = DressUtils.getBagItems(_loc1_);
            _loc3_ = this._currentModel.model.Bag.items[_loc2_];
            _loc4_ = this._self.Bag.items[_loc2_];
            if(_loc3_ && !_loc4_ && !this.checkOverDue(_loc3_) || !_loc3_ && _loc4_ || _loc3_ && _loc4_ && _loc3_.ItemID != _loc4_.ItemID)
            {
               if(PlayerManager.Instance.Self.Grade >= 3 && !PlayerManager.Instance.Self.IsWeakGuildFinish(Step.DRESS_OPEN))
               {
                  NewHandContainer.Instance.showArrow(ArrowType.DRESS_PUT_ON,180,"playerDress.dressTipPos","","",this,0,true);
               }
               return true;
            }
            _loc1_++;
         }
         return false;
      }
      
      private function checkOverDue(param1:InventoryItemInfo) : Boolean
      {
         var _loc5_:uint = 0;
         var _loc2_:Date = TimeManager.Instance.Now();
         var _loc3_:Date = DateUtils.getDateByStr(param1.BeginDate);
         var _loc4_:Number = Math.round((_loc2_.getTime() - _loc3_.getTime()) / 1000);
         if(_loc4_ >= 0)
         {
            _loc5_ = Math.floor(_loc4_ / 60 / 60 / 24);
         }
         if(_loc4_ < 0 || param1.IsUsed == true && param1.ValidDate > 0 && _loc5_ >= param1.ValidDate)
         {
            return true;
         }
         return false;
      }
      
      private function __hideWingClickHandler(param1:Event) : void
      {
         this._currentModel.model.wingHide = this._hideWingBtn.selected;
      }
      
      private function __hideSuitesChange(param1:Event) : void
      {
         this._currentModel.model.setSuiteHide(this._hideSuitBtn.selected);
      }
      
      private function __hideGlassChange(param1:Event) : void
      {
         this._currentModel.model.setGlassHide(this._hideGlassBtn.selected);
      }
      
      private function __hideHatChange(param1:Event) : void
      {
         this._currentModel.model.setHatHide(this._hideHatBtn.selected);
      }
      
      protected function __helpBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         var _loc2_:DisplayObject = ComponentFactory.Instance.creat("playerDress.HelpPrompt");
         var _loc3_:HelpFrame = ComponentFactory.Instance.creat("playerDress.HelpFrame");
         _loc3_.setView(_loc2_);
         _loc3_.titleText = LanguageMgr.GetTranslation("playerDress.dressReadMe");
         LayerManager.Instance.addToLayer(_loc3_,LayerManager.STAGE_DYANMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
      }
      
      private function enableOKBtn(param1:Boolean) : void
      {
         this._okBtn.enable = param1;
         if(param1)
         {
            if(this._okBtnSprite)
            {
               this._okBtnSprite.removeEventListener(MouseEvent.MOUSE_OVER,this.__okBtnOverHandler);
               this._okBtnSprite.removeEventListener(MouseEvent.MOUSE_OUT,this.__okBtnOutHandler);
               ObjectUtils.disposeObject(this._okBtnSprite);
               this._okBtnSprite = null;
            }
         }
         else if(!this._okBtnSprite)
         {
            this._okBtnSprite = new Sprite();
            this._okBtnSprite.addEventListener(MouseEvent.MOUSE_OVER,this.__okBtnOverHandler);
            this._okBtnSprite.addEventListener(MouseEvent.MOUSE_OUT,this.__okBtnOutHandler);
            this._okBtnSprite.graphics.beginFill(0,0);
            this._okBtnSprite.graphics.drawRect(0,0,this._okBtn.displayWidth,this._okBtn.displayHeight);
            this._okBtnSprite.graphics.endFill();
            this._okBtnSprite.x = this._okBtn.x - 1;
            this._okBtnSprite.y = this._okBtn.y + 3;
            addChild(this._okBtnSprite);
            this._okBtnTip = new OneLineTip();
            this._okBtnTip.tipData = LanguageMgr.GetTranslation("playerDress.okBtnTip");
            this._okBtnTip.visible = false;
         }
      }
      
      protected function __okBtnOverHandler(param1:MouseEvent) : void
      {
         this._okBtnTip.visible = true;
         LayerManager.Instance.addToLayer(this._okBtnTip,LayerManager.GAME_TOP_LAYER);
         var _loc2_:Point = this._okBtn.localToGlobal(new Point(0,0));
         this._okBtnTip.x = _loc2_.x - 64;
         this._okBtnTip.y = _loc2_.y + this._okBtn.height;
      }
      
      protected function __okBtnOutHandler(param1:MouseEvent) : void
      {
         this._okBtnTip.visible = false;
      }
      
      private function removeEvent() : void
      {
         this._self.Bag.removeEventListener(BagEvent.UPDATE,this.__updateGoods);
         this._hideHatBtn.removeEventListener(Event.SELECT,this.__hideHatChange);
         this._hideGlassBtn.removeEventListener(Event.SELECT,this.__hideGlassChange);
         this._hideSuitBtn.removeEventListener(Event.SELECT,this.__hideSuitesChange);
         this._hideWingBtn.removeEventListener(Event.SELECT,this.__hideWingClickHandler);
         this._selectedBox.listPanel.list.removeEventListener(ListItemEvent.LIST_ITEM_CLICK,this.__onListClick);
         this._saveBtn.removeEventListener(MouseEvent.CLICK,this.__saveBtnClick);
         this._okBtn.removeEventListener(MouseEvent.CLICK,this.__okBtnClick);
         this._helpBtn.removeEventListener(MouseEvent.CLICK,this.__helpBtnClick);
         if(this._okBtnSprite)
         {
            this._okBtnSprite.removeEventListener(MouseEvent.MOUSE_OVER,this.__okBtnOverHandler);
            this._okBtnSprite.removeEventListener(MouseEvent.MOUSE_OUT,this.__okBtnOutHandler);
         }
      }
      
      public function dispose() : void
      {
         this.removeEvent();
         ComponentSetting.COMBOX_LIST_LAYER = LayerManager.Instance.getLayerByType(LayerManager.STAGE_TOP_LAYER);
         NewHandContainer.Instance.clearArrowByID(ArrowType.DRESS_PUT_ON);
         ObjectUtils.disposeObject(this._BG);
         this._BG = null;
         ObjectUtils.disposeObject(this._hidePanel);
         this._hidePanel = null;
         ObjectUtils.disposeObject(this._saveBtn);
         this._saveBtn = null;
         ObjectUtils.disposeObject(this._okBtn);
         this._okBtn = null;
         ObjectUtils.disposeObject(this._okBtnSprite);
         this._okBtnSprite = null;
         ObjectUtils.disposeObject(this._okBtnTip);
         this._okBtnTip = null;
         ObjectUtils.disposeObject(this._selectedBox);
         this._selectedBox = null;
         ObjectUtils.disposeObject(this._descTxt);
         this._descTxt = null;
         ObjectUtils.disposeObject(this._hideGlassBtn);
         this._hideGlassBtn = null;
         ObjectUtils.disposeObject(this._hideHatBtn);
         this._hideHatBtn = null;
         ObjectUtils.disposeObject(this._hideSuitBtn);
         this._hideSuitBtn = null;
         ObjectUtils.disposeObject(this._hideWingBtn);
         this._hideWingBtn = null;
         ObjectUtils.disposeObject(this._character);
         this._character = null;
         ObjectUtils.disposeObject(this._helpBtn);
         this._helpBtn = null;
         var _loc1_:int = 0;
         while(_loc1_ <= CELLS_LENGTH - 1)
         {
            this._dressCells[_loc1_].removeEventListener(CellEvent.DOUBLE_CLICK,this.__cellDoubleClick);
            ObjectUtils.disposeObject(this._dressCells[_loc1_]);
            this._dressCells[_loc1_] = null;
            _loc1_++;
         }
      }
      
      public function get currentModel() : DressModel
      {
         return this._currentModel;
      }
   }
}
