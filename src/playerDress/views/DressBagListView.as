package playerDress.views
{
   import bagAndInfo.bag.BagListView;
   import bagAndInfo.cell.BagCell;
   import bagAndInfo.cell.BaseCell;
   import ddt.data.BagInfo;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.events.BagEvent;
   import ddt.manager.SocketManager;
   import flash.utils.ByteArray;
   import flash.utils.Dictionary;
   import playerDress.PlayerDressManager;
   import playerDress.components.DressUtils;
   
   public class DressBagListView extends BagListView
   {
      
      public static const DRESS_INDEX:int = 80;
       
      
      private var _dressType:int;
      
      private var _sex:int;
      
      private var _searchStr:String;
      
      private var _displayItems:Dictionary;
      
      private var _equipBag:BagInfo;
      
      private var _virtualBag:BagInfo;
      
      private var _currentPage:int = 1;
      
      public var locked:Boolean = false;
      
      public function DressBagListView(param1:int, param2:int = 7, param3:int = 49)
      {
         super(param1,param2,param3);
      }
      
      public function setSortType(param1:int, param2:Boolean, param3:String = "") : void
      {
         this._dressType = param1;
         this._sex = !!param2?int(1):int(2);
         this._searchStr = param3;
         this.sortItems();
      }
      
      override public function setData(param1:BagInfo) : void
      {
         if(this._equipBag != null)
         {
            this._equipBag.removeEventListener(BagEvent.UPDATE,this.__updateGoods);
         }
         this._equipBag = param1;
         this.setVirtualBagData();
         this._equipBag.addEventListener(BagEvent.UPDATE,this.__updateGoods);
      }
      
      private function setVirtualBagData() : void
      {
         var _loc2_:InventoryItemInfo = null;
         this._virtualBag = new BagInfo(BagInfo.EQUIPBAG,48);
         var _loc1_:int = 0;
         for each(_loc2_ in this._equipBag.items)
         {
            if(DressUtils.isDress(_loc2_))
            {
               this._virtualBag.items[_loc1_] = _loc2_;
               _loc1_++;
            }
         }
      }
      
      override protected function __updateGoods(param1:BagEvent) : void
      {
         if(!this.locked)
         {
            this.setVirtualBagData();
            this.sortItems();
         }
      }
      
      private function sortItems() : void
      {
         var _loc2_:* = null;
         var _loc3_:InventoryItemInfo = null;
         this._displayItems = new Dictionary();
         clearDataCells();
         var _loc1_:int = 0;
         this.sequenceItems();
         for(_loc2_ in this._virtualBag.items)
         {
            _loc3_ = this._virtualBag.items[_loc2_];
            if(_loc3_.NeedSex == this._sex || _loc3_.NeedSex == 0)
            {
               if(!PlayerDressManager.instance.showBind)
               {
                  if(_loc3_.IsBinds == true)
                  {
                     continue;
                  }
               }
               if(this._dressType == -1)
               {
                  if(this._searchStr != "" && _loc3_.Name.indexOf(this._searchStr) != -1)
                  {
                     this._displayItems[_loc1_] = _loc3_;
                     if(_loc1_ >= 0 && _loc1_ < _cellNum)
                     {
                        BaseCell(_cells[_loc1_]).info = _loc3_;
                     }
                     _loc1_++;
                  }
               }
               else if(this._dressType == 0)
               {
                  this._displayItems[_loc1_] = _loc3_;
                  if(_loc1_ >= 0 && _loc1_ < _cellNum)
                  {
                     BaseCell(_cells[_loc1_]).info = _loc3_;
                  }
                  _loc1_++;
               }
               else if(_loc3_.CategoryID == this._dressType)
               {
                  this._displayItems[_loc1_] = _loc3_;
                  if(_loc1_ >= 0 && _loc1_ < _cellNum)
                  {
                     BaseCell(_cells[_loc1_]).info = _loc3_;
                  }
                  _loc1_++;
               }
            }
         }
         this._currentPage = 1;
         (parent as DressBagView).currentPage = 1;
         (parent as DressBagView).updatePage();
      }
      
      private function sequenceItems() : void
      {
         var _loc4_:InventoryItemInfo = null;
         var _loc5_:ByteArray = null;
         var _loc8_:InventoryItemInfo = null;
         var _loc1_:BagInfo = new BagInfo(BagInfo.EQUIPBAG,48);
         var _loc2_:Array = [];
         var _loc3_:Array = [];
         for each(_loc4_ in this._virtualBag.items)
         {
            _loc2_.push({
               "TemplateID":_loc4_.TemplateID,
               "ItemID":_loc4_.ItemID,
               "CategoryIDSort":DressUtils.getBagGoodsCategoryIDSort(uint(_loc4_.CategoryID)),
               "Place":_loc4_.Place,
               "RemainDate":_loc4_.getRemainDate() > 0,
               "CanStrengthen":_loc4_.CanStrengthen,
               "StrengthenLevel":_loc4_.StrengthenLevel,
               "IsBinds":_loc4_.IsBinds
            });
         }
         _loc5_ = new ByteArray();
         _loc5_.writeObject(_loc2_);
         _loc5_.position = 0;
         _loc3_ = _loc5_.readObject() as Array;
         _loc2_.sortOn(["RemainDate","CategoryIDSort","TemplateID","CanStrengthen","IsBinds","StrengthenLevel","Place"],[Array.DESCENDING,Array.NUMERIC,Array.NUMERIC | Array.DESCENDING,Array.DESCENDING,Array.DESCENDING,Array.NUMERIC | Array.DESCENDING,Array.NUMERIC]);
         if(this.bagComparison(_loc2_,_loc3_))
         {
            return;
         }
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         while(_loc7_ <= _loc2_.length - 1)
         {
            for each(_loc8_ in this._virtualBag.items)
            {
               if(_loc2_[_loc7_].Place == _loc8_.Place)
               {
                  _loc1_.items[_loc6_] = _loc8_;
                  _loc6_++;
                  break;
               }
            }
            _loc7_++;
         }
         this._virtualBag = _loc1_;
      }
      
      private function bagComparison(param1:Array, param2:Array) : Boolean
      {
         if(param1.length < param2.length)
         {
            return false;
         }
         var _loc3_:int = param1.length;
         var _loc4_:int = 0;
         while(_loc4_ < _loc3_)
         {
            if(param1[_loc4_].ItemID != param2[_loc4_].ItemID || param1[_loc4_].TemplateID != param2[_loc4_].TemplateID)
            {
               return false;
            }
            _loc4_++;
         }
         return true;
      }
      
      public function foldItems() : void
      {
         var _loc3_:* = null;
         var _loc4_:InventoryItemInfo = null;
         var _loc5_:Boolean = false;
         var _loc6_:* = null;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc1_:Dictionary = new Dictionary();
         var _loc2_:Array = [];
         for(_loc3_ in this._virtualBag.items)
         {
            _loc4_ = this._virtualBag.items[_loc3_];
            if(DressUtils.isDress(_loc4_) && DressUtils.hasNoAddition(_loc4_))
            {
               _loc5_ = true;
               for(_loc6_ in _loc1_)
               {
                  if(_loc4_.TemplateID == parseInt(_loc6_))
                  {
                     _loc7_ = _loc1_[_loc6_];
                     _loc8_ = this._virtualBag.items[_loc7_].Place;
                     _loc2_.push({
                        "sPlace":_loc8_,
                        "tPlace":_loc4_.Place
                     });
                     _loc5_ = false;
                     break;
                  }
               }
               if(_loc5_)
               {
                  _loc1_[_loc4_.TemplateID] = _loc3_;
               }
            }
         }
         this._equipBag.isBatch = true;
         SocketManager.Instance.out.foldDressItem(_loc2_);
      }
      
      public function fillPage(param1:int) : void
      {
         var _loc2_:* = null;
         var _loc3_:int = 0;
         this._currentPage = param1;
         clearDataCells();
         for(_loc2_ in this._displayItems)
         {
            _loc3_ = parseInt(_loc2_) - (this._currentPage - 1) * _cellNum;
            if(_loc3_ >= 0 && _loc3_ < _cellNum)
            {
               BaseCell(_cells[_loc3_]).info = this._displayItems[_loc2_];
            }
         }
      }
      
      public function displayItemsLength() : int
      {
         var _loc2_:* = undefined;
         var _loc1_:int = 0;
         for each(_loc2_ in this._displayItems)
         {
            _loc1_++;
         }
         return _loc1_;
      }
      
      private function _cellsSort(param1:Array) : void
      {
         var _loc2_:int = 0;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc5_:int = 0;
         var _loc6_:BagCell = null;
         if(param1.length <= 0)
         {
            return;
         }
         _loc2_ = 0;
         while(_loc2_ < param1.length)
         {
            _loc3_ = param1[_loc2_].x;
            _loc4_ = param1[_loc2_].y;
            _loc5_ = _cellVec.indexOf(param1[_loc2_]);
            _loc6_ = _cellVec[_loc2_];
            param1[_loc2_].x = _loc6_.x;
            param1[_loc2_].y = _loc6_.y;
            _loc6_.x = _loc3_;
            _loc6_.y = _loc4_;
            _cellVec[_loc2_] = param1[_loc2_];
            _cellVec[_loc5_] = _loc6_;
            _loc2_++;
         }
      }
      
      public function unlockBag() : void
      {
         this.setVirtualBagData();
         this.sortItems();
         this.locked = false;
      }
      
      override public function dispose() : void
      {
         this.locked = false;
         if(this._equipBag)
         {
            this._equipBag.removeEventListener(BagEvent.UPDATE,this.__updateGoods);
         }
         super.dispose();
      }
   }
}
