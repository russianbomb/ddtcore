package playerDress.components
{
   import ddt.data.EquipType;
   import ddt.data.goods.InventoryItemInfo;
   
   public class DressUtils
   {
       
      
      public function DressUtils()
      {
         super();
      }
      
      public static function getBagItems(param1:int, param2:Boolean = false) : int
      {
         var _loc3_:Array = [0,2,4,11,1,3,5,13];
         if(!param2)
         {
            return _loc3_[param1] != null?int(_loc3_[param1]):int(-1);
         }
         return _loc3_.indexOf(param1);
      }
      
      public static function isDress(param1:InventoryItemInfo) : Boolean
      {
         switch(param1.CategoryID)
         {
            case EquipType.HEAD:
            case EquipType.GLASS:
            case EquipType.HAIR:
            case EquipType.EFF:
            case EquipType.CLOTH:
            case EquipType.FACE:
            case EquipType.SUITS:
            case EquipType.WING:
               return true;
            default:
               return false;
         }
      }
      
      public static function findItemPlace(param1:InventoryItemInfo) : int
      {
         switch(param1.CategoryID)
         {
            case EquipType.HEAD:
               return 0;
            case EquipType.GLASS:
               return 1;
            case EquipType.HAIR:
               return 2;
            case EquipType.EFF:
               return 3;
            case EquipType.CLOTH:
               return 4;
            case EquipType.FACE:
               return 5;
            case EquipType.SUITS:
               return 11;
            case EquipType.WING:
               return 13;
            default:
               return param1.Place;
         }
      }
      
      public static function hasNoAddition(param1:InventoryItemInfo) : Boolean
      {
         if(param1.isGold == false && param1.StrengthenLevel <= 0 && param1.AttackCompose <= 0 && param1.DefendCompose <= 0 && param1.AgilityCompose <= 0 && param1.LuckCompose <= 0 && param1.Hole5Level <= 0 && param1.Hole6Level <= 0 && param1.Hole1 <= 0 && param1.Hole2 <= 0 && param1.Hole3 <= 0 && param1.Hole4 <= 0 && param1.Hole5 <= 0 && param1.Hole6 <= 0 && param1.Hole5Exp <= 0 && param1.Hole6Exp <= 0 && param1.StrengthenExp <= 0 && param1.latentEnergyCurStr == "0,0,0,0")
         {
            return true;
         }
         return false;
      }
      
      public static function getBagGoodsCategoryIDSort(param1:uint) : int
      {
         var _loc2_:Array = [EquipType.ARM,EquipType.OFFHAND,EquipType.HEAD,EquipType.CLOTH,EquipType.ARMLET,EquipType.RING,EquipType.GLASS,EquipType.NECKLACE,EquipType.SUITS,EquipType.WING,EquipType.HAIR,EquipType.FACE,EquipType.EFF,EquipType.CHATBALL,EquipType.ATACCKT,EquipType.DEFENT,EquipType.ATTRIBUTE];
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_.length)
         {
            if(param1 == _loc2_[_loc3_])
            {
               return _loc3_;
            }
            _loc3_++;
         }
         return 9999;
      }
   }
}
