package playerDress
{
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.UIModuleLoader;
   import ddt.data.UIModuleTypes;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.view.UIModuleSmallLoading;
   import playerDress.components.DressModel;
   import playerDress.components.DressUtils;
   import playerDress.data.DressVo;
   import playerDress.data.PlayerDressEvent;
   import playerDress.views.DressBagView;
   import playerDress.views.PlayerDressView;
   import road7th.comm.PackageIn;
   import road7th.data.DictionaryData;
   
   public class PlayerDressManager
   {
      
      private static var _instance:PlayerDressManager;
       
      
      public var currentIndex:int;
      
      public var modelArr:Array;
      
      private var _funcArr:Array;
      
      private var _funcParamsArr:Array;
      
      private var _dressView:PlayerDressView;
      
      private var _dressBag:DressBagView;
      
      public var showBind:Boolean = true;
      
      public function PlayerDressManager()
      {
         this.modelArr = [];
         super();
         this._funcArr = [];
         this._funcParamsArr = [];
      }
      
      public static function get instance() : PlayerDressManager
      {
         if(!_instance)
         {
            _instance = new PlayerDressManager();
         }
         return _instance;
      }
      
      public function setup() : void
      {
         this.addEvents();
      }
      
      private function addEvents() : void
      {
         SocketManager.Instance.addEventListener(PlayerDressEvent.GET_DRESS_MODEL,this.setDressModelArr);
         SocketManager.Instance.addEventListener(PlayerDressEvent.CURRENT_MODEL,this.setCurrentModel);
         SocketManager.Instance.addEventListener(PlayerDressEvent.LOCK_DRESSBAG,this.unlockDressBag);
      }
      
      protected function setCurrentModel(param1:PlayerDressEvent) : void
      {
         var _loc3_:DictionaryData = null;
         var _loc4_:Array = null;
         var _loc5_:int = 0;
         var _loc6_:InventoryItemInfo = null;
         var _loc2_:PackageIn = param1.pkg;
         this.currentIndex = _loc2_.readInt();
         if(this.currentIndex == -1)
         {
            SocketManager.Instance.out.setCurrentModel(0);
            _loc3_ = PlayerManager.Instance.Self.Bag.items;
            _loc4_ = [];
            _loc5_ = 0;
            while(_loc5_ <= DressModel.DRESS_LEN - 1)
            {
               _loc6_ = _loc3_[DressUtils.getBagItems(_loc5_)];
               if(_loc6_)
               {
                  _loc4_.push(_loc6_.Place);
               }
               _loc5_++;
            }
            SocketManager.Instance.out.saveDressModel(0,_loc4_);
         }
      }
      
      protected function setDressModelArr(param1:PlayerDressEvent) : void
      {
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:Array = null;
         var _loc8_:int = 0;
         var _loc9_:DressVo = null;
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readInt();
         var _loc4_:int = 0;
         while(_loc4_ <= _loc3_ - 1)
         {
            _loc5_ = _loc2_.readInt();
            _loc6_ = _loc2_.readInt();
            _loc7_ = [];
            _loc8_ = 0;
            while(_loc8_ <= _loc6_ - 1)
            {
               _loc9_ = new DressVo();
               _loc9_.itemId = _loc2_.readInt();
               _loc9_.templateId = _loc2_.readInt();
               _loc7_.push(_loc9_);
               _loc8_++;
            }
            this.modelArr[_loc5_] = _loc7_;
            _loc4_++;
         }
         if(this.dressView)
         {
            this.dressView.setBtnsStatus();
         }
      }
      
      protected function unlockDressBag(param1:PlayerDressEvent) : void
      {
         if(this._dressBag)
         {
            this._dressBag.baglist.unlockBag();
         }
      }
      
      public function lockDressBag() : void
      {
         if(this._dressBag)
         {
            this._dressBag.baglist.locked = true;
         }
      }
      
      public function loadPlayerDressModule(param1:Function = null, param2:Array = null) : void
      {
         this._funcArr.push(param1);
         this._funcParamsArr.push(param2);
         UIModuleSmallLoading.Instance.progress = 0;
         UIModuleSmallLoading.Instance.show();
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadCompleteHandler);
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUimoduleLoadProgress);
         UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.PLAYER_DRESS);
      }
      
      private function onUimoduleLoadProgress(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.PLAYER_DRESS)
         {
            UIModuleSmallLoading.Instance.progress = param1.loader.progress * 100;
         }
      }
      
      private function loadCompleteHandler(param1:UIModuleEvent) : void
      {
         var _loc2_:int = 0;
         if(param1.module == UIModuleTypes.PLAYER_DRESS)
         {
            UIModuleSmallLoading.Instance.hide();
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadCompleteHandler);
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUimoduleLoadProgress);
            _loc2_ = 0;
            while(_loc2_ <= this._funcArr.length - 1)
            {
               (this._funcArr[_loc2_] as Function).apply(null,this._funcParamsArr[_loc2_]);
               _loc2_++;
            }
            this._funcArr = [];
            this._funcParamsArr = [];
         }
      }
      
      public function putOnDress(param1:InventoryItemInfo) : void
      {
         this._dressView.putOnDress(param1);
      }
      
      public function set dressView(param1:PlayerDressView) : void
      {
         this._dressView = param1;
      }
      
      public function set dressBag(param1:DressBagView) : void
      {
         this._dressBag = param1;
      }
      
      public function get dressView() : PlayerDressView
      {
         return this._dressView;
      }
      
      public function dispose() : void
      {
         this._dressView = null;
         this._dressBag = null;
      }
   }
}
