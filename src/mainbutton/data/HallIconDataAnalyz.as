package mainbutton.data
{
   import com.pickgliss.loader.DataAnalyzer;
   import com.pickgliss.utils.ObjectUtils;
   import flash.utils.Dictionary;
   import mainbutton.MainButton;
   
   public class HallIconDataAnalyz extends DataAnalyzer
   {
       
      
      public var _HallIconList:Dictionary;
      
      public function HallIconDataAnalyz(param1:Function)
      {
         super(param1);
      }
      
      override public function analyze(param1:*) : void
      {
         var _loc3_:XMLList = null;
         var _loc4_:int = 0;
         var _loc5_:MainButton = null;
         this._HallIconList = new Dictionary();
         var _loc2_:XML = new XML(param1);
         if(_loc2_.@value == "true")
         {
            _loc3_ = _loc2_..Item;
            _loc4_ = 0;
            while(_loc4_ < _loc3_.length())
            {
               _loc5_ = new MainButton();
               ObjectUtils.copyPorpertiesByXML(_loc5_,_loc3_[_loc4_]);
               this._HallIconList[_loc5_.ID] = _loc5_;
               _loc4_++;
            }
            this.addDiceIconData();
            onAnalyzeComplete();
         }
         else
         {
            message = _loc2_.@message;
            onAnalyzeError();
            onAnalyzeError();
         }
      }
      
      public function addDiceIconData() : void
      {
         var _loc1_:MainButton = new MainButton();
         _loc1_.ID = "8";
         _loc1_.IsShow = false;
         _loc1_.btnName = "diceBtn";
         _loc1_.btnMark = int(_loc1_.ID);
         _loc1_.btnCompleteVisable = 0;
         _loc1_.btnServerVisable = 0;
         this._HallIconList[_loc1_.ID] = _loc1_;
      }
      
      public function get list() : Dictionary
      {
         return this._HallIconList;
      }
   }
}
