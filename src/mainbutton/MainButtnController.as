package mainbutton
{
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import ddt.data.UIModuleTypes;
   import ddt.manager.PlayerManager;
   import ddt.view.UIModuleSmallLoading;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import mainbutton.data.MainButtonManager;
   
   public class MainButtnController extends EventDispatcher
   {
      
      private static var _instance:MainButtnController;
      
      public static var useFirst:Boolean = true;
      
      public static var loadComplete:Boolean = false;
      
      public static var ACTIVITIES:String = "1";
      
      public static var ROULETTE:String = "2";
      
      public static var VIP:String = "3";
      
      public static var SIGN:String = "5";
      
      public static var AWARD:String = "6";
      
      public static var ANGELBLESS:String = "7";
      
      public static var DICE:String = "8";
      
      public static var FIRSTRECHARGE:String = "8";
      
      public static var DDT_ACTIVITY:String = "ddtactivity";
      
      public static var DDT_AWARD:String = "ddtaward";
      
      public static var ICONCLOSE:String = "iconClose";
      
      public static var CLOSESIGN:String = "closeSign";
      
      public static var ICONOPEN:String = "iconOpen";
       
      
      public var btnList:Vector.<MainButton>;
      
      private var _currntType:String;
      
      private var _awardFrame:AwardFrame;
      
      private var _dailAwardState:Boolean;
      
      private var _vipAwardState:Boolean;
      
      public function MainButtnController()
      {
         super();
      }
      
      public static function get instance() : MainButtnController
      {
         if(!_instance)
         {
            _instance = new MainButtnController();
         }
         return _instance;
      }
      
      public function show(param1:String) : void
      {
         this._currntType = param1;
         if(loadComplete)
         {
            this.showFrame(this._currntType);
         }
         else if(useFirst)
         {
            UIModuleSmallLoading.Instance.progress = 0;
            UIModuleSmallLoading.Instance.show();
            UIModuleSmallLoading.Instance.addEventListener(Event.CLOSE,this.__onClose);
            UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.__progressShow);
            UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.__complainShow);
            UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.DDTMAINBTN);
            useFirst = false;
         }
      }
      
      private function showFrame(param1:String) : void
      {
         switch(param1)
         {
            case MainButtnController.DDT_AWARD:
               this._awardFrame = ComponentFactory.Instance.creatCustomObject("ddtmainbutton.AwardFrame");
               LayerManager.Instance.addToLayer(this._awardFrame,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
         }
      }
      
      private function __onClose(param1:Event) : void
      {
         UIModuleSmallLoading.Instance.hide();
         UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this.__onClose);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.__progressShow);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.__complainShow);
      }
      
      private function __progressShow(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.DDTMAINBTN)
         {
            UIModuleSmallLoading.Instance.progress = param1.loader.progress * 100;
         }
      }
      
      private function __complainShow(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.DDTMAINBTN)
         {
            UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this.__onClose);
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.__progressShow);
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.__complainShow);
            UIModuleSmallLoading.Instance.hide();
            loadComplete = true;
            this.showFrame(this._currntType);
         }
      }
      
      public function set DailyAwardState(param1:Boolean) : void
      {
         this._dailAwardState = param1;
      }
      
      public function get DailyAwardState() : Boolean
      {
         return this._dailAwardState;
      }
      
      public function set VipAwardState(param1:Boolean) : void
      {
         this._vipAwardState = param1;
      }
      
      public function get VipAwardState() : Boolean
      {
         return this._vipAwardState;
      }
      
      public function get BtnList() : Vector.<MainButton>
      {
         return this.btnList;
      }
      
      public function test() : Vector.<MainButton>
      {
         this.btnList = new Vector.<MainButton>();
         var _loc1_:MainButton = MainButtonManager.instance.getInfoByID(ACTIVITIES);
         var _loc2_:MainButton = MainButtonManager.instance.getInfoByID(ROULETTE);
         var _loc3_:MainButton = MainButtonManager.instance.getInfoByID(VIP);
         var _loc4_:MainButton = MainButtonManager.instance.getInfoByID(SIGN);
         var _loc5_:MainButton = MainButtonManager.instance.getInfoByID(AWARD);
         var _loc6_:MainButton = MainButtonManager.instance.getInfoByID(ANGELBLESS);
         var _loc7_:MainButton = MainButtonManager.instance.getInfoByID(DICE);
         if(_loc1_.IsShow)
         {
            _loc1_.btnMark = 1;
            _loc1_.btnServerVisable = 1;
            _loc1_.btnCompleteVisable = 1;
            this.btnList.push(_loc1_);
         }
         else
         {
            _loc1_.btnMark = 1;
            _loc1_.btnServerVisable = 2;
            _loc1_.btnCompleteVisable = 2;
         }
         if(_loc2_.IsShow)
         {
            _loc2_.btnMark = 2;
            _loc2_.btnServerVisable = 1;
            _loc2_.btnCompleteVisable = 1;
            this.btnList.push(_loc2_);
         }
         else
         {
            _loc2_.btnMark = 2;
            _loc2_.btnServerVisable = 2;
            _loc2_.btnCompleteVisable = 2;
         }
         if(_loc3_.IsShow)
         {
            _loc3_.btnMark = 3;
            _loc3_.btnServerVisable = 1;
            _loc3_.btnCompleteVisable = 1;
            this.btnList.push(_loc3_);
         }
         else
         {
            _loc3_.btnMark = 3;
            _loc3_.btnServerVisable = 2;
            _loc3_.btnCompleteVisable = 2;
         }
         if(_loc6_.IsShow)
         {
            _loc6_.btnMark = 7;
            _loc6_.btnServerVisable = 1;
            _loc6_.btnCompleteVisable = 1;
            this.btnList.push(_loc6_);
         }
         else
         {
            _loc6_.btnMark = 7;
            _loc6_.btnServerVisable = 2;
            _loc6_.btnCompleteVisable = 2;
         }
         _loc4_.btnMark = 5;
         _loc4_.btnServerVisable = 1;
         _loc4_.btnCompleteVisable = 1;
         this.btnList.push(_loc4_);
         this.btnList.push(_loc7_);
         if(PlayerManager.Instance.Self.IsVIP)
         {
            if(PlayerManager.Instance.Self.canTakeVipReward || this._dailAwardState)
            {
               _loc5_.btnMark = 6;
               _loc5_.btnServerVisable = 1;
               _loc5_.btnCompleteVisable = 1;
               this.btnList.push(_loc5_);
            }
            else
            {
               _loc5_.btnServerVisable = 2;
               _loc5_.btnCompleteVisable = 2;
            }
         }
         else if(this._dailAwardState)
         {
            _loc5_.btnMark = 6;
            _loc5_.btnServerVisable = 1;
            _loc5_.btnCompleteVisable = 1;
            this.btnList.push(_loc5_);
         }
         else
         {
            _loc5_.btnServerVisable = 2;
            _loc5_.btnCompleteVisable = 2;
         }
         return this.btnList;
      }
   }
}
