package firstRecharge
{
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.UIModuleTypes;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.ItemManager;
   import ddt.manager.SocketManager;
   import ddt.view.UIModuleSmallLoading;
   import firstRecharge.info.RechargeData;
   import firstRecharge.view.AccumulationView;
   import firstRecharge.view.FirstTouchView;
   import flash.events.Event;
   import road7th.comm.PackageIn;
   import wonderfulActivity.WonderfulActivityManager;
   
   public class FirstRechargeManger
   {
      
      private static var _instance:FirstRechargeManger;
       
      
      private var _firstTouchView:FirstTouchView;
      
      private var _accumulationView:AccumulationView;
      
      private var _isShowFirst:Boolean;
      
      private var _isOpen:Boolean;
      
      private var callback:Function;
      
      private var _isOver:Boolean;
      
      private var startTime:Date;
      
      private var endTime:Date;
      
      public var openFun:Function;
      
      public var endFun:Function;
      
      private var _goodsList:Vector.<RechargeData>;
      
      private var _goodsList_haiwai:Vector.<RechargeData>;
      
      private var _isopen:Boolean = false;
      
      public var rechargeMoneyTotal:int;
      
      public var rechargeGiftBagValue:int;
      
      public var rechargeEndTime:String;
      
      public function FirstRechargeManger()
      {
         super();
         this._goodsList = new Vector.<RechargeData>();
         this._goodsList_haiwai = new Vector.<RechargeData>();
      }
      
      public static function get Instance() : FirstRechargeManger
      {
         if(!_instance)
         {
            _instance = new FirstRechargeManger();
         }
         return _instance;
      }
      
      public function setup() : void
      {
         this.initEvent();
      }
      
      public function completeHander(param1:RechargeAnalyer) : void
      {
         this._goodsList = param1.goodsList;
         WonderfulActivityManager.Instance.updateChargeActiveTemplateXml();
      }
      
      public function get isopen() : Boolean
      {
         return this._isopen;
      }
      
      public function set ShowIcon(param1:Function) : void
      {
         this.callback = param1;
      }
      
      public function setGoods(param1:RechargeData) : InventoryItemInfo
      {
         var _loc2_:InventoryItemInfo = new InventoryItemInfo();
         _loc2_.TemplateID = param1.RewardItemID;
         _loc2_.StrengthenLevel = param1.StrengthenLevel;
         _loc2_.AgilityCompose = param1.AgilityCompose;
         _loc2_.AttackCompose = param1.AttackCompose;
         _loc2_.LuckCompose = param1.LuckCompose;
         _loc2_.DefendCompose = param1.DefendCompose;
         _loc2_ = ItemManager.fill(_loc2_);
         _loc2_.CanCompose = false;
         _loc2_.CanStrengthen = false;
         _loc2_.ValidDate = param1.RewardItemValid;
         _loc2_.BindType = 4;
         return _loc2_;
      }
      
      public function getGoodsList() : Vector.<RechargeData>
      {
         return this._goodsList;
      }
      
      public function getGoodsList_haiwai() : Vector.<RechargeData>
      {
         return this._goodsList_haiwai;
      }
      
      private function initEvent() : void
      {
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.CUMULATECHARGE_DATA,this.cumlatechargeData);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.CUMULATECHARGE_OPEN,this.cumlatechargeOpen);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.CUMULATECHARGE_OVER,this.cumlatechargeOver);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GET_SPREE,this.getSpree);
      }
      
      private function __firstRechargeOpen(param1:CrazyTankSocketEvent) : void
      {
         var _loc7_:RechargeData = null;
         this._goodsList_haiwai = new Vector.<RechargeData>();
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:Boolean = _loc2_.readBoolean();
         if(_loc3_)
         {
            this._isopen = true;
         }
         else
         {
            this._isopen = false;
         }
         if(this.callback != null)
         {
            this.callback(this._isopen);
         }
         this.rechargeMoneyTotal = _loc2_.readInt();
         this.rechargeGiftBagValue = _loc2_.readInt();
         var _loc4_:int = _loc2_.readInt();
         this.rechargeEndTime = _loc2_.readUTF();
         var _loc5_:int = _loc2_.readInt();
         var _loc6_:int = 0;
         while(_loc6_ < _loc5_)
         {
            _loc7_ = new RechargeData();
            _loc7_.RewardItemID = _loc2_.readInt();
            _loc7_.RewardItemCount = _loc2_.readInt();
            this._goodsList_haiwai.push(_loc7_);
            _loc6_++;
         }
         if(!this._isopen)
         {
            this._goodsList_haiwai = null;
            this.closeView();
         }
      }
      
      protected function getSpree(param1:CrazyTankSocketEvent) : void
      {
      }
      
      protected function cumlatechargeOver(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         this._isOver = _loc2_.readBoolean();
         this.endFun();
      }
      
      protected function cumlatechargeOpen(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         this._isOpen = _loc2_.readBoolean();
         if(this.openFun != null)
         {
            this.openFun();
         }
         if(this._isOpen)
         {
            this.closeView();
         }
      }
      
      private function closeView() : void
      {
         if(this._firstTouchView)
         {
            ObjectUtils.disposeObject(this._firstTouchView);
            this._firstTouchView = null;
         }
      }
      
      protected function cumlatechargeData(param1:CrazyTankSocketEvent) : void
      {
         var _loc5_:Object = null;
         var _loc6_:int = 0;
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readInt();
         var _loc4_:int = 0;
         while(_loc4_ < _loc3_)
         {
            _loc5_ = new Object();
            _loc5_.userId = _loc2_.readInt();
            _loc6_ = _loc2_.readInt();
            _loc5_.chargeMoney = _loc2_.readInt();
            _loc4_++;
         }
         if(_loc6_ == 1)
         {
            this._isShowFirst = true;
         }
         else
         {
            this._isShowFirst = false;
         }
      }
      
      public function showView() : void
      {
         this._isShowFirst = true;
         if(this._isShowFirst)
         {
            if(!this._firstTouchView)
            {
               UIModuleSmallLoading.Instance.progress = 0;
               UIModuleSmallLoading.Instance.show();
               UIModuleSmallLoading.Instance.addEventListener(Event.CLOSE,this.__onSmallLoadingClose);
               UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.gemstoneCompHander);
               UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.gemstoneProgress);
               UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.FRISTRECHARGE_SYS);
            }
            else
            {
               this._firstTouchView = ComponentFactory.Instance.creatComponentByStylename("firstrecharge.firstView");
               this._firstTouchView.setGoodsList(this._goodsList_haiwai);
               LayerManager.Instance.addToLayer(this._firstTouchView,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
            }
         }
         else if(!this._accumulationView)
         {
            UIModuleSmallLoading.Instance.progress = 0;
            UIModuleSmallLoading.Instance.show();
            UIModuleSmallLoading.Instance.addEventListener(Event.CLOSE,this.__onSmallLoadingClose);
            UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.gemstoneCompHander);
            UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.gemstoneProgress);
            UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.FRISTRECHARGE_SYS);
         }
         else
         {
            this._accumulationView = ComponentFactory.Instance.creatComponentByStylename("firstrecharge.accumulationView");
            LayerManager.Instance.addToLayer(this._accumulationView,LayerManager.GAME_DYNAMIC_LAYER,true);
         }
      }
      
      private function gemstoneProgress(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.FRISTRECHARGE_SYS)
         {
            UIModuleSmallLoading.Instance.progress = param1.loader.progress * 100;
         }
      }
      
      private function gemstoneCompHander(param1:UIModuleEvent) : void
      {
         if(param1.module != UIModuleTypes.FRISTRECHARGE_SYS)
         {
            return;
         }
         UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this.__onSmallLoadingClose);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.gemstoneCompHander);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.gemstoneProgress);
         UIModuleSmallLoading.Instance.hide();
         if(this._isShowFirst)
         {
            if(this._firstTouchView == null)
            {
               this._firstTouchView = ComponentFactory.Instance.creatComponentByStylename("firstrecharge.firstView");
               this._firstTouchView.setGoodsList(this._goodsList);
               LayerManager.Instance.addToLayer(this._firstTouchView,LayerManager.GAME_DYNAMIC_LAYER,true);
            }
         }
         else if(this._accumulationView == null)
         {
            this._accumulationView = ComponentFactory.Instance.creatComponentByStylename("firstrecharge.accumulationView");
            LayerManager.Instance.addToLayer(this._accumulationView,LayerManager.GAME_DYNAMIC_LAYER,true);
         }
      }
      
      public function get isOpen() : Boolean
      {
         return this._isOpen;
      }
      
      public function get isOver() : Boolean
      {
         return this._isOver;
      }
      
      public function get isShowFirst() : Boolean
      {
         return this._isShowFirst;
      }
      
      private function __onSmallLoadingClose(param1:Event) : void
      {
         UIModuleSmallLoading.Instance.hide();
         UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this.__onSmallLoadingClose);
      }
   }
}
