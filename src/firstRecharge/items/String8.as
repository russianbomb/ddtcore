package firstRecharge.items
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.Sprite;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   
   public class String8 extends Sprite
   {
       
      
      private var _bitMapData:BitmapData;
      
      private var _list:Vector.<BitmapData>;
      
      public function String8()
      {
         var _loc4_:Rectangle = null;
         var _loc5_:Point = null;
         var _loc6_:BitmapData = null;
         super();
         this._list = new Vector.<BitmapData>();
         x = 40;
         y = 239;
         rotation = -20;
         this._bitMapData = ComponentFactory.Instance.creatBitmapData("fristRecharge.str8");
         var _loc1_:int = this._bitMapData.width / 10;
         var _loc2_:int = this._bitMapData.height;
         var _loc3_:int = 0;
         while(_loc3_ < 10)
         {
            _loc4_ = new Rectangle(_loc1_ * _loc3_,0,_loc1_,_loc2_);
            _loc5_ = new Point(0,0);
            _loc6_ = new BitmapData(_loc1_,_loc2_);
            _loc6_.copyPixels(this._bitMapData,_loc4_,_loc5_);
            this._list.push(_loc6_);
            _loc3_++;
         }
      }
      
      public function setNum(param1:String) : void
      {
         var _loc4_:int = 0;
         var _loc5_:BitmapData = null;
         var _loc6_:Bitmap = null;
         this.clear();
         var _loc2_:int = param1.length;
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_)
         {
            _loc4_ = int(param1.charAt(_loc3_));
            _loc5_ = this._list[_loc4_].clone();
            _loc6_ = new Bitmap(_loc5_);
            _loc6_.x = _loc3_ * (_loc6_.width - 8);
            _loc6_.smoothing = true;
            addChild(_loc6_);
            _loc3_++;
         }
      }
      
      public function clear() : void
      {
         while(numChildren)
         {
            ObjectUtils.disposeObject(getChildAt(0));
         }
      }
   }
}
