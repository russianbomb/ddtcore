package firstRecharge.view
{
   import bagAndInfo.cell.BagCell;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.image.MutipleImage;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.LeavePageManager;
   import ddt.manager.SoundManager;
   import firstRecharge.FirstRechargeManger;
   import firstRecharge.info.RechargeData;
   import flash.display.Bitmap;
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   
   public class FirstTouchView extends Frame implements Disposeable
   {
       
      
      private var _treeImage:ScaleBitmapImage;
      
      private var _treeImage2:ScaleBitmapImage;
      
      private var _word:Bitmap;
      
      private var _worldtitle:Bitmap;
      
      private var _child:Bitmap;
      
      private var _back:MutipleImage;
      
      private var _btn:SimpleBitmapButton;
      
      private var _goodsContentList:Vector.<BagCell>;
      
      private var _line:Bitmap;
      
      private var _cartoonList:Vector.<MovieClip>;
      
      public function FirstTouchView()
      {
         super();
         this.initView();
         this.initEvent();
      }
      
      private function initEvent() : void
      {
         addEventListener(FrameEvent.RESPONSE,this._response);
      }
      
      private function _response(param1:FrameEvent) : void
      {
         if(param1.responseCode == FrameEvent.CLOSE_CLICK || param1.responseCode == FrameEvent.ESC_CLICK)
         {
            SoundManager.instance.play("008");
            ObjectUtils.disposeObject(this);
         }
      }
      
      private function initView() : void
      {
         var _loc2_:int = 0;
         var _loc3_:BagCell = null;
         var _loc4_:ScaleBitmapImage = null;
         var _loc5_:int = 0;
         var _loc6_:MovieClip = null;
         var _loc7_:MovieClip = null;
         this._goodsContentList = new Vector.<BagCell>();
         this._treeImage = ComponentFactory.Instance.creatComponentByStylename("fristRecharge.scale9cornerImageTree");
         addToContent(this._treeImage);
         this._treeImage2 = ComponentFactory.Instance.creatComponentByStylename("accumulationView.rightBottomBgImg");
         addToContent(this._treeImage2);
         this._back = ComponentFactory.Instance.creatComponentByStylename("firstRecharge.SignedAwardBack");
         addToContent(this._back);
         this._word = ComponentFactory.Instance.creatBitmap("fristRecharge.word");
         addToContent(this._word);
         this._worldtitle = ComponentFactory.Instance.creatBitmap("fristRecharge.libao.title");
         addToContent(this._worldtitle);
         titleText = "第一次亲密接触";
         this._cartoonList = new Vector.<MovieClip>(2);
         var _loc1_:int = 0;
         _loc2_ = 0;
         while(_loc2_ < 7)
         {
            _loc3_ = new BagCell(_loc2_);
            _loc4_ = ComponentFactory.Instance.creatComponentByStylename("recharge.ItemBg");
            _loc4_.x = _loc2_ * (_loc3_.width + 8) + 60;
            _loc4_.y = 254;
            _loc3_.x = _loc4_.x + 2;
            _loc3_.y = _loc4_.y + 2;
            addToContent(_loc4_);
            addToContent(_loc3_);
            _loc3_.setBgVisible(false);
            this._goodsContentList.push(_loc3_);
            _loc5_ = FirstRechargeManger.Instance.getGoodsList_haiwai().length;
            if(_loc5_ > 1)
            {
               if(_loc2_ == 0 || _loc2_ == 1)
               {
                  _loc6_ = ComponentFactory.Instance.creat("firstRecharge.cartoon");
                  _loc6_.mouseChildren = false;
                  _loc6_.mouseEnabled = false;
                  _loc6_.x = _loc4_.x - 21;
                  _loc6_.y = _loc4_.y - 36;
                  addToContent(_loc6_);
                  this._cartoonList[_loc2_] = _loc6_;
               }
            }
            else if(_loc2_ == 0)
            {
               _loc7_ = ComponentFactory.Instance.creat("firstRecharge.cartoon");
               _loc7_.mouseChildren = false;
               _loc7_.mouseEnabled = false;
               _loc7_.x = _loc4_.x - 21;
               _loc7_.y = _loc4_.y - 36;
               addToContent(_loc7_);
               this._cartoonList[_loc2_] = _loc7_;
            }
            _loc2_++;
         }
         this._child = ComponentFactory.Instance.creatBitmap("fristRecharge.child");
         addToContent(this._child);
         this._btn = ComponentFactory.Instance.creatComponentByStylename("accumulationView.ftxtBtn");
         addToContent(this._btn);
         this._btn.addEventListener(MouseEvent.CLICK,this.clickHander);
      }
      
      public function setGoodsList(param1:Vector.<RechargeData>) : void
      {
         var _loc2_:int = param1.length;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         while(_loc4_ < _loc2_)
         {
            if(_loc3_ >= 7)
            {
               return;
            }
            if(param1[_loc4_] && param1[_loc4_].RewardID < 2000)
            {
               this._goodsContentList[_loc4_].info = FirstRechargeManger.Instance.setGoods(param1[_loc4_]);
               this._goodsContentList[_loc4_].setCount(param1[_loc4_].RewardItemCount);
            }
            _loc3_++;
            _loc4_++;
         }
      }
      
      protected function clickHander(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         LeavePageManager.leaveToFillPath();
      }
      
      private function changeStringToDate(param1:String) : Date
      {
         var _loc2_:Array = null;
         var _loc3_:Array = null;
         var _loc4_:Array = null;
         var _loc5_:Date = null;
         if(param1 == "" || param1 == null)
         {
            return null;
         }
         _loc2_ = param1.split(" ");
         _loc3_ = _loc2_[0].split("-");
         _loc4_ = _loc2_[1].split(":");
         _loc5_ = new Date(_loc3_[0],_loc3_[1] - 1,_loc3_[2],_loc4_[0],_loc4_[1],_loc4_[2]);
         return _loc5_;
      }
      
      override public function dispose() : void
      {
         var _loc1_:MovieClip = null;
         removeEventListener(FrameEvent.RESPONSE,this._response);
         if(this._btn)
         {
            this._btn.removeEventListener(MouseEvent.CLICK,this.clickHander);
         }
         for each(_loc1_ in this._cartoonList)
         {
            _loc1_.gotoAndStop(1);
         }
         this._cartoonList = null;
         super.dispose();
      }
   }
}
