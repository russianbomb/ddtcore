package firstRecharge.view
{
   import bagAndInfo.cell.BagCell;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.image.Scale9CornerImage;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.LeavePageManager;
   import ddt.manager.SoundManager;
   import firstRecharge.items.FTextButton;
   import firstRecharge.items.PicItem;
   import firstRecharge.items.String8;
   import flash.display.Bitmap;
   import flash.events.MouseEvent;
   
   public class AccumulationView extends Frame implements Disposeable
   {
       
      
      private var _treeImage:ScaleBitmapImage;
      
      private var _treeImage2:Scale9CornerImage;
      
      private var _pricePic:Bitmap;
      
      private var _picBack:Bitmap;
      
      private var _barBack:Bitmap;
      
      private var _libaoTxt:Bitmap;
      
      private var _daojishiTxt:Bitmap;
      
      private var _itemList:Vector.<PicItem>;
      
      private var _goodsContentList:Vector.<BagCell>;
      
      private var _selcetedBitMap:Bitmap;
      
      private var _iconStrList:Array;
      
      private var _iconTxtStrList:Array;
      
      private var _txt1:FilterFrameText;
      
      private var _txt2:FilterFrameText;
      
      private var _txt3:FilterFrameText;
      
      private var _txt4:FilterFrameText;
      
      private var _txt5:FilterFrameText;
      
      private var _txt6:FilterFrameText;
      
      private var _txt7:FilterFrameText;
      
      private var _btn:FTextButton;
      
      private var _fengeLine:Bitmap;
      
      private var _goldLine:Bitmap;
      
      private var str8:String8;
      
      public function AccumulationView()
      {
         this._iconStrList = ["fristRecharge.level1","fristRecharge.level2","fristRecharge.level3","fristRecharge.level4","fristRecharge.level5","fristRecharge.level6"];
         this._iconTxtStrList = ["充值500点券","充值1000点券","充值2000点券","充值5000点券","充值10000点券","充值50000点券"];
         super();
         this.initView();
         this.initEvent();
      }
      
      private function initEvent() : void
      {
         addEventListener(FrameEvent.RESPONSE,this._response);
         addEventListener(MouseEvent.CLICK,this.mouseClickHander);
      }
      
      private function uinitEvent() : void
      {
         removeEventListener(FrameEvent.RESPONSE,this._response);
         removeEventListener(MouseEvent.CLICK,this.mouseClickHander);
         this._btn.removeEventListener(MouseEvent.CLICK,this.clickHander);
      }
      
      protected function _response(param1:FrameEvent) : void
      {
         if(param1.responseCode == FrameEvent.CLOSE_CLICK || param1.responseCode == FrameEvent.ESC_CLICK)
         {
            ObjectUtils.disposeObject(this);
         }
      }
      
      private function initView() : void
      {
         var _loc1_:int = 0;
         var _loc3_:PicItem = null;
         var _loc4_:BagCell = null;
         this._itemList = new Vector.<PicItem>();
         this._goodsContentList = new Vector.<BagCell>();
         this._treeImage = ComponentFactory.Instance.creatComponentByStylename("accumulationView.scale9cornerImageTree");
         addToContent(this._treeImage);
         this._treeImage2 = ComponentFactory.Instance.creatComponentByStylename("accumulationView.scale9cornerImageTree2");
         addToContent(this._treeImage2);
         this._picBack = ComponentFactory.Instance.creatBitmap("fristRecharge.pic.back");
         addToContent(this._picBack);
         this._barBack = ComponentFactory.Instance.creatBitmap("fristRecharge.bar.back");
         addToContent(this._barBack);
         this._libaoTxt = ComponentFactory.Instance.creatBitmap("fristRecharge.libao.giftList");
         addToContent(this._libaoTxt);
         this._pricePic = ComponentFactory.Instance.creatBitmap("fristRecharge.libao.price");
         addToContent(this._pricePic);
         this._daojishiTxt = ComponentFactory.Instance.creatBitmap("fristRecharge.downtimes");
         addToContent(this._daojishiTxt);
         this._txt2 = ComponentFactory.Instance.creatComponentByStylename("firstrecharge.txt2");
         this._txt2.text = "还要充值                 点券，您就能领取价值              点卷的礼包了！";
         addToContent(this._txt2);
         this._txt4 = ComponentFactory.Instance.creatComponentByStylename("firstrecharge.txt4");
         this._txt4.text = "倒计时天数";
         addToContent(this._txt4);
         this._txt6 = ComponentFactory.Instance.creatComponentByStylename("firstrecharge.txt6");
         this._txt6.text = "500";
         addToContent(this._txt6);
         this._txt7 = ComponentFactory.Instance.creatComponentByStylename("firstrecharge.txt7");
         this._txt7.text = "8888";
         addToContent(this._txt7);
         this.str8 = new String8();
         this.str8.setNum("1234");
         addToContent(this.str8);
         titleText = "充值奖励";
         _loc1_ = 0;
         while(_loc1_ < 6)
         {
            _loc3_ = new PicItem();
            if(_loc1_ == 0)
            {
               _loc3_.x = 25;
            }
            else
            {
               _loc3_.x = _loc1_ * (_loc3_.width + 1) + 40;
            }
            _loc3_.y = 57;
            _loc3_.id = _loc1_;
            _loc3_.setTxtStr(this._iconTxtStrList[_loc1_]);
            _loc3_.addIcon(this._iconStrList[_loc1_]);
            addToContent(_loc3_);
            this._itemList.push(_loc3_);
            _loc1_++;
         }
         var _loc2_:int = 0;
         while(_loc2_ < 8)
         {
            _loc4_ = new BagCell(_loc2_);
            _loc4_.x = _loc2_ * (_loc4_.width + 8) + 160;
            _loc4_.y = 233;
            addToContent(_loc4_);
            this._goodsContentList.push(_loc4_);
            _loc2_++;
         }
         this._selcetedBitMap = ComponentFactory.Instance.creatBitmap("fristRecharge.selected");
         addToContent(this._selcetedBitMap);
         this._btn = new FTextButton("accumulationView.ftxtBtn","firstrecharge.txt5");
         this._btn.x = 300;
         this._btn.y = 390;
         this._btn.setTxt("前去充值");
         addToContent(this._btn);
         this._btn.addEventListener(MouseEvent.CLICK,this.clickHander);
         this._fengeLine = ComponentFactory.Instance.creat("fristRecharge.libao.fengeLine");
         addToContent(this._fengeLine);
         this._goldLine = ComponentFactory.Instance.creat("fristRecharge.libao.gold");
         addToContent(this._goldLine);
      }
      
      protected function clickHander(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         LeavePageManager.leaveToFillPath();
      }
      
      protected function mouseClickHander(param1:MouseEvent) : void
      {
         if(param1.target is PicItem)
         {
            switch(param1.target.id)
            {
               case 0:
                  this.str8.setNum("288");
                  break;
               case 1:
                  this.str8.setNum("1788");
                  break;
               case 2:
                  this.str8.setNum("2298");
                  break;
               case 3:
                  this.str8.setNum("5268");
                  break;
               case 4:
                  this.str8.setNum("7288");
                  break;
               case 5:
                  this.str8.setNum("8888");
            }
            this._selcetedBitMap.x = param1.target.x - 2;
            this._selcetedBitMap.y = param1.target.y - 4;
         }
      }
      
      override public function dispose() : void
      {
         this.uinitEvent();
         while(numChildren)
         {
            ObjectUtils.disposeObject(getChildAt(0));
         }
      }
   }
}
