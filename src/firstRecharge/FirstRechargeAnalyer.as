package firstRecharge
{
   import com.pickgliss.loader.DataAnalyzer;
   import com.pickgliss.utils.ObjectUtils;
   import firstRecharge.info.RechargeData;
   
   public class FirstRechargeAnalyer extends DataAnalyzer
   {
       
      
      public var goodsList:Vector.<RechargeData>;
      
      public function FirstRechargeAnalyer(param1:Function)
      {
         super(param1);
      }
      
      override public function analyze(param1:*) : void
      {
         var _loc2_:XML = new XML(param1);
         var _loc3_:int = _loc2_.item.length();
         var _loc4_:XMLList = _loc2_..item;
         var _loc5_:int = 0;
         while(_loc5_ < _loc3_)
         {
            param1 = new RechargeData();
            ObjectUtils.copyPorpertiesByXML(param1,_loc4_[_loc5_]);
            this.goodsList.push(param1);
            _loc5_++;
         }
      }
   }
}
