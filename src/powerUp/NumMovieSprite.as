package powerUp
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   
   public class NumMovieSprite extends Sprite implements Disposeable
   {
       
      
      private var _powerNum:int;
      
      private var _powerString:String;
      
      private var _addPowerNum:int;
      
      private var _allPowerNum:int;
      
      private var _allPowerString:String;
      
      private var _iconArr:Array;
      
      private var _iconArr2:Array;
      
      private var _moveNumArr:Array;
      
      private var _frame:int;
      
      public function NumMovieSprite(param1:int, param2:int)
      {
         super();
         this._powerNum = param1;
         this._addPowerNum = param2;
         this._allPowerNum = this._powerNum + this._addPowerNum;
         this._powerString = String(this._powerNum);
         this._allPowerString = String(this._allPowerNum);
         this._iconArr = new Array();
         this._iconArr2 = new Array();
         this._moveNumArr = new Array();
         this.initView();
         addEventListener(Event.ENTER_FRAME,this.__updateNumHandler);
      }
      
      protected function __updateNumHandler(param1:Event) : void
      {
         var _loc2_:int = 0;
         var _loc3_:String = null;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:MovieClip = null;
         this._frame++;
         if(this._frame <= 10)
         {
            return;
         }
         if(this._frame > 10 && this._frame < 20)
         {
            _loc3_ = this._allPowerString.substr(this._allPowerString.length - this._powerString.length,this._powerString.length);
            _loc4_ = this._powerString.length - 1;
            while(_loc4_ >= 0)
            {
               _loc6_ = int(_loc3_.charAt(_loc4_));
               _loc7_ = int(this._powerString.charAt(_loc4_));
               if(_loc6_ < _loc7_)
               {
                  _loc6_ = _loc6_ + 10;
               }
               _loc2_ = _loc6_ - _loc7_;
               this._moveNumArr[_loc4_] = _loc2_;
               _loc4_--;
            }
            _loc5_ = 0;
            while(_loc5_ < this._iconArr.length)
            {
               if(this._moveNumArr[_loc5_] > 0 && this._moveNumArr[_loc5_] >= this._frame - 10)
               {
                  (this._iconArr[_loc5_] as MovieClip).gotoAndStop(this._frame + 1 - 10);
               }
               else
               {
                  (this._iconArr[_loc5_] as MovieClip).stop();
               }
               _loc5_++;
            }
         }
         else
         {
            for each(_loc8_ in this._iconArr2)
            {
               addChild(_loc8_);
            }
            removeEventListener(Event.ENTER_FRAME,this.__updateNumHandler);
         }
      }
      
      private function initView() : void
      {
         var _loc4_:MovieClip = null;
         var _loc5_:MovieClip = null;
         var _loc1_:int = this._allPowerString.length - this._powerString.length;
         var _loc2_:int = 0;
         while(_loc2_ < this._powerString.length)
         {
            _loc4_ = ComponentFactory.Instance.creat("num" + this._powerString.charAt(_loc2_));
            _loc4_.x = 210 + _loc4_.width / 1.6 * _loc1_ + _loc4_.width / 1.6 * _loc2_;
            _loc4_.y = -28;
            addChild(_loc4_);
            _loc4_.stop();
            this._iconArr.push(_loc4_);
            this._moveNumArr.push(0);
            _loc2_++;
         }
         var _loc3_:int = 0;
         while(_loc3_ < _loc1_)
         {
            _loc5_ = ComponentFactory.Instance.creat("num" + this._allPowerString.charAt(_loc3_));
            _loc5_.x = 210 + _loc5_.width / 1.6 * _loc1_ - _loc5_.width / 1.6 * (_loc1_ - _loc3_);
            _loc5_.y = -28;
            _loc5_.stop();
            this._iconArr2.push(_loc5_);
            _loc3_++;
         }
      }
      
      public function dispose() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ < this._iconArr.length)
         {
            if(this._iconArr[_loc1_])
            {
               ObjectUtils.disposeObject(this._iconArr[_loc1_]);
               this._iconArr[_loc1_] = null;
            }
            _loc1_++;
         }
         this._iconArr = null;
         var _loc2_:int = 0;
         while(_loc2_ < this._iconArr2.length)
         {
            if(this._iconArr2[_loc2_])
            {
               ObjectUtils.disposeObject(this._iconArr2[_loc2_]);
               this._iconArr2[_loc2_] = null;
            }
            _loc2_++;
         }
         this._iconArr2 = null;
         this._moveNumArr = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
