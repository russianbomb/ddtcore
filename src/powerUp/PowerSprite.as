package powerUp
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   
   public class PowerSprite extends Sprite implements Disposeable
   {
       
      
      private var _powerBgMc:MovieClip;
      
      private var _greenAddIcon:MovieClip;
      
      private var _lineMc1:MovieClip;
      
      private var _frameNum:int;
      
      private var _addPowerNum:int;
      
      private var _powerNum:int;
      
      private var _numMovieSprite:NumMovieSprite;
      
      private var _greenIconArr:Array;
      
      public function PowerSprite(param1:int, param2:int)
      {
         super();
         if(param1 < 0)
         {
            param1 = 0;
         }
         if(param2 < 0)
         {
            param2 = 0;
         }
         this._powerNum = param1;
         this._addPowerNum = param2;
         this._greenIconArr = new Array();
         this.initView();
         this.initEvent();
      }
      
      private function initView() : void
      {
         var _loc3_:MovieClip = null;
         this._powerBgMc = ComponentFactory.Instance.creat("powerBg");
         this._powerBgMc.x = 30;
         this._powerBgMc.y = 200;
         addChild(this._powerBgMc);
         this._lineMc1 = ComponentFactory.Instance.creat("powerLine");
         this._greenAddIcon = ComponentFactory.Instance.creat("greenAddIcon");
         var _loc1_:String = String(this._addPowerNum);
         this._greenIconArr.push(this._greenAddIcon);
         this._greenIconArr[0].x = 270;
         this._greenIconArr[0].y = -23;
         this._greenIconArr[0].alpha = 0;
         this._powerBgMc.addChild(this._greenIconArr[0]);
         var _loc2_:int = 0;
         while(_loc2_ < _loc1_.length)
         {
            _loc3_ = ComponentFactory.Instance.creat("greenNum" + _loc1_.charAt(_loc2_));
            _loc3_.x = 290 + _loc3_.width / 1.1 * _loc2_;
            _loc3_.y = -28;
            _loc3_.alpha = 0;
            this._powerBgMc.addChild(_loc3_);
            this._greenIconArr.push(_loc3_);
            _loc2_++;
         }
      }
      
      private function initEvent() : void
      {
         addEventListener(Event.ENTER_FRAME,this.__updatePowerMcHandler);
      }
      
      private function removeEvent() : void
      {
         removeEventListener(Event.ENTER_FRAME,this.__updatePowerMcHandler);
      }
      
      protected function __updatePowerMcHandler(param1:Event) : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         if(this._frameNum < 41)
         {
            this._frameNum++;
            if(this._frameNum > 5 && this._frameNum < 25)
            {
               if(this._frameNum == 15)
               {
                  this._numMovieSprite = new NumMovieSprite(this._powerNum,this._addPowerNum);
                  this._powerBgMc.addChild(this._numMovieSprite);
               }
               _loc2_ = 0;
               while(_loc2_ < this._greenIconArr.length)
               {
                  (this._greenIconArr[_loc2_] as MovieClip).y = (this._greenIconArr[_loc2_] as MovieClip).y - 4;
                  (this._greenIconArr[_loc2_] as MovieClip).alpha = (this._greenIconArr[_loc2_] as MovieClip).alpha + 0.05;
                  _loc2_++;
               }
            }
            else if(this._frameNum > 26 && this._frameNum < 37)
            {
               _loc3_ = 0;
               while(_loc3_ < this._greenIconArr.length)
               {
                  (this._greenIconArr[_loc3_] as MovieClip).y = (this._greenIconArr[_loc3_] as MovieClip).y - 3;
                  (this._greenIconArr[_loc3_] as MovieClip).alpha = (this._greenIconArr[_loc3_] as MovieClip).alpha - 0.1;
                  _loc3_++;
               }
            }
            else if(this._frameNum == 40)
            {
               this._lineMc1.x = this._powerBgMc.x - 20;
               this._lineMc1.y = this._powerBgMc.y - 73;
               addChild(this._lineMc1);
            }
         }
         else
         {
            dispatchEvent(new Event(PowerUpMovieManager.POWER_UP_MOVIE_OVER));
            this.removeEvent();
         }
      }
      
      public function dispose() : void
      {
         ObjectUtils.disposeObject(this._powerBgMc);
         this._powerBgMc = null;
         ObjectUtils.disposeObject(this._lineMc1);
         this._lineMc1 = null;
         var _loc1_:int = 0;
         while(_loc1_ < this._greenIconArr.length)
         {
            if(this._greenIconArr[_loc1_])
            {
               ObjectUtils.disposeObject(this._greenIconArr[_loc1_]);
               this._greenIconArr[_loc1_] = null;
            }
            _loc1_++;
         }
         this._greenIconArr = null;
         ObjectUtils.disposeObject(this._numMovieSprite);
         this._numMovieSprite = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
