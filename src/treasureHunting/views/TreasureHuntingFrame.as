package treasureHunting.views
{
   import bagAndInfo.cell.BaseCell;
   import baglocked.BaglockedManager;
   import com.greensock.TimelineLite;
   import com.greensock.TweenLite;
   import com.greensock.TweenProxy;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.controls.container.SimpleTileList;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.DisplayUtils;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.events.PlayerPropertyEvent;
   import ddt.manager.ChatManager;
   import ddt.manager.ItemManager;
   import ddt.manager.LanguageMgr;
   import ddt.manager.LeavePageManager;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.RouletteManager;
   import ddt.manager.SharedManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import ddt.view.caddyII.CaddyEvent;
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.DisplayObject;
   import flash.display.MovieClip;
   import flash.display.Shape;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.events.TimerEvent;
   import flash.geom.Point;
   import flash.utils.Timer;
   import treasureHunting.TreasureEvent;
   import treasureHunting.TreasureManager;
   import treasureHunting.items.TreasureItem;
   
   public class TreasureHuntingFrame extends Frame
   {
      
      private static const RESTRICT:String = "0-9";
      
      private static const DEFAULT:String = "1";
      
      private static const LENGTH:int = 16;
      
      private static const NUMBER:int = 4;
       
      
      private var _content:Sprite;
      
      private var _bg:ScaleBitmapImage;
      
      private var _treasureTitle:ScaleBitmapImage;
      
      private var _itemList:SimpleTileList;
      
      private var _itemArr:Vector.<TreasureItem>;
      
      private var _timesTxt:ScaleBitmapImage;
      
      private var _timesInput:FilterFrameText;
      
      private var _showPrizeBtn:BaseButton;
      
      private var _rankPrizeBtn:SimpleBitmapButton;
      
      private var _huntingBtn:BaseButton;
      
      private var _maxBtn:BaseButton;
      
      private var _glint:MovieClip;
      
      private var _lastTimeTxt:FilterFrameText;
      
      private var _myNumberTxt:FilterFrameText;
      
      private var _bagBtn:SimpleBitmapButton;
      
      private var _rankBtn:SimpleBitmapButton;
      
      private var _luckyStoneBG:Bitmap;
      
      private var _bagView:TreasureBagView;
      
      private var _rankView:TreasureRankView;
      
      private var _glintTimer:Timer;
      
      private var _rankTimer:Timer;
      
      private var _ran:int;
      
      private var _unitPrice:int;
      
      private var isClickMax:Boolean = false;
      
      private var moveCell:BaseCell;
      
      private var luckStoneCell:BaseCell;
      
      public function TreasureHuntingFrame()
      {
         super();
         this.initView();
         this.initEvent();
         this.initData();
      }
      
      private function initData() : void
      {
         SocketManager.Instance.out.sendQequestBadLuck();
         TreasureManager.instance.setFrame(this);
         this._glintTimer = new Timer(300,10);
         this._glintTimer.addEventListener(TimerEvent.TIMER,this.onTimer);
         this._glintTimer.addEventListener(TimerEvent.TIMER_COMPLETE,this.onTimerComplete);
         this._rankTimer = new Timer(5 * 60 * 1000);
         this._rankTimer.addEventListener(TimerEvent.TIMER,this.updateRank);
         this._unitPrice = TreasureManager.instance.needMoney;
      }
      
      protected function updateRank(param1:TimerEvent) : void
      {
         SocketManager.Instance.out.sendQequestBadLuck();
      }
      
      private function initView() : void
      {
         var _loc2_:TreasureItem = null;
         this._content = new Sprite();
         PositionUtils.setPos(this._content,"treasureHunting.Treasure.ContentPos2");
         this._bg = ComponentFactory.Instance.creatComponentByStylename("treasureHunting.Treasure.BG");
         this._content.addChild(this._bg);
         this._treasureTitle = ComponentFactory.Instance.creatComponentByStylename("treasureHunting.Treasure.Titile");
         this._content.addChild(this._treasureTitle);
         this._itemList = ComponentFactory.Instance.creatCustomObject("treasureHunting.Treasure.SimpleTileList",[NUMBER]);
         this._itemArr = new Vector.<TreasureItem>();
         var _loc1_:int = 1;
         while(_loc1_ <= LENGTH)
         {
            _loc2_ = new TreasureItem();
            _loc2_.initView(_loc1_);
            this._itemList.addChild(_loc2_);
            this._itemArr.push(_loc2_);
            _loc1_++;
         }
         this._content.addChild(this._itemList);
         this._itemArr[0].selectedLight.visible = true;
         this._timesTxt = ComponentFactory.Instance.creatComponentByStylename("treasureHunting.Treasure.TimesTxtImage");
         this._content.addChild(this._timesTxt);
         this._timesInput = ComponentFactory.Instance.creatComponentByStylename("treasureHunting.Treasure.TimesTxt");
         this._content.addChild(this._timesInput);
         this._timesInput.maxChars = 2;
         this._timesInput.text = DEFAULT;
         this._timesInput.restrict = RESTRICT;
         this._maxBtn = ComponentFactory.Instance.creatComponentByStylename("treasureHunting.Treasure.MaxBtn");
         this._content.addChild(this._maxBtn);
         this._huntingBtn = ComponentFactory.Instance.creatComponentByStylename("treasureHunting.Treasure.HuntBtn");
         this._content.addChild(this._huntingBtn);
         this._showPrizeBtn = ComponentFactory.Instance.creatComponentByStylename("treasureHunting.Treasure.AwardBtn");
         this._content.addChild(this._showPrizeBtn);
         this._rankPrizeBtn = ComponentFactory.Instance.creatComponentByStylename("treasureHunting.rankPrizeBtn");
         this._rankPrizeBtn.visible = false;
         this._content.addChild(this._rankPrizeBtn);
         this._bagView = new TreasureBagView();
         PositionUtils.setPos(this._bagView,"treasureHunting.rightViewPos");
         this._content.addChild(this._bagView);
         this._rankView = new TreasureRankView();
         PositionUtils.setPos(this._rankView,"treasureHunting.rightViewPos");
         this._content.addChild(this._rankView);
         this._rankView.visible = false;
         this._luckyStoneBG = ComponentFactory.Instance.creat("treasureHunting.luckyStoneBG");
         this._content.addChild(this._luckyStoneBG);
         this._bagBtn = ComponentFactory.Instance.creatComponentByStylename("treasureHunting.bagBtn");
         this._content.addChild(this._bagBtn);
         this._bagBtn.visible = false;
         this._rankBtn = ComponentFactory.Instance.creatComponentByStylename("treasureHunting.rankBtn");
         this._content.addChild(this._rankBtn);
         this._rankBtn.visible = false;
         this._lastTimeTxt = ComponentFactory.Instance.creatComponentByStylename("treasureHunting.lastTimeTxt");
         this._content.addChild(this._lastTimeTxt);
         this._myNumberTxt = ComponentFactory.Instance.creatComponentByStylename("treasureHunting.stoneNumberTxt");
         this._content.addChild(this._myNumberTxt);
         this._myNumberTxt.text = PlayerManager.Instance.Self.badLuckNumber.toString();
         addToContent(this._content);
      }
      
      private function initEvent() : void
      {
         addEventListener(FrameEvent.RESPONSE,this._response);
         this._showPrizeBtn.addEventListener(MouseEvent.CLICK,this.onShowPrizeBtnClick);
         this._rankPrizeBtn.addEventListener(MouseEvent.CLICK,this.onRankPrizeBtnClick);
         this._timesInput.addEventListener(Event.CHANGE,this._change);
         this._huntingBtn.addEventListener(MouseEvent.CLICK,this.onHuntingBtnClick);
         this._maxBtn.addEventListener(MouseEvent.CLICK,this.onMaxBtnClick);
         this._bagBtn.addEventListener(MouseEvent.CLICK,this.onBagBtnClick);
         this._rankBtn.addEventListener(MouseEvent.CLICK,this.onRankBtnClick);
         RouletteManager.instance.addEventListener(CaddyEvent.UPDATE_BADLUCK,this.__updateLastTime);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.UPDATE_PRIVATE_INFO,this.__updateInfo);
         PlayerManager.Instance.Self.addEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__changeBadLuckNumber);
      }
      
      protected function onRankBtnClick(param1:MouseEvent) : void
      {
         this._bagBtn.visible = true;
         this._rankBtn.visible = false;
         this._rankView.visible = true;
         this._bagView.visible = false;
      }
      
      protected function onBagBtnClick(param1:MouseEvent) : void
      {
         this._bagBtn.visible = false;
         this._rankBtn.visible = true;
         this._rankView.visible = false;
         this._bagView.visible = true;
      }
      
      private function onShowPrizeBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         TreasureManager.instance.openShowPrize();
      }
      
      private function onRankPrizeBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         TreasureManager.instance.openRankPrize();
      }
      
      private function _change(param1:Event) : void
      {
         var _loc2_:int = int(this._timesInput.text);
         var _loc3_:String = this._timesInput.text.toString();
         var _loc4_:int = TreasureBagView.BAG_SIZE;
         if(_loc2_ > _loc4_)
         {
            this._timesInput.text = _loc4_.toString();
         }
         if(_loc3_ == "0" || _loc3_ == "")
         {
            this._timesInput.text = "1";
         }
      }
      
      private function onHuntingBtnClick(param1:MouseEvent) : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:String = null;
         SoundManager.instance.play("008");
         if(this.isClickMax)
         {
            _loc3_ = PlayerManager.Instance.Self.CaddyBag.items.length;
            _loc4_ = TreasureBagView.BAG_SIZE - PlayerManager.Instance.Self.CaddyBag.items.length;
            if(PlayerManager.Instance.Self.Money < _loc4_ * this._unitPrice)
            {
               _loc4_ = Math.floor(PlayerManager.Instance.Self.Money / this._unitPrice) as int;
            }
            if(_loc4_ == 0)
            {
               _loc4_ = 1;
            }
            this._timesInput.text = String(_loc4_);
         }
         this.isClickMax = false;
         if(this._timesInput.text == "" || this._timesInput.text == "0")
         {
            return;
         }
         var _loc2_:int = parseInt(this._timesInput.text);
         if(SharedManager.Instance.isRemindTreasureBind)
         {
            _loc5_ = LanguageMgr.GetTranslation("treasureHunting.alert.title",_loc2_,this._unitPrice * _loc2_);
            TreasureManager.instance.showTransactionFrame(_loc5_,this.payForHunting,this.noAlertView,null,false);
         }
         else
         {
            this.payForHunting(SharedManager.Instance.isTreasureBind);
         }
      }
      
      private function onMaxBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         var _loc2_:int = PlayerManager.Instance.Self.CaddyBag.items.length;
         var _loc3_:int = TreasureBagView.BAG_SIZE - PlayerManager.Instance.Self.CaddyBag.items.length;
         if(PlayerManager.Instance.Self.Money < _loc3_ * this._unitPrice)
         {
            _loc3_ = Math.floor(PlayerManager.Instance.Self.Money / this._unitPrice) as int;
         }
         if(_loc3_ == 0)
         {
            _loc3_ = 1;
         }
         this._timesInput.text = String(_loc3_);
         this.isClickMax = true;
      }
      
      private function payForHunting(param1:Boolean) : void
      {
         SharedManager.Instance.isTreasureBind = param1;
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         var _loc2_:int = parseInt(this._timesInput.text);
         if(param1 && PlayerManager.Instance.Self.BandMoney < this._unitPrice * _loc2_)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("treasureHunting.tip2"));
            SharedManager.Instance.isRemindTreasureBind = true;
            return;
         }
         if(!param1 && PlayerManager.Instance.Self.Money < this._unitPrice * _loc2_)
         {
            LeavePageManager.showFillFrame();
            SharedManager.Instance.isRemindTreasureBind = true;
            return;
         }
         if(_loc2_ == 1)
         {
            this._huntingBtn.enable = false;
            TreasureManager.instance.isMovieComplete = false;
            TreasureManager.instance.addMask();
         }
         TreasureManager.instance.closeTransactionFrame();
         SocketManager.Instance.out.sendPayForHunting(param1,_loc2_);
         this.isClickMax = false;
         this._timesInput.text = 1 + "";
      }
      
      private function noAlertView(param1:Boolean) : void
      {
         SharedManager.Instance.isRemindTreasureBind = !param1;
      }
      
      private function onTimer(param1:TimerEvent) : void
      {
         var _loc2_:int = 0;
         this.removeAllItemLight();
         do
         {
            _loc2_ = Math.floor(Math.random() * 16) as int;
         }
         while(_loc2_ == this._ran);
         
         this._ran = _loc2_;
         this._itemArr[this._ran].selectedLight.visible = true;
         SoundManager.instance.play("203");
      }
      
      private function removeAllItemLight() : void
      {
         if(this._itemArr == null)
         {
            return;
         }
         var _loc1_:int = 0;
         while(_loc1_ <= this._itemArr.length - 1)
         {
            this._itemArr[_loc1_].selectedLight.visible = false;
            _loc1_++;
         }
      }
      
      private function onTimerComplete(param1:TimerEvent) : void
      {
         var _loc3_:int = 0;
         var _loc2_:Array = TreasureManager.instance.lightIndexArr;
         this.removeAllItemLight();
         this._glint = ComponentFactory.Instance.creat("treasureHunting.GlintAsset");
         this._glint.addEventListener(Event.ENTER_FRAME,this.onEnterFrame);
         _loc3_ = _loc2_[0];
         this._glint.scaleX = 1.3;
         this._glint.scaleY = 1.3;
         this._glint.x = this._itemList.x + this._itemArr[_loc3_].x + 10;
         this._glint.y = this._itemList.y + this._itemArr[_loc3_].y + 10;
         this._content.addChild(this._glint);
      }
      
      public function creatMoveCell(param1:int) : void
      {
         var _loc2_:Shape = new Shape();
         _loc2_.graphics.beginFill(16777215,0);
         _loc2_.graphics.drawRect(0,0,75,75);
         _loc2_.graphics.endFill();
         var _loc3_:InventoryItemInfo = new InventoryItemInfo();
         _loc3_.TemplateID = param1;
         _loc3_ = ItemManager.fill(_loc3_);
         if(param1 == 11550)
         {
            this.luckStoneCell = new BaseCell(_loc2_);
            this.luckStoneCell.info = _loc3_;
         }
         else
         {
            this.moveCell = new BaseCell(_loc2_);
            this.moveCell.info = _loc3_;
         }
      }
      
      private function onEnterFrame(param1:Event) : void
      {
         var _loc2_:Array = null;
         var _loc3_:int = 0;
         if(this._glint.currentFrame == this._glint.totalFrames)
         {
            this._glint.stop();
            this._glint.removeEventListener(Event.ENTER_FRAME,this.onEnterFrame);
            this._content.removeChild(this._glint);
            this._glint = null;
            _loc2_ = TreasureManager.instance.lightIndexArr;
            _loc3_ = _loc2_[0];
            this._itemArr[_loc3_].selectedLight.visible = true;
            SoundManager.instance.play("204");
            this._itemArr[_loc3_].addChild(this.moveCell);
            this.moveCell.visible = false;
            this.addMoveEffect(this.moveCell,582,67);
            if(this.luckStoneCell)
            {
               this._itemArr[_loc3_].addChild(this.luckStoneCell);
               this.luckStoneCell.visible = false;
               this.addMoveEffect(this.luckStoneCell,625,488);
               this.luckStoneCell = null;
            }
            this._huntingBtn.enable = true;
         }
      }
      
      private function addMoveEffect(param1:DisplayObject, param2:int, param3:int) : void
      {
         var _loc6_:TweenProxy = null;
         var _loc7_:TimelineLite = null;
         var _loc8_:TweenLite = null;
         var _loc9_:TweenLite = null;
         if(!param1)
         {
            return;
         }
         var _loc4_:BitmapData = new BitmapData(param1.width,param1.height,true,0);
         _loc4_.draw(param1);
         var _loc5_:Bitmap = new Bitmap(_loc4_,"auto",true);
         addChild(_loc5_);
         _loc6_ = TweenProxy.create(_loc5_);
         _loc6_.registrationX = _loc6_.width / 2;
         _loc6_.registrationY = _loc6_.height / 2;
         var _loc10_:Point = DisplayUtils.localizePoint(this,param1);
         _loc6_.x = _loc10_.x + _loc6_.width / 2;
         _loc6_.y = _loc10_.y + _loc6_.height / 2;
         _loc7_ = new TimelineLite();
         _loc7_.vars.onComplete = this.twComplete;
         _loc7_.vars.onCompleteParams = [_loc7_,_loc6_,_loc5_];
         _loc8_ = new TweenLite(_loc6_,0.4,{
            "x":param2,
            "y":param3
         });
         _loc9_ = new TweenLite(_loc6_,0.4,{
            "scaleX":0.1,
            "scaleY":0.1
         });
         _loc7_.append(_loc8_);
         _loc7_.append(_loc9_,-0.2);
      }
      
      private function twComplete(param1:TimelineLite, param2:TweenProxy, param3:Bitmap) : void
      {
         if(param1)
         {
            param1.kill();
         }
         if(param2)
         {
            param2.destroy();
         }
         if(param3.parent)
         {
            param3.parent.removeChild(param3);
            param3.bitmapData.dispose();
         }
         param2 = null;
         param3 = null;
         param1 = null;
         this.movieComplete();
      }
      
      private function movieComplete() : void
      {
         if(TreasureManager.instance.msgStr != "")
         {
            MessageTipManager.getInstance().show(TreasureManager.instance.countMsg);
            ChatManager.Instance.sysChatYellow(TreasureManager.instance.msgStr);
            TreasureManager.instance.countMsg = "";
            TreasureManager.instance.msgStr = "";
         }
         TreasureManager.instance.isMovieComplete = true;
         TreasureManager.instance.removeMask();
         TreasureManager.instance.dispatchEvent(new TreasureEvent(TreasureEvent.MOVIE_COMPLETE));
      }
      
      public function startTimer() : void
      {
         this._glintTimer.reset();
         this._glintTimer.start();
      }
      
      public function lightUpItemArr() : void
      {
         var _loc3_:int = 0;
         var _loc1_:Array = TreasureManager.instance.lightIndexArr;
         this.removeAllItemLight();
         var _loc2_:int = 0;
         while(_loc2_ <= _loc1_.length - 1)
         {
            _loc3_ = _loc1_[_loc2_];
            this._itemArr[_loc3_].selectedLight.visible = true;
            _loc2_++;
         }
         MessageTipManager.getInstance().show(TreasureManager.instance.countMsg);
         ChatManager.Instance.sysChatYellow(TreasureManager.instance.msgStr);
         TreasureManager.instance.countMsg = "";
         TreasureManager.instance.msgStr = "";
      }
      
      protected function _response(param1:FrameEvent) : void
      {
         var _loc2_:BaseAlerFrame = null;
         if(param1.responseCode == FrameEvent.CLOSE_CLICK || param1.responseCode == FrameEvent.ESC_CLICK)
         {
            SoundManager.instance.play("008");
            if(TreasureManager.instance.checkBag())
            {
               _loc2_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("treasureHunting.alert.ensureGetAll"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),false,true,true,LayerManager.ALPHA_BLOCKGOUND);
               _loc2_.mouseEnabled = false;
               _loc2_.addEventListener(FrameEvent.RESPONSE,this.onAlertResponse);
            }
            else
            {
               this.dispose();
            }
         }
      }
      
      private function onAlertResponse(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         (param1.currentTarget as BaseAlerFrame).removeEventListener(FrameEvent.RESPONSE,this.onAlertResponse);
         if(param1.responseCode == FrameEvent.ENTER_CLICK || param1.responseCode == FrameEvent.SUBMIT_CLICK)
         {
            SocketManager.Instance.out.getAllTreasure();
            this.dispose();
         }
         ObjectUtils.disposeObject(param1.currentTarget);
      }
      
      private function __updateLastTime(param1:CaddyEvent) : void
      {
         this._lastTimeTxt.text = LanguageMgr.GetTranslation("treasureHunting.lastTimeTxt") + param1.lastTime.replace(/:\d+$/g,"");
      }
      
      private function __changeBadLuckNumber(param1:PlayerPropertyEvent) : void
      {
         if(param1.changedProperties["BadLuckNumber"])
         {
            if(PlayerManager.Instance.Self.badLuckNumber == 0)
            {
               this._myNumberTxt.text = PlayerManager.Instance.Self.badLuckNumber.toString();
            }
         }
      }
      
      protected function __updateInfo(param1:CrazyTankSocketEvent) : void
      {
         this._myNumberTxt.text = PlayerManager.Instance.Self.badLuckNumber.toString();
      }
      
      private function removeEvents() : void
      {
         if(this._showPrizeBtn)
         {
            this._showPrizeBtn.removeEventListener(MouseEvent.CLICK,this.onShowPrizeBtnClick);
         }
         if(this._timesInput)
         {
            this._timesInput.removeEventListener(Event.CHANGE,this._change);
         }
         if(this._huntingBtn)
         {
            this._huntingBtn.removeEventListener(MouseEvent.CLICK,this.onHuntingBtnClick);
         }
         if(this._maxBtn)
         {
            this._maxBtn.removeEventListener(MouseEvent.CLICK,this.onMaxBtnClick);
         }
         if(this._bagBtn)
         {
            this._bagBtn.removeEventListener(MouseEvent.CLICK,this.onBagBtnClick);
         }
         if(this._rankBtn)
         {
            this._rankBtn.removeEventListener(MouseEvent.CLICK,this.onRankBtnClick);
         }
         RouletteManager.instance.removeEventListener(CaddyEvent.UPDATE_BADLUCK,this.__updateLastTime);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.UPDATE_PRIVATE_INFO,this.__updateInfo);
         PlayerManager.Instance.Self.removeEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__changeBadLuckNumber);
      }
      
      override public function dispose() : void
      {
         this.removeEvents();
         TreasureManager.instance.dispose();
         if(this._glintTimer)
         {
            this._glintTimer.removeEventListener(TimerEvent.TIMER,this.onTimer);
            this._glintTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,this.onTimerComplete);
            this._glintTimer = null;
         }
         if(this._rankTimer)
         {
            this._rankTimer.removeEventListener(TimerEvent.TIMER,this.updateRank);
            this._rankTimer = null;
         }
         if(this._bg)
         {
            ObjectUtils.disposeObject(this._bg);
         }
         this._bg = null;
         if(this._treasureTitle)
         {
            ObjectUtils.disposeObject(this._treasureTitle);
         }
         this._treasureTitle = null;
         if(this._timesTxt)
         {
            ObjectUtils.disposeObject(this._timesTxt);
         }
         this._timesTxt = null;
         if(this._timesInput)
         {
            ObjectUtils.disposeObject(this._timesInput);
         }
         this._timesInput = null;
         if(this._itemList)
         {
            ObjectUtils.disposeObject(this._itemList);
         }
         this._itemList = null;
         if(this._showPrizeBtn)
         {
            ObjectUtils.disposeObject(this._showPrizeBtn);
         }
         this._showPrizeBtn = null;
         if(this._huntingBtn)
         {
            ObjectUtils.disposeObject(this._huntingBtn);
         }
         this._huntingBtn = null;
         if(this._maxBtn)
         {
            ObjectUtils.disposeObject(this._maxBtn);
         }
         this._maxBtn = null;
         if(this._rankPrizeBtn)
         {
            ObjectUtils.disposeObject(this._rankPrizeBtn);
         }
         this._rankPrizeBtn = null;
         if(this._bagBtn)
         {
            ObjectUtils.disposeObject(this._bagBtn);
         }
         this._bagBtn = null;
         if(this._rankBtn)
         {
            ObjectUtils.disposeObject(this._rankBtn);
         }
         this._rankBtn = null;
         if(this._luckyStoneBG)
         {
            ObjectUtils.disposeObject(this._luckyStoneBG);
         }
         this._luckyStoneBG = null;
         if(this._bagView)
         {
            ObjectUtils.disposeObject(this._bagView);
         }
         this._bagView = null;
         if(this._rankView)
         {
            ObjectUtils.disposeObject(this._rankView);
         }
         this._rankView = null;
         if(this._lastTimeTxt)
         {
            ObjectUtils.disposeObject(this._lastTimeTxt);
         }
         this._lastTimeTxt = null;
         if(this._myNumberTxt)
         {
            ObjectUtils.disposeObject(this._myNumberTxt);
         }
         this._myNumberTxt = null;
         if(this.moveCell)
         {
            ObjectUtils.disposeObject(this.moveCell);
         }
         this.moveCell = null;
         if(this.luckStoneCell)
         {
            ObjectUtils.disposeObject(this.luckStoneCell);
         }
         this.luckStoneCell = null;
         if(this._content)
         {
            ObjectUtils.disposeObject(this._content);
         }
         this._content = null;
         this._itemArr = null;
         super.dispose();
      }
      
      public function get huntingBtn() : BaseButton
      {
         return this._huntingBtn;
      }
      
      public function get bagView() : TreasureBagView
      {
         return this._bagView;
      }
   }
}
