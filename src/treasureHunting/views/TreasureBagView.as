package treasureHunting.views
{
   import bagAndInfo.bag.BagListView;
   import bagAndInfo.cell.BagCell;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.BagInfo;
   import ddt.data.EquipType;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.events.CellEvent;
   import ddt.manager.LanguageMgr;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import flash.utils.Dictionary;
   import treasureHunting.TreasureEvent;
   import treasureHunting.TreasureManager;
   
   public class TreasureBagView extends Sprite implements Disposeable
   {
      
      public static const BAG_SIZE:int = 24;
       
      
      private var _bagBG:Bitmap;
      
      private var _baglist:BagListView;
      
      private var _getAllBtn:BaseButton;
      
      private var _sellAllBtn:BaseButton;
      
      private var _bagData:Dictionary;
      
      private var isBagUpdate:Boolean;
      
      public function TreasureBagView()
      {
         super();
         this.initData();
         this.initView();
         this.initEvent();
      }
      
      private function initData() : void
      {
         this.isBagUpdate = false;
         SocketManager.Instance.out.updateTreasureBag();
      }
      
      private function initView() : void
      {
         this._bagBG = ComponentFactory.Instance.creat("treasureHunting.bagBG");
         addChild(this._bagBG);
         this._baglist = new BagListView(0,4,BAG_SIZE);
         PositionUtils.setPos(this._baglist,"treasureHunting.baglistPos");
         this._baglist.vSpace = 4;
         this._baglist.hSpace = 5;
         addChild(this._baglist);
         this._getAllBtn = ComponentFactory.Instance.creatComponentByStylename("treasureHunting.bag.getAllBtn");
         addChild(this._getAllBtn);
         this._sellAllBtn = ComponentFactory.Instance.creatComponentByStylename("treasureHunting.bag.sellAllBtn");
         addChild(this._sellAllBtn);
      }
      
      private function initEvent() : void
      {
         this._getAllBtn.addEventListener(MouseEvent.CLICK,this.onGetAllBtnClick);
         this._sellAllBtn.addEventListener(MouseEvent.CLICK,this.onSellAllBtnClick);
         this._baglist.addEventListener(CellEvent.ITEM_CLICK,this.onItemClick);
         TreasureManager.instance.addEventListener(TreasureEvent.MOVIE_COMPLETE,this.onMovieComplete);
      }
      
      public function updateData(param1:Dictionary) : void
      {
         this._bagData = param1;
         this.isBagUpdate = true;
         if(TreasureManager.instance.isMovieComplete)
         {
            this.updateBagFrame();
         }
      }
      
      private function onMovieComplete(param1:TreasureEvent) : void
      {
         if(this.isBagUpdate)
         {
            this.updateBagFrame();
         }
      }
      
      private function updateBagFrame() : void
      {
         var _loc1_:* = null;
         var _loc2_:InventoryItemInfo = null;
         for(_loc1_ in this._bagData)
         {
            _loc2_ = PlayerManager.Instance.Self.CaddyBag.getItemAt(int(_loc1_));
            if(this._baglist != null)
            {
               this._baglist.setCellInfo(int(_loc1_),_loc2_);
            }
         }
         this.isBagUpdate = false;
      }
      
      private function onItemClick(param1:CellEvent) : void
      {
         SoundManager.instance.play("008");
         var _loc2_:BagCell = param1.data as BagCell;
         var _loc3_:int = (_loc2_.info as InventoryItemInfo).Count;
         var _loc4_:int = this._getBagType(_loc2_.info as InventoryItemInfo);
         SocketManager.Instance.out.sendMoveGoods(BagInfo.CADDYBAG,_loc2_.place,_loc4_,-1,_loc3_);
      }
      
      private function _getBagType(param1:InventoryItemInfo) : int
      {
         var _loc2_:int = 0;
         switch(param1.CategoryID)
         {
            case EquipType.UNFRIGHTPROP:
               if(param1.Property1 == "31")
               {
                  _loc2_ = BagInfo.BEADBAG;
               }
               else
               {
                  _loc2_ = BagInfo.PROPBAG;
               }
               break;
            case EquipType.FRIGHTPROP:
            case EquipType.TASK:
            case EquipType.TEXP:
            case EquipType.TEXP_TASK:
            case EquipType.ACTIVE_TASK:
            case EquipType.FOOD:
            case EquipType.PET_EGG:
            case EquipType.VEGETABLE:
               _loc2_ = BagInfo.PROPBAG;
               break;
            case EquipType.SEED:
            case EquipType.MANURE:
               _loc2_ = BagInfo.FARM;
               break;
            default:
               _loc2_ = BagInfo.EQUIPBAG;
         }
         return _loc2_;
      }
      
      private function onGetAllBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         SocketManager.Instance.out.getAllTreasure();
      }
      
      private function onSellAllBtnClick(param1:MouseEvent) : void
      {
         var _loc2_:BaseAlerFrame = null;
         SoundManager.instance.play("008");
         _loc2_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("treasureHunting.alert.ensureSellAll"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),false,true,true,LayerManager.ALPHA_BLOCKGOUND);
         _loc2_.mouseEnabled = false;
         _loc2_.addEventListener(FrameEvent.RESPONSE,this.onAlertResponse);
      }
      
      private function onAlertResponse(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         (param1.currentTarget as BaseAlerFrame).removeEventListener(FrameEvent.RESPONSE,this.onAlertResponse);
         if(param1.responseCode == FrameEvent.ENTER_CLICK || param1.responseCode == FrameEvent.SUBMIT_CLICK)
         {
            SocketManager.Instance.out.sendSellAll();
         }
         ObjectUtils.disposeObject(param1.currentTarget);
      }
      
      private function removeEvents() : void
      {
         if(this._getAllBtn)
         {
            this._getAllBtn.removeEventListener(MouseEvent.CLICK,this.onGetAllBtnClick);
         }
         if(this._sellAllBtn)
         {
            this._sellAllBtn.removeEventListener(MouseEvent.CLICK,this.onSellAllBtnClick);
         }
      }
      
      public function dispose() : void
      {
         if(this._bagBG)
         {
            ObjectUtils.disposeObject(this._bagBG);
         }
         this._bagBG = null;
         if(this._baglist)
         {
            ObjectUtils.disposeObject(this._baglist);
         }
         this._baglist = null;
         if(this._getAllBtn)
         {
            ObjectUtils.disposeObject(this._getAllBtn);
         }
         this._getAllBtn = null;
         if(this._sellAllBtn)
         {
            ObjectUtils.disposeObject(this._sellAllBtn);
         }
         this._sellAllBtn = null;
      }
   }
}
