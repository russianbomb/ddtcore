package treasureHunting.views
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.container.VBox;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.RouletteManager;
   import ddt.view.caddyII.CaddyEvent;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import treasureHunting.items.LuckRankItem;
   
   public class TreasureRankView extends Sprite implements Disposeable
   {
       
      
      private var _rankBG:Bitmap;
      
      private var _itemBGDeep:Bitmap;
      
      private var _itemBGLighter:Bitmap;
      
      private var _ranklist:VBox;
      
      private var _itemList:Vector.<LuckRankItem>;
      
      private var _dataList:Vector.<Object>;
      
      public function TreasureRankView()
      {
         this._dataList = new Vector.<Object>();
         super();
         this.initView();
         this.initEvent();
      }
      
      private function initView() : void
      {
         var _loc2_:LuckRankItem = null;
         this._rankBG = ComponentFactory.Instance.creat("treasureHunting.rankBG");
         addChild(this._rankBG);
         this._ranklist = ComponentFactory.Instance.creatComponentByStylename("treasureHunting.LuckBox");
         addChild(this._ranklist);
         this._itemList = new Vector.<LuckRankItem>();
         var _loc1_:int = 0;
         while(_loc1_ < 10)
         {
            _loc2_ = new LuckRankItem(_loc1_);
            _loc2_.addEventListener(MouseEvent.CLICK,this.onItemClick);
            this._ranklist.addChild(_loc2_);
            this._itemList.push(_loc2_);
            _loc1_++;
         }
      }
      
      protected function onItemClick(param1:MouseEvent) : void
      {
         var _loc2_:int = 0;
         while(_loc2_ <= this._itemList.length - 1)
         {
            (this._itemList[_loc2_] as LuckRankItem).selected = false;
            _loc2_++;
         }
         (param1.currentTarget as LuckRankItem).selected = true;
      }
      
      private function initEvent() : void
      {
         RouletteManager.instance.addEventListener(CaddyEvent.UPDATE_BADLUCK,this.__getLuckRank);
      }
      
      protected function __getLuckRank(param1:CaddyEvent) : void
      {
         var _loc3_:Object = null;
         this._dataList = param1.dataList;
         var _loc2_:int = 0;
         while(_loc2_ < 10 && _loc2_ < this._dataList.length)
         {
            _loc3_ = this._dataList[_loc2_];
            this._itemList[_loc2_].update(_loc2_,_loc3_["Nickname"],_loc3_["Count"]);
            _loc2_++;
         }
      }
      
      private function removeEvent() : void
      {
         RouletteManager.instance.removeEventListener(CaddyEvent.UPDATE_BADLUCK,this.__getLuckRank);
      }
      
      public function dispose() : void
      {
         this.removeEvent();
         if(this._rankBG)
         {
            ObjectUtils.disposeObject(this._rankBG);
         }
         this._rankBG = null;
         if(this._ranklist)
         {
            ObjectUtils.disposeObject(this._ranklist);
         }
         this._ranklist = null;
         this._itemList = null;
         this._dataList = null;
      }
   }
}
