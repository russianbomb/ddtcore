package treasureHunting
{
   import com.pickgliss.events.ComponentEvent;
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.EquipType;
   import ddt.data.UIModuleTypes;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.events.BagEvent;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.ChatManager;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.view.TransactionsFrame;
   import ddt.view.UIModuleSmallLoading;
   import ddt.view.caddyII.CaddyAwardInfo;
   import ddt.view.caddyII.CaddyAwardListFrame;
   import ddt.view.caddyII.CaddyModel;
   import ddt.view.caddyII.LookTrophy;
   import flash.display.Sprite;
   import flash.events.EventDispatcher;
   import flash.events.MouseEvent;
   import flash.utils.Dictionary;
   import hallIcon.HallIconManager;
   import hallIcon.HallIconType;
   import road7th.comm.PackageIn;
   import treasureHunting.views.TreasureEnterView;
   import treasureHunting.views.TreasureHuntingFrame;
   import wonderfulActivity.WonderfulActivityManager;
   
   public class TreasureManager extends EventDispatcher
   {
      
      private static var _instance:TreasureManager;
       
      
      private var treasureFrame:TreasureHuntingFrame;
      
      private var showPrize:LookTrophy;
      
      private var _transactionsFrame:TransactionsFrame;
      
      private var _listView:CaddyAwardListFrame;
      
      public var needMoney:int = 0;
      
      private var _dataXml:XML;
      
      private var _awards:Vector.<CaddyAwardInfo>;
      
      private var _func:Function;
      
      private var _funcParams:Array;
      
      public var hasItemsInbag:Boolean = false;
      
      public var isMovieComplete:Boolean = true;
      
      public var isActivityOpen:Boolean = false;
      
      public var startDate:Date;
      
      public var endDate:Date;
      
      private var _mask:Sprite;
      
      private var bagData:Dictionary;
      
      public var lightIndexArr:Array;
      
      public var msgStr:String;
      
      public var countMsg:String;
      
      public function TreasureManager()
      {
         super();
         this.bagData = new Dictionary();
         this.initEvent();
      }
      
      public static function get instance() : TreasureManager
      {
         if(!_instance)
         {
            _instance = new TreasureManager();
         }
         return _instance;
      }
      
      private function initEvent() : void
      {
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PAY_FOR_HUNTING,this.onPayForHunting);
         PlayerManager.Instance.Self.CaddyBag.addEventListener(BagEvent.UPDATE,this._update);
      }
      
      public function setup() : void
      {
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.INIT_TREASURE,this.__onInitTreasure);
      }
      
      private function __onInitTreasure(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:Boolean = _loc2_.readBoolean();
         this.isActivityOpen = _loc2_.readBoolean();
         if(this.isActivityOpen)
         {
            this.startDate = _loc2_.readDate();
            this.endDate = _loc2_.readDate();
            TreasureManager.instance.needMoney = _loc2_.readInt();
            this.showIcon();
            if(_loc3_)
            {
               ChatManager.Instance.sysChatAmaranth(LanguageMgr.GetTranslation("treasureHunting.start"));
            }
         }
         else
         {
            if(!WonderfulActivityManager.Instance.frame)
            {
               HallIconManager.instance.updateSwitchHandler(HallIconType.TREASUREHUNTING,false);
            }
            if(_loc3_)
            {
               ChatManager.Instance.sysChatAmaranth(LanguageMgr.GetTranslation("treasureHunting.end"));
            }
            this.removeMask();
         }
      }
      
      public function showIcon() : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.TREASUREHUNTING,true);
      }
      
      public function showFrame() : void
      {
         var _loc1_:TreasureEnterView = null;
         SoundManager.instance.play("008");
         _loc1_ = new TreasureEnterView();
         _loc1_.init();
         _loc1_.x = -227;
         HallIconManager.instance.showCommonFrame(_loc1_,"wonderfulActivityManager.btnTxt12");
      }
      
      public function loadTreasureHuntingModule(param1:Function = null, param2:Array = null) : void
      {
         this._func = param1;
         this._funcParams = param2;
         UIModuleSmallLoading.Instance.progress = 0;
         UIModuleSmallLoading.Instance.show();
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadCompleteHandler);
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUimoduleLoadProgress);
         UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.TREASURE_HUNTING);
      }
      
      private function onUimoduleLoadProgress(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.TREASURE_HUNTING)
         {
            UIModuleSmallLoading.Instance.progress = param1.loader.progress * 100;
         }
      }
      
      private function loadCompleteHandler(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.TREASURE_HUNTING)
         {
            UIModuleSmallLoading.Instance.hide();
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadCompleteHandler);
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUimoduleLoadProgress);
            if(null != this._func)
            {
               this._func.apply(null,this._funcParams);
            }
            this._func = null;
            this._funcParams = null;
         }
      }
      
      private function _update(param1:BagEvent) : void
      {
         var _loc3_:* = null;
         var _loc2_:Dictionary = param1.changedSlots;
         if(this.treasureFrame && this.treasureFrame.bagView)
         {
            this.treasureFrame.bagView.updateData(_loc2_);
         }
         for(_loc3_ in _loc2_)
         {
            this.bagData[_loc3_] = _loc2_[_loc3_];
         }
      }
      
      private function onPayForHunting(param1:CrazyTankSocketEvent) : void
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:String = null;
         var _loc7_:int = 0;
         this.lightIndexArr = [];
         this.msgStr = LanguageMgr.GetTranslation("treasureHunting.msg");
         var _loc2_:PackageIn = param1.pkg;
         this.isActivityOpen = _loc2_.readBoolean();
         var _loc3_:int = _loc2_.readInt();
         if(this.isActivityOpen == false || _loc3_ <= 0)
         {
            this.treasureFrame.huntingBtn.enable = true;
            TreasureManager.instance.isMovieComplete = true;
            this.removeMask();
            return;
         }
         this.countMsg = LanguageMgr.GetTranslation("treasureHunting.count",_loc3_);
         var _loc8_:int = 0;
         while(_loc8_ <= _loc3_ - 1)
         {
            _loc4_ = _loc2_.readInt();
            _loc5_ = _loc2_.readInt();
            _loc6_ = _loc2_.readUTF();
            _loc7_ = _loc2_.readInt();
            this.msgStr = this.msgStr + (_loc6_ + "x" + _loc7_ + "  ");
            this.lightIndexArr.push(_loc5_ - 1);
            _loc8_++;
         }
         this.treasureFrame.creatMoveCell(_loc4_);
         var _loc9_:int = _loc2_.readInt();
         if(_loc9_ > 0)
         {
            this.msgStr = this.msgStr + ("幸运彩石x" + _loc9_);
            this.treasureFrame.creatMoveCell(11550);
            SocketManager.Instance.out.sendQequestBadLuck();
         }
         if(_loc3_ == 1)
         {
            this.treasureFrame.startTimer();
         }
         else
         {
            this.treasureFrame.lightUpItemArr();
         }
      }
      
      public function openShowPrize() : void
      {
         this.showPrize = ComponentFactory.Instance.creatCustomObject("treasureHunting.LookTrophy");
         this.showPrize.show(CaddyModel.instance.getCaddyTrophy(EquipType.TREASURE_CADDY));
         this.showPrize.addEventListener(ComponentEvent.DISPOSE,this.onShowPrizeDispose);
      }
      
      public function openRankPrize() : void
      {
         this._listView = ComponentFactory.Instance.creatComponentByStylename("caddyAwardListFrame");
         LayerManager.Instance.addToLayer(this._listView,LayerManager.GAME_TOP_LAYER,true,LayerManager.NONE_BLOCKGOUND);
      }
      
      private function onShowPrizeDispose(param1:ComponentEvent) : void
      {
         this.showPrize.removeEventListener(ComponentEvent.DISPOSE,this.onShowPrizeDispose);
         ObjectUtils.disposeObject(this.showPrize);
         this.showPrize = null;
      }
      
      public function showTransactionFrame(param1:String, param2:Function = null, param3:Function = null, param4:Sprite = null, param5:Boolean = true) : void
      {
         this._transactionsFrame = ComponentFactory.Instance.creatComponentByStylename("treasureHunting.TransactionsFrame");
         this._transactionsFrame.setTxt(param1);
         this._transactionsFrame.buyFunction = param2;
         this._transactionsFrame.clickFunction = param3;
         this._transactionsFrame.target = param4;
         this._transactionsFrame.autoClose = param5;
         LayerManager.Instance.addToLayer(this._transactionsFrame,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.ALPHA_BLOCKGOUND);
      }
      
      public function closeTransactionFrame() : void
      {
         if(this._transactionsFrame)
         {
            this._transactionsFrame.dispose();
         }
      }
      
      public function addMask() : void
      {
         if(this._mask == null)
         {
            this._mask = new Sprite();
            this._mask.graphics.beginFill(0,0);
            this._mask.graphics.drawRect(0,0,2000,1200);
            this._mask.graphics.endFill();
            this._mask.addEventListener(MouseEvent.CLICK,this.onMaskClick);
         }
         LayerManager.Instance.addToLayer(this._mask,LayerManager.GAME_DYNAMIC_LAYER,false,LayerManager.ALPHA_BLOCKGOUND);
      }
      
      public function removeMask() : void
      {
         if(this._mask != null)
         {
            this._mask.parent.removeChild(this._mask);
            this._mask.removeEventListener(MouseEvent.CLICK,this.onMaskClick);
            this._mask = null;
         }
      }
      
      private function onMaskClick(param1:MouseEvent) : void
      {
         MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("treasureHunting.huntingNow"));
      }
      
      public function checkBag() : Boolean
      {
         var _loc1_:* = null;
         var _loc2_:InventoryItemInfo = null;
         for(_loc1_ in this.bagData)
         {
            _loc2_ = PlayerManager.Instance.Self.CaddyBag.getItemAt(int(_loc1_));
            if(_loc2_ != null)
            {
               return true;
            }
         }
         return false;
      }
      
      public function get awards() : Vector.<CaddyAwardInfo>
      {
         return this._awards;
      }
      
      private function removeEvent() : void
      {
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.PAY_FOR_HUNTING,this.onPayForHunting);
         PlayerManager.Instance.Self.CaddyBag.removeEventListener(BagEvent.UPDATE,this._update);
      }
      
      public function dispose() : void
      {
         this.removeMask();
         if(this.showPrize != null)
         {
            ObjectUtils.disposeObject(this.showPrize);
         }
         this.showPrize = null;
         if(this._listView != null)
         {
            ObjectUtils.disposeObject(this._listView);
         }
         this._listView = null;
         if(this._transactionsFrame != null)
         {
            ObjectUtils.disposeObject(this._transactionsFrame);
         }
         this._transactionsFrame = null;
         if(this._mask != null)
         {
            ObjectUtils.disposeObject(this._mask);
         }
         this._mask = null;
      }
      
      public function setFrame(param1:TreasureHuntingFrame) : void
      {
         this.treasureFrame = param1;
      }
   }
}
