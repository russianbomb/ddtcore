package overSeasCommunity.vietnam
{
import com.adobe.serialization.json.JSON;
import com.pickgliss.toplevel.StageReferance;
   import ddt.manager.LanguageMgr;
   import flash.display.BitmapData;
   import flash.events.EventDispatcher;
   import flash.external.ExternalInterface;
   import overSeasCommunity.vietnam.data.FeedItem;
   import overSeasCommunity.vietnam.openapi.BaseZMOpenAPI;
   import overSeasCommunity.vietnam.openapi.ZMFeedAPI;
   import overSeasCommunity.vietnam.openapi.ZMRequestEvent;
   import overSeasCommunity.vietnam.openapi.ZMSessionAPI;
   import overSeasCommunity.vietnam.openapi.ZMUserAPI;
   
   public class InterfaceController extends EventDispatcher
   {
       
      
      private var _communityUserId:int;
      
      private var _secret_key:String;
      
      private var _pub_key:String;
      
      public function InterfaceController()
      {
         super();
      }
      
      public function setup() : void
      {
         BaseZMOpenAPI.session_id = StageReferance.stage.loaderInfo.parameters["sessionId"];
         var _loc1_:ZMSessionAPI = new ZMSessionAPI(this.onLoggedInUser);
         _loc1_.getLoggedInUser(BaseZMOpenAPI.session_id);
      }
      
      public function pushFeed(param1:int, param2:String = "", param3:String = "", param4:Array = null) : void
      {
         var _loc5_:FeedItem = this.creatFeedItem(param1,param4,param2,param3);
         var _loc6_:ZMFeedAPI = new ZMFeedAPI();
         _loc6_.addEventListener(ZMRequestEvent.RESPONSE,this.__creatSignKey);
         _loc6_.creatSignKey(_loc5_,param4);
      }
      
      private function __creatSignKey(param1:ZMRequestEvent) : void
      {
         (param1.target as ZMFeedAPI).removeEventListener(ZMRequestEvent.RESPONSE,this.__creatSignKey);
         var _loc2_:Array = (param1.target as ZMFeedAPI).callbackArgs;
         var _loc3_:* = param1.result;
         _loc3_["itemTitle1"] = _loc2_[0];
         _loc3_["itemTitle2"] = _loc2_[1];
         _loc3_["itemTitle3"] = _loc2_[2];
         ExternalInterface.call("pushfeed",JSON.encode(_loc3_));
      }
      
      private function onCreatSignKey(param1:*) : void
      {
         ExternalInterface.call("pushfeed",param1);
      }
      
      private function creatFeedItem(param1:int, param2:Array, param3:String, param4:String) : FeedItem
      {
         var _loc5_:String = LanguageMgr.GetTranslation("community.feed.attach_name");
         var _loc6_:String = LanguageMgr.GetTranslation("community.feed.attach_href");
         var _loc7_:String = LanguageMgr.GetTranslation("community.feed.attach_caption");
         var _loc8_:String = LanguageMgr.GetTranslation("community.feed.actlink_href");
         var _loc9_:FeedItem = new FeedItem(this._communityUserId,param2,0,1,param1,"",_loc5_,_loc6_,_loc7_,param3,1,param4,_loc8_,"Link",_loc8_);
         return _loc9_;
      }
      
      private function onLoggedInUser(param1:int) : void
      {
         this._communityUserId = param1;
      }
      
      public function pushAndComment(param1:int, param2:String, param3:String) : void
      {
      }
      
      public function sendNotice(param1:int, param2:String) : void
      {
         var _loc3_:ZMFeedAPI = new ZMFeedAPI();
         _loc3_.sendEmail([param1],"",param2);
      }
      
      public function loadSyncFriends() : void
      {
      }
      
      public function pushAndUpload(param1:BitmapData, param2:String) : void
      {
         var _loc3_:ZMFeedAPI = new ZMFeedAPI();
         _loc3_.pushAndUpload(param1,param2);
      }
      
      private function __uploadComplete(param1:ZMRequestEvent) : void
      {
         (param1.target as ZMFeedAPI).removeEventListener(ZMRequestEvent.RESPONSE,this.__uploadComplete);
         var _loc2_:String = (param1.target as ZMFeedAPI).callbackArgs;
         var _loc3_:String = param1.result["img-link"];
         ExternalInterface.call("alert",_loc3_ + " - " + _loc2_);
      }
      
      public function leaveComment(param1:int, param2:String) : void
      {
         this.sendNotice(param1,param2);
      }
      
      public function checkAllowShareResume(param1:String, param2:Function) : void
      {
         var _loc3_:ZMUserAPI = new ZMUserAPI(param2);
         _loc3_.getInfoByUsername(param1,"");
      }
   }
}

import overSeasCommunity.vietnam.data.FeedItem;

class FeedJSONObject
{
    
   
   public var pub_key:String;
   
   public var sign_key:String;
   
   public var actId:int = 0;
   
   public var userIdFrom:int = 0;
   
   public var userIdTo:int = 0;
   
   public var objectId:String = "";
   
   public var attachName:String = "";
   
   public var attachHref:String = "";
   
   public var attachCaption:String = "";
   
   public var attachDescription:String = "";
   
   public var mediaType:int = 0;
   
   public var mediaImage:String = "";
   
   public var mediaSource:String = "";
   
   public var actionLinkText:String = "";
   
   public var actionLinkHref:String = "";
   
   public var tplId:int = 0;
   
   function FeedJSONObject(param1:FeedItem, param2:String, param3:String)
   {
      super();
      this.pub_key = param2;
      this.sign_key = param3;
      this.userIdFrom = param1.userIdFrom;
      this.userIdTo = param1.userIdTo;
      this.actId = param1.actId;
      this.tplId = param1.tplId;
      this.objectId = param1.objectId;
      this.attachName = param1.attachName;
      this.attachHref = param1.attachHref;
      this.attachCaption = param1.attachCaption;
      this.attachDescription = param1.attachDescription;
      this.mediaType = param1.mediaType;
      this.mediaImage = param1.mediaImage;
      this.mediaSource = param1.mediaSource;
      this.actionLinkText = param1.actionLinkText;
      this.actionLinkHref = param1.actionLinkHref;
   }
}
