package overSeasCommunity.vietnam.data
{
   public dynamic class FeedItem
   {
       
      
      private var _actId:int = 0;
      
      private var _userIdFrom:int = 0;
      
      private var _userIdTo:int = 0;
      
      private var _objectId:String = "";
      
      private var _attachName:String = "";
      
      private var _attachHref:String = "";
      
      private var _attachCaption:String = "";
      
      private var _attachDescription:String = "";
      
      private var _mediaType:int = 0;
      
      private var _mediaImage:String = "";
      
      private var _mediaSource:String = "";
      
      private var _actionLinkText:String = "";
      
      private var _actionLinkHref:String = "";
      
      private var _tplId:int = 0;
      
      public function FeedItem(param1:int, param2:Array, param3:int, param4:int, param5:int, param6:String, param7:String, param8:String, param9:String, param10:String, param11:int, param12:String, param13:String, param14:String, param15:String)
      {
         super();
         this._userIdFrom = param1;
         this._userIdTo = param3;
         this._actId = param4;
         this._tplId = param5;
         this._objectId = param6;
         this._attachName = param7.length > 80?param7.substring(0,80):param7;
         this._attachHref = param8.length > 150?param8.substring(0,150):param8;
         this._attachCaption = param9.length > 30?param9.substring(0,30):param9;
         this._attachDescription = param10.length > 200?param10.substring(0,200):param10;
         this._mediaType = param11;
         this._mediaImage = param12.length > 150?param12.substring(0,150):param12;
         this._mediaSource = param13.length > 150?param13.substring(0,150):param13;
         this._actionLinkText = param14.length > 20?param14.substring(0,20):param14;
         this._actionLinkHref = param15.length > 150?param15.substring(0,150):param15;
      }
      
      public function get userIdFrom() : int
      {
         return this._userIdFrom;
      }
      
      public function get userIdTo() : int
      {
         return this._userIdTo;
      }
      
      public function get actId() : int
      {
         return this._actId;
      }
      
      public function get tplId() : int
      {
         return this._tplId;
      }
      
      public function get objectId() : String
      {
         return this._objectId;
      }
      
      public function get attachName() : String
      {
         return this._attachName;
      }
      
      public function get attachHref() : String
      {
         return this._attachHref;
      }
      
      public function get attachCaption() : String
      {
         return this._attachCaption;
      }
      
      public function get attachDescription() : String
      {
         return this._attachDescription;
      }
      
      public function get mediaType() : int
      {
         return this._mediaType;
      }
      
      public function get mediaImage() : String
      {
         return this._mediaImage;
      }
      
      public function get mediaSource() : String
      {
         return this._mediaSource;
      }
      
      public function get actionLinkText() : String
      {
         return this._actionLinkText;
      }
      
      public function get actionLinkHref() : String
      {
         return this._actionLinkHref;
      }
   }
}
