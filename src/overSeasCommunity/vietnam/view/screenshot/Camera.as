package overSeasCommunity.vietnam.view.screenshot
{
   import com.pickgliss.ui.core.Disposeable;
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.geom.Rectangle;
   
   public class Camera extends Sprite implements Disposeable
   {
       
      
      private var _bitmapData:BitmapData;
      
      private var _image:Bitmap;
      
      private var _imageContainer:Sprite;
      
      private var _cameraBorder:Sprite;
      
      public function Camera()
      {
         super();
         this.init();
         this.addEvent();
      }
      
      private function init() : void
      {
         this._cameraBorder = new Sprite();
         this._imageContainer = new Sprite();
      }
      
      private function addEvent() : void
      {
         this._cameraBorder.addEventListener(Event.RESIZE,this.__resize);
      }
      
      private function removeEvent() : void
      {
         this._cameraBorder.removeEventListener(Event.RESIZE,this.__resize);
      }
      
      private function __resize(param1:Event) : void
      {
      }
      
      private function generateImage() : void
      {
         if(this._imageContainer.numChildren > 0)
         {
            this._imageContainer.removeChildAt(0);
         }
         if(this._bitmapData)
         {
            this._bitmapData.dispose();
            this._bitmapData = null;
         }
         this._image = null;
         var _loc1_:Rectangle = new Rectangle(x,y,width,height);
         this._bitmapData = this.drawImage(_loc1_);
         this._image = new Bitmap(this._bitmapData);
         this._imageContainer.addChild(this._image);
      }
      
      public function drawImage(param1:Rectangle) : BitmapData
      {
         var _loc2_:BitmapData = new BitmapData(param1.width,param1.height,true,0);
         _loc2_.draw(stage,null,null,null,param1);
         return _loc2_;
      }
      
      public function dispose() : void
      {
         this.removeEvent();
      }
   }
}
