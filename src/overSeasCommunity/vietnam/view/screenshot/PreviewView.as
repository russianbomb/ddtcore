package overSeasCommunity.vietnam.view.screenshot
{
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.TextInput;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.SoundManager;
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import overSeasCommunity.vietnam.ScreenshotController;
   
   public class PreviewView extends Frame
   {
       
      
      private var _borderBg:ScaleBitmapImage;
      
      private var _inputText:TextInput;
      
      private var _submitBtn:BaseButton;
      
      private var _image:Bitmap;
      
      private var _bgTitle:Bitmap;
      
      public var _controller:ScreenshotController;
      
      public function PreviewView()
      {
         super();
         this.initialize();
         this.addEvent();
      }
      
      private function initialize() : void
      {
         this._borderBg = ComponentFactory.Instance.creat("screenshot.PreviewBg");
         this._bgTitle = ComponentFactory.Instance.creatBitmap("asset.screenshot.PreviewTitle");
         this._image = ComponentFactory.Instance.creatCustomObject("screenshot.cutImage");
         var _loc1_:Sprite = new Sprite();
         _loc1_.addChild(this._borderBg);
         _loc1_.addChild(this._bgTitle);
         _loc1_.addChild(this._image);
         addToContent(_loc1_);
         this._inputText = ComponentFactory.Instance.creat("screenshot.descInput");
         this._submitBtn = ComponentFactory.Instance.creat("screenshot.submitBtn");
         var _loc2_:Sprite = new Sprite();
         _loc2_.addChild(this._inputText);
         _loc2_.addChild(this._submitBtn);
         addToContent(_loc2_);
      }
      
      private function addEvent() : void
      {
         this._submitBtn.addEventListener(MouseEvent.CLICK,this.__submitClick);
         addEventListener(FrameEvent.RESPONSE,this.__frameEventHandler);
      }
      
      private function removeEvent() : void
      {
         this._submitBtn.removeEventListener(MouseEvent.CLICK,this.__submitClick);
         removeEventListener(FrameEvent.RESPONSE,this.__frameEventHandler);
      }
      
      private function __submitClick(param1:MouseEvent) : void
      {
         this._controller.publish(this._image.bitmapData.clone(),this._inputText.text);
      }
      
      private function __frameEventHandler(param1:FrameEvent) : void
      {
         switch(param1.responseCode)
         {
            case FrameEvent.ESC_CLICK:
            case FrameEvent.CLOSE_CLICK:
               SoundManager.instance.play("008");
               this._controller.cancel();
         }
      }
      
      public function set controller(param1:ScreenshotController) : void
      {
         this._controller = param1;
      }
      
      public function show(param1:BitmapData) : void
      {
         var _loc2_:Sprite = null;
         var _loc3_:Sprite = null;
         this._image.bitmapData = param1;
         this._borderBg.width = Math.max(param1.width + 44,this._borderBg.width);
         this._borderBg.height = Math.max(param1.height + 90,this._borderBg.height);
         this._bgTitle.x = (this._borderBg.width - this._bgTitle.width) / 2;
         this._image.x = (this._borderBg.width - this._image.width) / 2 + 1;
         this._image.y = (this._borderBg.height - this._image.height) / 2 + 6;
         _loc2_ = this._image.parent as Sprite;
         _loc3_ = this._inputText.parent as Sprite;
         _loc3_.y = _loc2_.height + 8;
         _loc2_.x = (_container.width - _loc2_.width) / 2;
         _loc3_.x = (_container.width - _loc3_.width) / 2;
         width = _container.width + _containerX * 2;
         height = _container.height + _containerY * 2;
         LayerManager.Instance.addToLayer(this,LayerManager.GAME_TOP_LAYER,true,LayerManager.ALPHA_BLOCKGOUND);
      }
      
      override public function dispose() : void
      {
         super.dispose();
         this.removeEvent();
         this._controller = null;
         ObjectUtils.disposeObject(this._image);
         this._borderBg = null;
         ObjectUtils.disposeObject(this._bgTitle);
         this._bgTitle = null;
         ObjectUtils.disposeObject(this._borderBg);
         this._borderBg = null;
         ObjectUtils.disposeObject(this._inputText);
         this._inputText = null;
         ObjectUtils.disposeObject(this._submitBtn);
         this._submitBtn = null;
         ObjectUtils.disposeAllChildren(this);
         if(parent)
         {
            parent.removeChild(this);
         }
         dispatchEvent(new Event(Event.CLOSE));
      }
   }
}
