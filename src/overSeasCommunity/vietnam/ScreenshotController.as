package overSeasCommunity.vietnam
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.BitmapData;
   import flash.events.EventDispatcher;
   import overSeasCommunity.vietnam.view.screenshot.PreviewView;
   import overSeasCommunity.vietnam.view.screenshot.ScreenshotView;
   
   public class ScreenshotController extends EventDispatcher
   {
       
      
      private var _api:InterfaceController;
      
      private var _screenshotView:ScreenshotView;
      
      private var _previewView:PreviewView;
      
      public function ScreenshotController(param1:InterfaceController)
      {
         super();
         this._api = param1;
      }
      
      public function cancel() : void
      {
         this.disposeAll();
      }
      
      public function preview(param1:BitmapData) : void
      {
         if(this._screenshotView)
         {
            ObjectUtils.disposeObject(this._screenshotView);
            this._screenshotView = null;
         }
         if(!this._previewView)
         {
            this._previewView = ComponentFactory.Instance.creat("community.screenshot.PreviewView");
            this._previewView.controller = this;
         }
         this._previewView.show(param1);
      }
      
      public function show() : void
      {
         if(!this._screenshotView)
         {
            this._screenshotView = new ScreenshotView(this);
            this._screenshotView.show();
         }
         else
         {
            this.disposeAll();
         }
      }
      
      public function publish(param1:BitmapData, param2:String) : void
      {
         this._api.pushAndUpload(param1,param2);
         this.disposeAll();
      }
      
      private function disposeAll() : void
      {
         if(this._screenshotView)
         {
            ObjectUtils.disposeObject(this._screenshotView);
            this._screenshotView = null;
         }
         if(this._previewView)
         {
            ObjectUtils.disposeObject(this._previewView);
            this._previewView = null;
         }
      }
   }
}
