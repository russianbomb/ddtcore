package overSeasCommunity.vietnam.openapi
{
   import com.pickgliss.loader.LoaderManager;
   import flash.display.BitmapData;
   import flash.events.EventDispatcher;
   import flash.net.URLVariables;
   import flash.utils.Dictionary;
   import overSeasCommunity.OverSeasCommunController;
   
   [Event(name="response",type="community.openapi.ZMRequestEvent")]
   public class BaseZMOpenAPI extends EventDispatcher
   {
      
      public static const USER:int = 1;
      
      public static const SESSION:int = 2;
      
      public static const FEED:int = 3;
      
      public static const FRIEND:int = 4;
      
      public static var session_id:String;
       
      
      protected var _onResponseCall:Function;
      
      private var _requestUrl:String;
      
      public function BaseZMOpenAPI(param1:Function)
      {
         super();
         this._onResponseCall = param1;
      }
      
      protected function callMethodASync(param1:String, param2:Dictionary) : void
      {
         this._requestUrl = OverSeasCommunController.instance().vietnamCommunityInterfacePath(param1);
         this.sendRequest(param2);
      }
      
      protected function creatSeessionKey() : Dictionary
      {
         var _loc1_:Dictionary = new Dictionary();
         _loc1_["session_key"] = BaseZMOpenAPI.session_id;
         return _loc1_;
      }
      
      private function sendRequest(param1:Dictionary) : void
      {
         var _loc2_:ZMRequestLoader = new ZMRequestLoader(this._requestUrl,this.prepareData(param1));
         _loc2_.analyzer = new ZMResponseAnalyzer(this.onRequestComplete);
         LoaderManager.Instance.startLoad(_loc2_);
      }
      
      private function prepareData(param1:Dictionary) : URLVariables
      {
         var _loc3_:* = null;
         var _loc2_:URLVariables = new URLVariables();
         for(_loc3_ in param1)
         {
            _loc2_[_loc3_] = param1[_loc3_];
         }
         return _loc2_;
      }
      
      private function onRequestComplete(param1:ZMResponseAnalyzer) : void
      {
         if(this._onResponseCall != null)
         {
            this._onResponseCall(param1.UserId > 0?param1.UserId:param1.result);
         }
         dispatchEvent(new ZMRequestEvent(ZMRequestEvent.RESPONSE,param1.result));
      }
      
      protected function uploadPhoto(param1:BitmapData, param2:Dictionary) : void
      {
         var _loc3_:String = OverSeasCommunController.instance().vietnamCommunityInterfacePath("UploadPhoto");
         var _loc4_:ZMFileUploader = new ZMFileUploader(_loc3_,param1,this.prepareData(param2));
         _loc4_.analyzer = new ZMResponseAnalyzer(this.onRequestComplete);
         LoaderManager.Instance.startLoad(_loc4_);
      }
   }
}
