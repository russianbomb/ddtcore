package overSeasCommunity.vietnam.openapi
{
   public class ZMFriendAPI extends BaseZMOpenAPI
   {
       
      
      public function ZMFriendAPI(param1:Function = null)
      {
         super(param1);
      }
      
      public function getAppUsers() : void
      {
         callMethodASync("GetAppUsers",creatSeessionKey());
      }
      
      public function getLists() : void
      {
         callMethodASync("GetFriendsList",creatSeessionKey());
      }
   }
}
