package overSeasCommunity.vietnam.openapi
{
   import com.pickgliss.loader.LoaderManager;
   import com.pickgliss.loader.RequestLoader;
   import flash.net.URLVariables;
   
   public class ZMRequestLoader extends RequestLoader
   {
       
      
      public function ZMRequestLoader(param1:String, param2:URLVariables = null)
      {
         var _loc3_:int = LoaderManager.Instance.getNextLoaderID();
         super(_loc3_,param1,param2,"POST");
      }
   }
}
