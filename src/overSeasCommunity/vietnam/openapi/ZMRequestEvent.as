package overSeasCommunity.vietnam.openapi
{
   import flash.events.Event;
   
   public class ZMRequestEvent extends Event
   {
      
      public static const RESPONSE:String = "response";
       
      
      private var _result;
      
      public function ZMRequestEvent(param1:String, param2:*)
      {
         super(param1);
         this._result = param2;
      }
      
      public function get result() : *
      {
         return this._result;
      }
   }
}
