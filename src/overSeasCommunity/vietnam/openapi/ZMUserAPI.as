package overSeasCommunity.vietnam.openapi
{
   import flash.utils.Dictionary;
   
   public class ZMUserAPI extends BaseZMOpenAPI
   {
       
      
      public function ZMUserAPI(param1:Function = null)
      {
         super(param1);
      }
      
      public function getUserIdByUsername(param1:String) : void
      {
         var _loc2_:Dictionary = new Dictionary();
         _loc2_["users"] = param1;
         callMethodASync("GetUserIdByUsername",_loc2_);
      }
      
      public function getInfo(param1:String, param2:String) : void
      {
         var _loc3_:Dictionary = new Dictionary();
         _loc3_["ids"] = param1;
         _loc3_["fields"] = param2;
         callMethodASync("GetInfo",_loc3_);
      }
      
      public function getInfoByUsername(param1:String, param2:String) : void
      {
         var _loc3_:Dictionary = new Dictionary();
         _loc3_["users"] = param1;
         callMethodASync("GetInfoForJson",_loc3_);
      }
   }
}
