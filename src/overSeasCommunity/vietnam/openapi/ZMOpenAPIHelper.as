package overSeasCommunity.vietnam.openapi
{
   import flash.net.URLVariables;
   
   public class ZMOpenAPIHelper
   {
       
      
      public function ZMOpenAPIHelper()
      {
         super();
      }
      
      public static function getKeys(param1:*) : Array
      {
         var _loc3_:* = null;
         var _loc2_:Array = new Array();
         for(_loc3_ in param1)
         {
            _loc2_.push(_loc3_);
         }
         return _loc2_;
      }
      
      public static function solveRequestPath(param1:String, param2:URLVariables) : String
      {
         var _loc4_:* = null;
         var _loc3_:String = "?";
         for(_loc4_ in param2)
         {
            _loc3_ = _loc3_ + (_loc4_ + "=" + param2[_loc4_] + "&");
         }
         return param1 + _loc3_;
      }
   }
}
