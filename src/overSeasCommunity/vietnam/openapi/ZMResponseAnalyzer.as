package overSeasCommunity.vietnam.openapi
{
import com.adobe.serialization.json.JSON;
import com.pickgliss.loader.DataAnalyzer;
   
   public class ZMResponseAnalyzer extends DataAnalyzer
   {
       
      
      public var result;
      
      public var UserId:int;
      
      public function ZMResponseAnalyzer(param1:Function)
      {
         super(param1);
      }
      
      override public function analyze(param1:*) : void
      {
         var json:* = undefined;
         var data:* = param1;
         try
         {
            json = JSON.decode(data);
         }
         catch(e:Error)
         {
            json = {
               "error_code":0,
               "error_message":e.message
            };
         }
         var error_code:String = json["error_code"];
         if(error_code == "0")
         {
            this.result = json["data"];
            if(this.result.hasOwnProperty("uid"))
            {
               this.UserId = Number(this.result["uid"]);
            }
            onAnalyzeComplete();
         }
         else
         {
            message = json["error_message"];
            onAnalyzeError();
         }
      }
   }
}
