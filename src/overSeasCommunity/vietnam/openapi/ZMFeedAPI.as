package overSeasCommunity.vietnam.openapi
{
import com.adobe.serialization.json.JSON;

import flash.display.BitmapData;
   import flash.utils.Dictionary;
   import overSeasCommunity.vietnam.data.FeedItem;
   
   public class ZMFeedAPI extends BaseZMOpenAPI
   {
       
      
      public var callbackArgs;
      
      public function ZMFeedAPI(param1:Function = null)
      {
         super(param1);
      }
      
      public function sendEmail(param1:Array, param2:String, param3:String) : void
      {
         var _loc4_:Dictionary = new Dictionary();
         _loc4_["recipients"] = JSON.encode(param1);
         _loc4_["subject"] = param2;
         _loc4_["text"] = param3;
         callMethodASync("SendEmailToUsers",_loc4_);
      }
      
      public function sendEmailByTemplate(param1:Array, param2:String, param3:Object) : void
      {
         var _loc4_:Dictionary = new Dictionary();
         _loc4_["recipients"] = JSON.encode(param1);
         _loc4_["template_bundle_id"] = param2;
         _loc4_["template_data"] = JSON.encode(param3);
         callMethodASync("SendEmailByTemplate",_loc4_);
      }
      
      public function publish(param1:int, param2:Object) : void
      {
         var _loc3_:Dictionary = creatSeessionKey();
         _loc3_["template_bundle_id"] = String(param1);
         _loc3_["template_data"] = JSON.encode(param2);
         callMethodASync("PublishUserAction",_loc3_);
      }
      
      public function pushAndUpload(param1:BitmapData, param2:String) : void
      {
         var _loc3_:Dictionary = creatSeessionKey();
         _loc3_["template_bundle_id"] = 195;
         _loc3_["description"] = escape(param2);
         uploadPhoto(param1,_loc3_);
      }
      
      public function creatSignKey(param1:FeedItem, param2:*) : void
      {
         this.callbackArgs = param2;
         var _loc3_:Dictionary = new Dictionary();
         _loc3_["json"] = JSON.encode(param1);
         callMethodASync("SignkeyForJson",_loc3_);
      }
   }
}
