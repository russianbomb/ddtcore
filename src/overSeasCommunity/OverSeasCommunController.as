package overSeasCommunity
{
   import com.pickgliss.manager.CacheSysManager;
   import com.pickgliss.ui.ComponentFactory;
   import ddt.action.FrameShowAction;
   import ddt.constants.CacheConsts;
   import ddt.data.player.PlayerInfo;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.AcademyManager;
   import ddt.manager.PathManager;
   import ddt.manager.SharedManager;
   import ddt.manager.SocketManager;
   import flash.events.EventDispatcher;
   import flash.net.URLRequest;
   import flash.net.URLVariables;
   import flash.net.sendToURL;
   import game.model.GameInfo;
   import overSeasCommunity.overseas.controllers.BaseCommunityController;
   import overSeasCommunity.overseas.controllers.EasyGameController;
   import overSeasCommunity.overseas.controllers.ElexController;
   import overSeasCommunity.overseas.model.BaseCommunityModel;
   import overSeasCommunity.overseas.view.CommunityFrame;
   import overSeasCommunity.overseas.vo.OverSeasCommunType;
   import overSeasCommunity.vietnam.CommunityManager;
   
   public class OverSeasCommunController extends EventDispatcher
   {
      
      private static var _instance:OverSeasCommunController;
       
      
      private var _debug:Boolean = false;
      
      private var _communityType:int = 0;
      
      private var _model:BaseCommunityModel;
      
      private var _control:BaseCommunityController;
      
      public function OverSeasCommunController()
      {
         super();
      }
      
      public static function instance() : OverSeasCommunController
      {
         if(!_instance)
         {
            _instance = new OverSeasCommunController();
         }
         return _instance;
      }
      
      public function get model() : BaseCommunityModel
      {
         return this._model;
      }
      
      public function get communityType() : int
      {
         return this._communityType;
      }
      
      public function setup() : void
      {
         this._communityType = PathManager.OVERSEAS_COMMUNITY_TYPE;
         if(this._communityType == OverSeasCommunType.VIETNAME_COMMUNITY)
         {
            CommunityManager.Instance.setup();
            return;
         }
         if(!this._debug && this._communityType != OverSeasCommunType.VIETNAME_COMMUNITY)
         {
            SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GET_DYNAMIC,this.__overSeasCommun);
         }
         else
         {
            SocketManager.Instance.addEventListener("TestOverSeasCommun",this.__overSeasCommun);
         }
      }
      
      private function __overSeasCommun(param1:CrazyTankSocketEvent) : void
      {
         switch(this._communityType)
         {
            case OverSeasCommunType.NONE_COMMUNITY:
               break;
            case OverSeasCommunType.COMMON_COMMUNITY:
               this._model = new BaseCommunityModel();
               this._control = new BaseCommunityController(this._model);
               break;
            case OverSeasCommunType.VIETNAME_COMMUNITY:
               break;
            case OverSeasCommunType.ELEX_COMMUNITY:
               this._model = new BaseCommunityModel();
               this._control = new ElexController(this._model);
               this._control.isShowFrame = false;
               break;
            case OverSeasCommunType.EASYGAME_COMMUNITY:
               this._model = new BaseCommunityModel();
               this._control = new EasyGameController(this._model);
         }
         if(SharedManager.Instance.isCommunity)
         {
            if(!this._debug)
            {
               this._model.typeId = param1.pkg.readInt();
               this._model.backgroundServerTxt = param1.pkg.readUTF();
               this._model.receptionistTxt = param1.pkg.readUTF();
            }
            if(this._control.openTypeIDList.indexOf(this._model.typeId) != -1)
            {
               if(this._control.isShowFrame)
               {
                  this.showDialog();
               }
               else
               {
                  this._control.sendDynamic();
               }
            }
         }
      }
      
      private function showDialog() : void
      {
         var _loc1_:CommunityFrame = ComponentFactory.Instance.creatComponentByStylename("core.CommunityFrameView");
         _loc1_.model = this._model;
         _loc1_.control = this._control;
         _loc1_.receptionistTxt = this._model.receptionistTxt;
         if(CacheSysManager.isLock(CacheConsts.ALERT_IN_FIGHT))
         {
            CacheSysManager.getInstance().cache(CacheConsts.ALERT_IN_FIGHT,new FrameShowAction(_loc1_));
            return;
         }
         if(CacheSysManager.isLock(CacheConsts.ALERT_IN_MOVIE))
         {
            CacheSysManager.getInstance().cache(CacheConsts.ALERT_IN_MOVIE,new FrameShowAction(_loc1_));
            return;
         }
         if(AcademyManager.Instance.isFighting())
         {
            return;
         }
         _loc1_.show();
      }
      
      public function vietnamCommunityInterfacePath(param1:String) : String
      {
         return PathManager.vietnamCommunityInterfacePath(param1);
      }
      
      public function elexOverseasCommunity_CallJS() : String
      {
         return PathManager.OVERSEAS_COMMUNITY_CALLJS;
      }
      
      public function sendToAgent(param1:int, param2:int = -1, param3:String = "", param4:String = "", param5:int = -1, param6:String = "", param7:String = "") : void
      {
         var _loc8_:URLRequest = new URLRequest(PathManager.getSnsPath());
         var _loc9_:URLVariables = new URLVariables();
         _loc9_["op"] = param1;
         if(param2 > -1)
         {
            _loc9_["uid"] = param2;
         }
         if(param3 != "")
         {
            _loc9_["role"] = param3;
         }
         if(param4 != "")
         {
            _loc9_["ser"] = param4;
         }
         if(param5 > -1)
         {
            _loc9_["num"] = param5;
         }
         if(param6 != "")
         {
            _loc9_["pn"] = param6;
         }
         if(param7 != "")
         {
            _loc9_["role2"] = param7;
         }
         _loc8_.data = _loc9_;
         sendToURL(_loc8_);
      }
      
      public function isVNG() : Boolean
      {
         return OverSeasCommunController.instance().communityType != OverSeasCommunType.VIETNAME_COMMUNITY?Boolean(false):Boolean(true);
      }
      
      public function activeVNG() : Boolean
      {
         return this.isVNG() && CommunityManager.Instance.enable;
      }
      
      public function checkKillHeroBoss() : void
      {
         if(OverSeasCommunController.instance().communityType != OverSeasCommunType.VIETNAME_COMMUNITY)
         {
            return;
         }
         if(!CommunityManager.Instance.enable)
         {
            return;
         }
         if(CommunityManager.Instance.isHeroBossLevel())
         {
            CommunityManager.Instance.shareHeroMission();
         }
      }
      
      public function setGameInfo(param1:GameInfo, param2:int) : void
      {
         if(this.isVNG())
         {
            CommunityManager.Instance.CurrentGame = param1;
            CommunityManager.Instance.CurrentLevel = param2;
         }
      }
      
      public function setBoosName(param1:String) : void
      {
         if(this.isVNG())
         {
            if(CommunityManager.Instance.enable)
            {
               CommunityManager.Instance.currentBossName = param1;
            }
         }
      }
      
      public function shareEffort(param1:String) : void
      {
         if(this.isVNG())
         {
            if(CommunityManager.Instance.enable)
            {
               CommunityManager.Instance.shareEffort(param1);
            }
         }
      }
      
      public function shareWedding() : void
      {
         if(!this.isVNG())
         {
            return;
         }
         if(CommunityManager.Instance.enable)
         {
            CommunityManager.Instance.shareWedding();
         }
      }
      
      public function isVisibleShareBtn() : Boolean
      {
         return this.isVNG() && CommunityManager.Instance.enable;
      }
      
      public function checkShareResume(param1:PlayerInfo) : void
      {
         if(!this.isVNG())
         {
            return;
         }
         if(CommunityManager.Instance.enable)
         {
            CommunityManager.Instance.checkShareResume(param1);
         }
      }
      
      public function screenShot() : void
      {
         if(this.activeVNG())
         {
            CommunityManager.Instance.screenshot();
         }
      }
   }
}
