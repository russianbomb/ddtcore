package overSeasCommunity.overseas.vo
{
   public class OverSeasCommunType
   {
      
      public static const NONE_COMMUNITY:int = 0;
      
      public static const COMMON_COMMUNITY:int = 1;
      
      public static const VIETNAME_COMMUNITY:int = 2;
      
      public static const ELEX_COMMUNITY:int = 3;
      
      public static const EASYGAME_COMMUNITY:int = 4;
       
      
      public function OverSeasCommunType()
      {
         super();
      }
   }
}
