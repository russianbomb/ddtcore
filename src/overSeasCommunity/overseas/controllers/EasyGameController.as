package overSeasCommunity.overseas.controllers
{
   import ddt.data.player.SelfInfo;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerManager;
   import overSeasCommunity.OverSeasCommunController;
   import overSeasCommunity.overseas.model.BaseCommunityModel;
   
   public class EasyGameController extends BaseCommunityController
   {
       
      
      public function EasyGameController(param1:BaseCommunityModel)
      {
         super(param1);
         openTypeIDList = [1,2,11,14,15,16,17,18,19];
      }
      
      override public function sendDynamic() : void
      {
         this.getFeedParam(_model.typeId);
         super.sendDynamic();
      }
      
      override protected function getFeedParam(param1:int) : *
      {
         var _loc2_:SelfInfo = PlayerManager.Instance.Self;
         switch(param1)
         {
            case 1:
               OverSeasCommunController.instance().sendToAgent(2,_loc2_.ID,_loc2_.NickName,ServerManager.Instance.current.Name,_loc2_.Grade);
               break;
            case 2:
               OverSeasCommunController.instance().sendToAgent(3,_loc2_.ID,_loc2_.NickName,ServerManager.Instance.current.Name,int(_model.backgroundServerTxt));
               break;
            case 11:
               OverSeasCommunController.instance().sendToAgent(10,_loc2_.ID,_loc2_.NickName,ServerManager.Instance.current.Name);
               break;
            case 13:
               OverSeasCommunController.instance().sendToAgent(1,_loc2_.ID,_loc2_.NickName,ServerManager.Instance.current.Name);
               break;
            case 15:
               OverSeasCommunController.instance().sendToAgent(9,_loc2_.ID,_loc2_.NickName,ServerManager.Instance.current.Name,-1,_model.backgroundServerTxt);
               break;
            case 16:
               OverSeasCommunController.instance().sendToAgent(4,_loc2_.ID,_loc2_.NickName,ServerManager.Instance.current.Name,-1,PlayerManager.Instance.Self.ConsortiaName);
               break;
            case 17:
               OverSeasCommunController.instance().sendToAgent(5,_loc2_.ID,_loc2_.NickName,ServerManager.Instance.current.Name,-1,PlayerManager.Instance.Self.ConsortiaName);
               break;
            case 18:
               OverSeasCommunController.instance().sendToAgent(7,_loc2_.ID,_loc2_.NickName,ServerManager.Instance.current.Name,-1,"",PlayerManager.Instance.Self.SpouseName);
               break;
            case 19:
               OverSeasCommunController.instance().sendToAgent(8,_loc2_.ID,_loc2_.NickName,ServerManager.Instance.current.Name,-1,"",PlayerManager.Instance.Self.SpouseName);
         }
         return null;
      }
   }
}
