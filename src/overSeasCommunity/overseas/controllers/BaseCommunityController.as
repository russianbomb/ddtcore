package overSeasCommunity.overseas.controllers
{
   import ddt.manager.LanguageMgr;
   import ddt.manager.SocketManager;
   import flash.events.EventDispatcher;
   import overSeasCommunity.overseas.model.BaseCommunityModel;
   
   public class BaseCommunityController extends EventDispatcher
   {
       
      
      protected var _model:BaseCommunityModel;
      
      protected var _openTypeIDList:Array;
      
      private var _isShowFrame:Boolean = true;
      
      public function BaseCommunityController(param1:BaseCommunityModel)
      {
         super();
         this._model = param1;
         this._openTypeIDList = [1,2,11,14,15,16,17,18,19];
      }
      
      public function get isShowFrame() : Boolean
      {
         return this._isShowFrame;
      }
      
      public function set isShowFrame(param1:Boolean) : void
      {
         this._isShowFrame = param1;
      }
      
      public function get openTypeIDList() : Array
      {
         return this._openTypeIDList;
      }
      
      public function set openTypeIDList(param1:Array) : void
      {
         this._openTypeIDList = param1;
      }
      
      public function sendDynamic() : void
      {
         SocketManager.Instance.out.sendSnsMsg(this._model.typeId);
      }
      
      protected function getFeedParam(param1:int) : *
      {
      }
      
      public function getSayStr() : String
      {
         var _loc1_:String = "";
         switch(this._model.typeId)
         {
            case 1:
               _loc1_ = LanguageMgr.GetTranslation("ddt.view.SnsFrame.inputTextI");
               break;
            case 2:
               _loc1_ = LanguageMgr.GetTranslation("ddt.view.SnsFrame.inputTextII");
               break;
            case 3:
               _loc1_ = LanguageMgr.GetTranslation("ddt.view.SnsFrame.inputTextIII");
               break;
            case 4:
            case 6:
            case 7:
            case 8:
               _loc1_ = LanguageMgr.GetTranslation("ddt.view.SnsFrame.inputTextIV");
               break;
            case 5:
               _loc1_ = LanguageMgr.GetTranslation("ddt.view.SnsFrame.inputTextV");
               break;
            case 9:
               _loc1_ = LanguageMgr.GetTranslation("ddt.view.SnsFrame.inputTextVI");
               break;
            case 10:
               _loc1_ = LanguageMgr.GetTranslation("ddt.view.SnsFrame.inputTextVII");
               break;
            case 11:
               _loc1_ = LanguageMgr.GetTranslation("ddt.view.SnsFrame.inputTextVIII");
               break;
            default:
               _loc1_ = LanguageMgr.GetTranslation("ddt.view.SnsFrame.inputTextIV");
         }
         return _loc1_;
      }
   }
}
