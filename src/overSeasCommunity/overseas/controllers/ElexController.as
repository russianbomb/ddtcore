package overSeasCommunity.overseas.controllers
{
import com.adobe.serialization.json.JSON;

import ddt.manager.LanguageMgr;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerManager;
   import flash.external.ExternalInterface;
   import overSeasCommunity.OverSeasCommunController;
   import overSeasCommunity.overseas.model.BaseCommunityModel;
   
   public class ElexController extends BaseCommunityController
   {
       
      
      public function ElexController(param1:BaseCommunityModel)
      {
         super(param1);
         openTypeIDList = [1,2,11,14,15,16,17,18,19];
      }
      
      override public function sendDynamic() : void
      {
         if(ExternalInterface.available && OverSeasCommunController.instance().elexOverseasCommunity_CallJS())
         {
            ExternalInterface.call(OverSeasCommunController.instance().elexOverseasCommunity_CallJS(),OverSeasCommunController.instance().model.typeId,PlayerManager.Instance.Self.NickName,"Мой Мир",this.getFeedParam(OverSeasCommunController.instance().model.typeId));
         }
         super.sendDynamic();
      }
      
      override protected function getFeedParam(param1:int) : *
      {
         var _loc2_:String = new String();
         switch(param1)
         {
            case 1:
               _loc2_ = LanguageMgr.GetTranslation("share.feed.GradeUp.Title_1",PlayerManager.Instance.Self.Grade + 1);
               break;
            case 2:
               _loc2_ = LanguageMgr.GetTranslation("share.feed.Strengthen.Title_1",_model.backgroundServerTxt,ServerManager.Instance.current.Name);
               break;
            case 11:
               _loc2_ = "Побег из курятника (высок.)";
               break;
            case 14:
               _loc2_ = "";
               break;
            case 15:
               _loc2_ = LanguageMgr.GetTranslation("share.feed.HardFB.Title_1",_model.backgroundServerTxt);
               break;
            case 16:
               _loc2_ = LanguageMgr.GetTranslation("share.feed.CreatConsortia.Title_1",PlayerManager.Instance.Self.ConsortiaName);
               break;
            case 17:
               _loc2_ = LanguageMgr.GetTranslation("share.feed.JoinConsortia.Title_1",PlayerManager.Instance.Self.ConsortiaName);
               break;
            case 18:
               _loc2_ = "";
               break;
            case 19:
               _loc2_ = LanguageMgr.GetTranslation("share.feed.Wedding.Title_1",PlayerManager.Instance.Self.SpouseName);
         }
         return JSON.encode(_loc2_);
      }
   }
}
