package ddtActivityIcon
{
   import dayActivity.data.DayActiveData;
   import ddt.manager.LanguageMgr;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerConfigManager;
   import ddt.manager.TimeManager;
   import flash.events.EventDispatcher;
   import flash.events.IEventDispatcher;
   import flash.events.TimerEvent;
   import flash.utils.Timer;
   import lanternriddles.event.LanternEvent;
   import road7th.data.DictionaryData;
   import road7th.utils.DateUtils;
   
   public class DdtActivityIconManager extends EventDispatcher
   {
      
      private static var _instance:DdtActivityIconManager;
      
      private static const MINI:int = 1000 * 10;
      
      private static const HOURS:int = 1000 * 60 * 60;
      
      public static const READY_START:String = "ready_start";
      
      public static const START:String = "start";
      
      private static const NO_START:String = "no_start";
       
      
      private var _timerList:Vector.<DayActiveData>;
      
      private var _timer:Timer;
      
      private var todayActList:Array;
      
      private var _isOneOpen:Boolean;
      
      private var _isAlreadyOpen:Boolean;
      
      public var currObj:Array;
      
      public function DdtActivityIconManager(param1:IEventDispatcher = null)
      {
         super(param1);
         this._timer = new Timer(MINI);
         this._timer.addEventListener(TimerEvent.TIMER,this.timerHander);
      }
      
      public static function get Instance() : DdtActivityIconManager
      {
         if(_instance == null)
         {
            _instance = new DdtActivityIconManager();
         }
         return _instance;
      }
      
      private function timerHander(param1:TimerEvent) : void
      {
         this.checkTodayActList();
         this.checkSpecialActivity();
      }
      
      private function checkSpecialActivity() : void
      {
         var _loc7_:String = null;
         if(this._isAlreadyOpen)
         {
            return;
         }
         var _loc1_:Date = TimeManager.Instance.Now();
         var _loc2_:String = _loc1_.getFullYear().toString() + "/" + (_loc1_.month + 1).toString() + "/" + _loc1_.date.toString() + " ";
         var _loc3_:DictionaryData = ServerConfigManager.instance.serverConfigInfo;
         var _loc4_:Date = DateUtils.getDateByStr(_loc3_["LightRiddleBeginDate"].Value);
         var _loc5_:Date = DateUtils.getDateByStr(this.transformTime(_loc2_,_loc3_["LightRiddleBeginTime"].Value));
         var _loc6_:Date = DateUtils.getDateByStr(_loc3_["LightRiddleEndDate"].Value);
         if(PlayerManager.Instance.Self.Grade >= 15)
         {
            if(_loc1_.time > _loc4_.time && _loc1_.time < _loc6_.time)
            {
               if(_loc5_.time > _loc1_.time && _loc5_.time - _loc1_.time <= HOURS)
               {
                  this._isAlreadyOpen = true;
                  _loc7_ = this.addO(_loc5_.hours) + ":" + this.addO(_loc5_.minutes) + LanguageMgr.GetTranslation("ddt.activityIcon.timePreStartTxt");
                  dispatchEvent(new LanternEvent(LanternEvent.LANTERN_SETTIME,_loc7_));
               }
            }
         }
      }
      
      private function transformTime(param1:String, param2:String) : String
      {
         var _loc3_:Array = param2.split(" ");
         return param1 + _loc3_[1];
      }
      
      public function set timerList(param1:Vector.<DayActiveData>) : void
      {
         this._timerList = param1;
      }
      
      public function setup() : void
      {
         this._timer.reset();
         this.initToDayActivie();
         this._timer.start();
      }
      
      public function startTime() : void
      {
         this._timer.start();
      }
      
      public function stopTime() : void
      {
         this._timer.stop();
      }
      
      private function initToDayActivie() : Array
      {
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         if(this._timerList)
         {
            this.todayActList = [];
            _loc1_ = this._timerList.length;
            _loc2_ = 0;
            while(_loc2_ < _loc1_)
            {
               if(this.checkToday(this._timerList[_loc2_].DayOfWeek))
               {
                  if(!(this._timerList[_loc2_].ID == 5 && int(this._timerList[_loc2_].LevelLimit) > PlayerManager.Instance.Self.Grade))
                  {
                     if(!(this._timerList[_loc2_].ID == 4 && int(this._timerList[_loc2_].LevelLimit) > PlayerManager.Instance.Self.Grade))
                     {
                        this.todayActList.push(this.strToDataArray(this._timerList[_loc2_].ActiveTime,this._timerList[_loc2_].ID));
                     }
                  }
               }
               _loc2_++;
            }
         }
         return this.todayActList;
      }
      
      public function get isOneOpen() : Boolean
      {
         return this._isOneOpen;
      }
      
      public function set isOneOpen(param1:Boolean) : void
      {
         this._isOneOpen = param1;
      }
      
      private function checkTodayActList() : void
      {
         var _loc5_:Object = null;
         var _loc6_:Date = null;
         var _loc7_:Object = null;
         var _loc8_:Object = null;
         var _loc9_:Date = null;
         var _loc10_:Object = null;
         if(!this.todayActList)
         {
            return;
         }
         var _loc1_:int = this.todayActList.length;
         var _loc2_:Date = TimeManager.Instance.Now();
         var _loc3_:int = 0;
         while(_loc3_ < _loc1_)
         {
            _loc5_ = this.todayActList[_loc3_][0];
            _loc6_ = _loc5_.end;
            this.checkActOpen(_loc5_);
            if(this.todayActList[_loc3_][1] && _loc2_.time > _loc6_.time)
            {
               _loc7_ = this.todayActList[_loc3_][1];
               this.checkActOpen(_loc7_);
            }
            _loc3_++;
         }
         if(this._isOneOpen)
         {
            return;
         }
         var _loc4_:int = 0;
         while(_loc4_ < _loc1_)
         {
            _loc8_ = this.todayActList[_loc4_][0];
            _loc9_ = _loc8_.end;
            this.checkActReady(_loc8_);
            if(this.todayActList[_loc4_][1] && _loc2_.time > _loc9_.time)
            {
               _loc10_ = this.todayActList[_loc4_][1];
               this.checkActReady(_loc10_);
            }
            _loc4_++;
         }
      }
      
      private function checkToday(param1:String) : Boolean
      {
         var _loc2_:Date = TimeManager.Instance.Now();
         var _loc3_:Array = param1.split(",");
         var _loc4_:int = _loc3_.length;
         var _loc5_:int = 0;
         while(_loc5_ < _loc4_)
         {
            if(_loc2_.day == int(_loc3_[_loc5_]))
            {
               return true;
            }
            _loc5_++;
         }
         return false;
      }
      
      private function checkActOpen(param1:Object) : void
      {
         var _loc6_:int = 0;
         var _loc7_:String = null;
         var _loc2_:Date = TimeManager.Instance.Now();
         var _loc3_:Date = param1.start;
         var _loc4_:Date = param1.end;
         var _loc5_:int = _loc3_.time - _loc2_.time;
         if(_loc2_.time >= _loc3_.time && _loc2_.time < _loc4_.time)
         {
            if(param1.name == 1)
            {
               _loc6_ = 2;
            }
            else if(param1.name == 2)
            {
               _loc6_ = 1;
            }
            else if(param1.name == 19)
            {
               _loc6_ = 4;
            }
            else
            {
               _loc6_ = -100;
            }
            this._isOneOpen = true;
            this.currObj = new Array();
            this.currObj.push(param1.name);
            this.currObj.push(_loc6_);
            _loc7_ = this.addO(_loc3_.hours) + ":" + this.addO(_loc3_.minutes) + "Zaten açıldı";
            this.currObj.push(_loc7_);
            dispatchEvent(new ActivitStateEvent(DdtActivityIconManager.START,[param1.name,_loc6_,_loc7_]));
         }
      }
      
      private function checkActReady(param1:Object) : void
      {
         var _loc6_:int = 0;
         var _loc7_:String = null;
         var _loc2_:Date = TimeManager.Instance.Now();
         var _loc3_:Date = param1.start;
         var _loc4_:Date = param1.end;
         var _loc5_:int = _loc3_.time - _loc2_.time;
         if(_loc3_.time > _loc2_.time && _loc5_ < HOURS)
         {
            if(param1.name == 1)
            {
               _loc6_ = 2;
            }
            else if(param1.name == 2)
            {
               _loc6_ = 1;
            }
            else if(param1.name == 19)
            {
               _loc6_ = 4;
            }
            else
            {
               _loc6_ = -100;
            }
            this._isOneOpen = false;
            _loc7_ = this.addO(_loc3_.hours) + ":" + this.addO(_loc3_.minutes);
            dispatchEvent(new ActivitStateEvent(DdtActivityIconManager.READY_START,[param1.name,_loc6_,_loc7_]));
         }
      }
      
      private function addO(param1:Number) : String
      {
         var _loc2_:String = param1.toString();
         if(_loc2_.length == 1)
         {
            _loc2_ = "0" + _loc2_;
         }
         return _loc2_;
      }
      
      private function strToDataArray(param1:String, param2:int) : Array
      {
         var _loc10_:String = null;
         var _loc11_:String = null;
         var _loc12_:Date = null;
         var _loc13_:Date = null;
         var _loc14_:Object = null;
         var _loc3_:Array = [];
         var _loc4_:Array = param1.split("|");
         var _loc5_:String = _loc4_[0].substr(0,5);
         var _loc6_:String = _loc4_[0].substr(6,5);
         var _loc7_:Date = this.strToDate(_loc5_);
         var _loc8_:Date = this.strToDate(_loc6_);
         var _loc9_:Object = new Object();
         _loc9_.start = _loc7_;
         _loc9_.end = _loc8_;
         _loc9_.name = param2;
         _loc3_.push(_loc9_);
         if(_loc4_[1])
         {
            _loc10_ = _loc4_[1].substr(0,5);
            _loc11_ = _loc4_[1].substr(6,5);
            _loc12_ = this.strToDate(_loc10_);
            _loc13_ = this.strToDate(_loc11_);
            _loc14_ = new Object();
            _loc14_.start = _loc12_;
            _loc14_.end = _loc13_;
            _loc14_.name = param2;
            _loc3_.push(_loc14_);
         }
         return _loc3_;
      }
      
      private function strToDate(param1:String) : Date
      {
         var _loc2_:Date = TimeManager.Instance.Now();
         var _loc3_:Number = _loc2_.fullYear;
         var _loc4_:int = _loc2_.month;
         var _loc5_:Number = _loc2_.date;
         var _loc6_:String = param1.substr(0,2);
         var _loc7_:String = param1.substr(3,2);
         var _loc8_:Date = new Date(_loc3_,_loc4_,_loc5_,_loc6_,_loc7_);
         return _loc8_;
      }
      
      public function set isAlreadyOpen(param1:Boolean) : void
      {
         this._isAlreadyOpen = param1;
      }
   }
}
