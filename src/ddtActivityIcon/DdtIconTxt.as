package ddtActivityIcon
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import consortionBattle.view.ConsortiaBattleEntryBtn;
   import flash.display.Sprite;
   import worldboss.view.WorldBossIcon;
   
   public class DdtIconTxt extends Sprite
   {
       
      
      private var _txt:FilterFrameText;
      
      private var _isOver:Boolean;
      
      private var _sp:Sprite;
      
      private var _isOpen:Boolean;
      
      public function DdtIconTxt()
      {
         super();
         this.initView();
      }
      
      public function addActIcon(param1:Sprite) : void
      {
         this._sp = param1;
         if(param1 is WorldBossIcon)
         {
            param1.y = 127;
            param1.x = 33;
            addChild(param1);
            return;
         }
         if(param1 is ConsortiaBattleEntryBtn)
         {
            param1.x = 34;
            param1.y = 167;
            addChild(param1);
            return;
         }
         addChild(param1);
         param1.y = this._txt.y - param1.height;
         param1.x = 100 / 2 - param1.width / 2 + this._txt.x - 12;
         this.isOver = true;
      }
      
      public function resetPos() : void
      {
         this._sp.x = this._sp.y = 0;
         this._txt.x = this._sp.x + this._sp.width / 2 - this._txt.width / 2;
         this._txt.y = this._sp.y + this._sp.height;
      }
      
      public function set isOver(param1:Boolean) : void
      {
         this._isOver = param1;
      }
      
      public function get isOver() : Boolean
      {
         return this._isOver;
      }
      
      public function set isOpen(param1:Boolean) : void
      {
         this._isOpen = param1;
      }
      
      public function get isOpen() : Boolean
      {
         return this._isOpen;
      }
      
      private function initView() : void
      {
         this._txt = ComponentFactory.Instance.creatComponentByStylename("activity.txt");
         addChild(this._txt);
      }
      
      public function setTxt(param1:String) : void
      {
         this._txt.text = param1;
      }
      
      public function resetTxt() : void
      {
         if(this._txt)
         {
            this._txt.text = "";
         }
         this._isOver = false;
         this._isOpen = false;
         DdtActivityIconManager.Instance.isOneOpen = false;
         ObjectUtils.disposeObject(this._sp);
         this._sp = null;
      }
      
      public function dispose() : void
      {
         this._isOver = false;
         while(numChildren)
         {
            ObjectUtils.disposeObject(getChildAt(0));
         }
         this._sp = null;
         this._txt = null;
      }
      
      public function get sp() : Sprite
      {
         return this._sp;
      }
   }
}
