package flashP2P.stream
{
   import flash.events.NetStatusEvent;
   import flash.net.NetConnection;
   import flash.net.NetStream;
   
   public class SendStream extends NetStream
   {
       
      
      public const DDT_P2P_PUBLISH_NAME:String = "DDT-P2P-PUBLISH-NAME";
      
      private var _playerID:int;
      
      public function SendStream(param1:NetConnection, param2:String = "connectToFMS")
      {
         super(param1,param2);
         addEventListener(NetStatusEvent.NET_STATUS,this.__onStreamHandler);
         this.init();
      }
      
      protected function init() : void
      {
         publish(this.DDT_P2P_PUBLISH_NAME);
      }
      
      protected function __onStreamHandler(param1:NetStatusEvent) : void
      {
         switch(param1.info.code)
         {
            case "NetStream.Publish.Start":
         }
      }
   }
}
