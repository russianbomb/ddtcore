package flashP2P.stream
{
   import flash.events.NetStatusEvent;
   import flash.net.NetConnection;
   import flash.net.NetStream;
   import flash.utils.ByteArray;
   import flashP2P.event.StreamEvent;
   
   public class ReadStream extends NetStream
   {
       
      
      public const DDT_P2P_PUBLISH_NAME:String = "DDT-P2P-PUBLISH-NAME";
      
      private var _playerID:int;
      
      public function ReadStream(param1:NetConnection, param2:int, param3:String = "connectToFMS")
      {
         super(param1,param3);
         this._playerID = param2;
         addEventListener(NetStatusEvent.NET_STATUS,this.__onStreamHandler);
         this.init();
      }
      
      protected function init() : void
      {
         publish(this.DDT_P2P_PUBLISH_NAME);
      }
      
      protected function __onStreamHandler(param1:NetStatusEvent) : void
      {
         switch(param1.info.code)
         {
            case "NetStream.Publish.Start":
         }
      }
      
      public function get playerID() : int
      {
         return this._playerID;
      }
      
      public function readByteArray(param1:String, param2:ByteArray) : void
      {
         dispatchEvent(new StreamEvent(StreamEvent.DEFAULT_EVENT,param1,param2));
      }
   }
}
