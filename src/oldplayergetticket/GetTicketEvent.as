package oldplayergetticket
{
   import flash.events.Event;
   
   public class GetTicketEvent extends Event
   {
      
      public static const GETTICKET_DATA:String = "getTicket_data";
       
      
      public var money:int;
      
      public var level:int;
      
      public var levelMoney:int;
      
      public function GetTicketEvent(param1:String, param2:Boolean = false, param3:Boolean = false)
      {
         super(param1,param2,param3);
      }
   }
}
