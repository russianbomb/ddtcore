package dragonBoat.dataAnalyzer
{
   import com.pickgliss.loader.DataAnalyzer;
   import com.pickgliss.utils.ObjectUtils;
   import dragonBoat.data.DragonBoatActiveInfo;
   import dragonBoat.data.DragonBoatAwardInfo;
   
   public class DragonBoatActiveDataAnalyzer extends DataAnalyzer
   {
       
      
      private var _data:DragonBoatActiveInfo;
      
      private var _dataList:Array;
      
      private var _dataListSelf:Array;
      
      private var _dataListOther:Array;
      
      public function DragonBoatActiveDataAnalyzer(param1:Function)
      {
         super(param1);
      }
      
      override public function analyze(param1:*) : void
      {
         var _loc3_:XMLList = null;
         var _loc4_:int = 0;
         var _loc5_:DragonBoatAwardInfo = null;
         var _loc2_:XML = new XML(param1);
         this._data = new DragonBoatActiveInfo();
         this._dataList = [];
         this._dataListSelf = [];
         this._dataListOther = [];
         if(_loc2_.@value == "true")
         {
            _loc3_ = _loc2_..Active;
            _loc4_ = 0;
            if(_loc4_ < _loc3_.length())
            {
               ObjectUtils.copyPorpertiesByXML(this._data,_loc3_[_loc4_]);
            }
            _loc3_ = _loc2_..ActiveExp;
            _loc4_ = 0;
            while(_loc4_ < _loc3_.length())
            {
               if(_loc3_[_loc4_].@ActiveID == this._data.ActiveID.toString())
               {
                  this._dataList.push(int(_loc3_[_loc4_].@Exp));
               }
               _loc4_++;
            }
            this._dataList.sort(Array.NUMERIC);
            _loc3_ = _loc2_..ActiveAward;
            _loc4_ = 0;
            while(_loc4_ < _loc3_.length())
            {
               if(_loc3_[_loc4_].@ActiveID == this._data.ActiveID.toString())
               {
                  _loc5_ = new DragonBoatAwardInfo();
                  ObjectUtils.copyPorpertiesByXML(_loc5_,_loc3_[_loc4_]);
                  if(_loc3_[_loc4_].@IsArea == "1")
                  {
                     this._dataListSelf.push(_loc5_);
                  }
                  else
                  {
                     this._dataListOther.push(_loc5_);
                  }
               }
               _loc4_++;
            }
            this._dataListSelf.sortOn("RandID",Array.NUMERIC);
            this._dataListOther.sortOn("RandID",Array.NUMERIC);
            onAnalyzeComplete();
         }
         else
         {
            message = _loc2_.@message;
            onAnalyzeError();
         }
      }
      
      public function get data() : DragonBoatActiveInfo
      {
         return this._data;
      }
      
      public function get dataList() : Array
      {
         return this._dataList;
      }
      
      public function get dataListSelf() : Array
      {
         return this._dataListSelf;
      }
      
      public function get dataListOther() : Array
      {
         return this._dataListOther;
      }
   }
}
