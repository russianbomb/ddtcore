package dragonBoat.view
{
   import baglocked.BaglockedManager;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.ListPanel;
   import com.pickgliss.ui.controls.SelectedButton;
   import com.pickgliss.ui.controls.SelectedButtonGroup;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.ServerConfigInfo;
   import ddt.data.player.PlayerInfo;
   import ddt.events.BagEvent;
   import ddt.events.PlayerPropertyEvent;
   import ddt.manager.LanguageMgr;
   import ddt.manager.LeavePageManager;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerConfigManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import dragonBoat.DragonBoatEvent;
   import dragonBoat.DragonBoatManager;
   import flash.display.Bitmap;
   import flash.display.DisplayObject;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.events.TimerEvent;
   import flash.text.TextFormat;
   import flash.utils.Timer;
   import store.HelpFrame;
   
   public class DragonBoatFrame extends Frame
   {
       
      
      private var _bg:Bitmap;
      
      private var _leftDragonBoatSprite:Sprite;
      
      private var _leftPlayerSprite:Sprite;
      
      private var _dragonboatBg:Bitmap;
      
      private var _selfRankBg:Bitmap;
      
      private var _otherRankBg:Bitmap;
      
      private var _smallBorder:Bitmap;
      
      private var _progressBg:Bitmap;
      
      private var _progress:Bitmap;
      
      private var _progressMask:Bitmap;
      
      private var _selfRankSelectedBtn:SelectedButton;
      
      private var _otherRankSelectedBtn:SelectedButton;
      
      private var _btnGroup:SelectedButtonGroup;
      
      private var _selfRankSprite:Sprite;
      
      private var _otherRankSprite:Sprite;
      
      private var _normalDecorateBtn:SimpleBitmapButton;
      
      private var _highDecorateBtn:SimpleBitmapButton;
      
      private var _normalBuildBtn:SimpleBitmapButton;
      
      private var _highBuildBtn:SimpleBitmapButton;
      
      private var _scoreExchangeBtn:SimpleBitmapButton;
      
      private var _helpBtn:SimpleBitmapButton;
      
      private var _progressTxt:FilterFrameText;
      
      private var _timeTxt:FilterFrameText;
      
      private var _itemCountTxt:FilterFrameText;
      
      private var _scoreTxt:FilterFrameText;
      
      private var _scoreTxt2:FilterFrameText;
      
      private var _rankTxt:FilterFrameText;
      
      private var _needTxt:FilterFrameText;
      
      private var _leftBottomTxt:FilterFrameText;
      
      private var _rightBottomTxt:FilterFrameText;
      
      private var _selfRankList:ListPanel;
      
      private var _otherRankList:ListPanel;
      
      private var _selfDataList:Array;
      
      private var _selfRank:String = "";
      
      private var _selfLessScore:String = "";
      
      private var _otherDataList:Array;
      
      private var _otherRank:String = "";
      
      private var _otherLessScore:String = "";
      
      private var _timer:Timer;
      
      private var _displayMc:MovieClip;
      
      private var _dragonBoatLeftCurrentCharcter:DragonBoatLeftCurrentCharcter;
      
      private var type:int;
      
      private var _playerInfo:PlayerInfo;
      
      public function DragonBoatFrame()
      {
         this._selfDataList = [];
         this._otherDataList = [];
         super();
      }
      
      public function init2(param1:int) : void
      {
         this.type = param1;
         this.initView();
         this.initEvent();
         this.refreshView();
         this.refreshItemCount();
         this._btnGroup.selectIndex = 0;
         var _loc2_:Date = DragonBoatManager.instance.activeInfo.beginTimeDate;
         var _loc3_:Date = DragonBoatManager.instance.activeInfo.endTimeDate;
         this._timeTxt.text = LanguageMgr.GetTranslation("ddt.dragonBoat.timeTxt",_loc2_.fullYear,_loc2_.month + 1,_loc2_.date,_loc3_.fullYear,_loc3_.month + 1,_loc3_.date);
         this.initTimer();
         SocketManager.Instance.out.sendDragonBoatRefreshBoatStatus();
         SocketManager.Instance.out.sendDragonBoatRefreshRank();
         DragonBoatManager.instance.setOpenFrameParam();
      }
      
      private function initView() : void
      {
         if(DragonBoatManager.instance.isBuildEnd)
         {
            titleText = LanguageMgr.GetTranslation("ddt.hall.dragonboatentrybtnTitle");
         }
         else
         {
            titleText = LanguageMgr.GetTranslation("ddt.dragonBoat.frameTitle");
         }
         if(this.type == 1)
         {
            this._bg = ComponentFactory.Instance.creatBitmap("asset.dragonBoat.mainFrame.bg");
         }
         else if(this.type == 4)
         {
            this._bg = ComponentFactory.Instance.creatBitmap("asset.kingStatue.mainFrame.bg");
         }
         this._leftDragonBoatSprite = new Sprite();
         this._leftPlayerSprite = new Sprite();
         this._selfRankBg = ComponentFactory.Instance.creatBitmap("asset.dragonBoat.mainFrame.selfRank.bg");
         this._otherRankBg = ComponentFactory.Instance.creatBitmap("asset.dragonBoat.mainFrame.otherRank.bg");
         this._smallBorder = ComponentFactory.Instance.creatBitmap("asset.dragonBoat.mainFrame.smallBorder");
         this._selfRankSprite = new Sprite();
         PositionUtils.setPos(this._selfRankSprite,"dragonBoat.mainFrame.selfRankPos");
         this._selfRankSprite.addChild(this._selfRankBg);
         this._otherRankSprite = new Sprite();
         PositionUtils.setPos(this._otherRankSprite,"dragonBoat.mainFrame.otherRankPos");
         this._otherRankSprite.addChild(this._otherRankBg);
         this._progressBg = ComponentFactory.Instance.creatBitmap("asset.dragonBoat.mainFrame.progressBg");
         this._progress = ComponentFactory.Instance.creatBitmap("asset.dragonBoat.mainFrame.progress");
         this._progressMask = ComponentFactory.Instance.creatBitmap("asset.dragonBoat.mainFrame.progressBg");
         this._progress.mask = this._progressMask;
         this._selfRankSelectedBtn = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.selfRank.btn");
         this._otherRankSelectedBtn = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.otherRank.btn");
         this._btnGroup = new SelectedButtonGroup();
         this._btnGroup.addSelectItem(this._selfRankSelectedBtn);
         this._btnGroup.addSelectItem(this._otherRankSelectedBtn);
         this._normalDecorateBtn = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.normalDecorateBtn");
         this._highDecorateBtn = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.highDecorateBtn");
         this._normalBuildBtn = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.normalBuildBtn");
         this._highBuildBtn = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.highBuildBtn");
         this._scoreExchangeBtn = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.scoreBtn");
         this._helpBtn = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.helpBtn");
         this._progressTxt = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.progressTxt");
         this._timeTxt = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.timeTxt");
         this._itemCountTxt = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.scoreTxt");
         PositionUtils.setPos(this._itemCountTxt,"dragonBoat.mainFrame.itemCountTxtPos");
         this._scoreTxt = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.scoreTxt");
         PositionUtils.setPos(this._scoreTxt,"dragonBoat.mainFrame.scoreTxt1Pos");
         this._scoreTxt2 = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.scoreTxt");
         PositionUtils.setPos(this._scoreTxt2,"dragonBoat.mainFrame.scoreTxt2Pos");
         this._rankTxt = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.rankTxt");
         this._needTxt = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.needTxt");
         this._leftBottomTxt = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.bottomTxt");
         this._leftBottomTxt.text = LanguageMgr.GetTranslation("ddt.dragonBoat.leftBottomTxt");
         this._rightBottomTxt = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.bottomTxt2");
         var _loc1_:String = "";
         var _loc2_:ServerConfigInfo = ServerConfigManager.instance.findInfoByName("DragonBoatAreaMinScore");
         if(_loc2_)
         {
            _loc1_ = _loc2_.Value;
         }
         this._rightBottomTxt.text = LanguageMgr.GetTranslation("ddt.dragonBoat.rightBottomTxt",DragonBoatManager.instance.activeInfo.MinScore,_loc1_);
         this._selfRankList = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.selfRankList");
         this._selfRankSprite.addChild(this._selfRankList);
         this._otherRankList = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.otherRankList");
         this._otherRankSprite.addChild(this._otherRankList);
         switch(this.type)
         {
            case 1:
               titleText = LanguageMgr.GetTranslation("ddt.dragonBoat.frameTitle");
               this._bg = ComponentFactory.Instance.creatBitmap("asset.dragonBoat.mainFrame.bg");
               PositionUtils.setPos(this._itemCountTxt,"dragonBoat.mainFrame.itemCountTxtPos");
               this._leftBottomTxt.text = LanguageMgr.GetTranslation("ddt.dragonBoat.leftBottomTxt");
               this._rightBottomTxt.text = LanguageMgr.GetTranslation("ddt.dragonBoat.rightBottomTxt",DragonBoatManager.instance.activeInfo.MinScore,_loc1_);
               this._displayMc = ComponentFactory.Instance.creat("asset.dragonBoat.boatMc");
               this._displayMc.scaleX = 1.2;
               this._displayMc.scaleY = 1.2;
               PositionUtils.setPos(this._displayMc,"dragonBoat.shopFrame.boatMcPos");
               this._dragonboatBg = ComponentFactory.Instance.creatBitmap("asset.dragonBoat.mainFrame.dragonboatBg");
               break;
            case 4:
               titleText = LanguageMgr.GetTranslation("kingStatue.title");
               this._bg = ComponentFactory.Instance.creatBitmap("asset.kingStatue.mainFrame.bg");
               PositionUtils.setPos(this._itemCountTxt,"dragonBoat.mainFrame.itemCountTxtPos");
               this._leftBottomTxt.text = LanguageMgr.GetTranslation("kingStatue.leftBottomTxt");
               this._rightBottomTxt.text = LanguageMgr.GetTranslation("ddt.dragonBoat.rightBottomTxt2",DragonBoatManager.instance.activeInfo.MinScore,_loc1_);
               this._displayMc = ComponentFactory.Instance.creat("asset.kingStatue.statueFrameMc");
               PositionUtils.setPos(this._displayMc,"kingStatue.mainFrame.statuePos");
               this._dragonboatBg = ComponentFactory.Instance.creatBitmap("asset.kingStatue.mainFrame.kingStatueBg");
         }
         addToContent(this._bg);
         addToContent(this._leftDragonBoatSprite);
         addToContent(this._leftPlayerSprite);
         this._leftDragonBoatSprite.addChild(this._dragonboatBg);
         addToContent(this._selfRankSprite);
         addToContent(this._otherRankSprite);
         addToContent(this._smallBorder);
         this._leftDragonBoatSprite.addChild(this._displayMc);
         this._leftDragonBoatSprite.addChild(this._progress);
         this._leftDragonBoatSprite.addChild(this._progressBg);
         this._leftDragonBoatSprite.addChild(this._progressMask);
         addToContent(this._selfRankSelectedBtn);
         addToContent(this._otherRankSelectedBtn);
         this._leftDragonBoatSprite.addChild(this._normalDecorateBtn);
         this._leftDragonBoatSprite.addChild(this._highDecorateBtn);
         this._leftDragonBoatSprite.addChild(this._normalBuildBtn);
         this._leftDragonBoatSprite.addChild(this._highBuildBtn);
         addToContent(this._scoreExchangeBtn);
         addToContent(this._helpBtn);
         this._leftDragonBoatSprite.addChild(this._progressTxt);
         addToContent(this._timeTxt);
         addToContent(this._itemCountTxt);
         addToContent(this._scoreTxt);
         addToContent(this._scoreTxt2);
         addToContent(this._rankTxt);
         addToContent(this._needTxt);
         addToContent(this._leftBottomTxt);
         addToContent(this._rightBottomTxt);
         if(DragonBoatManager.instance.isBuildEnd)
         {
            this._leftDragonBoatSprite.visible = false;
         }
         else
         {
            this.isShowDragonboat(true);
         }
      }
      
      private function initTimer() : void
      {
         if(DragonBoatManager.instance.isCanBuildOrDecorate)
         {
            this._timer = new Timer(300000);
            this._timer.addEventListener(TimerEvent.TIMER,this.timerHander,false,0,true);
            this._timer.start();
         }
      }
      
      private function timerHander(param1:TimerEvent) : void
      {
         SocketManager.Instance.out.sendDragonBoatRefreshBoatStatus();
         SocketManager.Instance.out.sendDragonBoatRefreshRank();
      }
      
      private function refreshView() : void
      {
         var _loc2_:Boolean = false;
         var _loc3_:Boolean = false;
         this._displayMc.gotoAndStop(DragonBoatManager.instance.boatInWhatStatus);
         var _loc1_:int = DragonBoatManager.instance.boatCompleteStatus;
         this._progressMask.scaleX = _loc1_ / 100;
         this._progressTxt.text = LanguageMgr.GetTranslation("ddt.dragonBoat.progressTxt",_loc1_);
         this._scoreTxt.text = DragonBoatManager.instance.useableScore + "";
         this._scoreTxt2.text = DragonBoatManager.instance.totalScore + "";
         this._leftDragonBoatSprite.addChild(this._normalDecorateBtn);
         this._leftDragonBoatSprite.addChild(this._highDecorateBtn);
         this._leftDragonBoatSprite.addChild(this._normalBuildBtn);
         this._leftDragonBoatSprite.addChild(this._highBuildBtn);
         if(_loc1_ >= 100)
         {
            _loc2_ = false;
         }
         else
         {
            _loc2_ = true;
         }
         if(DragonBoatManager.instance.isCanBuildOrDecorate)
         {
            _loc3_ = true;
         }
         else
         {
            _loc3_ = false;
         }
         this.refreshBtnStatus(_loc2_,_loc3_);
      }
      
      private function refreshBtnStatus(param1:Boolean, param2:Boolean) : void
      {
         this._normalDecorateBtn.visible = !param1;
         this._highDecorateBtn.visible = !param1;
         this._normalBuildBtn.visible = param1;
         this._highBuildBtn.visible = param1;
         this._normalDecorateBtn.enable = param2;
         this._highDecorateBtn.enable = param2;
         this._normalBuildBtn.enable = param2;
         this._highBuildBtn.enable = param2;
      }
      
      private function refreshItemCount() : void
      {
         var _loc1_:int = 0;
         var _loc3_:int = 0;
         if(this.type == 1)
         {
            _loc1_ = DragonBoatManager.DRAGONBOAT_CHIP;
         }
         else
         {
            _loc1_ = DragonBoatManager.KINGSTATUE_CHIP;
         }
         this._itemCountTxt.text = PlayerManager.Instance.Self.PropBag.getItemCountByTemplateId(_loc1_,false).toString();
         var _loc2_:int = PlayerManager.Instance.Self.PropBag.getItemCountByTemplateId(DragonBoatManager.LINSHI_CHIP,false);
         if(_loc2_ > 0)
         {
            _loc3_ = PlayerManager.Instance.Self.PropBag.getItemCountByTemplateId(_loc1_,true) + _loc2_;
            this._itemCountTxt.text = _loc3_.toString();
         }
      }
      
      private function initEvent() : void
      {
         addEventListener(FrameEvent.RESPONSE,this.__responseHandler);
         this._btnGroup.addEventListener(Event.CHANGE,this.__changeHandler,false,0,true);
         this._selfRankSelectedBtn.addEventListener(MouseEvent.CLICK,this.__soundPlay,false,0,true);
         this._otherRankSelectedBtn.addEventListener(MouseEvent.CLICK,this.__soundPlay,false,0,true);
         this._normalBuildBtn.addEventListener(MouseEvent.CLICK,this.consumeHandler,false,0,true);
         this._highBuildBtn.addEventListener(MouseEvent.CLICK,this.consumeHandler,false,0,true);
         this._normalDecorateBtn.addEventListener(MouseEvent.CLICK,this.consumeHandler,false,0,true);
         this._highDecorateBtn.addEventListener(MouseEvent.CLICK,this.consumeHandler,false,0,true);
         this._helpBtn.addEventListener(MouseEvent.CLICK,this.openHelpFrame,false,0,true);
         this._scoreExchangeBtn.addEventListener(MouseEvent.CLICK,this.openShopFrame,false,0,true);
         DragonBoatManager.instance.addEventListener(DragonBoatManager.BUILD_OR_DECORATE_UPDATE,this.buildOrDecorateHandler);
         DragonBoatManager.instance.addEventListener(DragonBoatManager.REFRESH_BOAT_STATUS,this.refreshBoatStatusHandler);
         PlayerManager.Instance.Self.PropBag.addEventListener(BagEvent.UPDATE,this.itemUpdateHandler);
         DragonBoatManager.instance.addEventListener(DragonBoatManager.UPDATE_RANK_INFO,this.updateRankInfo);
      }
      
      private function updateRankInfo(param1:DragonBoatEvent) : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc2_:int = param1.tag;
         if(_loc2_ == 1)
         {
            this._selfDataList = param1.data.dataList;
            _loc3_ = param1.data.myRank;
            if(_loc3_ < 0)
            {
               this._selfRank = LanguageMgr.GetTranslation("ddt.dragonBoat.rankNoPlace");
            }
            else
            {
               this._selfRank = _loc3_.toString();
            }
            this._selfLessScore = param1.data.lessScore.toString();
            if(this._btnGroup.selectIndex == 0)
            {
               this.refreshRankView(1);
            }
            if(DragonBoatManager.instance.isBuildEnd)
            {
               this.isShowDragonboat(false);
            }
            else
            {
               this.isShowDragonboat(true);
            }
         }
         else
         {
            this._otherDataList = param1.data.dataList;
            _loc4_ = param1.data.myRank;
            if(_loc4_ < 0)
            {
               this._otherRank = LanguageMgr.GetTranslation("ddt.dragonBoat.rankNoPlace");
            }
            else
            {
               this._otherRank = _loc4_.toString();
            }
            this._otherLessScore = param1.data.lessScore.toString();
            if(this._btnGroup.selectIndex == 1)
            {
               this.refreshRankView(2);
            }
         }
      }
      
      private function refreshRankView(param1:int) : void
      {
         if(param1 == 1)
         {
            this._selfRankList.vectorListModel.clear();
            this._selfRankList.vectorListModel.appendAll(this._selfDataList);
            this._selfRankList.list.updateListView();
            this._rankTxt.text = this._selfRank;
            this._needTxt.text = this._selfLessScore;
         }
         else
         {
            this._otherRankList.vectorListModel.clear();
            this._otherRankList.vectorListModel.appendAll(this._otherDataList);
            this._otherRankList.list.updateListView();
            this._rankTxt.text = this._otherRank;
            this._needTxt.text = this._otherLessScore;
         }
         var _loc2_:int = 30;
         if(this._rankTxt.textWidth > this._rankTxt.width)
         {
            _loc2_ = 14;
            this._rankTxt.y = 420;
         }
         else
         {
            this._rankTxt.y = 413;
         }
         var _loc3_:TextFormat = this._rankTxt.getTextFormat();
         _loc3_.size = _loc2_;
         this._rankTxt.setTextFormat(_loc3_);
         this._rankTxt.defaultTextFormat = _loc3_;
      }
      
      private function openShopFrame(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         var _loc2_:DragonBoatShopFrame = ComponentFactory.Instance.creatComponentByStylename("DragonBoatShopFrame");
         LayerManager.Instance.addToLayer(_loc2_,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
      }
      
      private function openHelpFrame(param1:MouseEvent) : void
      {
         var _loc2_:DisplayObject = null;
         SoundManager.instance.play("008");
         switch(this.type)
         {
            case 1:
               _loc2_ = ComponentFactory.Instance.creat("dragonBoat.HelpPrompt");
               break;
            case 4:
               _loc2_ = ComponentFactory.Instance.creat("dragonBoat.statueHelpPrompt");
         }
         var _loc3_:HelpFrame = ComponentFactory.Instance.creat("dragonBoat.HelpFrame");
         _loc3_.setView(_loc2_);
         _loc3_.titleText = LanguageMgr.GetTranslation("store.view.HelpButtonText");
         LayerManager.Instance.addToLayer(_loc3_,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
      }
      
      private function refreshBoatStatusHandler(param1:Event) : void
      {
         this.refreshView();
      }
      
      private function itemUpdateHandler(param1:BagEvent) : void
      {
         this.refreshItemCount();
      }
      
      private function buildOrDecorateHandler(param1:Event) : void
      {
         SocketManager.Instance.out.sendDragonBoatRefreshRank();
         this.refreshView();
      }
      
      private function consumeHandler(param1:Event) : void
      {
         var _loc3_:DragonBoatConsumeView = null;
         SoundManager.instance.play("008");
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         var _loc2_:int = 1;
         switch(param1.currentTarget)
         {
            case this._normalBuildBtn:
               _loc2_ = 1;
               break;
            case this._normalDecorateBtn:
               _loc2_ = 2;
               break;
            case this._highBuildBtn:
               _loc2_ = 3;
               break;
            case this._highDecorateBtn:
               _loc2_ = 4;
         }
         switch(_loc2_)
         {
            case 1:
            case 2:
            case 3:
            case 4:
               if((_loc2_ == 1 || _loc2_ == 2) && int(this._itemCountTxt.text) <= 0)
               {
                  if(this.type == 1)
                  {
                     MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.dragonBoat.noEnoughPiece"),0,true);
                  }
                  else if(this.type == 4)
                  {
                     MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.dragonBoat.statueNoEnoughPiece"),0,true);
                  }
                  return;
               }
               if((_loc2_ == 3 || _loc2_ == 4) && PlayerManager.Instance.Self.Money < 100)
               {
                  LeavePageManager.showFillFrame();
                  return;
               }
               _loc3_ = ComponentFactory.Instance.creatComponentByStylename("DragonBoatNormalView");
               _loc3_.setView(_loc2_);
               LayerManager.Instance.addToLayer(_loc3_,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
               break;
         }
      }
      
      private function __changeHandler(param1:Event) : void
      {
         switch(this._btnGroup.selectIndex)
         {
            case 0:
               this._selfRankSelectedBtn.x = 390;
               this._selfRankSelectedBtn.y = 4;
               this._otherRankSelectedBtn.x = 563;
               this._otherRankSelectedBtn.y = 7;
               this._selfRankSprite.visible = true;
               this._otherRankSprite.visible = false;
               this.refreshRankView(1);
               break;
            case 1:
               this._selfRankSelectedBtn.x = 396;
               this._selfRankSelectedBtn.y = 6;
               this._otherRankSelectedBtn.x = 550;
               this._otherRankSelectedBtn.y = 4;
               this._selfRankSprite.visible = false;
               this._otherRankSprite.visible = true;
               this.refreshRankView(2);
         }
      }
      
      private function __soundPlay(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
      }
      
      private function __responseHandler(param1:FrameEvent) : void
      {
         if(param1.responseCode == FrameEvent.CLOSE_CLICK || param1.responseCode == FrameEvent.ESC_CLICK)
         {
            SoundManager.instance.play("008");
            this.dispose();
         }
      }
      
      private function isShowDragonboat(param1:Boolean) : void
      {
         this._leftDragonBoatSprite.visible = param1;
         if(this._dragonBoatLeftCurrentCharcter)
         {
            this._dragonBoatLeftCurrentCharcter.visible = !param1;
         }
         if(!param1)
         {
            this.showPlayerNo1();
         }
      }
      
      private function showPlayerNo1() : void
      {
         var _loc1_:Object = null;
         var _loc2_:Object = null;
         if(this._selfDataList.length <= 0)
         {
            return;
         }
         for each(_loc2_ in this._selfDataList)
         {
            if(_loc2_.rank == 1)
            {
               _loc1_ = _loc2_;
               break;
            }
         }
         this._playerInfo = new PlayerInfo();
         if(_loc1_.name == PlayerManager.Instance.Self.NickName)
         {
            this._playerInfo = PlayerManager.Instance.Self;
         }
         else
         {
            this._playerInfo = PlayerManager.Instance.findPlayerByNickName(this._playerInfo,_loc1_.name);
         }
         if(this._playerInfo.ID && this._playerInfo.Style)
         {
            this.initPlayerNo1();
         }
         else
         {
            SocketManager.Instance.out.sendItemEquip(_loc1_.name,true);
            this._playerInfo.addEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__playerInfoChange);
         }
      }
      
      private function __playerInfoChange(param1:PlayerPropertyEvent) : void
      {
         this._playerInfo.removeEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__playerInfoChange);
         this.initPlayerNo1();
      }
      
      private function initPlayerNo1() : void
      {
         if(!this._dragonBoatLeftCurrentCharcter)
         {
            this._dragonBoatLeftCurrentCharcter = new DragonBoatLeftCurrentCharcter();
            this._leftPlayerSprite.addChild(this._dragonBoatLeftCurrentCharcter);
         }
         this._dragonBoatLeftCurrentCharcter.updateInfo(this._playerInfo);
      }
      
      private function removeEvent() : void
      {
         removeEventListener(FrameEvent.RESPONSE,this.__responseHandler);
         this._btnGroup.removeEventListener(Event.CHANGE,this.__changeHandler);
         this._selfRankSelectedBtn.removeEventListener(MouseEvent.CLICK,this.__soundPlay);
         this._otherRankSelectedBtn.removeEventListener(MouseEvent.CLICK,this.__soundPlay);
         this._normalBuildBtn.removeEventListener(MouseEvent.CLICK,this.consumeHandler);
         this._highBuildBtn.removeEventListener(MouseEvent.CLICK,this.consumeHandler);
         this._normalDecorateBtn.removeEventListener(MouseEvent.CLICK,this.consumeHandler);
         this._highDecorateBtn.removeEventListener(MouseEvent.CLICK,this.consumeHandler);
         this._helpBtn.removeEventListener(MouseEvent.CLICK,this.openHelpFrame);
         this._scoreExchangeBtn.removeEventListener(MouseEvent.CLICK,this.openShopFrame);
         DragonBoatManager.instance.removeEventListener(DragonBoatManager.BUILD_OR_DECORATE_UPDATE,this.buildOrDecorateHandler);
         DragonBoatManager.instance.removeEventListener(DragonBoatManager.REFRESH_BOAT_STATUS,this.refreshBoatStatusHandler);
         PlayerManager.Instance.Self.PropBag.removeEventListener(BagEvent.UPDATE,this.itemUpdateHandler);
         DragonBoatManager.instance.removeEventListener(DragonBoatManager.UPDATE_RANK_INFO,this.updateRankInfo);
         if(this._playerInfo)
         {
            this._playerInfo.removeEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__playerInfoChange);
         }
      }
      
      override public function dispose() : void
      {
         this.removeEvent();
         if(this._dragonBoatLeftCurrentCharcter)
         {
            this._dragonBoatLeftCurrentCharcter.dispose();
         }
         this._dragonBoatLeftCurrentCharcter = null;
         if(this._timer)
         {
            this._timer.removeEventListener(TimerEvent.TIMER,this.timerHander);
            this._timer.stop();
            this._timer = null;
         }
         if(this._displayMc)
         {
            this._displayMc.gotoAndStop(this._displayMc.totalFrames);
            this._displayMc = null;
         }
         this._selfDataList = null;
         this._otherDataList = null;
         ObjectUtils.disposeObject(this._selfRankList);
         this._selfRankList = null;
         ObjectUtils.disposeObject(this._otherRankList);
         this._otherRankList = null;
         super.dispose();
         this._bg = null;
         this._selfRankBg = null;
         this._otherRankBg = null;
         this._smallBorder = null;
         this._progressBg = null;
         this._progress = null;
         this._progressMask = null;
         this._selfRankSelectedBtn = null;
         this._otherRankSelectedBtn = null;
         this._btnGroup = null;
         this._selfRankSprite = null;
         this._otherRankSprite = null;
         this._normalDecorateBtn = null;
         this._highDecorateBtn = null;
         this._normalBuildBtn = null;
         this._highBuildBtn = null;
         this._scoreExchangeBtn = null;
         this._helpBtn = null;
         this._progressTxt = null;
         this._timeTxt = null;
         this._itemCountTxt = null;
         this._scoreTxt = null;
         this._scoreTxt2 = null;
         this._rankTxt = null;
         this._needTxt = null;
         this._leftBottomTxt = null;
         this._rightBottomTxt = null;
         this._leftDragonBoatSprite = null;
         this._leftPlayerSprite = null;
      }
   }
}
