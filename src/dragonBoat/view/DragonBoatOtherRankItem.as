package dragonBoat.view
{
   import bagAndInfo.cell.BagCell;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.cell.IListCell;
   import com.pickgliss.ui.controls.list.List;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.ui.text.GradientText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.DisplayObject;
   import flash.display.Sprite;
   import flash.geom.Point;
   import flash.text.TextFormat;
   import vip.VipController;
   
   public class DragonBoatOtherRankItem extends Sprite implements Disposeable, IListCell
   {
       
      
      private var _data:Object;
      
      private var _rankIconList:Vector.<Bitmap>;
      
      private var _rankTxt:FilterFrameText;
      
      private var _nameTxt:GradientText;
      
      private var _zoneTxt:FilterFrameText;
      
      private var _scoreTxt:FilterFrameText;
      
      private var _awardHbox:Sprite;
      
      private var _awardList:Vector.<BagCell>;
      
      private var _awardHboxPos:Point;
      
      public function DragonBoatOtherRankItem()
      {
         var _loc3_:Bitmap = null;
         super();
         this._awardList = new Vector.<BagCell>();
         this._rankIconList = new Vector.<Bitmap>(3);
         var _loc1_:int = 0;
         while(_loc1_ < 3)
         {
            _loc3_ = ComponentFactory.Instance.creatBitmap("asset.dragonBoat.mainFrame.cellRankth" + (_loc1_ + 1));
            addChild(_loc3_);
            this._rankIconList[_loc1_] = _loc3_;
            _loc1_++;
         }
         this._rankTxt = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.cell.rankTxt");
         this._nameTxt = VipController.instance.getVipNameTxt(105,1);
         var _loc2_:TextFormat = new TextFormat();
         _loc2_.align = "center";
         _loc2_.bold = true;
         this._nameTxt.textField.defaultTextFormat = _loc2_;
         this._nameTxt.textSize = 14;
         PositionUtils.setPos(this._nameTxt,"dragonBoat.mainFrame.cell.nameTxtPos");
         addChild(this._nameTxt);
         this._zoneTxt = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.cell.scoreTxt");
         PositionUtils.setPos(this._zoneTxt,"dragonBoat.mainFrame.otherCell.zoneTxtPos");
         this._scoreTxt = ComponentFactory.Instance.creatComponentByStylename("dragonBoat.mainFrame.cell.scoreTxt");
         PositionUtils.setPos(this._scoreTxt,"dragonBoat.mainFrame.otherCell.scoreTxtPos");
         this._awardHboxPos = ComponentFactory.Instance.creatCustomObject("dragonBoat.shopFrame.cellAwardOtherPos");
         this._awardHbox = new Sprite();
         this._awardHbox.y = this._awardHboxPos.y;
         addChild(this._rankTxt);
         addChild(this._nameTxt);
         addChild(this._zoneTxt);
         addChild(this._scoreTxt);
         addChild(this._awardHbox);
      }
      
      private function clearAwardCell() : void
      {
         var _loc1_:BagCell = null;
         for each(_loc1_ in this._awardList)
         {
            _loc1_.info = null;
         }
      }
      
      private function updateAwardCell(param1:int, param2:ItemTemplateInfo) : void
      {
         var _loc3_:BagCell = null;
         if(param1 > this._awardList.length - 1)
         {
            _loc3_ = new BagCell(1,param2,true,null,false);
            _loc3_.tipGapH = 0;
            _loc3_.tipGapV = 0;
            _loc3_.scaleX = 0.6;
            _loc3_.scaleY = 0.6;
         }
         else
         {
            _loc3_ = this._awardList[param1];
         }
         _loc3_.info = param2;
         _loc3_.x = this._awardHbox.width + 5;
         this._awardHbox.addChild(_loc3_);
      }
      
      public function setListCellStatus(param1:List, param2:Boolean, param3:int) : void
      {
      }
      
      public function getCellValue() : *
      {
         return this._data;
      }
      
      public function setCellValue(param1:*) : void
      {
         var _loc4_:InventoryItemInfo = null;
         this._data = param1;
         var _loc2_:int = this._data.rank;
         this.setRankIconVisible(_loc2_);
         if(_loc2_ >= 1 && _loc2_ <= 3)
         {
            this._rankTxt.visible = false;
         }
         else
         {
            this._rankTxt.text = String(_loc2_);
            this._rankTxt.visible = true;
         }
         this._nameTxt.text = this._data.name;
         this._zoneTxt.text = this._data.zone;
         this._scoreTxt.text = this._data.score;
         if(this._awardHbox)
         {
            while(this._awardHbox.numChildren > 0)
            {
               this._awardHbox.removeChild(this._awardHbox.getChildAt(0));
            }
         }
         this.clearAwardCell();
         var _loc3_:int = 0;
         while(_loc3_ < this._data.itemInfoArr.length)
         {
            _loc4_ = this._data.itemInfoArr[_loc3_] as InventoryItemInfo;
            this.updateAwardCell(_loc3_,_loc4_);
            _loc3_++;
         }
         this._awardHbox.x = this._awardHboxPos.x + (120 - this._awardHbox.width) / 2;
      }
      
      private function setRankIconVisible(param1:int) : void
      {
         var _loc2_:int = this._rankIconList.length;
         var _loc3_:int = 1;
         while(_loc3_ <= _loc2_)
         {
            if(param1 == _loc3_)
            {
               this._rankIconList[_loc3_ - 1].visible = true;
            }
            else
            {
               this._rankIconList[_loc3_ - 1].visible = false;
            }
            _loc3_++;
         }
      }
      
      public function asDisplayObject() : DisplayObject
      {
         return this;
      }
      
      public function dispose() : void
      {
         if(this._awardHbox)
         {
            ObjectUtils.disposeAllChildren(this._awardHbox);
            this._awardHbox = null;
         }
         this._awardHboxPos = null;
         this._awardList = null;
         ObjectUtils.disposeAllChildren(this);
         this._data = null;
         this._rankIconList = null;
         this._rankTxt = null;
         this._nameTxt = null;
         this._zoneTxt = null;
         this._scoreTxt = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
