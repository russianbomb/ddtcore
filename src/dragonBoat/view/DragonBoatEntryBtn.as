package dragonBoat.view
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.core.Component;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SoundManager;
   import dragonBoat.DragonBoatManager;
   import flash.display.Bitmap;
   import flash.display.MovieClip;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.filters.GlowFilter;
   
   public class DragonBoatEntryBtn extends Component
   {
       
      
      private var _hallMainView:MovieClip;
      
      private var _consHitBtn:BaseButton;
      
      private var _boatMc:MovieClip;
      
      private var _newBg:Bitmap;
      
      private var _boatTxt:Bitmap;
      
      private var _boatTxt2:Bitmap;
      
      private var _bamboo:MovieClip;
      
      private var _isDispose:Boolean = false;
      
      private var _mouseOverFilter:Array;
      
      private var _isCreate:Boolean = false;
      
      private var activeId:int;
      
      public function DragonBoatEntryBtn(param1:MovieClip, param2:BaseButton)
      {
         this._mouseOverFilter = [new GlowFilter(16777113,1,35,35,2)];
         super();
         this._hallMainView = param1;
         this._consHitBtn = param2;
         this.x = 191;
         this.y = 350;
         this.buttonMode = true;
         this.mouseChildren = false;
         if(DragonBoatManager.instance.isStart && DragonBoatManager.instance.isLoadBoatResComplete)
         {
            this.initThis();
         }
         else
         {
            DragonBoatManager.instance.addEventListener(DragonBoatManager.BOAT_RES_LOAD_COMPLETE,this.resLoadComplete);
         }
         this.initTips();
      }
      
      private function initTips() : void
      {
         this.tipStyle = "ddt.view.tips.HallBuildTip";
         this.tipDirctions = "0,1,2";
         this.tipGapV = 120;
         this.tipGapH = 85;
      }
      
      override public function get tipData() : Object
      {
         var _loc1_:Object = new Object();
         if(DragonBoatManager.instance.isBuildEnd)
         {
            _loc1_["title"] = LanguageMgr.GetTranslation("ddt.hall.dragonboatentrybtnTitle");
         }
         else
         {
            _loc1_["title"] = LanguageMgr.GetTranslation("ddt.dragonBoat.frameTitle");
         }
         _loc1_["content"] = LanguageMgr.GetTranslation("ddt.hall.dragonboatentrybtnContent");
         return _loc1_;
      }
      
      private function overHandler(param1:MouseEvent) : void
      {
         if(!this._boatMc)
         {
            return;
         }
         this._boatMc.filters = this._mouseOverFilter;
      }
      
      private function outHandler(param1:MouseEvent) : void
      {
         if(!this._boatMc)
         {
            return;
         }
         this._boatMc.filters = null;
      }
      
      private function clickHandler(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         if(PlayerManager.Instance.Self.Grade < DragonBoatManager.instance.activeInfo.LimitGrade)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.dragonBoat.cannotEnterPromptTxt"));
            return;
         }
         DragonBoatManager.instance.loadUIModule(DragonBoatManager.instance.doOpenDragonBoatFrame);
      }
      
      private function refreshBoatStatusHandler(param1:Event) : void
      {
         if(this._boatMc)
         {
            this._boatMc.gotoAndStop(DragonBoatManager.instance.boatInWhatStatus);
         }
         this.isShowBoatTxt();
      }
      
      private function initThis() : void
      {
         this.activeId = DragonBoatManager.instance.activeInfo.ActiveID;
         this._isCreate = true;
         switch(this.activeId)
         {
            case 1:
               this._boatMc = ComponentFactory.Instance.creat("asset.dragonBoat.boatMc");
               this._boatMc.gotoAndStop(DragonBoatManager.instance.boatInWhatStatus);
               addChild(this._boatMc);
               this._boatTxt = ComponentFactory.Instance.creatBitmap("asset.dragonBoat.boatTxt");
               this._boatTxt.x = -72;
               this._boatTxt.y = -130;
               addChild(this._boatTxt);
               this._boatTxt2 = ComponentFactory.Instance.creatBitmap("asset.dragonBoat.boatTxt2");
               this._boatTxt2.x = -62;
               this._boatTxt2.y = -130;
               addChild(this._boatTxt2);
               this._newBg = ComponentFactory.Instance.creatBitmap("asset.dragonBoat.newHallBg");
               this._bamboo = ComponentFactory.Instance.creat("asset.dragonBoat.bamboo");
               this._bamboo.gotoAndStop(1);
               this._bamboo.x = 27.4;
               this._bamboo.y = -0.7;
               break;
            case 4:
               this._boatMc = ComponentFactory.Instance.creat("asset.kingStatue.statueMc");
               this._boatMc.gotoAndStop(DragonBoatManager.instance.boatInWhatStatus);
               addChild(this._boatMc);
               this._boatTxt = ComponentFactory.Instance.creatBitmap("asset.kingStatue.statueTxt");
               this._boatTxt.x = -96;
               this._boatTxt.y = -128;
               addChild(this._boatTxt);
         }
         this._newBg = ComponentFactory.Instance.creatBitmap("asset.dragonBoat.newHallBg");
         this.changeHallStateView();
         DragonBoatManager.instance.addEventListener(DragonBoatManager.REFRESH_BOAT_STATUS,this.refreshBoatStatusHandler);
         addEventListener(MouseEvent.CLICK,this.clickHandler,false,0,true);
         addEventListener(MouseEvent.MOUSE_OVER,this.overHandler,false,0,true);
         addEventListener(MouseEvent.MOUSE_OUT,this.outHandler,false,0,true);
         this.isShowBoatTxt();
      }
      
      private function isShowBoatTxt() : void
      {
         if(DragonBoatManager.instance.isBuildEnd && this.activeId == 1)
         {
            this._boatTxt.visible = false;
            if(this._boatTxt2)
            {
               this._boatTxt2.visible = true;
            }
         }
         else
         {
            this._boatTxt.visible = true;
            if(this._boatTxt2)
            {
               this._boatTxt2.visible = false;
            }
         }
      }
      
      private function changeHallStateView() : void
      {
         this._hallMainView["consortiaButton"].x = 240.8;
         this._hallMainView["consortiaButton"].y = 167.05;
         this._hallMainView["spring_water"].visible = false;
         this._hallMainView["txt_consortia_mc"].x = 306;
         this._hallMainView["txt_consortia_mc"].y = 185;
         this._consHitBtn.x = 242;
         this._consHitBtn.y = 176.4;
         this._hallMainView["bgSprite"].addChild(this._newBg);
         if(this._bamboo)
         {
            this._hallMainView["activityCoverLayer"].addChild(this._bamboo);
         }
      }
      
      private function recoverHallStateView() : void
      {
         this._hallMainView["consortiaButton"].x = 210.8;
         this._hallMainView["consortiaButton"].y = 190.65;
         this._hallMainView["spring_water"].visible = true;
         this._hallMainView["txt_consortia_mc"].x = 145.25;
         this._hallMainView["txt_consortia_mc"].y = 227.05;
         this._consHitBtn.x = 212;
         this._consHitBtn.y = 200;
         if(this._newBg)
         {
            this._hallMainView["bgSprite"].removeChild(this._newBg);
         }
         if(this._bamboo)
         {
            this._bamboo.gotoAndStop(2);
            this._hallMainView["activityCoverLayer"].removeChild(this._bamboo);
         }
      }
      
      private function resLoadComplete(param1:Event) : void
      {
         DragonBoatManager.instance.removeEventListener(DragonBoatManager.BOAT_RES_LOAD_COMPLETE,this.resLoadComplete);
         this.initThis();
      }
      
      override public function dispose() : void
      {
         super.dispose();
         if(this._isDispose)
         {
            return;
         }
         DragonBoatManager.instance.removeEventListener(DragonBoatManager.BOAT_RES_LOAD_COMPLETE,this.resLoadComplete);
         if(this._isCreate)
         {
            removeEventListener(MouseEvent.MOUSE_OVER,this.overHandler);
            removeEventListener(MouseEvent.MOUSE_OUT,this.outHandler);
            removeEventListener(MouseEvent.CLICK,this.clickHandler);
            DragonBoatManager.instance.removeEventListener(DragonBoatManager.REFRESH_BOAT_STATUS,this.refreshBoatStatusHandler);
            this.recoverHallStateView();
            this._newBg = null;
            this._bamboo = null;
            if(this._boatMc)
            {
               this._boatMc.gotoAndStop(this._boatMc.totalFrames);
            }
            ObjectUtils.disposeAllChildren(this);
            this._boatMc = null;
            this._boatTxt = null;
         }
         this._hallMainView = null;
         this._consHitBtn = null;
         this._mouseOverFilter = null;
         if(parent)
         {
            parent.removeChild(this);
         }
         this._isDispose = true;
      }
   }
}
