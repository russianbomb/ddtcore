package dragonBoat
{
   import GodSyah.GodSyahPackageType;
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.BaseLoader;
   import com.pickgliss.loader.LoadResourceManager;
   import com.pickgliss.loader.LoaderEvent;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.UIModuleTypes;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.loader.StartupResourceLoader;
   import ddt.manager.ItemManager;
   import ddt.manager.PathManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SharedManager;
   import ddt.manager.SocketManager;
   import ddt.manager.StateManager;
   import ddt.states.StateType;
   import ddt.view.UIModuleSmallLoading;
   import dragonBoat.data.DragonBoatActiveInfo;
   import dragonBoat.data.DragonBoatAwardInfo;
   import dragonBoat.dataAnalyzer.DragonBoatActiveDataAnalyzer;
   import dragonBoat.view.DragonBoatEntryBtn;
   import dragonBoat.view.DragonBoatFrame;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.events.IEventDispatcher;
   import road7th.comm.PackageIn;
   
   public class DragonBoatManager extends EventDispatcher
   {
      
      public static const END_UPDATE:String = "DragonBoatEndUpdate";
      
      public static const BOAT_RES_LOAD_COMPLETE:String = "DragonBoatBoatResLoadComplete";
      
      public static const BUILD_OR_DECORATE_UPDATE:String = "DragonBoatBuildOrDecorateUpdate";
      
      public static const REFRESH_BOAT_STATUS:String = "DragonBoatRefreshBoatStatus";
      
      public static const UPDATE_RANK_INFO:String = "DragBoatUpdateRankInfo";
      
      public static const DRAGONBOAT_CHIP:int = 11771;
      
      public static const KINGSTATUE_CHIP:int = 11771;
      
      public static const LINSHI_CHIP:int = 201309;
      
      private static var _instance:DragonBoatManager;
       
      
      private var _scoreRank:int;
      
      private var _scoreUse:int;
      
      private var _completeStatus:int;
      
      private var _func:Function;
      
      private var _funcParams:Array;
      
      private var _activeInfo:DragonBoatActiveInfo;
      
      private var _boatExpList:Array;
      
      private var _awardsListSelf:Array;
      
      private var _awardsListOther:Array;
      
      private var _awardsInfoSelf:Object;
      
      private var _awardsInfoOther:Object;
      
      private var _boatCompleteExp:int;
      
      private var _useableScore:int;
      
      private var _totalScore:int;
      
      private var _periodType:int = 0;
      
      private var _isLoadBoatResComplete:Boolean;
      
      private var _isLoadFrameResComplete:Boolean;
      
      private var _isHadOpenedFrame:Boolean;
      
      private var _entryBtn:DragonBoatEntryBtn;
      
      public function DragonBoatManager(param1:IEventDispatcher = null)
      {
         this._boatExpList = [];
         this._awardsListSelf = [];
         this._awardsListOther = [];
         this._awardsInfoSelf = {};
         this._awardsInfoOther = {};
         super(param1);
      }
      
      public static function get instance() : DragonBoatManager
      {
         if(_instance == null)
         {
            _instance = new DragonBoatManager();
         }
         return _instance;
      }
      
      public function get activeInfo() : DragonBoatActiveInfo
      {
         return this._activeInfo;
      }
      
      public function get boatCompleteStatus() : int
      {
         var _loc1_:int = this._boatCompleteExp * 100 / this._boatExpList[this._boatExpList.length - 1];
         return _loc1_ > 100?int(100):int(_loc1_);
      }
      
      public function get boatInWhatStatus() : int
      {
         var _loc1_:int = this._boatExpList.length;
         var _loc2_:int = 1;
         var _loc3_:int = _loc1_ - 1;
         while(_loc3_ >= 0)
         {
            if(this._boatCompleteExp >= this._boatExpList[_loc3_])
            {
               _loc2_ = _loc3_ + 1;
               break;
            }
            _loc3_--;
         }
         return _loc2_;
      }
      
      public function get isBuildEnd() : Boolean
      {
         if(this._boatCompleteExp > 0 && this.periodType == 2)
         {
            return true;
         }
         return false;
      }
      
      public function get useableScore() : int
      {
         return this._useableScore;
      }
      
      public function get totalScore() : int
      {
         return this._totalScore;
      }
      
      public function get isStart() : Boolean
      {
         return this._periodType == 1 || this._periodType == 2;
      }
      
      public function get periodType() : int
      {
         return this._periodType;
      }
      
      public function get isCanBuildOrDecorate() : Boolean
      {
         return this._periodType == 1;
      }
      
      public function get isLoadBoatResComplete() : Boolean
      {
         return this._isLoadBoatResComplete;
      }
      
      public function templateDataSetup(param1:DragonBoatActiveDataAnalyzer) : void
      {
         this._activeInfo = param1.data;
         this._boatExpList = param1.dataList;
         this._awardsListSelf = param1.dataListSelf;
         this._awardsListOther = param1.dataListOther;
      }
      
      public function getAwardInfoByRank(param1:int, param2:int) : Array
      {
         var _loc4_:DragonBoatAwardInfo = null;
         var _loc5_:DragonBoatAwardInfo = null;
         var _loc3_:Array = [];
         if(param1 == 1)
         {
            if(this._awardsInfoSelf[param2])
            {
               return this._awardsInfoSelf[param2] as Array;
            }
            for each(_loc4_ in this._awardsListSelf)
            {
               if(_loc4_.RandID == param2)
               {
                  _loc3_.push(this.createInventoryItemInfo(_loc4_));
               }
            }
            this._awardsInfoSelf[param2] = _loc3_;
         }
         else
         {
            if(this._awardsInfoOther[param2])
            {
               return this._awardsInfoOther[param2] as Array;
            }
            for each(_loc5_ in this._awardsListOther)
            {
               if(_loc5_.RandID == param2)
               {
                  _loc3_.push(this.createInventoryItemInfo(_loc5_));
               }
            }
            this._awardsInfoOther[param2] = _loc3_;
         }
         _loc3_.sortOn("TemplateID",Array.NUMERIC);
         return _loc3_;
      }
      
      private function createInventoryItemInfo(param1:DragonBoatAwardInfo) : InventoryItemInfo
      {
         var _loc2_:InventoryItemInfo = new InventoryItemInfo();
         _loc2_.TemplateID = param1.TemplateID;
         ItemManager.fill(_loc2_);
         _loc2_.StrengthenLevel = param1.StrengthenLevel;
         _loc2_.AttackCompose = param1.AttackCompose;
         _loc2_.DefendCompose = param1.DefendCompose;
         _loc2_.LuckCompose = param1.LuckCompose;
         _loc2_.AgilityCompose = param1.AgilityCompose;
         _loc2_.IsBinds = param1.IsBind;
         _loc2_.ValidDate = param1.ValidDate;
         _loc2_.Count = param1.Count;
         return _loc2_;
      }
      
      public function setup() : void
      {
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.DRAGON_BOAT,this.pkgHandler);
      }
      
      private function pkgHandler(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readByte();
         switch(_loc3_)
         {
            case DragonBoatPackageType.START_OR_CLOSE:
               this.openOrClose(_loc2_);
               break;
            case DragonBoatPackageType.BUILD_DECORATE:
               this.buildOrDecorate(_loc2_);
               break;
            case DragonBoatPackageType.REFRESH_BOAT_STATUS:
               this.refreshBoatStatus(_loc2_);
               break;
            case DragonBoatPackageType.REFRESH_RANK:
               this.updateSelfRank(_loc2_);
               break;
            case DragonBoatPackageType.REFRESH_RANK_OTHER:
               this.updateOtherRank(_loc2_);
               break;
            case GodSyahPackageType.GODSYAH_TEMPORARILY_POWER:
               StartupResourceLoader.Instance.reloadGodSyah(_loc2_);
         }
      }
      
      private function updateSelfRank(param1:PackageIn) : void
      {
         var _loc8_:Object = null;
         var _loc2_:int = param1.readInt();
         var _loc3_:Array = [];
         var _loc4_:int = 0;
         while(_loc4_ < _loc2_)
         {
            _loc8_ = {};
            _loc8_.rank = param1.readInt();
            _loc8_.score = param1.readInt();
            _loc8_.name = param1.readUTF();
            _loc8_.itemInfoArr = this.getAwardInfoByRank(1,_loc8_.rank);
            _loc3_.push(_loc8_);
            _loc4_++;
         }
         var _loc5_:int = param1.readInt();
         var _loc6_:int = param1.readInt();
         var _loc7_:DragonBoatEvent = new DragonBoatEvent(UPDATE_RANK_INFO);
         _loc7_.tag = 1;
         _loc7_.data = {
            "dataList":_loc3_,
            "myRank":_loc5_,
            "lessScore":_loc6_
         };
         dispatchEvent(_loc7_);
      }
      
      private function updateOtherRank(param1:PackageIn) : void
      {
         var _loc8_:Object = null;
         var _loc2_:int = param1.readInt();
         var _loc3_:Array = [];
         var _loc4_:int = 0;
         while(_loc4_ < _loc2_)
         {
            _loc8_ = {};
            _loc8_.rank = param1.readInt();
            _loc8_.score = param1.readInt();
            _loc8_.name = param1.readUTF();
            _loc8_.zone = param1.readUTF();
            _loc8_.itemInfoArr = this.getAwardInfoByRank(2,_loc8_.rank);
            _loc3_.push(_loc8_);
            _loc4_++;
         }
         var _loc5_:int = param1.readInt();
         var _loc6_:int = param1.readInt();
         var _loc7_:DragonBoatEvent = new DragonBoatEvent(UPDATE_RANK_INFO);
         _loc7_.tag = 2;
         _loc7_.data = {
            "dataList":_loc3_,
            "myRank":_loc5_,
            "lessScore":_loc6_
         };
         dispatchEvent(_loc7_);
      }
      
      private function openOrClose(param1:PackageIn) : void
      {
         var _loc3_:BaseLoader = null;
         var _loc2_:int = param1.readInt();
         this._periodType = param1.readInt();
         if(this.isStart)
         {
            this._boatCompleteExp = param1.readInt();
            this._isLoadBoatResComplete = false;
            _loc3_ = LoadResourceManager.Instance.createLoader(PathManager.getUIPath() + "/swf/dragonboatboatres.swf",BaseLoader.MODULE_LOADER);
            _loc3_.addEventListener(LoaderEvent.COMPLETE,this.onLoadComplete);
            LoadResourceManager.Instance.startLoad(_loc3_);
         }
         else
         {
            this.disposeEntryBtn();
            dispatchEvent(new Event(END_UPDATE));
         }
      }
      
      private function onLoadComplete(param1:LoaderEvent) : void
      {
         param1.loader.removeEventListener(LoaderEvent.COMPLETE,this.onLoadComplete);
         this._isLoadBoatResComplete = true;
         dispatchEvent(new Event(BOAT_RES_LOAD_COMPLETE));
         this._isLoadFrameResComplete = false;
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadPreResCompleteHandler,false,-1);
         UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.DRAGON_BOAT);
      }
      
      private function loadPreResCompleteHandler(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.DRAGON_BOAT)
         {
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadPreResCompleteHandler);
            this._isLoadFrameResComplete = true;
            if(StateManager.currentStateType == StateType.MAIN)
            {
               this.autoOpenFrame();
            }
         }
      }
      
      private function autoOpenFrame() : void
      {
         var _loc1_:DragonBoatFrame = null;
         if(!this._isHadOpenedFrame && !SharedManager.Instance.isDragonBoatOpenFrame && this.isStart && PlayerManager.Instance.Self.Grade >= this.activeInfo.LimitGrade)
         {
            _loc1_ = ComponentFactory.Instance.creatComponentByStylename("DragonBoatFrame");
            _loc1_.init2(this._activeInfo.ActiveID);
            LayerManager.Instance.addToLayer(_loc1_,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
         }
      }
      
      public function setOpenFrameParam() : void
      {
         this._isHadOpenedFrame = true;
         if(!SharedManager.Instance.isDragonBoatOpenFrame)
         {
            SharedManager.Instance.isDragonBoatOpenFrame = true;
            SharedManager.Instance.save();
         }
      }
      
      public function enterMainOpenFrame() : void
      {
         if(this._isLoadFrameResComplete)
         {
            this.autoOpenFrame();
         }
      }
      
      private function buildOrDecorate(param1:PackageIn) : void
      {
         this._useableScore = param1.readInt();
         this._totalScore = param1.readInt();
         dispatchEvent(new Event(BUILD_OR_DECORATE_UPDATE));
      }
      
      private function refreshBoatStatus(param1:PackageIn) : void
      {
         this._boatCompleteExp = param1.readInt();
         dispatchEvent(new Event(REFRESH_BOAT_STATUS));
      }
      
      public function loadUIModule(param1:Function = null, param2:Array = null) : void
      {
         this._func = param1;
         this._funcParams = param2;
         UIModuleSmallLoading.Instance.progress = 0;
         UIModuleSmallLoading.Instance.show();
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadCompleteHandler);
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUimoduleLoadProgress);
         UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.DRAGON_BOAT);
      }
      
      private function onUimoduleLoadProgress(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.DRAGON_BOAT)
         {
            UIModuleSmallLoading.Instance.progress = param1.loader.progress * 100;
         }
      }
      
      private function loadCompleteHandler(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.DRAGON_BOAT)
         {
            UIModuleSmallLoading.Instance.hide();
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadCompleteHandler);
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUimoduleLoadProgress);
            if(null != this._func)
            {
               this._func.apply(null,this._funcParams);
            }
            this._func = null;
            this._funcParams = null;
         }
      }
      
      public function doOpenDragonBoatFrame() : void
      {
         var _loc1_:DragonBoatFrame = ComponentFactory.Instance.creatComponentByStylename("DragonBoatFrame");
         _loc1_.init2(this._activeInfo.ActiveID);
         LayerManager.Instance.addToLayer(_loc1_,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
      }
      
      public function createEntryBtn(param1:Sprite, param2:MovieClip, param3:BaseButton, param4:MovieClip) : void
      {
         this._entryBtn = new DragonBoatEntryBtn(param2,param3);
         var _loc5_:int = param1.getChildIndex(param4) - 1;
         param1.addChildAt(this._entryBtn,_loc5_);
      }
      
      public function disposeEntryBtn() : void
      {
         ObjectUtils.disposeObject(this._entryBtn);
         this._entryBtn = null;
      }
   }
}
