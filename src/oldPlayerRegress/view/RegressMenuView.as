package oldPlayerRegress.view
{
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.ScrollPanel;
   import com.pickgliss.ui.controls.container.VBox;
   import com.pickgliss.ui.image.ScaleFrameImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import ddt.data.UIModuleTypes;
   import ddt.data.quest.QuestCategory;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.LanguageMgr;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.manager.TaskManager;
   import ddt.utils.PositionUtils;
   import ddt.view.UIModuleSmallLoading;
   import flash.display.DisplayObject;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import oldPlayerRegress.event.RegressEvent;
   import oldPlayerRegress.view.itemView.TaskItemView;
   import oldPlayerRegress.view.itemView.WelcomeView;
   import oldPlayerRegress.view.itemView.call.CallView;
   import oldPlayerRegress.view.itemView.packs.PacksView;
   import oldPlayerRegress.view.itemView.task.RegressTaskView;
   import quest.QuestInfoPanelView;
   
   public class RegressMenuView extends Frame
   {
      
      public static var loadComplete:Boolean = false;
      
      public static var useFirst:Boolean = true;
       
      
      private const LIST_SPACE:int = -5;
      
      private const TASK_TYPE:int = 4;
      
      private var _bgArray:Array;
      
      private var _menuItemBgSelect:ScaleFrameImage;
      
      private var _textArray:Array;
      
      private var _textNameArray:Array;
      
      private var _btnArray:Array;
      
      private var _viewArray:Array;
      
      private var _taskInfoView:QuestInfoPanelView;
      
      private var _itemList:VBox;
      
      private var _listView:ScrollPanel;
      
      private var _taskData:QuestCategory;
      
      private var _expandBg:DisplayObject;
      
      private var _taskMenuItem:Vector.<TaskItemView>;
      
      public function RegressMenuView()
      {
         super();
         this.loadTaskUI();
      }
      
      private function loadTaskUI() : void
      {
         if(loadComplete)
         {
            this._init();
         }
         else if(useFirst)
         {
            UIModuleSmallLoading.Instance.progress = 0;
            UIModuleSmallLoading.Instance.show();
            UIModuleSmallLoading.Instance.addEventListener(Event.CLOSE,this.__onClose);
            UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.__onTaskLoadProgress);
            UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.__onTaskLoadComplete);
            UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.QUEST);
         }
      }
      
      private function __onTaskLoadProgress(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.QUEST)
         {
            UIModuleSmallLoading.Instance.progress = param1.loader.progress * 100;
         }
      }
      
      protected function __onClose(param1:Event) : void
      {
         UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this.__onClose);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.__onTaskLoadProgress);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.__onTaskLoadComplete);
         UIModuleSmallLoading.Instance.hide();
      }
      
      private function __onTaskLoadComplete(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.QUEST)
         {
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.__onTaskLoadComplete);
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.__onTaskLoadProgress);
            UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this.__onClose);
            UIModuleSmallLoading.Instance.hide();
            loadComplete = true;
            useFirst = false;
            this._init();
         }
      }
      
      private function _init() : void
      {
         this.initData();
         this.initView();
         this.initEvent();
      }
      
      private function initData() : void
      {
         this._bgArray = new Array(new ScaleFrameImage(),new ScaleFrameImage(),new ScaleFrameImage(),new ScaleFrameImage());
         this._textArray = new Array(new FilterFrameText(),new FilterFrameText(),new FilterFrameText(),new FilterFrameText());
         this._textNameArray = new Array("ddt.regress.menuView.Welcome","ddt.regress.menuView.Packs","ddt.regress.menuView.Call","ddt.regress.menuView.Task");
         this._btnArray = new Array(new BaseButton(),new BaseButton(),new BaseButton(),new BaseButton());
         this._viewArray = new Array(new WelcomeView(),new PacksView(),new CallView(),new RegressTaskView());
         this._taskData = TaskManager.instance.getAvailableQuests(this.TASK_TYPE);
         this._taskMenuItem = new Vector.<TaskItemView>();
      }
      
      private function initView() : void
      {
         this.setItemView(this._bgArray,"regress.ActivityCellBgUnSelected");
         this._menuItemBgSelect = ComponentFactory.Instance.creatComponentByStylename("regress.ActivityCellBgSelected");
         addChild(this._menuItemBgSelect);
         var _loc1_:int = 0;
         while(_loc1_ < this._textArray.length)
         {
            this._textArray[_loc1_] = ComponentFactory.Instance.creatComponentByStylename("regress.view.menuItemName.Text");
            this._textArray[_loc1_].text = LanguageMgr.GetTranslation(this._textNameArray[_loc1_]);
            if(_loc1_ > 0)
            {
               this._textArray[_loc1_].y = this._textArray[_loc1_ - 1].y + this._textArray[_loc1_].height + 7;
            }
            addChild(this._textArray[_loc1_]);
            _loc1_++;
         }
         this.setItemView(this._btnArray,"regress.button");
         if(this._taskData.list.length > 0)
         {
            this.addTaskListView();
         }
         else
         {
            this._bgArray[3].visible = false;
            this._btnArray[3].visible = false;
            this._textArray[3].visible = false;
         }
         QuestInfoPanelView.panelHeight = "RegressMenuView";
         this._taskInfoView = new QuestInfoPanelView();
         PositionUtils.setPos(this._taskInfoView,"regress.task.view.pos");
         addChild(this._taskInfoView);
         var _loc2_:int = 0;
         while(_loc2_ < this._viewArray.length)
         {
            addChild(this._viewArray[_loc2_]);
            if(_loc2_ > 0)
            {
               this._viewArray[_loc2_].visible = false;
            }
            _loc2_++;
         }
      }
      
      private function addTaskListView() : void
      {
         this._expandBg = ComponentFactory.Instance.creatCustomObject("regress.taskExpandBg");
         addChild(this._expandBg);
         this._itemList = new VBox();
         this._itemList.spacing = this.LIST_SPACE;
         this._listView = ComponentFactory.Instance.creat("regress.taskItemList");
         this._listView.setView(this._itemList);
         this._listView.vScrollProxy = ScrollPanel.AUTO;
         this._listView.hScrollProxy = ScrollPanel.OFF;
         addChild(this._listView);
         this.addListItem();
      }
      
      private function addListItem() : void
      {
         var _loc2_:TaskItemView = null;
         var _loc1_:int = 0;
         while(_loc1_ < this._taskData.list.length)
         {
            _loc2_ = new TaskItemView(this.taskMenuItemClick);
            _loc2_.titleField.text = this._taskData.list[_loc1_].Title;
            _loc2_.clickID = _loc1_;
            if(this._taskData.list[_loc1_].isCompleted)
            {
               _loc2_.bmpOK.visible = true;
            }
            _loc2_.width = _loc2_.displayWidth;
            _loc2_.height = _loc2_.displayHeight;
            this._itemList.addChild(_loc2_);
            this._taskMenuItem.push(_loc2_);
            _loc1_++;
         }
         this._itemList.arrange();
         this._listView.invalidateViewport();
      }
      
      protected function __onUpdateTaskMenuItem(param1:Event) : void
      {
         this._taskData = TaskManager.instance.getAvailableQuests(this.TASK_TYPE);
         var _loc2_:int = 0;
         while(_loc2_ < this._taskMenuItem.length)
         {
            this._itemList.removeChild(this._taskMenuItem[_loc2_]);
            _loc2_++;
         }
         this._taskMenuItem.length = 0;
         if(this._taskData.list.length > 0)
         {
            this.addListItem();
            this.taskMenuItemClick(0);
         }
         else
         {
            this._expandBg.visible = false;
            this._listView.visible = false;
            this._bgArray[3].visible = false;
            this._btnArray[3].visible = false;
            this._textArray[3].visible = false;
            this.menuItemClick(0);
         }
      }
      
      private function taskMenuItemClick(param1:int) : void
      {
         SoundManager.instance.playButtonSound();
         this._menuItemBgSelect.y = this._btnArray[3].y - 2;
         this.setViewVisible();
         this.setTaskMenuBgFrame();
         if(this._taskMenuItem.length == 0)
         {
            return;
         }
         this._taskMenuItem[param1].itemBg.setFrame(2);
         this._taskInfoView.visible = true;
         this._taskInfoView.regressFlag = true;
         this._taskInfoView.info = this._taskData.list[param1];
         this._viewArray[3].visible = true;
         this._viewArray[3].taskInfo = this._taskData.list[param1];
         this._viewArray[3].show();
      }
      
      private function setTaskMenuBgFrame() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ < this._taskMenuItem.length)
         {
            this._taskMenuItem[_loc1_].itemBg.setFrame(1);
            _loc1_++;
         }
      }
      
      private function setItemView(param1:Array, param2:String) : void
      {
         var _loc3_:int = 0;
         while(_loc3_ < param1.length)
         {
            param1[_loc3_] = ComponentFactory.Instance.creatComponentByStylename(param2);
            if(_loc3_ > 0)
            {
               param1[_loc3_].y = param1[_loc3_ - 1].y + param1[_loc3_].height + 4;
            }
            addChild(param1[_loc3_]);
            _loc3_++;
         }
      }
      
      private function initEvent() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ < this._btnArray.length)
         {
            this._btnArray[_loc1_].addEventListener(MouseEvent.CLICK,this.__onMenuItemClick);
            _loc1_++;
         }
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.QUEST_UPDATE,TaskManager.instance.__updateAcceptedTask);
         TaskManager.instance.addEventListener(RegressEvent.REGRESS_UPDATE_TASKMENUITEM,this.__onUpdateTaskMenuItem);
      }
      
      private function __onMenuItemClick(param1:MouseEvent) : void
      {
         var _loc2_:int = 0;
         while(_loc2_ < this._btnArray.length)
         {
            if(this._btnArray[_loc2_] == param1.currentTarget)
            {
               SoundManager.instance.playButtonSound();
               this.menuItemClick(_loc2_);
               break;
            }
            _loc2_++;
         }
      }
      
      private function menuItemClick(param1:int) : void
      {
         this._menuItemBgSelect.y = this._btnArray[param1].y - 2;
         this.setViewVisible();
         this._taskInfoView.visible = false;
         if(param1 == this._btnArray.length - 1)
         {
            this.taskMenuItemClick(0);
         }
         else
         {
            this._viewArray[param1].show();
         }
         if(param1 == 2)
         {
            SocketManager.Instance.out.sendRegressApplyEnable();
         }
      }
      
      private function setViewVisible() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ < this._viewArray.length)
         {
            this._viewArray[_loc1_].visible = false;
            _loc1_++;
         }
      }
      
      private function removeEvent() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ < this._btnArray.length)
         {
            this._btnArray[_loc1_].removeEventListener(MouseEvent.CLICK,this.__onMenuItemClick);
            _loc1_++;
         }
         TaskManager.instance.removeEventListener(RegressEvent.REGRESS_UPDATE_TASKMENUITEM,this.__onUpdateTaskMenuItem);
      }
      
      private function removeArray(param1:Array) : void
      {
         var _loc2_:int = 0;
         while(_loc2_ < param1.length)
         {
            if(param1[_loc2_])
            {
               param1[_loc2_].dispose();
               param1[_loc2_] = null;
            }
            _loc2_++;
         }
         param1.length = 0;
      }
      
      override public function dispose() : void
      {
         super.dispose();
         this.removeEvent();
         this.removeArray(this._bgArray);
         this.removeArray(this._textArray);
         this.removeArray(this._btnArray);
         this.removeArray(this._viewArray);
         var _loc1_:int = 0;
         while(_loc1_ < this._taskMenuItem.length)
         {
            if(this._taskMenuItem[_loc1_])
            {
               this._taskMenuItem[_loc1_].dispose();
               this._taskMenuItem[_loc1_] = null;
            }
            _loc1_++;
         }
         this._taskMenuItem.length = 0;
         if(this._menuItemBgSelect)
         {
            this._menuItemBgSelect.dispose();
            this._menuItemBgSelect = null;
         }
         if(this._taskInfoView)
         {
            this._taskInfoView.dispose();
            this._taskInfoView = null;
         }
         var _loc2_:int = 0;
         while(_loc2_ < this._textNameArray.length)
         {
            this._textNameArray[_loc2_] = null;
            _loc2_++;
         }
         this._textNameArray.length = 0;
         if(this._itemList)
         {
            this._itemList.dispose();
            this._itemList = null;
         }
         if(this._listView)
         {
            this._listView.dispose();
            this._listView = null;
         }
         if(this._taskData)
         {
            this._taskData = null;
         }
         if(this._expandBg)
         {
            this._expandBg = null;
         }
      }
   }
}
