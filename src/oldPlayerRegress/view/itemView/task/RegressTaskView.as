package oldPlayerRegress.view.itemView.task
{
   import bagAndInfo.BagAndInfoManager;
   import battleGroud.BattleGroudManager;
   import com.pickgliss.effect.AlphaShinerAnimation;
   import com.pickgliss.effect.EffectColorType;
   import com.pickgliss.effect.EffectManager;
   import com.pickgliss.effect.EffectTypes;
   import com.pickgliss.effect.IEffect;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.ComponentSetting;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.fightLib.FightLibInfo;
   import ddt.data.quest.QuestInfo;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.FightLibManager;
   import ddt.manager.GameInSocketOut;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerConfigManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.manager.StateManager;
   import ddt.manager.TaskManager;
   import ddt.states.StateType;
   import ddtActivityIcon.ActivitStateEvent;
   import ddtActivityIcon.DdtActivityIconManager;
   import fightLib.LessonType;
   import flash.events.MouseEvent;
   import flash.utils.getTimer;
   import labyrinth.LabyrinthManager;
   import oldPlayerRegress.view.RegressView;
   import road7th.comm.PackageIn;
   import room.RoomManager;
   import trainer.controller.WeakGuildManager;
   import trainer.data.Step;
   
   public class RegressTaskView extends Frame
   {
       
      
      private var _taskInfo:QuestInfo;
      
      private var _taskIdArray:Array;
      
      private var _funcArray:Array;
      
      private var _gotoFunc:Function;
      
      private var _isBattleOpen:Boolean;
      
      private var _bottomBtnBg:ScaleBitmapImage;
      
      private var _getAwardBtn:BaseButton;
      
      private var _gotoBtn:BaseButton;
      
      private var _questBtnShine:IEffect;
      
      private var _lastCreatTime:int;
      
      private var _bossState:int = 0;
      
      public function RegressTaskView()
      {
         super();
         this._init();
      }
      
      private function _init() : void
      {
         this.initData();
         this.initView();
         this.initEvent();
      }
      
      private function initData() : void
      {
         this._isBattleOpen = false;
         this._funcArray = new Array(this.notGo,this.gotoHall,this.gotoDungeon,this.gotoBagAndInfo,this.gotoPetView,this.gotoLabyrinth,this.gotoBattle,this.gotoBuried,this.gotoConsortiria,this.gotoTrain);
         this._taskIdArray = new Array([1519,1520,1528],[1524,1525],[1526,1527],[1521],[1523],[1517,1518],[1515],[1522],[1516],[1529,1530]);
         SocketManager.Instance.out.sendConsortiaBossInfo(1);
      }
      
      private function initView() : void
      {
         this._bottomBtnBg = ComponentFactory.Instance.creatComponentByStylename("regress.bottomBgImg");
         this.getAwardBtn = ComponentFactory.Instance.creat("regress.getAward");
         this.gotoBtn = ComponentFactory.Instance.creat("regress.goto");
         this.gotoBtn.visible = false;
         var _loc1_:Object = new Object();
         _loc1_[AlphaShinerAnimation.COLOR] = EffectColorType.GOLD;
         this._questBtnShine = EffectManager.Instance.creatEffect(EffectTypes.ALPHA_SHINER_ANIMATION,this.getAwardBtn,_loc1_);
         this._questBtnShine.stop();
         addChild(this._bottomBtnBg);
         addChild(this.getAwardBtn);
         addChild(this.gotoBtn);
      }
      
      private function initEvent() : void
      {
         this.getAwardBtn.addEventListener(MouseEvent.CLICK,this.__onGetAwardBtnClick);
         this.gotoBtn.addEventListener(MouseEvent.CLICK,this.__onGotoBtnClick);
         DdtActivityIconManager.Instance.addEventListener(DdtActivityIconManager.START,this.battleOpenHandler);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.BATTLE_OVER,this.battleOverHandler);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.CONSORTIA_BOSS_INFO,this.consortiaBossHandler);
      }
      
      protected function __onGetAwardBtnClick(param1:MouseEvent) : void
      {
         var _loc5_:BaseAlerFrame = null;
         if(!this.taskInfo)
         {
            return;
         }
         SoundManager.instance.playButtonSound();
         var _loc2_:QuestInfo = this.taskInfo;
         var _loc3_:int = PlayerManager.Instance.Self.BandMoney;
         var _loc4_:int = ServerConfigManager.instance.getBindBidLimit(PlayerManager.Instance.Self.Grade,PlayerManager.Instance.Self.VIPLevel);
         if(_loc2_.RewardBindMoney != 0 && _loc2_.RewardBindMoney + PlayerManager.Instance.Self.BandMoney > ServerConfigManager.instance.getBindBidLimit(PlayerManager.Instance.Self.Grade,PlayerManager.Instance.Self.VIPLevel))
         {
            _loc5_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("tips"),LanguageMgr.GetTranslation("ddt.BindBid.tip"),LanguageMgr.GetTranslation("shop.PresentFrame.OkBtnText"),LanguageMgr.GetTranslation("shop.PresentFrame.CancelBtnText"),false,false,true,1);
            _loc5_.addEventListener(FrameEvent.RESPONSE,this.__onResponse);
         }
         else
         {
            this.finishQuest(_loc2_);
         }
      }
      
      protected function __onGotoBtnClick(param1:MouseEvent) : void
      {
         if(this._gotoFunc != null)
         {
            this._gotoFunc();
         }
      }
      
      private function finishQuest(param1:QuestInfo) : void
      {
         if(param1 && !param1.isCompleted)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("tank.view.task.TaskCatalogContentView.dropTaskIII"));
            return;
         }
         if(param1)
         {
            TaskManager.instance.sendQuestFinish(param1.QuestID);
         }
      }
      
      private function __onResponse(param1:FrameEvent) : void
      {
         param1.currentTarget.removeEventListener(FrameEvent.RESPONSE,this.__onResponse);
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK)
         {
            this.finishQuest(this.taskInfo);
         }
         ObjectUtils.disposeObject(param1.currentTarget);
      }
      
      private function removeEvent() : void
      {
         this.getAwardBtn.removeEventListener(MouseEvent.CLICK,this.__onGetAwardBtnClick);
         this.gotoBtn.removeEventListener(MouseEvent.CLICK,this.__onGotoBtnClick);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.CONSORTIA_BOSS_INFO,this.consortiaBossHandler);
         DdtActivityIconManager.Instance.removeEventListener(DdtActivityIconManager.START,this.battleOpenHandler);
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.BATTLE_OVER,this.battleOverHandler);
      }
      
      public function show() : void
      {
         this.visible = true;
      }
      
      private function setModuleInfo() : void
      {
         this._gotoFunc = this._funcArray[this.getFuncID(this.taskInfo._conditions[0].questID)];
         if(this.taskInfo.isCompleted)
         {
            this.getAwardBtn.visible = true;
            this.getAwardBtn.enable = true;
            this.gotoBtn.visible = false;
            this.questBtnShine.play();
         }
         else if(this._gotoFunc == this.notGo)
         {
            this.getAwardBtn.visible = true;
            this.getAwardBtn.enable = false;
            this.gotoBtn.visible = false;
            this.questBtnShine.stop();
         }
         else if(this._gotoFunc == this.gotoBattle)
         {
            this.getAwardBtn.visible = false;
            this.gotoBtn.visible = true;
            this.gotoBtn.enable = this._isBattleOpen;
            this.questBtnShine.stop();
         }
         else if(this._gotoFunc == this.gotoConsortiria)
         {
            this.getAwardBtn.visible = false;
            this.gotoBtn.visible = true;
            this.gotoBtn.enable = this._bossState == 1?Boolean(true):Boolean(false);
            this.questBtnShine.stop();
         }
         else
         {
            this.getAwardBtn.visible = false;
            this.gotoBtn.visible = true;
            this.gotoBtn.enable = true;
            this.questBtnShine.stop();
         }
      }
      
      private function getFuncID(param1:int) : int
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         while(_loc3_ < this._taskIdArray.length)
         {
            if(this._taskIdArray[_loc3_].indexOf(param1) != -1)
            {
               _loc2_ = _loc3_;
               break;
            }
            _loc3_++;
         }
         return _loc2_;
      }
      
      private function notGo() : void
      {
      }
      
      private function gotoHall() : void
      {
         if(!WeakGuildManager.Instance.checkOpen(Step.GAME_ROOM_OPEN,2))
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.functionLimitTip",2));
            return;
         }
         StateManager.setState(StateType.ROOM_LIST);
         ComponentSetting.SEND_USELOG_ID(3);
         if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.GAME_ROOM_OPEN) && !PlayerManager.Instance.Self.IsWeakGuildFinish(Step.GAME_ROOM_CLICKED))
         {
            SocketManager.Instance.out.syncWeakStep(Step.GAME_ROOM_CLICKED);
         }
      }
      
      private function gotoDungeon() : void
      {
         if(!WeakGuildManager.Instance.checkOpen(Step.DUNGEON_OPEN,8))
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.functionLimitTip",8));
            return;
         }
         if(!PlayerManager.Instance.checkEnterDungeon)
         {
            return;
         }
         StateManager.setState(StateType.DUNGEON_LIST);
         ComponentSetting.SEND_USELOG_ID(4);
         if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.DUNGEON_OPEN) && !PlayerManager.Instance.Self.IsWeakGuildFinish(Step.DUNGEON_CLICKED))
         {
            SocketManager.Instance.out.syncWeakStep(Step.DUNGEON_CLICKED);
         }
      }
      
      private function gotoBagAndInfo() : void
      {
         BagAndInfoManager.Instance.showBagAndInfo();
         (parent.parent.parent as RegressView).dispatchEvent(new FrameEvent(FrameEvent.CLOSE_CLICK));
      }
      
      private function gotoPetView() : void
      {
         BagAndInfoManager.Instance.showBagAndInfo(3);
         (parent.parent.parent as RegressView).dispatchEvent(new FrameEvent(FrameEvent.CLOSE_CLICK));
      }
      
      private function gotoLabyrinth() : void
      {
         if(!WeakGuildManager.Instance.checkOpen(Step.TOFF_LIST_OPEN,30))
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.functionLimitTip",30));
            return;
         }
         LabyrinthManager.Instance.show();
         (parent.parent.parent as RegressView).dispatchEvent(new FrameEvent(FrameEvent.CLOSE_CLICK));
      }
      
      private function battleOpenHandler(param1:ActivitStateEvent) : void
      {
         var _loc2_:Array = param1.data as Array;
         var _loc3_:int = _loc2_[0];
         if(_loc3_ == 5)
         {
            this._isBattleOpen = true;
            if(this._gotoFunc == this.gotoBattle)
            {
               this.gotoBtn.enable = true;
            }
         }
      }
      
      private function battleOverHandler(param1:CrazyTankSocketEvent) : void
      {
         this._isBattleOpen = false;
         if(this._gotoFunc == this.gotoBattle)
         {
            this.gotoBtn.enable = false;
         }
      }
      
      private function gotoBattle() : void
      {
         SoundManager.instance.play("008");
         if(PlayerManager.Instance.Self.Grade < 20)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.functionLimitTip",20));
            return;
         }
         if(PlayerManager.Instance.Self.Bag.getItemAt(6) == null)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("tank.room.RoomIIController.weapon"));
            return;
         }
         if(getTimer() - this._lastCreatTime > 1000)
         {
            this._lastCreatTime = getTimer();
            BattleGroudManager.Instance.addBattleSingleRoom();
         }
         (parent.parent.parent as RegressView).dispatchEvent(new FrameEvent(FrameEvent.CLOSE_CLICK));
      }
      
      private function gotoBuried() : void
      {
         SoundManager.instance.playButtonSound();
         SocketManager.Instance.out.enterBuried();
      }
      
      private function consortiaBossHandler(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         this._bossState = _loc2_.readByte();
         if(this._gotoFunc == this.gotoConsortiria)
         {
            this.gotoBtn.enable = this._bossState == 1?Boolean(true):Boolean(false);
         }
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.CONSORTIA_BOSS_INFO,this.consortiaBossHandler);
      }
      
      private function gotoConsortiria() : void
      {
         if(this._bossState == 1)
         {
            if(!WeakGuildManager.Instance.checkOpen(Step.CONSORTIA_OPEN,7))
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.functionLimitTip",7));
               return;
            }
            StateManager.setState(StateType.CONSORTIA);
            ComponentSetting.SEND_USELOG_ID(5);
            if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.CONSORTIA_OPEN) && !PlayerManager.Instance.Self.IsWeakGuildFinish(Step.CONSORTIA_CLICKED))
            {
               SocketManager.Instance.out.syncWeakStep(Step.CONSORTIA_CLICKED);
            }
         }
      }
      
      private function gotoTrain() : void
      {
         SoundManager.instance.play("008");
         if(PlayerManager.Instance.Self.WeaponID <= 0)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("tank.room.RoomIIController.weapon"));
            return;
         }
         TaskManager.instance.guideId = this.taskInfo.MapID;
         SocketManager.Instance.out.createUserGuide(5);
         RoomManager.Instance.addEventListener(CrazyTankSocketEvent.GAME_ROOM_CREATE,this.__gameStart);
      }
      
      private function __gameStart(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         RoomManager.Instance.removeEventListener(CrazyTankSocketEvent.GAME_ROOM_CREATE,this.__gameStart);
         switch(TaskManager.instance.guideId)
         {
            case 10:
               _loc2_ = FightLibInfo.EASY;
               _loc3_ = LessonType.Measure;
               break;
            case 11:
               _loc2_ = FightLibInfo.NORMAL;
               _loc3_ = LessonType.Measure;
               break;
            case 12:
               _loc2_ = FightLibInfo.DIFFICULT;
               _loc3_ = LessonType.Measure;
               break;
            case 13:
               _loc2_ = FightLibInfo.EASY;
               _loc3_ = LessonType.Twenty;
               break;
            case 14:
               _loc2_ = FightLibInfo.NORMAL;
               _loc3_ = LessonType.Twenty;
               break;
            case 15:
               _loc2_ = FightLibInfo.DIFFICULT;
               _loc3_ = LessonType.Twenty;
               break;
            case 16:
               _loc2_ = FightLibInfo.EASY;
               _loc3_ = LessonType.SixtyFive;
               break;
            case 17:
               _loc2_ = FightLibInfo.NORMAL;
               _loc3_ = LessonType.SixtyFive;
               break;
            case 18:
               _loc2_ = FightLibInfo.DIFFICULT;
               _loc3_ = LessonType.SixtyFive;
               break;
            case 19:
               _loc2_ = FightLibInfo.EASY;
               _loc3_ = LessonType.HighThrow;
               break;
            case 20:
               _loc2_ = FightLibInfo.NORMAL;
               _loc3_ = LessonType.HighThrow;
               break;
            case 21:
               _loc2_ = FightLibInfo.DIFFICULT;
               _loc3_ = LessonType.HighThrow;
               break;
            case 22:
               _loc2_ = FightLibInfo.EASY;
               _loc3_ = LessonType.HighGap;
               break;
            case 23:
               _loc2_ = FightLibInfo.NORMAL;
               _loc3_ = LessonType.HighGap;
               break;
            case 24:
               _loc2_ = FightLibInfo.DIFFICULT;
               _loc3_ = LessonType.HighGap;
         }
         _loc4_ = this.getSecondType(_loc3_,_loc2_);
         GameInSocketOut.sendGameRoomSetUp(_loc3_,5,false,"","",_loc4_,_loc2_,0,false,0);
         FightLibManager.Instance.currentInfoID = _loc3_;
         FightLibManager.Instance.currentInfo.difficulty = _loc2_;
         StateManager.setState(StateType.FIGHT_LIB);
      }
      
      private function getSecondType(param1:int, param2:int) : int
      {
         var _loc3_:int = 0;
         if(param1 == LessonType.Twenty || param1 == LessonType.SixtyFive || param1 == LessonType.HighThrow)
         {
            if(param2 == FightLibInfo.EASY)
            {
               _loc3_ = 6;
            }
            else if(param2 == FightLibInfo.NORMAL)
            {
               _loc3_ = 5;
            }
            else
            {
               _loc3_ = 3;
            }
         }
         else if(param1 == LessonType.HighGap)
         {
            if(param2 == FightLibInfo.EASY)
            {
               _loc3_ = 5;
            }
            else if(param2 == FightLibInfo.NORMAL)
            {
               _loc3_ = 4;
            }
            else
            {
               _loc3_ = 3;
            }
         }
         return _loc3_;
      }
      
      override public function dispose() : void
      {
         super.dispose();
         this.removeEvent();
         this._gotoFunc = null;
         if(this._bottomBtnBg)
         {
            this._bottomBtnBg.dispose();
            this._bottomBtnBg = null;
         }
         if(this.getAwardBtn)
         {
            this.getAwardBtn.dispose();
            this.getAwardBtn = null;
         }
         if(this.gotoBtn)
         {
            this.gotoBtn.dispose();
            this.gotoBtn = null;
         }
         if(this.questBtnShine)
         {
            this.questBtnShine.dispose();
            this.questBtnShine = null;
         }
         if(this.taskInfo)
         {
            this.taskInfo = null;
         }
         var _loc1_:int = 0;
         while(_loc1_ < this._taskIdArray.length)
         {
            this._taskIdArray[_loc1_] = null;
            _loc1_++;
         }
         this._taskIdArray.length = 0;
         var _loc2_:int = 0;
         while(_loc2_ < this._funcArray.length)
         {
            this._funcArray[_loc2_] = null;
            _loc2_++;
         }
         this._funcArray.length = 0;
      }
      
      public function get taskInfo() : QuestInfo
      {
         return this._taskInfo;
      }
      
      public function set taskInfo(param1:QuestInfo) : void
      {
         this._taskInfo = param1;
         if(this.taskInfo)
         {
            this.setModuleInfo();
         }
      }
      
      public function get getAwardBtn() : BaseButton
      {
         return this._getAwardBtn;
      }
      
      public function set getAwardBtn(param1:BaseButton) : void
      {
         this._getAwardBtn = param1;
      }
      
      public function get questBtnShine() : IEffect
      {
         return this._questBtnShine;
      }
      
      public function set questBtnShine(param1:IEffect) : void
      {
         this._questBtnShine = param1;
      }
      
      public function get gotoBtn() : BaseButton
      {
         return this._gotoBtn;
      }
      
      public function set gotoBtn(param1:BaseButton) : void
      {
         this._gotoBtn = param1;
      }
   }
}
