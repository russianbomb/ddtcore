package oldPlayerRegress.view.itemView
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.TextButton;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import com.pickgliss.ui.image.ScaleFrameImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import ddt.manager.LanguageMgr;
   import ddt.manager.PathManager;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.events.MouseEvent;
   
   public class WelcomeView extends Frame
   {
       
      
      private var _titleBg:Bitmap;
      
      private var _welTitleImg:ScaleFrameImage;
      
      private var _privilegeBtn:TextButton;
      
      private var _welDescript:FilterFrameText;
      
      private var _firstDes:FilterFrameText;
      
      private var _secondDes:FilterFrameText;
      
      private var _thirdDes:FilterFrameText;
      
      private var _firstDesData:FilterFrameText;
      
      private var _firstDesDataVector:Vector.<FilterFrameText>;
      
      private var _bottomBtnBg:ScaleBitmapImage;
      
      private var _secondDesData:FilterFrameText;
      
      private var _thirdDesData:FilterFrameText;
      
      private var _threeDes:FilterFrameText;
      
      private var _threeDesData:FilterFrameText;
      
      public function WelcomeView()
      {
         super();
         this._init();
      }
      
      private function _init() : void
      {
         this.initView();
         this.initEvent();
      }
      
      private function initView() : void
      {
         this._titleBg = ComponentFactory.Instance.creat("asset.regress.titleBg");
         this._welTitleImg = ComponentFactory.Instance.creatComponentByStylename("regress.WelTitle");
         this._privilegeBtn = ComponentFactory.Instance.creatComponentByStylename("regress.privilegeBtn");
         this._privilegeBtn.text = LanguageMgr.GetTranslation("ddt.regress.welView.Privilege");
         this._welDescript = ComponentFactory.Instance.creatComponentByStylename("regress.Description");
         this._welDescript.text = LanguageMgr.GetTranslation("ddt.regress.welView.Descript.3p",PathManager.russiaLanguage);
         this._firstDesDataVector = new Vector.<FilterFrameText>();
         this._welDescript.text = LanguageMgr.GetTranslation("ddt.regress.welView.Descript");
         this._firstDes = ComponentFactory.Instance.creatComponentByStylename("regress.first");
         this._firstDes.text = LanguageMgr.GetTranslation("ddt.regress.welView.First");
         this._secondDes = ComponentFactory.Instance.creatComponentByStylename("regress.second");
         this._secondDes.text = LanguageMgr.GetTranslation("ddt.regress.welView.Second",PathManager.russiaLanguage);
         this._thirdDes = ComponentFactory.Instance.creatComponentByStylename("regress.third");
         this._thirdDes.text = LanguageMgr.GetTranslation("ddt.regress.welView.Third");
         this._firstDesData = ComponentFactory.Instance.creatComponentByStylename("regress.firstDes");
         this._firstDesData.text = LanguageMgr.GetTranslation("ddt.regress.welView.FirstData");
         this._secondDesData = ComponentFactory.Instance.creatComponentByStylename("regress.secondDes");
         this._secondDesData.text = LanguageMgr.GetTranslation("ddt.regress.welView.SecondData");
         this._thirdDesData = ComponentFactory.Instance.creatComponentByStylename("regress.thirdDes");
         this._thirdDesData.text = LanguageMgr.GetTranslation("ddt.regress.welView.ThirdData");
         this._threeDes = ComponentFactory.Instance.creatComponentByStylename("regress.three");
         this._threeDes.text = LanguageMgr.GetTranslation("ddt.regress.welView.Three");
         this._threeDesData = ComponentFactory.Instance.creatComponentByStylename("regress.threeDes");
         this._threeDesData.text = LanguageMgr.GetTranslation("ddt.regress.welView.ThreeData");
         PositionUtils.setPos(this._firstDesData,"regress.firstDes.pos");
         addToContent(this._titleBg);
         addToContent(this._welTitleImg);
         addToContent(this._privilegeBtn);
         addToContent(this._welDescript);
         addToContent(this._firstDes);
         addToContent(this._secondDes);
         addToContent(this._firstDesData);
         addToContent(this._secondDesData);
         addToContent(this._threeDes);
         addToContent(this._threeDesData);
      }
      
      private function initEvent() : void
      {
         this._privilegeBtn.addEventListener(MouseEvent.CLICK,this.__onPrivilegeClick);
      }
      
      protected function __onPrivilegeClick(param1:MouseEvent) : void
      {
         SoundManager.instance.playButtonSound();
         var _loc2_:WelFrameView = ComponentFactory.Instance.creatComponentByStylename("regress.privilegeAssetFrame");
         LayerManager.Instance.addToLayer(_loc2_,LayerManager.GAME_TOP_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
      }
      
      private function removeEvent() : void
      {
         this._privilegeBtn.removeEventListener(MouseEvent.CLICK,this.__onPrivilegeClick);
      }
      
      public function show() : void
      {
         this.visible = true;
      }
      
      override public function dispose() : void
      {
         super.dispose();
         this.removeEvent();
         if(this._titleBg)
         {
            this._titleBg = null;
         }
         if(this._welTitleImg)
         {
            this._welTitleImg.dispose();
            this._welTitleImg = null;
         }
         if(this._privilegeBtn)
         {
            this._privilegeBtn.dispose();
            this._privilegeBtn = null;
         }
         if(this._welDescript)
         {
            this._welDescript.dispose();
            this._welDescript = null;
         }
         if(this._firstDes)
         {
            this._firstDes.dispose();
            this._firstDes = null;
         }
         if(this._secondDes)
         {
            this._secondDes.dispose();
            this._secondDes = null;
         }
         if(this._firstDesData)
         {
            this._firstDesData.dispose();
            this._firstDesData = null;
         }
         if(this._secondDesData)
         {
            this._secondDesData.dispose();
            this._secondDesData = null;
         }
      }
   }
}
