package oldPlayerRegress.view.itemView.packs
{
   import bagAndInfo.cell.BagCell;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.manager.ItemManager;
   
   public class PacksGiftView extends Frame
   {
       
      
      private var _packsGiftBgArray:Array;
      
      private var _giftContentList:Vector.<BagCell>;
      
      private var _getGiftData:Vector.<GiftData>;
      
      public function PacksGiftView(param1:Vector.<GiftData> = null)
      {
         super();
         this.getGiftData = param1;
         this._init();
      }
      
      private function _init() : void
      {
         this.initData();
         this.initView();
         this.initEvent();
      }
      
      private function initData() : void
      {
         this._packsGiftBgArray = new Array(new ScaleBitmapImage(),new ScaleBitmapImage(),new ScaleBitmapImage(),new ScaleBitmapImage(),new ScaleBitmapImage(),new ScaleBitmapImage(),new ScaleBitmapImage(),new ScaleBitmapImage());
         this._giftContentList = new Vector.<BagCell>();
      }
      
      private function initView() : void
      {
         var _loc1_:int = 0;
         var _loc2_:BagCell = null;
         _loc1_ = 0;
         while(_loc1_ < this._packsGiftBgArray.length)
         {
            this._packsGiftBgArray[_loc1_] = ComponentFactory.Instance.creatComponentByStylename("regress.packstemCellBg");
            if(_loc1_ > 0)
            {
               this._packsGiftBgArray[_loc1_].x = this._packsGiftBgArray[_loc1_ - 1].x + this._packsGiftBgArray[_loc1_ - 1].width + 8;
            }
            addChild(this._packsGiftBgArray[_loc1_]);
            _loc2_ = new BagCell(_loc1_);
            _loc2_.x = this._packsGiftBgArray[_loc1_].x;
            _loc2_.y = this._packsGiftBgArray[_loc1_].y;
            this._giftContentList.push(_loc2_);
            _loc1_++;
         }
         this.setGiftInfo();
      }
      
      public function setGoods(param1:int) : InventoryItemInfo
      {
         var _loc2_:InventoryItemInfo = new InventoryItemInfo();
         _loc2_.TemplateID = this.getGiftData[param1].giftID;
         _loc2_ = ItemManager.fill(_loc2_);
         _loc2_.IsBinds = true;
         return _loc2_;
      }
      
      public function setGiftInfo() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ < this._giftContentList.length)
         {
            if(this.getGiftData)
            {
               if(_loc1_ < this.getGiftData.length)
               {
                  this._giftContentList[_loc1_].info = this.setGoods(_loc1_);
                  this._giftContentList[_loc1_].setCount(this.getGiftData[_loc1_].giftCount);
                  addChild(this._giftContentList[_loc1_]);
               }
            }
            _loc1_++;
         }
      }
      
      public function removeGiftChild() : void
      {
         var _loc1_:int = 0;
         if(this.getGiftData)
         {
            _loc1_ = 0;
            while(_loc1_ < this.getGiftData.length && _loc1_ < 8)
            {
               removeChild(this._giftContentList[_loc1_]);
               _loc1_++;
            }
         }
      }
      
      private function initEvent() : void
      {
      }
      
      private function removeEvent() : void
      {
      }
      
      override public function dispose() : void
      {
         var _loc2_:int = 0;
         super.dispose();
         this.removeEvent();
         var _loc1_:int = 0;
         while(_loc1_ < this._packsGiftBgArray.length)
         {
            this._packsGiftBgArray[_loc1_] = null;
            _loc1_++;
         }
         if(this.getGiftData)
         {
            _loc2_ = 0;
            while(_loc2_ < this.getGiftData.length && _loc1_ < 8)
            {
               this._giftContentList[_loc2_] = null;
               _loc2_++;
            }
         }
         if(this._getGiftData)
         {
            this._getGiftData = null;
         }
      }
      
      public function get getGiftData() : Vector.<GiftData>
      {
         return this._getGiftData;
      }
      
      public function set getGiftData(param1:Vector.<GiftData>) : void
      {
         this._getGiftData = param1;
      }
   }
}
