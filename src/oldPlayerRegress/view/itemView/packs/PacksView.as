package oldPlayerRegress.view.itemView.packs
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import com.pickgliss.ui.image.ScaleFrameImage;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.events.MouseEvent;
   import road7th.comm.PackageIn;
   
   public class PacksView extends Frame
   {
       
      
      private var _titleBg:Bitmap;
      
      private var _packTitle:ScaleFrameImage;
      
      private var _titleBgII:Bitmap;
      
      private var _openPacks:ScaleFrameImage;
      
      private var _bottomBtnBg:ScaleBitmapImage;
      
      private var _getAwardBtn:BaseButton;
      
      private var _packsBg:ScaleBitmapImage;
      
      private var _packsSelect:ScaleFrameImage;
      
      private var _btnArray:Array;
      
      private var _recvArray:Array;
      
      private var _packsGiftView:PacksGiftView;
      
      private var _pakcsGiftData:Array;
      
      private var _clickID:int = 0;
      
      private var _dayNum:int = 0;
      
      private var _pageID:int = 0;
      
      private var _numID:int = 0;
      
      public function PacksView()
      {
         super();
         SocketManager.Instance.out.sendRegressPkg();
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.REGRESS_PACKS,this.__getPacksInfo);
      }
      
      private function __getPacksInfo(param1:CrazyTankSocketEvent) : void
      {
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:Vector.<GiftData> = null;
         var _loc11_:int = 0;
         var _loc12_:int = 0;
         var _loc13_:GiftData = null;
         SocketManager.Instance.removeEventListener(CrazyTankSocketEvent.REGRESS_PACKS,this.__getPacksInfo);
         this.removeVariable();
         var _loc2_:PackageIn = param1.pkg;
         this._dayNum = _loc2_.readInt();
         var _loc3_:int = _loc2_.readInt();
         _loc3_ = 6;
         this._pageID = int((this._dayNum - 1) / _loc3_);
         this._numID = (this._dayNum - 1) % _loc3_;
         this._init();
         var _loc4_:int = 0;
         while(_loc4_ < 15)
         {
            _loc6_ = _loc2_.readByte();
            if(_loc4_ < this._pageID * 6)
            {
               _loc7_ = _loc2_.readInt();
               _loc8_ = 0;
               while(_loc8_ < _loc7_)
               {
                  _loc2_.readInt();
                  _loc2_.readInt();
                  _loc8_++;
               }
            }
            else
            {
               if(_loc6_ != 0)
               {
                  _loc12_ = _loc4_ % 6;
                  this._recvArray[_loc12_].visible = true;
                  this._btnArray[_loc12_].removeEventListener(MouseEvent.CLICK,this.__onBtnClick);
                  (this._btnArray[_loc12_] as BaseButton).mouseEnabled = false;
               }
               _loc9_ = _loc2_.readInt();
               _loc10_ = new Vector.<GiftData>();
               _loc11_ = 0;
               while(_loc11_ < _loc9_)
               {
                  _loc13_ = new GiftData();
                  _loc13_.giftID = _loc2_.readInt();
                  _loc13_.giftCount = _loc2_.readInt();
                  _loc10_.push(_loc13_);
                  _loc11_++;
               }
               this._pakcsGiftData.push(_loc10_);
            }
            _loc4_++;
         }
         this._packsGiftView = new PacksGiftView();
         PositionUtils.setPos(this._packsGiftView,"regress.pakcs.gift.pos");
         addToContent(this._packsGiftView);
         _loc5_ = 0;
         while(_loc5_ <= this._numID)
         {
            if(this._btnArray[_loc5_].mouseEnabled == true)
            {
               this._packsSelect.visible = true;
               this._packsSelect.x = this._btnArray[_loc5_].x;
               this._packsSelect.y = this._btnArray[_loc5_].y;
               this._clickID = this._pageID * 6 + _loc5_;
               this._getAwardBtn.enable = true;
               this._packsGiftView.removeGiftChild();
               this._packsGiftView.getGiftData = this._pakcsGiftData[_loc5_];
               this._packsGiftView.setGiftInfo();
               break;
            }
            if(_loc5_ == this._numID)
            {
               this._getAwardBtn.enable = false;
               this._packsGiftView.removeGiftChild();
               if(this._numID + 1 < this._btnArray.length)
               {
                  this._clickID = this._numID + 1;
                  this._packsSelect.visible = true;
                  this._packsSelect.x = this._btnArray[this._clickID].x;
                  this._packsSelect.y = this._btnArray[this._clickID].y;
                  this._packsGiftView.getGiftData = this._pakcsGiftData[this._clickID];
                  this._packsGiftView.setGiftInfo();
               }
            }
            _loc5_++;
         }
      }
      
      private function _init() : void
      {
         this.initData();
         this.initView();
         this.initEvent();
      }
      
      private function initData() : void
      {
         this._pakcsGiftData = new Array();
         this._btnArray = new Array(new BaseButton(),new BaseButton(),new BaseButton(),new BaseButton(),new BaseButton(),new BaseButton());
         this._recvArray = new Array(new ScaleFrameImage(),new ScaleFrameImage(),new ScaleFrameImage(),new ScaleFrameImage(),new ScaleFrameImage(),new ScaleFrameImage());
      }
      
      private function initView() : void
      {
         this._titleBg = ComponentFactory.Instance.creat("asset.regress.titleBg");
         this._packTitle = ComponentFactory.Instance.creatComponentByStylename("regress.packTitle");
         this._titleBgII = ComponentFactory.Instance.creat("asset.regress.titleBg");
         PositionUtils.setPos(this._titleBgII,"regress.packs.titleII.pos");
         this._openPacks = ComponentFactory.Instance.creatComponentByStylename("regress.openPacks");
         this._bottomBtnBg = ComponentFactory.Instance.creatComponentByStylename("regress.bottomBgImg");
         this._packsBg = ComponentFactory.Instance.creatComponentByStylename("regress.packsBg");
         this._getAwardBtn = ComponentFactory.Instance.creat("regress.getAward");
         this._getAwardBtn.enable = false;
         if(this._dayNum > 12)
         {
            this._btnArray.length = 3;
         }
         var _loc1_:int = 0;
         while(_loc1_ < this._btnArray.length)
         {
            this._btnArray[_loc1_] = ComponentFactory.Instance.creatComponentByStylename("regress.giftAward" + String(_loc1_ + 1));
            addToContent(this._btnArray[_loc1_]);
            this._recvArray[_loc1_] = ComponentFactory.Instance.creatComponentByStylename("regress.packsReceived");
            this._recvArray[_loc1_].x = this._btnArray[_loc1_].x + 50;
            this._recvArray[_loc1_].y = this._btnArray[_loc1_].y - 4;
            addToContent(this._recvArray[_loc1_]);
            _loc1_++;
         }
         this._packsSelect = ComponentFactory.Instance.creatComponentByStylename("regress.packsSelected");
         addToContent(this._titleBg);
         addToContent(this._packTitle);
         addToContent(this._titleBgII);
         addToContent(this._openPacks);
         addToContent(this._bottomBtnBg);
         addToContent(this._packsBg);
         addToContent(this._getAwardBtn);
         addToContent(this._packsSelect);
      }
      
      private function initEvent() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ < this._btnArray.length)
         {
            this._btnArray[_loc1_].addEventListener(MouseEvent.CLICK,this.__onBtnClick);
            _loc1_++;
         }
         this._getAwardBtn.addEventListener(MouseEvent.CLICK,this.__onGetAwardClick);
      }
      
      protected function __onGetAwardClick(param1:MouseEvent) : void
      {
         SocketManager.Instance.out.sendRegressGetAwardPkg(this._clickID);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.REGRESS_PACKS,this.__getPacksInfo);
      }
      
      private function __onBtnClick(param1:MouseEvent) : void
      {
         var _loc2_:int = 0;
         this._packsSelect.visible = true;
         _loc2_ = 0;
         while(_loc2_ < this._btnArray.length)
         {
            if(this._btnArray[_loc2_] == param1.currentTarget)
            {
               SoundManager.instance.playButtonSound();
               this._clickID = this._pageID * 6 + _loc2_;
               this._getAwardBtn.enable = true;
               if(this._recvArray[_loc2_].visible || _loc2_ > this._numID)
               {
                  this._getAwardBtn.enable = false;
               }
               this._packsSelect.x = this._btnArray[_loc2_].x;
               this._packsSelect.y = this._btnArray[_loc2_].y;
               this._packsGiftView.removeGiftChild();
               this._packsGiftView.getGiftData = this._pakcsGiftData[_loc2_];
               this._packsGiftView.setGiftInfo();
               break;
            }
            _loc2_++;
         }
      }
      
      public function show() : void
      {
         this.visible = true;
      }
      
      private function removeEvent() : void
      {
         var _loc1_:int = 0;
         if(this._btnArray)
         {
            _loc1_ = 0;
            while(_loc1_ < this._btnArray.length)
            {
               this._btnArray[_loc1_].removeEventListener(MouseEvent.CLICK,this.__onBtnClick);
               _loc1_++;
            }
         }
      }
      
      override public function dispose() : void
      {
         super.dispose();
         this.removeEvent();
         this.removeVariable();
      }
      
      private function removeVariable() : void
      {
         var _loc1_:int = 0;
         this._clickID = 0;
         this._dayNum = 0;
         if(this._titleBg)
         {
            this._titleBg = null;
         }
         if(this._packTitle)
         {
            this._packTitle.dispose();
            this._packTitle = null;
         }
         if(this._titleBgII)
         {
            this._titleBgII = null;
         }
         if(this._openPacks)
         {
            this._openPacks.dispose();
            this._openPacks = null;
         }
         if(this._bottomBtnBg)
         {
            this._bottomBtnBg.dispose();
            this._bottomBtnBg = null;
         }
         if(this._getAwardBtn)
         {
            this._getAwardBtn.dispose();
            this._getAwardBtn = null;
         }
         if(this._packsBg)
         {
            this._packsBg.dispose();
            this._packsBg = null;
         }
         if(this._packsSelect)
         {
            this._packsSelect.dispose();
            this._packsSelect = null;
         }
         if(this._packsGiftView)
         {
            this._packsGiftView.dispose();
            this._packsGiftView = null;
         }
         if(this._pakcsGiftData)
         {
            _loc1_ = 0;
            while(_loc1_ < this._pakcsGiftData.length)
            {
               this._pakcsGiftData[_loc1_] = null;
               _loc1_++;
            }
            this._pakcsGiftData.length = 0;
         }
         this.removeArray(this._btnArray);
         this.removeArray(this._recvArray);
      }
      
      private function removeArray(param1:Array) : void
      {
         var _loc2_:int = 0;
         if(param1)
         {
            _loc2_ = 0;
            while(_loc2_ < param1.length)
            {
               if(param1[_loc2_])
               {
                  param1[_loc2_].dispose();
                  param1[_loc2_] = null;
               }
               _loc2_++;
            }
            param1.length = 0;
         }
      }
   }
}
