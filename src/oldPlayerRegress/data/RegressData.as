package oldPlayerRegress.data
{
   import oldPlayerRegress.RegressManager;
   import oldPlayerRegress.event.RegressEvent;
   import road7th.comm.PackageIn;
   
   public class RegressData
   {
      
      public static var isApplyEnable:Boolean;
      
      public static var isCallEnable:Boolean;
      
      public static var isFirstLogin:Boolean = true;
      
      public static var isOver:Boolean = true;
       
      
      public function RegressData()
      {
         super();
      }
      
      public static function recvPacksInfo(param1:PackageIn) : void
      {
         isCallEnable = Boolean(param1.readInt())?Boolean(false):Boolean(true);
         isApplyEnable = Boolean(param1.readInt())?Boolean(false):Boolean(true);
         var _loc2_:int = param1.readInt();
         isFirstLogin = _loc2_ > 1?Boolean(false):Boolean(true);
         var _loc3_:int = param1.readInt();
         var _loc4_:int = 0;
         while(_loc4_ < _loc3_)
         {
            if(param1.readByte() == 0)
            {
               isOver = false;
               break;
            }
            _loc4_++;
         }
         RegressManager.instance.dispatchEvent(new RegressEvent(RegressEvent.REGRESS_ADD_REGRESSBTN));
      }
   }
}
