package entertainmentMode
{
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.ui.ComponentFactory;
   import ddt.data.UIModuleTypes;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.view.UIModuleSmallLoading;
   import entertainmentMode.data.EntertainmentPackageType;
   import entertainmentMode.model.EntertainmentModel;
   import entertainmentMode.view.EntertainmentModeView;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import hallIcon.HallIconManager;
   import hallIcon.HallIconType;
   import road7th.comm.PackageIn;
   import roomList.LookupEnumerate;
   
   public class EntertainmentModeManager extends EventDispatcher
   {
      
      public static var loadComplete:Boolean = false;
      
      public static var useFirst:Boolean = true;
      
      private static var _instance:EntertainmentModeManager;
       
      
      private var _frameView:EntertainmentModeView;
      
      public var isopen:Boolean = false;
      
      public var openTime:String;
      
      public function EntertainmentModeManager()
      {
         super();
      }
      
      public static function get instance() : EntertainmentModeManager
      {
         if(!_instance)
         {
            _instance = new EntertainmentModeManager();
         }
         return _instance;
      }
      
      public function setup() : void
      {
         this.initEvent();
      }
      
      public function show() : void
      {
         if(PlayerManager.Instance.Self.Grade < 20)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.entertainmentMode.gradeMin"));
            return;
         }
         if(loadComplete)
         {
            this.showFrame();
         }
         else if(useFirst)
         {
            UIModuleSmallLoading.Instance.progress = 0;
            UIModuleSmallLoading.Instance.show();
            UIModuleSmallLoading.Instance.addEventListener(Event.CLOSE,this.__onClose);
            UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.__progressShow);
            UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.__complainShow);
            UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.ENTERTAINMENT_MODE);
         }
      }
      
      private function initEvent() : void
      {
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.ENTERTAINMENT,this.__handler);
      }
      
      private function removeEvent() : void
      {
      }
      
      private function __handler(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = null;
         var _loc3_:Date = null;
         var _loc4_:Date = null;
         var _loc5_:String = null;
         _loc2_ = param1.pkg;
         switch(param1._cmd)
         {
            case EntertainmentPackageType.BUY_ICON:
               EntertainmentModel.instance.score = _loc2_.readInt();
               break;
            case EntertainmentPackageType.GET_SCORE:
               this.isopen = _loc2_.readBoolean();
               _loc3_ = _loc2_.readDate();
               _loc4_ = _loc2_.readDate();
               _loc5_ = String(_loc3_.fullYear) + "-" + String(_loc3_.month + 1) + "-" + String(_loc3_.date);
               this.openTime = _loc5_ + " " + _loc3_.hours + ":" + (_loc3_.minutes < 10?"0" + String(_loc3_.minutes):_loc3_.minutes) + "-" + _loc4_.hours + ":" + (_loc4_.minutes < 10?"0" + String(_loc4_.minutes):_loc4_.minutes);
               this.showHideIcon(this.isopen);
         }
      }
      
      public function showHideIcon(param1:Boolean) : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.ENTERTAINMENT,param1);
      }
      
      protected function __onClose(param1:Event) : void
      {
         UIModuleSmallLoading.Instance.hide();
         UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this.__onClose);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.__progressShow);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.__complainShow);
      }
      
      private function __progressShow(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.ENTERTAINMENT_MODE)
         {
            UIModuleSmallLoading.Instance.progress = param1.loader.progress * 100;
         }
      }
      
      private function __complainShow(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.ENTERTAINMENT_MODE)
         {
            UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this.__onClose);
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.__progressShow);
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.__complainShow);
            UIModuleSmallLoading.Instance.hide();
            loadComplete = true;
            useFirst = false;
            this.showFrame();
         }
      }
      
      public function hide() : void
      {
         if(this._frameView != null)
         {
            this._frameView.dispose();
         }
         this._frameView = null;
         this.removeEvent();
      }
      
      private function showFrame() : void
      {
         this._frameView = ComponentFactory.Instance.creatComponentByStylename("entertainment.EntertainmentModeView");
         SocketManager.Instance.out.sendSceneLogin(LookupEnumerate.ROOMLIST_ENTERTAINMENT);
         this._frameView.show();
      }
   }
}
