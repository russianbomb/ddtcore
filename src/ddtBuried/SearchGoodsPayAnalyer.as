package ddtBuried
{
   import com.pickgliss.loader.DataAnalyzer;
   import com.pickgliss.utils.ObjectUtils;
   import ddtBuried.data.SearchGoodsData;
   
   public class SearchGoodsPayAnalyer extends DataAnalyzer
   {
       
      
      public var itemList:Vector.<SearchGoodsData>;
      
      public function SearchGoodsPayAnalyer(param1:Function)
      {
         super(param1);
      }
      
      override public function analyze(param1:*) : void
      {
         var _loc5_:SearchGoodsData = null;
         this.itemList = new Vector.<SearchGoodsData>();
         var _loc2_:XML = new XML(param1);
         var _loc3_:XMLList = _loc2_.item;
         var _loc4_:int = 0;
         while(_loc4_ < _loc3_.length())
         {
            _loc5_ = new SearchGoodsData();
            ObjectUtils.copyPorpertiesByXML(_loc5_,_loc3_[_loc4_]);
            this.itemList.push(_loc5_);
            _loc4_++;
         }
         onAnalyzeComplete();
      }
   }
}
