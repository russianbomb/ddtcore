package ddtBuried
{
   import com.pickgliss.loader.DataAnalyzer;
   import com.pickgliss.utils.ObjectUtils;
   import ddtBuried.data.UpdateStarData;
   
   public class UpdateStarAnalyer extends DataAnalyzer
   {
       
      
      public var itemList:Vector.<UpdateStarData>;
      
      public function UpdateStarAnalyer(param1:Function)
      {
         super(param1);
      }
      
      override public function analyze(param1:*) : void
      {
         var _loc6_:UpdateStarData = null;
         this.itemList = new Vector.<UpdateStarData>();
         var _loc2_:XML = new XML(param1);
         var _loc3_:int = _loc2_.Item.length();
         var _loc4_:XMLList = _loc2_.item;
         var _loc5_:int = 0;
         while(_loc5_ < _loc4_.length())
         {
            _loc6_ = new UpdateStarData();
            ObjectUtils.copyPorpertiesByXML(_loc6_,_loc4_[_loc5_]);
            this.itemList.push(_loc6_);
            _loc5_++;
         }
         onAnalyzeComplete();
      }
   }
}
