package ddtBuried
{
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.BagInfo;
   import ddt.data.ServerConfigInfo;
   import ddt.data.UIModuleTypes;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.LanguageMgr;
   import ddt.manager.LeavePageManager;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerConfigManager;
   import ddt.manager.SharedManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.view.TransactionsFrame;
   import ddt.view.UIModuleSmallLoading;
   import ddtBuried.data.MapItemData;
   import ddtBuried.data.SearchGoodsData;
   import ddtBuried.data.UpdateStarData;
   import ddtBuried.map.MapArrays;
   import flash.display.DisplayObject;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.filters.ColorMatrixFilter;
   import hallIcon.HallIconManager;
   import hallIcon.HallIconType;
   import labyrinth.LabyrinthManager;
   import road7th.comm.PackageIn;
   
   public class BuriedManager
   {
      
      public static const MAP1:int = 1;
      
      public static const MAP2:int = 2;
      
      public static const MAP3:int = 3;
      
      public static var evnetDispatch:EventDispatcher = new EventDispatcher();
      
      private static var _instance:BuriedManager;
       
      
      public var mapArrays:MapArrays;
      
      public var isOver:Boolean;
      
      private var _frame:BuriedFrame;
      
      public var mapdataList:Vector.<MapItemData>;
      
      public var mapID:int;
      
      public var level:int;
      
      public var nowPosition:int;
      
      private var num:int;
      
      private var serverConfigInfo:ServerConfigInfo;
      
      public var payMoneyList:Vector.<SearchGoodsData>;
      
      public var upDateStarList:Vector.<UpdateStarData>;
      
      private var pay_count:int;
      
      private var totol_count:int;
      
      public var limit:int;
      
      public var currPayLevel:int = -1;
      
      public var takeCardPayList:Array;
      
      private var cardPayinfo:ServerConfigInfo;
      
      public var eventPosition:int = -1;
      
      public var takeCardLimit:int;
      
      public var cardInitList:Array;
      
      public var currCardIndex:int;
      
      public var isContinue:Boolean;
      
      public var isGetGoods:Boolean;
      
      public var isOpenBuried:Boolean;
      
      public var isBuriedBox:Boolean;
      
      public var isGo:Boolean;
      
      public var isBack:Boolean;
      
      public var isBackToStart:Boolean;
      
      public var isGoEnd:Boolean;
      
      public var stoneNum:int;
      
      private var _isOpening:Boolean;
      
      private var _shopframe:BuriedShopFrame;
      
      public var currGoodID:int;
      
      private var _transactionsFrame:TransactionsFrame;
      
      private var _money:int;
      
      private var _outFun:Function;
      
      public var finalNum:int;
      
      public function BuriedManager()
      {
         super();
         this.mapArrays = new MapArrays();
      }
      
      public static function get Instance() : BuriedManager
      {
         if(!BuriedManager._instance)
         {
            BuriedManager._instance = new BuriedManager();
         }
         return BuriedManager._instance;
      }
      
      public function getTakeCardPay() : String
      {
         return this.takeCardPayList[3 - this.takeCardLimit];
      }
      
      public function get isOpening() : Boolean
      {
         return this._isOpening;
      }
      
      public function set isOpening(param1:Boolean) : void
      {
         this._isOpening = param1;
      }
      
      public function oneDegreeToTwoDegree(param1:String, param2:int, param3:int) : Array
      {
         var _loc8_:int = 0;
         var _loc4_:Array = param1.split(",");
         var _loc5_:int = 0;
         var _loc6_:Array = [];
         var _loc7_:int = 0;
         while(_loc7_ < param3)
         {
            _loc6_[_loc7_] = [];
            _loc8_ = 0;
            while(_loc8_ < param2)
            {
               _loc6_[_loc7_][_loc8_] = int(_loc4_[_loc5_]);
               _loc5_++;
               _loc8_++;
            }
            _loc7_++;
         }
         return _loc6_;
      }
      
      public function setup() : void
      {
         this.initEvents();
      }
      
      public function checkMoney(param1:Boolean, param2:int, param3:Function = null) : Boolean
      {
         this._money = param2;
         this._outFun = param3;
         if(param1)
         {
            if(PlayerManager.Instance.Self.BandMoney < param2)
            {
               if(param3 != null)
               {
                  this.initAlertFarme();
               }
               else
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("store.view.transfer.StoreIITransferBG.lijinbuzu"));
               }
               return true;
            }
         }
         else if(PlayerManager.Instance.Self.Money < param2)
         {
            LeavePageManager.showFillFrame();
            return true;
         }
         return false;
      }
      
      private function initAlertFarme() : void
      {
         var _loc1_:BaseAlerFrame = null;
         _loc1_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("buried.alertInfo.noBindMoney"),"",LanguageMgr.GetTranslation("cancel"),true,false,false,2);
         _loc1_.addEventListener(FrameEvent.RESPONSE,this.onResponseHander);
      }
      
      private function onResponseHander(param1:FrameEvent) : void
      {
         if(param1.responseCode == FrameEvent.ENTER_CLICK || param1.responseCode == FrameEvent.SUBMIT_CLICK)
         {
            if(this.checkMoney(false,this._money,this._outFun))
            {
               return;
            }
            if(this._transactionsFrame)
            {
               this._transactionsFrame.dispose();
            }
            if(this._outFun != null)
            {
               this._outFun(false);
            }
         }
         param1.currentTarget.dispose();
      }
      
      public function SearchGoodsTempHander(param1:UpdateStarAnalyer) : void
      {
         this.upDateStarList = param1.itemList;
      }
      
      public function getUpdateData(param1:Boolean) : UpdateStarData
      {
         var _loc3_:int = 0;
         var _loc2_:int = this.upDateStarList.length;
         if(param1)
         {
            _loc3_ = this.level + 1;
         }
         else
         {
            _loc3_ = this.level;
         }
         var _loc4_:int = 0;
         while(_loc4_ < _loc2_)
         {
            if(_loc3_ == this.upDateStarList[_loc4_].StarID)
            {
               return this.upDateStarList[_loc4_];
            }
            _loc4_++;
         }
         return null;
      }
      
      public function getPayData() : SearchGoodsData
      {
         var _loc1_:int = this.payMoneyList.length;
         var _loc2_:int = 0;
         while(_loc2_ < _loc1_)
         {
            if(this.currPayLevel == this.payMoneyList[_loc2_].Number)
            {
               return this.payMoneyList[_loc2_];
            }
            _loc2_++;
         }
         return null;
      }
      
      public function searchGoodsPayHander(param1:SearchGoodsPayAnalyer) : void
      {
         this.payMoneyList = param1.itemList;
      }
      
      private function initEvents() : void
      {
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PlayerEnter,this.playerEnterHander);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PlayNowPosition,this.playNowPositionHander);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PlayerRollDice,this.playerRollDiceHander);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.PlayerUpgradeStartLevel,this.playerUpgradeLevelEnterHander);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GetGoods,this.getGoodsHandler);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.FlopCard,this.flopCardHandler);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.TakeCardResponse,this.takeCardResponseHandler);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.RemoveEvent,this.removeEventHandler);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.OneStep,this.removeOneStepHandler);
         BuriedManager.evnetDispatch.addEventListener(BuriedEvent.LABYRINTH_OVER,this.openShopView);
      }
      
      private function removeEventHandler(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:Object = new Object();
         _loc2_.key = param1.pkg.readInt();
         _loc2_.value = param1.pkg.readInt();
         BuriedManager.evnetDispatch.dispatchEvent(new BuriedEvent(BuriedEvent.RemoveEvent,_loc2_));
      }
      
      private function removeOneStepHandler(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:Object = new Object();
         _loc2_.key = param1.pkg.readInt();
         _loc2_.value = param1.pkg.readInt();
         BuriedManager.evnetDispatch.dispatchEvent(new BuriedEvent(BuriedEvent.OneStep,_loc2_));
      }
      
      private function takeCardResponseHandler(param1:CrazyTankSocketEvent) : void
      {
         this.takeCardLimit = param1.pkg.readInt();
         var _loc2_:int = param1.pkg.readInt();
         var _loc3_:int = param1.pkg.readInt();
         var _loc4_:Object = new Object();
         _loc4_.tempID = _loc2_;
         _loc4_.count = _loc3_;
         evnetDispatch.dispatchEvent(new BuriedEvent(BuriedEvent.TAKE_CARD,_loc4_));
      }
      
      private function flopCardHandler(param1:CrazyTankSocketEvent) : void
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:Object = null;
         this.isOpenBuried = true;
         this.cardInitList = new Array();
         this.takeCardLimit = param1.pkg.readInt();
         var _loc2_:int = param1.pkg.readInt();
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_)
         {
            _loc4_ = param1.pkg.readInt();
            _loc5_ = param1.pkg.readInt();
            _loc6_ = new Object();
            _loc6_.tempID = _loc4_;
            _loc6_.count = _loc5_;
            this.cardInitList.push(_loc6_);
            _loc3_++;
         }
      }
      
      private function getGoodsHandler(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         this.currGoodID = _loc2_.readInt();
         this.isGetGoods = true;
      }
      
      private function playNowPositionHander(param1:CrazyTankSocketEvent) : void
      {
         this.isContinue = true;
         var _loc2_:PackageIn = param1.pkg;
         this.eventPosition = _loc2_.readInt();
         if(this.eventPosition - this.nowPosition == 1)
         {
            this.isGo = true;
         }
         else if(this.nowPosition - this.eventPosition == 1)
         {
            this.isBack = true;
         }
         else if(this.eventPosition == 0)
         {
            this.isBackToStart = true;
         }
         else if(this.eventPosition == 35)
         {
            this.isGoEnd = true;
         }
      }
      
      private function playerRollDiceHander(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         this.num = param1.pkg.readInt();
         this.finalNum = this.num;
         var _loc3_:int = param1.pkg.readInt();
         this.nowPosition = param1.pkg.readInt();
         if(this.nowPosition == 35)
         {
            this.isOver = true;
         }
         if(this.nowPosition == 0)
         {
            evnetDispatch.dispatchEvent(new BuriedEvent(BuriedEvent.UPDATABTNSTATS));
         }
         var _loc4_:String = ServerConfigManager.instance.serverConfigInfo["SearchGoodsFreeLimit"].Value;
         if(_loc4_ == "1")
         {
            this.limit = this.num - this.pay_count;
         }
         else
         {
            this.limit = this.num;
         }
         switch(_loc3_)
         {
            case 1:
               this._frame.setCrFrame("one");
               break;
            case 2:
               this._frame.setCrFrame("two");
               break;
            case 3:
               this._frame.setCrFrame("three");
               break;
            case 4:
               this._frame.setCrFrame("four");
               break;
            case 5:
               this._frame.setCrFrame("five");
               break;
            case 6:
               this._frame.setCrFrame("six");
         }
         this._frame.play();
         if(this.limit <= 0)
         {
            this.currPayLevel = 1;
            this._frame.upDataBtn();
            this._frame.setTxt(this.num.toString());
            return;
         }
         this.currPayLevel = -1;
         this._frame.setTxt(this.limit.toString());
      }
      
      public function getBuriedFrame() : Frame
      {
         return this._frame;
      }
      
      private function playerUpgradeLevelEnterHander(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         this.level = _loc2_.readInt();
         this._frame.updataStarLevel(this.level);
      }
      
      private function playerEnterHander(param1:CrazyTankSocketEvent) : void
      {
         var _loc5_:MapItemData = null;
         this.serverConfigInfo = ServerConfigManager.instance.serverConfigInfo["SearchGoodsFreeCount"];
         this.cardPayinfo = ServerConfigManager.instance.serverConfigInfo["SearchGoodsTakeCardMoney"];
         this.takeCardPayList = this.cardPayinfo.Value.split("|");
         this.pay_count = this.payMoneyList.length;
         this.totol_count = this.pay_count + int(this.serverConfigInfo.Value);
         this.dispose();
         this.mapdataList = new Vector.<MapItemData>();
         var _loc2_:PackageIn = param1.pkg;
         this.mapID = _loc2_.readInt();
         this.level = _loc2_.readInt();
         this.nowPosition = _loc2_.readInt();
         this.num = _loc2_.readInt();
         this.isOver = false;
         if(this.num > this.pay_count)
         {
            this.limit = this.num - this.pay_count;
         }
         else
         {
            this.limit = this.num;
            this.currPayLevel = this.pay_count - this.num + 1;
         }
         var _loc3_:int = _loc2_.readInt();
         if(_loc3_ == 0)
         {
            return;
         }
         var _loc4_:int = 0;
         while(_loc4_ < _loc3_)
         {
            _loc5_ = new MapItemData();
            _loc5_.type = _loc2_.readInt();
            _loc5_.tempID = _loc2_.readInt();
            this.mapdataList.push(_loc5_);
            _loc4_++;
         }
         if(this._frame)
         {
            ObjectUtils.disposeObject(this._frame);
            this._frame = null;
         }
         this.initBuriedFrame();
      }
      
      public function getBuriedStoneNum() : String
      {
         var _loc1_:BagInfo = PlayerManager.Instance.Self.getBag(BagInfo.PROPBAG);
         var _loc2_:int = _loc1_.getItemCountByTemplateId(11680);
         return _loc2_.toString();
      }
      
      public function setRemindRoll(param1:Boolean) : void
      {
         SharedManager.Instance.isRemindRoll = param1;
      }
      
      public function getRemindRoll() : Boolean
      {
         return SharedManager.Instance.isRemindRoll;
      }
      
      public function setRemindOverCard(param1:Boolean) : void
      {
         SharedManager.Instance.isRemindOverCard = param1;
      }
      
      public function getRemindOverCard() : Boolean
      {
         return SharedManager.Instance.isRemindOverCard;
      }
      
      public function setRemindOverBind(param1:Boolean) : void
      {
         SharedManager.Instance.isRemindOverCardBind = param1;
      }
      
      public function getRemindOverBind() : Boolean
      {
         return SharedManager.Instance.isRemindOverCardBind;
      }
      
      public function setRemindRollBind(param1:Boolean) : void
      {
         SharedManager.Instance.isRemindRollBind = param1;
      }
      
      public function getRemindRollBind() : Boolean
      {
         return SharedManager.Instance.isRemindRollBind;
      }
      
      public function initBuriedFrame() : void
      {
         if(!this._frame)
         {
            UIModuleSmallLoading.Instance.progress = 0;
            UIModuleSmallLoading.Instance.show();
            UIModuleSmallLoading.Instance.addEventListener(Event.CLOSE,this.onSmallLoadingClose);
            UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.createActivityFrame);
            UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUIProgress);
            UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.DDT_BURIED);
         }
         else
         {
            this._frame = ComponentFactory.Instance.creatComponentByStylename("ddtBuried.BuriedFrame");
            this._frame.addDiceView(this.mapID);
            this._frame.setTxt(this.limit.toString());
            this._frame.setStarList(this.level);
            if(this.currPayLevel >= 0)
            {
               this._frame.upDataBtn();
               this._frame.setTxt(this.num.toString());
            }
            this._isOpening = true;
            LayerManager.Instance.addToLayer(this._frame,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
         }
      }
      
      protected function createActivityFrame(param1:UIModuleEvent) : void
      {
         if(param1.module != UIModuleTypes.DDT_BURIED)
         {
            return;
         }
         UIModuleSmallLoading.Instance.hide();
         UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this.onSmallLoadingClose);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.createActivityFrame);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUIProgress);
         this._frame = ComponentFactory.Instance.creatComponentByStylename("ddtBuried.BuriedFrame");
         this._frame.addDiceView(this.mapID);
         this._frame.setTxt(this.limit.toString());
         if(this.currPayLevel >= 0)
         {
            this._frame.upDataBtn();
            this._frame.setTxt(this.num.toString());
         }
         this._isOpening = true;
         this._frame.setStarList(this.level);
         LayerManager.Instance.addToLayer(this._frame,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
      }
      
      protected function onUIProgress(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.DDT_BURIED)
         {
            UIModuleSmallLoading.Instance.progress = param1.loader.progress * 100;
         }
      }
      
      protected function onSmallLoadingClose(param1:Event) : void
      {
         UIModuleSmallLoading.Instance.hide();
         UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this.onSmallLoadingClose);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.createActivityFrame);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUIProgress);
      }
      
      private function openShopView(param1:BuriedEvent) : void
      {
         if(!this._frame)
         {
            return;
         }
         this._shopframe = ComponentFactory.Instance.creatComponentByStylename("ddtburied.view.labyrinthShopFrame");
         this._shopframe.addEventListener(FrameEvent.RESPONSE,this.frameEvent);
         LayerManager.Instance.addToLayer(this._shopframe,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
      }
      
      public function openshopHander() : void
      {
         if(LabyrinthManager.Instance.UILoadComplete)
         {
            this._shopframe = ComponentFactory.Instance.creatComponentByStylename("ddtburied.view.labyrinthShopFrame");
            this._shopframe.addEventListener(FrameEvent.RESPONSE,this.frameEvent);
            LayerManager.Instance.addToLayer(this._shopframe,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
         }
         else
         {
            LabyrinthManager.Instance.loadUIModule();
         }
      }
      
      protected function frameEvent(param1:FrameEvent) : void
      {
         SoundManager.instance.playButtonSound();
         this._shopframe.dispose();
      }
      
      public function dispose() : void
      {
         this._outFun = null;
         this.currPayLevel = -1;
         this._isOpening = false;
         this.isContinue = false;
         this.isGetGoods = false;
         this.isOpenBuried = false;
         this.isBuriedBox = false;
         this.isGo = false;
         this.isBack = false;
         this.isBackToStart = false;
         this.isGoEnd = false;
         this.isOver = false;
         if(this._frame && this._frame.parent)
         {
            this._frame.dispose();
         }
         if(this._shopframe && this._shopframe.parent)
         {
            this._shopframe.dispose();
         }
         if(this._shopframe && this._shopframe.parent)
         {
            this._shopframe.dispose();
         }
         if(this._transactionsFrame && this._transactionsFrame.parent)
         {
            this._transactionsFrame.dispose();
         }
         this._frame = null;
         this._shopframe = null;
         this._transactionsFrame = null;
      }
      
      public function showTransactionFrame(param1:String, param2:Function = null, param3:Function = null, param4:Sprite = null) : void
      {
         this._transactionsFrame = ComponentFactory.Instance.creatComponentByStylename("ddtBuried.views.TransactionsFrame");
         this._transactionsFrame.setTxt(param1);
         this._transactionsFrame.buyFunction = param2;
         this._transactionsFrame.clickFunction = param3;
         this._transactionsFrame.target = param4;
         this._transactionsFrame.changeSeleBtnSpace(126,93);
         LayerManager.Instance.addToLayer(this._transactionsFrame,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.ALPHA_BLOCKGOUND);
      }
      
      public function applyGray(param1:DisplayObject) : void
      {
         var _loc2_:Array = new Array();
         _loc2_ = _loc2_.concat([0.3086,0.6094,0.082,0,0]);
         _loc2_ = _loc2_.concat([0.3086,0.6094,0.082,0,0]);
         _loc2_ = _loc2_.concat([0.3086,0.6094,0.082,0,0]);
         _loc2_ = _loc2_.concat([0,0,0,1,0]);
         this.applyFilter(param1,_loc2_);
      }
      
      public function reGray(param1:DisplayObject) : void
      {
         param1.filters = null;
      }
      
      private function applyFilter(param1:DisplayObject, param2:Array) : void
      {
         var _loc3_:ColorMatrixFilter = new ColorMatrixFilter(param2);
         var _loc4_:Array = new Array();
         _loc4_.push(_loc3_);
         param1.filters = _loc4_;
      }
      
      public function checkShowIcon() : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.BURIED,true);
      }
   }
}
