package ddtBuried
{
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddtBuried.views.BuriedView;
   import ddtBuried.views.DiceView;
   
   public class BuriedFrame extends Frame
   {
      
      private static const MAP1:int = 1;
      
      private static const MAP2:int = 2;
      
      private static const MAP3:int = 3;
       
      
      private var _buriedView:BuriedView;
      
      private var _diceView:DiceView;
      
      private var _type:int;
      
      public function BuriedFrame()
      {
         super();
         this.initView();
         this.addEvent();
      }
      
      private function initView() : void
      {
      }
      
      public function addDiceView(param1:int) : void
      {
         this._diceView = new DiceView();
         switch(param1)
         {
            case MAP1:
               this._diceView.addMaps(BuriedManager.Instance.mapArrays.itemMaps1,11,7,2,94);
               break;
            case MAP2:
               this._diceView.addMaps(BuriedManager.Instance.mapArrays.itemMaps2,8,8,136,81);
               break;
            case MAP3:
               this._diceView.addMaps(BuriedManager.Instance.mapArrays.itemMaps3,9,8,136,74);
         }
         addToContent(this._diceView);
      }
      
      public function setStarList(param1:int) : void
      {
         this._diceView.setStarList(param1);
      }
      
      public function updataStarLevel(param1:int) : void
      {
         this._diceView.updataStarLevel(param1);
      }
      
      public function setCrFrame(param1:String) : void
      {
         this._diceView.setCrFrame(param1);
      }
      
      public function setTxt(param1:String) : void
      {
         this._diceView.setTxt(param1);
      }
      
      public function play() : void
      {
         this._diceView.play();
      }
      
      public function upDataBtn() : void
      {
         this._diceView.upDataBtn();
      }
      
      private function addEvent() : void
      {
         addEventListener(FrameEvent.RESPONSE,this._response);
         BuriedManager.evnetDispatch.addEventListener(BuriedEvent.OPEN_BURIEDVIEW,this.openBuriedHander);
      }
      
      private function removeEvents() : void
      {
         removeEventListener(FrameEvent.RESPONSE,this._response);
         BuriedManager.evnetDispatch.removeEventListener(BuriedEvent.OPEN_BURIEDVIEW,this.openBuriedHander);
      }
      
      private function openBuriedHander(param1:BuriedEvent) : void
      {
         if(this._buriedView)
         {
            ObjectUtils.disposeObject(this._buriedView);
            this._buriedView = null;
         }
         this._buriedView = new BuriedView();
         addToContent(this._buriedView);
      }
      
      private function _response(param1:FrameEvent) : void
      {
         if(param1.responseCode == FrameEvent.CLOSE_CLICK || param1.responseCode == FrameEvent.ESC_CLICK)
         {
            SoundManager.instance.play("008");
            this.dispose();
         }
      }
      
      override public function dispose() : void
      {
         this.removeEvents();
         if(this._diceView)
         {
            this._diceView.dispose();
         }
         if(this._buriedView)
         {
            this._buriedView.dispose();
         }
         ObjectUtils.disposeObject(this._buriedView);
         ObjectUtils.disposeObject(this._diceView);
         while(numChildren)
         {
            ObjectUtils.disposeObject(getChildAt(0));
         }
         super.dispose();
         this._diceView = null;
         this._buriedView = null;
         SocketManager.Instance.out.outCard();
      }
   }
}
