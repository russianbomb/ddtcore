package ddtBuried.map
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.utils.ObjectUtils;
   import ddtBuried.BuriedEvent;
   import ddtBuried.BuriedManager;
   import ddtBuried.role.BuriedPlayer;
   import flash.display.Bitmap;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.KeyboardEvent;
   import flash.events.TimerEvent;
   import flash.ui.Keyboard;
   import flash.utils.Timer;
   
   public class Maps extends Sprite
   {
       
      
      private var w:uint;
      
      private var h:uint;
      
      private var wh:uint;
      
      private var goo:Number;
      
      private var map:Sprite;
      
      private var mapArr:Array;
      
      private var roadMen:MovieClip;
      
      private var roadList:Array;
      
      private var roadTimer:Timer;
      
      private var timer_i:uint = 0;
      
      private var mapDataArray:Array;
      
      private var role:BuriedPlayer;
      
      public function Maps(param1:Array, param2:int, param3:int)
      {
         super();
         this.w = param2;
         this.h = param3;
         this.wh = 55;
         this.mapDataArray = param1;
         this.init();
      }
      
      private function init() : void
      {
         this.goo = 0.3;
         this.createMaps();
         this.roadMens();
         this.roadTimer = new Timer(300,0);
      }
      
      public function startMove(param1:int, param2:int) : void
      {
         var _loc6_:ARoad = null;
         var _loc7_:* = undefined;
         var _loc3_:int = param1;
         var _loc4_:int = param2;
         var _loc5_:* = this.mapArr[_loc4_][_loc3_];
         if(_loc5_.go == 0)
         {
            if(this.roadList)
            {
               for each(_loc7_ in this.roadList)
               {
                  _loc7_.alpha = 1;
               }
               this.roadList = [];
            }
            this.roadTimer.stop();
            this.roadMen.px = Math.floor(this.roadMen.x / this.wh);
            this.roadMen.py = Math.floor(this.roadMen.y / this.wh);
            _loc6_ = new ARoad();
            this.roadList = _loc6_.searchRoad(this.roadMen,_loc5_,this.mapArr);
            if(this.roadList.length > 0)
            {
               this.MC_play(this.roadList);
            }
         }
      }
      
      private function MC_play(param1:Array) : void
      {
         var _loc2_:* = undefined;
         param1.reverse();
         this.roadTimer.stop();
         this.timer_i = 0;
         this.roadTimer.addEventListener(TimerEvent.TIMER,this.goMap);
         this.roadTimer.start();
         for each(_loc2_ in param1)
         {
            _loc2_.alpha = 0.3;
         }
      }
      
      private function goMap(param1:TimerEvent) : void
      {
         var _loc2_:MovieClip = null;
         _loc2_ = this.roadList[this.timer_i];
         this.roadMen.x = _loc2_.x;
         this.roadMen.y = _loc2_.y;
         _loc2_.alpha = 1;
         this.timer_i++;
         if(this.timer_i >= this.roadList.length)
         {
            this.roadTimer.stop();
            if(BuriedManager.Instance.isOver)
            {
               BuriedManager.Instance.isOver = false;
               BuriedManager.evnetDispatch.dispatchEvent(new BuriedEvent(BuriedEvent.MAPOVER));
            }
            else
            {
               BuriedManager.evnetDispatch.dispatchEvent(new BuriedEvent(BuriedEvent.WALKOVER));
            }
         }
      }
      
      private function createMaps() : void
      {
         var _loc2_:uint = 0;
         var _loc3_:uint = 0;
         var _loc4_:MovieClip = null;
         this.mapArr = new Array();
         this.map = new Sprite();
         this.map.x = this.wh;
         this.map.y = this.wh;
         addChild(this.map);
         var _loc1_:uint = 0;
         while(_loc1_ < this.h)
         {
            this.mapArr.push(new Array());
            _loc2_ = 0;
            while(_loc2_ < this.w)
            {
               _loc3_ = this.mapDataArray[_loc1_][_loc2_];
               _loc4_ = this.drawRect(_loc3_);
               this.mapArr[_loc1_].push(_loc4_);
               this.mapArr[_loc1_][_loc2_].px = _loc2_;
               this.mapArr[_loc1_][_loc2_].py = _loc1_;
               this.mapArr[_loc1_][_loc2_].go = _loc3_;
               this.mapArr[_loc1_][_loc2_].x = _loc2_ * this.wh;
               this.mapArr[_loc1_][_loc2_].y = _loc1_ * this.wh;
               this.map.addChild(this.mapArr[_loc1_][_loc2_]);
               _loc2_++;
            }
            _loc1_++;
         }
         this.map.rotationX = -30;
         this.map.scaleX = 1.4;
         this.map.scaleY = 0.8;
      }
      
      private function clearMaps() : void
      {
         var _loc2_:uint = 0;
         var _loc3_:MovieClip = null;
         var _loc1_:uint = 0;
         while(_loc1_ < this.h)
         {
            _loc2_ = 0;
            while(_loc2_ < this.w)
            {
               _loc3_ = this.mapArr[_loc1_][_loc2_];
               while(_loc3_.numChildren)
               {
                  ObjectUtils.disposeObject(_loc3_.removeChildAt(0));
               }
               ObjectUtils.disposeObject(this.mapArr[_loc1_][_loc2_]);
               _loc2_++;
            }
            _loc1_++;
         }
      }
      
      public function getMapArray() : Array
      {
         return this.mapArr;
      }
      
      private function keyDowns(param1:KeyboardEvent) : void
      {
         var _loc2_:int = param1.keyCode;
         if(_loc2_ == Keyboard.SPACE)
         {
            removeChild(this.map);
            this.mapArr = [];
            this.createMaps();
            this.roadMens();
            this.roadTimer.stop();
         }
      }
      
      private function drawRect(param1:uint) : MovieClip
      {
         var _loc3_:uint = 0;
         var _loc4_:Bitmap = null;
         var _loc2_:MovieClip = new MovieClip();
         switch(param1)
         {
            case 0:
               _loc4_ = ComponentFactory.Instance.creat("buried.shaizi.mapItemBack");
               _loc4_.smoothing = true;
               _loc4_.width = 56;
               _loc4_.height = 56;
               _loc2_.addChild(_loc4_);
               return _loc2_;
            case 1:
               return _loc2_;
            default:
               return _loc2_;
         }
      }
      
      private function roleCallback(param1:BuriedPlayer, param2:Boolean) : void
      {
         if(!param1)
         {
            return;
         }
         param1.sceneCharacterStateType = "natural";
         param1.update();
      }
      
      private function roadMens() : void
      {
         this.roadMen = this.drawRect(2);
         var _loc1_:uint = Math.round(Math.random() * (this.w - 1));
         var _loc2_:uint = Math.round(Math.random() * (this.h - 1));
         this.map.addChild(this.roadMen);
      }
      
      public function setRoadMan(param1:int, param2:int) : void
      {
         this.roadMen.px = param1;
         this.roadMen.py = param2;
         this.roadMen.x = param1 * this.wh;
         this.roadMen.y = param2 * this.wh;
         this.mapArr[param2][param1].go = 0;
      }
      
      public function dispose() : void
      {
         this.clearMaps();
         this.mapArr = [];
         this.roadTimer.stop();
         this.roadTimer.removeEventListener(TimerEvent.TIMER,this.goMap);
         while(numChildren)
         {
            ObjectUtils.disposeObject(getChildAt(0));
         }
         this.roadTimer = null;
      }
   }
}
