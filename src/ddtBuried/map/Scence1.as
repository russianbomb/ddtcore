package ddtBuried.map
{
   import bagAndInfo.cell.BagCell;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Component;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.manager.ItemManager;
   import ddt.manager.LanguageMgr;
   import ddtBuried.BuriedManager;
   import ddtBuried.data.MapItemData;
   import flash.display.Bitmap;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.geom.Point;
   
   public class Scence1 extends MovieClip implements Disposeable
   {
       
      
      private var _content:Sprite;
      
      private var _mapArray:Array;
      
      private var _map:Maps;
      
      private var standArray:Array;
      
      public function Scence1(param1:String, param2:int, param3:int)
      {
         this._mapArray = [];
         super();
         this.initView(param1,param2,param3);
      }
      
      private function initView(param1:String, param2:int, param3:int) : void
      {
         this._mapArray = BuriedManager.Instance.oneDegreeToTwoDegree(param1,param2,param3);
         this._map = new Maps(this._mapArray,param2,param3);
         addChild(this._map);
         this.initMapItem();
      }
      
      private function initMapItem() : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:Bitmap = null;
         var _loc6_:Sprite = null;
         var _loc7_:Sprite = null;
         var _loc8_:MovieClip = null;
         var _loc12_:MapItemData = null;
         var _loc13_:Sprite = null;
         if(BuriedManager.Instance.mapID == BuriedManager.MAP1)
         {
            this.standArray = BuriedManager.Instance.mapArrays.standArray1;
         }
         else if(BuriedManager.Instance.mapID == BuriedManager.MAP2)
         {
            this.standArray = BuriedManager.Instance.mapArrays.standArray2;
         }
         else
         {
            this.standArray = BuriedManager.Instance.mapArrays.standArray3;
         }
         var _loc1_:Vector.<MapItemData> = BuriedManager.Instance.mapdataList;
         var _loc2_:int = _loc1_.length;
         _loc3_ = this.standArray[0].x;
         _loc4_ = this.standArray[0].y;
         _loc6_ = new Sprite();
         _loc5_ = ComponentFactory.Instance.creat("buried.shaizi.startPoint");
         _loc5_.scaleX = _loc5_.scaleY = 0.8;
         _loc6_.addChild(_loc5_);
         if(this._map.getMapArray()[_loc4_][_loc3_].numChildren > 0)
         {
            this._map.getMapArray()[_loc4_][_loc3_].removeChildAt(0);
         }
         this._map.getMapArray()[_loc4_][_loc3_].addChild(_loc6_);
         _loc7_ = this.creatIcon(BuriedManager.Instance.getUpdateData(false).DestinationReward);
         _loc3_ = this.standArray[35].x;
         _loc4_ = this.standArray[35].y;
         _loc8_ = ComponentFactory.Instance.creat("buried.over.light");
         _loc8_.scaleX = _loc8_.scaleY = 0.7;
         _loc8_.x = 347.15;
         _loc8_.y = 201.85;
         this._map.getMapArray()[_loc4_][_loc3_].addChild(_loc8_);
         this._map.getMapArray()[_loc4_][_loc3_].addChild(_loc7_);
         var _loc9_:int = 0;
         while(_loc9_ < _loc2_)
         {
            _loc12_ = _loc1_[_loc9_];
            _loc3_ = this.standArray[_loc12_.type].x;
            _loc4_ = this.standArray[_loc12_.type].y;
            _loc13_ = this.creatIcon(_loc12_.tempID);
            this._map.getMapArray()[_loc4_][_loc3_].addChild(_loc13_);
            _loc9_++;
         }
         var _loc10_:int = this.standArray[BuriedManager.Instance.nowPosition].x;
         var _loc11_:int = this.standArray[BuriedManager.Instance.nowPosition].y;
         this._map.setRoadMan(_loc10_,_loc11_);
      }
      
      public function getRoadPoint() : Point
      {
         var _loc1_:Point = new Point();
         var _loc2_:int = this.standArray[0].x;
         var _loc3_:int = this.standArray[0].y;
         _loc1_.x = this._map.getMapArray()[_loc3_][_loc2_].x;
         _loc1_.y = this._map.getMapArray()[_loc3_][_loc2_].y;
         return _loc1_;
      }
      
      public function updateRoadPoint(param1:int = 0) : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc2_:Bitmap = ComponentFactory.Instance.creat("buried.shaizi.nothing");
         if(param1 == 0)
         {
            _loc3_ = this.standArray[BuriedManager.Instance.nowPosition].x;
            _loc4_ = this.standArray[BuriedManager.Instance.nowPosition].y;
         }
         else
         {
            _loc3_ = this.standArray[param1].x;
            _loc4_ = this.standArray[param1].y;
         }
         this._map.getMapArray()[_loc4_][_loc3_].addChild(_loc2_);
      }
      
      private function creatIcon(param1:int) : Sprite
      {
         var _loc3_:Bitmap = null;
         var _loc4_:Component = null;
         var _loc5_:Component = null;
         var _loc6_:ItemTemplateInfo = null;
         var _loc7_:BagCell = null;
         var _loc2_:Sprite = new Sprite();
         switch(param1)
         {
            case 0:
               _loc3_ = ComponentFactory.Instance.creat("buried.shaizi.nothing");
               break;
            case -1:
               _loc3_ = ComponentFactory.Instance.creat("buried.shaizi.returnO");
               break;
            case -2:
               _loc3_ = ComponentFactory.Instance.creat("buried.shaizi.backStep");
               break;
            case -3:
               _loc3_ = ComponentFactory.Instance.creat("buried.shaizi.comStep");
               break;
            case -4:
               _loc3_ = ComponentFactory.Instance.creat("buried.shaizi.toOver");
               break;
            case -5:
               _loc3_ = ComponentFactory.Instance.creat("buried.shaizi.stonePic");
               _loc4_ = ComponentFactory.Instance.creat("buried.cellTipComponent");
               _loc4_.tipData = LanguageMgr.GetTranslation("buried.shaizi.stonePic.tipText");
               _loc2_ = _loc4_;
               break;
            case -6:
               _loc3_ = ComponentFactory.Instance.creat("buried.shaizi.renwu");
               _loc5_ = ComponentFactory.Instance.creat("buried.cellTipComponent");
               _loc5_.tipData = LanguageMgr.GetTranslation("buried.shaizi.renwu.tipText");
               _loc2_ = _loc5_;
               break;
            case -7:
               _loc3_ = ComponentFactory.Instance.creat("buried.shaizi.openQue");
               break;
            default:
               _loc6_ = ItemManager.Instance.getTemplateById(param1);
               _loc7_ = new BagCell(0,_loc6_);
               _loc7_.setBgVisible(false);
               _loc7_.width = 55;
               _loc7_.height = 55;
               _loc2_ = _loc7_;
         }
         if(_loc3_)
         {
            _loc3_.smoothing = true;
            _loc3_.width = 50;
            _loc3_.height = 50;
            _loc2_.addChild(_loc3_);
         }
         _loc2_.x = 3;
         _loc2_.y = 1;
         return _loc2_;
      }
      
      public function selfFindPath(param1:int, param2:int) : void
      {
         this._map.startMove(param1,param2);
      }
      
      public function dispose() : void
      {
         if(this._map)
         {
            this._map.dispose();
         }
         ObjectUtils.disposeObject(this._map);
         while(numChildren)
         {
            ObjectUtils.disposeObject(getChildAt(0));
         }
         this._map = null;
         this.standArray = null;
      }
   }
}
