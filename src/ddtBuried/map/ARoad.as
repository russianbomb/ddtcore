package ddtBuried.map
{
   import flash.display.MovieClip;
   import flash.display.Sprite;
   
   public class ARoad extends Sprite
   {
       
      
      private var startPoint:MovieClip;
      
      private var endPoint:MovieClip;
      
      private var mapArr:Array;
      
      private var w:uint;
      
      private var h:uint;
      
      private var openList:Array;
      
      private var closeList:Array;
      
      private var roadArr:Array;
      
      private var isPath:Boolean;
      
      private var isSearch:Boolean;
      
      public function ARoad()
      {
         this.openList = new Array();
         this.closeList = new Array();
         this.roadArr = new Array();
         super();
      }
      
      public function searchRoad(param1:MovieClip, param2:MovieClip, param3:Array) : Array
      {
         var _loc4_:MovieClip = null;
         this.startPoint = param1;
         this.endPoint = param2;
         this.mapArr = param3;
         this.w = this.mapArr[0].length - 1;
         this.h = this.mapArr.length - 1;
         this.openList.push(this.startPoint);
         while(true)
         {
            if(this.openList.length < 1)
            {
               return this.roadArr;
            }
            _loc4_ = this.openList.splice(this.getMinF(),1)[0];
            if(_loc4_ == this.endPoint)
            {
               while(_loc4_.father != this.startPoint.father)
               {
                  this.roadArr.push(_loc4_);
                  _loc4_ = _loc4_.father;
               }
               return this.roadArr;
            }
            this.closeList.push(_loc4_);
            this.addAroundPoint(_loc4_);
         }
         return null;
      }
      
      private function addAroundPoint(param1:MovieClip) : void
      {
         var _loc2_:uint = param1.px;
         var _loc3_:uint = param1.py;
         if(_loc2_ > 0 && this.mapArr[_loc3_][_loc2_ - 1].go == 0)
         {
            if(!this.inArr(this.mapArr[_loc3_][_loc2_ - 1],this.closeList))
            {
               if(!this.inArr(this.mapArr[_loc3_][_loc2_ - 1],this.openList))
               {
                  this.setGHF(this.mapArr[_loc3_][_loc2_ - 1],param1,10);
                  this.openList.push(this.mapArr[_loc3_][_loc2_ - 1]);
               }
               else
               {
                  this.checkG(this.mapArr[_loc3_][_loc2_ - 1],param1);
               }
            }
            if(_loc3_ > 0 && this.mapArr[_loc3_ - 1][_loc2_ - 1].go == 0 && this.mapArr[_loc3_ - 1][_loc2_].go == 0)
            {
               if(!this.inArr(this.mapArr[_loc3_ - 1][_loc2_ - 1],this.closeList) && !this.inArr(this.mapArr[_loc3_ - 1][_loc2_ - 1],this.openList))
               {
                  this.setGHF(this.mapArr[_loc3_ - 1][_loc2_ - 1],param1,14);
                  this.openList.push(this.mapArr[_loc3_ - 1][_loc2_ - 1]);
               }
            }
            if(_loc3_ < this.h && this.mapArr[_loc3_ + 1][_loc2_ - 1].go == 0 && this.mapArr[_loc3_ + 1][_loc2_].go == 0)
            {
               if(!this.inArr(this.mapArr[_loc3_ + 1][_loc2_ - 1],this.closeList) && !this.inArr(this.mapArr[_loc3_ + 1][_loc2_ - 1],this.openList))
               {
                  this.setGHF(this.mapArr[_loc3_ + 1][_loc2_ - 1],param1,14);
                  this.openList.push(this.mapArr[_loc3_ + 1][_loc2_ - 1]);
               }
            }
         }
         if(_loc2_ < this.w && this.mapArr[_loc3_][_loc2_ + 1].go == 0)
         {
            if(!this.inArr(this.mapArr[_loc3_][_loc2_ + 1],this.closeList))
            {
               if(!this.inArr(this.mapArr[_loc3_][_loc2_ + 1],this.openList))
               {
                  this.setGHF(this.mapArr[_loc3_][_loc2_ + 1],param1,10);
                  this.openList.push(this.mapArr[_loc3_][_loc2_ + 1]);
               }
               else
               {
                  this.checkG(this.mapArr[_loc3_][_loc2_ + 1],param1);
               }
            }
            if(_loc3_ > 0 && this.mapArr[_loc3_ - 1][_loc2_ + 1].go == 0 && this.mapArr[_loc3_ - 1][_loc2_].go == 0)
            {
               if(!this.inArr(this.mapArr[_loc3_ - 1][_loc2_ + 1],this.closeList) && !this.inArr(this.mapArr[_loc3_ - 1][_loc2_ + 1],this.openList))
               {
                  this.setGHF(this.mapArr[_loc3_ - 1][_loc2_ + 1],param1,14);
                  this.openList.push(this.mapArr[_loc3_ - 1][_loc2_ + 1]);
               }
            }
            if(_loc3_ < this.h && this.mapArr[_loc3_ + 1][_loc2_ + 1].go == 0 && this.mapArr[_loc3_ + 1][_loc2_].go == 0)
            {
               if(!this.inArr(this.mapArr[_loc3_ + 1][_loc2_ + 1],this.closeList) && !this.inArr(this.mapArr[_loc3_ + 1][_loc2_ + 1],this.openList))
               {
                  this.setGHF(this.mapArr[_loc3_ + 1][_loc2_ + 1],param1,14);
                  this.openList.push(this.mapArr[_loc3_ + 1][_loc2_ + 1]);
               }
            }
         }
         if(_loc3_ > 0 && this.mapArr[_loc3_ - 1][_loc2_].go == 0)
         {
            if(!this.inArr(this.mapArr[_loc3_ - 1][_loc2_],this.closeList))
            {
               if(!this.inArr(this.mapArr[_loc3_ - 1][_loc2_],this.openList))
               {
                  this.setGHF(this.mapArr[_loc3_ - 1][_loc2_],param1,10);
                  this.openList.push(this.mapArr[_loc3_ - 1][_loc2_]);
               }
               else
               {
                  this.checkG(this.mapArr[_loc3_ - 1][_loc2_],param1);
               }
            }
         }
         if(_loc3_ < this.h && this.mapArr[_loc3_ + 1][_loc2_].go == 0)
         {
            if(!this.inArr(this.mapArr[_loc3_ + 1][_loc2_],this.closeList))
            {
               if(!this.inArr(this.mapArr[_loc3_ + 1][_loc2_],this.openList))
               {
                  this.setGHF(this.mapArr[_loc3_ + 1][_loc2_],param1,10);
                  this.openList.push(this.mapArr[_loc3_ + 1][_loc2_]);
               }
               else
               {
                  this.checkG(this.mapArr[_loc3_ + 1][_loc2_],param1);
               }
            }
         }
      }
      
      private function inArr(param1:MovieClip, param2:Array) : Boolean
      {
         var _loc3_:* = undefined;
         for each(_loc3_ in param2)
         {
            if(param1 == _loc3_)
            {
               return true;
            }
         }
         return false;
      }
      
      private function setGHF(param1:MovieClip, param2:MovieClip, param3:*) : void
      {
         if(!param2.G)
         {
            param2.G = 0;
         }
         param1.G = param2.G + param3;
         param1.H = (Math.abs(param1.px - this.endPoint.px) + Math.abs(param1.py - this.endPoint.py)) * 10;
         param1.F = param1.H + param1.G;
         param1.father = param2;
      }
      
      private function checkG(param1:MovieClip, param2:MovieClip) : void
      {
         var _loc3_:* = param2.G + 10;
         if(_loc3_ <= param1.G)
         {
            param1.G = _loc3_;
            param1.F = param1.H + _loc3_;
            param1.father = param2;
         }
      }
      
      private function getMinF() : uint
      {
         var _loc3_:uint = 0;
         var _loc4_:* = undefined;
         var _loc1_:uint = 100000000;
         var _loc2_:uint = 0;
         for each(_loc4_ in this.openList)
         {
            if(_loc4_.F < _loc1_)
            {
               _loc1_ = _loc4_.F;
               _loc3_ = _loc2_;
            }
            _loc2_++;
         }
         return _loc3_;
      }
   }
}
