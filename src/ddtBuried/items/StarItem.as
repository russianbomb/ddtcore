package ddtBuried.items
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   
   public class StarItem extends Sprite
   {
       
      
      private var _list:Vector.<MovieClip>;
      
      public function StarItem()
      {
         super();
         this.initStarList();
      }
      
      private function initStarList() : void
      {
         var _loc2_:MovieClip = null;
         this._list = new Vector.<MovieClip>();
         var _loc1_:int = 0;
         while(_loc1_ < 5)
         {
            _loc2_ = ComponentFactory.Instance.creat("buried.core.improveEffect");
            _loc2_.x = (_loc2_.width + 2) * _loc1_;
            _loc2_.stop();
            addChild(_loc2_);
            this._list.push(_loc2_);
            _loc1_++;
         }
      }
      
      private function clearMc() : void
      {
         var _loc2_:MovieClip = null;
         var _loc1_:int = 0;
         while(_loc1_ < 5)
         {
            this._list[_loc1_].stop();
            while(this._list[_loc1_].numChildren)
            {
               if(this._list[_loc1_].getChildAt(0) is MovieClip)
               {
                  _loc2_ = this._list[_loc1_].getChildAt(0) as MovieClip;
                  while(_loc2_.numChildren)
                  {
                     ObjectUtils.disposeObject(_loc2_.getChildAt(0));
                  }
               }
               ObjectUtils.disposeObject(this._list[_loc1_].getChildAt(0));
            }
            _loc1_++;
         }
      }
      
      public function setStarList(param1:int) : void
      {
         var _loc2_:int = 0;
         while(_loc2_ < param1)
         {
            this._list[_loc2_].play();
            _loc2_++;
         }
      }
      
      public function updataStarLevel(param1:int) : void
      {
         this._list[param1 - 1].play();
      }
      
      public function dispose() : void
      {
         if(this._list)
         {
            this.clearMc();
         }
         while(numChildren)
         {
            ObjectUtils.disposeObject(getChildAt(0));
         }
         this._list = null;
      }
   }
}
