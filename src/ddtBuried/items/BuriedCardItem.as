package ddtBuried.items
{
   import bagAndInfo.cell.BagCell;
   import baglocked.BaglockedManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.manager.ItemManager;
   import ddt.manager.LanguageMgr;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddtBuried.BuriedEvent;
   import ddtBuried.BuriedManager;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   
   public class BuriedCardItem extends Sprite implements Disposeable
   {
       
      
      private var _mc:MovieClip;
      
      public var id:int;
      
      private var _tempID:int;
      
      private var _count:int;
      
      private var _isPress:Boolean;
      
      public function BuriedCardItem()
      {
         super();
         this.initView();
         useHandCursor = true;
         buttonMode = true;
         this.iniEvents();
      }
      
      private function iniEvents() : void
      {
         addEventListener(MouseEvent.CLICK,this.mouseClickHander);
      }
      
      private function removeEvents() : void
      {
         removeEventListener(MouseEvent.CLICK,this.mouseClickHander);
      }
      
      public function setGoodsInfo(param1:int, param2:int) : void
      {
         this._tempID = param1;
         this._count = param2;
      }
      
      public function set isPress(param1:Boolean) : void
      {
         this._isPress = param1;
      }
      
      private function mouseClickHander(param1:MouseEvent) : void
      {
         var _loc2_:int = 0;
         _loc2_ = int(BuriedManager.Instance.getTakeCardPay());
         SoundManager.instance.playButtonSound();
         BuriedManager.Instance.currCardIndex = this.id;
         if(this._isPress)
         {
            return;
         }
         this._isPress = true;
         this.mouseChildren = false;
         this.mouseEnabled = false;
         if(_loc2_ == 0)
         {
            SocketManager.Instance.out.takeCard();
         }
         else
         {
            if(PlayerManager.Instance.Self.bagLocked)
            {
               BaglockedManager.Instance.show();
               return;
            }
            if(BuriedManager.Instance.getRemindOverCard())
            {
               if(BuriedManager.Instance.checkMoney(BuriedManager.Instance.getRemindOverBind(),int(BuriedManager.Instance.getTakeCardPay()),SocketManager.Instance.out.takeCard))
               {
                  this._isPress = false;
                  BuriedManager.Instance.setRemindOverCard(false);
                  BuriedManager.Instance.setRemindOverBind(false);
                  BuriedManager.Instance.showTransactionFrame(LanguageMgr.GetTranslation("buried.alertInfo.rollDice",_loc2_),this.payForCardHander,this.clickCallBack);
                  return;
               }
               SocketManager.Instance.out.takeCard(BuriedManager.Instance.getRemindOverBind());
               return;
            }
            BuriedManager.Instance.showTransactionFrame(LanguageMgr.GetTranslation("buried.alertInfo.rollDice",_loc2_),this.payForCardHander,this.clickCallBack,this);
            this.mouseChildren = true;
            this.mouseEnabled = true;
         }
      }
      
      private function clickCallBack(param1:Boolean) : void
      {
         BuriedManager.Instance.setRemindOverCard(param1);
      }
      
      public function play() : void
      {
         this._mc.play();
         this.mouseChildren = false;
         this.mouseEnabled = false;
      }
      
      private function payForCardHander(param1:Boolean) : void
      {
         var _loc2_:int = int(BuriedManager.Instance.getTakeCardPay());
         if(BuriedManager.Instance.checkMoney(param1,_loc2_,SocketManager.Instance.out.takeCard))
         {
            this._isPress = false;
            BuriedManager.Instance.setRemindOverCard(false);
            BuriedManager.Instance.showTransactionFrame(LanguageMgr.GetTranslation("buried.alertInfo.rollDice",_loc2_),this.payForCardHander,this.clickCallBack,this);
            return;
         }
         if(BuriedManager.Instance.getRemindOverCard())
         {
            BuriedManager.Instance.setRemindOverBind(param1);
         }
         SocketManager.Instance.out.takeCard(param1);
      }
      
      private function initView() : void
      {
         this._mc = ComponentFactory.Instance.creat("buried.card.fanpai");
         this._mc.x = 1;
         this._mc.y = -11;
         this._mc.stop();
         addChild(this._mc);
         this._mc.addFrameScript(80,this.takeOver);
         this._mc.addFrameScript(9,this.initCard);
      }
      
      private function initCard() : void
      {
         var _loc1_:ItemTemplateInfo = null;
         var _loc2_:BagCell = null;
         _loc1_ = ItemManager.Instance.getTemplateById(this._tempID);
         _loc2_ = new BagCell(0,_loc1_);
         _loc2_.x = 39;
         _loc2_.y = 107;
         _loc2_.setBgVisible(false);
         _loc2_.setCount(this._count);
         this._mc["mc"].addChild(_loc2_);
         this._mc["mc"].goodsName.text = _loc1_.Name;
      }
      
      private function takeOver() : void
      {
         if(this._mc)
         {
            this._mc.stop();
         }
         this.mouseEnabled = false;
         this.mouseChildren = false;
         BuriedManager.evnetDispatch.dispatchEvent(new BuriedEvent(BuriedEvent.CARD_TAKE_OVER));
      }
      
      public function dispose() : void
      {
         this._isPress = false;
         this.removeEvents();
         if(this._mc)
         {
            this._mc.stop();
            if(this._mc["mc"])
            {
               while(this._mc["mc"].numChildren)
               {
                  ObjectUtils.disposeObject(this._mc["mc"].getChildAt(0));
               }
            }
            while(this._mc.numChildren)
            {
               ObjectUtils.disposeObject(this._mc.getChildAt(0));
            }
         }
         while(numChildren)
         {
            ObjectUtils.disposeObject(getChildAt(0));
         }
         this._mc = null;
      }
   }
}
