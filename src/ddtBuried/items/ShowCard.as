package ddtBuried.items
{
   import bagAndInfo.cell.BagCell;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.manager.ItemManager;
   import ddtBuried.BuriedEvent;
   import ddtBuried.BuriedManager;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   
   public class ShowCard extends Sprite
   {
       
      
      private var _mc:MovieClip;
      
      private var _list:Vector.<BagCell>;
      
      public function ShowCard()
      {
         super();
         this.initView();
      }
      
      private function initView() : void
      {
         this._list = new Vector.<BagCell>();
         this._mc = ComponentFactory.Instance.creat("buried.card.show");
         addChild(this._mc);
         this._mc.addFrameScript(69,this.cradOver);
         this._mc.addFrameScript(10,this.intCardShow);
      }
      
      private function intCardShow() : void
      {
         var _loc2_:int = 0;
         var _loc3_:Object = null;
         var _loc4_:ItemTemplateInfo = null;
         var _loc5_:BagCell = null;
         var _loc1_:int = BuriedManager.Instance.cardInitList.length;
         _loc2_ = 0;
         while(_loc2_ < _loc1_)
         {
            _loc3_ = new Object();
            _loc3_.tempID = BuriedManager.Instance.cardInitList[_loc2_].tempID;
            _loc3_.count = BuriedManager.Instance.cardInitList[_loc2_].count;
            _loc4_ = ItemManager.Instance.getTemplateById(_loc3_.tempID);
            _loc5_ = new BagCell(0,_loc4_);
            _loc5_.x = 39;
            _loc5_.y = 107;
            _loc5_.setBgVisible(false);
            _loc5_.setCount(_loc3_.count);
            this._mc["card" + (_loc2_ + 1)].addChild(_loc5_);
            this._list.push(_loc5_);
            this._mc["card" + (_loc2_ + 1)].goodsName.text = _loc4_.Name;
            _loc2_++;
         }
      }
      
      private function clearCell() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ < this._list.length)
         {
            this._list[_loc1_].dispose();
            ObjectUtils.disposeObject(this._list[_loc1_]);
            this._list[_loc1_] = null;
            while(this._mc["card" + (_loc1_ + 1)].numChildren)
            {
               ObjectUtils.disposeObject(this._mc["card" + (_loc1_ + 1)].getChildAt(0));
            }
            _loc1_++;
         }
      }
      
      public function play() : void
      {
         this._mc.play();
      }
      
      public function resetFrame() : void
      {
         this._mc.gotoAndStop(1);
      }
      
      private function cradOver() : void
      {
         this._mc.stop();
         BuriedManager.evnetDispatch.dispatchEvent(new BuriedEvent(BuriedEvent.CARD_SHOW_OVER));
      }
      
      public function dispose() : void
      {
         if(this._list)
         {
            this.clearCell();
         }
         if(this._mc)
         {
            this._mc.stop();
            while(this._mc.numChildren)
            {
               ObjectUtils.disposeObject(this._mc.getChildAt(0));
            }
         }
         while(numChildren)
         {
            ObjectUtils.disposeObject(getChildAt(0));
         }
         this._mc = null;
         this._list = null;
      }
   }
}
