package ddtBuried.views
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.events.PlayerPropertyEvent;
   import ddt.manager.LanguageMgr;
   import ddt.manager.PlayerManager;
   import ddt.manager.SoundManager;
   import ddtBuried.BuriedEvent;
   import ddtBuried.BuriedManager;
   import ddtBuried.items.BuriedCardItem;
   import ddtBuried.items.BuriedItem;
   import ddtBuried.items.BuriedReturnBtn;
   import ddtBuried.items.ShowCard;
   import ddtBuried.items.WashCard;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   
   public class BuriedView extends Sprite implements Disposeable
   {
      
      private static const GOODSID:int = 11680;
       
      
      private var _btnList:Vector.<BuriedItem>;
      
      private var _cardList:Vector.<BuriedCardItem>;
      
      private var _iconList:Array;
      
      private var _moneyList:Array;
      
      private var _back:Bitmap;
      
      private var _shopBtn:SimpleBitmapButton;
      
      private var _startBtn:SimpleBitmapButton;
      
      private var _showCard:ShowCard;
      
      private var _washCard:WashCard;
      
      private var _cardContent:Sprite;
      
      private var _returnBtn:BuriedReturnBtn;
      
      private var _fileTxt:FilterFrameText;
      
      private var _wordBack:Bitmap;
      
      public function BuriedView()
      {
         this._iconList = ["buried.stone","buried.bindMoney","buried.money"];
         this._moneyList = [BuriedManager.Instance.getBuriedStoneNum(),PlayerManager.Instance.Self.Money.toString(),PlayerManager.Instance.Self.BandMoney.toString()];
         super();
         this._btnList = new Vector.<BuriedItem>();
         this._cardList = new Vector.<BuriedCardItem>();
         this.initView();
         this.initEvents();
      }
      
      private function initEvents() : void
      {
         this._startBtn.addEventListener(MouseEvent.CLICK,this.startHandler);
         this._shopBtn.addEventListener(MouseEvent.CLICK,this.openShopHander);
         BuriedManager.evnetDispatch.addEventListener(BuriedEvent.CARD_SHOW_OVER,this.cardShowOverHander);
         BuriedManager.evnetDispatch.addEventListener(BuriedEvent.CARD_WASH_START,this.cardWashStartHander);
         BuriedManager.evnetDispatch.addEventListener(BuriedEvent.CARD_WASH_OVER,this.cardWashHander);
         BuriedManager.evnetDispatch.addEventListener(BuriedEvent.TAKE_CARD,this.cardTakeHander);
         BuriedManager.evnetDispatch.addEventListener(BuriedEvent.UpDate_Stone_Count,this.upDateStoneHander);
         PlayerManager.Instance.Self.addEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.onUpdate);
      }
      
      private function removeEvents() : void
      {
         this._shopBtn.removeEventListener(MouseEvent.CLICK,this.openShopHander);
         this._startBtn.removeEventListener(MouseEvent.CLICK,this.startHandler);
         BuriedManager.evnetDispatch.removeEventListener(BuriedEvent.UpDate_Stone_Count,this.upDateStoneHander);
         PlayerManager.Instance.Self.removeEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.onUpdate);
         BuriedManager.evnetDispatch.removeEventListener(BuriedEvent.CARD_SHOW_OVER,this.cardShowOverHander);
         BuriedManager.evnetDispatch.removeEventListener(BuriedEvent.CARD_WASH_START,this.cardWashStartHander);
         BuriedManager.evnetDispatch.removeEventListener(BuriedEvent.CARD_WASH_OVER,this.cardWashHander);
         BuriedManager.evnetDispatch.removeEventListener(BuriedEvent.TAKE_CARD,this.cardTakeHander);
      }
      
      private function upDateStoneHander(param1:BuriedEvent) : void
      {
         this._btnList[0].upDateTxt(BuriedManager.Instance.getBuriedStoneNum());
      }
      
      private function openShopHander(param1:MouseEvent) : void
      {
         BuriedManager.Instance.openshopHander();
         SoundManager.instance.playButtonSound();
      }
      
      private function onUpdate(param1:PlayerPropertyEvent) : void
      {
         this._btnList[1].upDateTxt(PlayerManager.Instance.Self.Money.toString());
         this._btnList[2].upDateTxt(PlayerManager.Instance.Self.BandMoney.toString());
      }
      
      private function returnToDice(param1:MouseEvent) : void
      {
         SoundManager.instance.playButtonSound();
         this.dispose();
      }
      
      private function cardTakeHander(param1:BuriedEvent) : void
      {
         var _loc2_:Object = param1.data;
         this._cardList[BuriedManager.Instance.currCardIndex].play();
         this._cardList[BuriedManager.Instance.currCardIndex].setGoodsInfo(param1.data.tempID,param1.data.count);
      }
      
      private function cardWashStartHander(param1:BuriedEvent) : void
      {
         if(!this._washCard)
         {
            this._washCard = new WashCard();
            addChild(this._washCard);
         }
         this._washCard.visible = true;
         this._washCard.play();
      }
      
      private function cardWashHander(param1:BuriedEvent) : void
      {
         this._cardContent.visible = true;
         this._washCard.visible = false;
         this._washCard.resetFrame();
      }
      
      private function cardShowOverHander(param1:BuriedEvent) : void
      {
         this._startBtn.mouseEnabled = true;
      }
      
      private function startHandler(param1:MouseEvent) : void
      {
         SoundManager.instance.playButtonSound();
         this._startBtn.mouseEnabled = false;
         this._startBtn.visible = false;
         this._showCard.visible = false;
         if(!this._washCard)
         {
            this._washCard = new WashCard();
            this._washCard.x = 492;
            this._washCard.y = 242;
            addChild(this._washCard);
         }
         this._washCard.visible = true;
         this._washCard.play();
      }
      
      private function initView() : void
      {
         var _loc3_:BuriedItem = null;
         var _loc4_:BuriedCardItem = null;
         this._back = ComponentFactory.Instance.creat("buried.open.back");
         addChild(this._back);
         this._shopBtn = ComponentFactory.Instance.creatComponentByStylename("buried.shopBtn");
         addChild(this._shopBtn);
         this._startBtn = ComponentFactory.Instance.creatComponentByStylename("buried.findBtn");
         addChild(this._startBtn);
         this._startBtn.mouseEnabled = false;
         this._wordBack = ComponentFactory.Instance.creat("buried.word.back");
         this._wordBack.x = 150;
         this._wordBack.y = 360;
         addChild(this._wordBack);
         var _loc1_:int = 0;
         while(_loc1_ < 3)
         {
            _loc3_ = new BuriedItem(this._moneyList[_loc1_],this._iconList[_loc1_]);
            _loc3_.x = (_loc3_.width + 4) * _loc1_ + 139;
            _loc3_.y = 554;
            addChild(_loc3_);
            this._btnList.push(_loc3_);
            _loc1_++;
         }
         this._cardContent = new Sprite();
         this._cardContent.x = 101;
         this._cardContent.y = 118;
         this._cardContent.visible = false;
         addChild(this._cardContent);
         var _loc2_:int = 0;
         while(_loc2_ < 5)
         {
            _loc4_ = new BuriedCardItem();
            _loc4_.id = _loc2_;
            _loc4_.x = (_loc4_.width + 19) * _loc2_;
            this._cardContent.addChild(_loc4_);
            this._cardList.push(_loc4_);
            _loc2_++;
         }
         this._showCard = new ShowCard();
         this._showCard.x = 493;
         this._showCard.y = 306;
         addChild(this._showCard);
         this._returnBtn = new BuriedReturnBtn();
         addChild(this._returnBtn);
         this._fileTxt = ComponentFactory.Instance.creatComponentByStylename("ddtburied.view.descriptTxt2");
         this._fileTxt.text = LanguageMgr.GetTranslation("buried.alertInfo.buriedAtion");
         this._fileTxt.x = (1000 - this._fileTxt.width) / 2;
         addChild(this._fileTxt);
      }
      
      private function clearCardItem() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ < this._cardList.length)
         {
            this._cardList[_loc1_].dispose();
            ObjectUtils.disposeObject(this._cardList[_loc1_]);
            this._cardList[_loc1_] = null;
            _loc1_++;
         }
      }
      
      private function clearBtnList() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ < this._btnList.length)
         {
            this._btnList[_loc1_].dispose();
            ObjectUtils.disposeObject(this._btnList[_loc1_]);
            this._btnList[_loc1_] = null;
            _loc1_++;
         }
      }
      
      public function dispose() : void
      {
         this.removeEvents();
         BuriedManager.Instance.currCardIndex = 0;
         if(this._cardList)
         {
            this.clearCardItem();
         }
         if(this._btnList)
         {
            this.clearBtnList();
         }
         this._showCard.dispose();
         if(this._washCard)
         {
            this._washCard.dispose();
         }
         this._returnBtn.dispose();
         while(numChildren)
         {
            ObjectUtils.disposeObject(getChildAt(0));
         }
         this._cardList = null;
         this._btnList = null;
      }
   }
}
