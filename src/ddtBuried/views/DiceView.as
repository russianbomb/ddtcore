package ddtBuried.views
{
   import bagAndInfo.cell.BagCell;
   import baglocked.BaglockedManager;
   import com.greensock.TweenMax;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.manager.ItemManager;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.manager.TaskManager;
   import ddt.utils.PositionUtils;
   import ddtBuried.BuriedEvent;
   import ddtBuried.BuriedManager;
   import ddtBuried.items.BuriedBox;
   import ddtBuried.items.BuriedReturnBtn;
   import ddtBuried.items.DiceRoll;
   import ddtBuried.items.StarItem;
   import ddtBuried.map.Scence1;
   import ddtBuried.role.BuriedPlayer;
   import flash.display.Bitmap;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import flash.geom.Point;
   
   public class DiceView extends Sprite implements Disposeable
   {
       
      
      private var _back:Bitmap;
      
      private var _mapContent:Sprite;
      
      private var _downBack:Bitmap;
      
      private var _starBtn:SimpleBitmapButton;
      
      private var _starBtnTip:SimpleBitmapButton;
      
      private var _freeBtn:SimpleBitmapButton;
      
      private var _shopBtn:SimpleBitmapButton;
      
      private var _scence:Scence1;
      
      private var _diceRoll:DiceRoll;
      
      private var _txtNum:FilterFrameText;
      
      private var _starItem:StarItem;
      
      private var _isWalkOver:Boolean = false;
      
      private var _buriedBox:BuriedBox;
      
      private var _returnBtn:BuriedReturnBtn;
      
      private var role:BuriedPlayer;
      
      private var rolePosition:int;
      
      private var rolePoint:Point;
      
      private var roleNowPosition:int;
      
      private var index:uint;
      
      private var _walkArray:Array;
      
      private var _fountain1:MovieClip;
      
      private var _fountain2:MovieClip;
      
      private var cell:BagCell;
      
      private var _fileTxt:FilterFrameText;
      
      private var _helpBtn:SimpleBitmapButton;
      
      private var _isRemoeEvent:Boolean;
      
      private var _isOneStep:Boolean;
      
      private var currPos:int;
      
      private var onstep:int;
      
      private var _isCount:Boolean = false;
      
      private var _isWalkOK:Boolean = true;
      
      private var _taskTrackView:TaskTrackView;
      
      public function DiceView()
      {
         super();
         this.initView();
         this.initEvents();
      }
      
      private function initView() : void
      {
         if(BuriedManager.Instance.mapID == BuriedManager.MAP1)
         {
            this._walkArray = BuriedManager.Instance.mapArrays.roadMapsList1;
         }
         else if(BuriedManager.Instance.mapID == BuriedManager.MAP2)
         {
            this._walkArray = BuriedManager.Instance.mapArrays.roadMapsList2;
         }
         else if(BuriedManager.Instance.mapID == BuriedManager.MAP3)
         {
            this._walkArray = BuriedManager.Instance.mapArrays.roadMapsList3;
         }
         this.rolePosition = BuriedManager.Instance.nowPosition;
         this.rolePoint = new Point(this._walkArray[this.rolePosition].x,this._walkArray[this.rolePosition].y);
         this._back = ComponentFactory.Instance.creat("buried.shaizi.back");
         addChild(this._back);
         this._fountain1 = ComponentFactory.Instance.creat("buried.dice.fountain");
         this._fountain1.scaleX = this._fountain1.scaleY = 2.5;
         this._fountain1.x = 64;
         this._fountain1.y = 460;
         addChild(this._fountain1);
         this._fountain2 = ComponentFactory.Instance.creat("buried.dice.fountain");
         this._fountain2.scaleX = this._fountain2.scaleY = 2.5;
         this._fountain2.x = 955;
         this._fountain2.y = 460;
         addChild(this._fountain2);
         this._fileTxt = ComponentFactory.Instance.creatComponentByStylename("ddtburied.right.aTtionTxt");
         this._fileTxt.text = LanguageMgr.GetTranslation("buried.alertInfo.diceAtion");
         addChild(this._fileTxt);
         this._mapContent = new Sprite();
         addChild(this._mapContent);
         this._downBack = ComponentFactory.Instance.creat("buried.shaizi.downBack");
         addChild(this._downBack);
         this._starBtnTip = ComponentFactory.Instance.creatComponentByStylename("ddtburied.updateStar");
         this._starBtnTip.tipData = LanguageMgr.GetTranslation("buried.alertInfo.starBtnTipfree");
         addChild(this._starBtnTip);
         this._starBtnTip.alpha = 0;
         this._starBtnTip.visible = false;
         this._starBtn = ComponentFactory.Instance.creatComponentByStylename("ddtburied.updateStar");
         this._starBtn.tipData = LanguageMgr.GetTranslation("buried.alertInfo.starBtnTipfree");
         if(BuriedManager.Instance.nowPosition > 0)
         {
            this._starBtn.enable = false;
            this._starBtnTip.visible = true;
         }
         addChild(this._starBtn);
         this._freeBtn = ComponentFactory.Instance.creatComponentByStylename("ddtburied.freeBtn");
         this._freeBtn.tipData = LanguageMgr.GetTranslation("buried.alertInfo.btnTipfree");
         addChild(this._freeBtn);
         this._txtNum = ComponentFactory.Instance.creatComponentByStylename("ddtburied.right.btnTxt");
         this._freeBtn.addChild(this._txtNum);
         this._shopBtn = ComponentFactory.Instance.creatComponentByStylename("ddtburied.shopBtn");
         addChild(this._shopBtn);
         this._helpBtn = ComponentFactory.Instance.creatComponentByStylename("ddtburied.helpBtn");
         addChild(this._helpBtn);
         this._starItem = new StarItem();
         this._starItem.x = 374;
         this._starItem.y = 547;
         addChild(this._starItem);
         this._diceRoll = new DiceRoll();
         this._diceRoll.x = 157;
         this._diceRoll.y = 238;
         addChild(this._diceRoll);
         this._taskTrackView = new TaskTrackView();
         PositionUtils.setPos(this._taskTrackView,"ddtburied.taskTrack.pos");
         this.role = new BuriedPlayer(PlayerManager.Instance.Self,this.roleCallback);
         this._returnBtn = new BuriedReturnBtn();
         addChild(this._returnBtn);
         this._buriedBox = new BuriedBox();
         this._buriedBox.visible = false;
         addChild(this._buriedBox);
      }
      
      private function roleCallback(param1:BuriedPlayer, param2:Boolean, param3:int = 0) : void
      {
         if(param3 == 0)
         {
            if(!param1)
            {
               return;
            }
            param1.sceneCharacterStateType = "natural";
            param1.update();
            param1.x = this.rolePoint.x;
            param1.y = this.rolePoint.y;
            addChild(param1);
            addChild(this._taskTrackView);
         }
      }
      
      public function upDataBtn() : void
      {
         if(this._isCount)
         {
            if(BuriedManager.Instance.getPayData())
            {
               this._freeBtn.tipData = LanguageMgr.GetTranslation("buried.alertInfo.btnTip",BuriedManager.Instance.getPayData().NeedMoney);
            }
            else
            {
               this._freeBtn.tipData = LanguageMgr.GetTranslation("buried.alertInfo.DiceOver");
            }
            this._freeBtn.addChild(this._txtNum);
            return;
         }
         var _loc1_:Boolean = true;
         if(this._freeBtn)
         {
            _loc1_ = this._freeBtn.mouseEnabled;
            this._freeBtn.removeEventListener(MouseEvent.CLICK,this.rollClickHander);
            ObjectUtils.disposeObject(this._freeBtn);
            this._freeBtn = null;
         }
         this._freeBtn = ComponentFactory.Instance.creatComponentByStylename("ddtburied.payBtn");
         this._freeBtn.mouseEnabled = _loc1_;
         this._isCount = true;
         if(BuriedManager.Instance.getPayData())
         {
            this._freeBtn.tipData = LanguageMgr.GetTranslation("buried.alertInfo.btnTip",BuriedManager.Instance.getPayData().NeedMoney);
         }
         else
         {
            this._freeBtn.tipData = LanguageMgr.GetTranslation("buried.alertInfo.DiceOver");
         }
         addChild(this._freeBtn);
         this._freeBtn.addChild(this._txtNum);
         this._freeBtn.addEventListener(MouseEvent.CLICK,this.rollClickHander);
      }
      
      public function setTxt(param1:String) : void
      {
         if(param1 == "0")
         {
            this._txtNum.text = "";
         }
         else
         {
            this._txtNum.text = "(" + param1 + ")";
         }
      }
      
      public function setCrFrame(param1:String) : void
      {
         this._diceRoll.setCrFrame(param1);
      }
      
      public function play() : void
      {
         this._diceRoll.play();
      }
      
      private function initEvents() : void
      {
         this._freeBtn.addEventListener(MouseEvent.CLICK,this.rollClickHander);
         this._shopBtn.addEventListener(MouseEvent.CLICK,this.openshopHander);
         this._starBtn.addEventListener(MouseEvent.CLICK,this.uplevelClickHander);
         this._helpBtn.addEventListener(MouseEvent.CLICK,this.openHelpViewHander);
         BuriedManager.evnetDispatch.addEventListener(BuriedEvent.RemoveEvent,this.removeEventHandler);
         BuriedManager.evnetDispatch.addEventListener(BuriedEvent.DICEOVER,this.diceOverHandler);
         BuriedManager.evnetDispatch.addEventListener(BuriedEvent.MAPOVER,this.mapOverHandler);
         BuriedManager.evnetDispatch.addEventListener(BuriedEvent.WALKOVER,this.roleWalkOverHander);
         BuriedManager.evnetDispatch.addEventListener(BuriedEvent.UPDATABTNSTATS,this.starBtnstatsHander);
         BuriedManager.evnetDispatch.addEventListener(BuriedEvent.BOXMOVIE_OVER,this.boxMoiveOverHander);
         BuriedManager.evnetDispatch.addEventListener(BuriedEvent.OneStep,this.oneStepHander);
      }
      
      private function removeEvents() : void
      {
         this._freeBtn.removeEventListener(MouseEvent.CLICK,this.rollClickHander);
         this._shopBtn.removeEventListener(MouseEvent.CLICK,this.openshopHander);
         this._starBtn.removeEventListener(MouseEvent.CLICK,this.uplevelClickHander);
         this._helpBtn.removeEventListener(MouseEvent.CLICK,this.openHelpViewHander);
         BuriedManager.evnetDispatch.removeEventListener(BuriedEvent.OneStep,this.oneStepHander);
         BuriedManager.evnetDispatch.removeEventListener(BuriedEvent.RemoveEvent,this.removeEventHandler);
         BuriedManager.evnetDispatch.removeEventListener(BuriedEvent.DICEOVER,this.diceOverHandler);
         BuriedManager.evnetDispatch.removeEventListener(BuriedEvent.MAPOVER,this.mapOverHandler);
         BuriedManager.evnetDispatch.removeEventListener(BuriedEvent.UPDATABTNSTATS,this.starBtnstatsHander);
         BuriedManager.evnetDispatch.removeEventListener(BuriedEvent.WALKOVER,this.roleWalkOverHander);
         BuriedManager.evnetDispatch.removeEventListener(BuriedEvent.BOXMOVIE_OVER,this.boxMoiveOverHander);
      }
      
      private function oneStepHander(param1:BuriedEvent) : void
      {
         var _loc2_:Object = param1.data;
         this.onstep = _loc2_.key;
         this._isOneStep = true;
      }
      
      private function removeEventHandler(param1:BuriedEvent) : void
      {
         var _loc2_:Object = param1.data;
         this.currPos = _loc2_.key;
         this._isRemoeEvent = true;
      }
      
      private function openHelpViewHander(param1:MouseEvent) : void
      {
         SoundManager.instance.playButtonSound();
         var _loc2_:DuriedHelpFrame = ComponentFactory.Instance.creatComponentByStylename("ddtburied.view.helpFrame");
         _loc2_.addEventListener(FrameEvent.RESPONSE,this.frameEvent);
         LayerManager.Instance.addToLayer(_loc2_,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
      }
      
      private function frameEvent(param1:FrameEvent) : void
      {
         SoundManager.instance.playButtonSound();
         param1.currentTarget.dispose();
      }
      
      private function boxMoiveOverHander(param1:BuriedEvent) : void
      {
         var _loc2_:BaseAlerFrame = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("tips"),LanguageMgr.GetTranslation("buried.alertInfo.mapover"),LanguageMgr.GetTranslation("ok"),"",true,true,true,LayerManager.ALPHA_BLOCKGOUND,null,"SimpleAlert",60,false);
         _loc2_.addEventListener(FrameEvent.RESPONSE,this.onMapOverResponse);
      }
      
      private function starBtnstatsHander(param1:BuriedEvent) : void
      {
         this._starBtn.enable = true;
         this._starBtnTip.visible = false;
      }
      
      private function flyGoods() : void
      {
         var _loc1_:int = BuriedManager.Instance.currGoodID;
         var _loc2_:ItemTemplateInfo = ItemManager.Instance.getTemplateById(_loc1_);
         this.cell = new BagCell(0,_loc2_);
         this.cell.x = this.role.x;
         this.cell.y = this.role.y;
         addChild(this.cell);
         TweenMax.to(this.cell,1,{
            "x":461,
            "y":518,
            "scaleX":0.1,
            "scaleY":0.1,
            "onComplete":this.comp,
            "bezier":[{
               "x":this.role.x,
               "y":this.role.y - 80
            }]
         });
      }
      
      private function comp() : void
      {
         ObjectUtils.disposeObject(this.cell);
      }
      
      private function walkContinueHander(param1:BuriedEvent) : void
      {
         var _loc2_:Array = null;
         if(BuriedManager.Instance.isGetGoods)
         {
            BuriedManager.Instance.isGetGoods = false;
            this._scence.updateRoadPoint();
            this.flyGoods();
         }
         if(this._isWalkOver)
         {
            this._isWalkOver = false;
            BuriedManager.Instance.isContinue = false;
            BuriedManager.Instance.nowPosition = BuriedManager.Instance.eventPosition;
            _loc2_ = this.configRoadArray();
            this.role.roleWalk(_loc2_);
            this.roleWalkPosition(BuriedManager.Instance.nowPosition);
         }
      }
      
      private function roleWalkOverHander(param1:BuriedEvent) : void
      {
         var _loc2_:Array = null;
         this._isWalkOK = true;
         this._isWalkOver = true;
         if(BuriedManager.Instance.isBackToStart)
         {
            BuriedManager.Instance.isBackToStart = false;
            this._starBtn.enable = true;
            this._starBtnTip.visible = false;
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("buried.alertInfo.toStart"));
         }
         else if(BuriedManager.Instance.isGo)
         {
            BuriedManager.Instance.isGo = false;
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("buried.alertInfo.toGo"));
         }
         else if(BuriedManager.Instance.isGoEnd)
         {
            BuriedManager.Instance.isOver = true;
            BuriedManager.Instance.isGoEnd = false;
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("buried.alertInfo.toEnd"));
         }
         else if(BuriedManager.Instance.isBack)
         {
            BuriedManager.Instance.isBack = false;
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("buried.alertInfo.toBack"));
         }
         if(BuriedManager.Instance.isOpenBuried)
         {
            BuriedManager.Instance.isOpenBuried = false;
            BuriedManager.evnetDispatch.dispatchEvent(new BuriedEvent(BuriedEvent.OPEN_BURIEDVIEW));
         }
         this._freeBtn.mouseEnabled = true;
         this._returnBtn.mouseEnabled = true;
         this._returnBtn.mouseChildren = true;
         if(this._isRemoeEvent && this._isWalkOver)
         {
            this._isRemoeEvent = false;
            this._scence.updateRoadPoint(this.currPos);
         }
         if(this._isOneStep && this._isWalkOver)
         {
            this._isOneStep = false;
            this._scence.updateRoadPoint(this.onstep);
         }
         this._taskTrackView.refreshTask();
         if(BuriedManager.Instance.isContinue)
         {
            BuriedManager.Instance.isContinue = false;
            this._freeBtn.mouseEnabled = false;
            this._returnBtn.mouseEnabled = false;
            this._returnBtn.mouseChildren = false;
            this._isWalkOver = false;
            BuriedManager.Instance.nowPosition = BuriedManager.Instance.eventPosition;
            this.roleWalkPosition(BuriedManager.Instance.nowPosition);
            _loc2_ = this.configRoadArray();
            this.role.roleWalk(_loc2_);
         }
         if(BuriedManager.Instance.isGetGoods && this._isWalkOver)
         {
            BuriedManager.Instance.isGetGoods = false;
            this._scence.updateRoadPoint();
            this.flyGoods();
         }
      }
      
      private function mapOverHandler(param1:BuriedEvent) : void
      {
         this._buriedBox.visible = true;
         this._buriedBox.initView(BuriedManager.Instance.level);
         this._buriedBox.play();
         this._freeBtn.mouseEnabled = false;
         this._returnBtn.mouseEnabled = false;
         this._returnBtn.mouseChildren = false;
      }
      
      private function onMapOverResponse(param1:FrameEvent) : void
      {
         var _loc2_:BaseAlerFrame = param1.currentTarget as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.onMapOverResponse);
         if(param1.responseCode == FrameEvent.ESC_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK || param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.CLOSE_CLICK)
         {
            SocketManager.Instance.out.refreshMap();
         }
         _loc2_.dispose();
      }
      
      private function diceOverHandler(param1:BuriedEvent) : void
      {
         var _loc2_:Array = this.configRoadArray();
         this._isWalkOK = false;
         this._isWalkOver = false;
         this.role.roleWalk(_loc2_);
         this.roleWalkPosition(BuriedManager.Instance.nowPosition);
         this.rolePosition = this.roleNowPosition;
         this._starBtn.enable = false;
         this._starBtnTip.visible = true;
         this._freeBtn.mouseEnabled = false;
         this._returnBtn.mouseEnabled = false;
         this._returnBtn.mouseChildren = false;
      }
      
      private function configRoadArray() : Array
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         this.roleNowPosition = BuriedManager.Instance.nowPosition;
         var _loc1_:Array = [];
         if(this.rolePosition < this.roleNowPosition)
         {
            _loc2_ = this.rolePosition;
            while(_loc2_ <= this.roleNowPosition)
            {
               _loc1_.push(this._walkArray[_loc2_]);
               _loc2_++;
            }
         }
         else
         {
            _loc3_ = this.rolePosition;
            while(_loc3_ >= this.roleNowPosition)
            {
               _loc1_.push(this._walkArray[_loc3_]);
               _loc3_--;
            }
         }
         this.rolePosition = this.roleNowPosition;
         return _loc1_;
      }
      
      private function roleWalkPosition(param1:int) : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         if(BuriedManager.Instance.mapID == BuriedManager.MAP1)
         {
            _loc2_ = BuriedManager.Instance.mapArrays.standArray1[param1].x;
            _loc3_ = BuriedManager.Instance.mapArrays.standArray1[param1].y;
         }
         else if(BuriedManager.Instance.mapID == BuriedManager.MAP2)
         {
            _loc2_ = BuriedManager.Instance.mapArrays.standArray2[param1].x;
            _loc3_ = BuriedManager.Instance.mapArrays.standArray2[param1].y;
         }
         else
         {
            _loc2_ = BuriedManager.Instance.mapArrays.standArray3[param1].x;
            _loc3_ = BuriedManager.Instance.mapArrays.standArray3[param1].y;
         }
         this._scence.selfFindPath(_loc2_,_loc3_);
      }
      
      private function rollClickHander(param1:MouseEvent) : void
      {
         if(BuriedManager.Instance.isOver)
         {
            return;
         }
         this._diceRoll.resetFrame();
         SoundManager.instance.playButtonSound();
         if(TaskManager.instance.isHaveBuriedQuest())
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("buried.alertInfo.questsContiniue"));
            return;
         }
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         if(BuriedManager.Instance.currPayLevel >= 0 && this._isWalkOK)
         {
            if(!BuriedManager.Instance.getPayData() && BuriedManager.Instance.currPayLevel >= 21)
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("buried.alertInfo.DiceOver"));
               return;
            }
            if(BuriedManager.Instance.getRemindRoll())
            {
               if(BuriedManager.Instance.checkMoney(BuriedManager.Instance.getRemindRollBind(),BuriedManager.Instance.getPayData().NeedMoney,SocketManager.Instance.out.rollDice))
               {
                  BuriedManager.Instance.setRemindRoll(false);
                  BuriedManager.Instance.setRemindRollBind(false);
                  BuriedManager.Instance.showTransactionFrame(LanguageMgr.GetTranslation("buried.alertInfo.rollDiceContiniue",BuriedManager.Instance.getPayData().NeedMoney),this.payRollHander,this.clickCallBack);
                  return;
               }
               SocketManager.Instance.out.rollDice(BuriedManager.Instance.getRemindRollBind());
               if(BuriedManager.Instance.finalNum <= 0)
               {
                  return;
               }
               this._isWalkOver = false;
               this._starBtn.enable = false;
               this._starBtnTip.visible = true;
               this._freeBtn.mouseEnabled = false;
               this._returnBtn.mouseEnabled = false;
               this._returnBtn.mouseChildren = false;
               return;
            }
            BuriedManager.Instance.showTransactionFrame(LanguageMgr.GetTranslation("buried.alertInfo.rollDiceContiniue",BuriedManager.Instance.getPayData().NeedMoney),this.payRollHander,this.clickCallBack);
            return;
         }
         SocketManager.Instance.out.rollDice();
         this._isWalkOK = false;
         this._isWalkOver = false;
         this._starBtn.enable = false;
         this._starBtnTip.visible = true;
         this._freeBtn.mouseEnabled = false;
         this._returnBtn.mouseEnabled = false;
         this._returnBtn.mouseChildren = false;
      }
      
      private function clickCallBack(param1:Boolean) : void
      {
         BuriedManager.Instance.setRemindRoll(param1);
      }
      
      private function payRollHander(param1:Boolean) : void
      {
         if(BuriedManager.Instance.checkMoney(param1,BuriedManager.Instance.getPayData().NeedMoney,SocketManager.Instance.out.rollDice))
         {
            BuriedManager.Instance.setRemindRoll(false);
            BuriedManager.Instance.showTransactionFrame(LanguageMgr.GetTranslation("buried.alertInfo.rollDiceContiniue",BuriedManager.Instance.getPayData().NeedMoney),this.payRollHander,this.clickCallBack);
            return;
         }
         if(BuriedManager.Instance.getRemindRoll())
         {
            BuriedManager.Instance.setRemindRollBind(param1);
         }
         SocketManager.Instance.out.rollDice(param1);
         this._freeBtn.mouseEnabled = false;
         this._starBtn.enable = false;
         this._returnBtn.mouseEnabled = false;
         this._returnBtn.mouseChildren = false;
         this._diceRoll.resetFrame();
      }
      
      private function openshopHander(param1:MouseEvent) : void
      {
         BuriedManager.Instance.openshopHander();
         SoundManager.instance.playButtonSound();
      }
      
      private function uplevelClickHander(param1:MouseEvent) : void
      {
         SoundManager.instance.playButtonSound();
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         if(BuriedManager.Instance.level >= 5)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("buried.alertInfo.starFull"));
            return;
         }
         var _loc2_:BaseAlerFrame = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("tips"),LanguageMgr.GetTranslation("buried.alertInfo.uplevel",BuriedManager.Instance.getUpdateData(true).NeedMoney),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),true,true,true,LayerManager.ALPHA_BLOCKGOUND,null,"SimpleAlert",60,false);
         _loc2_.addEventListener(FrameEvent.RESPONSE,this.onUpLevelResponse);
      }
      
      private function onUpLevelResponse(param1:FrameEvent) : void
      {
         var _loc2_:BaseAlerFrame = param1.currentTarget as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.onUpLevelResponse);
         if(param1.responseCode == FrameEvent.CLOSE_CLICK || param1.responseCode == FrameEvent.CANCEL_CLICK || param1.responseCode == FrameEvent.ESC_CLICK)
         {
            _loc2_.dispose();
            return;
         }
         if(param1.responseCode == FrameEvent.ENTER_CLICK || param1.responseCode == FrameEvent.SUBMIT_CLICK)
         {
            if(BuriedManager.Instance.checkMoney(_loc2_.isBand,BuriedManager.Instance.getUpdateData(true).NeedMoney,SocketManager.Instance.out.upgradeStartLevel))
            {
               _loc2_.dispose();
               return;
            }
            SocketManager.Instance.out.upgradeStartLevel(_loc2_.isBand);
         }
         _loc2_.dispose();
      }
      
      public function clearScene() : void
      {
         if(this._scence)
         {
            ObjectUtils.disposeObject(this._scence);
            this._scence = null;
         }
      }
      
      public function setStarList(param1:int) : void
      {
         this._starItem.setStarList(param1);
      }
      
      public function updataStarLevel(param1:int) : void
      {
         this._starItem.updataStarLevel(param1);
      }
      
      public function addMaps(param1:String, param2:int, param3:int, param4:int, param5:int) : void
      {
         this.clearScene();
         this._scence = new Scence1(param1,param2,param3);
         this._scence.x = param4;
         this._scence.y = param5;
         this._mapContent.addChild(this._scence);
      }
      
      public function dispose() : void
      {
         this.removeEvents();
         if(this._fountain1)
         {
            this._fountain1.stop();
            while(this._fountain1.numChildren)
            {
               ObjectUtils.disposeObject(this._fountain1.getChildAt(0));
            }
         }
         if(this._fountain2)
         {
            this._fountain2.stop();
            while(this._fountain2.numChildren)
            {
               ObjectUtils.disposeObject(this._fountain2.getChildAt(0));
            }
         }
         if(this._starItem)
         {
            this._starItem.dispose();
         }
         if(this._scence)
         {
            this._scence.dispose();
         }
         ObjectUtils.disposeObject(this._scence);
         ObjectUtils.disposeObject(this._txtNum);
         while(numChildren)
         {
            ObjectUtils.disposeObject(getChildAt(0));
         }
         this._scence = null;
         this._txtNum = null;
         this._fountain1 = null;
         this._fountain2 = null;
      }
   }
}
