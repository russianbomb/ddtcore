package lanternriddles.view
{
   import baglocked.BaglockedManager;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.LanguageMgr;
   import ddt.manager.LeavePageManager;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SharedManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.events.MouseEvent;
   import lanternriddles.LanternRiddlesManager;
   import lanternriddles.data.LanternAwardInfo;
   import lanternriddles.data.LanternInfo;
   import lanternriddles.event.LanternEvent;
   import road7th.comm.PackageIn;
   
   public class LanternRiddlesView extends Frame
   {
      
      private static var RANK_NUM:int = 8;
       
      
      private var _bg:Bitmap;
      
      private var _questionView:QuestionView;
      
      private var _doubleBtn:BaseButton;
      
      private var _hitBtn:BaseButton;
      
      private var _freeDouble:FilterFrameText;
      
      private var _freeHit:FilterFrameText;
      
      private var _careInfo:FilterFrameText;
      
      private var _questionNum:FilterFrameText;
      
      private var _myRank:FilterFrameText;
      
      private var _myInteger:FilterFrameText;
      
      private var _rankVec:Vector.<LanternRankItem>;
      
      private var _offY:int = 40;
      
      private var _doubleFreeCount:int;
      
      private var _hitFreeCount:int;
      
      private var _doublePrice:int;
      
      private var _hitPrice:int;
      
      private var _hitFlag:Boolean;
      
      private var _alertAsk:LanternAlertView;
      
      public function LanternRiddlesView()
      {
         super();
         this.initView();
         this.initEvent();
         this.sendPkg();
      }
      
      private function initView() : void
      {
         titleText = LanguageMgr.GetTranslation("lanternRiddles.view.Title");
         this._bg = ComponentFactory.Instance.creat("lantern.view.bg");
         addToContent(this._bg);
         this._questionView = new QuestionView();
         addToContent(this._questionView);
         this._doubleBtn = ComponentFactory.Instance.creat("lantern.view.doubleBtn");
         addToContent(this._doubleBtn);
         this._freeDouble = ComponentFactory.Instance.creatComponentByStylename("lantern.view.freeDouble");
         this._doubleBtn.addChild(this._freeDouble);
         this._hitBtn = ComponentFactory.Instance.creat("lantern.view.hitBtn");
         addToContent(this._hitBtn);
         this._freeHit = ComponentFactory.Instance.creatComponentByStylename("lantern.view.freeHit");
         this._hitBtn.addChild(this._freeHit);
         this._careInfo = ComponentFactory.Instance.creatComponentByStylename("lantern.view.careInfo");
         this._careInfo.text = LanguageMgr.GetTranslation("lanternRiddles.view.careInfoText");
         addToContent(this._careInfo);
         this._questionNum = ComponentFactory.Instance.creatComponentByStylename("lantern.view.questionNum");
         addToContent(this._questionNum);
         this._myRank = ComponentFactory.Instance.creatComponentByStylename("lantern.view.rank");
         addToContent(this._myRank);
         this._myInteger = ComponentFactory.Instance.creatComponentByStylename("lantern.view.integer");
         addToContent(this._myInteger);
         this._rankVec = new Vector.<LanternRankItem>();
         this.addRankView();
      }
      
      private function addRankView() : void
      {
         var _loc1_:LanternRankItem = null;
         var _loc2_:int = 0;
         _loc2_ = 0;
         while(_loc2_ < RANK_NUM)
         {
            _loc1_ = new LanternRankItem();
            _loc1_.buttonMode = true;
            PositionUtils.setPos(_loc1_,"lantern.view.rankPos");
            _loc1_.y = _loc1_.y + _loc2_ * this._offY;
            addToContent(_loc1_);
            this._rankVec.push(_loc1_);
            _loc2_++;
         }
      }
      
      private function initEvent() : void
      {
         addEventListener(FrameEvent.RESPONSE,this.__frameEventHandler);
         this._doubleBtn.addEventListener(MouseEvent.CLICK,this._onDoubleBtnClick);
         this._hitBtn.addEventListener(MouseEvent.CLICK,this.__onHitBtnClick);
         LanternRiddlesManager.instance.addEventListener(CrazyTankSocketEvent.LANTERNRIDDLES_QUESTION,this.__onSetQuestionInfo);
         LanternRiddlesManager.instance.addEventListener(CrazyTankSocketEvent.LANTERNRIDDLES_RANKINFO,this.__onSetRankInfo);
         LanternRiddlesManager.instance.addEventListener(CrazyTankSocketEvent.LANTERNRIDDLES_SKILL,this.__onSetBtnEnable);
      }
      
      protected function __onSetBtnEnable(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:Boolean = _loc2_.readBoolean();
         if(this._hitFlag)
         {
            this._questionView.setSelectBtnEnable(false);
            this._hitBtn.enable = !_loc3_;
         }
         else
         {
            this._doubleBtn.enable = !_loc3_;
         }
      }
      
      protected function __onSetQuestionInfo(param1:CrazyTankSocketEvent) : void
      {
         var _loc6_:Date = null;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:Boolean = false;
         var _loc11_:Boolean = false;
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readInt();
         var _loc4_:int = _loc2_.readInt();
         var _loc5_:LanternInfo = LanternRiddlesManager.instance.info[_loc4_];
         if(_loc5_)
         {
            this._questionView.count = _loc2_.readInt();
            _loc6_ = _loc2_.readDate();
            this._doubleFreeCount = _loc2_.readInt();
            this._doublePrice = _loc2_.readInt();
            this._hitFreeCount = _loc2_.readInt();
            this._hitPrice = _loc2_.readInt();
            _loc7_ = _loc2_.readInt();
            _loc8_ = _loc2_.readInt();
            _loc9_ = _loc2_.readInt();
            _loc10_ = _loc2_.readBoolean();
            _loc11_ = _loc2_.readBoolean();
            _loc5_.QuestionIndex = _loc3_;
            _loc5_.QuestionID = _loc4_;
            _loc5_.Option = _loc9_;
            _loc5_.EndDate = _loc6_;
            this._questionView.setSelectBtnEnable(true);
            this._questionView.info = _loc5_;
            this._freeDouble.text = LanguageMgr.GetTranslation("lanternRiddles.view.freeText",this._doubleFreeCount);
            this._freeHit.text = LanguageMgr.GetTranslation("lanternRiddles.view.freeText",this._hitFreeCount);
            this._myInteger.text = _loc7_.toString();
            this._questionNum.text = LanguageMgr.GetTranslation("lanternRiddles.view.questionNumText",_loc8_);
            if(this._questionView.countDownTime > 0)
            {
               this._doubleBtn.enable = !_loc11_;
               this._hitBtn.enable = !_loc10_;
               if(!this._hitBtn.enable)
               {
                  this._questionView.setSelectBtnEnable(false);
               }
            }
            else
            {
               this._questionView.setSelectBtnEnable(false);
               this._doubleBtn.enable = false;
               this._hitBtn.enable = false;
            }
            SocketManager.Instance.out.sendLanternRiddlesRankInfo();
            if(this._alertAsk)
            {
               this._alertAsk.dispose();
               this._alertAsk = null;
            }
         }
      }
      
      protected function __onSetRankInfo(param1:CrazyTankSocketEvent) : void
      {
         var _loc7_:LanternInfo = null;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:LanternAwardInfo = null;
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:Array = [];
         this._myRank.text = String(_loc2_.readInt());
         var _loc4_:int = _loc2_.readInt();
         var _loc5_:int = 0;
         while(_loc5_ < _loc4_)
         {
            _loc7_ = new LanternInfo();
            _loc7_.Rank = _loc2_.readInt();
            _loc7_.NickName = _loc2_.readUTF();
            _loc7_.TypeVIP = _loc2_.readByte();
            _loc7_.Integer = _loc2_.readInt();
            _loc8_ = _loc2_.readInt();
            _loc9_ = 0;
            while(_loc9_ < _loc8_)
            {
               _loc10_ = new LanternAwardInfo();
               _loc10_.TempId = _loc2_.readInt();
               _loc10_.AwardNum = _loc2_.readInt();
               _loc10_.IsBind = _loc2_.readBoolean();
               _loc10_.ValidDate = _loc2_.readInt();
               _loc7_.AwardInfoVec.push(_loc10_);
               _loc9_++;
            }
            _loc3_.push(_loc7_);
            _loc5_++;
         }
         _loc3_.sortOn("Rank",Array.NUMERIC);
         var _loc6_:int = 0;
         while(_loc6_ < _loc3_.length)
         {
            this._rankVec[_loc6_].info = _loc3_[_loc6_];
            _loc6_++;
         }
      }
      
      protected function _onDoubleBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.playButtonSound();
         this._hitFlag = false;
         if(this._doubleFreeCount <= 0)
         {
            if(!SharedManager.Instance.isBuyInteger)
            {
               if(PlayerManager.Instance.Self.bagLocked)
               {
                  BaglockedManager.Instance.show();
                  return;
               }
               this._alertAsk = ComponentFactory.Instance.creatComponentByStylename("lantern.view.alertView");
               this._alertAsk.text = LanguageMgr.GetTranslation("lanternRiddles.view.buyDoubleInteger.alertInfo",this._doublePrice);
               LayerManager.Instance.addToLayer(this._alertAsk,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
               this._alertAsk.addEventListener(LanternEvent.LANTERN_SELECT,this.__onLanternAlertSelect);
               this._alertAsk.addEventListener(FrameEvent.RESPONSE,this.__onBuyHandle);
            }
            else if(this.payment(SharedManager.Instance.isBuyIntegerBind))
            {
               SocketManager.Instance.out.sendLanternRiddlesUseSkill(this._questionView.info.QuestionID,this._questionView.info.QuestionIndex,1,SharedManager.Instance.isBuyIntegerBind);
            }
         }
         else if(!this._hitBtn.enable || this._questionView.info.Option != 0)
         {
            SocketManager.Instance.out.sendLanternRiddlesUseSkill(this._questionView.info.QuestionID,this._questionView.info.QuestionIndex,1);
         }
         else
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("lanternRiddles.view.doubleClick.tipsInfo"));
         }
      }
      
      protected function __onLanternAlertSelect(param1:LanternEvent) : void
      {
         this.setBindFlag(param1.flag);
      }
      
      protected function __onBuyHandle(param1:FrameEvent) : void
      {
         var _loc2_:BaseAlerFrame = param1.currentTarget as BaseAlerFrame;
         _loc2_.removeEventListener(LanternEvent.LANTERN_SELECT,this.__onLanternAlertSelect);
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.__onBuyHandle);
         switch(param1.responseCode)
         {
            case FrameEvent.SUBMIT_CLICK:
            case FrameEvent.ENTER_CLICK:
               if(this._hitFlag)
               {
                  SharedManager.Instance.isBuyHitBind = _loc2_.isBand;
               }
               else
               {
                  SharedManager.Instance.isBuyIntegerBind = _loc2_.isBand;
               }
               if(this.payment(_loc2_.isBand) && !LanternRiddlesManager.instance.checkMoney(50))
               {
                  SocketManager.Instance.out.sendLanternRiddlesUseSkill(this._questionView.info.QuestionID,this._questionView.info.QuestionIndex,!!this._hitFlag?0:1,_loc2_.isBand);
               }
               break;
            default:
               this.setBindFlag(false);
         }
         _loc2_.dispose();
         _loc2_ = null;
      }
      
      private function setBindFlag(param1:Boolean) : void
      {
         if(this._hitFlag)
         {
            SharedManager.Instance.isBuyHit = param1;
         }
         else
         {
            SharedManager.Instance.isBuyInteger = param1;
         }
      }
      
      private function payment(param1:Boolean) : Boolean
      {
         var _loc2_:BaseAlerFrame = null;
         if(param1)
         {
            if(!this.checkMoney(true))
            {
               _loc2_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("buried.alertInfo.noBindMoney"),"",LanguageMgr.GetTranslation("cancel"),true,false,false,2);
               _loc2_.addEventListener(FrameEvent.RESPONSE,this.onResponseHander);
               return false;
            }
         }
         else if(!this.checkMoney(false))
         {
            _loc2_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("tank.room.RoomIIView2.notenoughmoney.title"),LanguageMgr.GetTranslation("tank.room.RoomIIView2.notenoughmoney.content"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),false,true,true,LayerManager.ALPHA_BLOCKGOUND);
            _loc2_.addEventListener(FrameEvent.RESPONSE,this._response);
            return false;
         }
         return true;
      }
      
      protected function __onHitBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.playButtonSound();
         this._hitFlag = true;
         if(this._hitFreeCount <= 0)
         {
            if(!SharedManager.Instance.isBuyHit)
            {
               if(PlayerManager.Instance.Self.bagLocked)
               {
                  BaglockedManager.Instance.show();
                  return;
               }
               this._alertAsk = ComponentFactory.Instance.creatComponentByStylename("lantern.view.alertView");
               this._alertAsk.text = LanguageMgr.GetTranslation("lanternRiddles.view.buyHit.alertInfo",this._hitPrice);
               LayerManager.Instance.addToLayer(this._alertAsk,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
               this._alertAsk.addEventListener(LanternEvent.LANTERN_SELECT,this.__onLanternAlertSelect);
               this._alertAsk.addEventListener(FrameEvent.RESPONSE,this.__onBuyHandle);
            }
            else if(this.payment(SharedManager.Instance.isBuyHitBind))
            {
               SocketManager.Instance.out.sendLanternRiddlesUseSkill(this._questionView.info.QuestionID,this._questionView.info.QuestionIndex,0,SharedManager.Instance.isBuyHitBind);
            }
         }
         else
         {
            SocketManager.Instance.out.sendLanternRiddlesUseSkill(this._questionView.info.QuestionID,this._questionView.info.QuestionIndex,0);
         }
      }
      
      private function sendPkg() : void
      {
         SocketManager.Instance.out.sendLanternRiddlesQuestion();
      }
      
      public function show() : void
      {
         LayerManager.Instance.addToLayer(this,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.ALPHA_BLOCKGOUND);
      }
      
      private function removeEvent() : void
      {
         removeEventListener(FrameEvent.RESPONSE,this.__frameEventHandler);
         this._doubleBtn.removeEventListener(MouseEvent.CLICK,this._onDoubleBtnClick);
         this._hitBtn.removeEventListener(MouseEvent.CLICK,this.__onHitBtnClick);
         LanternRiddlesManager.instance.removeEventListener(CrazyTankSocketEvent.LANTERNRIDDLES_QUESTION,this.__onSetQuestionInfo);
         LanternRiddlesManager.instance.removeEventListener(CrazyTankSocketEvent.LANTERNRIDDLES_RANKINFO,this.__onSetRankInfo);
         LanternRiddlesManager.instance.removeEventListener(CrazyTankSocketEvent.LANTERNRIDDLES_SKILL,this.__onSetBtnEnable);
      }
      
      private function __frameEventHandler(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         switch(param1.responseCode)
         {
            case FrameEvent.ESC_CLICK:
            case FrameEvent.CLOSE_CLICK:
               LanternRiddlesManager.instance.hide();
         }
      }
      
      private function onResponseHander(param1:FrameEvent) : void
      {
         var _loc2_:BaseAlerFrame = null;
         (param1.currentTarget as BaseAlerFrame).removeEventListener(FrameEvent.RESPONSE,this.onResponseHander);
         if(param1.responseCode == FrameEvent.ENTER_CLICK || param1.responseCode == FrameEvent.SUBMIT_CLICK)
         {
            if(!this.checkMoney(false))
            {
               _loc2_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("tank.room.RoomIIView2.notenoughmoney.title"),LanguageMgr.GetTranslation("tank.room.RoomIIView2.notenoughmoney.content"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),false,true,true,LayerManager.ALPHA_BLOCKGOUND);
               _loc2_.addEventListener(FrameEvent.RESPONSE,this._response);
               return;
            }
            SocketManager.Instance.out.sendLanternRiddlesUseSkill(this._questionView.info.QuestionID,this._questionView.info.QuestionIndex,!!this._hitFlag?0:1,false);
         }
         param1.currentTarget.dispose();
      }
      
      private function _response(param1:FrameEvent) : void
      {
         (param1.currentTarget as BaseAlerFrame).removeEventListener(FrameEvent.RESPONSE,this._response);
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            LeavePageManager.leaveToFillPath();
         }
         ObjectUtils.disposeObject(param1.currentTarget);
      }
      
      private function checkMoney(param1:Boolean) : Boolean
      {
         var _loc2_:int = !!this._hitFlag?int(this._hitPrice):int(this._doublePrice);
         if(param1)
         {
            if(PlayerManager.Instance.Self.BandMoney < _loc2_)
            {
               return false;
            }
         }
         else if(PlayerManager.Instance.Self.Money < _loc2_)
         {
            return false;
         }
         return true;
      }
      
      override public function dispose() : void
      {
         var _loc1_:int = 0;
         super.dispose();
         this.removeEvent();
         if(this._bg)
         {
            this._bg.bitmapData.dispose();
            this._bg = null;
         }
         if(this._questionView)
         {
            this._questionView.dispose();
            this._questionView = null;
         }
         if(this._doubleBtn)
         {
            this._doubleBtn.dispose();
            this._doubleBtn = null;
         }
         if(this._hitBtn)
         {
            this._hitBtn.dispose();
            this._hitBtn = null;
         }
         if(this._freeDouble)
         {
            this._freeDouble.dispose();
            this._freeDouble = null;
         }
         if(this._freeHit)
         {
            this._freeHit.dispose();
            this._freeHit = null;
         }
         if(this._careInfo)
         {
            this._careInfo.dispose();
            this._careInfo = null;
         }
         if(this._myRank)
         {
            this._myRank.dispose();
            this._myRank = null;
         }
         if(this._myInteger)
         {
            this._myInteger.dispose();
            this._myInteger = null;
         }
         if(this._rankVec)
         {
            _loc1_ = 0;
            while(_loc1_ < this._rankVec.length)
            {
               this._rankVec[_loc1_].dispose();
               this._rankVec[_loc1_] = null;
               _loc1_++;
            }
            this._rankVec.length = 0;
            this._rankVec = null;
         }
         if(this.parent)
         {
            this.parent.removeChild(this);
         }
      }
   }
}
