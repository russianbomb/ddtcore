package wonderfulActivity
{
   import activeEvents.data.ActiveEventsInfo;
   import calendar.CalendarManager;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.BaseLoader;
   import com.pickgliss.loader.LoadResourceManager;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.utils.ObjectUtils;
   import consumeRank.ConsumeRankManager;
   import ddt.data.UIModuleTypes;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.loader.LoaderCreate;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.manager.StateManager;
   import ddt.manager.TimeManager;
   import ddt.states.StateType;
   import ddt.utils.FilterWordManager;
   import ddt.view.UIModuleSmallLoading;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.events.MouseEvent;
   import flash.events.TimerEvent;
   import flash.utils.Dictionary;
   import flash.utils.Timer;
   import road7th.comm.PackageIn;
   import shop.view.ShopPresentClearingFrame;
   import wonderfulActivity.data.ActivityTypeData;
   import wonderfulActivity.data.CanGetData;
   import wonderfulActivity.data.GiftBagInfo;
   import wonderfulActivity.data.GiftCurInfo;
   import wonderfulActivity.data.GmActivityInfo;
   import wonderfulActivity.data.LeftViewInfoVo;
   import wonderfulActivity.data.PlayerCurInfo;
   import wonderfulActivity.data.WonderfulActivityTypeData;
   import wonderfulActivity.event.WonderfulActivityEvent;
   import wonderfulActivity.limitActivity.LimitActivityFrame;
   import wonderfulActivity.limitActivity.SendGiftActivityFrame;
   
   public class WonderfulActivityManager extends EventDispatcher
   {
      
      private static var _instance:WonderfulActivityManager;
      
      public static var isFirstClick:Boolean = true;
       
      
      public var activityData:Dictionary;
      
      public var activityInitData:Dictionary;
      
      public var leftViewInfoDic:Dictionary;
      
      public var currentView;
      
      private var _frame:LimitActivityFrame;
      
      private var _sendGiftFrame:SendGiftActivityFrame;
      
      private var _info:GmActivityInfo;
      
      private var _actList:Array;
      
      public var activityTypeList:Vector.<ActivityTypeData>;
      
      public var activityFighterList:Vector.<ActivityTypeData>;
      
      public var activityExpList:Vector.<ActivityTypeData>;
      
      public var activityRechargeList:Vector.<ActivityTypeData>;
      
      public var chickenEndTime:Date;
      
      public var rouleEndTime:Date;
      
      private var _timerHanderFun:Dictionary;
      
      private var _timer:Timer;
      
      public var xiaoFeiScore:int;
      
      public var chongZhiScore:int;
      
      public var _stateList:Vector.<CanGetData>;
      
      public var deleWAIcon:Function;
      
      public var addWAIcon:Function;
      
      public var iconFlash:Function;
      
      public var isRuning:Boolean = true;
      
      public var currView:String;
      
      public var frameCanClose:Boolean = true;
      
      public var clickWonderfulActView:Boolean;
      
      public var stateDic:Dictionary;
      
      public var isSkipFromHall:Boolean;
      
      public var skipType:String;
      
      public var leftUnitViewType:int;
      
      private var lastActList:Array;
      
      private var sendGiftIsOut:Boolean = false;
      
      private var _giveFriendOpenFrame:ShopPresentClearingFrame;
      
      private var _battleCompanionInfo:InventoryItemInfo;
      
      private var _eventsActiveDic:Dictionary;
      
      public var selectId:String = "";
      
      public function WonderfulActivityManager()
      {
         this.lastActList = [];
         super();
         this._actList = [];
         this._timerHanderFun = new Dictionary();
         this._stateList = new Vector.<CanGetData>();
         this.activityInitData = new Dictionary();
         this.leftViewInfoDic = new Dictionary();
         this.activityFighterList = new Vector.<ActivityTypeData>();
         this.activityExpList = new Vector.<ActivityTypeData>();
         this.activityRechargeList = new Vector.<ActivityTypeData>();
         this.stateDic = new Dictionary();
      }
      
      public static function get Instance() : WonderfulActivityManager
      {
         if(!_instance)
         {
            _instance = new WonderfulActivityManager();
         }
         return _instance;
      }
      
      public function setup() : void
      {
         this.initAdvIdName();
         this.addEvents();
         ConsumeRankManager.instance.setup();
      }
      
      private function initAdvIdName() : void
      {
         this.leftViewInfoDic[ActivityType.CHONGZHIHUIKUI] = new LeftViewInfoVo(ActivityType.CHONGZHIHUIKUI,LanguageMgr.GetTranslation("wonderfulActivityManager.btnTxt1"));
         this.leftViewInfoDic[ActivityType.XIAOFEIHUIKUI] = new LeftViewInfoVo(ActivityType.XIAOFEIHUIKUI,LanguageMgr.GetTranslation("wonderfulActivityManager.btnTxt2"));
         this.leftViewInfoDic[ActivityType.ZHANYOUCHONGZHIHUIKUI] = new LeftViewInfoVo(ActivityType.ZHANYOUCHONGZHIHUIKUI,LanguageMgr.GetTranslation("wonderfulActivityManager.btnTxt6"));
      }
      
      private function addEvents() : void
      {
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.WONDERFUL_ACTIVITY,this.rechargeReturnHander);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.WONDERFUL_ACTIVITY_INIT,this.activityInitHandler);
      }
      
      private function activityInitHandler(param1:CrazyTankSocketEvent) : void
      {
         var _loc3_:int = 0;
         var _loc2_:PackageIn = param1.pkg;
         _loc3_ = _loc2_.readInt();
         if(_loc3_ == 0)
         {
            this.updateNewActivityXml();
         }
         else if(_loc3_ == 1)
         {
            this.activityInit(_loc2_);
            this.checkActivity();
            WonderfulActivityManager.Instance.initFrame(this.isSkipFromHall,this.skipType);
            SocketManager.Instance.out.updateConsumeRank();
            SocketManager.Instance.out.sendWonderfulActivity(0,-1);
            CalendarManager.getInstance().open(-1);
         }
         else if(_loc3_ == 2)
         {
            this.activityInit(_loc2_);
            this.checkActivity();
         }
         else if(_loc3_ == 3)
         {
            this.activityInit(_loc2_);
            this.checkActivity(_loc3_);
         }
      }
      
      private function updateNewActivityXml() : void
      {
         var _loc1_:BaseLoader = LoaderCreate.Instance.loadWonderfulActivityXml();
         LoadResourceManager.Instance.startLoad(_loc1_);
      }
      
      private function activityInit(param1:PackageIn) : void
      {
         var _loc4_:String = null;
         var _loc5_:int = 0;
         var _loc6_:Array = null;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:Dictionary = null;
         var _loc10_:int = 0;
         var _loc11_:PlayerCurInfo = null;
         var _loc12_:GiftCurInfo = null;
         var _loc13_:String = null;
         var _loc2_:int = param1.readInt();
         var _loc3_:int = 0;
         while(_loc3_ <= _loc2_ - 1)
         {
            _loc4_ = param1.readUTF();
            _loc5_ = param1.readInt();
            _loc6_ = [];
            _loc7_ = 0;
            while(_loc7_ <= _loc5_ - 1)
            {
               _loc11_ = new PlayerCurInfo();
               _loc11_.statusID = param1.readInt();
               _loc11_.statusValue = param1.readInt();
               _loc6_.push(_loc11_);
               _loc7_++;
            }
            _loc8_ = param1.readInt();
            _loc9_ = new Dictionary();
            _loc10_ = 0;
            while(_loc10_ <= _loc8_ - 1)
            {
               _loc12_ = new GiftCurInfo();
               _loc13_ = param1.readUTF();
               _loc12_.times = param1.readInt();
               _loc9_[_loc13_] = _loc12_;
               _loc10_++;
            }
            this.activityInitData[_loc4_] = {
               "statusArr":_loc6_,
               "giftInfoDic":_loc9_
            };
            _loc3_++;
         }
      }
      
      private function rechargeReturnHander(param1:CrazyTankSocketEvent) : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:Date = null;
         var _loc6_:Date = null;
         var _loc7_:CanGetData = null;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:Date = null;
         var _loc11_:int = 0;
         var _loc12_:int = 0;
         var _loc2_:int = param1.pkg.readByte();
         if(_loc2_ == 2)
         {
            if(StateManager.currentStateType == StateType.MAIN || this._frame)
            {
               this.updateFirstRechargeXml();
            }
         }
         else if(_loc2_ == 0)
         {
            _loc4_ = param1.pkg.readInt();
            _loc8_ = 0;
            while(_loc8_ < _loc4_)
            {
               _loc9_ = -1;
               _loc7_ = new CanGetData();
               _loc7_.id = param1.pkg.readInt();
               switch(_loc7_.id)
               {
                  case ActivityType.ZHANYOUCHONGZHIHUIKUI:
                     _loc9_ = ActivityType.ZHANYOUCHONGZHIHUIKUI;
                     _loc11_ = param1.pkg.readInt();
                     break;
                  case ActivityType.CHONGZHIHUIKUI:
                     _loc9_ = ActivityType.CHONGZHIHUIKUI;
                     this.chongZhiScore = param1.pkg.readInt();
                     break;
                  case ActivityType.XIAOFEIHUIKUI:
                     _loc9_ = ActivityType.XIAOFEIHUIKUI;
                     this.xiaoFeiScore = param1.pkg.readInt();
                     break;
                  default:
                     _loc12_ = param1.pkg.readInt();
               }
               _loc7_.num = param1.pkg.readInt();
               _loc5_ = param1.pkg.readDate();
               _loc6_ = param1.pkg.readDate();
               this.setActivityTime(_loc7_.id,_loc5_,_loc6_);
               this.updateStateList(_loc7_);
               if(_loc9_ != -1)
               {
                  _loc10_ = TimeManager.Instance.Now();
                  if(_loc10_.getTime() > _loc5_.getTime() && _loc10_.getTime() < _loc6_.getTime() && _loc7_.num != -2)
                  {
                     this.addElement(_loc9_);
                  }
                  else
                  {
                     this.removeElement(_loc9_);
                  }
               }
               _loc8_++;
            }
            if(_loc4_ == 1 && this._frame && this._frame.parent)
            {
               if(_loc7_)
               {
                  this._frame.setState(_loc7_.num,_loc7_.id);
               }
            }
         }
      }
      
      private function updateFirstRechargeXml() : void
      {
         var _loc1_:BaseLoader = LoaderCreate.Instance.firstRechargeLoader();
         LoadResourceManager.Instance.startLoad(_loc1_);
      }
      
      public function updateChargeActiveTemplateXml() : void
      {
         var _loc1_:BaseLoader = LoaderCreate.Instance.creatWondActiveLoader();
         LoadResourceManager.Instance.startLoad(_loc1_);
      }
      
      private function dispatchCheckEvent() : void
      {
         if(!this._frame && !this.clickWonderfulActView)
         {
            dispatchEvent(new WonderfulActivityEvent(WonderfulActivityEvent.CHECK_ACTIVITY_STATE));
         }
      }
      
      private function updateStateList(param1:CanGetData) : void
      {
         var _loc2_:int = this._stateList.length;
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_)
         {
            if(param1.id == this._stateList[_loc3_].id)
            {
               this._stateList[_loc3_] = param1;
               return;
            }
            _loc3_++;
         }
         this._stateList.push(param1);
      }
      
      private function timerHander(param1:TimerEvent) : void
      {
         var _loc2_:Function = null;
         for each(_loc2_ in this._timerHanderFun)
         {
            _loc2_();
         }
      }
      
      public function addTimerFun(param1:String, param2:Function) : void
      {
         this._timerHanderFun[param1] = param2;
         if(!this._timer)
         {
            this._timer = new Timer(1000);
            this._timer.start();
            this._timer.addEventListener(TimerEvent.TIMER,this.timerHander);
         }
      }
      
      public function delTimerFun(param1:String) : void
      {
         if(this._timerHanderFun[param1])
         {
            delete this._timerHanderFun[param1];
         }
         if(this.isEmptyDictionary(this._timerHanderFun))
         {
            if(this._timer)
            {
               this._timer.stop();
               this._timer.removeEventListener(TimerEvent.TIMER,this.timerHander);
               this._timer = null;
            }
         }
      }
      
      private function isEmptyDictionary(param1:Dictionary) : Boolean
      {
         var _loc2_:* = null;
         for(_loc2_ in param1)
         {
            if(_loc2_)
            {
               return false;
            }
         }
         return true;
      }
      
      public function getTimeDiff(param1:Date, param2:Date) : String
      {
         var _loc4_:uint = 0;
         var _loc5_:uint = 0;
         var _loc6_:uint = 0;
         var _loc7_:uint = 0;
         var _loc3_:Number = Math.round((param1.getTime() - param2.getTime()) / 1000);
         if(_loc3_ >= 0)
         {
            _loc4_ = Math.floor(_loc3_ / 60 / 60 / 24);
            _loc3_ = _loc3_ % (60 * 60 * 24);
            _loc5_ = Math.floor(_loc3_ / 60 / 60);
            _loc3_ = _loc3_ % (60 * 60);
            _loc6_ = Math.floor(_loc3_ / 60);
            _loc7_ = _loc3_ % 60;
            if(_loc4_ > 0)
            {
               return _loc4_ + LanguageMgr.GetTranslation("wonderfulActivityManager.d");
            }
            if(_loc5_ > 0)
            {
               return this.fixZero(_loc5_) + LanguageMgr.GetTranslation("wonderfulActivityManager.h");
            }
            if(_loc6_ > 0)
            {
               return this.fixZero(_loc6_) + LanguageMgr.GetTranslation("wonderfulActivityManager.m");
            }
            if(_loc7_ > 0)
            {
               return this.fixZero(_loc7_) + LanguageMgr.GetTranslation("wonderfulActivityManager.s");
            }
         }
         return "0";
      }
      
      private function fixZero(param1:uint) : String
      {
         return param1 < 10?String(param1):String(param1);
      }
      
      private function setActivityTime(param1:int, param2:Date, param3:Date) : void
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         if(param1 == ActivityType.ZHANYOUCHONGZHIHUIKUI)
         {
            if(this.activityFighterList.length == 0)
            {
               return;
            }
            this.activityFighterList[0].StartTime = param2;
            this.activityFighterList[0].EndTime = param3;
         }
         else if(param1 >= ActivityType.CHONGZHIHUIKUI && param1 < ActivityType.XIAOFEIHUIKUI)
         {
            if(this.activityRechargeList.length == 0)
            {
               return;
            }
            _loc4_ = 0;
            while(_loc4_ < this.activityRechargeList.length)
            {
               if(param1 == this.activityRechargeList[_loc4_].ID)
               {
                  this.activityRechargeList[_loc4_].StartTime = param2;
                  this.activityRechargeList[_loc4_].EndTime = param3;
                  break;
               }
               _loc4_++;
            }
         }
         else if(param1 >= ActivityType.XIAOFEIHUIKUI)
         {
            if(this.activityExpList.length == 0)
            {
               return;
            }
            _loc5_ = 0;
            while(_loc5_ < this.activityExpList.length)
            {
               if(param1 == this.activityExpList[_loc4_].ID)
               {
                  this.activityExpList[_loc4_].StartTime = param2;
                  this.activityExpList[_loc4_].EndTime = param3;
                  break;
               }
               _loc5_++;
            }
         }
      }
      
      public function wonderfulGMActiveInfo(param1:WonderfulGMActAnalyer) : void
      {
         this.activityData = param1.ActivityData;
         SocketManager.Instance.out.requestWonderfulActInit(2);
         SocketManager.Instance.out.updateConsumeRank();
      }
      
      public function wonderfulActiveType(param1:WonderfulActAnalyer) : void
      {
         this.activityFighterList = new Vector.<ActivityTypeData>();
         this.activityExpList = new Vector.<ActivityTypeData>();
         this.activityRechargeList = new Vector.<ActivityTypeData>();
         this.activityTypeList = param1.itemList;
         var _loc2_:int = this.activityTypeList.length;
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_)
         {
            this.activityTypeList[_loc3_].StartTime = new Date();
            this.activityTypeList[_loc3_].EndTime = new Date();
            if(this.activityTypeList[_loc3_].ID == ActivityType.ZHANYOUCHONGZHIHUIKUI)
            {
               this.activityFighterList.push(this.activityTypeList[_loc3_]);
            }
            else if(this.activityTypeList[_loc3_].ID >= ActivityType.CHONGZHIHUIKUI && this.activityTypeList[_loc3_].ID < ActivityType.XIAOFEIHUIKUI)
            {
               this.activityRechargeList.push(this.activityTypeList[_loc3_]);
            }
            else
            {
               this.activityExpList.push(this.activityTypeList[_loc3_]);
            }
            _loc3_++;
         }
         SocketManager.Instance.out.sendWonderfulActivity(0,-1);
      }
      
      public function initFrame(param1:Boolean = false, param2:String = "0") : void
      {
         this.isSkipFromHall = param1;
         this.skipType = param2;
         this.leftUnitViewType = Boolean(this.leftViewInfoDic[this.skipType])?int(this.leftViewInfoDic[this.skipType].unitIndex):int(2);
         if(!this._frame)
         {
            UIModuleSmallLoading.Instance.progress = 0;
            UIModuleSmallLoading.Instance.show();
            UIModuleSmallLoading.Instance.addEventListener(Event.CLOSE,this.onSmallLoadingClose);
            UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.createActivityFrame);
            UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUIProgress);
            UIModuleLoader.Instance.addUIModlue(UIModuleTypes.DDT_CALENDAR);
            UIModuleLoader.Instance.addUIModlue(UIModuleTypes.WONDERFULACTIVI);
         }
         else
         {
            this._frame = ComponentFactory.Instance.creatComponentByStylename("com.wonderfulActivity.LimitActivityFrame");
            LayerManager.Instance.addToLayer(this._frame,LayerManager.GAME_TOP_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
            this._frame.addElement(this.actList);
         }
      }
      
      public function addElement(param1:*, param2:Boolean = false) : void
      {
         param1 = String(param1);
         if(this._actList.indexOf(param1) == -1 && param2)
         {
            this._actList.push(param1);
         }
         if(this._actList.indexOf(param1) == -1)
         {
            this._actList.unshift(param1);
         }
         if(this._actList.length > 0)
         {
            if(this.addWAIcon != null)
            {
               this.addWAIcon();
            }
         }
         if(this._frame && this._frame.parent)
         {
            this._frame.addElement(this.actList);
         }
      }
      
      public function removeElement(param1:*) : void
      {
         param1 = String(param1);
         var _loc2_:int = this._actList.indexOf(param1);
         if(_loc2_ == -1)
         {
            return;
         }
         this._actList.splice(_loc2_,1);
         if(this._actList.length == 0)
         {
            this.dispose();
            return;
         }
         if(this._actList.length > 0)
         {
            if(this.currView == param1)
            {
               this.currView = this._actList[0];
            }
         }
         if(this._frame && this._frame.parent)
         {
            this._frame.addElement(this.actList);
         }
      }
      
      public function dispose() : void
      {
         if(!this.isRuning)
         {
            return;
         }
         this.clickWonderfulActView = false;
         ObjectUtils.disposeObject(this._frame);
         this._frame = null;
         this.currentView = null;
         this.isSkipFromHall = false;
         this.skipType = "0";
         if(this._timer)
         {
            this._timer.stop();
            this._timer.removeEventListener(TimerEvent.TIMER,this.timerHander);
            this._timer = null;
         }
         if(this._actList.length == 0)
         {
            if(this.deleWAIcon != null)
            {
               this.deleWAIcon();
            }
         }
      }
      
      protected function onUIProgress(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.WONDERFULACTIVI)
         {
            UIModuleSmallLoading.Instance.progress = param1.loader.progress * 100;
         }
      }
      
      protected function createActivityFrame(param1:UIModuleEvent) : void
      {
         if(param1.module != UIModuleTypes.WONDERFULACTIVI)
         {
            return;
         }
         UIModuleSmallLoading.Instance.hide();
         UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this.onSmallLoadingClose);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.createActivityFrame);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUIProgress);
         this._frame = ComponentFactory.Instance.creatComponentByStylename("com.wonderfulActivity.LimitActivityFrame");
         LayerManager.Instance.addToLayer(this._frame,LayerManager.GAME_TOP_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
         this._frame.addElement(this.actList);
      }
      
      private function checkActivity(param1:int = 0) : void
      {
         var _loc4_:GmActivityInfo = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:String = null;
         var _loc8_:Boolean = false;
         var _loc9_:LeftViewInfoVo = null;
         if(this.activityData == null)
         {
            return;
         }
         var _loc2_:Array = [];
         var _loc3_:Date = TimeManager.Instance.Now();
         for each(_loc4_ in this.activityData)
         {
            if(_loc3_.time < Date.parse(_loc4_.beginShowTime) || _loc3_.time > Date.parse(_loc4_.endShowTime))
            {
               continue;
            }
            switch(_loc4_.activityType)
            {
               case WonderfulActivityTypeData.MAIN_PAY_ACTIVITY:
                  switch(_loc4_.activityChildType)
                  {
                     case WonderfulActivityTypeData.FIRST_CONTACT:
                        break;
                     case WonderfulActivityTypeData.ACC_FIRST_PAY:
                        this.leftViewInfoDic[_loc4_.activityId] = new LeftViewInfoVo(ActivityType.ACT_ANNOUNCEMENT,"· " + _loc4_.activityName,_loc4_.icon);
                        this.addElement(_loc4_.activityId);
                        _loc2_.push(_loc4_.activityId);
                        break;
                     case WonderfulActivityTypeData.ONE_OFF_PAY:
                        this.leftViewInfoDic[_loc4_.activityId] = new LeftViewInfoVo(ActivityType.ACT_ANNOUNCEMENT,"· " + _loc4_.activityName,_loc4_.icon);
                        this.addElement(_loc4_.activityId);
                        _loc2_.push(_loc4_.activityId);
                        break;
                     case WonderfulActivityTypeData.ACCUMULATIVE_PAY:
                        this.leftViewInfoDic[_loc4_.activityId] = new LeftViewInfoVo(ActivityType.ACCUMULATIVE_PAY,"· " + _loc4_.activityName,_loc4_.icon);
                        this.addElement(_loc4_.activityId);
                        _loc2_.push(_loc4_.activityId);
                        break;
                     case WonderfulActivityTypeData.NEWGAMEBENIFIT:
                        this.leftViewInfoDic[_loc4_.activityId] = new LeftViewInfoVo(ActivityType.NEWGAMEBENIFIT,"· " + _loc4_.activityName,_loc4_.icon);
                        this.addElement(_loc4_.activityId);
                        _loc2_.push(_loc4_.activityId);
                        break;
                     case WonderfulActivityTypeData.PAY_RETURN:
                        this.leftViewInfoDic[_loc4_.activityId] = new LeftViewInfoVo(ActivityType.PAY_RETURN,"· " + _loc4_.activityName,_loc4_.icon);
                        this.addElement(_loc4_.activityId);
                        _loc2_.push(_loc4_.activityId);
                        break;
                     case WonderfulActivityTypeData.ONE_OFF_IN_TIME:
                        this.leftViewInfoDic[_loc4_.activityId] = new LeftViewInfoVo(ActivityType.ACT_ANNOUNCEMENT,"· " + _loc4_.activityName,_loc4_.icon);
                        this.addElement(_loc4_.activityId);
                        _loc2_.push(_loc4_.activityId);
                  }
                  continue;
               case WonderfulActivityTypeData.MAIN_CONSUME_ACTIVITY:
                  switch(_loc4_.activityChildType)
                  {
                     case WonderfulActivityTypeData.ONE_OFF_CONSUME:
                        this.leftViewInfoDic[_loc4_.activityId] = new LeftViewInfoVo(ActivityType.ACT_ANNOUNCEMENT,"· " + _loc4_.activityName,_loc4_.icon);
                        this.addElement(_loc4_.activityId);
                        _loc2_.push(_loc4_.activityId);
                        break;
                     case WonderfulActivityTypeData.CONSUME_RETURN:
                        this.leftViewInfoDic[_loc4_.activityId] = new LeftViewInfoVo(ActivityType.CONSUME_RETURN,"· " + _loc4_.activityName,_loc4_.icon);
                        this.addElement(_loc4_.activityId);
                        _loc2_.push(_loc4_.activityId);
                        break;
                     case WonderfulActivityTypeData.SPECIFIC_COUNT_CONSUME:
                        this.leftViewInfoDic[_loc4_.activityId] = new LeftViewInfoVo(ActivityType.ACT_ANNOUNCEMENT,"· " + _loc4_.activityName,_loc4_.icon);
                        this.addElement(_loc4_.activityId);
                        _loc2_.push(_loc4_.activityId);
                  }
                  continue;
               case WonderfulActivityTypeData.MAIN_FAMOUS_ACTIVITY:
                  if(_loc4_.activityChildType == WonderfulActivityTypeData.HERO_POWER || _loc4_.activityChildType == WonderfulActivityTypeData.HERO_GRADE)
                  {
                     for each(_loc9_ in this.leftViewInfoDic)
                     {
                        if(_loc9_.viewType == ActivityType.HERO)
                        {
                           _loc8_ = true;
                           break;
                        }
                     }
                     if(!_loc8_)
                     {
                        this.leftViewInfoDic[_loc4_.activityId] = new LeftViewInfoVo(ActivityType.HERO,"· " + _loc4_.activityName,_loc4_.icon);
                        this.addElement(_loc4_.activityId);
                     }
                     if(_loc2_.indexOf(_loc4_.activityId) == -1)
                     {
                        _loc2_.push(_loc4_.activityId);
                     }
                  }
                  continue;
               case WonderfulActivityTypeData.CONSORTION_ACTIVITY:
                  if(_loc4_.activityChildType == WonderfulActivityTypeData.TUANJIE_POWER)
                  {
                     this.leftViewInfoDic[_loc4_.activityId] = new LeftViewInfoVo(ActivityType.TUANJIE_POWER,"· " + _loc4_.activityName,_loc4_.icon);
                     this.addElement(_loc4_.activityId);
                     _loc2_.push(_loc4_.activityId);
                  }
                  continue;
               case WonderfulActivityTypeData.STRENGTHEN_ACTIVITY:
                  if(_loc4_.activityChildType == WonderfulActivityTypeData.STRENGTHEN_DAREN)
                  {
                     this.leftViewInfoDic[_loc4_.activityId] = new LeftViewInfoVo(ActivityType.STRENGTHEN_DAREN,"· " + _loc4_.activityName,_loc4_.icon);
                     this.addElement(_loc4_.activityId);
                     _loc2_.push(_loc4_.activityId);
                  }
                  continue;
               case WonderfulActivityTypeData.EXCHANGE_ACTIVITY:
                  if(_loc4_.activityChildType == WonderfulActivityTypeData.NORMAL_EXCHANGE)
                  {
                     this.leftViewInfoDic[_loc4_.activityId] = new LeftViewInfoVo(ActivityType.NORMAL_EXCHANGE,"· " + _loc4_.activityName,_loc4_.icon);
                     this.addElement(_loc4_.activityId);
                     _loc2_.push(_loc4_.activityId);
                  }
                  continue;
               case WonderfulActivityTypeData.MARRY_ACTIVITY:
                  if(_loc4_.activityChildType == WonderfulActivityTypeData.HOLD_WEDDING)
                  {
                     this.leftViewInfoDic[_loc4_.activityId] = new LeftViewInfoVo(ActivityType.ACT_ANNOUNCEMENT,"· " + _loc4_.activityName,_loc4_.icon);
                     this.addElement(_loc4_.activityId);
                     _loc2_.push(_loc4_.activityId);
                  }
                  continue;
               case WonderfulActivityTypeData.RECEIVE_ACTIVITY:
                  if(_loc4_.activityChildType == WonderfulActivityTypeData.USER_ID_RECEIVE || _loc4_.activityChildType == WonderfulActivityTypeData.DAILY_RECEIVE)
                  {
                     this.leftViewInfoDic[_loc4_.activityId] = new LeftViewInfoVo(ActivityType.RECEIVE_ACTIVITY,"· " + _loc4_.activityName,_loc4_.icon);
                     this.addElement(_loc4_.activityId);
                     _loc2_.push(_loc4_.activityId);
                  }
                  else if(_loc4_.activityChildType == WonderfulActivityTypeData.SEND_GIFT)
                  {
                     this._info = _loc4_;
                     if(this._sendGiftFrame && this.activityInitData[this._info.activityId] && this.activityInitData[this._info.activityId].giftInfoDic[this._info.giftbagArray[0].giftbagId].times != 0 && this._sendGiftFrame.nowId == this._info.activityId)
                     {
                        this._sendGiftFrame.setBtnFalse();
                     }
                     if(param1 != 3)
                     {
                        continue;
                     }
                     if(PlayerManager.Instance.Self.Grade > 2 && !this.sendGiftIsOut && this.activityInitData[this._info.activityId] && this.activityInitData[this._info.activityId].giftInfoDic[this._info.giftbagArray[0].giftbagId].times == 0 && !this._sendGiftFrame)
                     {
                        this._sendGiftFrame = ComponentFactory.Instance.creatComponentByStylename("com.wonderfulActivity.sendGiftFrame");
                        LayerManager.Instance.addToLayer(this._sendGiftFrame,LayerManager.GAME_TOP_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
                        this._sendGiftFrame.setData(this._info);
                        this.sendGiftIsOut = true;
                     }
                  }
                  continue;
               default:
                  continue;
            }
         }
         _loc5_ = -1;
         _loc6_ = 0;
         while(_loc6_ <= _loc2_.length - 1)
         {
            _loc5_ = this.lastActList.indexOf(_loc2_[_loc6_]);
            if(_loc5_ >= 0)
            {
               this.lastActList.splice(_loc5_,1);
            }
            _loc6_++;
         }
         for each(_loc7_ in this.lastActList)
         {
            this.removeElement(_loc7_);
         }
         this.lastActList = _loc2_;
         this.checkActState();
      }
      
      public function getActIdWithViewId(param1:int) : String
      {
         var _loc2_:* = null;
         var _loc3_:LeftViewInfoVo = null;
         for(_loc2_ in this.leftViewInfoDic)
         {
            _loc3_ = this.leftViewInfoDic[_loc2_];
            if(_loc3_.viewType == param1)
            {
               return _loc2_;
            }
         }
         return "";
      }
      
      private function checkActState() : void
      {
         var _loc1_:* = null;
         var _loc2_:LeftViewInfoVo = null;
         var _loc3_:Dictionary = null;
         var _loc4_:Array = null;
         var _loc5_:GiftBagInfo = null;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         for(_loc1_ in this.leftViewInfoDic)
         {
            this.stateDic[this.leftViewInfoDic[_loc1_].viewType] = false;
            if(!this.activityData[_loc1_] || !this.activityInitData[_loc1_])
            {
               continue;
            }
            _loc2_ = this.leftViewInfoDic[_loc1_];
            _loc3_ = this.activityInitData[_loc1_].giftInfoDic;
            _loc4_ = this.activityInitData[_loc1_].statusArr;
            switch(_loc2_.viewType)
            {
               case ActivityType.PAY_RETURN:
               case ActivityType.CONSUME_RETURN:
                  for each(_loc5_ in this.activityData[_loc1_].giftbagArray)
                  {
                     _loc6_ = _loc3_[_loc5_.giftbagId].times;
                     if(_loc5_.giftConditionArr[2].conditionValue == 0)
                     {
                        _loc7_ = int(Math.floor(_loc4_[0].statusValue / _loc5_.giftConditionArr[0].conditionValue)) - _loc6_;
                        if(_loc7_ > 0)
                        {
                           this.stateDic[_loc2_.viewType] = true;
                           break;
                        }
                     }
                     else if(_loc6_ == 0 && Math.floor(_loc4_[0].statusValue / _loc5_.giftConditionArr[0].conditionValue) > 0)
                     {
                        this.stateDic[_loc2_.viewType] = true;
                        break;
                     }
                  }
                  continue;
               default:
                  continue;
            }
         }
         this.dispatchCheckEvent();
      }
      
      protected function onSmallLoadingClose(param1:Event) : void
      {
         UIModuleSmallLoading.Instance.hide();
         UIModuleSmallLoading.Instance.removeEventListener(Event.CLOSE,this.onSmallLoadingClose);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.createActivityFrame);
         UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUIProgress);
      }
      
      public function useBattleCompanion(param1:InventoryItemInfo) : void
      {
         this._battleCompanionInfo = param1;
         this._giveFriendOpenFrame = ComponentFactory.Instance.creatComponentByStylename("core.ddtshop.ShopPresentClearingFrame");
         this._giveFriendOpenFrame.nameInput.enable = false;
         this._giveFriendOpenFrame.onlyFriendSelectView();
         this._giveFriendOpenFrame.show();
         this._giveFriendOpenFrame.presentBtn.addEventListener(MouseEvent.CLICK,this.__presentBtnClick,false,0,true);
         this._giveFriendOpenFrame.addEventListener(FrameEvent.RESPONSE,this.__responseHandler2,false,0,true);
      }
      
      private function __responseHandler2(param1:FrameEvent) : void
      {
         if(param1.responseCode == FrameEvent.CLOSE_CLICK || param1.responseCode == FrameEvent.ESC_CLICK || param1.responseCode == FrameEvent.CANCEL_CLICK)
         {
            this.removeBattleCompanion();
            this._giveFriendOpenFrame = null;
         }
      }
      
      private function removeBattleCompanion() : void
      {
         if(this._giveFriendOpenFrame && this._giveFriendOpenFrame.presentBtn)
         {
            this._giveFriendOpenFrame.presentBtn.removeEventListener(MouseEvent.CLICK,this.__presentBtnClick);
         }
         if(this._giveFriendOpenFrame)
         {
            this._giveFriendOpenFrame.removeEventListener(FrameEvent.RESPONSE,this.__responseHandler2);
         }
         this._battleCompanionInfo = null;
      }
      
      private function __presentBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         var _loc2_:String = this._giveFriendOpenFrame.nameInput.text;
         if(_loc2_ == "")
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("shop.ShopIIPresentView.give"));
            return;
         }
         if(FilterWordManager.IsNullorEmpty(_loc2_))
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("shop.ShopIIPresentView.space"));
            return;
         }
         SocketManager.Instance.out.sendBattleCompanionGive(this._giveFriendOpenFrame.selectPlayerId,this._battleCompanionInfo.BagType,this._battleCompanionInfo.Place);
         this.removeBattleCompanion();
         ObjectUtils.disposeObject(this._giveFriendOpenFrame);
         this._giveFriendOpenFrame = null;
      }
      
      private function getTodayList() : Array
      {
         var _loc5_:ActiveEventsInfo = null;
         var _loc1_:Array = [];
         var _loc2_:int = CalendarManager.getInstance().eventActives.length;
         var _loc3_:Date = TimeManager.Instance.Now();
         var _loc4_:int = 0;
         while(_loc4_ < _loc2_)
         {
            _loc5_ = CalendarManager.getInstance().eventActives[_loc4_];
            if(_loc3_.time > _loc5_.start.time && _loc3_.time < _loc5_.end.time)
            {
               _loc1_.push(_loc5_);
            }
            _loc4_++;
         }
         return _loc1_;
      }
      
      public function get eventsActiveDic() : Dictionary
      {
         return this._eventsActiveDic;
      }
      
      public function getActiveEventsInfoByID(param1:int) : ActiveEventsInfo
      {
         return this._eventsActiveDic[param1];
      }
      
      public function setLimitActivities(param1:Array) : void
      {
         var _loc4_:ActiveEventsInfo = null;
         var _loc5_:String = null;
         var _loc2_:int = 10000;
         this._eventsActiveDic = new Dictionary();
         var _loc3_:Date = TimeManager.Instance.Now();
         for each(_loc4_ in param1)
         {
            if(_loc3_.time > _loc4_.start.time && _loc3_.time < _loc4_.end.time)
            {
               this._eventsActiveDic[_loc2_] = _loc4_;
               _loc5_ = _loc2_.toString();
               this.leftViewInfoDic[_loc5_] = new LeftViewInfoVo(_loc2_,"· " + _loc4_.Title,1);
               if(_loc4_.HasKey != 1)
               {
                  this.addElement(_loc5_);
               }
               else
               {
                  this.addElement(_loc5_,true);
               }
               _loc2_++;
            }
         }
      }
      
      public function getActivityDataById(param1:String) : GmActivityInfo
      {
         return this.activityData[param1];
      }
      
      public function getActivityInitDataById(param1:String) : Object
      {
         return this.activityInitData[param1];
      }
      
      public function get frame() : LimitActivityFrame
      {
         return this._frame;
      }
      
      public function get sendFrame() : SendGiftActivityFrame
      {
         return this._sendGiftFrame;
      }
      
      public function get actList() : Array
      {
         return this._actList;
      }
   }
}
