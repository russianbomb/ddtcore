package wonderfulActivity.data
{
   public class WonderfulActivityTypeData
   {
      
      public static const MAIN_PAY_ACTIVITY:int = 0;
      
      public static const FIRST_CONTACT:int = 0;
      
      public static const ACC_FIRST_PAY:int = 1;
      
      public static const ONE_OFF_PAY:int = 2;
      
      public static const ACCUMULATIVE_PAY:int = 3;
      
      public static const NEWGAMEBENIFIT:int = 5;
      
      public static const PAY_RETURN:int = 6;
      
      public static const ONE_OFF_IN_TIME:int = 7;
      
      public static const MAIN_CONSUME_ACTIVITY:int = 1;
      
      public static const ONE_OFF_CONSUME:int = 0;
      
      public static const CONSUME_RETURN:int = 1;
      
      public static const SPECIFIC_COUNT_CONSUME:int = 2;
      
      public static const EXCHANGE_ACTIVITY:int = 2;
      
      public static const NORMAL_EXCHANGE:int = 0;
      
      public static const MARRY_ACTIVITY:int = 4;
      
      public static const HOLD_WEDDING:int = 2;
      
      public static const RECEIVE_ACTIVITY:int = 5;
      
      public static const USER_ID_RECEIVE:int = 1;
      
      public static const DAILY_RECEIVE:int = 2;
      
      public static const SEND_GIFT:int = 3;
      
      public static const CONSORTION_ACTIVITY:int = 6;
      
      public static const TUANJIE_POWER:int = 1;
      
      public static const MAIN_FAMOUS_ACTIVITY:int = 7;
      
      public static const HERO_POWER:int = 0;
      
      public static const HERO_GRADE:int = 1;
      
      public static const STRENGTHEN_ACTIVITY:int = 8;
      
      public static const STRENGTHEN_DAREN:int = 0;
      
      public static const GROUP_PURCHASE_ACTIVITY:int = 10;
      
      public static const NORMAL_GROUP_PURCHASE:int = 0;
      
      public static const FLOWER_GIVING_ACTIVITY:int = 11;
      
      public static const ROSE:int = 0;
      
      public static const MUM:int = 1;
      
      public static const CARNETION:int = 2;
      
      public static const CONSUME_RANK_ACTIVITY:int = 12;
      
      public static const CONSUME_RANK:int = 0;
       
      
      public function WonderfulActivityTypeData()
      {
         super();
      }
   }
}
