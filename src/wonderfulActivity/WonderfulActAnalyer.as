package wonderfulActivity
{
   import com.pickgliss.loader.DataAnalyzer;
   import com.pickgliss.utils.ObjectUtils;
   import wonderfulActivity.data.ActivityTypeData;
   
   public class WonderfulActAnalyer extends DataAnalyzer
   {
       
      
      public var itemList:Vector.<ActivityTypeData>;
      
      public function WonderfulActAnalyer(param1:Function)
      {
         super(param1);
      }
      
      override public function analyze(param1:*) : void
      {
         var _loc6_:ActivityTypeData = null;
         this.itemList = new Vector.<ActivityTypeData>();
         var _loc2_:XML = new XML(param1);
         var _loc3_:int = _loc2_.Item.length();
         var _loc4_:XMLList = _loc2_.Item;
         var _loc5_:int = 0;
         while(_loc5_ < _loc4_.length())
         {
            _loc6_ = new ActivityTypeData();
            ObjectUtils.copyPorpertiesByXML(_loc6_,_loc4_[_loc5_]);
            this.itemList.push(_loc6_);
            _loc5_++;
         }
         onAnalyzeComplete();
      }
   }
}
