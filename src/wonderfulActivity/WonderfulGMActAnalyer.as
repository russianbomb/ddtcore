package wonderfulActivity
{
   import com.pickgliss.loader.DataAnalyzer;
   import com.pickgliss.utils.ObjectUtils;
   import flash.utils.Dictionary;
   import wonderfulActivity.data.GiftBagInfo;
   import wonderfulActivity.data.GiftConditionInfo;
   import wonderfulActivity.data.GiftRewardInfo;
   import wonderfulActivity.data.GmActivityInfo;
   
   public class WonderfulGMActAnalyer extends DataAnalyzer
   {
       
      
      private var _activityData:Dictionary;
      
      public function WonderfulGMActAnalyer(param1:Function)
      {
         super(param1);
         this._activityData = new Dictionary();
      }
      
      override public function analyze(param1:*) : void
      {
         var _loc3_:XMLList = null;
         var _loc4_:int = 0;
         var _loc5_:GmActivityInfo = null;
         var _loc6_:Array = null;
         var _loc7_:XML = null;
         var _loc8_:XMLList = null;
         var _loc9_:int = 0;
         var _loc10_:GiftBagInfo = null;
         var _loc11_:Vector.<GiftConditionInfo> = null;
         var _loc12_:Vector.<GiftRewardInfo> = null;
         var _loc13_:XMLList = null;
         var _loc14_:int = 0;
         var _loc15_:GiftConditionInfo = null;
         var _loc16_:XMLList = null;
         var _loc17_:int = 0;
         var _loc18_:GiftRewardInfo = null;
         var _loc19_:int = 0;
         var _loc2_:XML = new XML(param1);
         if(_loc2_.@value == "true")
         {
            _loc3_ = _loc2_..ActiveInfo;
            _loc4_ = 0;
            while(_loc4_ < _loc3_.length())
            {
               _loc5_ = new GmActivityInfo();
               ObjectUtils.copyPorpertiesByXML(_loc5_,_loc3_[_loc4_].Activity[0]);
               _loc5_.beginShowTime = _loc5_.beginShowTime.replace(/-/g,"/");
               _loc5_.beginTime = _loc5_.beginTime.replace(/-/g,"/");
               _loc5_.endShowTime = _loc5_.endShowTime.replace(/-/g,"/");
               _loc5_.endTime = _loc5_.endTime.replace(/-/g,"/");
               _loc6_ = new Array();
               _loc7_ = _loc3_[_loc4_].ActiveGiftBag[0];
               if(_loc7_)
               {
                  _loc8_ = _loc7_..Gift;
               }
               else
               {
                  _loc8_ = null;
               }
               if(_loc7_ && _loc8_ && _loc8_.length() > 0)
               {
                  _loc9_ = 0;
                  while(_loc9_ < _loc8_.length())
                  {
                     _loc10_ = new GiftBagInfo();
                     ObjectUtils.copyPorpertiesByXML(_loc10_,_loc8_[_loc9_]);
                     _loc11_ = new Vector.<GiftConditionInfo>();
                     _loc12_ = new Vector.<GiftRewardInfo>();
                     if(_loc7_.ActiveCondition.length() > 0)
                     {
                        _loc13_ = _loc7_.ActiveCondition[_loc9_].Condition;
                        _loc14_ = 0;
                        while(_loc14_ < _loc13_.length())
                        {
                           _loc15_ = new GiftConditionInfo();
                           ObjectUtils.copyPorpertiesByXML(_loc15_,_loc13_[_loc14_]);
                           _loc11_.push(_loc15_);
                           _loc14_++;
                        }
                     }
                     if(_loc7_.ActiveReward.length() > 0)
                     {
                        _loc16_ = _loc7_.ActiveReward[_loc9_].Reward;
                        _loc17_ = 0;
                        while(_loc17_ < _loc16_.length())
                        {
                           _loc18_ = new GiftRewardInfo();
                           ObjectUtils.copyPorpertiesByXML(_loc18_,_loc16_[_loc17_]);
                           _loc19_ = int(_loc16_[_loc17_].@isBind[0]);
                           _loc18_.isBind = _loc19_ == 1;
                           _loc12_.push(_loc18_);
                           _loc17_++;
                        }
                     }
                     _loc10_.giftConditionArr = _loc11_;
                     _loc10_.giftRewardArr = _loc12_;
                     _loc6_.push(_loc10_);
                     _loc9_++;
                  }
               }
               _loc6_.sortOn("giftbagOrder",Array.NUMERIC);
               _loc5_.giftbagArray = _loc6_;
               this._activityData[_loc5_.activityId] = _loc5_;
               _loc4_++;
            }
            onAnalyzeComplete();
         }
         else
         {
            message = _loc2_.@message;
            onAnalyzeError();
            onAnalyzeError();
         }
      }
      
      public function get ActivityData() : Dictionary
      {
         return this._activityData;
      }
   }
}
