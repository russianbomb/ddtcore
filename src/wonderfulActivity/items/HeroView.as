package wonderfulActivity.items
{
   import bagAndInfo.cell.BagCell;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.data.player.SelfInfo;
   import ddt.manager.ItemManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import flash.display.Bitmap;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import flash.utils.Dictionary;
   import wonderfulActivity.WonderfulActivityManager;
   import wonderfulActivity.data.GiftBagInfo;
   import wonderfulActivity.data.GiftRewardInfo;
   import wonderfulActivity.data.GmActivityInfo;
   import wonderfulActivity.data.SendGiftInfo;
   import wonderfulActivity.data.WonderfulActivityTypeData;
   import wonderfulActivity.views.IRightView;
   
   public class HeroView extends Sprite implements IRightView
   {
       
      
      private var _back:Bitmap;
      
      private var _activeTimeBit:Bitmap;
      
      private var _activetimeFilter:FilterFrameText;
      
      private var _getButton:SimpleBitmapButton;
      
      private var _cartoonList:Vector.<MovieClip>;
      
      private var _cartoonVisibleArr:Array;
      
      private var _bagCellGrayArr:Array;
      
      private var _bagCellArr:Array;
      
      private var _mcNum:int;
      
      private var _info:SelfInfo;
      
      private var _fightPowerArr:Array;
      
      private var _gradeArr:Array;
      
      private var _numPower:int = 0;
      
      private var _numGrade:int = 0;
      
      private var _activityInfo1:GmActivityInfo;
      
      private var _activityInfo2:GmActivityInfo;
      
      private var _activityInfoArr:Array;
      
      private var _giftInfoDic:Dictionary;
      
      private var _statusPowerArr:Array;
      
      private var _statusGradeArr:Array;
      
      public function HeroView()
      {
         super();
         this._fightPowerArr = new Array();
         this._gradeArr = new Array();
         this._activityInfoArr = new Array();
         this._cartoonVisibleArr = [false,false,false,false];
         this._bagCellGrayArr = [false,false,false,false];
         this._bagCellArr = new Array();
         this._info = PlayerManager.Instance.Self;
      }
      
      public function setState(param1:int, param2:int) : void
      {
      }
      
      public function init() : void
      {
         this.initView();
         this.initData();
         this.initViewWithData();
      }
      
      private function initViewWithData() : void
      {
         var _loc1_:int = 0;
         var _loc2_:Array = null;
         var _loc3_:Array = null;
         var _loc4_:Bitmap = null;
         var _loc5_:FilterFrameText = null;
         var _loc6_:BagCell = null;
         var _loc7_:Bitmap = null;
         var _loc8_:FilterFrameText = null;
         var _loc9_:BagCell = null;
         if(!this.checkData())
         {
            return;
         }
         if(this._activityInfo1)
         {
            _loc2_ = [this._activityInfo1.beginTime.split(" ")[0],this._activityInfo1.endTime.split(" ")[0]];
            this._activetimeFilter.text = _loc2_[0] + "-" + _loc2_[1];
         }
         else if(this._activityInfo2)
         {
            _loc3_ = [this._activityInfo2.beginTime.split(" ")[0],this._activityInfo2.endTime.split(" ")[0]];
            this._activetimeFilter.text = _loc3_[0] + "-" + _loc3_[1];
         }
         _loc1_ = 0;
         while(_loc1_ < 4 && _loc1_ < this._numPower + this._numGrade)
         {
            if(_loc1_ < this._numPower)
            {
               _loc4_ = ComponentFactory.Instance.creat("wonderfulactivity.powerBitmap");
               _loc4_.x = _loc4_.x + 120 * _loc1_;
               addChild(_loc4_);
               _loc5_ = ComponentFactory.Instance.creatComponentByStylename("wonderfulactivity.right.heroTxt");
               _loc5_.x = _loc5_.x + 120 * _loc1_;
               _loc5_.text = this._fightPowerArr[_loc1_];
               addChild(_loc5_);
               _loc6_ = this.createBagCell(_loc1_,this._activityInfo1.giftbagArray[_loc1_]);
               _loc6_.x = _loc5_.x + 21;
               _loc6_.y = _loc5_.y + 45;
               addChild(_loc6_);
            }
            else
            {
               _loc7_ = ComponentFactory.Instance.creat("wonderfulactivity.levelBitmap");
               _loc7_.x = _loc7_.x + 120 * _loc1_;
               addChild(_loc7_);
               _loc8_ = ComponentFactory.Instance.creatComponentByStylename("wonderfulactivity.right.heroTxt");
               _loc8_.x = _loc8_.x + 120 * _loc1_;
               _loc8_.text = this._gradeArr[_loc1_ - this._numPower];
               addChild(_loc8_);
               _loc9_ = this.createBagCell(_loc1_,this._activityInfo2.giftbagArray[_loc1_ - this._numPower]);
               _loc9_.x = _loc7_.x + 31;
               _loc9_.y = _loc8_.y + 45;
               addChild(_loc9_);
            }
            _loc1_++;
         }
         this.initItem();
      }
      
      private function checkData() : Boolean
      {
         if((this._activityInfo1 || this._activityInfo2) && (this._fightPowerArr.length > 0 || this._gradeArr.length > 0))
         {
            return true;
         }
         return false;
      }
      
      private function initView() : void
      {
         this._back = ComponentFactory.Instance.creat("wonderfulactivity.hero.back");
         addChild(this._back);
         this._activeTimeBit = ComponentFactory.Instance.creat("wonderfulactivity.activetime");
         addChild(this._activeTimeBit);
         this._activetimeFilter = ComponentFactory.Instance.creatComponentByStylename("wonderfulactivity.right.activetimeTxt");
         addChild(this._activetimeFilter);
         this._getButton = ComponentFactory.Instance.creatComponentByStylename("wonderful.ActivityState.GetButton");
         addChild(this._getButton);
         this._getButton.enable = false;
      }
      
      private function createBagCell(param1:int, param2:GiftBagInfo) : BagCell
      {
         var _loc3_:GiftRewardInfo = param2.giftRewardArr[0];
         var _loc4_:InventoryItemInfo = new InventoryItemInfo();
         _loc4_.TemplateID = _loc3_.templateId;
         _loc4_ = ItemManager.fill(_loc4_);
         _loc4_.IsBinds = _loc3_.isBind;
         _loc4_.ValidDate = _loc3_.validDate;
         var _loc5_:Array = _loc3_.property.split(",");
         _loc4_._StrengthenLevel = parseInt(_loc5_[0]);
         _loc4_.AttackCompose = parseInt(_loc5_[1]);
         _loc4_.DefendCompose = parseInt(_loc5_[2]);
         _loc4_.AgilityCompose = parseInt(_loc5_[3]);
         _loc4_.LuckCompose = parseInt(_loc5_[4]);
         var _loc6_:BagCell = new BagCell(param1);
         _loc6_.info = _loc4_;
         _loc6_.setCount(_loc3_.count);
         _loc6_.setBgVisible(false);
         if(this._giftInfoDic)
         {
            if(this._giftInfoDic[param2.giftbagId] && this._giftInfoDic[param2.giftbagId].times > 0)
            {
               this._bagCellGrayArr[param1] = _loc6_.grayFilters = true;
            }
         }
         else
         {
            this._bagCellGrayArr[param1] = _loc6_.grayFilters = false;
         }
         this._bagCellArr.push(_loc6_);
         return _loc6_;
      }
      
      private function initData() : void
      {
         var _loc2_:GmActivityInfo = null;
         var _loc3_:int = 0;
         var _loc4_:Dictionary = null;
         var _loc5_:* = null;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:Dictionary = null;
         var _loc9_:* = null;
         var _loc10_:int = 0;
         var _loc1_:Dictionary = WonderfulActivityManager.Instance.activityInitData;
         for each(_loc2_ in WonderfulActivityManager.Instance.activityData)
         {
            if(_loc2_.activityType == WonderfulActivityTypeData.MAIN_FAMOUS_ACTIVITY)
            {
               if(_loc2_.activityChildType == WonderfulActivityTypeData.HERO_POWER)
               {
                  if(WonderfulActivityManager.Instance.activityInitData[_loc2_.activityId])
                  {
                     if(!this._giftInfoDic)
                     {
                        this._giftInfoDic = WonderfulActivityManager.Instance.activityInitData[_loc2_.activityId]["giftInfoDic"];
                     }
                     else
                     {
                        _loc4_ = WonderfulActivityManager.Instance.activityInitData[_loc2_.activityId]["giftInfoDic"];
                        if(_loc4_)
                        {
                           for(_loc5_ in _loc4_)
                           {
                              this._giftInfoDic[_loc5_] = _loc4_[_loc5_];
                           }
                        }
                     }
                     this._statusPowerArr = WonderfulActivityManager.Instance.activityInitData[_loc2_.activityId]["statusArr"];
                  }
                  this._numPower = _loc2_.giftbagArray.length;
                  _loc3_ = 0;
                  while(_loc3_ < this._numPower && _loc3_ < 4)
                  {
                     _loc6_ = 0;
                     while(_loc6_ < _loc2_.giftbagArray[_loc3_].giftConditionArr.length)
                     {
                        if(_loc2_.giftbagArray[_loc3_].giftConditionArr[_loc6_].conditionIndex == 0)
                        {
                           this._fightPowerArr.push(_loc2_.giftbagArray[_loc3_].giftConditionArr[_loc6_].conditionValue);
                        }
                        _loc6_++;
                     }
                     _loc3_++;
                  }
                  this._activityInfo1 = _loc2_;
                  this._activityInfoArr.push(this._activityInfo1);
               }
               else if(_loc2_.activityChildType == WonderfulActivityTypeData.HERO_GRADE)
               {
                  if(WonderfulActivityManager.Instance.activityInitData[_loc2_.activityId])
                  {
                     if(!this._giftInfoDic)
                     {
                        this._giftInfoDic = WonderfulActivityManager.Instance.activityInitData[_loc2_.activityId]["giftInfoDic"];
                     }
                     else
                     {
                        _loc8_ = WonderfulActivityManager.Instance.activityInitData[_loc2_.activityId]["giftInfoDic"];
                        if(_loc8_)
                        {
                           for(_loc9_ in _loc8_)
                           {
                              this._giftInfoDic[_loc9_] = _loc8_[_loc9_];
                           }
                        }
                     }
                     this._statusGradeArr = WonderfulActivityManager.Instance.activityInitData[_loc2_.activityId]["statusArr"];
                  }
                  this._numGrade = _loc2_.giftbagArray.length;
                  _loc7_ = 0;
                  while(_loc7_ < this._numGrade && _loc7_ < 4)
                  {
                     _loc10_ = 0;
                     while(_loc10_ < _loc2_.giftbagArray[_loc7_].giftConditionArr.length)
                     {
                        if(_loc2_.giftbagArray[_loc7_].giftConditionArr[_loc10_].conditionIndex == 0)
                        {
                           this._gradeArr.push(_loc2_.giftbagArray[_loc7_].giftConditionArr[_loc10_].conditionValue);
                        }
                        _loc10_++;
                     }
                     _loc7_++;
                  }
                  this._activityInfo2 = _loc2_;
                  this._activityInfoArr.push(this._activityInfo2);
               }
            }
         }
      }
      
      private function initCartoonPlayArr(param1:Array, param2:Array) : void
      {
         var _loc3_:int = 0;
         while(_loc3_ < param1.length && _loc3_ < 4)
         {
            if(this._statusPowerArr && this._statusPowerArr[0].statusValue >= param1[_loc3_] && !this._bagCellGrayArr[_loc3_])
            {
               this._cartoonVisibleArr[_loc3_] = true;
            }
            else
            {
               this._cartoonVisibleArr[_loc3_] = false;
            }
            _loc3_++;
         }
         var _loc4_:int = 0;
         while(_loc4_ < 4 - param1.length)
         {
            if(this._statusGradeArr && this._statusGradeArr[0].statusValue >= param2[_loc4_] && !this._bagCellGrayArr[_loc3_])
            {
               this._cartoonVisibleArr[param1.length + _loc4_] = true;
            }
            else
            {
               this._cartoonVisibleArr[param1.length + _loc4_] = false;
            }
            _loc4_++;
         }
      }
      
      private function initItem() : void
      {
         var _loc2_:MovieClip = null;
         this._cartoonList = new Vector.<MovieClip>();
         this.initCartoonPlayArr(this._fightPowerArr,this._gradeArr);
         var _loc1_:int = 0;
         while(_loc1_ < 4)
         {
            if(this._cartoonVisibleArr[_loc1_])
            {
               _loc2_ = ComponentFactory.Instance.creat("wonderfulactivity.cartoon");
               _loc2_.mouseChildren = false;
               _loc2_.mouseEnabled = false;
               _loc2_.x = 268 + 120 * _loc1_;
               _loc2_.y = 311;
               addChild(_loc2_);
               this._cartoonList.push(_loc2_);
            }
            _loc1_++;
         }
         if(this._cartoonList.length > 0)
         {
            this._getButton.enable = true;
            this._getButton.addEventListener(MouseEvent.CLICK,this.__getAward);
         }
      }
      
      private function __getAward(param1:MouseEvent) : void
      {
         var _loc6_:SendGiftInfo = null;
         var _loc7_:Array = null;
         var _loc8_:int = 0;
         SoundManager.instance.playButtonSound();
         var _loc2_:int = 0;
         while(_loc2_ < this._bagCellArr.length)
         {
            if(this._cartoonVisibleArr[_loc2_])
            {
               (this._bagCellArr[_loc2_] as BagCell).grayFilters = true;
            }
            _loc2_++;
         }
         var _loc3_:int = 0;
         while(_loc3_ < this._cartoonList.length)
         {
            this._cartoonList[_loc3_].parent.removeChild(this._cartoonList[_loc3_]);
            _loc3_++;
         }
         this._getButton.enable = false;
         var _loc4_:Vector.<SendGiftInfo> = new Vector.<SendGiftInfo>();
         var _loc5_:int = 0;
         while(_loc5_ < this._activityInfoArr.length)
         {
            _loc6_ = new SendGiftInfo();
            _loc6_.activityId = this._activityInfoArr[_loc5_].activityId;
            _loc7_ = new Array();
            _loc8_ = 0;
            while(_loc8_ < this._activityInfoArr[_loc5_].giftbagArray.length)
            {
               _loc7_.push(this._activityInfoArr[_loc5_].giftbagArray[_loc8_].giftbagId);
               _loc8_++;
            }
            _loc6_.giftIdArr = _loc7_;
            _loc4_.push(_loc6_);
            _loc5_++;
         }
         SocketManager.Instance.out.sendWonderfulActivityGetReward(_loc4_);
      }
      
      public function content() : Sprite
      {
         return this;
      }
      
      public function dispose() : void
      {
         var _loc1_:MovieClip = null;
         this._getButton.removeEventListener(MouseEvent.CLICK,this.__getAward);
         this._bagCellArr = null;
         for each(_loc1_ in this._cartoonList)
         {
            if(_loc1_.parent)
            {
               _loc1_.parent.removeChild(_loc1_);
            }
            _loc1_ = null;
         }
         this._cartoonList = null;
         while(this.numChildren)
         {
            ObjectUtils.disposeObject(this.getChildAt(0));
         }
         if(parent)
         {
            ObjectUtils.disposeObject(this);
         }
      }
   }
}
