package wonderfulActivity.items
{
   import activeEvents.data.ActiveEventsInfo;
   import calendar.CalendarManager;
   import calendar.view.ICalendar;
   import calendar.view.goodsExchange.GoodsExchangeInfo;
   import calendar.view.goodsExchange.SendGoodsExchangeInfo;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.container.SimpleTileList;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.SocketManager;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import road7th.utils.DateUtils;
   import wonderfulActivity.event.ActivityEvent;
   
   public class NationalDayView extends Sprite implements ICalendar
   {
       
      
      private var _bg:Bitmap;
      
      private var _activityTime:FilterFrameText;
      
      private var _description:FilterFrameText;
      
      private var _activityInfo:ActiveEventsInfo;
      
      private var _goodsExchangeInfoVector:Vector.<GoodsExchangeInfo>;
      
      private var _cellVector:Vector.<ActivitySeedCell>;
      
      private var _haveGoodsBox1:SimpleTileList;
      
      private var _haveGoodsBox2:SimpleTileList;
      
      private var _haveGoodsBox3:SimpleTileList;
      
      private var _haveGoodsBox:Vector.<SimpleTileList>;
      
      private var _exchangeCellVec:Vector.<ActivitySeedCell>;
      
      public function NationalDayView()
      {
         super();
         this.initview();
      }
      
      private function initview() : void
      {
         this._bg = ComponentFactory.Instance.creatBitmap("asset.activity.nationalbg");
         addChild(this._bg);
         this._activityTime = ComponentFactory.Instance.creatComponentByStylename("ddtcalendar.midAutumn.actTime");
         PositionUtils.setPos(this._activityTime,"ddtcalendar.nationalDayView.timePos");
         addChild(this._activityTime);
         this._description = ComponentFactory.Instance.creatComponentByStylename("ddtcalendar.midAutumn.description");
         PositionUtils.setPos(this._description,"ddtcalendar.nationalDayView.description");
         addChild(this._description);
         this._haveGoodsBox1 = ComponentFactory.Instance.creatCustomObject("ddtcalendar.nationalDayView.haveGoodsBox1",[4]);
         addChild(this._haveGoodsBox1);
         this._haveGoodsBox2 = ComponentFactory.Instance.creatCustomObject("ddtcalendar.nationalDayView.haveGoodsBox2",[4]);
         addChild(this._haveGoodsBox2);
         this._haveGoodsBox3 = ComponentFactory.Instance.creatCustomObject("ddtcalendar.nationalDayView.haveGoodsBox3",[4]);
         addChild(this._haveGoodsBox3);
         this._haveGoodsBox = new Vector.<SimpleTileList>();
         this._haveGoodsBox.push(this._haveGoodsBox1);
         this._haveGoodsBox.push(this._haveGoodsBox2);
         this._haveGoodsBox.push(this._haveGoodsBox3);
      }
      
      public function setData(param1:* = null) : void
      {
         var _loc2_:Array = null;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         this._activityInfo = param1 as ActiveEventsInfo;
         if(this._activityInfo)
         {
            this._goodsExchangeInfoVector = new Vector.<GoodsExchangeInfo>();
            _loc2_ = CalendarManager.getInstance().activeExchange;
            _loc3_ = _loc2_.length;
            _loc4_ = 0;
            while(_loc4_ < _loc3_)
            {
               if(this._activityInfo.ActiveID == _loc2_[_loc4_].ActiveID)
               {
                  this._goodsExchangeInfoVector.push(_loc2_[_loc4_]);
               }
               _loc4_++;
            }
            this.updateTimeShow();
            this.updateDescription();
            this.updateHaveGoodsBox();
            this.updateExchangeGoodsBox();
            this.addAwardAnimation();
         }
      }
      
      private function updateDescription() : void
      {
         this._description.text = this._activityInfo.Description;
      }
      
      private function addAwardAnimation() : void
      {
         var _loc5_:int = 0;
         var _loc1_:int = 4;
         var _loc2_:int = this._cellVector.length;
         var _loc3_:Boolean = true;
         var _loc4_:int = 0;
         while(_loc4_ < _loc2_)
         {
            if(_loc4_ == 0 || _loc4_ == 4 || _loc4_ == 12)
            {
               _loc1_ = _loc1_ + _loc4_;
               _loc3_ = true;
            }
            if(this._cellVector[_loc4_].needCount < 0)
            {
               _loc3_ = false;
            }
            else if(_loc4_ % _loc1_ == 3 && _loc3_)
            {
               _loc5_ = _loc4_ < 4?int(0):_loc4_ < 12?int(1):int(2);
               this._exchangeCellVec[_loc5_].addFireworkAnimation(_loc5_);
               this._exchangeCellVec[_loc5_].addEventListener(ActivityEvent.SEND_GOOD,this.__seedGood);
            }
            _loc4_++;
         }
      }
      
      private function __updateCellCount(param1:ActivityEvent) : void
      {
         this.setGetAwardBtnEnable(param1.id);
      }
      
      private function setGetAwardBtnEnable(param1:int) : void
      {
         var _loc2_:int = 0;
         if(this._cellVector[param1].needCount < 0)
         {
            _loc2_ = param1 < 4?int(0):param1 < 12?int(1):int(2);
            this._exchangeCellVec[_loc2_].removeFireworkAnimation();
            this._exchangeCellVec[_loc2_].removeEventListener(ActivityEvent.SEND_GOOD,this.__seedGood);
         }
      }
      
      protected function __seedGood(param1:ActivityEvent) : void
      {
         var _loc8_:SendGoodsExchangeInfo = null;
         var _loc2_:int = param1.id;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         if(_loc2_ == 0)
         {
            _loc3_ = 0;
            _loc4_ = 4;
         }
         else if(_loc2_ == 1)
         {
            _loc3_ = 4;
            _loc4_ = 12;
         }
         else
         {
            _loc3_ = 12;
            _loc4_ = 24;
         }
         var _loc5_:Boolean = false;
         var _loc6_:Vector.<SendGoodsExchangeInfo> = new Vector.<SendGoodsExchangeInfo>();
         var _loc7_:int = _loc3_;
         while(_loc7_ < _loc4_)
         {
            _loc8_ = new SendGoodsExchangeInfo();
            _loc8_.id = this._cellVector[_loc7_].info.TemplateID;
            _loc8_.place = this._cellVector[_loc7_].itemInfo.Place;
            _loc8_.bagType = this._cellVector[_loc7_].itemInfo.BagType;
            _loc6_.push(_loc8_);
            _loc7_++;
         }
         SocketManager.Instance.out.sendGoodsExchange(_loc6_,this._activityInfo.ActiveID,1,_loc2_);
      }
      
      private function updateHaveGoodsBox() : void
      {
         var _loc4_:int = 0;
         var _loc5_:ActivitySeedCell = null;
         if(!this._goodsExchangeInfoVector)
         {
            return;
         }
         this.cleanCell();
         var _loc1_:int = this._goodsExchangeInfoVector.length;
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         while(_loc3_ < 3)
         {
            this._haveGoodsBox[_loc3_].disposeAllChildren();
            ObjectUtils.removeChildAllChildren(this._haveGoodsBox[_loc3_]);
            _loc4_ = 0;
            while(_loc4_ < _loc1_)
            {
               if(this._goodsExchangeInfoVector[_loc4_].ItemType == _loc3_ * 2)
               {
                  _loc5_ = new ActivitySeedCell(this._goodsExchangeInfoVector[_loc4_],this._activityInfo.ActiveType,this._cellVector.length);
                  this._haveGoodsBox[_loc3_].addChild(_loc5_);
                  _loc5_.addEventListener(ActivityEvent.UPDATE_COUNT,this.__updateCellCount);
                  _loc2_ = _loc2_ + 1;
                  this._cellVector.push(_loc5_);
               }
               _loc4_++;
            }
            _loc3_++;
         }
      }
      
      private function updateExchangeGoodsBox() : void
      {
         var _loc4_:int = 0;
         var _loc5_:ActivitySeedCell = null;
         if(!this._goodsExchangeInfoVector)
         {
            return;
         }
         this.cleanExchangeCell();
         var _loc1_:int = this._goodsExchangeInfoVector.length;
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         while(_loc3_ < 5)
         {
            _loc4_ = 0;
            while(_loc4_ < _loc1_)
            {
               if(this._goodsExchangeInfoVector[_loc4_].ItemType == _loc3_ * 2 + 1)
               {
                  this.setFireworkTemplateID(this._goodsExchangeInfoVector[_loc4_]);
                  _loc5_ = new ActivitySeedCell(this._goodsExchangeInfoVector[_loc4_],this._activityInfo.ActiveType,this._exchangeCellVec.length,false);
                  addChild(_loc5_);
                  this._exchangeCellVec.push(_loc5_);
                  PositionUtils.setPos(_loc5_,"ddtcalendar.nationalDayView.fireworkPos" + this._exchangeCellVec.length);
                  _loc2_ = _loc2_ + 1;
               }
               _loc4_++;
            }
            _loc3_++;
         }
      }
      
      private function setFireworkTemplateID(param1:GoodsExchangeInfo) : void
      {
         if(param1.TemplateID > 112333 && param1.TemplateID < 112337)
         {
            param1.TemplateID = param1.TemplateID - 100616;
         }
      }
      
      private function cleanExchangeCell() : void
      {
         var _loc1_:ActivitySeedCell = null;
         for each(_loc1_ in this._exchangeCellVec)
         {
            _loc1_.removeEventListener(ActivityEvent.SEND_GOOD,this.__seedGood);
            ObjectUtils.disposeObject(_loc1_);
            _loc1_ = null;
         }
         this._exchangeCellVec = new Vector.<ActivitySeedCell>();
      }
      
      private function cleanCell() : void
      {
         var _loc1_:ActivitySeedCell = null;
         for each(_loc1_ in this._cellVector)
         {
            _loc1_.removeEventListener(ActivityEvent.UPDATE_COUNT,this.__updateCellCount);
            ObjectUtils.disposeObject(_loc1_);
            _loc1_ = null;
         }
         this._cellVector = new Vector.<ActivitySeedCell>();
      }
      
      private function updateTimeShow() : void
      {
         var _loc1_:Date = DateUtils.getDateByStr(this._activityInfo.StartDate);
         var _loc2_:Date = DateUtils.getDateByStr(this._activityInfo.EndDate);
         this._activityTime.text = this.addZero(_loc1_.fullYear) + "." + this.addZero(_loc1_.month + 1) + "." + this.addZero(_loc1_.date);
         this._activityTime.text = this._activityTime.text + ("-" + this.addZero(_loc2_.fullYear) + "." + this.addZero(_loc2_.month + 1) + "." + this.addZero(_loc2_.date));
      }
      
      private function addZero(param1:Number) : String
      {
         var _loc2_:String = null;
         if(param1 < 10)
         {
            _loc2_ = "0" + param1.toString();
         }
         else
         {
            _loc2_ = param1.toString();
         }
         return _loc2_;
      }
      
      public function dispose() : void
      {
         if(this._bg)
         {
            this._bg.bitmapData.dispose();
            this._bg = null;
         }
         this.cleanCell();
         this.cleanExchangeCell();
         if(this._activityTime)
         {
            this._activityTime.dispose();
            this._activityTime = null;
         }
         if(this._description)
         {
            this._description.dispose();
            this._description = null;
         }
         var _loc1_:int = 0;
         while(_loc1_ < this._haveGoodsBox.length)
         {
            this._haveGoodsBox[_loc1_].dispose();
            this._haveGoodsBox[_loc1_] = null;
            _loc1_++;
         }
         this._haveGoodsBox.length = 0;
         this._haveGoodsBox = null;
      }
   }
}
