package wonderfulActivity.views
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.container.VBox;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import wonderfulActivity.WonderfulActivityManager;
   import wonderfulActivity.event.WonderfulActivityEvent;
   
   public class ActivityLeftView extends Sprite implements Disposeable
   {
       
      
      private var _bg:Bitmap;
      
      private var _vbox:VBox;
      
      private var _unitList:Vector.<ActivityLeftUnitView>;
      
      private var tempArray:Array;
      
      private var _rightFun:Function;
      
      private var selectedUnitIndex:int;
      
      private var _isNewServerExist:Boolean = false;
      
      public function ActivityLeftView()
      {
         super();
         this.initView();
         this.selectedUnitIndex = -1;
      }
      
      public function setRightView(param1:Function) : void
      {
         this._rightFun = param1;
      }
      
      private function initView() : void
      {
         this._bg = ComponentFactory.Instance.creat("wonderful.leftview.BG");
         addChild(this._bg);
         this._vbox = ComponentFactory.Instance.creatComponentByStylename("wonderful.leftview.vbox");
         this._unitList = new Vector.<ActivityLeftUnitView>();
         addChild(this._vbox);
      }
      
      public function addUnitByType(param1:Array, param2:int) : void
      {
         var _loc3_:ActivityLeftUnitView = this.getLeftUnitView(param2);
         if(_loc3_ == null)
         {
            _loc3_ = new ActivityLeftUnitView(param2);
            _loc3_.addEventListener(WonderfulActivityEvent.SELECTED_CHANGE,this.refreshView);
            this._vbox.addChild(_loc3_);
            this._unitList.push(_loc3_);
         }
         this.setBgHeight(_loc3_);
         _loc3_.setData(param1,this._rightFun);
         this._vbox.refreshChildPos();
      }
      
      private function setBgHeight(param1:ActivityLeftUnitView) : void
      {
         if(this.isNewServerExist)
         {
            param1.bg.height = 300;
            param1.list.height = 280;
         }
         else
         {
            param1.bg.height = 360;
            param1.list.height = 340;
         }
      }
      
      public function checkNewServerExist() : void
      {
         var _loc1_:ActivityLeftUnitView = this.getLeftUnitView(3);
         if(!_loc1_)
         {
            return;
         }
         var _loc2_:int = 0;
         while(_loc2_ < this._vbox.numChildren)
         {
            if(this._vbox.getChildAt(_loc2_) == _loc1_)
            {
               this._vbox.removeChildAt(_loc2_);
               this._unitList[_loc2_].removeEventListener(WonderfulActivityEvent.SELECTED_CHANGE,this.refreshView);
               this._unitList.splice(this._unitList.indexOf(_loc2_),1);
               ObjectUtils.disposeObject(_loc1_);
               _loc1_ = null;
               break;
            }
            _loc2_++;
         }
         this._vbox.refreshChildPos();
      }
      
      public function extendUnitView() : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc1_:int = 0;
         if(this.selectedUnitIndex < 0)
         {
            if(WonderfulActivityManager.Instance.isSkipFromHall)
            {
               _loc3_ = 0;
               while(_loc3_ <= this._unitList.length - 1)
               {
                  if(this._unitList[_loc3_].type == WonderfulActivityManager.Instance.leftUnitViewType && this._unitList[_loc3_].getModelSize() > 0)
                  {
                     _loc1_ = _loc3_;
                     break;
                  }
                  _loc1_ = 2;
                  _loc3_++;
               }
            }
            else
            {
               _loc4_ = 0;
               while(_loc4_ <= this._unitList.length - 1)
               {
                  if(this._unitList[_loc4_].getModelSize() > 0)
                  {
                     _loc1_ = _loc4_;
                     break;
                  }
                  _loc4_++;
               }
            }
         }
         else
         {
            if(this.selectedUnitIndex > this._unitList.length - 1)
            {
               this.selectedUnitIndex = 0;
            }
            _loc1_ = this.selectedUnitIndex;
         }
         (this._unitList[_loc1_] as ActivityLeftUnitView).extendSelecteTheFirst();
         var _loc2_:int = 0;
         while(_loc2_ <= this._unitList.length - 1)
         {
            if(_loc2_ != _loc1_)
            {
               (this._unitList[_loc2_] as ActivityLeftUnitView).unextendHandler();
            }
            _loc2_++;
         }
         this._vbox.refreshChildPos();
      }
      
      private function getLeftUnitView(param1:int) : ActivityLeftUnitView
      {
         var _loc2_:int = 0;
         while(_loc2_ <= this._unitList.length - 1)
         {
            if(param1 == this._unitList[_loc2_].type)
            {
               return this._unitList[_loc2_];
            }
            _loc2_++;
         }
         return null;
      }
      
      private function refreshView(param1:WonderfulActivityEvent) : void
      {
         var _loc2_:ActivityLeftUnitView = param1.target as ActivityLeftUnitView;
         var _loc3_:int = 0;
         while(_loc3_ <= this._unitList.length - 1)
         {
            if(this._unitList[_loc3_] == _loc2_)
            {
               this.selectedUnitIndex = _loc3_;
            }
            else
            {
               this._unitList[_loc3_].unextendHandler();
            }
            _loc3_++;
         }
         this._vbox.arrange();
      }
      
      private function removeEvent() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ <= this._unitList.length - 1)
         {
            this._unitList[_loc1_].removeEventListener(WonderfulActivityEvent.SELECTED_CHANGE,this.refreshView);
            _loc1_++;
         }
      }
      
      public function dispose() : void
      {
         var _loc1_:ActivityLeftUnitView = null;
         this.removeEvent();
         if(this._bg)
         {
            ObjectUtils.disposeObject(this._bg);
         }
         this._bg = null;
         if(this._vbox)
         {
            ObjectUtils.disposeObject(this._vbox);
         }
         this._vbox = null;
         if(this._unitList)
         {
            for each(_loc1_ in this._unitList)
            {
               ObjectUtils.disposeObject(_loc1_);
               _loc1_ = null;
            }
         }
         this._unitList = null;
      }
      
      public function get isNewServerExist() : Boolean
      {
         return this._isNewServerExist;
      }
      
      public function set isNewServerExist(param1:Boolean) : void
      {
         this._isNewServerExist = param1;
      }
   }
}
