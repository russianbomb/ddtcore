package wonderfulActivity.views
{
   import activeEvents.data.ActiveEventsInfo;
   import calendar.view.ICalendar;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import consumeRank.views.ConsumeRankView;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.utils.Dictionary;
   import wonderfulActivity.ActivityType;
   import wonderfulActivity.WonderfulActivityManager;
   import wonderfulActivity.data.ActivityTypeData;
   import wonderfulActivity.items.AccumulativePayView;
   import wonderfulActivity.items.FighterRutrunView;
   import wonderfulActivity.items.HeroView;
   import wonderfulActivity.items.JoinIsPowerView;
   import wonderfulActivity.items.LimitActivityView;
   import wonderfulActivity.items.NewGameBenifitView;
   import wonderfulActivity.items.RechargeReturnView;
   import wonderfulActivity.items.StrengthDarenView;
   import wonderfulActivity.newActivity.AnnouncementAct.AnnouncementActView;
   import wonderfulActivity.newActivity.ExchangeAct.ExchangeActView;
   import wonderfulActivity.newActivity.GetRewardAct.GetRewardActView;
   import wonderfulActivity.newActivity.returnActivity.ReturnActivityView;
   
   public class WonderfulRightView extends Sprite implements Disposeable
   {
       
      
      private var _view:IRightView;
      
      private var _tittle:Bitmap;
      
      private var _currentInfo:ActiveEventsInfo;
      
      private var _currentLimitView:ICalendar;
      
      public function WonderfulRightView()
      {
         super();
      }
      
      public function setState(param1:int, param2:int) : void
      {
         if(!this._view)
         {
            return;
         }
         this._view.setState(param1,param2);
      }
      
      public function updateView(param1:String) : void
      {
         var _loc4_:ActivityTypeData = null;
         this.dispose();
         var _loc2_:Dictionary = WonderfulActivityManager.Instance.leftViewInfoDic;
         var _loc3_:int = _loc2_[param1].viewType;
         switch(_loc3_)
         {
            case ActivityType.CHONGZHIHUIKUI:
               _loc4_ = WonderfulActivityManager.Instance.activityRechargeList[0];
               this._view = new RechargeReturnView(1,_loc4_);
               break;
            case ActivityType.XIAOFEIHUIKUI:
               _loc4_ = WonderfulActivityManager.Instance.activityExpList[0];
               this._view = new RechargeReturnView(2,_loc4_);
               break;
            case ActivityType.ZHANYOUCHONGZHIHUIKUI:
               _loc4_ = WonderfulActivityManager.Instance.activityFighterList[0];
               this._view = new FighterRutrunView(_loc4_);
               break;
            case ActivityType.HERO:
               this._view = new HeroView();
               break;
            case ActivityType.NEWGAMEBENIFIT:
               this._view = new NewGameBenifitView();
               break;
            case ActivityType.ACCUMULATIVE_PAY:
               this._view = new AccumulativePayView();
               break;
            case ActivityType.STRENGTHEN_DAREN:
               this._view = new StrengthDarenView();
               break;
            case ActivityType.TUANJIE_POWER:
               this._view = new JoinIsPowerView();
               break;
            case ActivityType.ONE_OFF_PAY:
               break;
            case ActivityType.PAY_RETURN:
               this._view = new ReturnActivityView(0,param1);
               break;
            case ActivityType.CONSUME_RETURN:
               this._view = new ReturnActivityView(1,param1);
               break;
            case ActivityType.NORMAL_EXCHANGE:
               this._view = new ExchangeActView(param1);
               break;
            case ActivityType.ACT_ANNOUNCEMENT:
               this._view = new AnnouncementActView(param1);
               break;
            case ActivityType.RECEIVE_ACTIVITY:
               this._view = new GetRewardActView(param1);
               break;
            case ActivityType.CONSUME_RANK:
               this._view = new ConsumeRankView();
               break;
            default:
               if(this._view)
               {
                  this._view.dispose();
                  this._view = null;
               }
         }
         if(_loc3_ >= ActivityType.LIMIT_FLOOR && _loc3_ <= ActivityType.LIMIT_CEILING)
         {
            this._view = new LimitActivityView(_loc3_);
         }
         if(this._view)
         {
            this._view.init();
            addChild(this._view.content());
            WonderfulActivityManager.Instance.currentView = this._view;
         }
         if(WonderfulActivityManager.Instance.currentView as ExchangeActView)
         {
            PositionUtils.setPos(this,"limitActivity.wonderfulRightViewPos2");
         }
         else
         {
            PositionUtils.setPos(this,"limitActivity.wonderfulRightViewPos");
         }
         if(WonderfulActivityManager.Instance.currentView as LimitActivityView)
         {
            PositionUtils.setPos(this,"limitActivity.wonderfulRightViewPos2");
         }
      }
      
      public function dispose() : void
      {
         ObjectUtils.disposeObject(this._view);
         ObjectUtils.disposeObject(this._currentLimitView);
         this._view = null;
         this._currentLimitView = null;
      }
   }
}
