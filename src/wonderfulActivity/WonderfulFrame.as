package wonderfulActivity
{
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.LanguageMgr;
   import ddt.manager.SharedManager;
   import ddt.manager.SoundManager;
   import flash.utils.Dictionary;
   import hallIcon.HallIconManager;
   import hallIcon.HallIconType;
   import mysteriousRoullete.MysteriousManager;
   import treasureHunting.TreasureManager;
   import wonderfulActivity.data.ActivityCellVo;
   import wonderfulActivity.views.ActivityLeftView;
   import wonderfulActivity.views.WonderfulRightView;
   
   public class WonderfulFrame extends Frame
   {
       
      
      private var _bag:ScaleBitmapImage;
      
      private var _leftView:ActivityLeftView;
      
      private var _rightView:WonderfulRightView;
      
      private var allMusic:Boolean;
      
      public function WonderfulFrame()
      {
         super();
         escEnable = true;
         this.allMusic = SharedManager.Instance.allowMusic;
         SharedManager.Instance.allowMusic = false;
         SharedManager.Instance.changed();
         this.initview();
         this.addEvents();
      }
      
      public function setState(param1:int, param2:int) : void
      {
         if(!this._rightView && !this._rightView.parent)
         {
            return;
         }
         this._rightView.setState(param1,param2);
      }
      
      private function initview() : void
      {
         titleText = LanguageMgr.GetTranslation("wonderfulActivityManager.tittle");
         this._bag = ComponentFactory.Instance.creatComponentByStylename("wonderfulactivity.scale9cornerImageTree");
         addToContent(this._bag);
         this._leftView = new ActivityLeftView();
         this._leftView.x = 22;
         this._leftView.y = 46;
         addToContent(this._leftView);
         this._rightView = new WonderfulRightView();
         addToContent(this._rightView);
         this._leftView.setRightView(this._rightView.updateView);
      }
      
      private function addEvents() : void
      {
         addEventListener(FrameEvent.RESPONSE,this._response);
      }
      
      private function removeEvents() : void
      {
         removeEventListener(FrameEvent.RESPONSE,this._response);
      }
      
      public function addElement(param1:Array) : void
      {
         var _loc6_:ActivityCellVo = null;
         var _loc7_:String = null;
         var _loc2_:Array = [];
         var _loc3_:Array = [];
         var _loc4_:Array = [];
         var _loc5_:Dictionary = WonderfulActivityManager.Instance.leftViewInfoDic;
         for each(_loc7_ in param1)
         {
            _loc6_ = new ActivityCellVo();
            _loc6_.id = _loc7_;
            _loc6_.activityName = _loc5_[_loc7_].label;
            switch(_loc5_[_loc7_].unitIndex)
            {
               case 1:
                  _loc3_.push(_loc6_);
                  continue;
               case 2:
                  _loc2_.push(_loc6_);
                  continue;
               case 3:
                  _loc4_.push(_loc6_);
                  continue;
               default:
                  continue;
            }
         }
         if(_loc4_.length == 0)
         {
            this._leftView.isNewServerExist = false;
         }
         else
         {
            this._leftView.isNewServerExist = true;
         }
         this._leftView.addUnitByType(_loc2_,2);
         this._leftView.addUnitByType(_loc3_,1);
         if(this._leftView.isNewServerExist)
         {
            this._leftView.addUnitByType(_loc4_,3);
         }
         else
         {
            this._leftView.checkNewServerExist();
         }
         this._leftView.extendUnitView();
      }
      
      private function _response(param1:FrameEvent) : void
      {
         if(!WonderfulActivityManager.Instance.isRuning)
         {
            return;
         }
         if(param1.responseCode == FrameEvent.CLOSE_CLICK || param1.responseCode == FrameEvent.ESC_CLICK)
         {
            if(WonderfulActivityManager.Instance.frameCanClose)
            {
               SoundManager.instance.play("008");
               this.clear();
            }
         }
      }
      
      private function clear() : void
      {
         TreasureManager.instance.dispose();
         if(MysteriousManager.instance.isMysteriousClose)
         {
            HallIconManager.instance.updateSwitchHandler(HallIconType.MYSTERIOUROULETTE,false);
         }
         this.dispose();
         WonderfulActivityManager.Instance.dispose();
      }
      
      override public function dispose() : void
      {
         if(!WonderfulActivityManager.Instance.isRuning)
         {
            return;
         }
         SharedManager.Instance.allowMusic = this.allMusic;
         SharedManager.Instance.changed();
         this.removeEvents();
         ObjectUtils.disposeObject(this._leftView);
         ObjectUtils.disposeObject(this._rightView);
         this._leftView = null;
         this._rightView = null;
         super.dispose();
      }
   }
}
