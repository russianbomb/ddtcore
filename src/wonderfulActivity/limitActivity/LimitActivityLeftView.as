package wonderfulActivity.limitActivity
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.controls.ScrollPanel;
   import com.pickgliss.ui.controls.container.VBox;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.utils.ObjectUtils;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.utils.Dictionary;
   import wonderfulActivity.WonderfulActivityManager;
   import wonderfulActivity.data.ActivityCellVo;
   import wonderfulActivity.event.WonderfulActivityEvent;
   
   public class LimitActivityLeftView extends Sprite implements Disposeable
   {
       
      
      private var _bg:MovieClip;
      
      private var _scrollPanel:ScrollPanel;
      
      private var _vbox:VBox;
      
      private var _unitDic:Dictionary;
      
      private var _rightFun:Function;
      
      private var selectedUnitId:String = "";
      
      public function LimitActivityLeftView()
      {
         super();
         this.initView();
      }
      
      public function setRightView(param1:Function) : void
      {
         this._rightFun = param1;
      }
      
      private function initView() : void
      {
         this._bg = ComponentFactory.Instance.creat("wonderful.limitActivity.LargeBackgroundBg");
         addChild(this._bg);
         this._scrollPanel = ComponentFactory.Instance.creatComponentByStylename("wonderful.limitActivity.leftScrollpanel");
         this._vbox = ComponentFactory.Instance.creatComponentByStylename("wonderful.leftview.vbox");
         this._scrollPanel.setView(this._vbox);
         addChild(this._scrollPanel);
         this._scrollPanel.invalidateViewport();
         this._unitDic = new Dictionary();
      }
      
      public function setData(param1:Array) : void
      {
         var _loc3_:ActivityCellVo = null;
         var _loc4_:LimitActivityLeftUnit = null;
         var _loc2_:int = 0;
         while(_loc2_ < param1.length)
         {
            _loc3_ = param1[_loc2_] as ActivityCellVo;
            _loc4_ = this._unitDic[_loc3_.id];
            if(_loc4_ == null)
            {
               _loc4_ = new LimitActivityLeftUnit(_loc3_.id,_loc3_.activityName);
               _loc4_.addEventListener(WonderfulActivityEvent.SELECTED_CHANGE,this.refreshView);
               this._vbox.addChild(_loc4_);
               this._unitDic[_loc3_.id] = _loc4_;
            }
            _loc2_++;
         }
         if(this.selectedUnitId == "" && param1.length > 0)
         {
            this.selectedUnitId = param1[0].id;
            if(this._rightFun != null)
            {
               this._rightFun(this.selectedUnitId);
               if(this._unitDic[this.selectedUnitId])
               {
                  this._unitDic[this.selectedUnitId].selected = true;
               }
            }
         }
         this._vbox.refreshChildPos();
         this._scrollPanel.invalidateViewport();
         if(WonderfulActivityManager.Instance.selectId != "" && this._unitDic[WonderfulActivityManager.Instance.selectId])
         {
            (this._unitDic[WonderfulActivityManager.Instance.selectId] as LimitActivityLeftUnit).dispatchEvent(new WonderfulActivityEvent(WonderfulActivityEvent.SELECTED_CHANGE));
            WonderfulActivityManager.Instance.selectId = "";
         }
      }
      
      private function refreshView(param1:WonderfulActivityEvent) : void
      {
         var _loc2_:LimitActivityLeftUnit = null;
         var _loc3_:LimitActivityLeftUnit = null;
         for each(_loc2_ in this._unitDic)
         {
            _loc2_.selected = false;
         }
         _loc3_ = param1.target as LimitActivityLeftUnit;
         _loc3_.selected = true;
         this.selectedUnitId = _loc3_.id;
         if(this._rightFun != null)
         {
            this._rightFun(this.selectedUnitId);
         }
         this._vbox.arrange();
         this._scrollPanel.invalidateViewport();
      }
      
      private function removeEvent() : void
      {
         var _loc1_:LimitActivityLeftUnit = null;
         for each(_loc1_ in this._unitDic)
         {
            _loc1_.removeEventListener(WonderfulActivityEvent.SELECTED_CHANGE,this.refreshView);
         }
      }
      
      public function dispose() : void
      {
         var _loc1_:LimitActivityLeftUnit = null;
         this.removeEvent();
         if(this._bg)
         {
            this._bg.stop();
            ObjectUtils.disposeObject(this._bg);
            this._bg = null;
         }
         if(this._vbox)
         {
            ObjectUtils.disposeObject(this._vbox);
         }
         this._vbox = null;
         if(this._scrollPanel)
         {
            ObjectUtils.disposeAllChildren(this._scrollPanel);
            ObjectUtils.disposeObject(this._scrollPanel);
            this._scrollPanel = null;
         }
         if(this._unitDic)
         {
            for each(_loc1_ in this._unitDic)
            {
               ObjectUtils.disposeObject(_loc1_);
               _loc1_ = null;
            }
         }
         this._unitDic = null;
      }
   }
}
