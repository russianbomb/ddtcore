package wonderfulActivity
{
   public class ActivityType
   {
      
      public static const CHONGZHIHUIKUI:int = 3001;
      
      public static const XIAOFEIHUIKUI:int = 4001;
      
      public static const ZHANYOUCHONGZHIHUIKUI:int = 2001;
      
      public static const SHOUCHONGLIBAO:int = 7;
      
      public static const NEWGAMEBENIFIT:int = 9;
      
      public static const HERO:int = 10;
      
      public static const ACCUMULATIVE_PAY:int = 12;
      
      public static const TUANJIE_POWER:int = 14;
      
      public static const STRENGTHEN_DAREN:int = 15;
      
      public static const LIMIT_FLOOR:int = 10000;
      
      public static const LIMIT_CEILING:int = 11000;
      
      public static const ONE_OFF_PAY:int = 17;
      
      public static const PAY_RETURN:int = 18;
      
      public static const CONSUME_RETURN:int = 19;
      
      public static const NORMAL_EXCHANGE:int = 20;
      
      public static const ACT_ANNOUNCEMENT:int = 21;
      
      public static const RECEIVE_ACTIVITY:int = 22;
      
      public static const CONSUME_RANK:int = 23;
       
      
      public function ActivityType()
      {
         super();
      }
   }
}
