package ringStation.view
{
   import bagAndInfo.info.PlayerInfoViewControl;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.ui.text.GradientText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.player.PlayerInfo;
   import ddt.events.PlayerPropertyEvent;
   import ddt.manager.LanguageMgr;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.events.TextEvent;
   import ringStation.model.BattleFieldListItemInfo;
   import vip.VipController;
   
   public class BattleFieldsItem extends Sprite implements Disposeable
   {
       
      
      private var _fightIconBg:Bitmap;
      
      private var _fightIcon:Bitmap;
      
      private var _infoText:FilterFrameText;
      
      private var _index:int;
      
      private var _nickNameText:FilterFrameText;
      
      private var _vipName:GradientText;
      
      private var _battleInfo:BattleFieldListItemInfo;
      
      private var _playerInfo:PlayerInfo;
      
      private var msgID:String = "1";
      
      public function BattleFieldsItem(param1:int)
      {
         super();
         this._index = param1;
         this.init();
      }
      
      private function init() : void
      {
         this._infoText = ComponentFactory.Instance.creatComponentByStylename("ringStation.view.battleField.itemInfo");
         addChild(this._infoText);
         this._nickNameText = ComponentFactory.Instance.creatComponentByStylename("ringStation.view.battleItem.nickNameText");
         this._nickNameText.mouseEnabled = true;
         this._nickNameText.addEventListener(TextEvent.LINK,this.__nickNameLinkHandler);
         addChild(this._nickNameText);
      }
      
      public function update(param1:BattleFieldListItemInfo) : void
      {
         this._battleInfo = param1;
         if(this._fightIconBg)
         {
            ObjectUtils.disposeObject(this._fightIconBg);
         }
         this._fightIconBg = null;
         if(this._fightIcon)
         {
            ObjectUtils.disposeObject(this._fightIcon);
         }
         this._fightIcon = null;
         if(this._battleInfo.DareFlag)
         {
            this._fightIcon = ComponentFactory.Instance.creatBitmap("ringStation.view.swordIcon");
         }
         else
         {
            this._fightIcon = ComponentFactory.Instance.creatBitmap("ringStation.view.shieldIcon");
         }
         this._fightIconBg = ComponentFactory.Instance.creat("ringStation.view.fightIconBg");
         addChild(this._fightIconBg);
         addChild(this._fightIcon);
         var _loc2_:String = "";
         _loc2_ = this._index == 0?LanguageMgr.GetTranslation("ringStation.view.battleFieldsView.itemInfoNew"):"";
         this._infoText.htmlText = _loc2_ + this.updateText(this._battleInfo);
         this._nickNameText.htmlText = "<u><a href=\'event:battleTxt\'>" + this._battleInfo.UserName + "</a></u>";
         this.setUerNameLength(this._nickNameText);
         if(this.msgID == "1")
         {
            this._nickNameText.x = 269;
         }
         else if(this.msgID == "2")
         {
            this._nickNameText.x = 269;
         }
         else if(this.msgID == "3")
         {
            this._nickNameText.x = 207;
         }
         else if(this.msgID == "4")
         {
            this._nickNameText.x = 124;
         }
         else if(this.msgID == "5")
         {
            this._nickNameText.x = 26;
            this._nickNameText.y = 13;
         }
         else if(this.msgID == "6")
         {
            this._nickNameText.x = 123;
         }
         this.findPlayer(param1.UserName);
      }
      
      private function findPlayer(param1:String) : void
      {
         this._playerInfo = new PlayerInfo();
         if(param1 == PlayerManager.Instance.Self.NickName)
         {
            this._playerInfo = PlayerManager.Instance.Self;
         }
         else
         {
            this._playerInfo = PlayerManager.Instance.findPlayerByNickName(this._playerInfo,param1);
         }
         if(this._playerInfo.ID && this._playerInfo.Style)
         {
            this.updateTxt();
         }
         else
         {
            SocketManager.Instance.out.sendItemEquip(param1,true);
            this._playerInfo.addEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__playerInfoChange);
         }
      }
      
      private function __playerInfoChange(param1:PlayerPropertyEvent) : void
      {
         this._playerInfo.removeEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__playerInfoChange);
         this.updateTxt();
      }
      
      private function updateTxt() : void
      {
         if(this._playerInfo.IsVIP)
         {
            this._vipName = VipController.instance.getVipNameTxt(68,this._playerInfo.typeVIP);
            this._vipName.addEventListener(MouseEvent.CLICK,this.__nickNameLinkHandler);
            this._vipName.textField.autoSize = "none";
            this._vipName.textField.setTextFormat(this._nickNameText.getTextFormat());
            this._vipName.textField.defaultTextFormat = this._nickNameText.getTextFormat();
            this._vipName.buttonMode = true;
            this._vipName.textSize = 12;
            this._vipName.x = this._nickNameText.x;
            this._vipName.y = this._nickNameText.y;
            this._vipName.htmlText = "<u><a href=\'event:battleTxt\'>" + this._nickNameText.text + "</a></u>";
            addChild(this._vipName);
            PositionUtils.adaptNameStyle(this._playerInfo,this._nickNameText,this._vipName);
         }
      }
      
      private function updateText(param1:BattleFieldListItemInfo) : String
      {
         if(param1.DareFlag)
         {
            this.msgID = !!param1.SuccessFlag?"1":"2";
            if(param1.SuccessFlag && param1.Level == 0)
            {
               this.msgID = "5";
            }
         }
         else
         {
            this.msgID = !!param1.SuccessFlag?"3":"4";
            if(!param1.SuccessFlag && param1.Level == 0)
            {
               this.msgID = "6";
            }
         }
         return LanguageMgr.GetTranslation("ringStation.view.battleFieldsView.itemInfo" + this.msgID,"                                    ",param1.Level);
      }
      
      private function setUerNameLength(param1:FilterFrameText) : void
      {
         var _loc2_:int = 0;
         if(param1.textWidth >= 60)
         {
            _loc2_ = param1.getCharIndexAtPoint(param1.width - 12,5);
            param1.text = param1.text.substring(0,_loc2_) + "...";
         }
      }
      
      private function __nickNameLinkHandler(param1:Event) : void
      {
         SoundManager.instance.play("008");
         if(this._playerInfo)
         {
            PlayerInfoViewControl.view(this._playerInfo);
         }
      }
      
      public function dispose() : void
      {
         if(this._fightIconBg)
         {
            ObjectUtils.disposeObject(this._fightIconBg);
            this._fightIconBg = null;
         }
         if(this._fightIcon)
         {
            ObjectUtils.disposeObject(this._fightIcon);
            this._fightIcon = null;
         }
         if(this._infoText)
         {
            ObjectUtils.disposeObject(this._infoText);
            this._infoText = null;
         }
         if(this._nickNameText)
         {
            this._nickNameText.removeEventListener(TextEvent.LINK,this.__nickNameLinkHandler);
            ObjectUtils.disposeObject(this._nickNameText);
         }
         if(this._vipName)
         {
            this._vipName.removeEventListener(MouseEvent.CLICK,this.__nickNameLinkHandler);
            ObjectUtils.disposeObject(this._vipName);
            this._vipName = null;
         }
      }
   }
}
