package ringStation.view
{
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import ddt.data.player.PlayerInfo;
   import ddt.manager.LanguageMgr;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.manager.StateManager;
   import ddt.states.StateType;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import game.GameManager;
   import ringStation.RingStationManager;
   import ringStation.event.RingStationEvent;
   import road7th.comm.PackageIn;
   import trainer.data.ArrowType;
   import trainer.view.NewHandContainer;
   
   public class RingStationView extends Frame
   {
      
      private static var CHALLENGEPERSON_NUM:int = 4;
       
      
      private var _titleBitmap:Bitmap;
      
      private var _frameBottom:ScaleBitmapImage;
      
      private var _helpBtn:BaseButton;
      
      private var _userView:StationUserView;
      
      private var _challengeVec:Vector.<ChallengePerson>;
      
      private var _arrowSrite:Sprite;
      
      private var _nameArray:Array;
      
      public function RingStationView()
      {
         this._nameArray = new Array(LanguageMgr.GetTranslation("ringStation.view.person.name0"),LanguageMgr.GetTranslation("ringStation.view.person.name1"),LanguageMgr.GetTranslation("ringStation.view.person.name2"));
         super();
         this.initView();
         this.initEvent();
         this.sendPkg();
      }
      
      private function initView() : void
      {
         var _loc2_:ChallengePerson = null;
         titleText = LanguageMgr.GetTranslation("ddt.ringstation.titleInfo");
         this._titleBitmap = ComponentFactory.Instance.creat("ringstation.view.title");
         addToContent(this._titleBitmap);
         this._frameBottom = ComponentFactory.Instance.creatComponentByStylename("ringstation.frameBottom");
         addToContent(this._frameBottom);
         this._helpBtn = ComponentFactory.Instance.creat("ringStation.view.helpBtn");
         addToContent(this._helpBtn);
         this._userView = new StationUserView();
         PositionUtils.setPos(this._userView,"ringStation.view.userViewPos");
         addToContent(this._userView);
         this._challengeVec = new Vector.<ChallengePerson>();
         var _loc1_:int = 0;
         while(_loc1_ < CHALLENGEPERSON_NUM)
         {
            _loc2_ = new ChallengePerson();
            PositionUtils.setPos(_loc2_,"ringStation.challenge.personPos" + _loc1_);
            addToContent(_loc2_);
            this._challengeVec.push(_loc2_);
            _loc1_++;
         }
      }
      
      private function initEvent() : void
      {
         addEventListener(FrameEvent.RESPONSE,this.__frameEventHandler);
         this._helpBtn.addEventListener(MouseEvent.CLICK,this.__onHelpClick);
         SocketManager.Instance.addEventListener(RingStationEvent.RINGSTATION_VIEWINFO,this.__setViewInfo);
         GameManager.Instance.addEventListener(GameManager.START_LOAD,this.__startLoading);
         StateManager.getInGame_Step_1 = true;
      }
      
      protected function __onHelpClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         var _loc2_:HelpView = ComponentFactory.Instance.creat("ringStation.helpView");
         LayerManager.Instance.addToLayer(_loc2_,LayerManager.STAGE_TOP_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
      }
      
      private function __startLoading(param1:Event) : void
      {
         StateManager.getInGame_Step_6 = true;
         StateManager.setState(StateType.ROOM_LOADING,GameManager.Instance.Current);
         StateManager.getInGame_Step_7 = true;
      }
      
      private function sendPkg() : void
      {
         SocketManager.Instance.out.sendRingStationGetInfo();
      }
      
      protected function __setViewInfo(param1:RingStationEvent) : void
      {
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         var _loc12_:int = 0;
         var _loc13_:int = 0;
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readInt();
         this._userView.setRankNum(_loc3_);
         this._userView.setChallengeNum(_loc2_.readInt());
         this._userView.buyCount = _loc2_.readInt();
         this._userView.buyPrice = _loc2_.readInt();
         var _loc4_:Date = _loc2_.readDate();
         this._userView.setChallengeTime(_loc4_);
         this._userView.cdPrice = _loc2_.readInt();
         _loc2_.readInt();
         var _loc5_:String = _loc2_.readUTF();
         this._userView.setSignText(_loc5_);
         this._userView.setAwardNum(_loc2_.readInt());
         this._userView.setAwardTime(_loc2_.readDate());
         this._userView.setChampionText(_loc2_.readUTF());
         if(_loc3_ == 0)
         {
            this._arrowSrite = new Sprite();
            addToContent(this._arrowSrite);
            NewHandContainer.Instance.showArrow(ArrowType.RING_STATION,45,"ringStation.view.challenge.arrowPos","","",this._arrowSrite,0,true);
         }
         var _loc6_:int = _loc2_.readInt();
         var _loc7_:Vector.<PlayerInfo> = new Vector.<PlayerInfo>();
         var _loc8_:Array = new Array();
         var _loc9_:int = 0;
         while(_loc9_ < _loc6_)
         {
            _loc7_.push(this.readPersonInfo(_loc2_));
            _loc8_.push(_loc7_[_loc9_].Rank);
            _loc9_++;
         }
         if(_loc6_ == 1 && _loc7_[0].Rank == 0)
         {
            this._challengeVec[0].updatePerson(_loc7_[0]);
            _loc10_ = 1;
            while(_loc10_ < CHALLENGEPERSON_NUM)
            {
               _loc7_[0].NickName = this._nameArray[_loc10_ - 1];
               this._challengeVec[_loc10_].updatePerson(_loc7_[0]);
               _loc10_++;
            }
         }
         else
         {
            _loc8_.sort(Array.NUMERIC);
            _loc11_ = 0;
            while(_loc11_ < _loc8_.length)
            {
               _loc13_ = 0;
               while(_loc13_ < _loc7_.length)
               {
                  if(_loc7_[_loc13_].Rank == _loc8_[_loc11_])
                  {
                     this._challengeVec[_loc11_].updatePerson(_loc7_[_loc13_]);
                  }
                  _loc13_++;
               }
               _loc11_++;
            }
            _loc12_ = _loc6_;
            while(_loc12_ < this._challengeVec.length)
            {
               this._challengeVec[_loc12_].setWaiting();
               _loc12_++;
            }
         }
      }
      
      private function readPersonInfo(param1:PackageIn) : PlayerInfo
      {
         var _loc2_:PlayerInfo = new PlayerInfo();
         _loc2_.ID = param1.readInt();
         _loc2_.LoginName = param1.readUTF();
         _loc2_.NickName = param1.readUTF();
         _loc2_.typeVIP = param1.readByte();
         _loc2_.VIPLevel = param1.readInt();
         _loc2_.Grade = param1.readInt();
         _loc2_.Sex = param1.readBoolean();
         _loc2_.Style = param1.readUTF();
         _loc2_.Colors = param1.readUTF();
         _loc2_.Skin = param1.readUTF();
         _loc2_.ConsortiaName = param1.readUTF();
         _loc2_.Hide = param1.readInt();
         _loc2_.Offer = param1.readInt();
         _loc2_.WinCount = param1.readInt();
         _loc2_.TotalCount = param1.readInt();
         _loc2_.EscapeCount = param1.readInt();
         _loc2_.Repute = param1.readInt();
         _loc2_.Nimbus = param1.readInt();
         _loc2_.GP = param1.readInt();
         _loc2_.FightPower = param1.readInt();
         _loc2_.AchievementPoint = param1.readInt();
         _loc2_.Rank = param1.readInt();
         _loc2_.WeaponID = param1.readInt();
         _loc2_.signMsg = param1.readUTF();
         return _loc2_;
      }
      
      public function show() : void
      {
         LayerManager.Instance.addToLayer(this,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.ALPHA_BLOCKGOUND);
      }
      
      private function removeEvent() : void
      {
         removeEventListener(FrameEvent.RESPONSE,this.__frameEventHandler);
         this._helpBtn.removeEventListener(MouseEvent.CLICK,this.__onHelpClick);
         SocketManager.Instance.removeEventListener(RingStationEvent.RINGSTATION_VIEWINFO,this.__setViewInfo);
         GameManager.Instance.removeEventListener(GameManager.START_LOAD,this.__startLoading);
         StateManager.getInGame_Step_8 = true;
      }
      
      private function __frameEventHandler(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         switch(param1.responseCode)
         {
            case FrameEvent.ESC_CLICK:
            case FrameEvent.CLOSE_CLICK:
               RingStationManager.instance.hide();
         }
      }
      
      override public function dispose() : void
      {
         var _loc1_:int = 0;
         super.dispose();
         this.removeEvent();
         if(this._titleBitmap)
         {
            this._titleBitmap = null;
         }
         if(this._frameBottom)
         {
            this._frameBottom.dispose();
            this._frameBottom = null;
         }
         if(this._helpBtn)
         {
            this._helpBtn.dispose();
            this._helpBtn = null;
         }
         if(this._userView)
         {
            this._userView.dispose();
            this._userView = null;
         }
         if(this._challengeVec)
         {
            _loc1_ = 0;
            while(_loc1_ < this._challengeVec.length)
            {
               this._challengeVec[_loc1_].dispose();
               this._challengeVec[_loc1_] = null;
               _loc1_++;
            }
            this._challengeVec.length = 0;
            this._challengeVec = null;
         }
         if(this._arrowSrite)
         {
            NewHandContainer.Instance.clearArrowByID(ArrowType.RING_STATION);
            this._arrowSrite = null;
         }
         if(this.parent)
         {
            this.parent.removeChild(this);
         }
      }
   }
}
