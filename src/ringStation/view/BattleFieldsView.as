package ringStation.view
{
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.image.MutipleImage;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.LanguageMgr;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import flash.geom.Point;
   import ringStation.event.RingStationEvent;
   import ringStation.model.BattleFieldListItemInfo;
   import road7th.comm.PackageIn;
   
   public class BattleFieldsView extends Frame
   {
      
      private static const BATTLEFILEDSNUM:int = 11;
       
      
      private var _bg:MutipleImage;
      
      private var _itemVec:Vector.<BattleFieldsItem>;
      
      public function BattleFieldsView()
      {
         super();
         this.initView();
         this.initEvent();
      }
      
      private function initView() : void
      {
         titleText = LanguageMgr.GetTranslation("ringStation.view.battleFieldsView.titleInfo");
         this._bg = ComponentFactory.Instance.creatComponentByStylename("ringStation.view.battleField.bg");
         addToContent(this._bg);
         this.initItemData();
         this.sendPkg();
      }
      
      private function initItemData() : void
      {
         var _loc2_:int = 0;
         var _loc3_:BattleFieldsItem = null;
         this._itemVec = new Vector.<BattleFieldsItem>();
         var _loc1_:Point = PositionUtils.creatPoint("ringStation.view.battleField.itemPos");
         _loc2_ = 0;
         while(_loc2_ < BATTLEFILEDSNUM)
         {
            _loc3_ = new BattleFieldsItem(_loc2_);
            _loc3_.x = _loc1_.x;
            _loc3_.y = _loc1_.y + _loc2_ * 33.7;
            addToContent(_loc3_);
            this._itemVec.push(_loc3_);
            _loc2_++;
         }
      }
      
      private function sendPkg() : void
      {
         SocketManager.Instance.out.sendRingStationBattleField();
      }
      
      private function initEvent() : void
      {
         addEventListener(FrameEvent.RESPONSE,this.__frameEventHandler);
         SocketManager.Instance.addEventListener(RingStationEvent.RINGSTATION_NEWBATTLEFIELD,this.__onUpdateNewBattleField);
      }
      
      protected function __onUpdateNewBattleField(param1:RingStationEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:BattleFieldListItemInfo = new BattleFieldListItemInfo();
         var _loc4_:int = _loc2_.readInt();
         var _loc5_:String = "";
         var _loc6_:int = 0;
         while(_loc6_ < _loc4_)
         {
            _loc3_.DareFlag = _loc2_.readBoolean();
            _loc3_.UserName = _loc2_.readUTF();
            _loc3_.SuccessFlag = _loc2_.readBoolean();
            _loc3_.Level = _loc2_.readInt();
            this._itemVec[_loc6_].update(_loc3_);
            _loc6_++;
         }
      }
      
      public function show() : void
      {
         LayerManager.Instance.addToLayer(this,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.ALPHA_BLOCKGOUND);
      }
      
      private function removeEvent() : void
      {
         removeEventListener(FrameEvent.RESPONSE,this.__frameEventHandler);
         SocketManager.Instance.removeEventListener(RingStationEvent.RINGSTATION_NEWBATTLEFIELD,this.__onUpdateNewBattleField);
      }
      
      private function __frameEventHandler(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         switch(param1.responseCode)
         {
            case FrameEvent.ESC_CLICK:
            case FrameEvent.CLOSE_CLICK:
               this.dispose();
         }
      }
      
      override public function dispose() : void
      {
         super.dispose();
         this.removeEvent();
         if(this._bg)
         {
            ObjectUtils.disposeObject(this._bg);
         }
         this._bg = null;
         var _loc1_:int = 0;
         while(_loc1_ < this._itemVec.length)
         {
            if(this._itemVec[_loc1_])
            {
               ObjectUtils.disposeObject(this._itemVec[_loc1_]);
               this._itemVec[_loc1_] = null;
            }
            _loc1_++;
         }
         this._itemVec.length = 0;
         this._itemVec = null;
         if(this.parent)
         {
            this.parent.removeChild(this);
         }
      }
   }
}
