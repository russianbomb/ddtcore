package ringStation.view
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.image.MovieImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.ui.text.GradientText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.player.PlayerInfo;
   import ddt.manager.LanguageMgr;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import ddt.view.character.CharactoryFactory;
   import ddt.view.character.ICharacter;
   import ddt.view.common.LevelIcon;
   import flash.display.Bitmap;
   import flash.display.DisplayObject;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import ringStation.RingStationManager;
   import vip.VipController;
   
   public class ChallengePerson extends Sprite
   {
       
      
      private var _bg:Bitmap;
      
      private var _waiting:Bitmap;
      
      private var _levelIcon:LevelIcon;
      
      private var _nickNameText:FilterFrameText;
      
      private var _vipName:GradientText;
      
      private var _rank:FilterFrameText;
      
      private var _player:ICharacter;
      
      private var _challengeBtn:MovieImage;
      
      private var _playerInfo:PlayerInfo;
      
      private var _clickDate:Number = 0;
      
      private var _signBG:Bitmap;
      
      private var _signBG2:Bitmap;
      
      private var _signText:FilterFrameText;
      
      private var _maskSprite:Sprite;
      
      private var _playerSprite:Sprite;
      
      public function ChallengePerson()
      {
         super();
         this.initView();
         this.initEvent();
      }
      
      private function initView() : void
      {
         this._bg = ComponentFactory.Instance.creat("ringStation.view.challengeBg");
         addChild(this._bg);
         this._levelIcon = new LevelIcon();
         PositionUtils.setPos(this._levelIcon,"ringStation.view.player.levelPos");
         addChild(this._levelIcon);
         this._playerSprite = new Sprite();
         addChild(this._playerSprite);
         this._signBG2 = ComponentFactory.Instance.creat("ringStation.view.challengeSingBG");
         this._signBG2.y = 27;
         this._signBG2.alpha = 0.5;
         addChild(this._signBG2);
         this._nickNameText = ComponentFactory.Instance.creatComponentByStylename("ringStation.view.person.nickNameText");
         addChild(this._nickNameText);
         this._rank = ComponentFactory.Instance.creatComponentByStylename("ringStation.view.person.rank");
         this._challengeBtn = ComponentFactory.Instance.creat("ringStation.view.player.challengeBtn");
         this._challengeBtn.buttonMode = true;
         this._challengeBtn.visible = false;
      }
      
      private function addSignCell() : void
      {
         this._signBG = ComponentFactory.Instance.creat("ringStation.view.challengeSingBG");
         this._signBG.alpha = 0.5;
         this._signText = ComponentFactory.Instance.creatComponentByStylename("ringStation.view.signText2");
         this._signText.text = LanguageMgr.GetTranslation("ringstation.view.signNormal");
         addChild(this._signBG);
         addChild(this._signText);
      }
      
      private function initEvent() : void
      {
         addEventListener(MouseEvent.MOUSE_OVER,this.__onMouseOver);
         addEventListener(MouseEvent.MOUSE_OUT,this.__onMouseOut);
         this._challengeBtn.addEventListener(MouseEvent.CLICK,this.__onMouseClick);
      }
      
      public function setWaiting() : void
      {
         this._waiting = ComponentFactory.Instance.creat("ringStation.view.player.waiting");
         addChild(this._waiting);
      }
      
      public function updatePerson(param1:PlayerInfo) : void
      {
         this._playerInfo = param1;
         this._levelIcon.setInfo(param1.Grade,param1.Repute,param1.WinCount,param1.TotalCount,param1.FightPower,param1.Offer,true,false);
         this._nickNameText.htmlText = "<u><a href=\'event:marrytype:2541\'>" + param1.NickName + "</a></u>";
         if(param1.IsVIP)
         {
            this._vipName = VipController.instance.getVipNameTxt(104,param1.typeVIP);
            this._vipName.textSize = 16;
            this._vipName.x = this._nickNameText.x;
            this._vipName.y = this._nickNameText.y - 2;
            this._vipName.htmlText = "<u><a href=\'event:marrytype:2541\'>" + param1.NickName + "</a></u>";
            addChild(this._vipName);
         }
         PositionUtils.adaptNameStyle(param1,this._nickNameText,this._vipName);
         this._player = CharactoryFactory.createCharacter(param1,"room");
         this._player.showGun = true;
         this._player.show();
         this._player.setShowLight(true);
         PositionUtils.setPos(this._player,"ringStation.view.player.playerPos");
         this._playerSprite.addChild(this._player as DisplayObject);
         this._maskSprite = new Sprite();
         this._maskSprite.graphics.beginFill(16777215,1);
         this._maskSprite.graphics.drawRect(6,27,166,170);
         this._maskSprite.graphics.endFill();
         this._player.mask = this._maskSprite;
         addChild(this._maskSprite);
         addChild(this._challengeBtn);
         if(param1.Rank == 0)
         {
            this._rank.text = LanguageMgr.GetTranslation("ringStation.view.person.noRank");
         }
         else
         {
            this._rank.text = LanguageMgr.GetTranslation("ringStation.view.person.rankText",param1.Rank);
         }
         addChild(this._rank);
         this.addSignCell();
         var _loc2_:String = LanguageMgr.GetTranslation("ringstation.view.signNormal");
         if(param1.signMsg == "" || param1.signMsg == _loc2_)
         {
            this._signText.text = LanguageMgr.GetTranslation("ringstation.view.signNormal");
         }
         else
         {
            this._signText.text = param1.signMsg;
            if(this._signText.text.length > 15)
            {
               this._signText.text = this._signText.text.substr(0,15) + "...";
            }
         }
      }
      
      protected function __onMouseClick(param1:MouseEvent) : void
      {
         if(new Date().time - this._clickDate > 1000)
         {
            this._clickDate = new Date().time;
            SoundManager.instance.playButtonSound();
            RingStationManager.challenge = true;
            SocketManager.Instance.out.sendRingStationChallenge(this._playerInfo.ID,this._playerInfo.Rank);
         }
      }
      
      protected function __onMouseOver(param1:MouseEvent) : void
      {
         this._challengeBtn.visible = true;
      }
      
      protected function __onMouseOut(param1:MouseEvent) : void
      {
         this._challengeBtn.visible = false;
      }
      
      private function removeEvent() : void
      {
         removeEventListener(MouseEvent.MOUSE_OVER,this.__onMouseOver);
         removeEventListener(MouseEvent.MOUSE_OUT,this.__onMouseOut);
         this._challengeBtn.removeEventListener(MouseEvent.CLICK,this.__onMouseClick);
      }
      
      public function dispose() : void
      {
         this.removeEvent();
         if(this._bg)
         {
            this._bg.bitmapData.dispose();
            this._bg = null;
         }
         if(this._waiting)
         {
            this._waiting.bitmapData.dispose();
            this._waiting = null;
         }
         if(this._levelIcon)
         {
            this._levelIcon.dispose();
            this._levelIcon = null;
         }
         if(this._nickNameText)
         {
            this._nickNameText.dispose();
            this._nickNameText = null;
         }
         if(this._vipName)
         {
            this._vipName.dispose();
            this._vipName = null;
         }
         if(this._rank)
         {
            this._rank.dispose();
            this._rank = null;
         }
         if(this._player)
         {
            this._player.dispose();
            this._player = null;
         }
         if(this._challengeBtn)
         {
            this._challengeBtn.dispose();
            this._challengeBtn = null;
         }
         if(this._playerSprite)
         {
            ObjectUtils.disposeAllChildren(this._playerSprite);
            ObjectUtils.disposeObject(this._playerSprite);
            this._playerSprite = null;
         }
         if(this._signBG)
         {
            this._signBG.bitmapData.dispose();
            this._signBG = null;
         }
         if(this._signBG2)
         {
            this._signBG2.bitmapData.dispose();
            this._signBG2 = null;
         }
         if(this._signText)
         {
            this._signText.dispose();
            this._signText = null;
         }
      }
   }
}
