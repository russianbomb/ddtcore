package ringStation.view
{
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.ListPanel;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import ddt.manager.LanguageMgr;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import flash.display.Bitmap;
   import flash.events.MouseEvent;
   import labyrinth.data.RankingInfo;
   import ringStation.event.RingStationEvent;
   import road7th.comm.PackageIn;
   
   public class ArmoryView extends Frame
   {
       
      
      private var _bg:Bitmap;
      
      private var _closeBg:ScaleBitmapImage;
      
      private var _closeBtn:BaseButton;
      
      private var _list:ListPanel;
      
      public function ArmoryView()
      {
         super();
         this.initView();
         this.initEvent();
         this.sendPkg();
      }
      
      private function initView() : void
      {
         titleText = LanguageMgr.GetTranslation("ringStation.view.armoryView.titleInfo");
         this._bg = ComponentFactory.Instance.creat("ringStation.view.armory.bg");
         addToContent(this._bg);
         this._closeBg = ComponentFactory.Instance.creatComponentByStylename("ringStation.view.armory.closeBg");
         addToContent(this._closeBg);
         this._closeBtn = ComponentFactory.Instance.creatComponentByStylename("ringStation.view.armory.closeBtn");
         addToContent(this._closeBtn);
         this._list = ComponentFactory.Instance.creatComponentByStylename("ringStation.view.armory.List");
         addToContent(this._list);
      }
      
      private function sendPkg() : void
      {
         SocketManager.Instance.out.sendRingStationArmory();
      }
      
      private function initEvent() : void
      {
         addEventListener(FrameEvent.RESPONSE,this.__frameEventHandler);
         this._closeBtn.addEventListener(MouseEvent.CLICK,__onCloseClick);
         SocketManager.Instance.addEventListener(RingStationEvent.RINGSTATION_ARMORY,this.__getArmoryInfo);
      }
      
      protected function __getArmoryInfo(param1:RingStationEvent) : void
      {
         var _loc6_:RankingInfo = null;
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:Array = new Array();
         var _loc4_:int = _loc2_.readInt();
         var _loc5_:int = 0;
         while(_loc5_ < _loc4_)
         {
            _loc6_ = new RankingInfo();
            _loc6_.PlayerRank = _loc2_.readInt();
            _loc6_.FamLevel = _loc2_.readInt();
            _loc6_.PlayerName = _loc2_.readUTF();
            _loc6_.Fighting = _loc2_.readInt();
            _loc2_.readInt();
            _loc3_.push(_loc6_);
            _loc5_++;
         }
         this._list.vectorListModel.clear();
         this._list.vectorListModel.appendAll(_loc3_);
      }
      
      public function show() : void
      {
         LayerManager.Instance.addToLayer(this,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.ALPHA_BLOCKGOUND);
      }
      
      private function removeEvent() : void
      {
         removeEventListener(FrameEvent.RESPONSE,this.__frameEventHandler);
         this._closeBtn.removeEventListener(MouseEvent.CLICK,__onCloseClick);
         SocketManager.Instance.removeEventListener(RingStationEvent.RINGSTATION_ARMORY,this.__getArmoryInfo);
      }
      
      private function __frameEventHandler(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         switch(param1.responseCode)
         {
            case FrameEvent.ESC_CLICK:
            case FrameEvent.CLOSE_CLICK:
               this.dispose();
         }
      }
      
      override public function dispose() : void
      {
         super.dispose();
         this.removeEvent();
         if(this._bg)
         {
            this._bg.bitmapData.dispose();
            this._bg = null;
         }
         if(this._closeBg)
         {
            this._closeBg.dispose();
            this._closeBg = null;
         }
         if(this._closeBtn)
         {
            this._closeBtn.dispose();
            this._closeBtn = null;
         }
         if(this._list)
         {
            this._list.dispose();
            this._list = null;
         }
      }
   }
}
