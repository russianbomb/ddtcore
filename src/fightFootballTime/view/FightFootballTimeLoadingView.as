package fightFootballTime.view
{
   import com.greensock.TweenLite;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.utils.PositionUtils;
   import fightFootballTime.manager.FightFootballTimeManager;
   import flash.display.Bitmap;
   import flash.display.MovieClip;
   import flash.events.Event;
   import flash.events.TimerEvent;
   import flash.utils.Timer;
   import game.model.GameInfo;
   import room.events.RoomPlayerEvent;
   import room.model.RoomPlayer;
   import roomLoading.view.RoomLoadingView;
   
   public class FightFootballTimeLoadingView extends RoomLoadingView
   {
      
      private static const LEVEL_ICON_CLASSPATH:String = "asset.LevelIcon.Level_";
      
      public static const LOADING_FINISHED:String = "loadingFinished";
       
      
      private var playerMovie:MovieClip;
      
      private var vsMovie:MovieClip;
      
      private var redIcon:MovieClip;
      
      private var blueIcon:MovieClip;
      
      private var timerMovie:MovieClip;
      
      private var timer:Timer;
      
      private var perecentageTxtArr:Array;
      
      public function FightFootballTimeLoadingView(param1:GameInfo)
      {
         this.perecentageTxtArr = [null,null,null,null];
         FightFootballTimeManager.instance.isInLoading = true;
         super(param1);
      }
      
      private function initData() : void
      {
         var _loc4_:RoomPlayer = null;
         var _loc5_:int = 0;
         var _loc6_:String = null;
         var _loc7_:int = 0;
         var _loc1_:int = _gameInfo.roomPlayers.length;
         var _loc2_:Array = _gameInfo.roomPlayers;
         var _loc3_:int = 0;
         while(_loc3_ < _loc1_)
         {
            _loc4_ = _gameInfo.roomPlayers[_loc3_];
            _loc4_.position = _loc3_ + 1;
            _loc5_ = _loc4_.playerInfo.Sex == true?int(0):int(1);
            _loc6_ = _loc4_.playerInfo.NickName;
            _loc7_ = _loc4_.playerInfo.Grade;
            this.setPlayerRole(_loc4_.position,_loc5_);
            this.setLevelAndName(_loc4_,_loc6_,_loc7_);
            _loc4_.addEventListener(RoomPlayerEvent.PROGRESS_CHANGE,this.__onProgress);
            _loc3_++;
         }
      }
      
      protected function __onProgress(param1:RoomPlayerEvent) : void
      {
         var _perecentageTxt:FilterFrameText = null;
         var _okTxt:Bitmap = null;
         var event:RoomPlayerEvent = param1;
         var finishTxt:Function = function():void
         {
            _perecentageTxt.text = "100%";
            removeTxt();
         };
         var removeTxt:Function = function():void
         {
            if(_perecentageTxt)
            {
               _perecentageTxt.parent.removeChild(_perecentageTxt);
            }
         };
         var roomPlayer:RoomPlayer = event.currentTarget as RoomPlayer;
         var name:String = roomPlayer.playerInfo.NickName;
         _perecentageTxt = this.perecentageTxtArr[roomPlayer.position - 1];
         if(_perecentageTxt == null)
         {
            return;
         }
         _perecentageTxt.text = String(int(roomPlayer.progress)) + "%";
         if(roomPlayer.progress > 99)
         {
            _okTxt = ComponentFactory.Instance.creatBitmap("asset.roomLoading.LoadingOK");
            PositionUtils.setPos(_okTxt,"fightFootballTime.roomloading.oktxt" + roomPlayer.position);
            addChild(_okTxt);
            roomPlayer.removeEventListener(RoomPlayerEvent.PROGRESS_CHANGE,this.__onProgress);
            dispatchEvent(new Event(LOADING_FINISHED));
            finishTxt();
         }
      }
      
      override protected function init() : void
      {
         super.init();
         _bg = ComponentFactory.Instance.creatBitmap("fightFootballTime.roomloading.bg");
         this.playerMovie = ComponentFactory.Instance.creat("fightFootballTime.roomloading.playerMovie");
         PositionUtils.setPos(this.playerMovie,"fightFootballTime.rooloading.playerMoviePos");
         this.vsMovie = ComponentFactory.Instance.creat("fightFootballTime.roomloading.vsMovie");
         PositionUtils.setPos(this.vsMovie,"fightFootballTime.rooloading.vsMoviePos");
         this.redIcon = ComponentFactory.Instance.creat("fightFootballTime.roomloading.redIcon");
         PositionUtils.setPos(this.redIcon,"fightFootballTime.rooloading.redIconPos");
         this.blueIcon = ComponentFactory.Instance.creat("fightFootballTime.roomloading.blueIcon");
         PositionUtils.setPos(this.blueIcon,"fightFootballTime.rooloading.blueIconPos");
         this.timerMovie = ComponentFactory.Instance.creat("fightFootballTime.roomloading.timer");
         this.timerMovie.gotoAndStop(this.timerMovie.totalFrames);
         PositionUtils.setPos(this.timerMovie,"fightFootballTime.rooloading.timerMoviePos");
         this.timerMovie.alpha = 0;
         TweenLite.to(this.timerMovie,1,{
            "x":509,
            "y":388,
            "alpha":1,
            "onComplete":function():void
            {
               timer.start();
            }
         });
         this.timer = new Timer(1000,60);
         this.timer.addEventListener(TimerEvent.TIMER,this._timerCount);
         addChild(_bg);
         addChild(this.playerMovie);
         addChild(this.vsMovie);
         addChild(this.blueIcon);
         addChild(this.redIcon);
         addChild(this.timerMovie);
         addChild(_battleField);
         this.initData();
      }
      
      private function _timerCount(param1:TimerEvent) : void
      {
         var _loc2_:Timer = param1.currentTarget as Timer;
         if(this.timerMovie)
         {
            this.timerMovie.gotoAndStop(61 - _loc2_.currentCount);
         }
      }
      
      private function setPlayerRole(param1:int, param2:int) : void
      {
         if(param1 == 1 && param2 == 0)
         {
            this.playerMovie.player1.role.gotoAndStop("red_boy");
         }
         else if(param1 == 1 && param2 == 1)
         {
            this.playerMovie.player1.role.gotoAndStop("red_girl");
         }
         else if(param1 == 2 && param2 == 0)
         {
            this.playerMovie.player2.role.gotoAndStop("blue_boy");
         }
         else if(param1 == 2 && param2 == 1)
         {
            this.playerMovie.player2.role.gotoAndStop("blue_girl");
         }
         else if(param1 == 3 && param2 == 0)
         {
            this.playerMovie.player3.role.gotoAndStop("red_boy");
         }
         else if(param1 == 3 && param2 == 1)
         {
            this.playerMovie.player3.role.gotoAndStop("red_girl");
         }
         else if(param1 == 4 && param2 == 0)
         {
            this.playerMovie.player4.role.gotoAndStop("blue_boy");
         }
         else if(param1 == 4 && param2 == 1)
         {
            this.playerMovie.player4.role.gotoAndStop("blue_girl");
         }
      }
      
      private function setLevelAndName(param1:RoomPlayer, param2:String, param3:int) : void
      {
         var _loc4_:Bitmap = this.creatLevelBitmap(param3);
         var _loc5_:int = param1.position;
         var _loc6_:FilterFrameText = ComponentFactory.Instance.creatComponentByStylename("roomLoading.CharacterItemNameTxt");
         _loc6_.text = param2;
         var _loc7_:FilterFrameText = ComponentFactory.Instance.creatComponentByStylename("roomLoading.CharacterItemNameTxt");
         _loc7_.text = "0%";
         this.perecentageTxtArr[_loc5_ - 1] = _loc7_;
         if(_loc5_ == 1)
         {
            PositionUtils.setPos(_loc4_,"fightFootballTime.rooloading.levelIconPos1");
            PositionUtils.setPos(_loc6_,"fightFootballTime.rooloading.nameTxtPos1");
            PositionUtils.setPos(_loc7_,"fightFootballTime.rooloading.percentTxtPos1");
            this.playerMovie.player1.addChild(_loc4_);
            this.playerMovie.player1.addChild(_loc6_);
            this.playerMovie.player1.addChild(_loc7_);
         }
         else if(_loc5_ == 2)
         {
            PositionUtils.setPos(_loc4_,"fightFootballTime.rooloading.levelIconPos2");
            PositionUtils.setPos(_loc6_,"fightFootballTime.rooloading.nameTxtPos2");
            PositionUtils.setPos(_loc7_,"fightFootballTime.rooloading.percentTxtPos2");
            this.playerMovie.player2.addChild(_loc4_);
            this.playerMovie.player2.addChild(_loc6_);
            this.playerMovie.player2.addChild(_loc7_);
         }
         else if(_loc5_ == 3)
         {
            PositionUtils.setPos(_loc4_,"fightFootballTime.rooloading.levelIconPos3");
            PositionUtils.setPos(_loc6_,"fightFootballTime.rooloading.nameTxtPos3");
            PositionUtils.setPos(_loc7_,"fightFootballTime.rooloading.percentTxtPos3");
            this.playerMovie.player3.addChild(_loc4_);
            this.playerMovie.player3.addChild(_loc6_);
            this.playerMovie.player3.addChild(_loc7_);
         }
         else if(_loc5_ == 4)
         {
            PositionUtils.setPos(_loc4_,"fightFootballTime.rooloading.levelIconPos4");
            PositionUtils.setPos(_loc6_,"fightFootballTime.rooloading.nameTxtPos4");
            PositionUtils.setPos(_loc7_,"fightFootballTime.rooloading.percentTxtPos4");
            this.playerMovie.player4.addChild(_loc4_);
            this.playerMovie.player4.addChild(_loc6_);
            this.playerMovie.player4.addChild(_loc7_);
         }
      }
      
      private function creatLevelBitmap(param1:int) : Bitmap
      {
         var _loc2_:Bitmap = ComponentFactory.Instance.creatBitmap(LEVEL_ICON_CLASSPATH + param1.toString());
         _loc2_.smoothing = true;
         return _loc2_;
      }
      
      private function removeEvent() : void
      {
         if(this.timer)
         {
            this.timer.stop();
            this.timer.removeEventListener(TimerEvent.TIMER,this._timerCount);
            this.timer == null;
         }
      }
      
      override public function dispose() : void
      {
         FightFootballTimeManager.instance.isInLoading = false;
         super.dispose();
         if(_bg)
         {
            ObjectUtils.disposeObject(_bg);
         }
         _bg = null;
         if(this.playerMovie)
         {
            ObjectUtils.disposeObject(this.playerMovie);
         }
         this.playerMovie = null;
         if(this.vsMovie)
         {
            ObjectUtils.disposeObject(this.vsMovie);
         }
         this.vsMovie = null;
         if(this.redIcon)
         {
            ObjectUtils.disposeObject(this.redIcon);
         }
         this.redIcon = null;
         if(this.blueIcon)
         {
            ObjectUtils.disposeObject(this.blueIcon);
         }
         this.blueIcon = null;
         if(this.redIcon)
         {
            ObjectUtils.disposeObject(this.redIcon);
         }
         this.redIcon = null;
         if(this.timerMovie)
         {
            ObjectUtils.disposeObject(this.timerMovie);
         }
         this.timerMovie = null;
         if(parent)
         {
            parent.removeChild(this);
         }
      }
   }
}
