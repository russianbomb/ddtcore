package fightFootballTime.view
{
   import com.greensock.TweenLite;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.ui.text.GradientText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.utils.PositionUtils;
   import fightFootballTime.manager.FightFootballTimeManager;
   import flash.display.Bitmap;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.TimerEvent;
   import flash.utils.Timer;
   import game.GameManager;
   import game.model.LocalPlayer;
   import game.view.experience.ExpLeftView;
   
   public class FightFootballTimeExpLeftView extends ExpLeftView implements Disposeable
   {
       
      
      private var resultIcon:MovieClip;
      
      private var overInfoBg:Bitmap;
      
      private var myScoreBg:Bitmap;
      
      private var scoreInfoBg:Bitmap;
      
      private var exptxt:Bitmap;
      
      private var totaltxt:Bitmap;
      
      private var goldtxt:Bitmap;
      
      private var jiahao:Bitmap;
      
      private var fenge:Bitmap;
      
      private var fenge2:Bitmap;
      
      private var stateIcon:MovieClip;
      
      private var charater:MovieClip;
      
      private var scoretxt:GradientText;
      
      private var finalexptxt:GradientText;
      
      private var finalscore:int;
      
      private var finalexp:int;
      
      private var scoreBg:Bitmap;
      
      private var expBg:Bitmap;
      
      private var redTeamIcon:MovieClip;
      
      private var blueTeamIcon:MovieClip;
      
      private var redScore:FilterFrameText;
      
      private var blueScore:FilterFrameText;
      
      private var bifen:Bitmap;
      
      private var door1:MovieClip;
      
      private var door2:MovieClip;
      
      private var door3:MovieClip;
      
      private var door4:MovieClip;
      
      private var door5:MovieClip;
      
      private var time1:MovieClip;
      
      private var time2:MovieClip;
      
      private var time3:MovieClip;
      
      private var time4:MovieClip;
      
      private var time5:MovieClip;
      
      private var doorScore1:int = 0;
      
      private var doorScore2:int = 0;
      
      private var doorScore3:int = 0;
      
      private var doorScore4:int = 0;
      
      private var doorScore5:int = 0;
      
      private var upView:Sprite;
      
      private var downView:Sprite;
      
      public function FightFootballTimeExpLeftView()
      {
         super();
      }
      
      override protected function init() : void
      {
         var _loc1_:LocalPlayer = GameManager.Instance.Current.selfGamePlayer;
         var _loc2_:Array = _loc1_.selfInfo.scoreArr;
         this.getStateIcon();
         this.getUpView();
         this.getDownView();
         this.getScore(GameManager.Instance.Current.redScore,GameManager.Instance.Current.blueScore);
         this.getDoor(_loc2_);
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         while(_loc4_ < _loc2_.length)
         {
            _loc3_ = _loc3_ + _loc2_[_loc4_];
            _loc4_++;
         }
         var _loc5_:int = _loc3_ * 50;
         this.getScoreAndExp(_loc3_,_loc5_);
         addChild(this.stateIcon);
      }
      
      private function getCharater(param1:int, param2:int) : void
      {
         if(param1 == 0 && param2 == 0)
         {
            this.charater.gotoAndStop("blue_boy");
            this.charater.scaleX = -1;
            PositionUtils.setPos(this.charater,"fightFootballTime.expView.charaterpos");
         }
         else if(param1 == 0 && param2 == 1)
         {
            this.charater.gotoAndStop("blue_girl");
            this.charater.scaleX = -1;
            PositionUtils.setPos(this.charater,"fightFootballTime.expView.charaterpos");
         }
         else if(param1 == 1 && param2 == 0)
         {
            this.charater.gotoAndStop("red_boy");
            this.charater.scaleX = -1;
            PositionUtils.setPos(this.charater,"fightFootballTime.expView.charaterpos");
         }
         else if(param1 == 1 && param2 == 1)
         {
            this.charater.gotoAndStop("red_gril");
            this.charater.scaleX = -1;
            PositionUtils.setPos(this.charater,"fightFootballTime.expView.charaterpos");
         }
      }
      
      private function getScore(param1:int, param2:int) : void
      {
         this.redTeamIcon = ComponentFactory.Instance.creat("fightFootballTime.expView.teamIcon");
         this.blueTeamIcon = ComponentFactory.Instance.creat("fightFootballTime.expView.teamIcon");
         this.redTeamIcon.gotoAndStop("red_small");
         this.blueTeamIcon.gotoAndStop("blue_small");
         this.redScore = ComponentFactory.Instance.creatComponentByStylename("fightFootballTime.expView.RedScoreTxt");
         this.blueScore = ComponentFactory.Instance.creatComponentByStylename("fightFootballTime.expView.BlueScoreTxt");
         this.bifen = ComponentFactory.Instance.creatBitmap("fightFootballTime.expView.bihao");
         var _loc3_:int = GameManager.Instance.Current.selfGamePlayer.team;
         if(_loc3_ == 1)
         {
            PositionUtils.setPos(this.blueTeamIcon,"fightFootballTime.expView.redTeamIconpos1");
            PositionUtils.setPos(this.redTeamIcon,"fightFootballTime.expView.blueTeamIconpos1");
            PositionUtils.setPos(this.blueScore,"fightFootballTime.expView.redScorepos1");
            PositionUtils.setPos(this.redScore,"fightFootballTime.expView.blueScorepos1");
            PositionUtils.setPos(this.bifen,"fightFootballTime.expView.bihaopos1");
         }
         else if(_loc3_ == 2)
         {
            PositionUtils.setPos(this.redTeamIcon,"fightFootballTime.expView.redTeamIconpos1");
            PositionUtils.setPos(this.blueTeamIcon,"fightFootballTime.expView.blueTeamIconpos1");
            PositionUtils.setPos(this.redScore,"fightFootballTime.expView.redScorepos1");
            PositionUtils.setPos(this.blueScore,"fightFootballTime.expView.blueScorepos1");
            PositionUtils.setPos(this.bifen,"fightFootballTime.expView.bihaopos1");
         }
         this.redScore.text = param1 + "";
         this.blueScore.text = param2 + "";
         this.upView.addChild(this.redTeamIcon);
         this.upView.addChild(this.blueTeamIcon);
         this.upView.addChild(this.redScore);
         this.upView.addChild(this.blueScore);
         this.upView.addChild(this.bifen);
      }
      
      private function getDoor(param1:Array) : void
      {
         var _loc3_:Bitmap = null;
         this.door1 = ComponentFactory.Instance.creat("fightFootballTime.expView.door");
         this.door2 = ComponentFactory.Instance.creat("fightFootballTime.expView.door");
         this.door3 = ComponentFactory.Instance.creat("fightFootballTime.expView.door");
         this.door4 = ComponentFactory.Instance.creat("fightFootballTime.expView.door");
         this.door5 = ComponentFactory.Instance.creat("fightFootballTime.expView.door");
         this.time1 = ComponentFactory.Instance.creat("fightFootballTime.expView.cishu");
         this.time2 = ComponentFactory.Instance.creat("fightFootballTime.expView.cishu");
         this.time3 = ComponentFactory.Instance.creat("fightFootballTime.expView.cishu");
         this.time4 = ComponentFactory.Instance.creat("fightFootballTime.expView.cishu");
         this.time5 = ComponentFactory.Instance.creat("fightFootballTime.expView.cishu");
         this.door1.gotoAndStop(1);
         this.door2.gotoAndStop(2);
         this.door3.gotoAndStop(3);
         this.door4.gotoAndStop(4);
         this.door5.gotoAndStop(5);
         PositionUtils.setPos(this.door1,"fightFootballTime.expView.doorpos1");
         PositionUtils.setPos(this.door2,"fightFootballTime.expView.doorpos2");
         PositionUtils.setPos(this.door3,"fightFootballTime.expView.doorpos3");
         PositionUtils.setPos(this.door4,"fightFootballTime.expView.doorpos4");
         PositionUtils.setPos(this.door5,"fightFootballTime.expView.doorpos5");
         var _loc2_:int = 0;
         while(_loc2_ < param1.length)
         {
            if(param1[_loc2_] != 0)
            {
               if(param1[_loc2_] == 1)
               {
                  this.doorScore1++;
               }
               else if(param1[_loc2_] == 2)
               {
                  this.doorScore2++;
               }
               else if(param1[_loc2_] == 3)
               {
                  this.doorScore3++;
               }
               else if(param1[_loc2_] == 4)
               {
                  this.doorScore4++;
               }
               else if(param1[_loc2_] == 5)
               {
                  this.doorScore5++;
               }
            }
            _loc2_++;
         }
         this.time1.gotoAndStop(this.doorScore1 + 1);
         this.time2.gotoAndStop(this.doorScore2 + 1);
         this.time3.gotoAndStop(this.doorScore3 + 1);
         this.time4.gotoAndStop(this.doorScore4 + 1);
         this.time5.gotoAndStop(this.doorScore5 + 1);
         PositionUtils.setPos(this.time1,"fightFootballTime.expView.timepos1");
         PositionUtils.setPos(this.time2,"fightFootballTime.expView.timepos2");
         PositionUtils.setPos(this.time3,"fightFootballTime.expView.timepos3");
         PositionUtils.setPos(this.time4,"fightFootballTime.expView.timepos4");
         PositionUtils.setPos(this.time5,"fightFootballTime.expView.timepos5");
         if(this.doorScore1 == 0 && this.doorScore2 == 0 && this.doorScore3 == 0 && this.doorScore4 == 0 && this.doorScore5 == 0)
         {
            _loc3_ = ComponentFactory.Instance.creatBitmap("fightFootballTime.expView.noScoreTxt");
            this.upView.addChild(_loc3_);
         }
         else
         {
            this.upView.addChild(this.door1);
            this.upView.addChild(this.door2);
            this.upView.addChild(this.door3);
            this.upView.addChild(this.door4);
            this.upView.addChild(this.door5);
            this.upView.addChild(this.time1);
            this.upView.addChild(this.time2);
            this.upView.addChild(this.time3);
            this.upView.addChild(this.time4);
            this.upView.addChild(this.time5);
         }
      }
      
      private function getUpView() : void
      {
         this.upView = new Sprite();
         this.myScoreBg = ComponentFactory.Instance.creatBitmap("fightFootballTime.expView.myScoreBg");
         this.scoreInfoBg = ComponentFactory.Instance.creatBitmap("fightFootballTime.expView.scoreInfoBg");
         this.fenge = ComponentFactory.Instance.creatBitmap("fightFootballTime.expView.fenge");
         this.fenge2 = ComponentFactory.Instance.creatBitmap("fightFootballTime.expView.fenge");
         PositionUtils.setPos(this.fenge2,"fightFootballTime.expView.fengepos");
         this.charater = ComponentFactory.Instance.creat("fightFootballTime.expView.charater");
         var _loc1_:int = GameManager.Instance.Current.selfGamePlayer.team;
         var _loc2_:int = GameManager.Instance.Current.selfGamePlayer.playerInfo.SexByInt;
         this.getCharater(_loc1_ - 1,_loc2_ - 1);
         this.upView.addChild(this.scoreInfoBg);
         this.upView.addChild(this.myScoreBg);
         this.upView.addChild(this.fenge);
         this.upView.addChild(this.fenge2);
         this.upView.addChild(this.charater);
         addChild(this.upView);
         this.upView.alpha = 0;
         this.upView.x = -100;
         TweenLite.to(this.upView,1,{
            "x":0,
            "alpha":1
         });
      }
      
      private function getScoreAndExp(param1:int, param2:int) : void
      {
         this.finalscore = param1;
         this.finalexp = param2;
         this.scoretxt = ComponentFactory.Instance.creatComponentByStylename("fightFootballTime.expView.finalScoreTxt");
         this.finalexptxt = ComponentFactory.Instance.creatComponentByStylename("fightFootballTime.expView.finalScoreTxt");
         PositionUtils.setPos(this.scoretxt,"fightFootballTime.expView.scoretxtpos");
         PositionUtils.setPos(this.finalexptxt,"fightFootballTime.expView.exptxtpos");
         this.scoretxt.text = 0 + "";
         this.finalexptxt.text = 0 + "";
         this.downView.addChild(this.scoretxt);
         this.downView.addChild(this.finalexptxt);
         var _loc3_:Timer = new Timer(1,0);
         _loc3_.addEventListener(TimerEvent.TIMER,this._increaceScore);
         _loc3_.start();
      }
      
      private function _increaceScore(param1:TimerEvent) : void
      {
         var _loc2_:Timer = param1.currentTarget as Timer;
         var _loc3_:int = _loc2_.currentCount * 10;
         if(_loc3_ < this.finalscore)
         {
            this.scoretxt.text = _loc3_ + "";
         }
         else
         {
            this.scoretxt.text = this.finalscore + "";
         }
         if(_loc3_ < this.finalexp)
         {
            this.finalexptxt.text = _loc3_ + "";
         }
         else
         {
            this.finalexptxt.text = this.finalexp + "";
         }
         if(this.finalexptxt.text == this.finalexp + "" && this.scoretxt.text == this.finalscore + "")
         {
            _loc2_.stop();
            _loc2_.removeEventListener(TimerEvent.TIMER,this._increaceScore);
            _loc2_ = null;
         }
      }
      
      private function getDownView() : void
      {
         this.downView = new Sprite();
         this.overInfoBg = ComponentFactory.Instance.creatBitmap("fightFootballTime.expView.overInfoBg");
         this.totaltxt = ComponentFactory.Instance.creatBitmap("fightFootballTime.expView.totaltxt");
         this.goldtxt = ComponentFactory.Instance.creatBitmap("fightFootballTime.expView.goldtxt");
         this.exptxt = ComponentFactory.Instance.creatBitmap("fightFootballTime.expView.exptxt");
         this.jiahao = ComponentFactory.Instance.creatBitmap("fightFootballTime.expView.jiahao");
         this.scoreBg = ComponentFactory.Instance.creatBitmap("fightFootballTime.expView.scoreTxtBg");
         this.expBg = ComponentFactory.Instance.creatBitmap("fightFootballTime.expView.expTxtBg");
         this.downView.addChild(this.overInfoBg);
         this.downView.addChild(this.totaltxt);
         this.downView.addChild(this.goldtxt);
         this.downView.addChild(this.exptxt);
         this.downView.addChild(this.jiahao);
         this.downView.addChild(this.scoreBg);
         this.downView.addChild(this.expBg);
         addChild(this.downView);
         this.downView.alpha = 0;
         this.downView.x = -100;
         TweenLite.to(this.downView,1.5,{
            "x":0,
            "alpha":1
         });
      }
      
      private function getStateIcon() : void
      {
         if(GameManager.Instance.Current.selfGamePlayer.isWin)
         {
            this.stateIcon = ComponentFactory.Instance.creat("fightFootballTime.expView.stateIconWin");
            PositionUtils.setPos(this.stateIcon,"fightFootballTime.expView.stateIconWinpos");
         }
         else if(!GameManager.Instance.Current.selfGamePlayer.isWin)
         {
            if(GameManager.Instance.Current.redScore == GameManager.Instance.Current.blueScore && GameManager.Instance.Current.roomType == FightFootballTimeManager.FIGHTFOOTBALLTIME_ROOM)
            {
               this.stateIcon = ComponentFactory.Instance.creat("fightFootballTime.expView.stateIconPing");
               PositionUtils.setPos(this.stateIcon,"fightFootballTime.expView.stateIconPingpos");
            }
            else
            {
               this.stateIcon = ComponentFactory.Instance.creat("fightFootballTime.expView.stateIconFail");
               PositionUtils.setPos(this.stateIcon,"fightFootballTime.expView.stateIconFailpos");
            }
         }
      }
      
      override public function dispose() : void
      {
         if(this.resultIcon)
         {
            ObjectUtils.disposeObject(this.resultIcon);
         }
         this.resultIcon = null;
         if(this.overInfoBg)
         {
            ObjectUtils.disposeObject(this.overInfoBg);
         }
         this.overInfoBg = null;
         if(this.myScoreBg)
         {
            ObjectUtils.disposeObject(this.myScoreBg);
         }
         this.myScoreBg = null;
         if(this.scoreInfoBg)
         {
            ObjectUtils.disposeObject(this.scoreInfoBg);
         }
         this.scoreInfoBg = null;
         if(this.exptxt)
         {
            ObjectUtils.disposeObject(this.exptxt);
         }
         this.exptxt = null;
         if(this.totaltxt)
         {
            ObjectUtils.disposeObject(this.totaltxt);
         }
         this.totaltxt = null;
         if(this.goldtxt)
         {
            ObjectUtils.disposeObject(this.goldtxt);
         }
         this.goldtxt = null;
         if(this.jiahao)
         {
            ObjectUtils.disposeObject(this.jiahao);
         }
         this.jiahao = null;
         if(this.fenge)
         {
            ObjectUtils.disposeObject(this.fenge);
         }
         this.fenge = null;
         if(this.fenge2)
         {
            ObjectUtils.disposeObject(this.fenge2);
         }
         this.fenge2 = null;
         if(this.stateIcon)
         {
            ObjectUtils.disposeObject(this.stateIcon);
         }
         this.stateIcon = null;
         if(this.charater)
         {
            ObjectUtils.disposeObject(this.charater);
         }
         this.charater = null;
         if(this.scoretxt)
         {
            ObjectUtils.disposeObject(this.scoretxt);
         }
         this.scoretxt = null;
         if(this.finalexptxt)
         {
            ObjectUtils.disposeObject(this.finalexptxt);
         }
         this.finalexptxt = null;
         if(this.scoreBg)
         {
            ObjectUtils.disposeObject(this.scoreBg);
         }
         this.scoreBg = null;
         if(this.expBg)
         {
            ObjectUtils.disposeObject(this.expBg);
         }
         this.expBg = null;
         if(this.redTeamIcon)
         {
            ObjectUtils.disposeObject(this.redTeamIcon);
         }
         this.redTeamIcon = null;
         if(this.blueTeamIcon)
         {
            ObjectUtils.disposeObject(this.blueTeamIcon);
         }
         this.blueTeamIcon = null;
         if(this.redScore)
         {
            ObjectUtils.disposeObject(this.redScore);
         }
         this.redScore = null;
         if(this.blueScore)
         {
            ObjectUtils.disposeObject(this.blueScore);
         }
         this.blueScore = null;
         if(this.bifen)
         {
            ObjectUtils.disposeObject(this.bifen);
         }
         this.bifen = null;
         if(this.door1)
         {
            ObjectUtils.disposeObject(this.door1);
         }
         this.door1 = null;
         if(this.door2)
         {
            ObjectUtils.disposeObject(this.door2);
         }
         this.door2 = null;
         if(this.time1)
         {
            ObjectUtils.disposeObject(this.time1);
         }
         this.time1 = null;
         if(this.time2)
         {
            ObjectUtils.disposeObject(this.time2);
         }
         this.time2 = null;
         if(this.upView)
         {
            ObjectUtils.disposeObject(this.upView);
         }
         this.upView = null;
         if(this.downView)
         {
            ObjectUtils.disposeObject(this.downView);
         }
         this.downView = null;
         if(this.parent)
         {
            this.parent.removeChild(this);
         }
      }
   }
}
