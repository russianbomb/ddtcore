package gotopage.view
{
   import bagAndInfo.BagAndInfoManager;
   import battleGroud.BattleGroudManager;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.ComponentSetting;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.controls.container.SimpleTileList;
   import com.pickgliss.ui.image.MutipleImage;
   import com.pickgliss.ui.image.Scale9CornerImage;
   import com.pickgliss.ui.vo.AlertInfo;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.dailyRecord.DailyRecordControl;
   import ddt.manager.AcademyManager;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.manager.StateManager;
   import ddt.states.StateType;
   import flash.events.MouseEvent;
   import kingBless.KingBlessManager;
   import league.manager.LeagueManager;
   import room.RoomManager;
   import setting.controll.SettingController;
   import trainer.controller.WeakGuildManager;
   import trainer.data.Step;
   import vip.VipController;
   
   public class GotoPageView extends BaseAlerFrame
   {
       
      
      private var _btnList:Vector.<SimpleBitmapButton>;
      
      private var _btnListContainer:SimpleTileList;
      
      private var _bg:Scale9CornerImage;
      
      private var _VIPBtn:SimpleBitmapButton;
      
      private var _kingBlessBtn:SimpleBitmapButton;
      
      private var _battleBtn:SimpleBitmapButton;
      
      private var _battleShopBtn:SimpleBitmapButton;
      
      private var _boguShopBtn:SimpleBitmapButton;
      
      private var _templeBtn:SimpleBitmapButton;
      
      private var _friendBtn:SimpleBitmapButton;
      
      private var _dailyBtn:SimpleBitmapButton;
      
      private var _setBtn:SimpleBitmapButton;
      
      private var _giftBoxBtn:SimpleBitmapButton;
      
      private var _vline:MutipleImage;
      
      private var _hline:MutipleImage;
      
      private var _event:MouseEvent;
      
      public function GotoPageView()
      {
         super();
         this.initView();
      }
      
      private function initView() : void
      {
         info = new AlertInfo(LanguageMgr.GetTranslation("tank.view.ChannelList.FastMenu.titleText"));
         _info.showSubmit = false;
         _info.showCancel = false;
         _info.moveEnable = false;
         this._bg = ComponentFactory.Instance.creatComponentByStylename("gotopage.GotoPageView.bg");
         addToContent(this._bg);
         this._VIPBtn = ComponentFactory.Instance.creatComponentByStylename("gotopage.VIPBtn");
         addToContent(this._VIPBtn);
         this._kingBlessBtn = ComponentFactory.Instance.creatComponentByStylename("gotopage.kingBlessBtn");
         addToContent(this._kingBlessBtn);
         this._battleShopBtn = ComponentFactory.Instance.creatComponentByStylename("gotopage.battleShopBtn");
         addToContent(this._battleShopBtn);
         this._boguShopBtn = ComponentFactory.Instance.creatComponentByStylename("gotopage.boguShopBtn");
         this._templeBtn = ComponentFactory.Instance.creatComponentByStylename("gotopage.templeBtn");
         addToContent(this._templeBtn);
         this._friendBtn = ComponentFactory.Instance.creatComponentByStylename("gotopage.friendBtn");
         addToContent(this._friendBtn);
         this._dailyBtn = ComponentFactory.Instance.creatComponentByStylename("gotopage.dailyBtn");
         addToContent(this._dailyBtn);
         this._setBtn = ComponentFactory.Instance.creatComponentByStylename("gotopage.setBtn");
         addToContent(this._setBtn);
         this._giftBoxBtn = ComponentFactory.Instance.creatComponentByStylename("gotopage.giftBoxBtn");
         addToContent(this._giftBoxBtn);
         this._battleBtn = ComponentFactory.Instance.creatComponentByStylename("gotopage.battleBtn");
         addToContent(this._battleBtn);
         this._vline = ComponentFactory.Instance.creatComponentByStylename("gotopage.vline");
         addToContent(this._vline);
         this._hline = ComponentFactory.Instance.creatComponentByStylename("gotopage.hline");
         addToContent(this._hline);
         this._btnList = new Vector.<SimpleBitmapButton>();
         this._btnList.push(this._VIPBtn);
         this._btnList.push(this._kingBlessBtn);
         this._btnList.push(this._battleBtn);
         this._btnList.push(this._battleShopBtn);
         this._btnList.push(this._boguShopBtn);
         this._btnList.push(this._templeBtn);
         this._btnList.push(this._friendBtn);
         this._btnList.push(this._dailyBtn);
         this._btnList.push(this._giftBoxBtn);
         this._btnList.push(this._setBtn);
         this.creatBtn();
      }
      
      private function creatBtn() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ < this._btnList.length)
         {
            this._btnList[_loc1_].addEventListener(MouseEvent.CLICK,this.__clickHandle);
            _loc1_++;
         }
      }
      
      private function clearBtn() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ < this._btnList.length)
         {
            if(this._btnList[_loc1_])
            {
               this._btnList[_loc1_].removeEventListener(MouseEvent.CLICK,this.__clickHandle);
               ObjectUtils.disposeObject(this._btnList[_loc1_]);
            }
            this._btnList[_loc1_] = null;
            _loc1_++;
         }
      }
      
      private function __clickHandle(param1:MouseEvent) : void
      {
         param1.stopImmediatePropagation();
         this._event = param1;
         SoundManager.instance.play("047");
         if(param1.currentTarget != this._dailyBtn && param1.currentTarget != this._setBtn && param1.currentTarget != this._VIPBtn && param1.currentTarget != this._battleBtn && RoomManager.Instance.current != null && RoomManager.Instance.current.selfRoomPlayer != null)
         {
            if((StateManager.currentStateType == StateType.MISSION_ROOM || RoomManager.Instance.current.isOpenBoss) && !RoomManager.Instance.current.selfRoomPlayer.isViewer)
            {
               this.showAlert();
               return;
            }
         }
         this.skipView(param1);
      }
      
      private function showAlert() : void
      {
         var _loc1_:BaseAlerFrame = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("tank.missionsettle.dungeon.leaveConfirm.contents"),"",LanguageMgr.GetTranslation("cancel"),true,true,false,LayerManager.BLCAK_BLOCKGOUND);
         _loc1_.moveEnable = false;
         _loc1_.addEventListener(FrameEvent.RESPONSE,this.__onResponse);
      }
      
      private function __onResponse(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         var _loc2_:BaseAlerFrame = param1.target as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.__onResponse);
         _loc2_.dispose();
         _loc2_ = null;
         if(param1.responseCode == FrameEvent.ENTER_CLICK || param1.responseCode == FrameEvent.SUBMIT_CLICK)
         {
            this.skipView(this._event);
         }
         else
         {
            this.dispose();
            dispatchEvent(new FrameEvent(FrameEvent.CLOSE_CLICK));
         }
      }
      
      private function skipView(param1:MouseEvent) : void
      {
         var _loc3_:Boolean = false;
         var _loc2_:int = this._btnList.indexOf(param1.currentTarget as SimpleBitmapButton);
         switch(_loc2_)
         {
            case 0:
               VipController.instance.show();
               break;
            case 1:
               KingBlessManager.instance.loadKingBlessModule(KingBlessManager.instance.doOpenKingBlessFrame);
               break;
            case 2:
               if(!WeakGuildManager.Instance.checkOpen(Step.TOFF_LIST_OPEN,16))
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.functionLimitTip",16));
                  return;
               }
               BattleGroudManager.Instance.initBattleView();
               break;
            case 3:
               LeagueManager.instance.showLeagueShopFrame();
               break;
            case 4:
               StateManager.setState(StateType.LITTLEHALL);
               break;
            case 5:
               if(PlayerManager.Instance.Self.Grade < AcademyManager.TARGET_PLAYER_MIN_LEVEL)
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.functionLimitTip",6));
                  return;
               }
               if(PlayerManager.Instance.Self.apprenticeshipState == AcademyManager.NONE_STATE)
               {
                  StateManager.setState(StateType.ACADEMY_REGISTRATION);
               }
               else
               {
                  StateManager.setState(StateType.ACADEMY_REGISTRATION);
               }
               ComponentSetting.SEND_USELOG_ID(119);
               break;
            case 6:
               if(!WeakGuildManager.Instance.checkOpen(Step.CIVIL_OPEN,5))
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.functionLimitTip",5));
                  return;
               }
               _loc3_ = false;
               if(PlayerManager.Instance.Self.IsWeakGuildFinish(Step.CIVIL_OPEN) && !PlayerManager.Instance.Self.IsWeakGuildFinish(Step.CIVIL_CLICKED))
               {
                  SocketManager.Instance.out.syncWeakStep(Step.CIVIL_CLICKED);
                  _loc3_ = true;
               }
               StateManager.setState(StateType.CIVIL,_loc3_);
               ComponentSetting.SEND_USELOG_ID(10);
               break;
            case 7:
               DailyRecordControl.Instance.alertDailyFrame();
               break;
            case 8:
               if(PlayerManager.Instance.Self.Grade < 16)
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.functionLimitTip",16));
                  return;
               }
               BagAndInfoManager.Instance.showGiftFrame();
               break;
            case 9:
               SettingController.Instance.switchVisible();
         }
         dispatchEvent(new FrameEvent(FrameEvent.CLOSE_CLICK));
      }
      
      override public function dispose() : void
      {
         GotoPageController.Instance.isShow = false;
         if(this._bg)
         {
            ObjectUtils.disposeObject(this._bg);
         }
         this._bg = null;
         if(this._VIPBtn)
         {
            ObjectUtils.disposeObject(this._VIPBtn);
         }
         this._VIPBtn = null;
         if(this._kingBlessBtn)
         {
            ObjectUtils.disposeObject(this._kingBlessBtn);
         }
         this._kingBlessBtn = null;
         if(this._battleBtn)
         {
            ObjectUtils.disposeObject(this._battleBtn);
         }
         this._battleBtn = null;
         if(this._battleShopBtn)
         {
            ObjectUtils.disposeObject(this._battleShopBtn);
         }
         this._battleShopBtn = null;
         if(this._templeBtn)
         {
            ObjectUtils.disposeObject(this._templeBtn);
         }
         this._templeBtn = null;
         if(this._boguShopBtn)
         {
            ObjectUtils.disposeObject(this._boguShopBtn);
         }
         this._boguShopBtn = null;
         if(this._friendBtn)
         {
            ObjectUtils.disposeObject(this._friendBtn);
         }
         this._friendBtn = null;
         if(this._dailyBtn)
         {
            ObjectUtils.disposeObject(this._dailyBtn);
         }
         this._dailyBtn = null;
         if(this._setBtn)
         {
            ObjectUtils.disposeObject(this._setBtn);
         }
         this._setBtn = null;
         if(this._giftBoxBtn)
         {
            ObjectUtils.disposeObject(this._giftBoxBtn);
         }
         this._giftBoxBtn = null;
         if(this._vline)
         {
            ObjectUtils.disposeObject(this._vline);
         }
         this._vline = null;
         if(this._hline)
         {
            ObjectUtils.disposeObject(this._hline);
         }
         this._hline = null;
         if(this._btnList)
         {
            this.clearBtn();
         }
         ObjectUtils.disposeObject(this._btnListContainer);
         this._btnList = null;
         if(parent)
         {
            parent.removeChild(this);
         }
         super.dispose();
      }
   }
}
