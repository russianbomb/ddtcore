package godsRoads.manager
{
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.DataAnalyzer;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import ddt.data.UIModuleTypes;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.view.UIModuleSmallLoading;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.events.MouseEvent;
   import godsRoads.analyze.GodsRoadsDataAnalyzer;
   import godsRoads.data.GodsRoadsAwards;
   import godsRoads.data.GodsRoadsMissionVo;
   import godsRoads.data.GodsRoadsPkgType;
   import godsRoads.data.GodsRoadsStepVo;
   import godsRoads.data.GodsRoadsVo;
   import godsRoads.model.GodsRoadsModel;
   import godsRoads.view.GodsRoadsView;
   import hallIcon.HallIconManager;
   import hallIcon.HallIconType;
   import road7th.comm.PackageIn;
   
   public class GodsRoadsManager extends EventDispatcher
   {
      
      private static var _instance:GodsRoadsManager;
       
      
      public var _model:GodsRoadsModel;
      
      private var _isOpen:Boolean = false;
      
      public var _view:GodsRoadsView;
      
      private var _func:Function;
      
      private var _xml:XML;
      
      private var _funcParams:Array;
      
      public var lastStep:int = 0;
      
      public var lastMssion:int = 0;
      
      public function GodsRoadsManager(param1:PrivateClass)
      {
         this._funcParams = [];
         super();
         this._model = new GodsRoadsModel();
      }
      
      public static function get instance() : GodsRoadsManager
      {
         if(GodsRoadsManager._instance == null)
         {
            GodsRoadsManager._instance = new GodsRoadsManager(new PrivateClass());
         }
         return GodsRoadsManager._instance;
      }
      
      public function setup() : void
      {
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.GODS_ROADS,this.pkgHandler);
      }
      
      public function loadGodsRoadsModule(param1:Function = null, param2:Array = null) : void
      {
         this._func = param1;
         this._funcParams = param2;
         UIModuleSmallLoading.Instance.progress = 0;
         UIModuleSmallLoading.Instance.show();
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadCompleteHandler);
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUimoduleLoadProgress);
         UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.GODSROADS);
      }
      
      private function loadCompleteHandler(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.GODSROADS)
         {
            UIModuleSmallLoading.Instance.hide();
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadCompleteHandler);
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUimoduleLoadProgress);
            if(null != this._func)
            {
               this._func.apply(null,this._funcParams);
            }
            this._func = null;
            this._funcParams = null;
         }
      }
      
      private function onUimoduleLoadProgress(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.GODSROADS)
         {
            UIModuleSmallLoading.Instance.progress = param1.loader.progress * 100;
         }
      }
      
      private function pkgHandler(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = null;
         var _loc4_:Boolean = false;
         _loc2_ = param1.pkg;
         var _loc3_:int = param1._cmd;
         switch(_loc3_)
         {
            case GodsRoadsPkgType.GODS_ROADS_OPEN:
               this._isOpen = _loc2_.readBoolean();
               if(this._isOpen)
               {
                  this.showGodsRoads();
               }
               else
               {
                  this.hideGodsRoadsIcon();
               }
               break;
            case GodsRoadsPkgType.ENTER_GODS_ROADS:
               this.doOpenGodsRoads(_loc2_);
               break;
            case GodsRoadsPkgType.GET_AWARDS:
               _loc4_ = _loc2_.readBoolean();
               if(_loc4_)
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("ddt.godsRoads.getawards"));
               }
               this.sendEnter();
         }
      }
      
      public function changeSteps(param1:int) : void
      {
         this._view.changeSteps(param1);
      }
      
      public function templateDataSetup(param1:DataAnalyzer) : void
      {
         if(param1 is GodsRoadsDataAnalyzer)
         {
            this._model.missionInfo = GodsRoadsDataAnalyzer(param1).dataList;
         }
         dispatchEvent(new Event("XMLdata_Complete"));
      }
      
      public function showGodsRoads() : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.GODSROADS,true);
      }
      
      private function hideGodsRoadsIcon() : void
      {
         HallIconManager.instance.updateSwitchHandler(HallIconType.GODSROADS,false);
      }
      
      public function openGodsRoads(param1:MouseEvent) : void
      {
         SoundManager.instance.playButtonSound();
         this.loadGodsRoadsModule(this.sendEnter);
      }
      
      private function sendEnter() : void
      {
         SocketManager.Instance.out.enterGodsRoads();
      }
      
      private function doOpenGodsRoads(param1:PackageIn) : void
      {
         var _loc4_:GodsRoadsStepVo = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:GodsRoadsMissionVo = null;
         var _loc8_:int = 0;
         var _loc9_:GodsRoadsAwards = null;
         var _loc10_:GodsRoadsAwards = null;
         this._model.godsRoadsData = new GodsRoadsVo();
         var _loc2_:GodsRoadsVo = this._model.godsRoadsData;
         _loc2_.Level = param1.readInt();
         _loc2_.currentLevel = param1.readInt();
         _loc2_.steps = new Vector.<GodsRoadsStepVo>();
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_.Level)
         {
            _loc4_ = new GodsRoadsStepVo();
            _loc4_.isGetAwards = param1.readBoolean();
            _loc4_.missionsNum = param1.readInt();
            _loc4_.currentStep = _loc3_ + 1;
            _loc4_.awards = [];
            _loc4_.missionVos = [];
            _loc5_ = 0;
            while(_loc5_ < _loc4_.missionsNum)
            {
               _loc7_ = new GodsRoadsMissionVo();
               _loc7_.ID = param1.readInt();
               _loc7_.isFinished = param1.readBoolean();
               _loc7_.condition1 = param1.readInt();
               _loc7_.condition2 = param1.readInt();
               _loc7_.condition3 = param1.readInt();
               _loc7_.condition4 = param1.readInt();
               _loc7_.isGetAwards = param1.readBoolean();
               _loc7_.awardsNum = param1.readInt();
               _loc7_.awards = [];
               _loc8_ = 0;
               while(_loc8_ < _loc7_.awardsNum)
               {
                  _loc9_ = new GodsRoadsAwards();
                  _loc9_.TemplateID = param1.readInt();
                  _loc9_.StrengthLevel = param1.readInt();
                  _loc9_.Count = param1.readInt();
                  _loc9_.ValidDate = param1.readInt();
                  _loc9_.AttackCompose = param1.readInt();
                  _loc9_.DefendCompose = param1.readInt();
                  _loc9_.AgilityCompose = param1.readInt();
                  _loc9_.LuckCompose = param1.readInt();
                  _loc9_.IsBind = param1.readBoolean();
                  _loc7_.awards.push(_loc9_);
                  _loc8_++;
               }
               _loc4_.missionVos.push(_loc7_);
               _loc5_++;
            }
            _loc2_.steps.push(_loc4_);
            _loc4_.awadsNum = param1.readInt();
            _loc6_ = 0;
            while(_loc6_ < _loc4_.awadsNum)
            {
               _loc10_ = new GodsRoadsAwards();
               _loc10_.TemplateID = param1.readInt();
               _loc10_.StrengthLevel = param1.readInt();
               _loc10_.Count = param1.readInt();
               _loc10_.ValidDate = param1.readInt();
               _loc10_.AttackCompose = param1.readInt();
               _loc10_.DefendCompose = param1.readInt();
               _loc10_.AgilityCompose = param1.readInt();
               _loc10_.LuckCompose = param1.readInt();
               _loc10_.IsBind = param1.readBoolean();
               _loc4_.awards.push(_loc10_);
               _loc6_++;
            }
            _loc3_++;
         }
         if(this._view == null)
         {
            this._view = ComponentFactory.Instance.creat("godsRoads.GodsRoadsView");
            this._view.initView();
            this._view.titleText = LanguageMgr.GetTranslation("ddt.godsRoads.title");
            this._view.addEventListener(Event.REMOVED_FROM_STAGE,this.disposeView);
            LayerManager.Instance.addToLayer(this._view,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
         }
         this._view.updateView(this._model,this.lastStep,this.lastMssion);
      }
      
      private function disposeView(param1:Event) : void
      {
         this._view.removeEventListener(Event.REMOVED_FROM_STAGE,this.disposeView);
         this._view = null;
      }
      
      public function get isOpen() : Boolean
      {
         return this._isOpen;
      }
      
      public function get XMLData() : XML
      {
         return this._xml;
      }
      
      public function set XMLData(param1:XML) : void
      {
         this._xml = param1;
      }
   }
}

class PrivateClass
{
    
   
   function PrivateClass()
   {
      super();
   }
}
