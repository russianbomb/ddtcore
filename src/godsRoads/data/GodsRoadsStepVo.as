package godsRoads.data
{
   public class GodsRoadsStepVo
   {
       
      
      public var isGetAwards:Boolean;
      
      public var missionsNum:int;
      
      public var missionVos:Array;
      
      public var awadsNum:int;
      
      public var awards:Array;
      
      public var currentStep:int = 0;
      
      public function GodsRoadsStepVo()
      {
         this.missionVos = [];
         this.awards = [];
         super();
      }
      
      public function getFinishPerNum() : int
      {
         var _loc4_:GodsRoadsMissionVo = null;
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         while(_loc3_ < this.missionVos.length)
         {
            _loc4_ = this.missionVos[_loc3_] as GodsRoadsMissionVo;
            if(_loc4_.isFinished)
            {
               _loc2_++;
            }
            _loc3_++;
         }
         _loc1_ = _loc2_ / this.missionVos.length * 100;
         return _loc1_;
      }
      
      public function getFinishPerString() : String
      {
         var _loc4_:GodsRoadsMissionVo = null;
         var _loc1_:String = "";
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         while(_loc3_ < this.missionVos.length)
         {
            _loc4_ = this.missionVos[_loc3_] as GodsRoadsMissionVo;
            if(_loc4_.isFinished)
            {
               _loc2_++;
            }
            _loc3_++;
         }
         _loc1_ = _loc2_ + "/" + this.missionVos.length;
         return _loc1_;
      }
   }
}
