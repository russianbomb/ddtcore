package godsRoads.analyze
{
   import com.pickgliss.loader.DataAnalyzer;
   import godsRoads.data.GodsRoadsMissionInfo;
   import godsRoads.manager.GodsRoadsManager;
   
   public class GodsRoadsDataAnalyzer extends DataAnalyzer
   {
       
      
      public var dataList:Vector.<GodsRoadsMissionInfo>;
      
      public function GodsRoadsDataAnalyzer(param1:Function)
      {
         this.dataList = new Vector.<GodsRoadsMissionInfo>();
         super(param1);
      }
      
      override public function analyze(param1:*) : void
      {
         var _loc3_:XMLList = null;
         var _loc4_:int = 0;
         var _loc5_:GodsRoadsMissionInfo = null;
         var _loc6_:XMLList = null;
         var _loc2_:XML = new XML(param1);
         GodsRoadsManager.instance.XMLData = _loc2_;
         if(_loc2_.@value == "true")
         {
            _loc3_ = _loc2_..Item;
            _loc4_ = 0;
            while(_loc4_ < _loc3_.length())
            {
               if(_loc3_[_loc4_].@QuestType != 1)
               {
                  _loc5_ = new GodsRoadsMissionInfo();
                  _loc6_ = _loc3_[_loc4_]..Item_Condiction;
                  _loc5_.questId = _loc3_[_loc4_].@ID;
                  _loc5_.Title = _loc3_[_loc4_].@Title;
                  _loc5_.Detail = _loc3_[_loc4_].@Detail;
                  _loc5_.Objective = _loc3_[_loc4_].@Objective;
                  _loc5_.NeedMinLevel = _loc3_[_loc4_].@NeedMinLevel;
                  _loc5_.NeedMaxLevel = _loc3_[_loc4_].@NeedMaxLevel;
                  _loc5_.Period = _loc3_[_loc4_].@Period;
                  _loc5_.conditiontId = _loc6_[0].@CondictionID;
                  _loc5_.conditiontTitle = _loc6_[0].@CondictionTitle;
                  _loc5_.conditionType = _loc6_[0].@CondictionType;
                  _loc5_.Para1 = _loc6_[0].@Para1;
                  _loc5_.Para2 = _loc6_[0].@Para2;
                  _loc5_.IndexType = _loc6_[0].@IndexType;
                  this.dataList.push(_loc5_);
               }
               _loc4_++;
            }
            onAnalyzeComplete();
         }
      }
   }
}
