package kingBless
{
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.BuffInfo;
   import ddt.data.UIModuleTypes;
   import ddt.data.player.SelfInfo;
   import ddt.events.CrazyTankSocketEvent;
   import ddt.manager.LanguageMgr;
   import ddt.manager.PathManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.manager.TimeManager;
   import ddt.view.UIModuleSmallLoading;
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.events.TimerEvent;
   import flash.utils.Timer;
   import kingBless.view.KingBlessMainFrame;
   import road7th.comm.PackageIn;
   
   public class KingBlessManager extends EventDispatcher
   {
      
      public static const UPDATE_BUFF_DATA_EVENT:String = "update_buff_data_event";
      
      public static const UPDATE_MAIN_EVENT:String = "update_main_event";
      
      public static const STRENGTH_ENCHANCE:int = 1;
      
      public static const PET_REFRESH:int = 2;
      
      public static const BEAD_MASTER:int = 3;
      
      public static const HELP_STRAW:int = 4;
      
      public static const DUNGEON_HERO:int = 5;
      
      public static const TASK_SPIRIT:int = 6;
      
      private static var _instance:KingBlessManager;
       
      
      private var _openType:int;
      
      private var _endTime:Date;
      
      private var _buffData:Object;
      
      private var _timer:Timer;
      
      private var _count:int;
      
      private var _func:Function;
      
      private var _funcParams:Array;
      
      private var _isChecked:Boolean = false;
      
      private var _confirmFrame:BaseAlerFrame;
      
      public function KingBlessManager()
      {
         super(null);
      }
      
      public static function get instance() : KingBlessManager
      {
         if(_instance == null)
         {
            _instance = new KingBlessManager();
         }
         return _instance;
      }
      
      public function get openType() : int
      {
         return this._openType;
      }
      
      public function setup() : void
      {
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.KING_BLESS_MAIN,this.updateAllData);
         SocketManager.Instance.addEventListener(CrazyTankSocketEvent.KING_BLESS_UPDATE_BUFF_DATA,this.updateBuffData);
         this._timer = new Timer(1000);
         this._timer.addEventListener(TimerEvent.TIMER,this.timerHandler);
      }
      
      public function clearConfirmFrame() : void
      {
         if(this._confirmFrame)
         {
            this._confirmFrame.removeEventListener(FrameEvent.RESPONSE,this.__confirmOneDay);
            this._confirmFrame.removeEventListener(FrameEvent.RESPONSE,this.__confirmDue);
            ObjectUtils.disposeObject(this._confirmFrame);
         }
         this._confirmFrame = null;
      }
      
      public function checkShowDueAlert() : void
      {
         if(this._isChecked)
         {
            return;
         }
         this._isChecked = true;
         var _loc1_:SelfInfo = PlayerManager.Instance.Self;
         if(_loc1_.Grade < 10 || _loc1_.isSameDay)
         {
            return;
         }
         if(!this._endTime)
         {
            return;
         }
         var _loc2_:String = "";
         if(this._openType > 0 && this._endTime.getTime() - TimeManager.Instance.Now().getTime() < TimeManager.DAY_TICKS)
         {
            _loc2_ = LanguageMgr.GetTranslation("ddt.kingBless.oneDayTipTxt.3p",PathManager.russiaLanguage);
            this._confirmFrame = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),_loc2_,LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),true,true,true,LayerManager.BLCAK_BLOCKGOUND);
            this._confirmFrame.moveEnable = false;
            this._confirmFrame.addEventListener(FrameEvent.RESPONSE,this.__confirmOneDay);
         }
         else if(this._openType == 0 && _loc1_.LastDate.valueOf() < this._endTime.valueOf())
         {
            _loc2_ = LanguageMgr.GetTranslation("ddt.kingBless.dueTipTxt.3p",PathManager.russiaLanguage);
            this._confirmFrame = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("ddt.kingBless.dueTipTxt"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),true,true,true,LayerManager.BLCAK_BLOCKGOUND);
            this._confirmFrame.moveEnable = false;
            this._confirmFrame.addEventListener(FrameEvent.RESPONSE,this.__confirmDue);
         }
      }
      
      private function __confirmOneDay(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         this._confirmFrame.removeEventListener(FrameEvent.RESPONSE,this.__confirmOneDay);
         this._confirmFrame = null;
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            this.loadKingBlessModule(this.doOpenKingBlessFrame);
         }
      }
      
      private function __confirmDue(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         this._confirmFrame.removeEventListener(FrameEvent.RESPONSE,this.__confirmDue);
         this._confirmFrame = null;
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            this.loadKingBlessModule(this.doOpenKingBlessFrame);
         }
      }
      
      public function doOpenKingBlessFrame() : void
      {
         var _loc1_:KingBlessMainFrame = ComponentFactory.Instance.creatComponentByStylename("KingBlessMainFrame");
         LayerManager.Instance.addToLayer(_loc1_,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
      }
      
      public function getRemainTimeTxt() : Object
      {
         var _loc1_:Object = null;
         var _loc11_:int = 0;
         var _loc17_:String = null;
         var _loc2_:Number = this._endTime.getTime();
         var _loc3_:Number = TimeManager.Instance.Now().getTime();
         var _loc4_:Number = _loc2_ - _loc3_;
         if(this._openType == 0 || _loc4_ < 1000)
         {
            _loc1_ = new Object();
            _loc1_.isOpen = false;
            _loc1_.content = LanguageMgr.GetTranslation("ddt.kingBlessFrame.noOpenIconBtnTipTxt.3p",PathManager.russiaLanguage);
            return _loc1_;
         }
         var _loc5_:String = LanguageMgr.GetTranslation("ddt.kingBlessFrame.awardNameStateTxt") + "\r";
         var _loc6_:String = LanguageMgr.GetTranslation("ddt.kingBlessFrame.awardNameTxt5") + "\r";
         var _loc7_:String = LanguageMgr.GetTranslation("ddt.kingBlessFrame.awardNameTxt1",this.getOneBuffData(PET_REFRESH)) + "\r";
         var _loc8_:String = LanguageMgr.GetTranslation("ddt.kingBlessFrame.awardNameTxt2",this.getOneBuffData(BEAD_MASTER)) + "\r";
         var _loc9_:int = this.getOneBuffData(HELP_STRAW);
         var _loc10_:BuffInfo = PlayerManager.Instance.Self.buffInfo[BuffInfo.Save_Life] as BuffInfo;
         if(_loc10_)
         {
            _loc11_ = _loc10_.ValidCount > _loc9_?int(_loc9_):int(_loc10_.ValidCount);
         }
         else
         {
            _loc11_ = 0;
         }
         var _loc12_:String = LanguageMgr.GetTranslation("ddt.kingBlessFrame.awardNameTxt3",_loc11_) + "\r";
         var _loc13_:String = LanguageMgr.GetTranslation("ddt.kingBlessFrame.awardNameTxt4",this.getOneBuffData(DUNGEON_HERO)) + "\r";
         var _loc14_:String = LanguageMgr.GetTranslation("ddt.kingBlessFrame.awardNameTxt6") + "\r";
         var _loc15_:String = _loc7_ + _loc8_ + _loc12_ + _loc13_ + _loc6_ + _loc14_;
         var _loc16_:int = 0;
         if(_loc4_ / TimeManager.DAY_TICKS > 1)
         {
            _loc16_ = _loc4_ / TimeManager.DAY_TICKS;
            _loc17_ = _loc16_ + " " + LanguageMgr.GetTranslation("day");
         }
         else if(_loc4_ / TimeManager.HOUR_TICKS > 1)
         {
            _loc16_ = _loc4_ / TimeManager.HOUR_TICKS;
            _loc17_ = _loc16_ + " " + LanguageMgr.GetTranslation("hour");
         }
         else if(_loc4_ / TimeManager.Minute_TICKS > 1)
         {
            _loc16_ = _loc4_ / TimeManager.Minute_TICKS;
            _loc17_ = _loc16_ + " " + LanguageMgr.GetTranslation("minute");
         }
         else
         {
            _loc16_ = _loc4_ / TimeManager.Second_TICKS;
            _loc17_ = _loc16_ + " " + LanguageMgr.GetTranslation("second");
         }
         _loc1_ = new Object();
         _loc1_.isSelf = _loc1_.isOpen = true;
         _loc1_.title = _loc5_;
         _loc1_.content = _loc15_;
         _loc1_.bottom = LanguageMgr.GetTranslation("ddt.kingBlessFrame.remainTimeTxt") + _loc17_;
         return _loc1_;
      }
      
      private function timerHandler(param1:TimerEvent) : void
      {
         this._count--;
         if(this._count <= 0)
         {
            this._openType = 0;
            this._timer.stop();
            this.helpStrawShowHandler();
            dispatchEvent(new Event(UPDATE_MAIN_EVENT));
         }
      }
      
      private function updateAllData(param1:CrazyTankSocketEvent) : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc2_:PackageIn = param1.pkg;
         this._openType = _loc2_.readInt();
         this._endTime = _loc2_.readDate();
         if(this._openType > 0)
         {
            _loc3_ = _loc2_.readInt();
            this._buffData = {};
            _loc4_ = 0;
            while(_loc4_ < _loc3_)
            {
               _loc5_ = _loc2_.readInt();
               this._buffData[_loc5_] = _loc2_.readInt();
               _loc4_++;
            }
            this._count = int((this._endTime.getTime() - TimeManager.Instance.Now().getTime()) / 1000);
            if(this._count > 0)
            {
               this._timer.reset();
               this._timer.start();
            }
         }
         else
         {
            this._buffData = null;
         }
         this.helpStrawShowHandler();
         dispatchEvent(new Event(UPDATE_MAIN_EVENT));
      }
      
      private function helpStrawShowHandler() : void
      {
         var _loc1_:BuffInfo = PlayerManager.Instance.Self.getBuff(BuffInfo.Save_Life);
         if(_loc1_)
         {
            _loc1_.additionCount = this.getOneBuffData(HELP_STRAW);
         }
      }
      
      private function updateBuffData(param1:CrazyTankSocketEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readInt();
         this._buffData[_loc3_] = _loc2_.readInt();
         dispatchEvent(new Event(UPDATE_BUFF_DATA_EVENT));
      }
      
      public function getOneBuffData(param1:int) : int
      {
         if(this._openType > 0 && this._buffData)
         {
            return this._buffData[param1];
         }
         return 0;
      }
      
      public function loadKingBlessModule(param1:Function = null, param2:Array = null) : void
      {
         this._func = param1;
         this._funcParams = param2;
         UIModuleSmallLoading.Instance.progress = 0;
         UIModuleSmallLoading.Instance.show();
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadCompleteHandler);
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUimoduleLoadProgress);
         UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.KING_BLESS);
      }
      
      private function onUimoduleLoadProgress(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.KING_BLESS)
         {
            UIModuleSmallLoading.Instance.progress = param1.loader.progress * 100;
         }
      }
      
      private function loadCompleteHandler(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.KING_BLESS)
         {
            UIModuleSmallLoading.Instance.hide();
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadCompleteHandler);
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUimoduleLoadProgress);
            if(null != this._func)
            {
               this._func.apply(null,this._funcParams);
            }
            this._func = null;
            this._funcParams = null;
         }
      }
   }
}
