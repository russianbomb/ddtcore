package kingBless.view
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.ui.tip.BaseTip;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.LanguageMgr;
   import flash.display.Bitmap;
   
   public class KingBlessBuffTipView extends BaseTip
   {
       
      
      private var _bg:Bitmap;
      
      private var _titleTxt:FilterFrameText;
      
      private var _valueTxtList:Vector.<FilterFrameText>;
      
      private var _tipTxt:FilterFrameText;
      
      private var _valueNameList:Array;
      
      public function KingBlessBuffTipView()
      {
         var _loc2_:FilterFrameText = null;
         super();
         this._valueNameList = LanguageMgr.GetTranslation("ddt.kingBless.game.buffTipView.valueNameTxtList").split(",");
         this._bg = ComponentFactory.Instance.creatBitmap("asset.game.kingbless.buffTipViewBg");
         this._titleTxt = ComponentFactory.Instance.creatComponentByStylename("game.kingbless.tipView.titleTxt");
         this._titleTxt.text = LanguageMgr.GetTranslation("ddt.kingBlessFrame.awardNameTxtList").split(",")[0];
         this._tipTxt = ComponentFactory.Instance.creatComponentByStylename("game.kingbless.tipView.tipTxt");
         this._tipTxt.text = LanguageMgr.GetTranslation("ddt.kingBless.game.buffTipView.tipTxt");
         addChild(this._bg);
         addChild(this._titleTxt);
         addChild(this._tipTxt);
         this._valueTxtList = new Vector.<FilterFrameText>(4);
         var _loc1_:int = 0;
         while(_loc1_ < 4)
         {
            _loc2_ = ComponentFactory.Instance.creatComponentByStylename("game.kingbless.tipView.valueTxt");
            _loc2_.y = _loc2_.y + _loc1_ * 20;
            addChild(_loc2_);
            this._valueTxtList[_loc1_] = _loc2_;
            _loc1_++;
         }
      }
      
      override public function set tipData(param1:Object) : void
      {
         var _loc2_:int = int(param1);
         var _loc3_:int = 0;
         while(_loc3_ < 4)
         {
            this._valueTxtList[_loc3_].text = LanguageMgr.GetTranslation("ddt.kingBless.game.buffTipView.valueTxt",this._valueNameList[_loc3_],_loc2_);
            _loc3_++;
         }
      }
      
      override public function get width() : Number
      {
         if(this._bg)
         {
            return this._bg.width;
         }
         return super.width;
      }
      
      override public function get height() : Number
      {
         if(this._bg)
         {
            return this._bg.height;
         }
         return super.height;
      }
      
      override public function dispose() : void
      {
         var _loc1_:FilterFrameText = null;
         ObjectUtils.disposeObject(this._bg);
         this._bg = null;
         ObjectUtils.disposeObject(this._titleTxt);
         this._titleTxt = null;
         ObjectUtils.disposeObject(this._tipTxt);
         this._tipTxt = null;
         for each(_loc1_ in this._valueTxtList)
         {
            ObjectUtils.disposeObject(_loc1_);
         }
         this._valueTxtList = null;
         this._valueNameList = null;
         super.dispose();
      }
   }
}
