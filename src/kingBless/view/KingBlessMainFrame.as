package kingBless.view
{
   import baglocked.BaglockedManager;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.toplevel.StageReferance;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.ScrollPanel;
   import com.pickgliss.ui.controls.SelectedButton;
   import com.pickgliss.ui.controls.SelectedButtonGroup;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.controls.container.VBox;
   import com.pickgliss.ui.image.Image;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.manager.LanguageMgr;
   import ddt.manager.LeavePageManager;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PathManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.utils.FilterWordManager;
   import ddtBuried.BuriedManager;
   import flash.display.Bitmap;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import kingBless.KingBlessManager;
   import shop.view.ShopPresentClearingFrame;
   
   public class KingBlessMainFrame extends Frame
   {
       
      
      private var _awardIconBtnBg:Bitmap;
      
      private var _monthBtnBg:Bitmap;
      
      private var _bottomBg:Bitmap;
      
      private var _txtBg:Bitmap;
      
      private var _oneMonthBtn:SelectedButton;
      
      private var _threeMonthBtn:SelectedButton;
      
      private var _sixMonthBtn:SelectedButton;
      
      private var _selectedButtonGroup:SelectedButtonGroup;
      
      private var _openFriendBtn:SimpleBitmapButton;
      
      private var _openBtn:SimpleBitmapButton;
      
      private var _awardIconList:Vector.<Image>;
      
      private var _awardNameTxtList:Vector.<FilterFrameText>;
      
      private var _tipTxt:FilterFrameText;
      
      private var _descTxt:FilterFrameText;
      
      private var _vbox:VBox;
      
      private var _scrollPanel:ScrollPanel;
      
      private var _nameList:Array;
      
      private var _descList:Array;
      
      private var _tipList:Array;
      
      private var _tipNoOpenTxt:String;
      
      private var _needMoneyList:Array;
      
      private var _openTimeDescList:Array;
      
      private var _giveFriendOpenFrame:ShopPresentClearingFrame;
      
      private var _discountIcon:Image;
      
      private var _friendInfo:Object;
      
      private var payMoney:int;
      
      private var _isGive:Boolean;
      
      public function KingBlessMainFrame()
      {
         super();
         this.initData();
         this.initView();
         this.initEvent();
      }
      
      private function initData() : void
      {
         var _loc2_:Array = null;
         var _loc3_:int = 0;
         this._nameList = LanguageMgr.GetTranslation("ddt.kingBlessFrame.awardNameTxtList1").split(",");
         this._descList = [];
         this._tipList = [];
         var _loc1_:int = 1;
         while(_loc1_ <= 3)
         {
            this._tipList.push(LanguageMgr.GetTranslation("ddt.kingBlessFrame.awardTipTxt" + _loc1_));
            _loc2_ = [];
            _loc3_ = 1;
            while(_loc3_ <= 6)
            {
               _loc2_.push(LanguageMgr.GetTranslation("ddt.kingBlessFrame.awardDescTxt" + _loc1_.toString() + _loc3_.toString()));
               _loc3_++;
            }
            this._descList.push(_loc2_);
            _loc1_++;
         }
         this._tipNoOpenTxt = LanguageMgr.GetTranslation("ddt.kingBlessFrame.noOpenTipTxt");
         this._needMoneyList = LanguageMgr.GetTranslation("ddt.kingBlessFrame.openNeedMoney").split(",");
         this._openTimeDescList = LanguageMgr.GetTranslation("ddt.kingBlessFrame.openTime").split(",");
      }
      
      private function initView() : void
      {
         var _loc2_:Image = null;
         var _loc3_:FilterFrameText = null;
         titleText = LanguageMgr.GetTranslation("ddt.kingBlessFrame.titleTxt.3p",PathManager.russiaLanguage);
         this._awardIconBtnBg = ComponentFactory.Instance.creatBitmap("asset.kingbless.awardIconBtnBg");
         this._monthBtnBg = ComponentFactory.Instance.creatBitmap("asset.kingbless.monthBtnBg");
         this._bottomBg = ComponentFactory.Instance.creatBitmap("asset.kingbless.bottomBg");
         this._txtBg = ComponentFactory.Instance.creatBitmap("asset.kingbless.txtBg");
         this._oneMonthBtn = ComponentFactory.Instance.creatComponentByStylename("kingBlessFrame.oneMonthBtn");
         this._threeMonthBtn = ComponentFactory.Instance.creatComponentByStylename("kingBlessFrame.threeMonthBtn");
         this._sixMonthBtn = ComponentFactory.Instance.creatComponentByStylename("kingBlessFrame.sixMonthBtn");
         this._selectedButtonGroup = new SelectedButtonGroup();
         this._selectedButtonGroup.addSelectItem(this._oneMonthBtn);
         this._selectedButtonGroup.addSelectItem(this._threeMonthBtn);
         this._selectedButtonGroup.addSelectItem(this._sixMonthBtn);
         this._selectedButtonGroup.selectIndex = 2;
         this._openFriendBtn = ComponentFactory.Instance.creatComponentByStylename("kingBlessFrame.openFriendBtn");
         this._openBtn = ComponentFactory.Instance.creatComponentByStylename("kingBlessFrame.openBtn");
         this._tipTxt = ComponentFactory.Instance.creatComponentByStylename("kingBlessFrame.tipTxt");
         this._descTxt = ComponentFactory.Instance.creatComponentByStylename("kingBlessFrame.descTxt");
         this._vbox = ComponentFactory.Instance.creatComponentByStylename("kingBlessFrame.txtVBox");
         this._scrollPanel = ComponentFactory.Instance.creatComponentByStylename("kingBlessFrame.txtScrollPanel");
         this._discountIcon = ComponentFactory.Instance.creatComponentByStylename("kingBlessFrame.discountIcon");
         addToContent(this._awardIconBtnBg);
         addToContent(this._monthBtnBg);
         addToContent(this._bottomBg);
         addToContent(this._txtBg);
         addToContent(this._oneMonthBtn);
         addToContent(this._threeMonthBtn);
         addToContent(this._sixMonthBtn);
         addToContent(this._scrollPanel);
         addToContent(this._openFriendBtn);
         addToContent(this._openBtn);
         addToContent(this._discountIcon);
         this._awardIconList = new Vector.<Image>(6);
         this._awardNameTxtList = new Vector.<FilterFrameText>(6);
         var _loc1_:int = 0;
         while(_loc1_ < 6)
         {
            _loc2_ = ComponentFactory.Instance.creatComponentByStylename("kingBlessFrame.awardIcon");
            _loc2_.resourceLink = "asset.kingbless.awardIcon" + (_loc1_ + 1);
            _loc3_ = ComponentFactory.Instance.creatComponentByStylename("kingBlessFrame.iconNameTxt");
            _loc3_.text = this._nameList[_loc1_];
            _loc2_.x = _loc2_.x + _loc1_ * 79;
            _loc3_.x = _loc3_.x + _loc1_ * 79;
            addToContent(_loc2_);
            addToContent(_loc3_);
            this._awardIconList[_loc1_] = _loc2_;
            this._awardNameTxtList[_loc1_] = _loc3_;
            _loc1_++;
         }
         this.refreshIconTipData();
         this.refreshShowTxt();
         this.refreshOpenBtn();
      }
      
      private function initEvent() : void
      {
         addEventListener(FrameEvent.RESPONSE,this.__responseHandler);
         this._selectedButtonGroup.addEventListener(Event.CHANGE,this.selectedButtonChange,false,0,true);
         KingBlessManager.instance.addEventListener(KingBlessManager.UPDATE_MAIN_EVENT,this.refreshView);
         this._openFriendBtn.addEventListener(MouseEvent.CLICK,this.openFriendHandler,false,0,true);
         this._openBtn.addEventListener(MouseEvent.CLICK,this.openHandler,false,0,true);
      }
      
      private function openHandler(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         if(!this.judgeOpen())
         {
            return;
         }
         this.openConfirmFrame();
      }
      
      private function judgeOpen() : Boolean
      {
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return false;
         }
         var _loc1_:int = this._selectedButtonGroup.selectIndex;
         this.payMoney = this._needMoneyList[_loc1_];
         return true;
      }
      
      private function openConfirmFrame() : void
      {
         var _loc2_:String = null;
         var _loc3_:BaseAlerFrame = null;
         var _loc1_:int = this._selectedButtonGroup.selectIndex;
         if(!this._friendInfo)
         {
            _loc2_ = LanguageMgr.GetTranslation("ddt.kingBlessFrame.openPromptTxt.3p",PathManager.russiaLanguage,this._openTimeDescList[_loc1_],this._needMoneyList[_loc1_]);
            _loc3_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),_loc2_,LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),true,true,true,LayerManager.BLCAK_BLOCKGOUND,null,"SimpleAlert",30,true);
            _loc3_.moveEnable = false;
            _loc3_.addEventListener(FrameEvent.RESPONSE,this.__confirm);
            this._isGive = false;
         }
         else
         {
            this._isGive = true;
            _loc2_ = LanguageMgr.GetTranslation("ddt.kingBlessFrame.openFriendPromptTxt.3p",this._friendInfo["name"],PathManager.russiaLanguage,this._openTimeDescList[_loc1_],this._needMoneyList[_loc1_]);
            _loc3_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),_loc2_,LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),true,true,true,LayerManager.BLCAK_BLOCKGOUND);
            _loc3_.moveEnable = false;
            _loc3_.isBand = false;
            _loc3_.addEventListener(FrameEvent.RESPONSE,this.__confirm);
         }
      }
      
      private function __confirm(param1:FrameEvent) : void
      {
         var _loc3_:int = 0;
         var _loc4_:String = null;
         var _loc5_:int = 0;
         SoundManager.instance.play("008");
         var _loc2_:BaseAlerFrame = param1.currentTarget as BaseAlerFrame;
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            if(!this._friendInfo)
            {
               _loc3_ = PlayerManager.Instance.Self.ID;
               _loc4_ = "";
            }
            else
            {
               _loc3_ = this._friendInfo["id"];
               _loc4_ = this._friendInfo["msg"];
            }
            _loc5_ = this._selectedButtonGroup.selectIndex + 1;
            if(!this._isGive)
            {
               if(BuriedManager.Instance.checkMoney(_loc2_.isBand,this.payMoney))
               {
                  return;
               }
               SocketManager.Instance.out.sendOpenKingBless(_loc5_,_loc3_,_loc4_,_loc2_.isBand);
               _loc2_.dispose();
            }
            else if(!_loc2_.isBand && PlayerManager.Instance.Self.Money >= this.payMoney)
            {
               SocketManager.Instance.out.sendOpenKingBless(_loc5_,_loc3_,_loc4_,_loc2_.isBand);
            }
            else
            {
               LeavePageManager.showFillFrame();
               _loc2_.dispose();
               return;
            }
         }
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.__confirm);
         this._friendInfo = null;
      }
      
      private function openFriendHandler(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         if(!this.judgeOpen())
         {
            return;
         }
         this._giveFriendOpenFrame = ComponentFactory.Instance.creatComponentByStylename("core.ddtshop.ShopPresentClearingFrame");
         this._giveFriendOpenFrame.nameInput.enable = false;
         this._giveFriendOpenFrame.titleTxt.visible = false;
         this._giveFriendOpenFrame.setType();
         this._giveFriendOpenFrame.show();
         this._giveFriendOpenFrame.presentBtn.addEventListener(MouseEvent.CLICK,this.__presentBtnClick,false,0,true);
         this._giveFriendOpenFrame.addEventListener(FrameEvent.RESPONSE,this.__responseHandler2,false,0,true);
      }
      
      private function __responseHandler2(param1:FrameEvent) : void
      {
         if(param1.responseCode == FrameEvent.CLOSE_CLICK || param1.responseCode == FrameEvent.ESC_CLICK || param1.responseCode == FrameEvent.CANCEL_CLICK)
         {
            StageReferance.stage.focus = this;
            this._giveFriendOpenFrame = null;
         }
      }
      
      private function __presentBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         var _loc2_:String = this._giveFriendOpenFrame.nameInput.text;
         if(_loc2_ == "")
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("shop.ShopIIPresentView.give"));
            return;
         }
         if(FilterWordManager.IsNullorEmpty(_loc2_))
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("shop.ShopIIPresentView.space"));
            return;
         }
         this._friendInfo = {};
         this._friendInfo["id"] = this._giveFriendOpenFrame.selectPlayerId;
         this._friendInfo["name"] = _loc2_;
         this._friendInfo["msg"] = FilterWordManager.filterWrod(this._giveFriendOpenFrame.textArea.text);
         this.openConfirmFrame();
      }
      
      private function refreshView(param1:Event) : void
      {
         this.refreshIconTipData();
         this.refreshOpenBtn();
      }
      
      private function selectedButtonChange(param1:Event) : void
      {
         SoundManager.instance.play("008");
         this.refreshShowTxt();
      }
      
      private function refreshOpenBtn() : void
      {
         var _loc1_:int = KingBlessManager.instance.openType;
         if(_loc1_ > 0)
         {
            this._openBtn.backStyle = "asset.kingbless.renewBtn";
         }
         else
         {
            this._openBtn.backStyle = "asset.kingbless.openBtn";
         }
      }
      
      private function refreshIconTipData() : void
      {
         var _loc1_:int = KingBlessManager.instance.openType;
         var _loc2_:int = 0;
         while(_loc2_ < 6)
         {
            if(_loc1_ > 0)
            {
               this._awardIconList[_loc2_].tipData = this._descList[_loc1_ - 1][_loc2_];
            }
            else
            {
               this._awardIconList[_loc2_].tipData = this._tipNoOpenTxt;
            }
            _loc2_++;
         }
      }
      
      private function refreshShowTxt() : void
      {
         var _loc5_:String = null;
         var _loc6_:Array = null;
         var _loc1_:int = this._selectedButtonGroup.selectIndex;
         this._tipTxt.text = this._tipList[_loc1_];
         var _loc2_:Array = this._descList[_loc1_];
         var _loc3_:String = "";
         var _loc4_:int = 0;
         while(_loc4_ < 6)
         {
            _loc5_ = _loc2_[_loc4_];
            _loc6_ = _loc5_.match(/\d+/);
            if(_loc6_ && _loc6_.length > 0)
            {
               _loc5_ = _loc5_.replace(_loc6_[0],"<FONT COLOR=\'#FF0000\'>" + _loc6_[0] + "</FONT>");
            }
            _loc3_ = _loc3_ + (LanguageMgr.GetTranslation("ddt.kingBlessFrame.awardDescTxt",(_loc4_ + 1).toString(),this._nameList[_loc4_],_loc5_) + "\n");
            _loc4_++;
         }
         this._descTxt.htmlText = _loc3_;
         this._vbox.addChild(this._tipTxt);
         this._vbox.addChild(this._descTxt);
         this._scrollPanel.setView(this._vbox);
      }
      
      private function __responseHandler(param1:FrameEvent) : void
      {
         if(param1.responseCode == FrameEvent.CLOSE_CLICK || param1.responseCode == FrameEvent.ESC_CLICK)
         {
            SoundManager.instance.play("008");
            this.dispose();
         }
      }
      
      private function removeEvent() : void
      {
         removeEventListener(FrameEvent.RESPONSE,this.__responseHandler);
         this._selectedButtonGroup.removeEventListener(Event.CHANGE,this.selectedButtonChange);
         KingBlessManager.instance.removeEventListener(KingBlessManager.UPDATE_MAIN_EVENT,this.refreshView);
         this._openFriendBtn.removeEventListener(MouseEvent.CLICK,this.openFriendHandler);
         this._openBtn.removeEventListener(MouseEvent.CLICK,this.openHandler);
      }
      
      override public function dispose() : void
      {
         var _loc1_:Image = null;
         var _loc2_:FilterFrameText = null;
         this.removeEvent();
         ObjectUtils.disposeObject(this._awardIconBtnBg);
         this._awardIconBtnBg = null;
         ObjectUtils.disposeObject(this._monthBtnBg);
         this._monthBtnBg = null;
         ObjectUtils.disposeObject(this._bottomBg);
         this._bottomBg = null;
         ObjectUtils.disposeObject(this._txtBg);
         this._txtBg = null;
         ObjectUtils.disposeObject(this._selectedButtonGroup);
         this._selectedButtonGroup = null;
         ObjectUtils.disposeObject(this._oneMonthBtn);
         this._oneMonthBtn = null;
         ObjectUtils.disposeObject(this._threeMonthBtn);
         this._threeMonthBtn = null;
         ObjectUtils.disposeObject(this._sixMonthBtn);
         this._sixMonthBtn = null;
         ObjectUtils.disposeObject(this._tipTxt);
         this._tipTxt = null;
         ObjectUtils.disposeObject(this._descTxt);
         this._descTxt = null;
         ObjectUtils.disposeObject(this._vbox);
         this._vbox = null;
         ObjectUtils.disposeObject(this._scrollPanel);
         this._scrollPanel = null;
         ObjectUtils.disposeObject(this._openBtn);
         this._openBtn = null;
         ObjectUtils.disposeObject(this._openFriendBtn);
         this._openFriendBtn = null;
         ObjectUtils.disposeObject(this._giveFriendOpenFrame);
         this._giveFriendOpenFrame = null;
         ObjectUtils.disposeObject(this._discountIcon);
         this._discountIcon = null;
         for each(_loc1_ in this._awardIconList)
         {
            ObjectUtils.disposeObject(_loc1_);
         }
         this._awardIconList = null;
         for each(_loc2_ in this._awardNameTxtList)
         {
            ObjectUtils.disposeObject(_loc2_);
         }
         this._awardNameTxtList = null;
         this._nameList = null;
         this._descList = null;
         super.dispose();
      }
   }
}
