package magicStone.components
{
   import bagAndInfo.cell.BagCell;
   import bagAndInfo.cell.DragEffect;
   import baglocked.BaglockedManager;
   import com.pickgliss.events.InteractiveEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.DoubleClickManager;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.events.CellEvent;
   import ddt.manager.DragManager;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.DisplayObject;
   import flash.events.MouseEvent;
   import flash.geom.Point;
   
   public class MgStoneCell extends BagCell
   {
       
      
      private var _lockIcon:Bitmap;
      
      protected var _nameTxt:FilterFrameText;
      
      public function MgStoneCell(param1:int = 0, param2:ItemTemplateInfo = null, param3:Boolean = true, param4:DisplayObject = null)
      {
         super(param1,param2,param3,param4);
         _picPos = new Point(2,0);
         this.initEvents();
      }
      
      private function initEvents() : void
      {
         addEventListener(InteractiveEvent.CLICK,this.onClick);
         addEventListener(InteractiveEvent.DOUBLE_CLICK,this.onDoubleClick);
         DoubleClickManager.Instance.enableDoubleClick(this);
      }
      
      private function removeEvents() : void
      {
         removeEventListener(InteractiveEvent.CLICK,this.onClick);
         removeEventListener(InteractiveEvent.DOUBLE_CLICK,this.onDoubleClick);
         DoubleClickManager.Instance.disableDoubleClick(this);
      }
      
      override protected function onMouseOver(param1:MouseEvent) : void
      {
      }
      
      override protected function onMouseClick(param1:MouseEvent) : void
      {
      }
      
      protected function onClick(param1:InteractiveEvent) : void
      {
         dispatchEvent(new CellEvent(CellEvent.ITEM_CLICK,this));
      }
      
      protected function onDoubleClick(param1:InteractiveEvent) : void
      {
         SoundManager.instance.playButtonSound();
         if(info)
         {
            dispatchEvent(new CellEvent(CellEvent.DOUBLE_CLICK,this));
         }
      }
      
      override public function dragStart() : void
      {
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         if(_info && !locked && stage && _allowDrag)
         {
            if(DragManager.startDrag(this,_info,createDragImg(),stage.mouseX,stage.mouseY,DragEffect.MOVE))
            {
               locked = true;
               this.dragHidePicTxt();
               if(_info && _pic.numChildren > 0)
               {
                  dispatchEvent(new CellEvent(CellEvent.DRAGSTART,this.info,true));
               }
            }
         }
      }
      
      override public function dragDrop(param1:DragEffect) : void
      {
         var _loc2_:InventoryItemInfo = null;
         if(param1.data is InventoryItemInfo)
         {
            _loc2_ = param1.data as InventoryItemInfo;
            if(param1.source is MgStoneCell)
            {
               SocketManager.Instance.out.moveMagicStone(_loc2_.Place,_place);
               DragManager.acceptDrag(this);
               return;
            }
         }
         else if(param1.source is MgStoneLockBtn)
         {
            DragManager.acceptDrag(this);
         }
      }
      
      override public function dragStop(param1:DragEffect) : void
      {
         SoundManager.instance.play("008");
         dispatchEvent(new CellEvent(CellEvent.DRAGSTOP,null,true));
         if(param1.action == DragEffect.MOVE)
         {
            if(!param1.target)
            {
               param1.action = DragEffect.NONE;
               if(!(param1.target is MgStoneCell))
               {
                  if(!param1.target)
                  {
                     MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("magicStone.CanntDestory"));
                  }
               }
            }
            else
            {
               locked = false;
            }
         }
         this.dragShowPicTxt();
         super.dragStop(param1);
      }
      
      private function dragHidePicTxt() : void
      {
         if(this._lockIcon)
         {
            this._lockIcon.visible = false;
         }
      }
      
      private function dragShowPicTxt() : void
      {
         if(itemInfo.goodsLock && this._lockIcon)
         {
            this._lockIcon.visible = true;
         }
      }
      
      public function lockMgStone() : void
      {
         if(!itemInfo.goodsLock)
         {
            if(this._lockIcon)
            {
               this._lockIcon.visible = true;
            }
            else
            {
               this._lockIcon = ComponentFactory.Instance.creatBitmap("asset.beadSystem.beadInset.lockIcon1");
               this._lockIcon.scaleX = this._lockIcon.scaleY = 0.7;
               this.addChild(this._lockIcon);
            }
            setChildIndex(this._lockIcon,numChildren - 1);
            SocketManager.Instance.out.lockMagicStone(_place);
         }
         else
         {
            if(this._lockIcon)
            {
               this._lockIcon.visible = false;
            }
            SocketManager.Instance.out.lockMagicStone(_place);
         }
      }
      
      override public function set info(param1:ItemTemplateInfo) : void
      {
         if(_info)
         {
            _tipData = null;
            locked = false;
            if(this._nameTxt)
            {
               this._nameTxt.htmlText = "";
               this._nameTxt.visible = false;
            }
         }
         super.info = param1;
         if(param1)
         {
            if(!this._nameTxt)
            {
               this._nameTxt = ComponentFactory.Instance.creatComponentByStylename("magicStone.mgStoneCell.name");
               this._nameTxt.mouseEnabled = false;
               addChild(this._nameTxt);
            }
            this._nameTxt.text = param1.Name.substr(8,param1.Name.length - 9);
            this._nameTxt.visible = true;
            setChildIndex(this._nameTxt,numChildren - 1);
            if(itemInfo && itemInfo.goodsLock)
            {
               if(this._lockIcon)
               {
                  this._lockIcon.visible = true;
               }
               else
               {
                  this._lockIcon = ComponentFactory.Instance.creatBitmap("asset.beadSystem.beadInset.lockIcon1");
                  this._lockIcon.scaleX = this._lockIcon.scaleY = 0.7;
                  this.addChild(this._lockIcon);
               }
               setChildIndex(this._lockIcon,numChildren - 1);
            }
            else if(this._lockIcon)
            {
               this._lockIcon.visible = false;
            }
         }
         else if(this._lockIcon)
         {
            this._lockIcon.visible = false;
         }
      }
      
      override protected function createLoading() : void
      {
         super.createLoading();
         PositionUtils.setPos(_loadingasset,"ddt.personalInfocell.loadingPos");
      }
      
      override public function dispose() : void
      {
         this.removeEvents();
         ObjectUtils.disposeObject(this._lockIcon);
         this._lockIcon = null;
         ObjectUtils.disposeObject(this._nameTxt);
         this._nameTxt = null;
         super.dispose();
      }
   }
}
