package magicStone.data
{
   import flash.events.Event;
   import road7th.comm.PackageIn;
   
   public class MagicStoneEvent extends Event
   {
      
      public static const MAGIC_STONE_SCORE:String = "magicStoneScore";
      
      public static const UPDATE_BAG:String = "updateBag";
      
      public static const UPDATE_REMAIN_COUNT:String = "updateRemainCount";
       
      
      private var _pkg:PackageIn;
      
      public function MagicStoneEvent(param1:String, param2:PackageIn = null)
      {
         super(param1,bubbles,cancelable);
         this._pkg = param2;
      }
      
      public function get pkg() : PackageIn
      {
         return this._pkg;
      }
   }
}
