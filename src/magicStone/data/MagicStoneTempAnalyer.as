package magicStone.data
{
   import com.pickgliss.loader.DataAnalyzer;
   import com.pickgliss.utils.ObjectUtils;
   
   public class MagicStoneTempAnalyer extends DataAnalyzer
   {
       
      
      private var _mgStoneTempArr:Array;
      
      public function MagicStoneTempAnalyer(param1:Function)
      {
         super(param1);
      }
      
      override public function analyze(param1:*) : void
      {
         var _loc3_:XMLList = null;
         var _loc4_:int = 0;
         var _loc5_:MgStoneTempVO = null;
         var _loc2_:XML = new XML(param1);
         if(_loc2_.@value == "true")
         {
            this._mgStoneTempArr = [];
            _loc3_ = _loc2_..Item;
            _loc4_ = 0;
            while(_loc4_ <= _loc3_.length() - 1)
            {
               _loc5_ = new MgStoneTempVO();
               ObjectUtils.copyPorpertiesByXML(_loc5_,_loc3_[_loc4_]);
               this._mgStoneTempArr.push(_loc5_);
               _loc4_++;
            }
            onAnalyzeComplete();
         }
         else
         {
            message = _loc2_.@message;
            onAnalyzeError();
            onAnalyzeError();
         }
      }
      
      public function get mgStoneTempArr() : Array
      {
         return this._mgStoneTempArr;
      }
   }
}
