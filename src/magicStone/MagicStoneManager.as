package magicStone
{
   import com.pickgliss.events.UIModuleEvent;
   import com.pickgliss.loader.UIModuleLoader;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.UIModuleTypes;
   import ddt.data.goods.ItemTemplateInfo;
   import ddt.data.player.PlayerInfo;
   import ddt.manager.SocketManager;
   import ddt.view.UIModuleSmallLoading;
   import flash.display.MovieClip;
   import flash.events.Event;
   import magicStone.data.MagicStoneEvent;
   import magicStone.data.MagicStoneTempAnalyer;
   import magicStone.data.MgStoneTempVO;
   import magicStone.views.MagicStoneInfoView;
   import magicStone.views.MagicStoneShopFrame;
   import road7th.comm.PackageIn;
   
   public class MagicStoneManager
   {
      
      private static var _instance:MagicStoneManager;
       
      
      private var _func:Function;
      
      private var _funcParams:Array;
      
      public var mgStoneScore:int = 0;
      
      private var _infoView:MagicStoneInfoView;
      
      private var _shopFrame:MagicStoneShopFrame;
      
      private var _mgStoneTempArr:Array;
      
      private var _upgradeMC:MovieClip;
      
      public var isNoPrompt:Boolean;
      
      public var isBand:Boolean;
      
      public function MagicStoneManager()
      {
         super();
      }
      
      public static function get instance() : MagicStoneManager
      {
         if(!_instance)
         {
            _instance = new MagicStoneManager();
         }
         return _instance;
      }
      
      public function setup() : void
      {
         this.addEvents();
      }
      
      public function addEvents() : void
      {
         SocketManager.Instance.addEventListener(MagicStoneEvent.MAGIC_STONE_SCORE,this.__getMagicStoneScore);
      }
      
      public function removeEvents() : void
      {
         SocketManager.Instance.removeEventListener(MagicStoneEvent.MAGIC_STONE_SCORE,this.__getMagicStoneScore);
      }
      
      protected function __getMagicStoneScore(param1:MagicStoneEvent) : void
      {
         var _loc2_:PackageIn = param1.pkg;
         this.mgStoneScore = _loc2_.readInt();
         if(this._infoView)
         {
            this._infoView.updateScore(this.mgStoneScore);
         }
         if(this._shopFrame)
         {
            this._shopFrame.updateScore(this.mgStoneScore);
         }
      }
      
      public function loadMgStoneTempComplete(param1:MagicStoneTempAnalyer) : void
      {
         this._mgStoneTempArr = param1.mgStoneTempArr;
      }
      
      public function getNeedExp(param1:int, param2:int) : int
      {
         var _loc4_:MgStoneTempVO = null;
         var _loc3_:int = 0;
         while(_loc3_ <= this._mgStoneTempArr.length - 1)
         {
            _loc4_ = this._mgStoneTempArr[_loc3_] as MgStoneTempVO;
            if(_loc4_.TemplateID == param1 && _loc4_.Level == param2)
            {
               return _loc4_.Exp;
            }
            _loc3_++;
         }
         return 0;
      }
      
      public function getNeedExpPerLevel(param1:int, param2:int) : int
      {
         var _loc6_:MgStoneTempVO = null;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         while(_loc5_ <= this._mgStoneTempArr.length - 1)
         {
            _loc6_ = this._mgStoneTempArr[_loc5_] as MgStoneTempVO;
            if(_loc6_.TemplateID == param1)
            {
               if(_loc6_.Level == param2 - 1)
               {
                  _loc3_ = _loc6_.Exp;
               }
               else if(_loc6_.Level == param2)
               {
                  _loc4_ = _loc6_.Exp;
               }
            }
            _loc5_++;
         }
         return _loc4_ - _loc3_ > 0?int(_loc4_ - _loc3_):int(0);
      }
      
      public function getExpOfCurLevel(param1:int, param2:int) : int
      {
         var _loc3_:int = 1;
         var _loc4_:int = MagicStoneManager.instance.getNeedExpPerLevel(param1,_loc3_);
         while(_loc4_ > 0 && param2 >= _loc4_)
         {
            _loc3_++;
            param2 = param2 - _loc4_;
            _loc4_ = MagicStoneManager.instance.getNeedExpPerLevel(param1,_loc3_);
         }
         return param2;
      }
      
      public function fillPropertys(param1:ItemTemplateInfo) : void
      {
         var _loc3_:MgStoneTempVO = null;
         var _loc2_:int = 0;
         while(_loc2_ <= this._mgStoneTempArr.length - 1)
         {
            _loc3_ = this._mgStoneTempArr[_loc2_] as MgStoneTempVO;
            if(_loc3_.TemplateID == param1.TemplateID && _loc3_.Level == param1.Level)
            {
               param1.Attack = _loc3_.Attack;
               param1.Defence = _loc3_.Defence;
               param1.Agility = _loc3_.Agility;
               param1.Luck = _loc3_.Luck;
               param1.MagicAttack = _loc3_.MagicAttack;
               param1.MagicDefence = _loc3_.MagicDefence;
            }
            _loc2_++;
         }
      }
      
      public function playUpgradeMc() : void
      {
         if(this._upgradeMC)
         {
            this._upgradeMC.stop();
            this._upgradeMC.removeEventListener(Event.ENTER_FRAME,this.__disposeUpgradeMC);
            ObjectUtils.disposeObject(this._upgradeMC);
            this._upgradeMC = null;
         }
         this._upgradeMC = ComponentFactory.Instance.creat("magicStone.upgradeMc");
         this._upgradeMC.x = 455;
         this._upgradeMC.y = 123;
         this._upgradeMC.addEventListener(Event.ENTER_FRAME,this.__disposeUpgradeMC);
         LayerManager.Instance.addToLayer(this._upgradeMC,LayerManager.STAGE_TOP_LAYER,false,LayerManager.BLCAK_BLOCKGOUND,true);
         this._upgradeMC.gotoAndPlay(1);
      }
      
      protected function __disposeUpgradeMC(param1:Event) : void
      {
         var _loc2_:MovieClip = param1.target as MovieClip;
         if(_loc2_.currentFrame == _loc2_.totalFrames)
         {
            _loc2_.gotoAndStop(1);
            _loc2_.removeEventListener(Event.ENTER_FRAME,this.__disposeUpgradeMC);
            ObjectUtils.disposeObject(_loc2_);
            _loc2_ = null;
         }
      }
      
      public function loadResModule(param1:Function = null, param2:Array = null) : void
      {
         this._func = param1;
         this._funcParams = param2;
         UIModuleSmallLoading.Instance.progress = 0;
         UIModuleSmallLoading.Instance.show();
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadCompleteHandler);
         UIModuleLoader.Instance.addEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUimoduleLoadProgress);
         UIModuleLoader.Instance.addUIModuleImp(UIModuleTypes.MAGIC_STONE);
      }
      
      private function onUimoduleLoadProgress(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.MAGIC_STONE)
         {
            UIModuleSmallLoading.Instance.progress = param1.loader.progress * 100;
         }
      }
      
      private function loadCompleteHandler(param1:UIModuleEvent) : void
      {
         if(param1.module == UIModuleTypes.MAGIC_STONE)
         {
            UIModuleSmallLoading.Instance.hide();
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_COMPLETE,this.loadCompleteHandler);
            UIModuleLoader.Instance.removeEventListener(UIModuleEvent.UI_MODULE_PROGRESS,this.onUimoduleLoadProgress);
            if(null != this._func)
            {
               this._func.apply(null,this._funcParams);
            }
            this._func = null;
            this._funcParams = null;
         }
      }
      
      public function updataCharacter(param1:PlayerInfo) : void
      {
         if(this.infoView)
         {
            this.infoView.updataCharacter(param1);
         }
      }
      
      public function get infoView() : MagicStoneInfoView
      {
         return this._infoView;
      }
      
      public function set infoView(param1:MagicStoneInfoView) : void
      {
         this._infoView = param1;
      }
      
      public function get shopFrame() : MagicStoneShopFrame
      {
         return this._shopFrame;
      }
      
      public function set shopFrame(param1:MagicStoneShopFrame) : void
      {
         this._shopFrame = param1;
      }
   }
}
