package magicStone.views
{
   import baglocked.BaglockedManager;
   import beadSystem.views.BeadFeedInfoFrame;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.controls.TextButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.image.MutipleImage;
   import com.pickgliss.ui.image.Scale9CornerImage;
   import com.pickgliss.ui.image.ScaleBitmapImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.BagInfo;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.data.player.PlayerInfo;
   import ddt.events.CellEvent;
   import ddt.events.PlayerPropertyEvent;
   import ddt.manager.KeyboardShortcutsManager;
   import ddt.manager.LanguageMgr;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerConfigManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import flash.display.Bitmap;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import magicStone.MagicStoneManager;
   import magicStone.components.MgStoneCell;
   import magicStone.components.MgStoneLockBtn;
   import magicStone.components.MgStoneUtils;
   import road7th.data.DictionaryEvent;
   
   public class MagicStoneBagView extends Sprite implements Disposeable
   {
      
      private static const PAGE_COUNT:int = 2;
       
      
      private var _bg:MutipleImage;
      
      private var _bagList:MagicStoneBagList;
      
      private var _batCombBtn:SimpleBitmapButton;
      
      private var _lockBtn:MgStoneLockBtn;
      
      private var _sortBtn:TextButton;
      
      private var _prevBtn:BaseButton;
      
      private var _nextBtn:BaseButton;
      
      private var _pageBg:Scale9CornerImage;
      
      private var _pageTxt:FilterFrameText;
      
      private var _moneyBg:ScaleBitmapImage;
      
      private var _bindMoneyBg:ScaleBitmapImage;
      
      private var _moneyIcon:Bitmap;
      
      private var _bindMoneyIcon:Bitmap;
      
      private var _moneyTxt:FilterFrameText;
      
      private var _bindMoneyTxt:FilterFrameText;
      
      private var _combineLightMC:MovieClip;
      
      private var _curPage:int;
      
      private var _combineArr:Array;
      
      private var _highLevelArr:Array;
      
      private var _allExp:int;
      
      private var _isPlayMc:Boolean = false;
      
      private var _updateItem:InventoryItemInfo;
      
      public function MagicStoneBagView()
      {
         this._combineArr = [];
         this._highLevelArr = [];
         super();
         this.initView();
         this.initData();
         this.initEvents();
      }
      
      private function initView() : void
      {
         this._bg = ComponentFactory.Instance.creatComponentByStylename("magicStone.bagViewBg");
         addChild(this._bg);
         this._bagList = new MagicStoneBagList(0,8,56);
         PositionUtils.setPos(this._bagList,"magicStone.bagListPos");
         addChild(this._bagList);
         this._batCombBtn = ComponentFactory.Instance.creatComponentByStylename("magicStone.batCombBtn");
         addChild(this._batCombBtn);
         this._batCombBtn.tipData = LanguageMgr.GetTranslation("magicStone.batCombineTips");
         this._lockBtn = ComponentFactory.Instance.creatComponentByStylename("magicStone.lockBtn");
         addChild(this._lockBtn);
         this._lockBtn.tipData = LanguageMgr.GetTranslation("magicStone.lockTips");
         this._sortBtn = ComponentFactory.Instance.creatComponentByStylename("magicStone.sortBtn");
         this._sortBtn.text = LanguageMgr.GetTranslation("ddt.beadSystem.sortBtnTxt");
         addChild(this._sortBtn);
         this._moneyBg = ComponentFactory.Instance.creat("magicStone.momeyBg");
         addChild(this._moneyBg);
         this._moneyBg.tipData = LanguageMgr.GetTranslation("tank.view.bagII.GoldDirections");
         this._bindMoneyBg = ComponentFactory.Instance.creat("magicStone.bindMomeyBg");
         addChild(this._bindMoneyBg);
         var _loc1_:int = 60000;
         var _loc2_:int = int(ServerConfigManager.instance.VIPExtraBindMoneyUpper[PlayerManager.Instance.Self.VIPLevel - 1]);
         this._bindMoneyBg.tipData = LanguageMgr.GetTranslation("tank.view.bagII.GiftDirections",(_loc1_ + _loc2_).toString());
         this._moneyIcon = ComponentFactory.Instance.creatBitmap("bagAndInfo.info.PointCoupon");
         PositionUtils.setPos(this._moneyIcon,"magicStone.moneyIconPos");
         addChild(this._moneyIcon);
         this._bindMoneyIcon = ComponentFactory.Instance.creatBitmap("bagAndInfo.info.ddtMoney1");
         PositionUtils.setPos(this._bindMoneyIcon,"magicStone.bindMoneyIconPos");
         addChild(this._bindMoneyIcon);
         this._moneyTxt = ComponentFactory.Instance.creatComponentByStylename("BagMoneyInfoText");
         PositionUtils.setPos(this._moneyTxt,"magicStone.moneyTxtPos");
         addChild(this._moneyTxt);
         this._bindMoneyTxt = ComponentFactory.Instance.creatComponentByStylename("BagGiftInfoText");
         PositionUtils.setPos(this._bindMoneyTxt,"magicStone.bindMoneyTxtPos");
         addChild(this._bindMoneyTxt);
         this._prevBtn = ComponentFactory.Instance.creatComponentByStylename("magicStone.prevBtn");
         addChild(this._prevBtn);
         this._nextBtn = ComponentFactory.Instance.creatComponentByStylename("magicStone.nextBtn");
         addChild(this._nextBtn);
         this._pageBg = ComponentFactory.Instance.creatComponentByStylename("magicStone.pageBG");
         addChild(this._pageBg);
         this._pageTxt = ComponentFactory.Instance.creatComponentByStylename("magicStone.pageTxt");
         addChild(this._pageTxt);
         this._pageTxt.text = "1/2";
      }
      
      private function initData() : void
      {
         this.curPage = 1;
         this._bagList.setData(PlayerManager.Instance.Self.magicStoneBag);
         this.updateMoney();
      }
      
      private function initEvents() : void
      {
         PlayerManager.Instance.Self.addEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__propertyChange);
         PlayerManager.Instance.Self.magicStoneBag.items.addEventListener(DictionaryEvent.ADD,this.__magicStoneAdd);
         this._bagList.addEventListener(CellEvent.ITEM_CLICK,this.__cellClick);
         this._bagList.addEventListener(CellEvent.DOUBLE_CLICK,this.__cellDoubleClick);
         this._batCombBtn.addEventListener(MouseEvent.CLICK,this.__batCombBtnClick);
         this._lockBtn.addEventListener(MouseEvent.CLICK,this.__lockBtnClick);
         this._sortBtn.addEventListener(MouseEvent.CLICK,this.__sortBtnClick);
         this._prevBtn.addEventListener(MouseEvent.CLICK,this.__prevBtnClick);
         this._nextBtn.addEventListener(MouseEvent.CLICK,this.__nextBtnClick);
      }
      
      private function __propertyChange(param1:PlayerPropertyEvent) : void
      {
         if(param1.changedProperties[PlayerInfo.BandMONEY] || param1.changedProperties[PlayerInfo.MONEY])
         {
            this.updateMoney();
         }
      }
      
      private function updateMoney() : void
      {
         if(this._moneyTxt)
         {
            this._moneyTxt.text = PlayerManager.Instance.Self.Money.toString();
         }
         if(this._bindMoneyTxt)
         {
            this._bindMoneyTxt.text = PlayerManager.Instance.Self.BandMoney.toString();
         }
      }
      
      private function __magicStoneAdd(param1:DictionaryEvent) : void
      {
         var _loc2_:InventoryItemInfo = InventoryItemInfo(param1.data);
         var _loc3_:int = (_loc2_.Place - MgStoneUtils.BAG_START) / MgStoneUtils.PAGE_COUNT + 1;
         if(_loc3_ <= 0 || _loc3_ > PAGE_COUNT || _loc3_ == this.curPage)
         {
            return;
         }
         this.curPage = _loc3_;
         this._bagList.updateBagList();
      }
      
      protected function __cellClick(param1:CellEvent) : void
      {
         var _loc3_:InventoryItemInfo = null;
         param1.stopImmediatePropagation();
         var _loc2_:MgStoneCell = param1.data as MgStoneCell;
         if(_loc2_)
         {
            _loc3_ = _loc2_.itemInfo as InventoryItemInfo;
         }
         if(_loc3_ == null)
         {
            return;
         }
         if(!_loc2_.locked)
         {
            SoundManager.instance.play("008");
            _loc2_.dragStart();
         }
      }
      
      protected function __cellDoubleClick(param1:CellEvent) : void
      {
         var _loc3_:InventoryItemInfo = null;
         var _loc6_:int = 0;
         param1.stopImmediatePropagation();
         var _loc2_:MgStoneCell = param1.data as MgStoneCell;
         if(_loc2_)
         {
            _loc3_ = _loc2_.itemInfo as InventoryItemInfo;
         }
         if(_loc3_ == null)
         {
            return;
         }
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         var _loc4_:BagInfo = PlayerManager.Instance.Self.magicStoneBag;
         var _loc5_:int = 0;
         while(_loc5_ <= 8)
         {
            _loc6_ = MgStoneUtils.getPlace(_loc5_);
            if(!_loc4_.getItemAt(_loc6_))
            {
               SocketManager.Instance.out.moveMagicStone(_loc3_.Place,_loc6_);
               break;
            }
            _loc5_++;
         }
      }
      
      protected function __batCombBtnClick(param1:MouseEvent) : void
      {
         var _loc7_:InventoryItemInfo = null;
         SoundManager.instance.play("008");
         this._updateItem = PlayerManager.Instance.Self.magicStoneBag.getItemAt(MagicStoneInfoView.UPDATE_CELL);
         if(!this._updateItem)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("magicStone.updateCellEmpty"));
            return;
         }
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         if(this._updateItem.Level >= 10)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("magicStone.updateCellMaxLevel"));
            return;
         }
         var _loc2_:int = this._updateItem.StrengthenExp;
         var _loc3_:int = MagicStoneManager.instance.getNeedExp(this._updateItem.TemplateID,10);
         var _loc4_:int = MgStoneUtils.BAG_START + (this._curPage - 1) * MgStoneUtils.PAGE_COUNT;
         var _loc5_:int = MgStoneUtils.BAG_START + this._curPage * MgStoneUtils.PAGE_COUNT - 1;
         this._allExp = 0;
         this._combineArr = [];
         this._highLevelArr = [];
         var _loc6_:int = _loc4_;
         while(_loc6_ <= _loc5_)
         {
            _loc7_ = PlayerManager.Instance.Self.magicStoneBag.getItemAt(_loc6_);
            if(_loc7_ && !_loc7_.goodsLock)
            {
               if(!(_loc7_.Level >= 10 && (int(_loc7_.Property3) >= 4 || _loc7_.Quality >= 7)))
               {
                  if(int(_loc7_.Property3) != 0 && (int(_loc7_.Property3) >= 3 || _loc7_.Level >= 7 || _loc7_.Quality >= 7))
                  {
                     this._highLevelArr.push(_loc7_);
                  }
                  else
                  {
                     this._allExp = this._allExp + _loc7_.StrengthenExp;
                     this._combineArr.push(_loc7_.Place);
                     if(this._allExp + _loc2_ >= _loc3_)
                     {
                        break;
                     }
                  }
               }
            }
            _loc6_++;
         }
         if(this._combineArr.length == 0 && this._highLevelArr.length == 0)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("magicStone.bagEmpty"));
            return;
         }
         if(this._combineArr.length > 0)
         {
            this._isPlayMc = true;
            this.combineConfirmAlert();
         }
         else if(this._highLevelArr.length > 0)
         {
            this.highLevelAlert();
         }
      }
      
      private function combineConfirmAlert() : void
      {
         var _loc1_:BaseAlerFrame = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("tips"),LanguageMgr.GetTranslation("magicStone.getExp",this._allExp),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),false,true,false,LayerManager.ALPHA_BLOCKGOUND);
         var _loc2_:FilterFrameText = ComponentFactory.Instance.creatComponentByStylename("magicStone.ensureAlertTxt");
         _loc2_.htmlText = LanguageMgr.GetTranslation("magicStone.getExp",this._allExp);
         _loc1_.addEventListener(FrameEvent.RESPONSE,this.__onCombineResponse);
      }
      
      private function highLevelAlert() : void
      {
         var _loc1_:InventoryItemInfo = this._highLevelArr.pop();
         var _loc2_:int = this._updateItem.StrengthenExp;
         var _loc3_:int = MagicStoneManager.instance.getNeedExp(this._updateItem.TemplateID,10);
         if(_loc2_ + this._allExp >= _loc3_)
         {
            return;
         }
         var _loc4_:BeadFeedInfoFrame = ComponentFactory.Instance.creat("BeadFeedInfoFrame");
         _loc4_.setBeadName(_loc1_.Name + "(ур. " + _loc1_.Level + ")");
         _loc4_.itemInfo = _loc1_;
         LayerManager.Instance.addToLayer(_loc4_,LayerManager.STAGE_DYANMIC_LAYER,true,LayerManager.BLCAK_BLOCKGOUND);
         _loc4_.textInput.setFocus();
         _loc4_.addEventListener(FrameEvent.RESPONSE,this.__onConfirmResponse);
      }
      
      protected function __onConfirmResponse(param1:FrameEvent) : void
      {
         var _loc2_:BeadFeedInfoFrame = param1.currentTarget as BeadFeedInfoFrame;
         SoundManager.instance.playButtonSound();
         switch(param1.responseCode)
         {
            case FrameEvent.SUBMIT_CLICK:
            case FrameEvent.ENTER_CLICK:
               if(_loc2_.textInput.text == "YES" || _loc2_.textInput.text == "yes")
               {
                  this._allExp = _loc2_.itemInfo.StrengthenExp;
                  this._combineArr = [];
                  this._combineArr.push(_loc2_.itemInfo.Place);
                  this.combineConfirmAlert();
                  _loc2_.removeEventListener(FrameEvent.RESPONSE,this.__onConfirmResponse);
                  ObjectUtils.disposeObject(_loc2_);
               }
               else
               {
                  MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("magicStone.pleaseInputYes"));
               }
               break;
            default:
               if(this._highLevelArr.length > 0)
               {
                  this.highLevelAlert();
               }
               _loc2_.removeEventListener(FrameEvent.RESPONSE,this.__onConfirmResponse);
               ObjectUtils.disposeObject(_loc2_);
         }
      }
      
      protected function __onCombineResponse(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         switch(param1.responseCode)
         {
            case FrameEvent.ESC_CLICK:
            case FrameEvent.CLOSE_CLICK:
            case FrameEvent.CANCEL_CLICK:
               if(this._isPlayMc)
               {
                  this._isPlayMc = false;
                  break;
               }
               if(this._highLevelArr.length > 0)
               {
                  this.highLevelAlert();
               }
               break;
            case FrameEvent.SUBMIT_CLICK:
            case FrameEvent.ENTER_CLICK:
               if(this._isPlayMc)
               {
                  if(!this._combineLightMC)
                  {
                     KeyboardShortcutsManager.Instance.forbiddenFull();
                     this._combineLightMC = ComponentFactory.Instance.creat("magicStone.combineLightMc");
                     this._combineLightMC.gotoAndPlay(1);
                     this._combineLightMC.scaleX = this._combineLightMC.scaleY = 0.9;
                     PositionUtils.setPos(this._combineLightMC,"magicStone.combineLightMcPos");
                     this._combineLightMC.addEventListener(Event.ENTER_FRAME,this.__disposeCombineLightMC);
                     LayerManager.Instance.addToLayer(this._combineLightMC,LayerManager.STAGE_TOP_LAYER,false,LayerManager.BLCAK_BLOCKGOUND,true);
                  }
               }
               else
               {
                  this.updateMagicStone();
                  if(this._highLevelArr.length > 0)
                  {
                     this.highLevelAlert();
                  }
               }
         }
         param1.currentTarget.removeEventListener(FrameEvent.RESPONSE,this.__onCombineResponse);
         ObjectUtils.disposeObject(param1.currentTarget);
      }
      
      protected function __disposeCombineLightMC(param1:Event) : void
      {
         if(this._combineLightMC.currentFrame == this._combineLightMC.totalFrames)
         {
            this._combineLightMC.gotoAndStop(1);
            this._combineLightMC.removeEventListener(Event.ENTER_FRAME,this.__disposeCombineLightMC);
            ObjectUtils.disposeObject(this._combineLightMC);
            this._combineLightMC = null;
            this._isPlayMc = false;
            KeyboardShortcutsManager.Instance.cancelForbidden();
            this.updateMagicStone();
            if(this._highLevelArr.length > 0)
            {
               this.highLevelAlert();
            }
         }
      }
      
      private function updateMagicStone() : void
      {
         SocketManager.Instance.out.updateMagicStone(this._combineArr);
         var _loc1_:int = this._updateItem.StrengthenExp;
         var _loc2_:int = MagicStoneManager.instance.getNeedExp(this._updateItem.TemplateID,this._updateItem.StrengthenLevel + 1);
         if(_loc2_ != 0 && this._allExp + _loc1_ >= _loc2_)
         {
            MagicStoneManager.instance.playUpgradeMc();
         }
      }
      
      protected function __lockBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
      }
      
      protected function __sortBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         var _loc2_:BagInfo = PlayerManager.Instance.Self.magicStoneBag;
         PlayerManager.Instance.Self.PropBag.sortBag(BagInfo.MAGICSTONE,_loc2_,MgStoneUtils.BAG_START,MgStoneUtils.BAG_END);
      }
      
      protected function __prevBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         if(this.curPage > 1)
         {
            this.curPage--;
            this._bagList.updateBagList();
         }
      }
      
      protected function __nextBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         if(this.curPage < PAGE_COUNT)
         {
            this.curPage++;
            this._bagList.updateBagList();
         }
      }
      
      public function get curPage() : int
      {
         return this._curPage;
      }
      
      public function set curPage(param1:int) : void
      {
         this._curPage = param1;
         this._bagList.curPage = this._curPage;
         this._pageTxt.text = this._curPage + "/" + PAGE_COUNT;
      }
      
      private function removeEvents() : void
      {
         PlayerManager.Instance.Self.removeEventListener(PlayerPropertyEvent.PROPERTY_CHANGE,this.__propertyChange);
         PlayerManager.Instance.Self.magicStoneBag.items.removeEventListener(DictionaryEvent.ADD,this.__magicStoneAdd);
         this._bagList.removeEventListener(CellEvent.ITEM_CLICK,this.__cellClick);
         this._bagList.removeEventListener(CellEvent.DOUBLE_CLICK,this.__cellDoubleClick);
         this._batCombBtn.removeEventListener(MouseEvent.CLICK,this.__batCombBtnClick);
         this._lockBtn.removeEventListener(MouseEvent.CLICK,this.__lockBtnClick);
         this._sortBtn.removeEventListener(MouseEvent.CLICK,this.__sortBtnClick);
         this._prevBtn.removeEventListener(MouseEvent.CLICK,this.__prevBtnClick);
         this._nextBtn.removeEventListener(MouseEvent.CLICK,this.__nextBtnClick);
      }
      
      public function dispose() : void
      {
         this.removeEvents();
         ObjectUtils.disposeObject(this._bg);
         this._bg = null;
         ObjectUtils.disposeObject(this._bagList);
         this._bagList = null;
         ObjectUtils.disposeObject(this._batCombBtn);
         this._batCombBtn = null;
         ObjectUtils.disposeObject(this._lockBtn);
         this._lockBtn = null;
         ObjectUtils.disposeObject(this._sortBtn);
         this._sortBtn = null;
         ObjectUtils.disposeObject(this._prevBtn);
         this._prevBtn = null;
         ObjectUtils.disposeObject(this._nextBtn);
         this._nextBtn = null;
         ObjectUtils.disposeObject(this._pageBg);
         this._pageBg = null;
         ObjectUtils.disposeObject(this._pageTxt);
         this._pageTxt = null;
         ObjectUtils.disposeObject(this._moneyBg);
         this._moneyBg = null;
         ObjectUtils.disposeObject(this._moneyIcon);
         this._moneyIcon = null;
         ObjectUtils.disposeObject(this._moneyTxt);
         this._moneyTxt = null;
         ObjectUtils.disposeObject(this._bindMoneyBg);
         this._bindMoneyBg = null;
         ObjectUtils.disposeObject(this._bindMoneyIcon);
         this._bindMoneyIcon = null;
         ObjectUtils.disposeObject(this._bindMoneyTxt);
         this._bindMoneyTxt = null;
      }
   }
}
