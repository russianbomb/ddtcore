package magicStone.views
{
   import bagAndInfo.cell.BagCell;
   import bagAndInfo.cell.CellFactory;
   import baglocked.BaglockedManager;
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.events.InteractiveEvent;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.SimpleBitmapButton;
   import com.pickgliss.ui.controls.TextButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.core.Disposeable;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.DoubleClickManager;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.BagInfo;
   import ddt.data.EquipType;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.data.player.PlayerInfo;
   import ddt.data.player.SelfInfo;
   import ddt.events.BagEvent;
   import ddt.events.CellEvent;
   import ddt.manager.LanguageMgr;
   import ddt.manager.LeavePageManager;
   import ddt.manager.MessageTipManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.ServerConfigManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import ddt.utils.PositionUtils;
   import ddt.view.character.CharactoryFactory;
   import ddt.view.character.RoomCharacter;
   import ddt.view.tips.OneLineTip;
   import flash.display.Bitmap;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.geom.Point;
   import flash.utils.Dictionary;
   import magicStone.MagicStoneManager;
   import magicStone.components.EmbedMgStoneCell;
   import magicStone.components.MagicStoneBatFrame;
   import magicStone.components.MagicStoneConfirmView;
   import magicStone.components.MagicStoneProgress;
   import magicStone.components.MgStoneCell;
   import magicStone.components.MgStoneUtils;
   import playerDress.PlayerDressManager;
   import playerDress.components.DressModel;
   import playerDress.components.DressUtils;
   import playerDress.data.DressVo;
   import road7th.data.DictionaryData;
   
   public class MagicStoneInfoView extends Sprite implements Disposeable
   {
      
      private static const CELL_LEN:int = 9;
      
      public static const UPDATE_CELL:int = 31;
       
      
      private var _bg:Bitmap;
      
      private var _lightBg:Bitmap;
      
      private var _whiteStone:Bitmap;
      
      private var _blueStone:Bitmap;
      
      private var _purpleStone:Bitmap;
      
      private var _scoreTxt:FilterFrameText;
      
      private var _covertBtn:TextButton;
      
      private var _exploreBtn:SimpleBitmapButton;
      
      private var _exploreBatBtn:SimpleBitmapButton;
      
      private var _lowStoneLight:Bitmap;
      
      private var _midStoneLight:Bitmap;
      
      private var _highStoneLight:Bitmap;
      
      private var _lowSprite:Sprite;
      
      private var _midSprite:Sprite;
      
      private var _highSprite:Sprite;
      
      private var _oneLineTip:OneLineTip;
      
      private var _lightFilters:Array;
      
      private var _progress:MagicStoneProgress;
      
      private var _mgStoneCells:Vector.<EmbedMgStoneCell>;
      
      private var _cells:Dictionary;
      
      public var selectedIndex:int;
      
      private var _mgStonebag:BagInfo;
      
      private var _batFrame:MagicStoneBatFrame;
      
      private var _character:RoomCharacter;
      
      private var _currentModel:DressModel;
      
      public function MagicStoneInfoView()
      {
         super();
         this.selectedIndex = 1;
         this._mgStoneCells = new Vector.<EmbedMgStoneCell>();
         this._cells = new Dictionary();
         MagicStoneManager.instance.infoView = this;
         this._currentModel = new DressModel();
         this.updateModel();
         this.initView();
         this.initData();
         this.initEvent();
      }
      
      public function updataCharacter(param1:PlayerInfo) : void
      {
         if(this._character)
         {
            this._character.dispose();
            this._character = null;
         }
         this._character = CharactoryFactory.createCharacter(param1,"room") as RoomCharacter;
         this._character.showGun = false;
         this._character.show(false,-1);
         PositionUtils.setPos(this._character,"magicStone.characterPos");
         addChild(this._character);
      }
      
      private function initView() : void
      {
         var _loc2_:int = 0;
         var _loc3_:EmbedMgStoneCell = null;
         this._bg = ComponentFactory.Instance.creat("magicStone.bg");
         addChild(this._bg);
         this._lightBg = ComponentFactory.Instance.creat("magicStone.lightBg");
         addChild(this._lightBg);
         this._scoreTxt = ComponentFactory.Instance.creatComponentByStylename("magicStone.scoreTxt");
         addChild(this._scoreTxt);
         this._scoreTxt.text = "12354";
         this._covertBtn = ComponentFactory.Instance.creatComponentByStylename("magicStone.covertBtn");
         addChild(this._covertBtn);
         this._covertBtn.text = LanguageMgr.GetTranslation("magicStone.covertBtnTxt");
         this._exploreBtn = ComponentFactory.Instance.creatComponentByStylename("magicStone.exploreBtn");
         addChild(this._exploreBtn);
         this._exploreBatBtn = ComponentFactory.Instance.creatComponentByStylename("magicStone.exploreBatBtn");
         addChild(this._exploreBatBtn);
         this._progress = new MagicStoneProgress();
         PositionUtils.setPos(this._progress,"magicStone.progressPos");
         addChild(this._progress);
         this.updataCharacter(this._currentModel.model);
         var _loc1_:int = 0;
         while(_loc1_ <= CELL_LEN - 1)
         {
            _loc2_ = MgStoneUtils.getPlace(_loc1_);
            _loc3_ = CellFactory.instance.createEmbedMgStoneCell(_loc2_) as EmbedMgStoneCell;
            _loc3_.addEventListener(InteractiveEvent.CLICK,this.__cellClickHandler);
            _loc3_.addEventListener(InteractiveEvent.DOUBLE_CLICK,this.__doubleClickHandler);
            DoubleClickManager.Instance.enableDoubleClick(_loc3_);
            PositionUtils.setPos(_loc3_,"magicStone.mgStoneCellPos" + _loc1_);
            addChild(_loc3_);
            this._mgStoneCells.push(_loc3_);
            this._cells[_loc2_] = _loc3_;
            _loc1_++;
         }
         this._lowSprite = new Sprite();
         PositionUtils.setPos(this._lowSprite,"magicStone.whitePos");
         this._lowSprite.graphics.beginFill(255,0);
         this._lowSprite.graphics.drawRoundRect(0,0,77,77,15,15);
         this._lowSprite.graphics.endFill();
         this._lowSprite.buttonMode = true;
         addChild(this._lowSprite);
         this._midSprite = new Sprite();
         PositionUtils.setPos(this._midSprite,"magicStone.bluePos");
         this._midSprite.graphics.beginFill(255,0);
         this._midSprite.graphics.drawRoundRect(0,0,77,77,15,15);
         this._midSprite.graphics.endFill();
         this._midSprite.buttonMode = true;
         addChild(this._midSprite);
         this._highSprite = new Sprite();
         PositionUtils.setPos(this._highSprite,"magicStone.purplePos");
         this._highSprite.graphics.beginFill(255,0);
         this._highSprite.graphics.drawRoundRect(0,0,77,77,15,15);
         this._highSprite.graphics.endFill();
         this._highSprite.buttonMode = true;
         addChild(this._highSprite);
         this._whiteStone = ComponentFactory.Instance.creat("magicStone.white");
         this._lowSprite.addChild(this._whiteStone);
         this._blueStone = ComponentFactory.Instance.creat("magicStone.blue");
         this._midSprite.addChild(this._blueStone);
         this._purpleStone = ComponentFactory.Instance.creat("magicStone.purple");
         this._highSprite.addChild(this._purpleStone);
         this._lightFilters = ComponentFactory.Instance.creatFilters("lightFilter");
         this._lowStoneLight = ComponentFactory.Instance.creat("magicStone.lowStoneLight");
         addChild(this._lowStoneLight);
         this._midStoneLight = ComponentFactory.Instance.creat("magicStone.midStoneLight");
         addChild(this._midStoneLight);
         this._midStoneLight.visible = false;
         this._highStoneLight = ComponentFactory.Instance.creat("magicStone.highStoneLight");
         addChild(this._highStoneLight);
         this._highStoneLight.visible = false;
      }
      
      public function updateModel() : void
      {
         var _loc6_:InventoryItemInfo = null;
         var _loc7_:InventoryItemInfo = null;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         var _loc1_:SelfInfo = PlayerManager.Instance.Self;
         var _loc2_:int = PlayerDressManager.instance.currentIndex;
         if(_loc1_.Sex)
         {
            this._currentModel.model.updateStyle(_loc1_.Sex,_loc1_.Hide,DressModel.DEFAULT_MAN_STYLE,",,,,,,","");
         }
         else
         {
            this._currentModel.model.updateStyle(_loc1_.Sex,_loc1_.Hide,DressModel.DEFAULT_WOMAN_STYLE,",,,,,,","");
         }
         var _loc3_:DictionaryData = new DictionaryData();
         var _loc4_:Array = PlayerDressManager.instance.modelArr[_loc2_];
         var _loc5_:Boolean = false;
         if(_loc4_)
         {
            _loc8_ = 0;
            while(_loc8_ <= _loc4_.length - 1)
            {
               _loc9_ = (_loc4_[_loc8_] as DressVo).templateId;
               _loc10_ = (_loc4_[_loc8_] as DressVo).itemId;
               _loc7_ = new InventoryItemInfo();
               _loc6_ = _loc1_.Bag.getItemByItemId(_loc10_);
               if(!_loc6_)
               {
                  _loc6_ = _loc1_.Bag.getItemByTemplateId(_loc9_);
                  _loc5_ = true;
               }
               if(_loc6_)
               {
                  _loc7_.setIsUsed(_loc6_.IsUsed);
                  ObjectUtils.copyProperties(_loc7_,_loc6_);
                  _loc11_ = DressUtils.findItemPlace(_loc7_);
                  _loc3_.add(_loc11_,_loc7_);
                  if(_loc7_.CategoryID == EquipType.FACE)
                  {
                     this._currentModel.model.Skin = _loc7_.Skin;
                  }
                  this._currentModel.model.setPartStyle(_loc7_.CategoryID,_loc7_.NeedSex,_loc7_.TemplateID,_loc7_.Color);
               }
               _loc8_++;
            }
         }
         this._currentModel.model.Bag.items = _loc3_;
      }
      
      private function initData() : void
      {
         var _loc2_:int = 0;
         var _loc3_:InventoryItemInfo = null;
         this._mgStonebag = PlayerManager.Instance.Self.magicStoneBag;
         this.clearCells();
         var _loc1_:int = 0;
         while(_loc1_ <= CELL_LEN - 1)
         {
            _loc2_ = MgStoneUtils.getPlace(_loc1_);
            _loc3_ = this._mgStonebag.getItemAt(_loc2_);
            if(_loc3_)
            {
               this.setCellInfo(_loc3_.Place,_loc3_);
            }
            _loc1_++;
         }
         this.updateProgress();
      }
      
      private function updateProgress() : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc1_:InventoryItemInfo = this._mgStonebag.getItemAt(UPDATE_CELL);
         if(_loc1_)
         {
            _loc2_ = _loc1_.StrengthenExp - MagicStoneManager.instance.getNeedExp(_loc1_.TemplateID,_loc1_.StrengthenLevel);
            _loc3_ = MagicStoneManager.instance.getNeedExpPerLevel(_loc1_.TemplateID,_loc1_.StrengthenLevel + 1);
            this._progress.setData(_loc2_,_loc3_);
         }
         else
         {
            this._progress.setData(0,0);
         }
      }
      
      private function clearCells() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ <= CELL_LEN - 1)
         {
            this._mgStoneCells[_loc1_].info = null;
            _loc1_++;
         }
      }
      
      private function initEvent() : void
      {
         addEventListener(CellEvent.ITEM_CLICK,this.__cellClick);
         this._covertBtn.addEventListener(MouseEvent.CLICK,this.__covertBtnClick);
         this._exploreBtn.addEventListener(MouseEvent.CLICK,this.__exploreBtnClick);
         this._exploreBatBtn.addEventListener(MouseEvent.CLICK,this.__exploreBatBtnClick);
         this._lowSprite.addEventListener(MouseEvent.CLICK,this.__selectedHandler);
         this._lowSprite.addEventListener(MouseEvent.MOUSE_OVER,this.__mouseOverHandler);
         this._lowSprite.addEventListener(MouseEvent.MOUSE_OUT,this.__mouseOutHandler);
         this._midSprite.addEventListener(MouseEvent.CLICK,this.__selectedHandler);
         this._midSprite.addEventListener(MouseEvent.MOUSE_OVER,this.__mouseOverHandler);
         this._midSprite.addEventListener(MouseEvent.MOUSE_OUT,this.__mouseOutHandler);
         this._highSprite.addEventListener(MouseEvent.CLICK,this.__selectedHandler);
         this._highSprite.addEventListener(MouseEvent.MOUSE_OVER,this.__mouseOverHandler);
         this._highSprite.addEventListener(MouseEvent.MOUSE_OUT,this.__mouseOutHandler);
         PlayerManager.Instance.Self.magicStoneBag.addEventListener(BagEvent.UPDATE,this.__updateGoods);
      }
      
      protected function __cellClickHandler(param1:InteractiveEvent) : void
      {
         if((param1.currentTarget as BagCell).info != null)
         {
            dispatchEvent(new CellEvent(CellEvent.ITEM_CLICK,param1.currentTarget,false,false,param1.ctrlKey));
         }
      }
      
      protected function __doubleClickHandler(param1:InteractiveEvent) : void
      {
         var _loc2_:InventoryItemInfo = (param1.currentTarget as BagCell).info as InventoryItemInfo;
         if(_loc2_ != null)
         {
            SoundManager.instance.play("008");
            if(this.isBagFull())
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("magicStone.bagFull"));
            }
            else
            {
               SocketManager.Instance.out.moveMagicStone(_loc2_.Place,-1);
            }
         }
      }
      
      private function isBagFull() : Boolean
      {
         var _loc2_:InventoryItemInfo = null;
         var _loc1_:int = MgStoneUtils.BAG_START;
         while(_loc1_ <= MgStoneUtils.BAG_END)
         {
            _loc2_ = this._mgStonebag.getItemAt(_loc1_);
            if(!_loc2_)
            {
               return false;
            }
            _loc1_++;
         }
         return true;
      }
      
      protected function __cellClick(param1:CellEvent) : void
      {
         var _loc3_:InventoryItemInfo = null;
         param1.stopImmediatePropagation();
         var _loc2_:MgStoneCell = param1.data as MgStoneCell;
         if(_loc2_)
         {
            _loc3_ = _loc2_.itemInfo as InventoryItemInfo;
         }
         if(_loc3_ == null)
         {
            return;
         }
         if(!_loc2_.locked)
         {
            SoundManager.instance.play("008");
            _loc2_.dragStart();
         }
      }
      
      protected function __updateGoods(param1:BagEvent) : void
      {
         var _loc3_:InventoryItemInfo = null;
         var _loc4_:InventoryItemInfo = null;
         var _loc2_:Dictionary = param1.changedSlots;
         for each(_loc3_ in _loc2_)
         {
            if(_loc3_.Place >= 0 && _loc3_.Place <= UPDATE_CELL)
            {
               _loc4_ = this._mgStonebag.getItemAt(_loc3_.Place);
               if(_loc4_)
               {
                  this.setCellInfo(_loc4_.Place,_loc4_);
               }
               else
               {
                  this.setCellInfo(_loc3_.Place,null);
               }
               dispatchEvent(new Event(Event.CHANGE));
            }
         }
         this.updateProgress();
      }
      
      public function setCellInfo(param1:int, param2:InventoryItemInfo) : void
      {
         var _loc3_:String = String(param1);
         if(param2 == null)
         {
            if(this._cells[_loc3_])
            {
               this._cells[_loc3_].info = null;
            }
            return;
         }
         if(param2.Count == 0)
         {
            this._cells[_loc3_].info = null;
         }
         else
         {
            this._cells[_loc3_].info = param2;
         }
      }
      
      protected function __covertBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         var _loc2_:MagicStoneShopFrame = ComponentFactory.Instance.creatCustomObject("magicStone.magicStoneShopFrame");
         _loc2_.addEventListener(FrameEvent.RESPONSE,this.__frameEvent);
         _loc2_.show();
      }
      
      protected function __frameEvent(param1:FrameEvent) : void
      {
         SoundManager.instance.play("008");
         var _loc2_:Disposeable = param1.target as Disposeable;
         _loc2_.dispose();
         _loc2_ = null;
      }
      
      protected function __exploreBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         var _loc2_:int = this.getNeedMoney();
         if(MagicStoneManager.instance.isNoPrompt)
         {
            if(MagicStoneManager.instance.isBand && PlayerManager.Instance.Self.BandMoney < _loc2_)
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("bindMoneyPoorNote"));
               MagicStoneManager.instance.isNoPrompt = false;
            }
            else if(!MagicStoneManager.instance.isBand && PlayerManager.Instance.Self.Money < _loc2_)
            {
               MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("moneyPoorNote"));
               MagicStoneManager.instance.isNoPrompt = false;
            }
            else
            {
               SocketManager.Instance.out.exploreMagicStone(this.selectedIndex,MagicStoneManager.instance.isBand);
               return;
            }
         }
         var _loc3_:BaseAlerFrame = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("magicStone.exploreConfirmTxt",this.getNeedMoney()),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),true,true,true,LayerManager.BLCAK_BLOCKGOUND,null,"magicStone.confirmView",30,true,AlertManager.SELECTBTN);
         _loc3_.moveEnable = false;
         _loc3_.addEventListener(FrameEvent.RESPONSE,this.comfirmHandler,false,0,true);
      }
      
      private function comfirmHandler(param1:FrameEvent) : void
      {
         var _loc3_:int = 0;
         var _loc4_:BaseAlerFrame = null;
         SoundManager.instance.play("008");
         var _loc2_:BaseAlerFrame = param1.currentTarget as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.comfirmHandler);
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            _loc3_ = this.getNeedMoney();
            if(_loc2_.isBand && PlayerManager.Instance.Self.BandMoney < _loc3_)
            {
               _loc4_ = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),LanguageMgr.GetTranslation("sevenDouble.game.useSkillNoEnoughReConfirm"),LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),true,true,true,LayerManager.BLCAK_BLOCKGOUND);
               _loc4_.moveEnable = false;
               _loc4_.addEventListener(FrameEvent.RESPONSE,this.reConfirmHandler,false,0,true);
               return;
            }
            if(!_loc2_.isBand && PlayerManager.Instance.Self.Money < _loc3_)
            {
               LeavePageManager.showFillFrame();
               return;
            }
            if((_loc2_ as MagicStoneConfirmView).isNoPrompt)
            {
               MagicStoneManager.instance.isNoPrompt = true;
               MagicStoneManager.instance.isBand = _loc2_.isBand;
            }
            SocketManager.Instance.out.exploreMagicStone(this.selectedIndex,_loc2_.isBand);
         }
      }
      
      private function reConfirmHandler(param1:FrameEvent) : void
      {
         var _loc3_:int = 0;
         SoundManager.instance.play("008");
         var _loc2_:BaseAlerFrame = param1.currentTarget as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.reConfirmHandler);
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            _loc3_ = this.getNeedMoney();
            if(PlayerManager.Instance.Self.Money < _loc3_)
            {
               LeavePageManager.showFillFrame();
               return;
            }
            SocketManager.Instance.out.exploreMagicStone(this.selectedIndex,_loc2_.isBand);
         }
      }
      
      public function getNeedMoney() : int
      {
         var _loc2_:String = null;
         var _loc3_:Array = null;
         var _loc4_:Array = null;
         var _loc1_:Object = ServerConfigManager.instance.serverConfigInfo["OpenMagicBoxMoney"];
         if(_loc1_)
         {
            _loc2_ = _loc1_.Value;
            if(_loc2_ && _loc2_ != "")
            {
               _loc3_ = _loc2_.split("|");
               if(_loc3_[this.selectedIndex - 1])
               {
                  _loc4_ = _loc3_[this.selectedIndex - 1].split(",");
                  return parseInt(_loc4_[0]);
               }
            }
         }
         return 0;
      }
      
      public function getNeedMoney2(param1:int) : int
      {
         var _loc3_:String = null;
         var _loc4_:Array = null;
         var _loc5_:Array = null;
         var _loc2_:Object = ServerConfigManager.instance.serverConfigInfo["OpenMagicBoxMoney"];
         if(_loc2_)
         {
            _loc3_ = _loc2_.Value;
            if(_loc3_ && _loc3_ != "")
            {
               _loc4_ = _loc3_.split("|");
               if(_loc4_[param1])
               {
                  _loc5_ = _loc4_[param1].split(",");
                  return parseInt(_loc5_[0]);
               }
            }
         }
         return 0;
      }
      
      protected function __exploreBatBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         if(PlayerManager.Instance.Self.bagLocked)
         {
            BaglockedManager.Instance.show();
            return;
         }
         var _loc2_:int = this.getBagRemain();
         if(_loc2_ == 0)
         {
            MessageTipManager.getInstance().show(LanguageMgr.GetTranslation("magicStone.bagFull"));
         }
         else
         {
            this._batFrame = ComponentFactory.Instance.creat("magicStone.batFrame");
            this._batFrame.init2(0);
            this._batFrame.setNumMax(this.getBagRemain());
            this._batFrame.updateTotalCost();
            this._batFrame.show();
         }
      }
      
      private function getBagRemain() : int
      {
         var _loc3_:InventoryItemInfo = null;
         var _loc1_:int = 0;
         var _loc2_:int = MgStoneUtils.BAG_START;
         while(_loc2_ <= MgStoneUtils.BAG_END)
         {
            _loc3_ = this._mgStonebag.getItemAt(_loc2_);
            if(!_loc3_)
            {
               _loc1_++;
            }
            _loc2_++;
         }
         return _loc1_;
      }
      
      protected function __selectedHandler(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         this._lowStoneLight.visible = false;
         this._midStoneLight.visible = false;
         this._highStoneLight.visible = false;
         switch(param1.currentTarget)
         {
            case this._lowSprite:
               this.selectedIndex = 1;
               this._lowStoneLight.visible = true;
               break;
            case this._midSprite:
               this.selectedIndex = 2;
               this._midStoneLight.visible = true;
               break;
            case this._highSprite:
               this.selectedIndex = 3;
               this._highStoneLight.visible = true;
         }
      }
      
      protected function __mouseOverHandler(param1:MouseEvent) : void
      {
         var _loc4_:int = 0;
         var _loc2_:Sprite = param1.currentTarget as Sprite;
         var _loc3_:Point = localToGlobal(new Point(_loc2_.x,_loc2_.y));
         this._oneLineTip = new OneLineTip();
         LayerManager.Instance.addToLayer(this._oneLineTip,LayerManager.GAME_DYNAMIC_LAYER,false,LayerManager.NONE_BLOCKGOUND);
         this._oneLineTip.x = _loc3_.x - 30;
         this._oneLineTip.y = _loc3_.y - 45;
         switch(_loc2_)
         {
            case this._lowSprite:
               _loc4_ = this.getNeedMoney2(0);
               this._oneLineTip.tipData = LanguageMgr.GetTranslation("magicStone.lowSpriteTip",_loc4_);
               break;
            case this._midSprite:
               _loc4_ = this.getNeedMoney2(1);
               this._oneLineTip.tipData = LanguageMgr.GetTranslation("magicStone.midSpriteTip",_loc4_);
               break;
            case this._highSprite:
               _loc4_ = this.getNeedMoney2(2);
               this._oneLineTip.tipData = LanguageMgr.GetTranslation("magicStone.highSpriteTip",_loc4_);
         }
         this._lowSprite.filters = [];
         this._midSprite.filters = [];
         this._highSprite.filters = [];
         _loc2_.filters = this._lightFilters;
      }
      
      protected function __mouseOutHandler(param1:MouseEvent) : void
      {
         ObjectUtils.disposeObject(this._oneLineTip);
         this._oneLineTip = null;
         this._lowSprite.filters = [];
         this._midSprite.filters = [];
         this._highSprite.filters = [];
      }
      
      public function updateScore(param1:int) : void
      {
         this._scoreTxt.text = param1.toString();
      }
      
      private function removeEvents() : void
      {
         removeEventListener(CellEvent.ITEM_CLICK,this.__cellClick);
         this._covertBtn.removeEventListener(MouseEvent.CLICK,this.__covertBtnClick);
         this._exploreBtn.removeEventListener(MouseEvent.CLICK,this.__exploreBtnClick);
         this._exploreBatBtn.removeEventListener(MouseEvent.CLICK,this.__exploreBatBtnClick);
         this._lowSprite.removeEventListener(MouseEvent.CLICK,this.__selectedHandler);
         this._lowSprite.removeEventListener(MouseEvent.MOUSE_OVER,this.__mouseOverHandler);
         this._lowSprite.removeEventListener(MouseEvent.MOUSE_OUT,this.__mouseOutHandler);
         this._midSprite.removeEventListener(MouseEvent.CLICK,this.__selectedHandler);
         this._midSprite.removeEventListener(MouseEvent.MOUSE_OVER,this.__mouseOverHandler);
         this._midSprite.removeEventListener(MouseEvent.MOUSE_OUT,this.__mouseOutHandler);
         this._highSprite.removeEventListener(MouseEvent.CLICK,this.__selectedHandler);
         this._highSprite.removeEventListener(MouseEvent.MOUSE_OVER,this.__mouseOverHandler);
         this._highSprite.removeEventListener(MouseEvent.MOUSE_OUT,this.__mouseOutHandler);
         PlayerManager.Instance.Self.magicStoneBag.removeEventListener(BagEvent.UPDATE,this.__updateGoods);
         var _loc1_:int = 0;
         while(_loc1_ <= this._mgStoneCells.length - 1)
         {
            if(this._mgStoneCells[_loc1_])
            {
               this._mgStoneCells[_loc1_].removeEventListener(InteractiveEvent.CLICK,this.__cellClickHandler);
               this._mgStoneCells[_loc1_].removeEventListener(InteractiveEvent.DOUBLE_CLICK,this.__doubleClickHandler);
            }
            _loc1_++;
         }
      }
      
      public function dispose() : void
      {
         MagicStoneManager.instance.infoView = null;
         if(this._cells[UPDATE_CELL] && this._cells[UPDATE_CELL].info)
         {
            if(!this.isBagFull())
            {
               SocketManager.Instance.out.moveMagicStone(UPDATE_CELL,-1);
            }
         }
         this.removeEvents();
         var _loc1_:int = 0;
         while(_loc1_ <= this._mgStoneCells.length - 1)
         {
            ObjectUtils.disposeObject(this._mgStoneCells[_loc1_]);
            this._mgStoneCells[_loc1_] = null;
            _loc1_++;
         }
         this._cells = null;
         ObjectUtils.disposeObject(this._bg);
         this._bg = null;
         ObjectUtils.disposeObject(this._lightBg);
         this._lightBg = null;
         ObjectUtils.disposeObject(this._whiteStone);
         this._whiteStone = null;
         ObjectUtils.disposeObject(this._blueStone);
         this._blueStone = null;
         ObjectUtils.disposeObject(this._purpleStone);
         this._purpleStone = null;
         ObjectUtils.disposeObject(this._scoreTxt);
         this._scoreTxt = null;
         ObjectUtils.disposeObject(this._covertBtn);
         this._covertBtn = null;
         ObjectUtils.disposeObject(this._exploreBtn);
         this._exploreBtn = null;
         ObjectUtils.disposeObject(this._exploreBatBtn);
         this._exploreBatBtn = null;
         ObjectUtils.disposeObject(this._progress);
         this._progress = null;
         ObjectUtils.disposeObject(this._lowStoneLight);
         this._lowStoneLight = null;
         ObjectUtils.disposeObject(this._lowSprite);
         this._lowSprite = null;
         ObjectUtils.disposeObject(this._midSprite);
         this._midSprite = null;
         ObjectUtils.disposeObject(this._highSprite);
         this._highSprite = null;
         ObjectUtils.disposeObject(this._oneLineTip);
         this._oneLineTip = null;
         ObjectUtils.disposeObject(this._character);
         this._character = null;
      }
   }
}
