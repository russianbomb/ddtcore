package magicStone.views
{
   import bagAndInfo.bag.BagListView;
   import bagAndInfo.cell.CellFactory;
   import com.pickgliss.events.InteractiveEvent;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.utils.DoubleClickManager;
   import ddt.data.BagInfo;
   import ddt.data.goods.InventoryItemInfo;
   import ddt.events.BagEvent;
   import ddt.events.CellEvent;
   import flash.events.Event;
   import flash.utils.Dictionary;
   import magicStone.components.MgStoneCell;
   import magicStone.components.MgStoneUtils;
   
   public class MagicStoneBagList extends BagListView
   {
       
      
      private var _curPage:int;
      
      private var _startIndex:int;
      
      private var _endIndex:int;
      
      public function MagicStoneBagList(param1:int, param2:int = 7, param3:int = 49)
      {
         this._curPage = 1;
         this._startIndex = MgStoneUtils.BAG_START;
         this._endIndex = MgStoneUtils.BAG_START + param3 - 1;
         super(param1,param2,param3);
      }
      
      override protected function createCells() : void
      {
         var _loc2_:MgStoneCell = null;
         _cells = new Dictionary();
         _cellMouseOverBg = ComponentFactory.Instance.creatBitmap("bagAndInfo.cell.bagCellOverBgAsset");
         var _loc1_:int = MgStoneUtils.BAG_START;
         while(_loc1_ <= MgStoneUtils.BAG_END)
         {
            _loc2_ = MgStoneCell(CellFactory.instance.createMgStoneCell(_loc1_));
            _loc2_.addEventListener(InteractiveEvent.CLICK,__clickHandler);
            _loc2_.addEventListener(InteractiveEvent.DOUBLE_CLICK,__doubleClickHandler);
            DoubleClickManager.Instance.enableDoubleClick(_loc2_);
            _loc2_.addEventListener(CellEvent.LOCK_CHANGED,__cellChanged);
            if(_loc1_ >= this._startIndex && _loc1_ <= this._endIndex)
            {
               addChild(_loc2_);
            }
            _cells[_loc1_] = _loc2_;
            _cellVec.push(_loc2_);
            _loc1_++;
         }
      }
      
      override public function setData(param1:BagInfo) : void
      {
         if(_bagdata == param1)
         {
            return;
         }
         if(_bagdata)
         {
            _bagdata.removeEventListener(BagEvent.UPDATE,this.__updateGoods);
         }
         _bagdata = param1;
         this.updateBagList();
         _bagdata.addEventListener(BagEvent.UPDATE,this.__updateGoods);
      }
      
      override protected function __updateGoods(param1:BagEvent) : void
      {
         var _loc3_:InventoryItemInfo = null;
         var _loc4_:InventoryItemInfo = null;
         var _loc2_:Dictionary = param1.changedSlots;
         for each(_loc3_ in _loc2_)
         {
            if(_loc3_.Place >= MgStoneUtils.BAG_START && _loc3_.Place <= MgStoneUtils.BAG_END)
            {
               _loc4_ = _bagdata.getItemAt(_loc3_.Place);
               if(_loc4_)
               {
                  this.setCellInfo(_loc4_.Place,_loc4_);
               }
               else
               {
                  this.setCellInfo(_loc3_.Place,null);
               }
               dispatchEvent(new Event(Event.CHANGE));
            }
         }
      }
      
      override public function setCellInfo(param1:int, param2:InventoryItemInfo) : void
      {
         var _loc3_:String = String(param1);
         if(param2 == null)
         {
            if(_cells[_loc3_])
            {
               _cells[_loc3_].info = null;
            }
            return;
         }
         if(param2.Count == 0)
         {
            _cells[_loc3_].info = null;
         }
         else
         {
            _cells[_loc3_].info = param2;
         }
      }
      
      public function updateBagList() : void
      {
         var _loc2_:* = null;
         clearDataCells();
         removeAllChild();
         var _loc1_:int = MgStoneUtils.BAG_START;
         while(_loc1_ <= MgStoneUtils.BAG_END)
         {
            if(_loc1_ >= this._startIndex && _loc1_ <= this._endIndex)
            {
               addChild(_cells[_loc1_]);
            }
            _loc1_++;
         }
         for(_loc2_ in _bagdata.items)
         {
            if(_cells[_loc2_] != null)
            {
               _bagdata.items[_loc2_].isMoveSpace = true;
               _cells[_loc2_].info = _bagdata.items[_loc2_];
            }
         }
      }
      
      public function set curPage(param1:int) : void
      {
         this._curPage = param1;
         this._startIndex = MgStoneUtils.BAG_START + (this._curPage - 1) * _cellNum;
         this._endIndex = MgStoneUtils.BAG_START + this._curPage * _cellNum - 1;
      }
      
      override public function dispose() : void
      {
         if(_bagdata)
         {
            _bagdata.removeEventListener(BagEvent.UPDATE,this.__updateGoods);
         }
         super.dispose();
      }
   }
}
