package magicStone.views
{
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.image.Image;
   import com.pickgliss.ui.image.Scale9CornerImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.ui.vo.AlertInfo;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.data.goods.ShopItemInfo;
   import ddt.manager.LanguageMgr;
   import ddt.manager.ShopManager;
   import ddt.manager.SocketManager;
   import ddt.manager.SoundManager;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import magicStone.MagicStoneManager;
   import magicStone.components.MagicStoneShopItem;
   import magicStone.data.MagicStoneEvent;
   import road7th.comm.PackageIn;
   
   public class MagicStoneShopFrame extends BaseAlerFrame
   {
      
      public static const SHOP_ITEM_NUM:uint = 8;
       
      
      private var _goodItems:Vector.<MagicStoneShopItem>;
      
      private var _rightItemLightMc:MovieClip;
      
      private var _goodItemContainerAll:Sprite;
      
      private var _goodItemContainerBg:Image;
      
      private var _navigationBarContainer:Sprite;
      
      private var _prePageBtn:BaseButton;
      
      private var _nextPageBtn:BaseButton;
      
      private var _currentPageTxt:FilterFrameText;
      
      private var _currentPageInput:Scale9CornerImage;
      
      private var _scoreNumBG:Scale9CornerImage;
      
      private var _scoreText:FilterFrameText;
      
      private var _scoreNumText:FilterFrameText;
      
      private var _label:FilterFrameText;
      
      private var curPage:int = 1;
      
      public function MagicStoneShopFrame()
      {
         super();
      }
      
      override protected function init() : void
      {
         super.init();
         MagicStoneManager.instance.shopFrame = this;
         this.initView();
         this.initEvents();
         this.loadList();
      }
      
      private function initView() : void
      {
         var _loc3_:int = 0;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc1_:String = LanguageMgr.GetTranslation("magicStone.shopFrameTitle");
         var _loc2_:AlertInfo = new AlertInfo(_loc1_,"",LanguageMgr.GetTranslation("tank.calendar.Activity.BackButtonText"));
         info = _loc2_;
         this._goodItems = new Vector.<MagicStoneShopItem>();
         this._rightItemLightMc = ComponentFactory.Instance.creatCustomObject("magicStone.shopFrame.RightItemLightMc");
         this._goodItemContainerAll = ComponentFactory.Instance.creatCustomObject("magicStone.shopFrame.GoodItemContainerAll");
         this._goodItemContainerBg = ComponentFactory.Instance.creatComponentByStylename("magicStone.shopFrame.GoodItemContainerBg");
         this._navigationBarContainer = ComponentFactory.Instance.creatCustomObject("magicStone.shopFrame.navigationBarContainer");
         this._prePageBtn = ComponentFactory.Instance.creatComponentByStylename("magicStone.shopFrame.BtnPrePage");
         this._navigationBarContainer.addChild(this._prePageBtn);
         this._nextPageBtn = ComponentFactory.Instance.creatComponentByStylename("magicStone.shopFrame.BtnNextPage");
         this._navigationBarContainer.addChild(this._nextPageBtn);
         this._currentPageInput = ComponentFactory.Instance.creatComponentByStylename("magicStone.shopFrame.CurrentPageInput");
         this._navigationBarContainer.addChild(this._currentPageInput);
         this._currentPageTxt = ComponentFactory.Instance.creatComponentByStylename("magicStone.shopFrame.pageTxt");
         this._navigationBarContainer.addChild(this._currentPageTxt);
         this._scoreNumBG = ComponentFactory.Instance.creatComponentByStylename("magicStone.shopFrame.coinBG");
         this._scoreText = ComponentFactory.Instance.creatComponentByStylename("magicStone.shopFrame.scoreText");
         this._scoreText.text = LanguageMgr.GetTranslation("magicStone.remainScore");
         this._scoreNumText = ComponentFactory.Instance.creatComponentByStylename("magicStone.shopFrame.scoreNumTxt");
         this._scoreNumText.text = MagicStoneManager.instance.mgStoneScore.toString();
         this._label = ComponentFactory.Instance.creatComponentByStylename("magicStone.shopFrame.label");
         this._label.text = LanguageMgr.GetTranslation("magicStone.onlyBindedCanBuy");
         addToContent(this._goodItemContainerBg);
         addToContent(this._goodItemContainerAll);
         addToContent(this._navigationBarContainer);
         addToContent(this._scoreNumBG);
         addToContent(this._scoreText);
         addToContent(this._scoreNumText);
         addToContent(this._label);
         _loc3_ = 0;
         while(_loc3_ < SHOP_ITEM_NUM)
         {
            this._goodItems[_loc3_] = new MagicStoneShopItem();
            _loc4_ = this._goodItems[_loc3_].width;
            _loc5_ = this._goodItems[_loc3_].height;
            _loc4_ = _loc4_ * int(_loc3_ % 2);
            _loc5_ = _loc5_ * int(_loc3_ / 2);
            this._goodItems[_loc3_].x = _loc4_;
            this._goodItems[_loc3_].y = _loc5_ + _loc3_ / 2 * 2;
            this._goodItemContainerAll.addChild(this._goodItems[_loc3_]);
            this._goodItems[_loc3_].setItemLight(this._rightItemLightMc);
            _loc3_++;
         }
      }
      
      public function loadList() : void
      {
         this.setList(ShopManager.Instance.getValidSortedGoodsByType(this.getType(),this.curPage));
         SocketManager.Instance.out.updateRemainCount();
      }
      
      public function setList(param1:Vector.<ShopItemInfo>) : void
      {
         this.clearitems();
         var _loc2_:int = 0;
         while(_loc2_ < SHOP_ITEM_NUM)
         {
            this._goodItems[_loc2_].selected = false;
            if(!param1)
            {
               break;
            }
            if(_loc2_ < param1.length && param1[_loc2_])
            {
               this._goodItems[_loc2_].shopItemInfo = param1[_loc2_];
            }
            _loc2_++;
         }
         this._currentPageTxt.text = this.curPage + "/" + ShopManager.Instance.getResultPages(this.getType());
      }
      
      private function clearitems() : void
      {
         var _loc1_:int = 0;
         while(_loc1_ <= this._goodItems.length - 1)
         {
            this._goodItems[_loc1_].shopItemInfo = null;
            _loc1_++;
         }
      }
      
      private function initEvents() : void
      {
         this._prePageBtn.addEventListener(MouseEvent.CLICK,this.__pageBtnClick);
         this._nextPageBtn.addEventListener(MouseEvent.CLICK,this.__pageBtnClick);
         SocketManager.Instance.addEventListener(MagicStoneEvent.UPDATE_REMAIN_COUNT,this.__updateRemainCount);
      }
      
      protected function __updateRemainCount(param1:MagicStoneEvent) : void
      {
         var _loc5_:MagicStoneShopItem = null;
         var _loc2_:PackageIn = param1.pkg;
         var _loc3_:int = _loc2_.readInt();
         var _loc4_:int = 0;
         while(_loc4_ <= this._goodItems.length - 1)
         {
            _loc5_ = this._goodItems[_loc4_];
            if(_loc5_.shopItemInfo && _loc5_.shopItemInfo.TemplateInfo.Property3 == "0")
            {
               _loc5_.setRemainCount(_loc3_);
            }
            _loc4_++;
         }
      }
      
      private function __pageBtnClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         if(ShopManager.Instance.getResultPages(this.getType()) == 0)
         {
            return;
         }
         switch(param1.currentTarget)
         {
            case this._prePageBtn:
               if(this.curPage == 1)
               {
                  this.curPage = ShopManager.Instance.getResultPages(this.getType()) + 1;
               }
               this.curPage--;
               break;
            case this._nextPageBtn:
               if(this.curPage == ShopManager.Instance.getResultPages(this.getType()))
               {
                  this.curPage = 0;
               }
               this.curPage++;
         }
         this.loadList();
      }
      
      public function updateScore(param1:int) : void
      {
         if(this._scoreNumText)
         {
            this._scoreNumText.text = param1.toString();
         }
      }
      
      public function show() : void
      {
         LayerManager.Instance.addToLayer(this,LayerManager.GAME_DYNAMIC_LAYER,true,LayerManager.ALPHA_BLOCKGOUND);
      }
      
      private function removeEvents() : void
      {
         this._prePageBtn.removeEventListener(MouseEvent.CLICK,this.__pageBtnClick);
         this._nextPageBtn.removeEventListener(MouseEvent.CLICK,this.__pageBtnClick);
         SocketManager.Instance.removeEventListener(MagicStoneEvent.UPDATE_REMAIN_COUNT,this.__updateRemainCount);
      }
      
      override public function dispose() : void
      {
         MagicStoneManager.instance.shopFrame = null;
         this.removeEvents();
         var _loc1_:int = 0;
         while(_loc1_ <= this._goodItems.length - 1)
         {
            ObjectUtils.disposeObject(this._goodItems[_loc1_]);
            this._goodItems[_loc1_] = null;
            _loc1_++;
         }
         ObjectUtils.disposeObject(this._rightItemLightMc);
         this._rightItemLightMc = null;
         ObjectUtils.disposeObject(this._goodItemContainerAll);
         this._goodItemContainerAll = null;
         ObjectUtils.disposeObject(this._goodItemContainerBg);
         this._goodItemContainerBg = null;
         ObjectUtils.disposeObject(this._navigationBarContainer);
         this._navigationBarContainer = null;
         ObjectUtils.disposeObject(this._prePageBtn);
         this._prePageBtn = null;
         ObjectUtils.disposeObject(this._nextPageBtn);
         this._nextPageBtn = null;
         ObjectUtils.disposeObject(this._currentPageTxt);
         this._currentPageTxt = null;
         ObjectUtils.disposeObject(this._currentPageInput);
         this._currentPageInput = null;
         ObjectUtils.disposeObject(this._scoreNumBG);
         this._scoreNumBG = null;
         ObjectUtils.disposeObject(this._scoreText);
         this._scoreText = null;
         ObjectUtils.disposeObject(this._scoreNumText);
         this._scoreNumText = null;
         ObjectUtils.disposeObject(this._label);
         this._label = null;
         super.dispose();
      }
      
      public function getType() : int
      {
         return 101;
      }
   }
}
