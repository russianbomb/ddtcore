package login.view
{
   import com.pickgliss.events.FrameEvent;
   import com.pickgliss.events.ListItemEvent;
   import com.pickgliss.loader.BaseLoader;
   import com.pickgliss.loader.LoadResourceManager;
   import com.pickgliss.loader.LoaderEvent;
   import com.pickgliss.loader.RequestLoader;
   import com.pickgliss.ui.AlertManager;
   import com.pickgliss.ui.ComponentFactory;
   import com.pickgliss.ui.LayerManager;
   import com.pickgliss.ui.controls.BaseButton;
   import com.pickgliss.ui.controls.Frame;
   import com.pickgliss.ui.controls.ListPanel;
   import com.pickgliss.ui.controls.TextButton;
   import com.pickgliss.ui.controls.alert.BaseAlerFrame;
   import com.pickgliss.ui.controls.container.VBox;
   import com.pickgliss.ui.image.MutipleImage;
   import com.pickgliss.ui.text.FilterFrameText;
   import com.pickgliss.utils.ObjectUtils;
   import ddt.command.StripTip;
   import ddt.data.Role;
   import ddt.manager.LanguageMgr;
   import ddt.manager.PathManager;
   import ddt.manager.PlayerManager;
   import ddt.manager.SelectListManager;
   import ddt.manager.SoundManager;
   import ddt.view.tips.OneLineTip;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.MouseEvent;
   import flash.filters.ColorMatrixFilter;
   import flash.net.URLVariables;
   
   public class ChooseRoleFrame extends Frame
   {
       
      
      private var _visible:Boolean = true;
      
      private var _listBack:MutipleImage;
      
      private var _enterButton:BaseButton;
      
      private var _list:VBox;
      
      private var _selectedItem:RoleItem;
      
      private var _disenabelFilter:ColorMatrixFilter;
      
      private var _rename:Boolean = false;
      
      private var _renameFrame:RoleRenameFrame;
      
      private var _consortiaRenameFrame:ConsortiaRenameFrame;
      
      private var _roleList:ListPanel;
      
      private var _gradeText:FilterFrameText;
      
      private var _nameText:FilterFrameText;
      
      private var _recoverBtn:TextButton;
      
      private var _deleteBtn:TextButton;
      
      private var _recoverBtnStrip:StripTip;
      
      private var _deleteBtnStrip:StripTip;
      
      private var _oneLineTip:OneLineTip;
      
      private var _ReOrDeOperate:int;
      
      private var _recordOperateRoleItem:RoleItem;
      
      public function ChooseRoleFrame()
      {
         super();
         this.configUi();
      }
      
      private function configUi() : void
      {
         this._disenabelFilter = ComponentFactory.Instance.model.getSet("login.ChooseRole.DisenableGF");
         titleStyle = "login.Title";
         titleText = LanguageMgr.GetTranslation("tank.loginstate.chooseCharacter");
         this._listBack = ComponentFactory.Instance.creatComponentByStylename("login.chooseRoleFrame.bg");
         addToContent(this._listBack);
         this._gradeText = ComponentFactory.Instance.creatComponentByStylename("login.chooseRoleFrame.gradeText");
         addToContent(this._gradeText);
         this._nameText = ComponentFactory.Instance.creatComponentByStylename("login.chooseRoleFrame.nameText");
         addToContent(this._nameText);
         this._roleList = ComponentFactory.Instance.creatComponentByStylename("login.ChooseRole.RoleList");
         addToContent(this._roleList);
         this._recoverBtn = ComponentFactory.Instance.creatComponentByStylename("login.choooseRoleFrame.recoverBtn");
         this._recoverBtn.text = LanguageMgr.GetTranslation("ddt.chooseRoleFrame.recoverBtnTxt");
         addToContent(this._recoverBtn);
         this._recoverBtnStrip = ComponentFactory.Instance.creatComponentByStylename("login.chooseRoleFrame.textBtnStrip");
         this._recoverBtnStrip.x = this._recoverBtn.x;
         this._recoverBtnStrip.y = this._recoverBtn.y;
         addToContent(this._recoverBtnStrip);
         this._recoverBtnStrip.visible = false;
         this._deleteBtn = ComponentFactory.Instance.creatComponentByStylename("login.choooseRoleFrame.deleteBtn");
         this._deleteBtn.text = LanguageMgr.GetTranslation("ddt.chooseRoleFrame.deleteBtnTxt");
         addToContent(this._deleteBtn);
         this._deleteBtnStrip = ComponentFactory.Instance.creatComponentByStylename("login.chooseRoleFrame.textBtnStrip");
         this._deleteBtnStrip.x = this._deleteBtn.x;
         this._deleteBtnStrip.y = this._deleteBtn.y;
         addToContent(this._deleteBtnStrip);
         this._deleteBtnStrip.visible = false;
         this._enterButton = ComponentFactory.Instance.creatComponentByStylename("login.ChooseRole.EnterButton");
         addToContent(this._enterButton);
         this._oneLineTip = new OneLineTip();
         addToContent(this._oneLineTip);
         this._oneLineTip.visible = false;
         this.addEvent();
         var _loc1_:int = 0;
         while(_loc1_ < SelectListManager.Instance.list.length)
         {
            this.addRole(SelectListManager.Instance.list[_loc1_] as Role);
            _loc1_++;
         }
         AlertManager.Instance.layerType = LayerManager.STAGE_TOP_LAYER;
      }
      
      private function addEvent() : void
      {
         this._enterButton.addEventListener(MouseEvent.CLICK,this.__onEnterClick);
         this._roleList.list.addEventListener(ListItemEvent.LIST_ITEM_CLICK,this.__onRoleClick);
         this._recoverBtn.addEventListener(MouseEvent.CLICK,this.recoverOrDeleteHandler,false,0,true);
         this._deleteBtn.addEventListener(MouseEvent.CLICK,this.recoverOrDeleteHandler,false,0,true);
         this._recoverBtn.addEventListener(MouseEvent.MOUSE_OVER,this.overHandler,false,0,true);
         this._recoverBtn.addEventListener(MouseEvent.MOUSE_OUT,this.outHandler,false,0,true);
         this._deleteBtn.addEventListener(MouseEvent.MOUSE_OVER,this.overHandler,false,0,true);
         this._deleteBtn.addEventListener(MouseEvent.MOUSE_OUT,this.outHandler,false,0,true);
         this._recoverBtnStrip.addEventListener(MouseEvent.MOUSE_OVER,this.overHandler,false,0,true);
         this._recoverBtnStrip.addEventListener(MouseEvent.MOUSE_OUT,this.outHandler,false,0,true);
         this._deleteBtnStrip.addEventListener(MouseEvent.MOUSE_OVER,this.overHandler,false,0,true);
         this._deleteBtnStrip.addEventListener(MouseEvent.MOUSE_OUT,this.outHandler,false,0,true);
      }
      
      private function overHandler(param1:MouseEvent) : void
      {
         var _loc2_:Sprite = param1.target as Sprite;
         var _loc3_:String = "";
         switch(_loc2_)
         {
            case this._recoverBtn:
            case this._recoverBtnStrip:
               _loc3_ = LanguageMgr.GetTranslation("ddt.chooseRoleFrame.recoverBtnTipTxt");
               break;
            case this._deleteBtn:
            case this._deleteBtnStrip:
               _loc3_ = LanguageMgr.GetTranslation("ddt.chooseRoleFrame.deleteBtnTipTxt");
         }
         this._oneLineTip.tipData = _loc3_;
         this._oneLineTip.x = _loc2_.x - (this._oneLineTip.width - _loc2_.width) / 2;
         this._oneLineTip.y = _loc2_.y + _loc2_.height;
         this._oneLineTip.visible = true;
      }
      
      private function outHandler(param1:MouseEvent) : void
      {
         this._oneLineTip.visible = false;
      }
      
      private function recoverOrDeleteHandler(param1:MouseEvent) : void
      {
         var _loc2_:String = null;
         SoundManager.instance.play("008");
         if(!this._selectedItem)
         {
            return;
         }
         if(param1.target == this._deleteBtn)
         {
            _loc2_ = LanguageMgr.GetTranslation("ddt.chooseRoleFrame.deleteTipTxt");
            this._ReOrDeOperate = 1;
         }
         else
         {
            _loc2_ = LanguageMgr.GetTranslation("ddt.chooseRoleFrame.recoverTipTxt");
            this._ReOrDeOperate = 2;
         }
         var _loc3_:BaseAlerFrame = AlertManager.Instance.simpleAlert(LanguageMgr.GetTranslation("AlertDialog.Info"),_loc2_,LanguageMgr.GetTranslation("ok"),LanguageMgr.GetTranslation("cancel"),true,false,false,LayerManager.BLCAK_BLOCKGOUND);
         _loc3_.moveEnable = false;
         _loc3_.addEventListener(FrameEvent.RESPONSE,this.__confirm);
      }
      
      private function __confirm(param1:FrameEvent) : void
      {
         var _loc3_:URLVariables = null;
         var _loc4_:RequestLoader = null;
         SoundManager.instance.play("008");
         var _loc2_:BaseAlerFrame = param1.currentTarget as BaseAlerFrame;
         _loc2_.removeEventListener(FrameEvent.RESPONSE,this.__confirm);
         if(param1.responseCode == FrameEvent.SUBMIT_CLICK || param1.responseCode == FrameEvent.ENTER_CLICK)
         {
            this._recordOperateRoleItem = this._selectedItem;
            _loc3_ = new URLVariables();
            _loc3_["username"] = PlayerManager.Instance.Account.Account;
            _loc3_["nickname"] = this._selectedItem.roleInfo.NickName;
            _loc3_["operation"] = this._ReOrDeOperate;
            _loc4_ = LoadResourceManager.Instance.createLoader(PathManager.solveRequestPath("LoginRemoveSmallAccount.ashx"),BaseLoader.REQUEST_LOADER,_loc3_);
            _loc4_.addEventListener(LoaderEvent.LOAD_ERROR,this.__onRequestRecoverDeleteError,false,0,true);
            _loc4_.addEventListener(LoaderEvent.COMPLETE,this.__onRequestRecoverDeleteComplete,false,0,true);
            LoadResourceManager.Instance.startLoad(_loc4_);
         }
      }
      
      private function __onRequestRecoverDeleteError(param1:LoaderEvent) : void
      {
         this._recordOperateRoleItem = null;
         var _loc2_:RequestLoader = param1.target as RequestLoader;
         _loc2_.removeEventListener(LoaderEvent.LOAD_ERROR,this.__onRequestRecoverDeleteError);
         _loc2_.removeEventListener(LoaderEvent.COMPLETE,this.__onRequestRecoverDeleteComplete);
      }
      
      private function __onRequestRecoverDeleteComplete(param1:LoaderEvent) : void
      {
         var _loc4_:String = null;
         var _loc5_:int = 0;
         var _loc6_:Vector.<Role> = null;
         var _loc7_:Role = null;
         var _loc2_:RequestLoader = param1.target as RequestLoader;
         _loc2_.removeEventListener(LoaderEvent.LOAD_ERROR,this.__onRequestRecoverDeleteError);
         _loc2_.removeEventListener(LoaderEvent.COMPLETE,this.__onRequestRecoverDeleteComplete);
         var _loc3_:XML = new XML(param1.loader.content);
         if(_loc3_.@value == "true")
         {
            _loc4_ = _loc3_.@NickName;
            _loc5_ = int(_loc3_.@LoginState);
            _loc6_ = SelectListManager.Instance.list;
            for each(_loc7_ in _loc6_)
            {
               if(_loc7_.NickName == _loc4_)
               {
                  _loc7_.LoginState = _loc5_;
                  break;
               }
            }
            if(this._recordOperateRoleItem)
            {
               this._recordOperateRoleItem.refreshDeleteIcon();
            }
            this.judgeSelecteRoleState();
         }
         this._recordOperateRoleItem = null;
      }
      
      private function removeEvent() : void
      {
         if(this._enterButton)
         {
            this._enterButton.removeEventListener(MouseEvent.CLICK,this.__onEnterClick);
         }
         this._roleList.list.removeEventListener(ListItemEvent.LIST_ITEM_CLICK,this.__onRoleClick);
         if(this._recoverBtn)
         {
            this._recoverBtn.removeEventListener(MouseEvent.CLICK,this.recoverOrDeleteHandler);
            this._recoverBtn.removeEventListener(MouseEvent.MOUSE_OVER,this.overHandler);
            this._recoverBtn.removeEventListener(MouseEvent.MOUSE_OUT,this.outHandler);
         }
         if(this._deleteBtn)
         {
            this._deleteBtn.removeEventListener(MouseEvent.CLICK,this.recoverOrDeleteHandler);
            this._deleteBtn.removeEventListener(MouseEvent.MOUSE_OVER,this.overHandler);
            this._deleteBtn.removeEventListener(MouseEvent.MOUSE_OUT,this.outHandler);
         }
         if(this._recoverBtnStrip)
         {
            this._recoverBtnStrip.removeEventListener(MouseEvent.MOUSE_OVER,this.overHandler);
            this._recoverBtnStrip.removeEventListener(MouseEvent.MOUSE_OUT,this.outHandler);
         }
         if(this._deleteBtnStrip)
         {
            this._deleteBtnStrip.removeEventListener(MouseEvent.MOUSE_OVER,this.overHandler);
            this._deleteBtnStrip.removeEventListener(MouseEvent.MOUSE_OUT,this.outHandler);
         }
      }
      
      private function __onEnterClick(param1:MouseEvent) : void
      {
         SoundManager.instance.play("008");
         if(this._selectedItem == null)
         {
            return;
         }
         if(this._selectedItem.roleInfo.Rename || this._selectedItem.roleInfo.ConsortiaRename)
         {
            if(this._selectedItem.roleInfo.Rename && !this._selectedItem.roleInfo.NameChanged)
            {
               this.startRename(this._selectedItem.roleInfo);
               return;
            }
            if(this._selectedItem.roleInfo.ConsortiaRename && !this._selectedItem.roleInfo.ConsortiaNameChanged)
            {
               this.startRenameConsortia(this._selectedItem.roleInfo);
               return;
            }
            dispatchEvent(new Event(Event.COMPLETE));
         }
         else
         {
            dispatchEvent(new Event(Event.COMPLETE));
         }
      }
      
      private function __onRoleClick(param1:ListItemEvent) : void
      {
         var _loc2_:RoleItem = param1.cell as RoleItem;
         this.selectedItem = _loc2_;
      }
      
      private function startRenameConsortia(param1:Role) : void
      {
         this._consortiaRenameFrame = ComponentFactory.Instance.creatComponentByStylename("ConsortiaRenameFrame");
         this._consortiaRenameFrame.roleInfo = param1;
         this._consortiaRenameFrame.addEventListener(Event.COMPLETE,this.__consortiaRenameComplete);
         LayerManager.Instance.addToLayer(this._consortiaRenameFrame,LayerManager.STAGE_TOP_LAYER,true,LayerManager.ALPHA_BLOCKGOUND);
      }
      
      private function startRename(param1:Role) : void
      {
         this._renameFrame = ComponentFactory.Instance.creatComponentByStylename("RoleRenameFrame");
         this._renameFrame.roleInfo = param1;
         this._renameFrame.addEventListener(Event.COMPLETE,this.__onRenameComplete);
         LayerManager.Instance.addToLayer(this._renameFrame,LayerManager.STAGE_TOP_LAYER,true,LayerManager.ALPHA_BLOCKGOUND);
      }
      
      private function __onRenameComplete(param1:Event) : void
      {
         this._renameFrame.removeEventListener(Event.COMPLETE,this.__onRenameComplete);
         ObjectUtils.disposeObject(this._renameFrame);
         this._renameFrame = null;
         this.__onEnterClick(null);
      }
      
      private function __consortiaRenameComplete(param1:Event) : void
      {
         this._consortiaRenameFrame.removeEventListener(Event.COMPLETE,this.__onRenameComplete);
         ObjectUtils.disposeObject(this._consortiaRenameFrame);
         this._consortiaRenameFrame = null;
         this.__onEnterClick(null);
      }
      
      public function addRole(param1:Role) : void
      {
         this._roleList.vectorListModel.insertElementAt(param1,this._roleList.vectorListModel.elements.length);
      }
      
      override public function dispose() : void
      {
         AlertManager.Instance.layerType = LayerManager.STAGE_DYANMIC_LAYER;
         this._visible = false;
         this.removeEvent();
         if(this._listBack)
         {
            ObjectUtils.disposeObject(this._listBack);
            this._listBack = null;
         }
         if(this._gradeText)
         {
            ObjectUtils.disposeObject(this._gradeText);
            this._gradeText = null;
         }
         if(this._nameText)
         {
            ObjectUtils.disposeObject(this._nameText);
            this._nameText = null;
         }
         if(this._roleList)
         {
            ObjectUtils.disposeObject(this._roleList);
            this._roleList = null;
         }
         if(this._recoverBtn)
         {
            ObjectUtils.disposeObject(this._recoverBtn);
            this._recoverBtn = null;
         }
         if(this._deleteBtn)
         {
            ObjectUtils.disposeObject(this._deleteBtn);
            this._deleteBtn = null;
         }
         if(this._recoverBtnStrip)
         {
            ObjectUtils.disposeObject(this._recoverBtnStrip);
            this._recoverBtnStrip = null;
         }
         if(this._deleteBtnStrip)
         {
            ObjectUtils.disposeObject(this._deleteBtnStrip);
            this._deleteBtnStrip = null;
         }
         this._recordOperateRoleItem = null;
         if(this._enterButton)
         {
            ObjectUtils.disposeObject(this._enterButton);
            this._enterButton = null;
         }
         super.dispose();
      }
      
      public function get selectedRole() : Role
      {
         return this._selectedItem.roleInfo;
      }
      
      public function get selectedItem() : RoleItem
      {
         return this._selectedItem;
      }
      
      public function set selectedItem(param1:RoleItem) : void
      {
         var _loc2_:RoleItem = null;
         if(this._selectedItem != param1)
         {
            _loc2_ = this._selectedItem;
            this._selectedItem = param1;
            if(this._selectedItem != null)
            {
               this._selectedItem.selected = true;
               SelectListManager.Instance.currentLoginRole = this._selectedItem.roleInfo;
               this.judgeSelecteRoleState();
            }
            if(_loc2_)
            {
               _loc2_.selected = false;
               _loc2_ = null;
            }
         }
      }
      
      private function judgeSelecteRoleState() : void
      {
         if(this._selectedItem.roleInfo.LoginState == 1)
         {
            this._enterButton.enable = false;
            this._recoverBtn.enable = true;
            this._deleteBtn.enable = false;
            this._recoverBtnStrip.visible = false;
            this._deleteBtnStrip.visible = true;
         }
         else
         {
            this._enterButton.enable = true;
            this._recoverBtn.enable = false;
            this._recoverBtnStrip.visible = true;
            if(this._selectedItem.roleInfo.Grade >= 40 || SelectListManager.Instance.haveNotDeleteRoleNum == 1)
            {
               this._deleteBtn.enable = false;
               this._deleteBtnStrip.visible = true;
            }
            else
            {
               this._deleteBtn.enable = true;
               this._deleteBtnStrip.visible = false;
            }
         }
      }
   }
}
