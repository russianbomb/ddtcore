package avmplus
{
   public final class DescribeType
   {
      public static const HIDE_NSURI_METHODS:uint    = 0x0001;
      public static const INCLUDE_BASES:uint         = 0x0002;
      public static const INCLUDE_INTERFACES:uint    = 0x0004;
      public static const INCLUDE_VARIABLES:uint     = 0x0008;
      public static const INCLUDE_ACCESSORS:uint     = 0x0010;
      public static const INCLUDE_METHODS:uint       = 0x0020;
      public static const INCLUDE_METADATA:uint      = 0x0040;
      public static const INCLUDE_CONSTRUCTOR:uint   = 0x0080;
      public static const INCLUDE_TRAITS:uint        = 0x0100;
      public static const USE_ITRAITS:uint           = 0x0200;
      // if set, hide everything from the base Object class
      public static const HIDE_OBJECT:uint           = 0x0400;

      // set of flags that replicates the behavior of flash.util.describeType in FlashPlayer 9 & 10
      public const FLASH10_FLAGS:uint =   INCLUDE_BASES |
              INCLUDE_INTERFACES |
              INCLUDE_VARIABLES |
              INCLUDE_ACCESSORS |
              INCLUDE_METHODS |
              INCLUDE_METADATA |
              INCLUDE_CONSTRUCTOR |
              INCLUDE_TRAITS |
              // include this buggy behavior by default, to match legacy Flash behavior
              HIDE_NSURI_METHODS |
              // Flash hides everything in class Object
              HIDE_OBJECT;

      public static const ACCESSOR_FLAGS:uint = INCLUDE_TRAITS | INCLUDE_ACCESSORS;

      public static const INTERFACE_FLAGS:uint = INCLUDE_TRAITS | INCLUDE_INTERFACES;

      public static const METHOD_FLAGS:uint = INCLUDE_TRAITS | INCLUDE_METHODS;

      public static const VARIABLE_FLAGS:uint = INCLUDE_TRAITS | INCLUDE_VARIABLES;

      public static const GET_INSTANCE_INFO:uint = INCLUDE_BASES | INCLUDE_INTERFACES | INCLUDE_VARIABLES | INCLUDE_ACCESSORS | INCLUDE_METHODS | INCLUDE_METADATA | INCLUDE_CONSTRUCTOR | INCLUDE_TRAITS | USE_ITRAITS;

      public static const GET_CLASS_INFO:uint = INCLUDE_INTERFACES | INCLUDE_VARIABLES | INCLUDE_ACCESSORS | INCLUDE_METHODS | INCLUDE_METADATA | INCLUDE_TRAITS | HIDE_OBJECT;


      public function DescribeType()
      {
         super();
      }

      public static function getJSONFunction():Function
      {
         try
         {
            return null;
         }
         catch (e:*)
         {
         }
         return null;
      }
   }
}
